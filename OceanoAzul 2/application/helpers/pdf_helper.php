<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function pdf($html, $filename, $stream) 
{
/*
    //require_once("dompdf/dompdf_config.inc.php");
    require_once("dompdf/dompdf/autoload.inc.php");
    //use Dompdf\Dompdf;
    $dompdf = new Dompdf\DOMPDF();
    //$dompdf->getDomPDF()->get_option('enable_html5_parser');
    //$dompdf->set_option('enable_html5_parser', true);
    $dompdf->load_html($html);
    $dompdf->render();
    
    if ($stream) {
    
        $dompdf->stream($filename.".pdf");
        
    } else {
    
        return $dompdf->output();
    }
    
*/
    require_once 'dompdf/dompdf/autoload.inc.php';
    
    $options= new Dompdf\options();
    $options->set('isRemoteEnabled',TRUE);

    $dompdf = new Dompdf\DOMPDF($options);
    $dompdf->loadHtml(ob_get_clean());
    $dompdf->loadHtml($html);
    $dompdf->render();

    if ($stream) {
        $filename =$filename;
        $dompdf->stream($filename.".pdf");
        
    } else {
    
        //return $dompdf->output();
        return $dompdf->output();
    }
    
    //$filename = "profesionales.pdf";
    
    //file_put_contents($filename, $pdf);
    //$dompdf->stream($filename);
}
?> 
