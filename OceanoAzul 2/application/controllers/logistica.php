<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Logistica extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('logistica_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('logistica_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('logistica/lista',$data);
        }
		function pdfrepe( ) {
            $this->load->model('logistica_model');
			//$data['result']=$this->cargos_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('logistica/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$data['ini'] = $this->input->post('ini');
			$data['fin'] = $this->input->post('fin');
			//$data['tablac'] = $this->input->post('mytablaCD');
			$html = $this->load->view('logistica/listapdfe', $data, true);  
			pdf ($html,'logistica/listapdfe', true);
        	set_paper('letter');
        }
		function pdfrepeva( ) {
            $this->load->model('logistica_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('logistica/lista',$data);
			$data['res'] = $this->input->post('inomrespon');
			$data['ent'] = $this->input->post('ient');
			$data['ser1'] = $this->input->post('iser1');
			$data['serc'] = $this->input->post('iserc');
			$data['act1'] = $this->input->post('iact1');
			$data['acta'] = $this->input->post('iacta');
			$data['pre1'] = $this->input->post('ipre1');
			$data['prep'] = $this->input->post('iprep');
			$data['efi1'] = $this->input->post('iefi1');
			$data['efie'] = $this->input->post('iefie');
			$data['adm1'] = $this->input->post('iadm1');
			$data['adma'] = $this->input->post('iadma');
			$data['gra1'] = $this->input->post('igra1');
			$data['grag'] = $this->input->post('igrag');
			$data['tablac'] = $this->input->post('tablaevatec');
			
			$html = $this->load->view('logistica/listapdfevatec', $data, true); 
			pdf ($html,'logistica/evatec', true);	
        }
		function tabla($extra=0,$desde='',$hasta=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($desde!='') $filter['where']['fechar >=']=$desde;
			if($hasta!='') $filter['where']['fechar <=']=$hasta; 
			$filter['num'] = $extra;			
        	$data['rows'] = $this->logistica_model->getEntregas($filter);
        	$data['num_rows'] = $this->logistica_model->getNumRowsE($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablatecres(){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->logistica_model->getEntregastecres($filter);
        	//$data['num_rows'] = $this->logistica_model->getNumRowsEtecres($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablatecresdet($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->logistica_model->getEntregastecresdet($extra);
        	//$data['num_rows'] = $this->logistica_model->getNumRowsEtecres($filter);
        	echo '('.json_encode($data).')'; 
    	}
		/*public function combo(){        
        	$filter['NomDes']=$this->input->post('NomDes');           
        	$data = $this->cargos_model->getElements($filter);        
        	echo '('.json_encode($data).')'; 
    	}*/
		 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('logistica_model');		
		$nbs=$this->input->post('enc');
		$fol=$this->input->post('fol');
		$ser=$this->input->post('ser');
		$act=$this->input->post('act');
		$pre=$this->input->post('pre');
		$efi=$this->input->post('efi');		
		$adm=$this->input->post('adm');		
		$obs=$this->input->post('obs');
		$foc=$this->input->post('alerta');	
		$nr=$this->input->post('nr');
		$nc=$this->input->post('nc');		
		if($fol!=''){	
			$this->logistica_model->agregar($nbs,$fol,$act,$pre,$efi,$ser,$adm,$obs,$foc,$nr,$nc);			
			redirect('logistica');
		}
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('logistica',$datos);
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('logistica_model');
		$id_post=$this->input->post('id'); 
		$nbs=$this->input->post('enc');
		$fol=$this->input->post('fol');
		$ser=$this->input->post('ser');
		$act=$this->input->post('act');
		$pre=$this->input->post('pre');
		$efi=$this->input->post('efi');		
		$adm=$this->input->post('adm');		
		$obs=$this->input->post('obs');
		$foc=$this->input->post('alerta');		
		$veri=$this->input->post('veri');
		if($id_post!=''){
			$return=$this->logistica_model->actualizar($id_post,$nbs,$fol,$act,$pre,$efi,$ser,$adm,$obs,$foc,$veri); 			
			redirect('logistica');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->logistica_model->getCliente($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('logistica/actualizar',$datos);
		}
		
		function borrar($id=0){
		$this->load->helper('url');
		$this->load->model('logistica_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->logistica_model->borrar($id_post); 			
			redirect('logistica');
		}
		}
		
		//tecnicos
		function tablatec($extra=0,$rz=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//if($extra=='1') $filter['where']['estatus <=']=$extra; else $filter['where']['estatus =']=$extra;
			$filter['where']['numbios =']=$extra;
			$reza=$rz;
			$data['rows'] = $this->logistica_model->getTecnicos($filter,$reza);
        	$data['num_rows'] = $this->logistica_model->getNumRowsT($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablatotet($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//if($extra=='1') $filter['where']['estatus <=']=$extra; else $filter['where']['estatus =']=$extra;
			//$filter['where']['numbios =']=$extra;
			$data['rows'] = $this->logistica_model->getTotet($filter);
        	$data['num_rows'] = $this->logistica_model->getNumRowsTotet($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablatotdeti($bio=0,$mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//if($extra=='1') $filter['where']['estatus <=']=$extra; else $filter['where']['estatus =']=$extra;
			$filter['where']['numbios =']=$bio;
			if($mes>0) $filter['where']['month(FechaR) =']=$mes;
			$data['rows'] = $this->logistica_model->getTecnicosdet($filter);
        	$data['num_rows'] = $this->logistica_model->getNumRowsTdet($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function pdfrept() {
            $this->load->model('logistica_model');
			
			$data['tecnico'] = $this->input->post('tecnico');
			$data['total'] = $this->input->post('totel');
			$datax['result']=$this->logistica_model->verTecnico1($data['tecnico']);
			foreach ($datax['result'] as $row): 
				$tec=$row->NomBio;
			endforeach;
			$data['tecnico']=$tec;
			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('logistica/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('logistica/listapdft', $data, true);  
			pdf ($html,'logistica/listapdft', true);        	
        	set_paper('letter');
			
        }
		function pdfrepdet() {
            $this->load->model('logistica_model');
			
			$data['tecnico'] = $this->input->post('tecnico1');
			$data['total'] = $this->input->post('toteld');
			$datax['result']=$this->logistica_model->verTecnico1($data['tecnico']);
			foreach ($datax['result'] as $row): 
				$tec=$row->NomBio;
			endforeach;
			$data['tecnico']=$tec;
			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('logistica/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('logistica/listapdfdet', $data, true);  
			pdf ($html,'logistica/listapdfdet', true);        	
        	set_paper('letter');
			
        }
		function pdfreptmes() {
            $this->load->model('logistica_model');
			$mes = $this->input->post('mez');
			if($mes==1) $mes="Enero";
			if($mes==2) $mes="Febrero";
			if($mes==3) $mes="Marzo";
			if($mes==4) $mes="Abril";
			if($mes==5) $mes="Mayo";
			if($mes==6) $mes="Junio";
			if($mes==7) $mes="Julio";
			if($mes==8) $mes="Agosto";
			if($mes==9) $mes="Septiembre";
			$data['mes'] = $mes;
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('logistica/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('logistica/listapdfdetmes', $data, true);  
			pdf ($html,'logistica/listapdfdetmes', true);        	
        	set_paper('letter');
			
        }	
		function pdfreptcontec() {
            $this->load->model('logistica_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('logistica/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('logistica/listapdfcontec', $data, true);  
			pdf ($html,'logistica/listapdfcontec', true);        	
        	//set_paper('letter');
			
        }	
		//general por mes
		function tablatecmes($extra=0,$rz=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//if($extra=='1') $filter['where']['estatus <=']=$extra; else $filter['where']['estatus =']=$extra;
			$filter['where']['month(fechar) =']=$extra;
			$reza=$rz;
			$data['rows'] = $this->logistica_model->getTecnicosGral($filter,$reza);
        	$data['num_rows'] = $this->logistica_model->getNumRowsTGral($filter);
        	echo '('.json_encode($data).')'; 
    	}
    }
    
?>