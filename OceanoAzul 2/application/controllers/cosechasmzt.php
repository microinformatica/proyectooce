<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     class Cosechasmzt extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
       	$this->load->model('cosechasmzt_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('cosechasmzt_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('cosechasmzt/lista',$data);
        }
		function pdfrepcos( ) {
            $this->load->model('cosechasmzt_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('cosechasmzt/lista',$data);
			$data['tabla'] = $this->input->post('tabla');
			$data['dia'] = $this->input->post('dia');
			$data['tip'] = $this->input->post('tip');
			$html = $this->load->view('cosechasmzt/listapdfdia', $data, true);  
			if($data['tip']==0) pdf ($html,$data['dia'].'_Gral', true);
			if($data['tip']==1 || $data['tip']==2) pdf ($html,$data['dia'].'_Maq', true);         	
        }
		function pdfrepcosg( ) {
            $this->load->model('cosechasmzt_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('cosechasmzt/lista',$data);
			$data['tabla'] = $this->input->post('tablag');
			$html = $this->load->view('cosechasmzt/listapdfgral', $data, true);  
			//$dia=date("Y-m-d");
			pdf ($html,'Rep_Gral_'.date("d-m-Y"), true);
			         	
        }
		public function tablacosechas($ciclo=0,$dia=0,$tipo=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($dia!=0){$filter['where']['feccos =']=$dia;}
			if($tipo>0) {$filter['where']['tipcos =']=$tipo;}
			
        	$data['rows'] = $this->cosechasmzt_model->cosechas($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		public function tablagral($ciclo=0,$secc=0){
			$filter = $this->ajaxsorter->filter($this->input);	
			$filter['where']['numgra =']=4;
			//$filter['where']['ciccha =']=$ciclo;
			if($secc>0) $filter['where']['secc =']=$secc;        
        	  
			$data['rows'] = $this->cosechasmzt_model->gral($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		function agregaCos(){
		$this->load->helper('url');
		$this->load->model('cosechasmzt_model');
		$fec=$this->input->post('fec'); 
		$tip=$this->input->post('tip');
		$num=$this->input->post('num');
		$cli=$this->input->post('cli');
		$est=$this->input->post('est');
		$grs=$this->input->post('grs');
		$kgs=$this->input->post('kgs');
		$pre=$this->input->post('pre');
		$cic=$this->input->post('cic');
		if($fec!=''){	
			$this->cosechasmzt_model->agregacos($fec,$tip,$num,$cli,$est,$grs,$kgs,$pre,$cic);			
			redirect('cosechasmzt');
		}
		}
		function actualizaCos($id=0){
		$this->load->helper('url');
		$this->load->model('cosechasmzt_model');
		$id=$this->input->post('id');
		$fec=$this->input->post('fec'); 
		$tip=$this->input->post('tip');
		$num=$this->input->post('num');
		$cli=$this->input->post('cli');
		$est=$this->input->post('est');
		$grs=$this->input->post('grs');
		$kgs=$this->input->post('kgs');
		$pre=$this->input->post('pre');
		$cic=$this->input->post('cic');
		if($id!=''){
			$return=$this->cosechasmzt_model->actualizacos($id,$fec,$tip,$num,$cli,$est,$grs,$kgs,$pre,$cic); 			
			redirect('cosechasmzt');
		}		
		}
		
		function quitarCos($id=0){
		$this->load->helper('url');
		$this->load->model('cosechasmzt_model');
		$id_post=$this->input->post('id'); 
		$cic=$this->input->post('cic');
		if($id_post!=''){
			$return=$this->cosechasmzt_model->borrarcos($id_post,$cic); 			
			redirect('cosechasmzt');
		}
		}
		public function combob($cic=0){
			$cic=$this->input->post('cicloG');
			$filter['secc']=$this->input->post('secc');
			//if($secc==4) $filter['where']['secc >=']='$secc';
			$data = $this->cosechasmzt_model->getElementsb($filter,$cic); 
			echo '('.json_encode($data).')';              
    	}
		
    }
    
?>