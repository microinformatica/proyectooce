<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Kpiadm extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('kpiadm_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('kpiadm_model');
			/*$fi=date("Y-m-d");$ff= date("Y-m-d");
			$data['txtFI']=date("Y-m-d");*/
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('kpiadm/lista',$data);
        }			
		
		function pdfrepent() {
            $this->load->model('kpiadm_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			
			$data['tablac'] = $this->input->post('tablaent');
			
			$html = $this->load->view('kpiadm/listapdfent', $data, true);
			//$cia='aqp';
			//pdf ($html,'saldos/edocta/'.$cia, true);
			pdf ($html,'kpiadm/listapdfent', true);
        	set_paper('letter');
        }
		function pdfrepentdet() {
            $this->load->model('kpiadm_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			
			$data['tablac'] = $this->input->post('tablaentdet');
			$data['zon'] = $this->input->post('zona1');
			$data['cli'] = $this->input->post('clie1');
			
			$html = $this->load->view('kpiadm/listapdfentdet', $data, true);
			//$cia='aqp';
			//pdf ($html,'saldos/edocta/'.$cia, true);
			pdf ($html,'kpiadm/listapdfentdet', true);
        	set_paper('letter');
        }
		
		
		public function tablatec($cic='',$extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['month(fechar) =']=$extra;
			//$filter['where']['month(fechar) <=']=$extra;
			$cic='r'.$cic;
        	$data['rows'] = $this->kpiadm_model->gettec($filter,$cic,$extra);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablacom($cic='',$extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['month(fechar) =']=$extra;
			//$filter['where']['month(fechar) <=']=$extra;
			$cic='r'.$cic;
        	$data['rows'] = $this->kpiadm_model->getcom($filter,$cic,$extra);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablakpi($mes='',$depto=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['month(fech) =']=$mes;
			//$filter['where']['month(fechar) <=']=$extra;
			$filter['where']['ndep =']=$depto;
        	$data['rows'] = $this->kpiadm_model->kpiadm($filter);
        	echo '('.json_encode($data).')';                
    	}
		
    }
    
?>