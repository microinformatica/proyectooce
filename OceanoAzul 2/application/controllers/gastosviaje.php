<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Gastosviaje extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('gastosviaje_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('gastosviaje_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('gastosviaje/lista',$data);
        }
		function pdfrep() {
            $this->load->model('gastosviaje_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabla');
			$data['ciclo'] =$this->input->post('ciclo');
			$html = $this->load->view('gastosviaje/listapdf', $data, true);  
			pdf ($html,'gastosviaje/listapdf', true);
        }
		function pdfrep1() {
            $this->load->model('gastosviaje_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabla');
			$data['ciclo'] =$this->input->post('cicloR');
			$html = $this->load->view('gastosviaje/listapdf', $data, true);  
			pdf ($html,'gastosviaje/listapdf', true);
        }
		public function tabla($cic=0){        
        	$filter = $this->ajaxsorter->filter($this->input);
        	$cic1=($cic-2000);
        	$cic='r'.$cic1;
        	if($cic1==14)$sol='solicitud'; else $sol='solicitud_'.$cic1;          	
        	$data['rows'] = $this->gastosviaje_model->getgastosviaje($filter,$cic,$sol);
        	echo '('.json_encode($data).')';                
    	}
		public function tablares($cic=0){        
        	$filter = $this->ajaxsorter->filter($this->input);
        	$cic1=($cic-2000);
        	$cic='r'.$cic1;
        	if($cic1==14)$sol='solicitud'; else $sol='solicitud_'.$cic1;          	
        	$data['rows'] = $this->gastosviaje_model->getgastosviajeRes($filter,$cic,$sol);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaZona(){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$data['rows'] = $this->gastosviaje_model->getZonasvta($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablacd($zon=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($zon=='El%20Dorado'){$zon='El Dorado';}
			if($zon=='Sur%20Sinaloa'){$zon='Sur Sinaloa';}
			$filter['where']['zona =']=$zon;  
        	$data['rows'] = $this->gastosviaje_model->getZonasvtacd($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablacd1($zon=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($zon=='El%20Dorado'){$zon='El Dorado';}
			if($zon=='Sur%20Sinaloa'){$zon='Sur Sinaloa';}
			$filter['where']['zona =']=$zon;  
        	$data['rows'] = $this->gastosviaje_model->getZonasvtacd1($filter);
        	echo '('.json_encode($data).')';                
    	}
    }
    
?>