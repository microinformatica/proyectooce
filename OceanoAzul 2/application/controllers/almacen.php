<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Almacen extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
       	$this->load->model('almacen_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('almacen_model');
			//$data['result']=$this->produccion_model->Entregado();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('almacen/lista',$data);
        }
		function pdfrepd( ) {
            $this->load->model('almacen_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('almacen/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('almacen/listapdfdia', $data, true);  
			pdf ($html,'almacen/listapdfdia', true);
        	set_paper('letter');
        }
		function tablad($desde='',$hasta=''){
			$this->load->model('almacen_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			if($desde!='') $filter['where']['fechas >=']=$desde;
			if($hasta!='') $filter['where']['fechas <=']=$hasta;		
			$data['rows'] = $this->almacen_model->getalmacendia($filter);
        	$data['num_rows'] = $this->almacen_model->getNumRowsdia($filter);		
			echo '('.json_encode($data).')'; 
    	}			
		function actualizard($id=0){
			$this->load->model('almacen_model');
			$id_post=$this->input->post('id'); 
			$can=$this->input->post('can');
			$uni=$this->input->post('uni');
			$des=$this->input->post('des');
			$obs=$this->input->post('obs');
			$cans=$this->input->post('cans');
			if($id_post!=''){
				$return=$this->almacen_model->actualizard($id_post,$can,$uni,$des,$obs,$cans); 			
				redirect('almacen');
			}
		}
		function borrard($id=0){
			$this->load->helper('url');
			$this->load->model('almacen_model');
			$id_post=$this->input->post('id');
			if($id_post!=''){
				$return=$this->almacen_model->borrard($id_post); 			
				redirect('almacen');
			}
		}
		
		function pdfrepr( ) {
            $this->load->model('almacen_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('almacen/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$req = $this->input->post('requisicion');
			$data['req'] = $this->almacen_model->verNomReq($req);
			$html = $this->load->view('almacen/listapdfreq', $data, true);  
			pdf ($html,'Requisición:'.$data['req'], true);
        	set_paper('letter');
        }
		function tablar($req=''){        
        	$filter = $this->ajaxsorter->filter($this->input);		
			$filter['where']['nrs']=$req;	
			$data['rows'] = $this->almacen_model->getalmacenreq($filter);
        	$data['num_rows'] = $this->almacen_model->getNumRowsreq($filter);		
			echo '('.json_encode($data).')'; 
    	}
		function agregarr(){
			$this->load->model('almacen_model');		
			$can=$this->input->post('can');
			$uni=$this->input->post('uni');
			$des=$this->input->post('des');
			$obs=$this->input->post('obs');
			$req=$this->input->post('req');
			if($can!=''){	
				$this->almacen_model->agregarr($can,$uni,$des,$obs,$req);			
				redirect('almacen');
			}
		} 
		function actualizarr($id=0){
			$this->load->model('almacen_model');
			$id_post=$this->input->post('id'); 
			$can=$this->input->post('can');
			$uni=$this->input->post('uni');
			$des=$this->input->post('des');
			$obs=$this->input->post('obs');
			$cans=$this->input->post('cans');
			if($id_post!=''){
				$return=$this->almacen_model->actualizarr($id_post,$can,$uni,$des,$obs,$cans); 			
				redirect('almacen');
			}
		}
		function borrarr($id=0){
			$this->load->helper('url');
			$this->load->model('almacen_model');
			$id_post=$this->input->post('id');
			if($id_post!=''){
				$return=$this->almacen_model->borrarr($id_post); 			
				redirect('almacen');
			}
		}
		function agregarreq(){
			$this->load->helper('url');
			$this->load->model('almacen_model');
			$return=$this->almacen_model->nueRequisicion(); 			
			redirect('almacen');
		}
    }
    
?>