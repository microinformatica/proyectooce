<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Tablero extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('tablero_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('tablero_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('tablero/lista',$data);
        }
		
		public function tablaequipos(){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			if($usuario=='Joaquin Domínguez') $usu=1; 
			if($usuario=='Miguel Mendoza') $usu=2;
			if($usuario!='Joaquin Domínguez' && $usuario!='Miguel Mendoza') $usu=3;
			$data['rows'] = $this->tablero_model->getEquipos($filter, $usu);
        	$data['num_rows'] = $this->tablero_model->getNumRowsE($filter, $usu);
        	echo '('.json_encode($data).')'; 
    	}
		public function obtenerdatos($extra=0){
			//$this->load->view('tablero/lista',$data);	
            $filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			if($usuario=='Joaquin Domínguez') $usu=1; 
			if($usuario=='Miguel Mendoza') $usu=2;
			if($usuario!='Joaquin Domínguez' && $usuario!='Miguel Mendoza') $usu=3;
			//$eqpnum = $this->input->post('id');
			$filter['where']['ubi =']=$extra;
			$data['rows'] = $this->tablero_model->getEquiposSele($filter, $usu,$extra);
        	//$data['num_rows'] = $this->tablero_model->getNumRowsSele($filter, $usu);        	
        	echo '('.json_encode($data).')';
        }
		function pdfrepdetmes( ) {
            $this->load->model('tablero_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('tablero/lista',$data);
			$data['cic'] = '['.$this->input->post('cicd').']';
			$mes = $this->input->post('mesd');
			switch ($mes){
					case '1': $mes='[Enero]';break;
					case '2': $mes='[Febrero]';break;
					case '3': $mes='[Marzo]';break;
					case '4': $mes='[Abril]';break;
					case '5': $mes='[Mayo]';break;
					case '6': $mes='[Junio]';break;
					case '7': $mes='[Julio]';break;
					case '8': $mes='[Agosto]';break;
					case '9': $mes='[Septiembre]';break;
					case '10': $mes='[Octubre]';break;
					case '11': $mes='[Noviembre]';break;
					case '12': $mes='[Diciembre]';break;
				}
			$data['mes'] = $mes;
			$tip = $this->input->post('tipd');
			switch ($tip){
					case '0': $tip='[Todos] ';break;
					case '1': $tip='[Bomba] ';break;
					case '2': $tip='[Filtro] ';break;
					case '3': $tip='[Ozono] ';break;
					case '4': $tip='[U.V.] ';break;
					case '5': $tip='[Intercambiador] ';break;
					case '6': $tip='[Blower] ';break;
					case '7': $tip='[Calentador] ';break;
					case '8': $tip='[Chiller] ';break;
					case '9': $tip='[Agua] ';break;
				}	
			$data['tip'] =$tip;
			
			$data['tabla'] = $this->input->post('tabla');
			$html = $this->load->view('tablero/listapdfimp', $data, true);  
			pdf ($html,'tablero/EquipoMes', true);
        }
		function pdfrepeqpdet( ) {
            $this->load->model('tablero_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('tablero/lista',$data);
			$data['ubica'] = $this->input->post('eqpubi');
			$data['tabla'] = $this->input->post('tabla');
			$html = $this->load->view('tablero/listapdfeqpimp', $data, true);  
			pdf ($html,'tablero/EquipoUbicacion', true);
        }
		function agregare(){
		$this->load->helper('url');
		$this->load->model('tablero_model');		
		$tip=$this->input->post('tip');
		$mod=$this->input->post('mod');
		$dep=$this->input->post('dep');
		$ubi=$this->input->post('ubi');
		$fec=$this->input->post('fec');
		$feca=$this->input->post('feca');
		$fac=$this->input->post('fac');
		$pre=$this->input->post('pre');
		$pro=$this->input->post('pro');
		if($mod!=''){	
			$this->tablero_model->agregare($tip,$mod,$dep,$ubi,$fec,$feca,$fac,$pre,$pro);			
			redirect('tablero');
		}
		}
		
		function actualizare($id=0){
		$this->load->helper('url');
		$this->load->model('tablero_model');
		$id_post=$this->input->post('id'); 
		$tip=$this->input->post('tip');
		$mod=$this->input->post('mod');
		$dep=$this->input->post('dep');
		$ubi=$this->input->post('ubi');
		$fec=$this->input->post('fec');
		$feca=$this->input->post('feca');
		$fac=$this->input->post('fac');
		$pre=$this->input->post('pre');
		$pro=$this->input->post('pro');
		if($id_post!=''){
			$return=$this->tablero_model->actualizare($id_post,$tip,$mod,$dep,$ubi,$fec,$feca,$fac,$pre,$pro); 			
			redirect('tablero');
		}
		}
		
		function borrare($id=0){
		$this->load->helper('url');
		$this->load->model('tablero_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->tablero_model->borrare($id_post); 			
			redirect('tablero');
		}
		}
		public function combo($extra=0){
			$this->load->model('tablero_model');
			//$act=$this->input->post('actual');
			$usuario=$this->usuario;
			if($usuario=='Joaquin Domínguez') $usu=1; 
			if($usuario=='Miguel Mendoza') $usu=2;
			if($usuario!='Joaquin Domínguez' && $usuario!='Miguel Mendoza') $usu=3;
			$filter['cat']=$this->input->post('actual');           
			$data = $this->tablero_model->getCategorias($filter,$usu);        
        	echo '('.json_encode($data).')'; 
			
    	}
		function actualizaDet($id=0){
		$this->load->helper('url');
		$this->load->model('tablero_model');
		$id_post=$this->input->post('id');
		$mod=$this->input->post('mod'); 
		$fec=$this->input->post('fec');
		if($id_post!=''){
			$return=$this->tablero_model->actualizaDet($id_post,$mod,$fec); 			
			redirect('tablero');
		}
		}
		function agregaDet(){
		$this->load->helper('url');
		$this->load->model('tablero_model');		
		$mod=$this->input->post('mod');
		$ubi=$this->input->post('ubi');
		$feca=$this->input->post('fec');
		$tip=$this->input->post('tip');
		$usuario=$this->usuario;
		if($usuario=='Joaquin Domínguez') $dep=1; 
		if($usuario=='Miguel Mendoza') $dep=2;
		if($mod!=''){	
			$this->tablero_model->agregaDet($mod,$dep,$ubi,$feca,$tip);			
			redirect('tablero');
		}
		}
		function quitarDet($id=0){
		$this->load->helper('url');
		$this->load->model('tablero_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->tablero_model->quitarDet($id_post); 			
			redirect('tablero');
		}
		}
		function tablatecmes($ciclo=0,$mes=0,$tip=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($ciclo>0) $filter['where']['year(feca) =']=$ciclo;
			if($mes>0) $filter['where']['month(feca) =']=$mes;
			if($tip>0) $filter['where']['tipo =']=$tip;
			$data['rows'] = $this->tablero_model->getTecnicosGral($filter,$tip);
        	$data['num_rows'] = $this->tablero_model->getNumRowsTGral($filter);
        	echo '('.json_encode($data).')'; 
    	}
    }
    
?>