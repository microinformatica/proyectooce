<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Almacendia extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('almacendia_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('almacendia_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;		
			$this->load->view('almacendia/lista',$data);
        }
		function pdfrep( ) {
            $this->load->model('almacendia_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('almacendia/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('almacendia/listapdf', $data, true);  
			pdf ($html,'almacendia/listapdf', true);
        	set_paper('letter');
        }
		function tabla($cer=''){
			$per=$this->session->userdata('id_usuario');        
        	$filter = $this->ajaxsorter->filter($this->input);			
			$data['rows'] = $this->almacendia_model->getalmacendia($filter,$per);
        	$data['num_rows'] = $this->almacendia_model->getNumRowsdia($filter,$per);		
			echo '('.json_encode($data).')'; 
    	}
		function actualizar($id=0){
			$this->load->model('almacendia_model');
			$id_post=$this->input->post('id'); 
			$can=$this->input->post('can');
			$uni=$this->input->post('uni');
			$des=$this->input->post('des');
			$obs=$this->input->post('obs');
			$cans=$this->input->post('cans');
			$cali=$this->input->post('cali');
			if($id_post!=''){
				$return=$this->almacendia_model->actualizar($id_post,$can,$uni,$des,$obs,$cans,$cali); 			
				redirect('almacendia');
			}
		}
		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('almacendia_model');
			$id_post=$this->input->post('id');
			if($id_post!=''){
				$return=$this->almacendia_model->borrar($id_post); 			
				redirect('almacendia');
			}
		}
		
	}
?>