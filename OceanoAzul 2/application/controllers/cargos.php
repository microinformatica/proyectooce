<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Cargos extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('cargos_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('cargos_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('cargos/lista',$data);
        }
		function pdfrep( ) {
            $this->load->model('cargos_model');
			//$data['result']=$this->cargos_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('cargos/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$data['ini'] = $this->input->post('ini');
			$data['fin'] = $this->input->post('fin');
			//$data['tablac'] = $this->input->post('mytablaCD');
			$html = $this->load->view('cargos/listapdf', $data, true);  
			pdf ($html,'cargos/listapdf', true);
        	set_paper('letter');
        }
		public function tabla($ciclo='',$mostrar=0,$desde='',$hasta=''){ //,$tecnico='',$destino='',$unidad=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($desde!='') $filter['where']['fechas >=']=$desde;
			if($hasta!='') $filter['where']['fechas <=']=$hasta; 
			if($mostrar==1) $filter['where']['numrems =']=0;
			/*if($tecnico!='') $filter['where']['numbios =']=$tecnico;
			if($destino!='') $filter['where']['nomdess =']=$destino;
			if($unidad!='') $filter['where']['numunis =']=$unidad;*/
			//$filter['num'] = $extra; 
        	$data['rows'] = $this->cargos_model->getCargos($filter,$ciclo);
        	$data['num_rows'] = $this->cargos_model->getNumRows($filter,$ciclo);
        	echo '('.json_encode($data).')'; 
    	}
		public function tablacargos($destino='',$unidad=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($destino!='Todos'){
        		if($destino=='El%20Dorado') $destino='El Dorado';
				if($destino=='El%20Carrizo') $destino='El Carrizo';
				if($destino=='La%20Cruz') $destino='La Cruz';
				if($destino=='Los%20Mochis') $destino='Los Mochis';
				if($destino=='Cd.%20Obregon') $destino='Cd. Obregon';
				if($destino=='San%20Blas') $destino='San Blas';
				if($destino=='Reynosa%20Tam.') $destino='Reynosa Tam.';
				if($destino=='Nayarit%20Pimientillo') $destino='Nayarit Pimientillo';
				//if($zona=='Yucat%C3%A1n') $destino='Yucatán';
        		$filter['where']['nomdes']=$destino;
			} 
			//if($destino!='') $filter['where']['nomdes =']=$destino;
			if($unidad!='') $filter['where']['numunid =']=$unidad; 
			$data['rows'] = $this->cargos_model->getCargosVia($filter);
        	$data['num_rows'] = $this->cargos_model->getNumRowsVia($filter);
        	echo '('.json_encode($data).')'; 
    	}
		public function combo(){        
        	$filter['NomDes']=$this->input->post('NomDes');           
        	$data = $this->cargos_model->getElements($filter);        
        	echo '('.json_encode($data).')'; 
    	}
		public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	} 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('cargos_model');		
		$fec=$this->input->post('fec');
		$rem=$this->input->post('rem');
		$enc=$this->input->post('enc');
		$des=$this->input->post('des');
		$uni=$this->input->post('uni');
		$ali=$this->input->post('ali');
		$com=$this->input->post('com');
		$cas=$this->input->post('cas');
		$hos=$this->input->post('hos');
		$fit=$this->input->post('fit');
		$tot=$this->input->post('tot');
		$rep=$this->input->post('rep');
		$cg=$this->input->post('cg');		
		$corte=$this->input->post('corte');
		if($rem!=''){	
			$this->cargos_model->agregar($fec,$rem,$enc,$des,$uni,$ali,$com,$cas,$hos,$fit,$tot,$rep,$cg,$corte);			
			redirect('cargos');
		}
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('cargos/agregar',$datos);
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('cargos_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$rem=$this->input->post('rem');
		$remi['result'] = $this->cargos_model->remision($rem);
		$cliente=0;
		foreach ($remi['result'] as $row): 
			$cliente=$row->NumCliR;										
		endforeach;
		$cli=$cliente;
		$enc=$this->input->post('enc');
		$des=$this->input->post('des');
		$uni=$this->input->post('uni');
		$ali=$this->input->post('ali');
		$com=$this->input->post('com');
		$cas=$this->input->post('cas');
		$hos=$this->input->post('hos');
		$fit=$this->input->post('fit');
		$tot=$this->input->post('tot');
		$rep=$this->input->post('rep');
		$cg=$this->input->post('cg');
		$corte=$this->input->post('corte');
		if($id_post!=''){
			$return=$this->cargos_model->actualizar($id_post,$fec,$rem,$enc,$des,$uni,$ali,$com,$cas,$hos,$fit,$tot,$rep,$cg,$cli,$corte); 			
			redirect('cargos');
		}
		/*if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->cargos_model->getCliente($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('cargos/actualizar',$datos);*/
		}
		
		function borrar($id=0){
		$this->load->helper('url');
		$this->load->model('cargos_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->cargos_model->borrar($id_post); 			
			redirect('cargos');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->cargos_model->getCargos($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('cargos/borrar',$datos);
		}
				
    }
    
?>