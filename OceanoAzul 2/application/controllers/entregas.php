<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Entregas extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('entregas_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('entregas_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('entregas/lista',$data);
        }
		function pdfrep( ) {
            $this->load->model('entregas_model');
			//$data['result']=$this->cargos_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('entregas/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$data['ini'] = $this->input->post('ini');
			$data['fin'] = $this->input->post('fin');
			//$data['tablac'] = $this->input->post('mytablaCD');
			$html = $this->load->view('entregas/listapdf', $data, true);  
			pdf ($html,'entregas/listapdf', true);
        	set_paper('letter');
        }
		function pdfrepeva( ) {
            $this->load->model('logistica_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('logistica/lista',$data);
			$data['res'] = $this->input->post('inomrespon');
			$data['ent'] = $this->input->post('ient');
			$data['ser1'] = $this->input->post('iser1');
			$data['serc'] = $this->input->post('iserc');
			$data['act1'] = $this->input->post('iact1');
			$data['acta'] = $this->input->post('iacta');
			$data['pre1'] = $this->input->post('ipre1');
			$data['prep'] = $this->input->post('iprep');
			$data['efi1'] = $this->input->post('iefi1');
			$data['efie'] = $this->input->post('iefie');
			$data['adm1'] = $this->input->post('iadm1');
			$data['adma'] = $this->input->post('iadma');
			$data['gra1'] = $this->input->post('igra1');
			$data['grag'] = $this->input->post('igrag');
			$data['tablac'] = $this->input->post('tablaevatec');
			
			$html = $this->load->view('entregas/listapdfevatec', $data, true); 
			pdf ($html,'entregas/evatec', true);	
        }
		function pdfreptcontec() {
            $this->load->model('logistica_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('logistica/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('entregas/listapdfcontec', $data, true);  
			pdf ($html,'entregas/listapdfcontec', true);        	
        	//set_paper('letter');
			
        }
		public function tabla($extra=0,$desde='',$hasta=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($desde!='') $filter['where']['fechar >=']=$desde;
			if($hasta!='') $filter['where']['fechar <=']=$hasta; 
			$filter['num'] = $extra;			
        	$data['rows'] = $this->entregas_model->getEntregas($filter);
        	$data['num_rows'] = $this->entregas_model->getNumRowsE($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablatecres(){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->entregas_model->getEntregastecres($filter);
        	//$data['num_rows'] = $this->logistica_model->getNumRowsEtecres($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablatecresdet($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->entregas_model->getEntregastecresdet($extra);
        	//$data['num_rows'] = $this->logistica_model->getNumRowsEtecres($filter);
        	echo '('.json_encode($data).')'; 
    	}
		public function combo(){        
        	$filter['NomDes']=$this->input->post('NomDes');           
        	$data = $this->cargos_model->getElements($filter);        
        	echo '('.json_encode($data).')'; 
    	}
		 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('entregas_model');		
		$nbs=$this->input->post('enc');
		$fol=$this->input->post('fol');
		$ser=$this->input->post('ser');
		$act=$this->input->post('act');
		$pre=$this->input->post('pre');
		$efi=$this->input->post('efi');		
		$adm=$this->input->post('adm');		
		$obs=$this->input->post('obs');
		$foc=$this->input->post('alerta');	
		$nr=$this->input->post('nr');
		$nc=$this->input->post('nc');		
		if($fol!=''){	
			$this->entregas_model->agregar($nbs,$fol,$act,$pre,$efi,$ser,$adm,$obs,$foc,$nr,$nc);			
			redirect('entregas');
		}
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('entregas',$datos);
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('entregas_model');
		$id_post=$this->input->post('id'); 
		$nbs=$this->input->post('enc');
		$fol=$this->input->post('fol');
		$ser=$this->input->post('ser');
		$act=$this->input->post('act');
		$pre=$this->input->post('pre');
		$efi=$this->input->post('efi');		
		$adm=$this->input->post('adm');		
		$obs=$this->input->post('obs');
		$foc=$this->input->post('alerta');		
		$veri=$this->input->post('veri');
		if($id_post!=''){
			$return=$this->entregas_model->actualizar($id_post,$nbs,$fol,$act,$pre,$efi,$ser,$adm,$obs,$foc,$veri); 			
			redirect('entregas');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->entregas_model->getCliente($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('entregas/actualizar',$datos);
		}
		
		function borrar($id=0){
		$this->load->helper('url');
		$this->load->model('entregas_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->entregas_model->borrar($id_post); 			
			redirect('entregas');
		}
		/*if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->cargos_model->getCargos($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('cargos/borrar',$datos);*/
		}
				
    }
    
?>