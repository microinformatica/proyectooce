<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Limpieza extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('limpieza_model');         
        $this->load->library(array('ajaxsorter','session','enletras','libreria','user_agent'));		
        $this->load->helper(array('url','form','html','pdf'));
		$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		
		if($id_usuario==false)redirect('login'); 
	   }
        
        function index() {
            $this->load->model('limpieza_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('limpieza/lista',$data);
        }
		
		
		public function tablaeva($ciclo='',$mes=0,$cat){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->limpieza_model->getlimpiezaG($filter,$ciclo,$mes,$cat);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablamaq($ciclo='',$fec=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['fecmaq =']=$fec;
			$data['rows'] = $this->limpieza_model->getmaq($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablacar($ciclo='',$fec=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['feccar =']=$fec;
			$data['rows'] = $this->limpieza_model->getcar($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablaveh($ciclo='',$fec=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['fecveh =']=$fec;
			$data['rows'] = $this->limpieza_model->getveh($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		
		function reportedie( ) {
        	$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablacomg');
			$this->load->view('limpieza/reportedie',$data); 
        }
		function pdfmes() {
            $this->load->model('limpieza_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablaeva');
			$data['mes']=$this->input->post('cmbMeskpi');
			$data['cic']=$this->input->post('ciclorep');
			switch($data['mes']){
				case 1 : $mes='Enero'; break;
				case 2 : $mes='Febrero'; break;
				case 3 : $mes='Marzo'; break;
				case 4 : $mes='Abril'; break;
				case 5 : $mes='Mayo'; break;
				case 6 : $mes='Junio'; break;
				case 7 : $mes='Julio'; break;
				case 8 : $mes='Agosto'; break;
				case 9 : $mes='Septiembre'; break;
				case 10 : $mes='Octubre'; break;
				case 11 : $mes='Noviembre'; break;
				case 12 : $mes='Diciembre'; break;
			}
			$data['mes']=$mes;
			$nc=$this->input->post('categoria');
			if($nc==1) $data['nc']='Maquinaria';
			if($nc==2) $data['nc']='Carcamo'; 
			if($nc==3) $data['nc']='Vehiculo';
			$html = $this->load->view('limpieza/kpimes', $data, true);  
			pdf ($html,$data['nc'].'_KPI_'.$mes.'_'.$data['cic'], true);        	
        }
		
		function agregarmcv(){
		$this->load->helper('url');
		$this->load->model('limpieza_model');
		$todos=$this->input->post('todos');
		$fecha=$this->input->post('fec');		
		$cic=$this->input->post('cic');
		$cat=$this->input->post('cat');
		if($todos==1){$mcv=1;$mot=1;$tra=1;$shi=1;$sus=1;$bom=1;}
		else{
			$mcv=$this->input->post('mcv');
			$mot=$this->input->post('mot');
			$tra=$this->input->post('tra');
			$shi=$this->input->post('shi');
			$sus=$this->input->post('sus');
			$cic=$this->input->post('cic');
			$cat=$this->input->post('cat');
			$bom=$this->input->post('bom');	
		}
		
		if($fecha!=''){	
			$this->limpieza_model->agregarmcv($fecha,$mcv,$mot,$tra,$shi,$sus,$cic,$cat,$bom,$todos);			
			redirect('limpieza');
		}
		}
		function actualizarmcv($id=0){
		$this->load->helper('url');
		$this->load->model('limpieza_model');
		$id=$this->input->post('id');
		$mot=$this->input->post('mot');
		$tra=$this->input->post('tra');
		$shi=$this->input->post('shi');
		$sus=$this->input->post('sus');
		$cic=$this->input->post('cic');
		$cat=$this->input->post('cat');
		$bom=$this->input->post('bom');
		if($id!=''){
			$return=$this->limpieza_model->actualizarmcv($id,$mot,$tra,$shi,$sus,$cic,$cat,$bom); 			
			redirect('limpieza');
		}		
		}
		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('limpieza_model');
			$id_post=$this->input->post('id');
			$tab=$this->input->post('tabla'); 
			$cam=$this->input->post('campo'); 
			$ciclo=$this->input->post('cic');
			if($id_post!=''){
				$return=$this->limpieza_model->quitar($id_post,$tab,$cam,$ciclo); 			
				redirect('aqpgranja');
			}
		}
		function buscardia(){
			$data='';
			$fec = $this->input->post('fec');
			$cic = $this->input->post('cic');
			$cat = $this->input->post('cat');
			$data =$this->limpieza_model->history($fec,$cic,$cat);
			//$size=sizeof($data);
			if($data!=''){
				//si lo encuentra deja los datos
				echo json_encode(array('dia'=>$data->dia));
			}else{
				echo json_encode(array('dia'=>''));
			}
						
		}	
   }
    
?>