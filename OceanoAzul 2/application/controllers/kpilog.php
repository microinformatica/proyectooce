<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Kpilog extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('kpilog_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('kpilog_model');
			$fi=date("Y-m-d");$ff= date("Y-m-d");
			$data['txtFI']=date("Y-m-d");
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('kpilog/lista',$data);
        }			
		
		function pdfrepent() {
            $this->load->model('kpilog_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			
			$data['tablac'] = $this->input->post('tablaent');
			
			$html = $this->load->view('kpilog/listapdfent', $data, true);
			//$cia='aqp';
			//pdf ($html,'saldos/edocta/'.$cia, true);
			pdf ($html,'kpilog/listapdfent', true);
        	set_paper('letter');
        }
		function pdfrepentdet() {
            $this->load->model('kpilog_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			
			$data['tablac'] = $this->input->post('tablaentdet');
			$data['zon'] = $this->input->post('zona1');
			$data['cli'] = $this->input->post('clie1');
			
			$html = $this->load->view('kpilog/listapdfentdet', $data, true);
			//$cia='aqp';
			//pdf ($html,'saldos/edocta/'.$cia, true);
			pdf ($html,'kpilog/listapdfentdet', true);
        	set_paper('letter');
        }
		public function tablacvimp($extra=0,$desde='',$hasta=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	if($desde!='') $filter['where']['fechar >=']=$desde;
			if($hasta!='') $filter['where']['fechar <=']=$hasta;
			$cic='r'.$extra;
        	$data['rows'] = $this->kpilog_model->getcvimp($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaent($cic='',$desde=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($desde!='') $filter['where']['fechar >=']=$desde;
			$nuevafecha = strtotime ( '+6 day' , strtotime ( $desde ) ) ;
			$hasta = date ( 'Y-m-j' , $nuevafecha );
			//if($hasta!='') $filter['where']['fechar <=']=$hasta;
			$cic='r'.$cic;
        	$data['rows'] = $this->kpilog_model->getent($filter,$cic,$desde);
        	echo '('.json_encode($data).')';                
    	}
		public function tabladeta($cic='',$desde='',$cli){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($desde!='') $filter['where']['fechar >=']=$desde;
			$nuevafecha = strtotime ( '+6 day' , strtotime ( $desde ) ) ;
			$hasta = date ( 'Y-m-j' , $nuevafecha );
			//if($hasta!='') $filter['where']['fechar <=']=$hasta;
			$cic='r'.$cic;
        	$data['rows'] = $this->kpilog_model->getdeta($filter,$cic,$desde,$cli);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablatectra($cic='',$extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['month(fechar) =']=$extra;
			//$filter['where']['month(fechar) <=']=$extra;
			$cic='r'.$cic;
        	$data['rows'] = $this->kpilog_model->gettectra($filter,$cic,$extra);
        	echo '('.json_encode($data).')';                
    	}
		public function tablakpi($mes='',$depto=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['month(fech) =']=$mes;
			//$filter['where']['month(fechar) <=']=$extra;
			$filter['where']['ndep =']=$depto;
        	$data['rows'] = $this->kpilog_model->kpilog($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablacos($cic='',$extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['month(fechar) =']=$extra;
			//$filter['where']['month(fechar) <=']=$extra;
			$cic='r'.$cic;
        	$data['rows'] = $this->kpilog_model->getcos($filter,$cic,$extra);
        	echo '('.json_encode($data).')';                
    	}	
		public function tablarescho($mes=0,$cho=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($mes>0) $filter['where']['month(fechar) =']=$mes;
			if($cho!=1)	$filter['where']['NumCho']=$cho;
			$data['rows'] = $this->kpilog_model->getEscanercho($filter);
        	echo '('.json_encode($data).')';                
    	}
    }
    
?>