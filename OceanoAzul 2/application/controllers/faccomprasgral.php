<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Faccomprasgral extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('faccomprasgral_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('faccomprasgral_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('faccomprasgral/lista',$data);
        }
		function pdfrep() {
            $this->load->model('faccomprasgral_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabla');
			$cat=$this->input->post('catego');
			if($cat==0) $cat="Todos";
			if($cat==1) $cat="Larvicultura";
			if($cat==2) $cat="Maduración";
			if($cat==3) $cat="Insumos Varios";
			$data['categoria'] = $cat;
			$html = $this->load->view('faccomprasgral/listapdf', $data, true);  
			pdf ($html,'faccomprasgral/listapdf', true);
        }
		public function tabla($ciclo='',$tip=0,$mu=0,$buscar=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	if($tip!=0){ $filter['where']['categoria']=$tip; }
			if($mu!=0){ $filter['where']['dp']=$mu; }
        	$data['rows'] = $this->faccomprasgral_model->getfaccomprasgral($filter,$buscar,$ciclo);
        	$data['num_rows'] = $this->faccomprasgral_model->getNumRows($filter,$buscar,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		
		function agregarR(){
			$con=$this->input->post('con');
			$tip=$this->input->post('tip');
			$ana=$this->input->post('ana');
			if($con!=''){	
				$this->faccomprasgral_model->agregarR($con,$tip,$ana);			
				redirect('faccomprasgral');
			}
		} 
		function actualizarR($id=0){
			$this->load->helper('url');
			$this->load->model('faccomprasgral_model');
			$id_post=$this->input->post('id'); 
			$con=$this->input->post('con');
			$tip=$this->input->post('tip');
			$ana=$this->input->post('ana');
			if($id_post!=''){
				$return=$this->faccomprasgral_model->actualizarR($id_post,$con,$tip,$ana); 			
				redirect('faccomprasgral');
			}
		}
    }
    
?>