<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Almacenes extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('almacenes_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('almacenes_model');
			//$data['result']=$this->almacenes_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('almacenes/lista',$data);
        }
		function pdfrep() {
            $this->load->model('almacenes_model');
			//$data['result']=$this->almacenes_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('almacenes/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('almacenes/listapdf', $data, true);  
			pdf ($html,'almacenes/listapdf', true);
        	set_paper('letter');
        }
		public function tabla($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($extra==0){$filter['num'] = $extra;}
			if($extra==1){$filter['where']['dom =']='';} 
			if($extra==2){$filter['where']['loc =']='';}
			if($extra==3){$filter['where']['edo =']='-';}
			if($extra==4){$filter['where']['rfc =']='';}
			if($extra==5){$filter['where']['cp =']='';}
			if($extra==6){$filter['where']['zona =']='-';}
			$data['rows'] = $this->almacenes_model->getUsuarios($filter);
        	$data['num_rows'] = $this->almacenes_model->getNumRows($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function combo(){        
        	$filter['id_unidad']=$this->input->post('id_unidad');           
        	$data = $this->base_model->getElements($filter);        
        	echo '('.json_encode($data).')';              
    	}
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	} 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('almacenes_model');
		$nombre=$this->input->post('nombre');
		$dom=$this->input->post('dom');
		$loc=$this->input->post('loc');
		$edo=$this->input->post('edo');
		$rfc=$this->input->post('rfc');
		$cp=$this->input->post('cp');
		$con=$this->input->post('con');
		$cor=$this->input->post('cor');
		$tel=$this->input->post('tel');
		$zona=$this->input->post('zona');
		$avi=$this->input->post('avi');
		$fec=$this->input->post('fec');
		$tip=$this->input->post('tip');
		$mar=$this->input->post('mar');
		$mod=$this->input->post('mod');
		$col=$this->input->post('col');
		$pla=$this->input->post('pla');
		$cho=$this->input->post('cho');
		$gui=$this->input->post('gui');
		$fle=$this->input->post('fle');
		$pes=$this->input->post('pes');
		$pue=$this->input->post('pue');
		if($nombre!=''){	
			$this->almacenes_model->agregar($nombre,$dom,$loc,$edo,$rfc,$cp,$zona,$cor,$con,$tel,$avi,$fec,$tip,$mar,$mod,$col,$pla,$cho,$gui,$fle,$pes,$pue);
			redirect('almacenes');
		}
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('almacenes/agregar',$datos);
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('almacenes_model');
		$id_post=$this->input->post('id'); //id
		$nombre=$this->input->post('nombre');
		$dom=$this->input->post('dom');
		$loc=$this->input->post('loc');
		$edo=$this->input->post('edo');
		$rfc=$this->input->post('rfc');
		$cp=$this->input->post('cp');
		$con=$this->input->post('con');
		$cor=$this->input->post('cor');
		$tel=$this->input->post('tel');
		$zona=$this->input->post('zona');
		$avi=$this->input->post('avi');
		$fec=$this->input->post('fec');
		$tip=$this->input->post('tip');
		$mar=$this->input->post('mar');
		$mod=$this->input->post('mod');
		$col=$this->input->post('col');
		$pla=$this->input->post('pla');
		$cho=$this->input->post('cho');
		$gui=$this->input->post('gui');
		$fle=$this->input->post('fle');
		$pes=$this->input->post('pes');
		$pue=$this->input->post('pue');
		if($id_post!=''){
			$return=$this->almacenes_model->actualizar($id_post,$nombre,$dom,$loc,$edo,$rfc,$cp,$zona,$cor,$con,$tel,$avi,$fec,$tip,$mar,$mod,$col,$pla,$cho,$gui,$fle,$pes,$pue); 
			redirect('almacenes');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->almacenes_model->getCliente($id);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('almacenes/actualizar',$datos);
	}
		
		function borrara($id=0){
		$this->load->helper('url');
		$this->load->model('almacenes_model');
		$id_post=$this->input->post('id');
		if($id_post!=''){
			$return=$this->almacenes_model->borrara($id_post); 			
			redirect('programa');
		}
		}
		
    }
    
?>