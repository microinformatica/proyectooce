<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Inventarios extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
       	$this->load->model('inventarios_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('inventarios_model');
			//$data['result']=$this->produccion_model->Entregado();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			/*
			$rem['result'] = $this->inventarios_model->ultimofolio();
			$txtuf=0;
			foreach ($rem['result'] as $row): 
				$txtuf=str_pad($row->ultimo+1, 5, "0", STR_PAD_LEFT);	
			endforeach;
			$data['txtuf']=$txtuf;
			*/
			$this->load->view('inventarios/lista',$data);
        }
		
		function agregarcotcli(){
			$this->load->model('inventarios_model');		
			$nom=$this->input->post('nom');
			$dom=$this->input->post('dom');
			$tel=$this->input->post('tel');
			if($nom!=''){	
				$this->inventarios_model->agregarcotcli($nom,$dom,$tel);	
				redirect('inventarios');
			}
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('inventarios/lista',$data);			
		} 
		function actualizarcotcli(){
			$this->load->model('inventarios_model');
			$id=$this->input->post('id');		
			$nom=$this->input->post('nom');
			$dom=$this->input->post('dom');
			$tel=$this->input->post('tel');
			if($nom!=''){	
				$this->inventarios_model->actualizarcotcli($id,$nom,$dom,$tel);	
				redirect('inventarios');
			}
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('inventarios/lista',$data);			
		} 
		function tablacot(){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			/*if($cic!='0') $filter['where']['cicent =']=$cic;	
			if($alm!='0') $filter['where']['alment =']=$alm;
			if($gra!='0') $filter['where']['graent =']=$gra;
			if($tal!='0') $filter['where']['talent =']=$tal;*/
			$data['rows'] = $this->inventarios_model->getcotizaciones($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablainv($cic=0,$alm=0,$gra=0,$tal=0){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($cic!='0') $filter['where']['cicent =']=$cic;	
			if($alm!='0') $filter['where']['alment =']=$alm;
			if($gra!='0') $filter['where']['graent =']=$gra;
			if($tal!='0') $filter['where']['talent =']=$tal;
			$data['rows'] = $this->inventarios_model->getinventarios($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function tablainve($cic=0,$alm=0,$gra=0,$tal=0){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($cic!='0') $filter['where']['cicent =']=$cic;	
			if($alm!='0') $filter['where']['alment =']=$alm;
			if($gra!='0') $filter['where']['graent =']=$gra;
			if($tal!='0') $filter['where']['talent =']=$tal;
			$data['rows'] = $this->inventarios_model->getinventariose($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function pdfrepinv( ) {
            $this->load->model('inventarios_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('inventarios/lista',$data);
			$data['tablasi'] = $this->input->post('tablasi');
			$data['dia'] = date('d-m-Y');
			$html = $this->load->view('inventarios/listapdfinv', $data, true);
			pdf ($html,'Inventario_'.$data['dia'], true);        	
        }
		function pdfrepinvg( ) {
            $this->load->model('inventarios_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('inventarios/lista',$data);
			$data['tablasi'] = $this->input->post('tablasg');
			$data['dia'] = date('d-m-Y');
			$html = $this->load->view('inventarios/listapdfinv', $data, true);
			pdf ($html,'Inventario_'.$data['dia'], true);        	
        }
		function pdfrepact( ) {
            $this->load->model('inventarios_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('inventarios/lista',$data);
			$data['tablasa'] = $this->input->post('tablasa');
			$data['dia'] = date('d-m-Y');
			$html = $this->load->view('inventarios/listapdfact', $data, true);
			pdf ($html,'Exi_Talla_'.$data['dia'], true);        	
        }
		function tablaactual($cic=0){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			$data['rows'] = $this->inventarios_model->getactual($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function tablarc($cli=0,$tal=0){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($cli>0) $filter['where']['clicb =']=$cli;
			if($tal>0) $filter['where']['talcb =']=$tal;
			$data['rows'] = $this->inventarios_model->getrc($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablatodo($cic=0){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			$data['rows'] = $this->inventarios_model->gettodo($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function tablaexa($cic=0){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['cicent =']=$cic;	
			$data['rows'] = $this->inventarios_model->getexitall($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function tablasal($cic=0,$est=1){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['ciclo =']='20'.$cic;
			$filter['where']['estsal =']=$est;	
			/*if($alm!='0') $filter['where']['alment =']=$alm;
			if($gra!='0') $filter['where']['graent =']=$gra;
			if($tal!='0') $filter['where']['talent =']=$tal;*/
			$data['rows'] = $this->inventarios_model->getsalidas($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablasald($cic=0,$uf=0){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['ciclo =']='20'.$cic;
			if($uf!='0') $filter['where']['folsal =']=$uf;	
			$data['rows'] = $this->inventarios_model->getsalidasd($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function agregarsal(){
			$this->load->model('inventarios_model');		
			$fec=$this->input->post('fec');
			$est=$this->input->post('est');
			$alm=$this->input->post('alm');
			$cli=$this->input->post('cli');
			$caj=$this->input->post('caj');
			$pre=$this->input->post('pre');
			$tal=$this->input->post('tal');
			$ent=$this->input->post('ent');
			$fol=$this->input->post('fol');
			$coa=$this->input->post('coa');
			$coc=$this->input->post('coc');
			$gra=$this->input->post('gra');
			$clo=$this->input->post('clo');
			$gpos=$this->input->post('gpos');
			if($fec!=''){	
				$this->inventarios_model->agregarsal($fec,$est,$alm,$cli,$caj,$pre,$tal,$ent,$fol,$coa,$coc,$gra,$clo,$gpos);			
				redirect('inventarios');
			}
		} 
		function actualizarsal($id=0){
			$this->load->model('inventarios_model');
			$id_post=$this->input->post('id'); 
			$caj=$this->input->post('caj');
			$pre=$this->input->post('pre');
			$coa=$this->input->post('coa');
			$coc=$this->input->post('coc');
			$est=$this->input->post('est');
			$idents=$this->input->post('idents');
			$gpos=$this->input->post('gpos');
			if($id_post!=''){
				$return=$this->inventarios_model->actualizarsal($id_post,$caj,$pre,$coa,$coc,$est,$idents,$gpos); 			
				redirect('inventarios');
			}
		}
		public function combob(){
			$filter['est']=$this->input->post('est');
			//if($secc==4) $filter['where']['secc >=']='$secc';
			$datac = $this->inventarios_model->getElementsb($filter); 
			echo '('.json_encode($datac).')';              
    	}
		public function combocb(){
			$cic=$this->input->post('cic');
			$cic=$cic-2000;
			//if($secc==4) $filter['where']['secc >=']='$secc';
			$datac = $this->inventarios_model->getElementscb($cic); 
			echo '('.json_encode($datac).')';              
    	}
		public function comborc(){
			$cic=$this->input->post('cic');
			$cic=$cic-2000;
			//if($secc==4) $filter['where']['secc >=']='$secc';
			$datac = $this->inventarios_model->getElementsrc($cic); 
			echo '('.json_encode($datac).')';              
    	}
		function buscar(){
			//$pil = $this->input->post('pil');
			//busca el actual si lo encuentra deja los datos
			//$data =$this->inventarios_model->ultimofolio();
			$rem['result'] = $this->inventarios_model->ultimofolio();
			$uf=0;
			foreach ($rem['result'] as $row): 
				$uf=$row->ultimo+1;										
			endforeach;
			echo json_encode(array('uf'=>$uf));
			
						
		}
		function buscarc(){
			$cli = $this->input->post('cli');
			$est=$this->input->post('est');
			//busca el actual si lo encuentra deja los datos
			$data =$this->inventarios_model->contacto($cli,$est);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				if($est==1)
				echo json_encode(array('conc'=>$data->con,'nomc'=>$data->Razon));
				else
				echo json_encode(array('conc'=>$data->con,'nomc'=>$data->Razon,'avisoc'=>$data->avisoc));	
			}
						
		}

		function tablaent($cic=0,$alm=0,$gra=0,$tal=0){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($alm!='0') $filter['where']['idap =']=$alm;
			if($gra!='0') $filter['where']['idgp =']=$gra;
			if($tal!='0') $filter['where']['idtp =']=$tal;
			$filter['where']['lotp !=']='';
			$data['rows'] = $this->inventarios_model->getentradas($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function pdfrepsal( ) {
            $this->load->model('inventarios_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('inventarios/lista',$data);
			$data['tablasd'] = $this->input->post('tablasd');
			$data['dia'] = $this->input->post('dia');
			$data['alm'] = $this->input->post('alms');
			$data['cona'] = $this->input->post('conas');
			$data['conc'] = $this->input->post('concs');
			$data['fol'] = $this->input->post('fols');
			$data['numalm'] = $this->input->post('numalm');
			$data['est'] = $this->input->post('esttv');
			$data['almori'] = $this->input->post('almori');
			if($data['est']==1){$html = $this->load->view('inventarios/listapdfdia', $data, true);}
			else{$html = $this->load->view('inventarios/listapdftras', $data, true);}	
			pdf ($html,$data['fol'].'_'.$data['dia'], true);
        	set_paper('letter');
        }
		
		function agregarmaq(){
			$this->load->model('inventarios_model');		
			$fec=$this->input->post('fec');
			$alm=$this->input->post('alm');
			$cic=$this->input->post('cic');
			$gra=$this->input->post('gra');
			if($fec!=''){	
				$this->inventarios_model->agregarmaq($fec,$alm,$cic,$gra);			
				redirect('inventarios');
			}
		} 
		function actualizarmaq($id=0){
			$this->load->model('inventarios_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$alm=$this->input->post('alm');
			$cic=$this->input->post('cic');
			$gra=$this->input->post('gra');
			if($id_post!=''){
				$return=$this->inventarios_model->actualizarmaq($id_post,$fec,$alm,$cic,$gra); 			
				redirect('inventarios');
			}
		}
		function tablamaqf($id=''){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			$filter['where']['idfe =']=$id;
			$data['rows'] = $this->inventarios_model->getinventariosdiaf($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function agregarmaqf(){
			$this->load->model('inventarios_model');		
			$est=$this->input->post('est');
			$grg=$this->input->post('grg');
			$grp=$this->input->post('grp');
			$ntp=$this->input->post('ntp');
			$kgt=$this->input->post('kgt');
			$idf=$this->input->post('idf');
			if($ntp!=''){	
				$this->inventarios_model->agregarmaqf($est,$grg,$grp,$ntp,$kgt,$idf);			
				redirect('inventarios');
			}
		} 
		function actualizarmaqf($id=0){
			$this->load->model('inventarios_model');
			$id_post=$this->input->post('id'); 
			$est=$this->input->post('est');
			$grg=$this->input->post('grg');
			$grp=$this->input->post('grp');
			$ntp=$this->input->post('ntp');
			$kgt=$this->input->post('kgt');
			$idf=$this->input->post('idf');
			if($id_post!=''){
				$return=$this->inventarios_model->actualizarmaqf($id_post,$est,$grg,$grp,$ntp,$kgt,$idf); 			
				redirect('inventarios');
			}
		}
		function tablamaqp($id=''){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			$filter['where']['idpe =']=$id;
			$data['rows'] = $this->inventarios_model->getinventariosdiap($filter,$id);
        	echo '('.json_encode($data).')'; 
    	}			
		function agregarmaqp(){
			$this->load->model('inventarios_model');		
			$lot=$this->input->post('lot');
			$idt=$this->input->post('idt');
			$mas=$this->input->post('mas');
			$kgm=$this->input->post('kgm');
			$idg=$this->input->post('idg');
			$ida=$this->input->post('ida');
			$idp=$this->input->post('idp');
			$pre=$this->input->post('pre');
			$fec=$this->input->post('fec');
			if($mas!=''){	
				$this->inventarios_model->agregarmaqp($lot,$idt,$mas,$kgm,$idg,$ida,$idp,$pre,$fec);			
				redirect('inventarios');
			}
		} 
		function actualizarmaqp($id=0){
			$this->load->model('inventarios_model');
			$id_post=$this->input->post('id'); 
			$lot=$this->input->post('lot');
			$idt=$this->input->post('idt');
			$mas=$this->input->post('mas');
			$kgm=$this->input->post('kgm');
			$idg=$this->input->post('idg');
			$ida=$this->input->post('ida');
			$idp=$this->input->post('idp');
			$pre=$this->input->post('pre');
			$fec=$this->input->post('fec');
			if($id_post!=''){
				$return=$this->inventarios_model->actualizarmaqp($id_post,$lot,$idt,$mas,$kgm,$idg,$ida,$idp,$pre,$fec); 			
				redirect('inventarios');
			}
		}
		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('inventarios_model');
			$id_post=$this->input->post('id');
			$tab=$this->input->post('tabla'); 
			$cam=$this->input->post('campo');
			$est=$this->input->post('est');
			$fol=$this->input->post('fol');
			$tal=$this->input->post('tal');
			$cic=$this->input->post('cic');
			/*$ident=$this->input->post('ident'); 
			$kgs=$this->input->post('kgs');*/
			if($id_post!=''){
				//$return=$this->inventarios_model->quitar($id_post,$tab,$cam,$ident,$kgs); 			
				$return=$this->inventarios_model->quitar($id_post,$tab,$cam,$est,$fol,$tal,$cic);
				redirect('inventarios');
			}
		}
		
		function pdfrepr( ) {
            $this->load->model('inventarios_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('inventarios/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$req = $this->input->post('requisicion');
			$data['req'] = $this->inventarios_model->verNomReq($req);
			$html = $this->load->view('inventarios/listapdfreq', $data, true);  
			pdf ($html,'Requisición:'.$data['req'], true);
        	set_paper('letter');
        }
		function tablar($req=''){        
        	$filter = $this->ajaxsorter->filter($this->input);		
			$filter['where']['nrs']=$req;	
			$data['rows'] = $this->inventarios_model->getinventariosreq($filter);
        	$data['num_rows'] = $this->inventarios_model->getNumRowsreq($filter);		
			echo '('.json_encode($data).')'; 
    	}
		
		function borrarr($id=0){
			$this->load->helper('url');
			$this->load->model('inventarios_model');
			$id_post=$this->input->post('id');
			if($id_post!=''){
				$return=$this->inventarios_model->borrarr($id_post); 			
				redirect('inventarios');
			}
		}
		function agregarreq(){
			$this->load->helper('url');
			$this->load->model('inventarios_model');
			$return=$this->inventarios_model->nueRequisicion(); 			
			redirect('inventarios');
		}
		
		function ultimofolio(){
			$cic = $this->input->post('cic');
			//busca el actual si lo encuentra deja los datos
			$data =$this->inventarios_model->ultimofolio($cic);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				
				echo json_encode(array('folio'=>(str_pad(($data->folio)+1, 5, "0", STR_PAD_LEFT))));
			}else{
				echo json_encode(array('folio'=>''));
			}			
		}
		
		function pdfrepcot( ) {
            $this->load->model('inventarios_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('inventarios/lista',$data);
			$data['tablascot'] = $this->input->post('tablacot');
			$data['fol'] = $this->input->post('ctfol');
			$data['dia'] = $this->input->post('ctdia');
			$data['cli'] = $this->input->post('ctcli');
			$data['dom'] = $this->input->post('ctdom');
			$data['tel'] = $this->input->post('cttel');
			if($data['usuario']=='Zuleima Benitez'){$data['cel']='(669)166-5456'; $data['correo']='clientes@bonatto.com.mx';}
			else {$data['cel']='(669)994-7702'; $data['correo']='efrain@bonatto.com.mx';}
			$html = $this->load->view('inventarios/listapdfcot', $data, true);
			pdf ($html,$data['fol'].'_'.$data['dia'], true);
        	set_paper('letter');
        }
		
		function pdfrepcotrc( ) {
            $this->load->model('inventarios_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('inventarios/lista',$data);
			$data['tablascot'] = $this->input->post('tablarc');
			$data['dia'] = date('Y-m-d');
			if($data['usuario']=='Zuleima Benitez'){$data['cel']='(669)166-5456'; $data['correo']='clientes@bonatto.com.mx';}
			else {$data['cel']='(669)994-7702'; $data['correo']='efrain@bonatto.com.mx';}
			$html = $this->load->view('inventarios/listapdfcotrc', $data, true);
			pdf ($html,'CotGral_'.$data['dia'], true);
        	set_paper('letter');
        }
		
		function ultimofoliocot(){
			//busca el actual si lo encuentra deja los datos
			$data =$this->inventarios_model->ultimofoliocot();
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				
				echo json_encode(array('folio'=>(str_pad(($data->folio)+1, 5, "0", STR_PAD_LEFT))));
			}else{
				echo json_encode(array('folio'=>''));
			}			
		}
		function precios(){
			$tal = $this->input->post('tal');
			$datacb =$this->inventarios_model->precioscot($tal);
			$size=sizeof($datacb);
			if($size>0){
				echo json_encode(array('premen'=>$datacb->premen,'premay'=>$datacb->premay));
			}else{
				echo json_encode(array('premen'=>'','premay'=>''));
			}			
		}
		function datoscli(){
			$cli = $this->input->post('cli');
			$datacb =$this->inventarios_model->datosclicot($cli);
			$size=sizeof($datacb);
			if($size>0){
				echo json_encode(array('nomcb'=>$datacb->nomcb,'domcb'=>$datacb->domcb,'telcb'=>$datacb->telcb));
			}else{
				echo json_encode(array('nomcb'=>'','domcb'=>'','telcb'=>''));
			}			
		}
		function tablacotd($fol=0){
			$this->load->model('inventarios_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['foltalcb =']=$fol;	
			$data['rows'] = $this->inventarios_model->getcotizacionesd($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function agregarcot(){
			$this->load->model('inventarios_model');		
			$fec=$this->input->post('fec');
			$fol=$this->input->post('fol');
			$cli=$this->input->post('cli');
			$tal=$this->input->post('tal');
			$pmen=$this->input->post('pmen');
			$pmay=$this->input->post('pmay');
			$zon=$this->input->post('zon');
			if($fec!=''){	
				$this->inventarios_model->agregarcot($fec,$fol,$cli,$tal,$pmen,$pmay,$zon);			
				redirect('inventarios');
			}
		} 
		function actualizarcot($id=0){
			$this->load->model('inventarios_model');
			$id_post=$this->input->post('id'); 
			$tal=$this->input->post('tal');
			$pmen=$this->input->post('pmen');
			$pmay=$this->input->post('pmay');
			$zon=$this->input->post('zon');
			if($id_post!=''){
				$return=$this->inventarios_model->actualizarcot($id_post,$tal,$pmen,$pmay,$zon); 			
				redirect('inventarios');
			}
		}
		
		function borrartal($id=0){
			$this->load->helper('url');
			$this->load->model('inventarios_model');
			$id_post=$this->input->post('id');
			$tab=$this->input->post('tabla'); 
			$cam=$this->input->post('campo');
			if($id_post!=''){
				$return=$this->inventarios_model->quitartal($id_post,$tab,$cam);
				redirect('inventarios');
			}
		}	
		
    }
    
?>