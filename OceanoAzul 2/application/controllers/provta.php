<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     class Provta extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
       	$this->load->model('provta_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('provta_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('provta/lista',$data);
        }
		
		function reportet() {
            $data['tablac'] = $this->input->post('tabla'); 
			$ngra=$this->input->post('numgrabgz');
			if($ngra==2) $data['ng']='OA-Kino'; 
			if($ngra==4) $data['ng']='OA-Ahome';
			$this->load->view('provta/reporte',$data); 
        }
		
		
		function pdfreppvr( ) {
            $this->load->model('provta_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('provta/lista',$data);
			$data['tabla'] = $this->input->post('tablapvr');
			$html = $this->load->view('provta/preciosr', $data, true);  
			pdf ($html,'Precios', true);
        }
		function pdfreppre( ) {
            $this->load->model('provta_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('provta/lista',$data);
			$data['tabla'] = $this->input->post('tabla');
			$html = $this->load->view('provta/precios', $data, true);  
			pdf ($html,'Precios', true);
        }
		public function tablaprovta($dia=0,$tipo=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($dia!=0){$filter['where']['feccos =']=$dia;}
			if($tipo>0) {$filter['where']['tipcos =']=$tipo;}
			
        	$data['rows'] = $this->provta_model->provta($filter);
        	echo '('.json_encode($data).')';                
    	}
		/*public function tablagral($secc=0){
			$filter = $this->ajaxsorter->filter($this->input);	
			$filter['where']['numgra =']=4;
			//$filter['where']['ciccha =']=$ciclo;
			if($secc>0) $filter['where']['secc =']=$secc;        
        	  
			$data['rows'] = $this->provta_model->gral($filter);
        	echo '('.json_encode($data).')';                
    	}
		*/
			
		public function tablagral($cica=0,$gra=0,$secc=0,$tc=0){
			$filter = $this->ajaxsorter->filter($this->input);	
			$cic=substr($cica, 0,4);$cic=substr($cic, 2,2);
			//$cic=substr($cic);
			if($cica>0) $filter['where']['cicg =']=$cica;
			if($gra>0) $filter['where']['numgra =']=$gra;
			if($secc>0) $filter['where']['secc =']=$secc;        
        	  
			$data['rows'] = $this->provta_model->gral($filter,$cic,$cica,$tc,$gra);
        	echo '('.json_encode($data).')';                
    	}
		public function tablasue($cic=0){
			$filter = $this->ajaxsorter->filter($this->input);
			if($cic>0) $filter['where']['cicsem =']=$cic;	
			$data['rows'] = $this->provta_model->sueldos($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaotc($cic=0){
			$filter = $this->ajaxsorter->filter($this->input);
			if($cic>0) $filter['where']['cicotc =']=$cic;	
			$data['rows'] = $this->provta_model->otroscostos($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablacom($cic=0){
			$filter = $this->ajaxsorter->filter($this->input);
			if($cic>0) $filter['where']['cicdie =']=$cic;		
			$data['rows'] = $this->provta_model->combustible($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaprevtas($fec=''){
			$filter = $this->ajaxsorter->filter($this->input);
			if($fec!='') $filter['where']['fecpre =']=$fec;
			$data['rows'] = $this->provta_model->prevta($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaprevtasr($cic='',$talla=0){
			$filter = $this->ajaxsorter->filter($this->input);
			if($cic!='') $filter['where']['ciclo =']='20'.$cic;
			if($talla>0) $filter['where']['talsal =']=$talla;
			$data['rows'] = $this->provta_model->prevtar($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaprevtash($grs=0){
			$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['idgrsp =']=$grs;
			$data['rows'] = $this->provta_model->prevtah($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablagrsmes(){
			$filter = $this->ajaxsorter->filter($this->input);
			$data['rows'] = $this->provta_model->grsmes($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		public function secciones1(){
			$filter['numgra']=$this->input->post('numgrabg');
			$data = $this->provta_model->secciones($filter); 
			echo '('.json_encode($data).')';              
    	}

		public function secciones($filest=0){
			$filest=$this->input->post('filest');
			$data = $this->provta_model->secciones($filest); 
			echo '('.json_encode($data).')';              
    	}

		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('provta_model');
			$id_post=$this->input->post('id');
			$tab=$this->input->post('tabla'); 
			$cam=$this->input->post('campo'); 
			$ciclo=19;
			if($id_post!=''){
				$return=$this->provta_model->quitar($id_post,$tab,$cam,$ciclo); 			
				redirect('provta');
			}
		}
		
		function actualizarsue($id=0){
		$this->load->helper('url');
		$this->load->model('provta_model');
		$id_post=$this->input->post('id'); 
		$num=$this->input->post('num');
		$fec=$this->input->post('fec');
		$imp=$this->input->post('imp');
		$obs=$this->input->post('obs');
		$cic=$this->input->post('cic');
		if($id_post!=''){
			$return=$this->provta_model->actualizarsue($id_post,$num,$fec,$imp,$obs,$cic); 			
			redirect('provta');
		}
		
		}
		
		function agregarsue(){
		$this->load->helper('url');
		$this->load->model('provta_model');		
		$num=$this->input->post('num');
		$fec=$this->input->post('fec');
		$imp=$this->input->post('imp');
		$obs=$this->input->post('obs');
		$cic=$this->input->post('cic');
		if($fec!=''){	
			$this->provta_model->agregarsue($num,$fec,$imp,$obs,$cic);			
			redirect('provta');
		}
		}
		
		
		function actualizardie($id=0){
		$this->load->helper('url');
		$this->load->model('provta_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$fac=$this->input->post('fac');
		$lts=$this->input->post('lts');
		$pre=$this->input->post('pre');
		$tip=$this->input->post('tip');
		$cic=$this->input->post('cic');
		if($tip==2) $lts=1;
		if($id_post!=''){
			$return=$this->provta_model->actualizardie($id_post,$fec,$fac,$lts,$pre,$tip,$cic); 			
			redirect('provta');
		}
		
		}
		
		function agregardie(){
		$this->load->helper('url');
		$this->load->model('provta_model');		
		$fec=$this->input->post('fec');
		$fac=$this->input->post('fac');
		$lts=$this->input->post('lts');
		$pre=$this->input->post('pre');
		$tip=$this->input->post('tip');
		$cic=$this->input->post('cic');
		if($tip==2) $lts=1;
		if($fec!=''){	
			$this->provta_model->agregardie($fec,$fac,$lts,$pre,$tip,$cic);			
			redirect('provta');
		}
		}		
		
		
		function actualizarpre($id=0){
		$this->load->helper('url');
		$this->load->model('provta_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$grs=$this->input->post('grs');
		$pcc=$this->input->post('pcc');
		if($id_post!=''){
			$return=$this->provta_model->actualizarpre($id_post,$fec,$grs,$pcc); 			
			redirect('provta');
		}
		
		}
		
		function agregarpre(){
		$this->load->helper('url');
		$this->load->model('provta_model');		
		$fec=$this->input->post('fec');
		$grs=$this->input->post('grs');
		$pcc=$this->input->post('pcc');
		if($fec!=''){	
			$this->provta_model->agregarpre($fec,$grs,$pcc);			
			redirect('provta');
		}
		}
		
		function actualizarotc($id=0){
		$this->load->helper('url');
		$this->load->model('provta_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$imp=$this->input->post('imp');
		$obs=$this->input->post('obs');
		$cic=$this->input->post('cic');
		if($id_post!=''){
			$return=$this->provta_model->actualizarotc($id_post,$cic,$fec,$imp,$obs); 			
			redirect('provta');
		}
		
		}
		
		function agregarotc(){
		$this->load->helper('url');
		$this->load->model('provta_model');		
		$fec=$this->input->post('fec');
		$imp=$this->input->post('imp');
		$obs=$this->input->post('obs');
		$cic=$this->input->post('cic');
		if($fec!=''){	
			$this->provta_model->agregarotc($cic,$fec,$imp,$obs);			
			redirect('provta');
		}
		}
		
		public function combob(){
			$filter['ciclo']=$this->input->post('ciclo');
			$datab = $this->provta_model->getElementsb($filter); 
			echo '('.json_encode($datab).')';              
    	}

    }
    
?>