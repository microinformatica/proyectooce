<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Depositos extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('depositos_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('depositos_model');
			$fi=date("Y-m-d");$ff= date("Y-m-d");
			#$data['dpDesde']="2012-01-01"; //$fi;
			#$data['dpHasta']=$ff;			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('depositos/lista',$data);
        }
		function pdfrep() {
            $this->load->model('depositos_model');
			//$data['result']=$this->depositos_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('depositos/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('depositos/listapdf', $data, true);  
			pdf ($html,'depositos/listapdf', true);
        	set_paper('letter');
        }
		public function tabla($desde='',$hasta=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($desde!='')
				$filter['where']['fecha >=']=$desde;
			if($hasta!='')
				$filter['where']['fecha <=']=$hasta;
			
			//$filter['where']['Car']=0;
			//$filter['where']['Des']=0;
			//$filter['num'] = $extra; 
        	$data['rows'] = $this->depositos_model->getDepositos($filter);
        	$data['num_rows'] = $this->depositos_model->getNumRows($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaclientes($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra; 
        	$data['rows'] = $this->depositos_model->getClientes($filter);
        	$data['num_rows'] = $this->depositos_model->getNumClientes($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function combo(){        
        	$filter['id_unidad']=$this->input->post('id_unidad');           
        	$data = $this->base_model->getElements($filter);        
        	echo '('.json_encode($data).')';              
    	}
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	} 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('depositos_model');		
		$fec=$this->input->post('fec');
		$usd=$this->input->post('usd'); if ($usd=='') $usd=0;
		$tc=$this->input->post('tc');  if ($tc=='') $tc=0;
		$cta=$this->input->post('cta');
		$obs=$this->input->post('obs');
		$mn=$this->input->post('mn'); if ($mn=='') $mn=0;
		$numcli=$this->input->post('nrc');
		$ciclo=$this->input->post('ciclo');
		$bormaq=$this->input->post('bormaq');
		//if($rem!=''){	
			$this->depositos_model->agregar($fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$bormaq);			
			redirect('depositos');
		//}
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('depositos/agregar',$datos);
		}
		
		function actualizar($id=0){
		//$this->load->helper('url');
		//$this->load->model('depositos_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$usd=$this->input->post('usd'); if ($usd=='') $usd=0;
		$tc=$this->input->post('tc'); if ($tc=='') $tc=0;
		$cta=$this->input->post('cta');
		$obs=$this->input->post('obs');
		$mn=$this->input->post('mn');  if ($mn=='') $mn=0;
		$numcli=$this->input->post('nrc');
		$ciclo=$this->input->post('ciclo');
		$bormaq=$this->input->post('bormaq');
		if($id_post!=''){
			$return=$this->depositos_model->actualizar($id_post,$fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$bormaq); 			
			redirect('depositos');
		}
		/*if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->depositos_model->getCliente($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('depositos/actualizar',$datos);*/
		}
		function borrar($id=0){
		//$this->load->helper('url');
		//$this->load->model('depositos_model');
		$id_post=$this->input->post('id'); 
		$usd=$this->input->post('usd');   if ($usd=='') $usd=0;
		$numcli=$this->input->post('nrc');		
		if($id_post!=''){
			$return=$this->depositos_model->borrar($id_post,$usd,$numcli); 			
			redirect('depositos');
		}
		/*if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->depositos_model->getCliente($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('depositos/borrar',$datos);*/
		}
				
    }
    
?>