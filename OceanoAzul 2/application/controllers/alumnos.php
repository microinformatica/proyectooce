<?php 
    class Alumnos extends CI_Controller {
        public function __construct() {
        parent::__construct();	
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
	    
	   }
        function index() {
        	//http://sistemas.upsin.edu.mx/manual_ci/assets/js/jquery-1.7.2.min.js
            $this->load->model('alumnos_model');
			/*$this->alumnos_model->insertar('Mateo Benitez',2);*/
			/*$this->alumnos_model->agregar('Jose Lopez',21,1);
			echo $this->alumnos_model->actualizar(3,"zuleima Patricia",17);
			echo "Desactivar registro";
			echo $this->alumnos_model->desactivar(3);
			echo "Activar registro";
			echo $this->alumnos_model->activar(3);
			echo $this->alumnos_model->getNombre(3);
			echo $this->alumnos_model->getEdad(3);
			$data['result']=$this->alumnos_model->get_last_ten_entries();*/
			$data['result']=$this->alumnos_model->verActivos();
			$this->load->view('alumnos/lista',$data);
        }
		function agregar(){
		$this->load->helper('url');
		$this->load->model('alumnos_model');
		$nombre=$this->input->post('nombre');
		$edad=$this->input->post('edad');
		if($nombre!='' && $edad!=''){
			$this->alumnos_model->agregar($nombre,$edad,1);
			redirect('alumnos');
		}
		$this->load->view('alumnos/agregar');
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('alumnos_model');
		$id_post=$this->input->post('id');
		
		$nombre=$this->input->post('nombre');
		$edad=$this->input->post('edad');
		if($id_post!=''){
			
			$return=$this->alumnos_model->actualizar($id_post,$nombre,$edad); 
			if($return>0){
				
			}else{
			
			}
			redirect('alumnos');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->alumnos_model->getAlumno($id);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$this->load->view('alumnos/actualizar',$datos);
	}
		function buscar(){
		$busqueda=$this->input->post('busqueda');
		$this->load->helper('url');
		$this->load->model('alumnos_model');
		$datos['result']=$this->alumnos_model->buscar_activos($busqueda);
		$datos['busqueda']=$busqueda;
		$this->load->view('alumnos/buscar',$datos);
}
		
		
    }
    
?>