<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Bcv extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('bcv_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }        
        function index() {
            $this->load->model('bcv_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('bcv/lista',$data);
        }
		function pdfrep() {
            $this->load->model('bcv_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$data['zona']=$this->input->post('zonasel');
			//$data['ciclo']=$this->input->post('ciclosel2');
			$this->load->view('bcv/lista',$data);
			//$data['zona']=$this->input->post('zonasel');			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('bcv/listapdf', $data, true);  
			pdf ($html,'bcv/listapdf', true);
        	set_paper('letter');
        }		
		public function tabla($bcv=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']=$bcv;
			$data['rows'] = $this->bcv_model->getBCV($filter);
        	$data['num_rows'] = $this->bcv_model->getNumRowsBCV($filter);
        	echo '('.json_encode($data).')';
    	}
		public function combo(){        
        	$filter['NomDes']=$this->input->post('NomDes');           
        	$data = $this->viaticos_model->getElements($filter);        
        	echo '('.json_encode($data).')';  
    	}
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	}
		function agregar(){
		$this->load->helper('url');
		$this->load->model('cargos_model');		
		$tablas=$this->input->post('tablas');	
		$nom=$this->input->post('nom');
		$act=$this->input->post('act');
		if($nom!=''){	
			$this->bcv_model->agregar($nom,$act,$tablas);			
			redirect('bcv');
		}
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('bcv/lista',$datos);
		} 
		function actualizar($id=0){
		$filter = $this->ajaxsorter->filter($this->input); 		
		$this->load->helper('url');
		$this->load->model('bcv_model');	
		$tablas=$this->input->post('tablas');	
		$id_post=$this->input->post('id'); 
		$nom=$this->input->post('nom');
		$act=$this->input->post('act');
		if($id_post!=''){
			$return=$this->bcv_model->actualizar($id_post,$nom,$act,$tablas); 			
			redirect('bcv');
		}
		/*if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->bcv_model->getViatico($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('bcv/lista',$datos);*/
		}
		function borrar($id=0){
		$this->load->helper('url');
		$this->load->model('bcv_model');
		$id_post=$this->input->post('id');
		$tablas=$this->input->post('tablas'); 
		if($id_post!=''){
			$return=$this->bcv_model->borrar($id_post,$tablas); 			
			redirect('bcv');
		}
		/*if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->viaticos_model->getViatico($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('viaticos/lista',$datos);*/
		}		
    }
    
?>