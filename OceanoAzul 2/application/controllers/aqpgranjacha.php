<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Aqpgranjacha extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('aqpgranjacha_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('aqpgranjacha_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('aqpgranjacha/lista',$data);
        }
		function pdfdiacha() {
            $this->load->model('aqpgranjacha_model');
			//$data['result']=$this->aqpgranjacha_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablacha');
			$data['dia']=$this->input->post('diaali');
			$ng=$this->input->post('granjaa');
			if($ng==1) $data['ng']='Santa Fe';
			if($ng==2) $data['ng']='Gran Kino'; 
			if($ng==3) $data['ng']='Jazmin';
			if($ng==4) $data['ng']='Acuícola Océano Azul S.A. de C.V.';
			$data['fec1'] = $this->input->post('txtFIAli');
			$html = $this->load->view('aqpgranjacha/alimentos', $data, true);  
			pdf ($html,$data['ng'].'_Alimento_'.$data['dia'], true);        	
        }
		function pdfrepgral() {
            $this->load->model('aqpgranjacha_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablagral');
			$ng=$this->input->post('numgrachat');
			$mes=$this->input->post('cmbMesgral');
			switch($mes){
				case 1 : $mes='Enero'; break;
				case 2 : $mes='Febrero'; break;
				case 3 : $mes='Marzo'; break;
				case 4 : $mes='Abril'; break;
				case 5 : $mes='Mayo'; break;
				case 6 : $mes='Junio'; break;
				case 7 : $mes='Julio'; break;
				case 8 : $mes='Agosto'; break;
				case 9 : $mes='Septiembre'; break;
				case 10 : $mes='Octubre'; break;
				case 11 : $mes='Noviembre'; break;
				case 12 : $mes='Diciembre'; break;
			}
			if($ng==1) $data['ng']='Santa Fe';
			if($ng==2) $data['ng']='Gran Kino'; 
			if($ng==3) $data['ng']='Jazmin';
			if($ng==4) $data['ng']='Ahome';
			$data['mes'] = $mes;
			$html = $this->load->view('aqpgranjacha/general', $data, true);  
			pdf ($html,$data['ng'].'_Ali_Mes_'.$data['mes'], true);        	
        }
		public function tabladiacha($numgra=0,$ciclo='',$fec=0,$sec=0,$est=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgracha =']=$numgra;
			$filter['where']['ciccha =']=$ciclo;
			if($est==0){
				$filter['where']['feccha =']=$fec;	
			}else{
				$filter['where']['idpischa =']=$est;
			}
			$data['rows'] = $this->aqpgranjacha_model->getDatosdia($filter,$fec,$est,$numgra,$sec,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tabladiacha1($numgra=0,$ciclo='',$fec=0,$sec=0,$est=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgracha =']=$numgra;
			$filter['where']['ciccha =']=$ciclo;
			
			if($est==0){
				$filter['where']['feccha =']=$fec;
				$filter['where']['secc =']=$sec;
				
			}else{
				$filter['where']['secc =']=$sec;	
				$filter['where']['idpischa =']=$est;
				
			}
        	$data['rows'] = $this->aqpgranjacha_model->getDatosdia($filter,$fec,$est,$numgra,$sec);
        	echo '('.json_encode($data).')';                
    	}
		public function tablafac($gra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$data['rows'] = $this->aqpgranjacha_model->getDatosfac($filter,$gra);
        	echo '('.json_encode($data).')';                
    	}
		public function tablagral($gra=0,$cic='',$mes=0,$ha=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($gra>0) $filter['where']['numgracha =']=$gra;			
			$filter['where']['ciccha =']=$cic;
			$filter['where']['month(feccha) =']=$mes;
			$filter['where']['feccha >']='2017-07-11';
			$data['rows'] = $this->aqpgranjacha_model->getDatosgral($filter,$ha);
        	echo '('.json_encode($data).')';                
    	}
		public function tablames($cic='',$mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgracha =']=4;			
			$filter['where']['ciccha =']=$cic;
			$filter['where']['month(feccha) =']=$mes;
			//$filter['where']['feccha >']='2017-07-11';
			$data['rows'] = $this->aqpgranjacha_model->getDatosmes($filter,$mes,$cic);
        	echo '('.json_encode($data).')';                
    	}
		function actualizarfac($id=0){
		$this->load->helper('url');
		$this->load->model('aqpgranjacha_model');
		$id_post=$this->input->post('id'); 
		$por=$this->input->post('por');
		$gra=$this->input->post('gra');
		if($id_post!=''){
			$return=$this->aqpgranjacha_model->actualizarfac($id_post,$por,$gra); 			
			redirect('aqpgranjacha');
		}
		}
		function actualizardia($id=0){
		$this->load->helper('url');
		$this->load->model('aqpgranjacha_model');
		$id_post=$this->input->post('id'); 
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$cic=$this->input->post('cic');
		$numgra=$this->input->post('gra');
		$ch1=$this->input->post('ch1');
		/*$ch2=$this->input->post('ch2');
		$ch3=$this->input->post('ch3');
		$ch4=$this->input->post('ch4');
		$ch5=$this->input->post('ch5');
		$ch6=$this->input->post('ch6');
		$tur=$this->input->post('tur');*/
		$kgt=$this->input->post('kgt');
		$rac=$this->input->post('rac');
		if($id_post!=''){
			//$return=$this->aqpgranjacha_model->actualizardia($id_post,$pis,$fec,$cic,$numgra,$ch1,$ch2,$ch3,$ch4,$ch5,$ch6,$tur,$kgt); 			
			$return=$this->aqpgranjacha_model->actualizardia($id_post,$pis,$fec,$cic,$numgra,$ch1,$kgt,$rac);
			redirect('aqpgranjacha');
		}
		
		}
		
		function agregardia(){
		$this->load->helper('url');
		$this->load->model('aqpgranjacha_model');		
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$cic=$this->input->post('cic');
		$numgra=$this->input->post('gra');
		$ch1=$this->input->post('ch1');
		$ch2=$this->input->post('ch2');
		$ch3=$this->input->post('ch3');
		$ch4=$this->input->post('ch4');
		$ch5=$this->input->post('ch5');
		$ch6=$this->input->post('ch6');
		$tur=$this->input->post('tur');
		$kgt=$this->input->post('kgt');
		$rac=$this->input->post('rac');
		if($pis!=''){	
			$this->aqpgranjacha_model->agregardia($pis,$fec,$cic,$numgra,$ch1,$ch2,$ch3,$ch4,$ch5,$ch6,$tur,$kgt,$rac);			
			redirect('aqpgranjacha');
		}
		}
		public function combob(){
			$filter['cicg']=$this->input->post('ciccha');
			$ciclo=$this->input->post('ciccha');
			//$filter['secc']=$this->input->post('secc');
			$data = $this->aqpgranjacha_model->getElementsb($filter,$ciclo); 
			echo '('.json_encode($data).')';              
    	}
		
		function buscar(){
			$pil = $this->input->post('pil');
			//busca el actual si lo encuentra deja los datos
			$data =$this->aqpgranjacha_model->history($pil);
			if($data->cp1=='-1') $data->cp1=0;if($data->cp2=='-1') $data->cp2=0;if($data->cp3=='-1') $data->cp3=0;if($data->cp4=='-1') $data->cp4=0;
			if($data->cp5=='-1') $data->cp5=0;if($data->cp6=='-1') $data->cp6=0;
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('cp1'=>$data->cp1,'cp2'=>$data->cp2,'cp3'=>$data->cp3,'cp4'=>$data->cp4,'cp5'=>$data->cp5,'cp6'=>$data->cp6));
			}
						
		}
		function buscarhas(){
			$pil = $this->input->post('pil');
			//busca el actual si lo encuentra deja los datos
			$data =$this->aqpgranjacha_model->historyhas($pil);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('hasga'=>$data->hasg));
			}
						
		}
		function borrarcha($id=0){
			$this->load->helper('url');
			$this->load->model('aqpgranjacha_model');
			$id_post=$this->input->post('id');
			if($id_post!=''){
				$return=$this->aqpgranjacha_model->quitarcha($id_post); 			
				redirect('aqpgranjacha');
			}
		}
				
    }
    
?>