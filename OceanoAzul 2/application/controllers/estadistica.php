<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Estadistica extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('estadistica_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('estadistica_model');
			$data['result']=$this->estadistica_model->zonas();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('estadisticas/lista',$data);
        }
		function pdfrep() {
            $this->load->model('estadistica_model');
			//$data['result']=$this->estadistica_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['zona']=$this->input->post('zonasel');
			$this->load->view('estadisticas/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('estadisticas/listapdf', $data, true);  
			pdf ($html,'estadisticas/listapdf', true);
        	set_paper('letter');
        }
		/*public function tabla11($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra; 
        	$data['rows'] = $this->estadistica_model->getEstadistica($filter);
        	$data['num_rows'] = $this->estadistica_model->getNumRows($filter);
        	echo '('.json_encode($data).')';                
    	}*/
		public function tabla($zona='',$cancelacion=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	if($zona!=''){
        		if($zona=='El%20Dorado') $zona='El Dorado';
				if($zona=='Sur%20Sinaloa') $zona='Sur Sinaloa';
				if($zona=='Obreg%C3%B3n') $zona='Obregón';
				if($zona=='Yucat%C3%A1n') $zona='Yucatán';
        		$filter['where']['zona']=$zona;
			} 
			if($cancelacion==-1){$filter['where']['SI']=0;}
        	$data['rows'] = $this->estadistica_model->getEstadistica($filter);
        	$data['num_rows'] = $this->estadistica_model->getNumRows($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function combo(){        
        	$filter['id_unidad']=$this->input->post('id_unidad');           
        	$data = $this->base_model->getElements($filter);        
        	echo '('.json_encode($data).')';              
    	}
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	} 
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('estadistica_model');
		$id_post=$this->input->post('id'); 
		$obs=$this->input->post('obs');
		$sn=$this->input->post('sn');
		if($id_post!=''){
			$return=$this->estadistica_model->actualizar($id_post,$obs,$sn); 			
			redirect('estadistica');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->estadistica_model->getCliente($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('estadisticas/actualizar',$datos);
		}
		
				
    }
    
?>