<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Clientes extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('clientes_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('clientes_model');
			//$data['result']=$this->clientes_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('clientes/lista',$data);
        }
		function pdfrep() {
            $this->load->model('clientes_model');
			//$data['result']=$this->clientes_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('clientes/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('clientes/listapdf', $data, true);  
			pdf ($html,'clientes/listapdf', true);
        	set_paper('letter');
        }
		public function tabla($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($extra==0){$filter['num'] = $extra;}
			if($extra==1){$filter['where']['dom =']='';} 
			if($extra==2){$filter['where']['loc =']='';}
			if($extra==3){$filter['where']['edo =']='-';}
			if($extra==4){$filter['where']['rfc =']='';}
			if($extra==5){$filter['where']['cp =']='';}
			if($extra==6){$filter['where']['zona =']='-';}
			if($extra==7){$filter['where']['ubigra =']='';}
        	$data['rows'] = $this->clientes_model->getUsuarios($filter);
        	$data['num_rows'] = $this->clientes_model->getNumRows($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function combo(){        
        	$filter['id_unidad']=$this->input->post('id_unidad');           
        	$data = $this->base_model->getElements($filter);        
        	echo '('.json_encode($data).')';              
    	}
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	} 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('clientes_model');
		$nombre=$this->input->post('nombre');
		$dom=$this->input->post('dom');$ubi=$this->input->post('ubi');
		$loc=$this->input->post('loc');
		$edo=$this->input->post('edo');
		$rfc=$this->input->post('rfc');
		$cp=$this->input->post('cp');
		$con=$this->input->post('con');
		$cor=$this->input->post('cor');
		$tel=$this->input->post('tel');
		$zona=$this->input->post('zona');
		$pre=$this->input->post('pre');
		$avi=$this->input->post('avi');
		$gui=$this->input->post('gui');
		//if($nombre!='' && $dom!=''){
		if($nombre!=''){	
			$this->clientes_model->agregar($nombre,$dom,$loc,$edo,$rfc,$cp,$zona,$ubi,$cor,$con,$tel,$pre,$avi,$gui);
			redirect('clientes');
		}
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('clientes/agregar',$datos);
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('clientes_model');
		$id_post=$this->input->post('id'); //id
		$nombre=$this->input->post('nombre');
		$dom=$this->input->post('dom');$ubi=$this->input->post('ubi');
		$loc=$this->input->post('loc');
		$edo=$this->input->post('edo');
		$rfc=$this->input->post('rfc');
		$cp=$this->input->post('cp');
		$con=$this->input->post('con');
		$cor=$this->input->post('cor');
		$tel=$this->input->post('tel');
		$zona=$this->input->post('zona');
		$pre=$this->input->post('pre');
		$avi=$this->input->post('avi');
		$gui=$this->input->post('gui');
		//echo $id_post.$nombre.$dom;
		if($id_post!=''){
			$return=$this->clientes_model->actualizar($id_post,$nombre,$dom,$loc,$edo,$rfc,$cp,$zona,$ubi,$cor,$con,$tel,$pre,$avi,$gui); 
			redirect('clientes');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->clientes_model->getCliente($id);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('clientes/actualizar',$datos);
	}
		function buscar(){
		$busqueda=$this->input->post('busqueda');
		$zon=$this->input->post('zon');
		$this->load->helper('url');
		$this->load->model('clientes_model');
		$datos['result']=$this->clientes_model->buscar_activos($busqueda,$zon);		
		$datos['busqueda']=$busqueda;
		$datos['zon']=$zon;		
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('clientes/buscar',$datos);
}
		function borrara($id=0){
		$this->load->helper('url');
		$this->load->model('clientes_model');
		$id_post=$this->input->post('id');
		if($id_post!=''){
			$return=$this->clientes_model->borrara($id_post); 			
			redirect('programa');
		}
		}
		
    }
    
?>