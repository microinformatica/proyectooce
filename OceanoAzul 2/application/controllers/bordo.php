<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     class Bordo extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
       	$this->load->model('bordo_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('bordo_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			/*$cic=date('y');		
			$fol['result'] = $this->bordo_model->ultimofolio($cic);
			$uf=0;
			foreach ($fol['result'] as $row): 
				$uf=$row->ultimo+1;										
			endforeach;
			$data['folio']=$uf;*/											
			$this->load->view('bordo/lista',$data);
        }

		function reporte( ) {
            $data['tablac'] = $this->input->post('tabla'); 
			$ngra=$this->input->post('numgrabg');
			if($ngra==2) $data['ng']='OA-Kino'; 
			if($ngra==4) $data['ng']='OA-Ahome';
			if($ngra==6) $data['ng']='OA-Huatabampo';
			$this->load->view('aqpgranja/reporte',$data); 
        }
		function reportet( ) {
            $data['tablac'] = $this->input->post('tabla'); 
			$ngra=$this->input->post('numgrabgz');
			if($ngra==2) $data['ng']='OA-Kino'; 
			if($ngra==4) $data['ng']='OA-Ahome';
			if($ngra==6) $data['ng']='OA-Huatabampo';
			$this->load->view('aqpgranja/reporte',$data); 
        }
		
		function buscarfn($cic=0){
			$data=array();
			$fol['result'] = $this->bordo_model->ultimofolio($cic);
			$uf=0;
			foreach ($fol['result'] as $row): 
				$uf=$row->ultimo+1;										
			endforeach;
			$data['folio']=$uf;	
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				//echo json_encode(array('numfn'=>($data->ultimo+1)));
				echo json_encode(array('folio'=>$data['folio']));
			}else{
				echo json_encode(array('numfn'=>''));
			}
						
		}
		
		function ultimofolio(){
			$cic = $this->input->post('cic');
			//busca el actual si lo encuentra deja los datos
			//$data='';
			$data='';
			$data =$this->bordo_model->ultimofolio($cic);
			//$size=sizeof($data);
			if($data!=''){
				//si lo encuentra deja los datos
				echo json_encode(array('folio'=>($data->folio)+1));
			}else{
				echo json_encode(array('folio'=>''));
			}			
		}
		
		
		
		function pdfrepcos( ) {
            $this->load->model('bordo_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('bordo/lista',$data);
			$data['tabla'] = $this->input->post('tabla');
			$data['dia'] = $this->input->post('dia');
			$data['tip'] = $this->input->post('tip');
			$data['fol'] = $this->input->post('fol');
			$data['cli'] = $this->input->post('cli');
			$data['gra'] = $this->input->post('gra');
			$html = $this->load->view('bordo/listapdfdia', $data, true);  
			if($data['tip']==0) pdf ($html,$data['dia'].'_Gral', true);
			if($data['tip']==1) pdf ($html,$data['dia'].'_Maq', true);         	
        }
		function pdfrepcosb( ) {
            $this->load->model('bordo_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('bordo/lista',$data);
			$data['tabla'] = $this->input->post('tabla');
			$data['gra'] = $this->input->post('gra');
			$dia=date("d-m-Y");
			$data['dia'] =date("Y-m-d");
			$html = $this->load->view('bordo/listapdfdiab', $data, true);  
			pdf ($html,'Bordo_'.$dia, true);
			         	
        }
		function pdfrepcosg( ) {
            $this->load->model('bordo_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('bordo/lista',$data);
			$data['tabla'] = $this->input->post('tablag');
			$data['gra'] = $this->input->post('gra1');
			$html = $this->load->view('bordo/listapdfgral', $data, true);  
			//$dia=date("Y-m-d");
			pdf ($html,'Rep_Gral_'.date("d-m-Y"), true);
			         	
        }
		public function tablabordo($cic=0,$dia=0,$gra=0,$tipo=0,$folio=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($dia!=0){$filter['where']['feccos =']=$dia;}
			if($gra!=0){$filter['where']['numgrab =']=$gra;}
			if($tipo>0) {$filter['where']['tipcos =']=$tipo;}
			if($folio!='') {$filter['where']['folio =']=$folio;}
			$data['rows'] = $this->bordo_model->bordo($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}
		public function tablabordoc($cic=0,$gra=0,$est=0,$nci=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($gra!=0){$filter['where']['numgrab =']=$gra;}
			if($est!=0) {$filter['where']['estcos =']=$est;}
			if($nci!=0) {$filter['where']['ncicos =']='20'.$cic.'-'.$nci;}
			$data['rows'] = $this->bordo_model->bordoc($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}
		public function tablagralb($cic=0,$gra=0){
			$filter = $this->ajaxsorter->filter($this->input);	
			if($gra>0) $filter['where']['numgrab =']=$gra;
			$data['rows'] = $this->bordo_model->bordogral($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}
		public function tablagral($cic=0,$gra=0,$secc=0){
			$filter = $this->ajaxsorter->filter($this->input);	
			if($gra>0) $filter['where']['numgrab =']=$gra;
			if($secc>0) $filter['where']['secc =']=$secc;        
        	  
			$data['rows'] = $this->bordo_model->gral($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}
		function agregaCos(){
		$this->load->helper('url');
		$this->load->model('bordo_model');
		$fec=$this->input->post('fec'); $tip=$this->input->post('tip');	$num=$this->input->post('num');
		$cli=$this->input->post('cli');	$est=$this->input->post('est');	$grs=$this->input->post('grs');
		$kgs=$this->input->post('kgs');	$pre=$this->input->post('pre');	$gra=$this->input->post('gra');
		$fol=$this->input->post('fol');	$cic=$this->input->post('cic'); $nci=$this->input->post('nci'); 
		if($fec!=''){	
			$this->bordo_model->agregacos($fec,$tip,$num,$cli,$est,$grs,$kgs,$pre,$gra,$fol,$cic,$nci);			
			redirect('bordo');
		}
		}
		function actualizaCos($id=0){
		$this->load->helper('url');
		$this->load->model('bordo_model');
		$id=$this->input->post('id');
		$fec=$this->input->post('fec'); $tip=$this->input->post('tip');	$num=$this->input->post('num');
		$cli=$this->input->post('cli');	$est=$this->input->post('est');	$grs=$this->input->post('grs');
		$kgs=$this->input->post('kgs');	$pre=$this->input->post('pre');	$gra=$this->input->post('gra');
		$fol=$this->input->post('fol');	$cic=$this->input->post('cic'); $nci=$this->input->post('nci'); 
		if($id!=''){
			$return=$this->bordo_model->actualizacos($id,$fec,$tip,$num,$cli,$est,$grs,$kgs,$pre,$gra,$fol,$cic,$nci); 	
			redirect('bordo');
		}		
		}
		
		function quitarCos($id=0){
		$this->load->helper('url');
		$this->load->model('bordo_model');
		$id_post=$this->input->post('id');
		$cic=$this->input->post('cic'); 
		if($id_post!=''){
			$return=$this->bordo_model->borrarcos($id_post,$cic); 			
			redirect('bordo');
		}
		}
		
		public function combob($cic=0){
			$cic=$this->input->post('cicloG');
			$filter['numgra']=$this->input->post('numgra');
			$data = $this->bordo_model->getElementsb($filter,$cic); 
			echo '('.json_encode($data).')';              
    	}
    	public function combobp($filest=0){
			$filest=$this->input->post('filest');
			$data = $this->bordo_model->getElementsbp($filest); 
			echo '('.json_encode($data).')';              
    	}
		/*
		public function combobB(){
			//$filter['cicg']=$this->input->post('cicbor');
			$filter['where']['cicg =']=$this->input->post('cicbor');
			$ciclo=$this->input->post('cicbor');
			$data = $this->bordo_model->getElementsb($filter,$ciclo); 
			echo '('.json_encode($data).')';              
    	}*/
		
		public function secciones($cic=0){
			$cic=$this->input->post('cicloG');
			$filter['numgra']=$this->input->post('numgrabg');
			$data = $this->bordo_model->secciones($filter,$cic); 
			echo '('.json_encode($data).')';              
    	}
		
		
    }
    
?>