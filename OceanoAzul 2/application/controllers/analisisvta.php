<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Analisisvta extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('analisisvta_model');         
        $this->load->library(array('ajaxsorter','session','enletras','libreria','user_agent'));		
        $this->load->helper(array('url','form','html','pdf'));
		$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login'); 
	   }
        
        function index() {
            $this->load->model('analisisvta_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('analisisvta/lista',$data);
        }
		
		public function tablavtag($ciclo='',$alm=0,$cli=0,$tal=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['ciclo =']=$ciclo;
			if($alm>0)$filter['where']['almsal =']=$alm;
			if($cli>0)$filter['where']['clisal =']=$cli;
			//if($tal>0)$filter['where']['talsal =']=$tal;
			$data['rows'] = $this->analisisvta_model->getvtaG($filter,$tal);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablavtam($ciclo='',$alm=0,$cli=0,$tal=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['ciclo =']=$ciclo;
			if($alm>0)$filter['where']['almsal =']=$alm;
			if($cli>0)$filter['where']['clisal =']=$cli;
			//if($tal>0)$filter['where']['talsal =']=$tal;
			$data['rows'] = $this->analisisvta_model->getvtaM($filter,$tal);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablavtat($ciclo='',$alm=0,$cli=0,$tal=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['ciclo =']=$ciclo;
			if($alm>0)$filter['where']['almsal =']=$alm;
			if($cli>0)$filter['where']['clisal =']=$cli;
			///if($tal>0)$filter['where']['gpoid =']=$tal;
			$data['rows'] = $this->analisisvta_model->getvtaT($filter,$tal);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablavtamt($ciclo='',$alm=0,$cli=0,$rep=0,$tal=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['ciclo =']=$ciclo;
			if($alm>0)$filter['where']['almsal =']=$alm;
			if($cli>0)$filter['where']['clisal =']=$cli;
			$data['rows'] = $this->analisisvta_model->getvtaMT($filter,$tal,$rep);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablavtact($ciclo='',$alm=0,$cli1=0,$cli2=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['ciclo =']=$ciclo;
			//if($alm>0)$filter['where']['almsal =']=$alm;
			//if($cli>0)$filter['where']['clisal =']=$cli;
			$data['rows'] = $this->analisisvta_model->getvtaCT($filter,$cli1,$cli2);
        	echo '('.json_encode($data).')';                
    	}
		public function tablavtamr($ciclo='',$alm=0,$cli=0,$tal=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['ciclo =']=$ciclo;
			if($alm>0)$filter['where']['almsal =']=$alm;
			if($cli>0)$filter['where']['clisal =']=$cli;
			//if($tal>0)$filter['where']['talsal =']=$tal;
			$data['rows'] = $this->analisisvta_model->getvtaMR($filter,$tal,$cli,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		public function tablavtamrg($ciclo='',$alm=0,$cli=0,$tal=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['ciclo =']=$ciclo;
			if($alm>0)$filter['where']['almsal =']=$alm;
			if($cli>0)$filter['where']['clisal =']=$cli;
			//if($tal>0)$filter['where']['talsal =']=$tal;
			$data['rows'] = $this->analisisvta_model->getvtaMRG($filter,$tal,$cli,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		function tablainv($cic=0,$alm=0,$gra=0,$tal=0){
			$this->load->model('analisisvta_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($cic!='0') $filter['where']['cicent =']=($cic-2000);	
			if($alm!='0') $filter['where']['alment =']=$alm;
			if($gra!='0') $filter['where']['graent =']=$gra;
			if($tal!='0') $filter['where']['talent =']=$tal;
			$data['rows'] = $this->analisisvta_model->getinventarios($filter,$cic,$alm);
        	echo '('.json_encode($data).')'; 
    	}
		function tablainvg($cic=0,$alm=0,$gra=0,$tal=0){
			$this->load->model('analisisvta_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($cic!='0') $filter['where']['cicent =']=($cic-2000);	
			if($alm!='0') $filter['where']['alment =']=$alm;
			if($gra!='0') $filter['where']['graent =']=$gra;
			if($tal!='0') $filter['where']['talent =']=$tal;
			$data['rows'] = $this->analisisvta_model->getinventariosg($filter,$cic,$alm,$gra);
        	echo '('.json_encode($data).')'; 
    	}
		function tablafin($cic=0,$alm=0,$gra=0,$tal=0){
			$this->load->model('analisisvta_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($gra!='0') $filter['where']['idgp =']=$gra;
			/*if($cic!='0') $filter['where']['cicent =']=($cic-2000);	
			if($alm!='0') $filter['where']['alment =']=$alm;
			
			if($tal!='0') $filter['where']['talent =']=$tal;*/
			$cic=$cic-2000;
			$data['rows'] = $this->analisisvta_model->getfinal($filter,$cic,$alm,$gra);
        	echo '('.json_encode($data).')'; 
    	}
		function reporte1( ) {
        	$data['usuario']=$this->usuario;$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablavtag');$data['texto'] = $this->input->post('texto');
			$data['ciclo'] = $this->input->post('cic');
			$this->load->view('analisisvta/reporte1',$data); 
        }
		function reporte2( ) {
        	$data['usuario']=$this->usuario;$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablavtag');$data['texto'] = $this->input->post('texto');
			$data['ciclo'] = $this->input->post('cicm');
			$this->load->view('analisisvta/reporte1',$data); 
        }
		function reporte3( ) {
        	$data['usuario']=$this->usuario;$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablavtag');$data['texto'] = $this->input->post('texto');
			$data['ciclo'] = $this->input->post('cict');
			$this->load->view('analisisvta/reporte1',$data); 
        }
		function reporte4( ) {
        	$data['usuario']=$this->usuario;$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablavtag');$data['texto'] = $this->input->post('texto');
			$data['ciclo'] = $this->input->post('cicmt');
			$this->load->view('analisisvta/reporte1',$data); 
        }
		function reporte5( ) {
        	$data['usuario']=$this->usuario;$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablavtag');$data['texto'] = $this->input->post('texto');
			$data['ciclo'] = $this->input->post('cicct');
			$this->load->view('analisisvta/reporte1',$data); 
        }
		function reporte6( ) {
        	$data['usuario']=$this->usuario;$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablavtag');$data['texto'] = $this->input->post('texto');
			$data['ciclo'] = $this->input->post('cicmr');
			$this->load->view('analisisvta/reporte1',$data); 
        }
		function reporte7( ) {
        	$data['usuario']=$this->usuario;$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablavtag');$data['texto'] = $this->input->post('texto');
			$data['ciclo'] = $this->input->post('cicdt');
			$this->load->view('analisisvta/reporte1',$data); 
        }
		function reporte8( ) {
        	$data['usuario']=$this->usuario;$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablavtag');$data['texto'] = $this->input->post('texto');
			$data['ciclo'] = $this->input->post('cicf');
			$this->load->view('analisisvta/reporte1',$data); 
        }
		function buscarcli(){
			$cli = $this->input->post('cli');
			//busca el actual si lo encuentra deja los datos
			$data =$this->analisisvta_model->historycli($cli);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('razon'=>$data->Razon));
			}
						
		}
		function buscarconge(){
			$con = $this->input->post('con');
			//busca el actual si lo encuentra deja los datos
			$data =$this->analisisvta_model->historycon($con);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('noma'=>$data->noma));
			}
						
		}
		function buscartalgpo(){
			$tal = $this->input->post('tal');
			//busca el actual si lo encuentra deja los datos
			$data =$this->analisisvta_model->historygpo($tal);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('gpoid'=>$data->gpoid));
			}else echo json_encode(array('gpoid'=>0));
						
		}
		public function comboa(){
			$filter['ciclo']=$this->input->post('ciclo');
			$dataa = $this->analisisvta_model->getElementsa($filter); 
			echo '('.json_encode($dataa).')';              
    	}
		public function combob(){
			$filter['ciclo']=$this->input->post('ciclo');
			$datab = $this->analisisvta_model->getElementsb($filter); 
			echo '('.json_encode($datab).')';              
    	}
		public function comboc(){
			$filter['ida']=$this->input->post('ida');
			$datac = $this->analisisvta_model->getElementsc($filter); 
			echo '('.json_encode($datac).')';              
    	}
		
   }
    
?>