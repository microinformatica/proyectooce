<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Avisos extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
       	$this->load->model('avisos_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('avisos_model');
			//$data['result']=$this->produccion_model->Entregado();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('avisos/lista',$data);
        }
		function pdfrepmaq( ) {
            $this->load->model('avisos_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('avisos/lista',$data);
			$data['tablaf'] = $this->input->post('tablaf');
			$data['tablap'] = $this->input->post('tablap');
			$data['dia'] = $this->input->post('dia');
			$data['gra'] = $this->input->post('gra');
			$data['cic'] = $this->input->post('cic');
			$data['fre'] = $this->input->post('fre');
			$data['pro'] = $this->input->post('pro');
			$data['ren'] = $this->input->post('rend');
			$data['imp'] = $this->input->post('imp');
			$html = $this->load->view('avisos/listapdfdia', $data, true);  
			pdf ($html,$data['dia'].'_'.$data['gra'], true);
        	set_paper('letter');
        }
		
		function busquedas($cic=0,$desde='',$hasta=''){
			$this->load->model('avisos_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			if($desde!='') $filter['where']['fecavi >=']=$desde;
			if($hasta!='') $filter['where']['fecavi <=']=$hasta;
			$data['rows'] = $this->avisos_model->busquedas($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function tablac($cic=0,$id=''){
			$this->load->model('avisos_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			$filter['where']['idavicli =']=$id;
			$data['rows'] = $this->avisos_model->getavisosc($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function agregaravif(){
			$this->load->model('avisos_model');		
			$cli=$this->input->post('cli');
			$can=$this->input->post('can');
			$clia=$this->input->post('clia');
			$cic=$this->input->post('cic');
			if($cli!=''){	
				$this->avisos_model->agregaravif($cli,$can,$clia,$cic);			
				redirect('avisos');
			}
		} 
		function actualizaravif($id=0){
			$this->load->model('avisos_model');
			$id_post=$this->input->post('id'); 
			$cli=$this->input->post('cli');
			$can=$this->input->post('can');
			$clia=$this->input->post('clia');
			$cic=$this->input->post('cic');
			if($id_post!=''){
				$return=$this->avisos_model->actualizaravif($id_post,$cli,$can,$clia,$cic); 			
				redirect('avisos');
			}
		}
					
		
		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('avisos_model');
			$id_post=$this->input->post('id');
			$tab=$this->input->post('tabla'); 
			$cam=$this->input->post('campo'); 
			$cic=$this->input->post('cic');
			if($id_post!=''){
				$return=$this->avisos_model->quitar($id_post,$tab,$cam,$cic); 			
				redirect('avisos');
			}
		}
		
		public function tablaclientes($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra;        	
        	$data['rows'] = $this->avisos_model->getClientes($filter);
        	echo '('.json_encode($data).')';                
    	}
		function tablaa($cic=0,$fec=''){
			$this->load->model('avisos_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			if($fec!='') $filter['where']['fecavi =']=$fec;
			$data['rows'] = $this->avisos_model->getavisos($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function agregaravi(){
			$this->load->model('avisos_model');		
			$fec=$this->input->post('fec');
			$fol=$this->input->post('fol');
			$pre=$this->input->post('pre');
			$del=$this->input->post('del');
			$cic=$this->input->post('cic');
			if($fec!=''){	
				$this->avisos_model->agregaravi($fec,$fol,$pre,$del,$cic);			
				redirect('avisos');
			}
		} 
		function actualizaravi($id=0){
			$this->load->model('avisos_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$fol=$this->input->post('fol');
			$pre=$this->input->post('pre');
			$del=$this->input->post('del');
			$cic=$this->input->post('cic');
			if($id_post!=''){
				$return=$this->avisos_model->actualizaravi($id_post,$fec,$fol,$pre,$del,$cic); 			
				redirect('avisos');
			}
		}
    }
    
?>