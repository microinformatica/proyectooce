<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Larvicultura extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('produccion_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('produccion_model');
			//$data['result']=$this->produccion_model->Entregado();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('larvicultura/lista',$data);
        }
		function pdfrep() {
            $this->load->model('produccion_model');
			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			/*$data['zona']=$this->input->post('zonasel');
			$data['ciclo']=$this->input->post('ciclosel2');*/
			$this->load->view('larvicultura/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('larvicultura/listapdf', $data, true);  
			pdf ($html,'larvicultura/listapdf', true);
        	set_paper('letter');
        }
		function tablalar($hoy='',$ini='',$fin=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $hoy; 		
			$filter['where']['numpila >=']=$ini;
			$filter['where']['numpila <=']=$fin;
			$data['rows'] = $this->produccion_model->getLar($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsL($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablalarsie(){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//$filter['num'] = $hoy; 		
			$data['rows'] = $this->produccion_model->getLarsie($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsLsie($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function actualizarl($id=0){
			$this->load->model('produccion_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$can=$this->input->post('can');
			$vacio=$this->input->post('vacio');
			if($id_post!=''){
				$return=$this->produccion_model->actualizarl($id_post,$fec,$can,$vacio); 			
				redirect('larvicultura');
			}
		}
    }
    
?>