<?php 
    class Menu extends CI_Controller {
       public function __construct() {
        parent::__construct();	
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->library(array('session','user_agent'));
		//$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
		$this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		$this->perfilsel=$this->session->userdata('perfilsel');
		if($id_usuario==false)redirect('login');
		    
	   }
       public function index() {
            $this->load->model('menu_model');
			$data['result']=$this->menu_model->opcionesMenu($this->perfilsel);
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('menu_v',$data);
        }
	}
?>