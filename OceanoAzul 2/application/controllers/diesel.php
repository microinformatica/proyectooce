<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Diesel extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('diesel_model');         
        $this->load->library(array('ajaxsorter','session','enletras','libreria','user_agent'));		
        $this->load->helper(array('url','form','html','pdf'));
		$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		
		if($id_usuario==false)redirect('login'); 
	   }
        
        function index() {
            $this->load->model('diesel_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('diesel/lista',$data);
        }
		
		
		public function tablacomg($ciclo='',$por=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->diesel_model->getdieselG($filter,$ciclo,$por);
        	
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablacomgc($mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			 
			$data['rows'] = $this->diesel_model->getdieselGc($filter,$mes);
        	
        	echo '('.json_encode($data).')';                
    	}
		function reportedie( ) {
        	$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablacomg');
			$this->load->view('diesel/reportedie',$data); 
        }
   }
    
?>