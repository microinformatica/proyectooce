<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Facnot extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('facnot_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('facnot_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('facnot/lista',$data);
        }
		function pdffacnot() {
            $this->load->model('facnot_model');
			//$data['result']=$this->facnot_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablaFacNot');
			$tpfn= $this->input->post('tipfne');
			if($tpfn==1) $data['facnot'] ='Facturas'; 
			else $data['facnot'] ='Notas de Crédito';
			$html = $this->load->view('facnot/FacNot', $data, true);  
			pdf ($html,$data['facnot'], true);        	
        }
		function pdfrepfn() {
            $this->load->model('facnot_model');
			//$data['result']=$this->facnot_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablafn');
			$html = $this->load->view('facnot/clientesfacnot', $data, true);  
			pdf ($html,'Clientes', true);        	
        }
		function pdfdep() {
            $this->load->model('facnot_model');
			//$data['result']=$this->facnot_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tabladep');
			$html = $this->load->view('facnot/depositos', $data, true);  
			pdf ($html,'Depositos', true);        	
        }
		function pdffacsd() {
            $this->load->model('facnot_model');
			//$data['result']=$this->facnot_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['cliente'] = $this->input->post('razon');
			$data['tabla'] = $this->input->post('tablasind');
			$html = $this->load->view('facnot/movssindep', $data, true);  
			pdf ($html,'Facturas', true);        	
        }
		public function tablafn($gra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$data['rows'] = $this->facnot_model->getDatosfn($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablafacnot($tip=0,$sd=0,$fal=0,$dia=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($tip!=0) $filter['where']['tipfn =']=$tip;
			if($sd!=0) $filter['where']['facsd =']=$sd;
			if($fal!=0) $filter['where']['tipfn =']=6; else $filter['where']['tipfn !=']=6;
			//else { $filter['where']['tipfn <']=6;}
			if($dia!='') $filter['where']['fecfn =']=$dia;
			$data['rows'] = $this->facnot_model->getDatosfacnot($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablasal($cli=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//if($cli!=0) 
			$filter['where']['idcfn =']=$cli;
			$data['rows'] = $this->facnot_model->getSaldos($filter);
        	echo '('.json_encode($data).')';                
    	}
		function actualizarD($id=0){
		$this->load->helper('url');
		$this->load->model('facnot_model');
		$id_post=$this->input->post('id'); 
		$fac=$this->input->post('fac');
		$est=$this->input->post('est');
		$fac=$this->input->post('fac');
		$gra=$this->input->post('gra');
		if($id_post!=''){
			$return=$this->facnot_model->actualizarD($id_post,$fac,$est,$gra); 			
			redirect('facnot');
		}
		}
		function actualizarfn($id=0){
		$this->load->helper('url');
		$this->load->model('facnot_model');
		$id_post=$this->input->post('id'); 
		$rfc=$this->input->post('rfc');
		$ras=$this->input->post('ras');
		if($id_post!=''){
			$return=$this->facnot_model->actualizarfn($id_post,$rfc,$ras); 			
			redirect('facnot');
		}
		}
		
		
		function agregarfn(){
		$this->load->helper('url');
		$this->load->model('facnot_model');		
		$rfc=$this->input->post('rfc');
		$ras=$this->input->post('ras');
		if($rfc!=''){	
			$this->facnot_model->agregarfn($rfc,$ras);			
			redirect('facnot');
		}
		}
		function actualizarfacnot($id=0){
		$this->load->helper('url');
		$this->load->model('facnot_model');
		$id_post=$this->input->post('id'); 
		$idc=$this->input->post('idc');
		$num=$this->input->post('num');
		$fec=$this->input->post('fec');
		$imp=$this->input->post('imp');
		$tippd=$this->input->post('tippd');
		$tipc=$this->input->post('tipc');
		$tipfn=$this->input->post('tipfn');
		$est=$this->input->post('est');
		$obs=$this->input->post('obs');
		$efe=$this->input->post('efe');
		$apl=$this->input->post('apl');
		$fsd=$this->input->post('fsd');
		$cic=$this->input->post('cic');
		$ban=$this->input->post('ban');
		if($id_post!=''){
			$return=$this->facnot_model->actualizarfacnot($id_post,$idc,$num,$fec,$imp,$tippd,$tipc,$tipfn,$est,$obs,$efe,$apl,$fsd,$cic,$ban); 			
			redirect('facnot');
		}
		}
		
		function agregarfacnot(){
		$this->load->helper('url');
		$this->load->model('facnot_model');		
		$idc=$this->input->post('idc');
		$num=$this->input->post('num');
		$fec=$this->input->post('fec');
		$imp=$this->input->post('imp');
		$tippd=$this->input->post('tippd');
		$tipc=$this->input->post('tipc');
		$tipfn=$this->input->post('tipfn');
		$est=$this->input->post('est');
		$obs=$this->input->post('obs');
		$efe=$this->input->post('efe');
		$apl=$this->input->post('apl');
		$fsd=$this->input->post('fsd');
		$cic=$this->input->post('cic');
		$ban=$this->input->post('ban');
		if($idc!=''){	
			$this->facnot_model->agregarfacnot($idc,$num,$fec,$imp,$tippd,$tipc,$tipfn,$est,$obs,$efe,$apl,$fsd,$cic,$ban);			
			redirect('facnot');
		}
		}
		
		function borrarabo($id=0){
			$this->load->helper('url');
			$this->load->model('facnot_model');
			$id_post=$this->input->post('id');
			if($id_post!=''){
				$return=$this->facnot_model->quitarabo($id_post); 			
				redirect('facnot');
			}
		}
		
		function buscarrfc(){
			$rfc = $this->input->post('rfc');
			$data =$this->facnot_model->historyrfc($rfc);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('rfc'=>$data->rfcfn,'ras'=>$data->rasfn));
			}else{
				echo json_encode(array('rfc'=>'','ras'=>''));
			}
						
		}	
		function buscarfn(){
			$data=array();
			$data='';
			$lfn = $this->input->post('fn');
			$data =$this->facnot_model->historyfn($lfn);
			//$size=sizeof($data);
			if($data!=''){
				//si lo encuentra deja los datos
				//echo json_encode(array('numfn'=>($data->ultimo+1)));
				echo json_encode(array('numfn'=>$data->ultimo+1));
			}else{
				echo json_encode(array('numfn'=>''));
			}
						
		}
		
		public function tabdep($desde='',$hasta='',$evo=0,$sinf=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($desde!='')
				$filter['where']['fecha >=']=$desde;
			if($hasta!='')
				$filter['where']['fecha <=']=$hasta;
			if($evo>0)
				$filter['where']['evo !=']=$evo;
			if($sinf>0)
				$filter['where']['factura =']='0';
			$data['rows'] = $this->facnot_model->getDep($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		public function combob(){
			//$filter['aliag']=$this->input->post('cmbAli');
			$filter['where']['idcfn =']=$this->input->post('idctefn');
			$filter['where']['facsd =']=1;
			$data = $this->facnot_model->getElementsb($filter); 
			echo '('.json_encode($data).')';              
    	}				
    }
    
?>