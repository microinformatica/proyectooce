<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Tallas extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('tallas_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('tallas_model');
			//$data['result']=$this->tallas_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('tallas/lista',$data);
        }
		
		public function tabla($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->tallas_model->gettallas($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		function pdfrepvta() {
            $this->load->model('tallas_model');
			$dia=date('d-m-Y');
			$data['dia'] = date('d-m-Y');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('tallas/lista',$data);
			$data['tablavtas'] = $this->input->post('tablav');
			$html = $this->load->view('tallas/listapdf', $data, true);  
			pdf ($html,'gral_tallas_'.$dia, true);   
        }
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('tallas_model');
		$id_post=$this->input->post('id'); //id
		$nom=$this->input->post('nom');
		$dia=$this->input->post('dia');
		$pmen=$this->input->post('pmen'); if($pmen=='') $pmen=0;
		$pmay=$this->input->post('pmay'); if($pmay=='') $pmay=0;
		$gpo=$this->input->post('gpo');
		$gpoid=$this->input->post('gpoid');
		$gpogrl=$this->input->post('gpogrl');
		if($id_post!=''){
			$return=$this->tallas_model->actualizar($id_post,$nom,$dia,$pmen,$pmay,$gpo,$gpoid,$gpogrl); 
			redirect('tallas');
		}		
		}
		
		function agregar(){
		$this->load->helper('url');
		$this->load->model('almacenes_model');
		$nom=$this->input->post('nom');
		$dia=$this->input->post('dia');
		$pmen=$this->input->post('pmen'); if($pmen=='') $pmen=0;
		$pmay=$this->input->post('pmay'); if($pmay=='') $pmay=0;
		$gpo=$this->input->post('gpo');
		$gpoid=$this->input->post('gpoid');
		$gpogrl=$this->input->post('gpogrl');
		if($nom!=''){	
			$this->tallas_model->agregar($nom,$dia,$pmen,$pmay,$gpo,$gpoid,$gpogrl);
			redirect('tallas');
		}
		}
		
		function buscargpo(){
			$gpo = $this->input->post('gpo');
			$data =$this->tallas_model->history($gpo);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('gpogrl'=>$data->gpogrl,'grupo'=>$data->grupo));
			}
						
		}
    }
    
?>