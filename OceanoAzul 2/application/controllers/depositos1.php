<?php 
    class Depositos extends CI_Controller {
        public function __construct() {
        parent::__construct();	
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');  
		$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        function index() {
        	//$fi=date("Y-m-d"); $ff=date("Y-m-d"); 		//$fi=suma_fechas($fi, -7);
        	$fi=date("Y-m-d");$ff= date("Y-m-d");
					 
	   		$this->load->model('depositos_model');
			$data['result']=$this->depositos_model->verActivos($fi,$ff);
			$data['dpDesde']=$fi;
			$data['dpHasta']=$ff;
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('depositos/lista',$data);
        }
		function suma_fechas($fecha,$ndias){
      		if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha))           
              list($dia,$mes,$ao)=explode("/", $fecha);
      		if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha))
              list($dia,$mes,$ao)=explode("-",$fecha);
        	$nueva = mktime(0,0,0, $mes,$dia,$ao) + $ndias * 24 * 60 * 60;
	        $nuevafecha=date("d-m-Y",$nueva);
    	  return ($nuevafecha);  
		}
		function buscar(){
			$fi=$this->input->post('dpDesde');
			$ff=$this->input->post('dpHasta');
			$nom=$this->input->post('busqueda');	
			$this->load->helper('url');
			$this->load->model('depositos_model');
			$datos['result']=$this->depositos_model->buscar_activos($nom,$fi,$ff);		
			$datos['busqueda']=$nom;
			$datos['dpDesde']=$fi;
			$datos['dpHasta']=$ff;		
			$datos['usuario']=$this->usuario;
			$datos['perfil']=$this->perfil;
			$this->load->view('depositos/buscar',$datos);
		}
		function agregar(){
		$this->load->helper('url');
		$this->load->model('depositos_model');
		$fec=$this->input->post('txtFecha');
		$dolar=$this->input->post('txtDolar');
		$tc=$this->input->post('txtTC');
		$cta=$this->input->post('txtCta');
		$obs=$this->input->post('txtobs');
		$pesos=$this->input->post('txtPesos');
		$cliente=$this->input->post('txtNumero');
		$ciclo=$this->input->post('lstciclo');
		//if($nombre!='' && $dom!=''){
		if($cliente!='' && $fec!=''){	
			$this->depositos_model->agregar($fec,$dolar,$tc,$cta,$obs,$pesos,$cliente,$ciclo);
			redirect('depositos');
		}
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('depositos/agregar',$datos);
		}
		
		function actualizar($txtNumero=0){
		$this->load->helper('url');
		$this->load->model('depositos_model');
		$id_post=$this->input->post('txtND'); //id
		$fec=$this->input->post('txtFecha');
		$dolar=$this->input->post('txtDolar');
		$tc=$this->input->post('txtTC');
		$cta=$this->input->post('txtCta');
		$obs=$this->input->post('txtobs');
		$pesos=$this->input->post('txtPesos');
		$cliente=$this->input->post('txtNumero');
		$ciclo=$this->input->post('lstciclo');
		if($id_post!=''){
			$return=$this->depositos_model->actualizar($id_post,$fec,$dolar,$tc,$cta,$obs,$pesos,$cliente,$ciclo); 
			redirect('depositos');
		}
		if($txtNumero<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->depositos_model->getDeposito($txtNumero);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('depositos/actualizar',$datos);
		}
		
		function datosCli(){
			$this->load->helper('url');
			$this->load->model('depositos_model');
			$usuario=$this->input->post('id');
			$row=$this->depositos_model->getClienteD($usuario);
			$size=sizeof($row);
			$error_msg="";
			if($size>0){
				$status=1;
			}else{
				$status=-1;
				$error_msg="No Existe Usuario.";
			}
			//echo json_encode($row);
			echo json_encode(array('razon'=>$row->Razon,'zona'=>$row->Zona,'numero'=>$row->Numero));
		}
		
    }
    
?>