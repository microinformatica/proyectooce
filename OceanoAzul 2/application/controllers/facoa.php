<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Facoa extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('facoa_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('facoa_model');
			//$data['result']=$this->facoa_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('facoa/lista',$data);
        }
		function pdfrep() {
            $this->load->model('facoa_model');
			//$data['result']=$this->facoa_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('facoa/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('facoa/listapdf', $data, true);  
			pdf ($html,'facoa/listapdf', true);
        	set_paper('letter');
        }
		public function tabla($cic=0,$gra=0,$mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($extra==0){$filter['num'] = $extra;}
			if($gra>0){$filter['where']['idgag =']=$gra;} 
			if($mes>0){$filter['where']['month(fecpag) =']=$mes;}
			$data['rows'] = $this->facoa_model->getfacoa($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}
		public function tablafle($cic=0,$gra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($extra==0){$filter['num'] = $extra;}
			if($gra>0){$filter['where']['idgag =']=$gra;} 
			$data['rows'] = $this->facoa_model->getfle($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaali($cic=0,$mes=0,$gra=0,$ali=0,$med=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($extra==0){$filter['num'] = $extra;}
			//$filter['where']['month(fecpag) =']=$mes;
			if($mes>0){$filter['where']['month(fecpag) =']=$mes;} 
			if($gra>0){$filter['where']['idgag =']=$gra;} 
			if($ali!='0'){$filter['where']['aliag =']=$ali;}
			if($med!='0'){$filter['where']['medag =']=$med;}
			$data['rows'] = $this->facoa_model->getAli($filter,$cic,$mes,$ali,$med);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaalig($cic=0,$mes=0,$ali=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($mes>0){$filter['where']['month(FecE) =']=$mes;} 
			if($ali!='0'){$filter['where']['CBE =']=$ali;}			
			$data['rows'] = $this->facoa_model->getAlig($filter,$cic,$mes,$ali);
        	echo '('.json_encode($data).')';                
    	}
		function agregar(){
			$this->load->model('facoa_model');		
			$feca=$this->input->post('feca');
			$gra=$this->input->post('gra');
			$fol=$this->input->post('fol');
			$des=$this->input->post('des');
			$med=$this->input->post('med');
			$ton=$this->input->post('ton');
			$tc=$this->input->post('tc');
			$fecp=$this->input->post('fecp');
			$obs=$this->input->post('obs');
			$pro=$this->input->post('pro');
			$cic=$this->input->post('cic');
			$ip=$this->input->post('ip');
			$if=$this->input->post('if');
			$tcp=$this->input->post('tcp');
			$tip=$this->input->post('tip');
			$fecf=$this->input->post('fecf');
			$facf=$this->input->post('facf');
			$prof=$this->input->post('prof');
			$subf=$this->input->post('subf');
			$npag=$this->input->post('npag');
			$ests=$this->input->post('ests');
			if($gra!=''){	
				$this->facoa_model->agregar($feca,$gra,$fol,$des,$med,$ton,$tc,$fecp,$obs,$pro,$cic,$ip,$if,$tcp,$tip,$fecf,$facf,$prof,$subf,$npag,$ests);			
				redirect('facoa');
			}
		} 
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('facoa_model');
		$id_post=$this->input->post('id'); //id
		$feca=$this->input->post('feca');
		$gra=$this->input->post('gra');
		$fol=$this->input->post('fol');
		$des=$this->input->post('des');
		$med=$this->input->post('med');
		$ton=$this->input->post('ton');
		$tc=$this->input->post('tc');
		$fecp=$this->input->post('fecp');
		$obs=$this->input->post('obs');
		$pro=$this->input->post('pro');
		$cic=$this->input->post('cic');
		$ip=$this->input->post('ip');
		$if=$this->input->post('if');
		$tcp=$this->input->post('tcp');
		$tip=$this->input->post('tip');
		$fecf=$this->input->post('fecf');
		$facf=$this->input->post('facf');
		$prof=$this->input->post('prof');
		$subf=$this->input->post('subf');
		$npag=$this->input->post('npag');
		$ests=$this->input->post('ests');
		if($id_post!=''){
			$return=$this->facoa_model->actualizar($id_post,$feca,$gra,$fol,$des,$med,$ton,$tc,$fecp,$obs,$pro,$cic,$ip,$if,$tcp,$tip,$fecf,$facf,$prof,$subf,$npag,$ests); 
			redirect('facoa');
		}		
		}
		
		function borrar($id=0){
		$this->load->helper('url');
		$this->load->model('facoa_model');
		$id_post=$this->input->post('id');
		$cic=$this->input->post('cic');
		if($id_post!=''){
			$return=$this->facoa_model->borrar($id_post,$cic); 			
			redirect('facoa');
		}
		}
		
		public function combob(){
			//$filter['aliag']=$this->input->post('cmbAli');
			$filter['where']['aliag =']=$this->input->post('aliag');
			$data = $this->facoa_model->getElementsb($filter); 
			echo '('.json_encode($data).')';              
    	}		
    }
    
?>