<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Produccion extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('produccion_model');
		$this->load->library(array('ajaxsorter','session','libreria'));	         	
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('produccion_model');
			//$data['result']=$this->produccion_model->Entregado();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$rem['result'] = $this->produccion_model->ultimocodigo();
			$txtRemision=0;$urr=0;
			foreach ($rem['result'] as $row): 
				$txtRemision=$row->ultimo+1;										
				$urr=$row->ultimoR+1;
			endforeach;
			$data['txtRemision']=$txtRemision;
			$data['urr']=$urr;

			$rem['result'] = $this->produccion_model->ultimocodigo2();
			$txtRemision2=0;$urr2=0;
			foreach ($rem['result'] as $row): 
				$txtRemision2=$row->ultimo+1;										
				$urr2=$row->ultimoR+1;
			endforeach;
			$data['txtRemision2']=$txtRemision2;
			$data['urr2']=$urr2;
			$this->load->view('produccion/lista',$data);
        }
		
		function pdfrep() {
            $this->load->model('produccion_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('produccion/lista',$data);
			$data['tablar1'] = $this->input->post('tablar1');
			$data['tablar2'] = $this->input->post('tablar2');
			$data['tablar3'] = $this->input->post('tablar3');
			$data['tablar4'] = $this->input->post('tablar4');
			$data['tablar5'] = $this->input->post('tablar5');
			$data['tablar6'] = $this->input->post('tablar6');
			$data['tablar7'] = $this->input->post('tablar7');
			$data['dia'] = $this->input->post('diaA');
			$html = $this->load->view('produccion/listapdf', $data, true);
			pdf ($html,'produccion_existencia_'.$data['dia'], true);
        	//set_paper('letter');
        }
		function pdfrepcod() {
            $this->load->model('produccion_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$tc=$this->input->post('tc');
			if($tc==1){
			$data['tabla'] = $this->input->post('tablacod1');
			$data['pila'] = $this->input->post('cmbPila');
			}else{
			$data['tabla'] = $this->input->post('tablacod2');
			$data['pila'] = $this->input->post('cmbPila2');	
			}
			$html = $this->load->view('produccion/Codigos', $data, true);
			pdf ($html,'Cod_Pil_'.$data['pila'], true);
        	//set_paper('letter');
        }
		function pdfrept() {
            $this->load->model('produccion_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('produccion/lista',$data);
			$data['tablar1'] = $this->input->post('tablar11');
			$data['tablar2'] = $this->input->post('tablar22');
			$data['tablar3'] = $this->input->post('tablar33');
			//$data['tablar4'] = $this->input->post('tablar4');
			$data['tablar5'] = $this->input->post('tablar55');
			$data['dia'] = $this->input->post('diat');
			//$data['tablar6'] = $this->input->post('tablar6');
			//$data['tablar7'] = $this->input->post('tablar7');
			$html = $this->load->view('produccion/listapdft', $data, true);
			//pdf ($html,'tratamientos', true);
			pdf ($html,'Tra_Dia_'.date('d-m-Y', strtotime('+1 day')), true);
        	//set_paper('letter');
        }
				
		function tablacod($npil='0'){        
        	$filter = $this->ajaxsorter->filter($this->input);
			if ($npil!='0')$filter['where']['nom =']=$npil;  
			$data['rows'] = $this->produccion_model->getCod($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsCod($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablacod2($npil='0'){        
        	$filter = $this->ajaxsorter->filter($this->input);
			if ($npil!='0')$filter['where']['nom =']=$npil;   
			$data['rows'] = $this->produccion_model->getCod2($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsCod2($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablaras($hoy='',$ini='',$fin=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $hoy; 		
			$filter['where']['numpila >=']=$ini;
			$filter['where']['numpila <=']=$fin;
			$data['rows'] = $this->produccion_model->getRas($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsR($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablalar($hoy='',$ini='',$fin=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $hoy; 		
			$filter['where']['numpila >=']=$ini;
			$filter['where']['numpila <=']=$fin;
			$data['rows'] = $this->produccion_model->getLar($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsL($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablalarsie(){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//$filter['num'] = $hoy; 		
			$data['rows'] = $this->produccion_model->getLarsie($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsLsie($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablarasA($hoy=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $hoy; 		
			$data['rows'] = $this->produccion_model->getRasA($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsRA($filter);
        	echo '('.json_encode($data).')'; 
    	}	
		function tablarasAT($hoy=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $hoy; 		
			$data['rows'] = $this->produccion_model->getRasAT($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsRAT($filter);
        	echo '('.json_encode($data).')'; 
    	}	
		function historial(){
			$this->load->model('produccion_model');
			$return=$this->produccion_model->history(); 			
			//if($return==2)	redirect('produccion');
			echo json_encode($return);
		}
		function actualizar($id=0){
			$this->load->model('produccion_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$pl=$this->input->post('pl');
			$can=$this->input->post('can');
			$vacio=$this->input->post('vacio');
			$sal=$this->input->post('sal');
			$ori=$this->input->post('ori');
			if($id_post!=''){
				$return=$this->produccion_model->actualizar($id_post,$fec,$pl,$can,$vacio,$sal,$ori); 			
				redirect('produccion');
			}
		}	
		function actualizarl($id=0){
			$this->load->model('produccion_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$can=$this->input->post('can');
			$vacio=$this->input->post('vacio');
			if($id_post!=''){
				$return=$this->produccion_model->actualizarl($id_post,$fec,$can,$vacio); 			
				redirect('produccion');
			}
		}
		function actualizarcod($id=0){
			$this->load->model('produccion_model');
			$id_post=$this->input->post('id');
			$pil=$this->input->post('pil'); 
			$fec=$this->input->post('fec');
			$pls=$this->input->post('pls');
			$est=$this->input->post('est');
			$p1=$this->input->post('p1');
			$p2=$this->input->post('p2');
			$p3=$this->input->post('p3');
			$p4=$this->input->post('p4');
			$nau=$this->input->post('nau');
			$mad=$this->input->post('mad');
			$ori=$this->input->post('ori');
			$cod=$this->input->post('cod');
			$sala=$this->input->post('sala');
			$tabla=$this->input->post('tabla');
			if($id_post!=''){
				$return=$this->produccion_model->actualizarcod($id_post,$pil,$fec,$pls,$est,$p1,$p2,$p3,$p4,$nau,$mad,$ori,$cod,$sala,$tabla); 			
				redirect('produccion');
			}
		}
		function agregarcod(){
		$this->load->helper('url');
		$this->load->model('produccion_model');		
		$pil=$this->input->post('pil');
		$fec=$this->input->post('fec');
		$pls=$this->input->post('pls');
		$est=$this->input->post('est');
		$p1=$this->input->post('p1');
		$p2=$this->input->post('p2');
		$p3=$this->input->post('p3');
		$p4=$this->input->post('p4');
		$nau=$this->input->post('nau');
		$mad=$this->input->post('mad');
		$ori=$this->input->post('ori');
		$cod=$this->input->post('cod');
		$sala=$this->input->post('sala');
		$tabla=$this->input->post('tabla');
		if($fec!=''){	
			$this->produccion_model->agregarcod($pil,$fec,$pls,$est,$p1,$p2,$p3,$p4,$nau,$mad,$ori,$cod,$sala,$tabla);			
			redirect('produccion');
		}
		}
		public function combo(){
			$filter['nom']=$this->input->post('nom');           
       		$data = $this->produccion_model->getElements($filter); 
			echo '('.json_encode($data).')';              
    	}
    }
    
?>