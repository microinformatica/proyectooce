<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Raceways extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('produccion_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('produccion_model');
			//$data['result']=$this->produccion_model->Entregado();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('raceways/lista',$data);
        }
		function pdfrep() {
            $this->load->model('produccion_model');
			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('produccion/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('raceways/listapdf', $data, true);  
			pdf ($html,'raceways/listapdf', true);
        	set_paper('letter');
        }
				
		function tablaras($hoy='',$ini='',$fin=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $hoy; 		
			$filter['where']['numpila >=']=$ini;
			$filter['where']['numpila <=']=$fin;
			$data['rows'] = $this->produccion_model->getRas($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsR($filter);
        	echo '('.json_encode($data).')'; 
    	}
		
		function tablarasA($hoy=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $hoy; 		
			$data['rows'] = $this->produccion_model->getRasA($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsRA($filter);
        	echo '('.json_encode($data).')'; 
    	}	
		function tablarasAT($hoy=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $hoy; 		
			$data['rows'] = $this->produccion_model->getRasAT($filter);
        	$data['num_rows'] = $this->produccion_model->getNumRowsRAT($filter);
        	echo '('.json_encode($data).')'; 
    	}	
		function historial(){
			$this->load->model('produccion_model');
			$return=$this->produccion_model->history(); 			
			//if($return==2)	redirect('produccion');
			echo json_encode($return);
		}
		function actualizar($id=0){
			$this->load->model('produccion_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$pl=$this->input->post('pl');
			$can=$this->input->post('can');
			$vacio=$this->input->post('vacio');
			if($id_post!=''){
				$return=$this->produccion_model->actualizar($id_post,$fec,$pl,$can,$vacio); 			
				redirect('raceways');
			}
		}	
	}
?>