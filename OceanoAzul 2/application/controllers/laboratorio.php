<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Laboratorio extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('laboratorio_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('laboratorio_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('laboratorio/lista',$data);
        }
		//TIPOS
		public function tablatipos(){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			$data['rows'] = $this->laboratorio_model->getTipos($filter);
        	$data['num_rows'] = $this->laboratorio_model->getNumRowsT($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function agregare(){
		$this->load->helper('url');
		$this->load->model('laboratorio_model');		
		$tip=$this->input->post('tip');
		if($tip!=''){	
			$this->laboratorio_model->agregare($tip);			
			redirect('laboratorio');
		}
		}
		function actualizare($id=0){
		$this->load->helper('url');
		$this->load->model('laboratorio_model');
		$id_post=$this->input->post('id'); 
		$tip=$this->input->post('tip');
		if($id_post!=''){
			$return=$this->laboratorio_model->actualizare($id_post,$tip); 			
			redirect('laboratorio');
		}
		}
		function borrare($id=0){
		$this->load->helper('url');
		$this->load->model('laboratorio_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->laboratorio_model->borrare($id_post); 			
			redirect('laboratorio');
		}
		}
		//VALORES
		public function tablaval($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			$filter['where']['idfis =']=$extra;
			$data['rows'] = $this->laboratorio_model->getValores($filter);
        	$data['num_rows'] = $this->laboratorio_model->getNumRowsV($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function actualizaDet($id=0){
		$this->load->helper('url');
		$this->load->model('laboratorio_model');
		$id_post=$this->input->post('id');
		$val=$this->input->post('val'); 
		$des=$this->input->post('des');
		$nom=$this->input->post('nom');
		if($id_post!=''){
			$return=$this->laboratorio_model->actualizaDet($id_post,$val,$des,$nom); 			
			redirect('laboratorio');
		}
		}
		function agregaDet(){
		$this->load->helper('url');
		$this->load->model('laboratorio_model');		
		$val=$this->input->post('val'); 
		$des=$this->input->post('des');
		$fis=$this->input->post('fis');
		$nom=$this->input->post('nom');
		$usuario=$this->usuario;
		if($val!=''){	
			$this->laboratorio_model->agregaDet($val,$des,$fis,$nom);			
			redirect('laboratorio');
		}
		}
		function quitarDet($id=0){
		$this->load->helper('url');
		$this->load->model('laboratorio_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->laboratorio_model->quitarDet($id_post); 			
			redirect('laboratorio');
		}
		}
		//ANALISIS DIARIO
		public function tablafres($extra=0,$pila=0,$critica=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['fec =']=$extra;
			if($pila>0) $filter['where']['pila =']=$pila;
			if($critica>0) $filter['where']['critica =']=$critica;
			$data['rows'] = $this->laboratorio_model->getFresco($filter);
        	$data['num_rows'] = $this->laboratorio_model->getNumRowsF($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function pdfrepdia() {
            $this->load->model('laboratorio_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabladps');
			$mes = $this->input->post('critica');
			switch ($mes){
					case '0': $mes='[Todas Las Pilas]';break;
					case '1': $mes='[Pilas Criticas]';break;
					case '2': $mes='[Pilas Buenas]';break;
				}
			$data['estatus'] = $mes;
			//$data['dia'] = $this->input->post('pfecd');
			$tip = $this->input->post('tip');
			$data['fecha'] = $this->input->post('feca');
			if($tip==1){
				$html = $this->load->view('laboratorio/listapdfdia', $data, true);$tipo='Gral_';
			}else{  
				$html = $this->load->view('laboratorio/listapdfdiamod', $data, true);$tipo='Pilas_';  
			}
			pdf ($html,'Ana_Fresco_'.$tipo.$data['fecha'], true);
        }
		function agregara(){
		$this->load->helper('url');
		$this->load->model('laboratorio_model');		
		$fec=$this->input->post('fec');
		$pl=$this->input->post('pl');
		$pil=$this->input->post('pil');
		$org=$this->input->post('org');
		$bra=$this->input->post('bra');
		$obsb=$this->input->post('obsb');
		$inte=$this->input->post('inte');
		$exo=$this->input->post('exo');
		$obse=$this->input->post('obse');
		$hep1=$this->input->post('hep1');
		$hep2=$this->input->post('hep2');
		$hep3=$this->input->post('hep3');
		$obs=$this->input->post('obs');
		if($fec!=''){	
			$this->laboratorio_model->agregara($fec,$pl,$pil,$org,$bra,$obsb,$inte,$exo,$obse,$hep1,$hep2,$hep3,$obs);			
			redirect('laboratorio');
		}
		}
		function actualizara($id=0){
		$this->load->helper('url');
		$this->load->model('laboratorio_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$pl=$this->input->post('pl');
		$pil=$this->input->post('pil');
		$org=$this->input->post('org');
		$bra=$this->input->post('bra');
		$obsb=$this->input->post('obsb');
		$inte=$this->input->post('inte');
		$exo=$this->input->post('exo');
		$obse=$this->input->post('obse');
		$hep1=$this->input->post('hep1');
		$hep2=$this->input->post('hep2');
		$hep3=$this->input->post('hep3');
		$obs=$this->input->post('obs');
		if($id_post!=''){
			$return=$this->laboratorio_model->actualizara($id_post,$fec,$pl,$pil,$org,$bra,$obsb,$inte,$exo,$obse,$hep1,$hep2,$hep3,$obs); 			
			redirect('laboratorio');
		}
		}
		function borrara($id=0){
		$this->load->helper('url');
		$this->load->model('laboratorio_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->laboratorio_model->borrara($id_post); 			
			redirect('laboratorio');
		}
		}
		public function criticas($pila=0,$feca='',$valor='',$solodep=0){
			$this->load->model('laboratorio_model');
			$pila=$this->input->post('pila');//$cliente=289;
			$feca=$this->input->post('feca');//$tabla='r13';
			$valor=$this->input->post('valor');//$rod='Remisiones';						
			$solodep=$this->input->post('solodep');//$rod='Remisiones';
			$data= $this->laboratorio_model->datoscriticos($pila,$feca,$valor,$solodep);
    	}
		
		
		
		public function obtenerdatos($extra=0){
			//$this->load->view('laboratorio/lista',$data);	
            $filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			if($usuario=='Joaquin Domínguez') $usu=1; 
			if($usuario=='Miguel Mendoza') $usu=2;
			if($usuario!='Joaquin Domínguez' && $usuario!='Miguel Mendoza') $usu=3;
			//$eqpnum = $this->input->post('id');
			$filter['where']['ubi =']=$extra;
			$data['rows'] = $this->laboratorio_model->getEquiposSele($filter, $usu,$extra);
        	//$data['num_rows'] = $this->laboratorio_model->getNumRowsSele($filter, $usu);        	
        	echo '('.json_encode($data).')';
        }
		function pdfrepdetmes( ) {
            $this->load->model('laboratorio_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('laboratorio/lista',$data);
			$data['cic'] = '['.$this->input->post('cicd').']';
			$mes = $this->input->post('mesd');
			switch ($mes){
					case '1': $mes='[Enero]';break;
					case '2': $mes='[Febrero]';break;
					case '3': $mes='[Marzo]';break;
					case '4': $mes='[Abril]';break;
					case '5': $mes='[Mayo]';break;
					case '6': $mes='[Junio]';break;
					case '7': $mes='[Julio]';break;
					case '8': $mes='[Agosto]';break;
					case '9': $mes='[Septiembre]';break;
					case '10': $mes='[Octubre]';break;
					case '11': $mes='[Noviembre]';break;
					case '12': $mes='[Diciembre]';break;
				}
			$data['mes'] = $mes;
			$tip = $this->input->post('tipd');
			switch ($tip){
					case '0': $tip='[Todos] ';break;
					case '1': $tip='[Bomba] ';break;
					case '2': $tip='[Filtro] ';break;
					case '3': $tip='[Ozono] ';break;
					case '4': $tip='[U.V.] ';break;
					case '5': $tip='[Intercambiador] ';break;
					case '6': $tip='[Blower] ';break;
					case '7': $tip='[Calentador] ';break;
					case '7': $tip='[Chiller] ';break;
				}	
			$data['tip'] =$tip;
			
			$data['tabla'] = $this->input->post('tabla');
			$html = $this->load->view('laboratorio/listapdfimp', $data, true);  
			pdf ($html,'laboratorio/EquipoMes', true);
        }
		function pdfrepeqpdet( ) {
            $this->load->model('laboratorio_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('laboratorio/lista',$data);
			$data['ubica'] = $this->input->post('eqpubi');
			$data['tabla'] = $this->input->post('tabla');
			$html = $this->load->view('laboratorio/listapdfeqpimp', $data, true);  
			pdf ($html,'laboratorio/EquipoUbicacion', true);
        }
		
		
		
		function tablatecmes($ciclo=0,$mes=0,$tip=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($ciclo>0) $filter['where']['year(feca) =']=$ciclo;
			if($mes>0) $filter['where']['month(feca) =']=$mes;
			if($tip>0) $filter['where']['tipo =']=$tip;
			$data['rows'] = $this->laboratorio_model->getTecnicosGral($filter,$tip);
        	$data['num_rows'] = $this->laboratorio_model->getNumRowsTGral($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function historial(){
			//$this->load->model('laboratorio_model');			
			$nadr = $this->input->post('nadr');
			$estr = $this->input->post('estr');
			$return=$this->laboratorio_model->historial($nadr,$estr); 			
			//if($return==2)	redirect('produccion');
			echo json_encode($return);
		}
    }
    
?>