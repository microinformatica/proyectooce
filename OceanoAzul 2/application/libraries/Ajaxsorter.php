<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajaxsorter {

    public function filter($input){
        //FALTA CONVERTIR ESTA FUNCION EN UNA LIBRERIA PARA CODEIGNITER
        //PARA BRINDAR FUNCIONALIDAD CON LA INTEGRACION DE LA LIBRERIA UNICAMENTE
        $data['order'] = $input->post('order');//VALOR DE ORDENACION (campo tipo)      
        $data['offset'] = $input->post('pa');//PAGINA SOLICITADA     
        $data['limit'] =$input->post('max_pp');//MAXIMO POR PAGINA        
        $data['values'] =  explode(',',$input->post('value'));//ARREGLO DE VALORES 
        $data['fields'] =   explode(',',$input->post('field'));//ARREGLO DE CAMPOS  
        $data['equ'] =   explode(',',$input->post('equ'));//ARREGLO DE CAMPOS          
        //FORMACION DE LAS CONDICIONES A PARTIR DE LOS VALORES QUE LLEGAN		
        $data['where'] =array();
        $dat=array();
        
        for($it=0;$it<(count($data['values']));$it++){ 
            if($data['values'][$it]!=""){  
                $data['where'][$data['fields'][$it]. " ".$data['equ'][$it]]= (($data['equ'][$it]==="like")?'%':'').$data['values'][$it].(($data['equ'][$it]==="like")?'%':'');				
            }                            
        }
        //SE CALCULA EL OFFSET
        if($data['offset']>0 and $data['limit']>0) $data['offset'] =$data['offset']*$data['limit'];
        return $data;
    }
}

/* End of file Someclass.php */