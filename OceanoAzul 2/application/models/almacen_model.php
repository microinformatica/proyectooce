<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Almacen_model extends CI_Model {
        public $id="NS";	//public $id="NS";        
		public $fr="FechaR";  //public $fr="FechaR";
		public $can="CantidadS";  //public $can="CantidadS";
		public $uni="UnidadS";   //public $uni="UnidadS";
		public $des="DescripcionS"; //public $des="DescripcionS";
		public $cans="CanS";   //public $cans="CanS";
		public $obs="Obs";  //public $obs="Obs";
        public $tablasol="solicitudes";  //public $tablasol="solicitudes";
		public $tablareq="requisicion";  //public $tablareq="requisicion";
			
		public $idr="Nr";
		public $reqn="Requisicion";
		public $req="NRS";
		public $fs="FechaS";        
		
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		function getalmacendia($filter){
			//Select Requisicion,NS,FechaS,CantidadS,UnidadS,DescripcionS,FechaR,CanS,Obs,FechaSS 
			//FROM solicitudes 
			//INNER JOIN requisicion ON Nr=NRS Where (FechaSS  >= '$fi' AND FechaSS <= '$ff' and CanS >= 0 )  
			//order by FechaS DESC, Requisicion
			$this->db->select('Requisicion,NS,FechaS,CantidadS,UnidadS,DescripcionS,FechaR,CanS,Obs,FechaSS,DATEDIFF( CURDATE(),FechaS ) AS dt',NULL,FALSE);
			$this->db->join('requisicion', 'Nr=NRS','inner');
			$this->db->where('CanS >=',0);
			$this->db->order_by('FechaS');
			$this->db->order_by('Requisicion');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablasol);
			$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';$cont=0;$feci="";
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				
				$fecha=$row->FechaS;$des=$row->DescripcionS;$obs=$row->Obs;$cancelar=$row->CanS;
				$row->des1=$row->DescripcionS;
				$row->FechaS=substr($row->FechaS, 0, 10);
				$row->FechaS = date("d-m-Y",strtotime($row->FechaS));
				$row->hora=substr ($fecha, 11, 8);
				$inc="";$obs1="";
				if($cancelar==1){ $inc=" ** INCOMPLETO **";}
				if($obs!=""){ $obs1=" -( ".$obs." )-";}
				$row->DescripcionS=$des.$inc.$obs1; 
				$row->totp=($cont+=1); 
				$fec=$row->FechaS;
				//if($feci!=$fec){	$row->FechaS1=$row->FechaS; $feci=$row->FechaS;	}else{ $row->FechaS1="";}  		
				if($feci!=$fec){	$row->FechaS1=$row->FechaS.' ['.$row->dt.']'; $feci=$row->FechaS;	}else{ $row->FechaS1="";}		
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		function getNumRowsdia($filter){
			$this->db->join('requisicion', 'Nr=NRS','inner');
			$this->db->where('CanS >=',0);
			$this->db->order_by('FechaS','DESC');
			$this->db->order_by('Requisicion');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablasol);
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function actualizard($id,$can,$uni,$des,$obs,$cans){
	 		$hoy=date("y-m-d H:i:s");
			$data=array($this->can=>$cer,$this->fr=>$hoy,$this->can=>$can,$this->uni=>$uni,$this->des=>$des,$this->cans=>$cans,$this->obs=>$obs);
			$this->db->where($this->id,$id);
			$this->db->update($this->tablasol,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function borrard($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tablasol);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function getalmacenreq($filter){
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablasol);
			$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';$cont=0;$feci="";$fec="";
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				$fecha=$row->FechaS;$des=$row->DescripcionS;$obs=$row->Obs;$cancelar=$row->CanS;				
				$row->FechaS=substr($row->FechaS, 0, 10);
				$row->FechaS = date("d-m-Y",strtotime($row->FechaS));
				if($row->FechaR!=0){$row->Recibido=1;}else{$row->FechaR="";}
				$row->hora=substr ($fecha, 11, 8);
				$inc="";
				if($cancelar==1){ $inc=" ** INCOMPLETO **";}
				$row->DescripcionS1=$des.$inc; 
				$row->totp=($cont+=1);
				$fec=$row->FechaS;
				if($feci!=$fec){	$row->FechaS1=$row->FechaS; $feci=$row->FechaS;	}else{ $row->FechaS1="";} 				
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		function getNumRowsreq($filter){
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablasol);
			return $result->num_rows();//Se regresan la cantidad de registros encontrados 
		}
		function agregarr($can,$uni,$des,$obs,$req){
			$hoy=date("y-m-d H:i:s");	
			$data=array($this->fs=>$hoy,$this->can=>$can,$this->uni=>$uni,$this->des=>$des,$this->obs=>$obs,$this->req=>$req);			
			$this->db->insert($this->tablasol,$data);
			return $this->db->insert_id();
		}
		function actualizarr($id,$can,$uni,$des,$obs,$cans){
	 		if($cans==0){$hoy=0;}else{$hoy=date("y-m-d H:i:s");}
			$data=array($this->fr=>$hoy,$this->can=>$can,$this->uni=>$uni,$this->des=>$des,$this->obs=>$obs,$this->cans=>$cans);
			$this->db->where($this->id,$id);
			$this->db->update($this->tablasol,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		
		function borrarr($id){
			$this->db->where($this->id,$id);
			$this->db->delete($this->tablasol);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function verRequisicion(){
			$this->db->order_by($this->idr,'DESC');
			$query = $this->db->get($this->tablareq);
			return $query->result();
		}
		function nueRequisicion(){
			$this->db->select('max(Requisicion) as req');
			$result = $this->db->get($this->tablareq);
			foreach($result->result() as $row):
				$reqn=($row->req+1);
			endforeach;	
			$data=array($this->reqn=>$reqn);
			$this->db->insert($this->tablareq,$data);
			return $this->db->insert_id();
			//return $query->result();
		}
		function verNomReq($req){
			$this->db->select('Requisicion');
			$this->db->where($this->idr,$req);	
			$query = $this->db->get($this->tablareq);
			foreach($query->result() as $row):
				$remi=$row->Requisicion;
			endforeach;
			return $remi;
		}
    }
    
?>