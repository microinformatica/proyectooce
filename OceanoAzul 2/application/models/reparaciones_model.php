<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Reparaciones_model extends CI_Model {
        public $nrep="nrep";
		public $fecr="fecr";
		public $deps="deps";
		public $area="area";
		public $det="det";
		public $eje="eje";
		public $pri="priori";
		public $cal="cal";
		public $obsc="obsc";
		public $estatus="estatus";
		public $fecf="fecf";
		public $obs="obs";
		public $rea="realizo";
		public $tabla="reparaciones";
		var $today;
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		//Salas
		function verDiaj(){
			$this->db->select('fecr');	
			$this->db->group_by('fecr');
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}
		function getreparaciones($filter){
			$this->db->select('nrep,fecr,deps,area,det,eje,priori,cal,obsc,estatus,fecf,obs,realizo,DATEDIFF( fecf,fecr ) AS termino,DATEDIFF( CURDATE(),fecr ) AS actual',NULL,FALSE);
			$this->db->order_by('fecr');
			//$this->db->order_by('priori','DESC');
			//$this->db->order_by('nfic');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }			
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();$dia='';$cont=0;
			//Se forma el arreglo que sera retornado							
			foreach ($result->result() as $row):
				if($dia!=$row->fecr){
					$row->fecdd = $fec->fecha21($row->fecr); $dia=$row->fecr;
				}else{
					$row->fecdd ='';
				}
				if($row->fecf!='0000-00-00'){
					 $row->fecff = $fec->fecha21($row->fecf)."<br> DT [".$row->termino."]";
					 $row->focodia =$row->termino;
				}else{
					$row->fecff = "DT [".$row->actual."]";$row->focodia =$row->actual;
					$row->fecf='';	
				}	
				$row->totp=($cont+=1); 			
				$data[] = $row;
			endforeach;				
			return $data;
		}
		
		function getNumRows($filter){
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados			
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		
		public function agregar($fecr,$deps,$area,$det,$eje,$pri,$cal,$obsc,$estatus,$fecf,$obs,$rea){
			if($fecf!='') $data=array($this->fecr=>$fecr,$this->deps=>$deps,$this->area=>$area,$this->det=>$det,$this->eje=>$eje,$this->pri=>$pri,$this->cal=>$cal,$this->obsc=>$obsc,$this->estatus=>$estatus,$this->fecf=>$fecf,$this->obs=>$obs,$this->rea=>$rea); 
			else $data=array($this->fecr=>$fecr,$this->deps=>$deps,$this->area=>$area,$this->det=>$det,$this->eje=>$eje,$this->pri=>$pri,$this->cal=>$cal,$this->obsc=>$obsc,$this->estatus=>$estatus,$this->obs=>$obs,$this->rea=>$rea);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}
		
		public function actualizar($id,$fecr,$deps,$area,$det,$eje,$pri,$cal,$obsc,$estatus,$fecf,$obs,$rea){
			if($estatus=='Proceso') $fecf='';
			$data=array($this->fecr=>$fecr,$this->deps=>$deps,$this->area=>$area,$this->det=>$det,$this->eje=>$eje,$this->pri=>$pri,$this->cal=>$cal,$this->obsc=>$obsc,$this->estatus=>$estatus,$this->fecf=>$fecf,$this->obs=>$obs,$this->rea=>$rea);
			$this->db->where($this->nrep,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		
	
    }
    
?>