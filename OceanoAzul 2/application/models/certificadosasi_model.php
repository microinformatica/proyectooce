<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Certificadosasi_model extends CI_Model {        
        public $id="NumReg";
        public $numcer="NumCer";
        public $iv="IniVig";
        public $fv="FinVig";
        public $cp="CanPos";
        public $cn="CanNau";
		public $cer="NumCerR";
		public $rem="NumRegR";
        public $tabla="certificados";
		public $tablarem="r17";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		function getCertificadosasi($filter){
			//"Select FechaR,RemisionR,Tipo,NumCliR,Razon,Zona,Estatus,NumRegR,Cancelacion,NumCerR,NumCer 
			//FROM clientes 
			//INNER JOIN (certificados inner join r14 on r14.NumCerR=certificados.NumReg) ON r14.NumCliR=clientes.Numero 
			//where NumRegR='".$claveR."' order by r14.RemisionR ";
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			$this->db->join('certificados', 'NumCerR=NumReg','inner');
			$this->db->where('Estatus =',0);
			//$this->db->order_by('Razon');
			$this->db->order_by('RemisionR');
			if($filter['where']!='') $this->db->where($filter['where']);			
			$result = $this->db->get($this->tablarem);
			$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				//$row->CantidadRR = number_format(($row->CantidadRR), 3, '.', ','); 
				$row->Cantidad = number_format(($row->CantidadRR), 3, '.', ','); 
				$row->FechaR = date("d-m-Y",strtotime($row->FechaR));
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		function getNumRowsasi($filter){
			$this->db->join('clientes', 'NumCliR=Numero','inner');
			$this->db->join('certificados', 'NumCerR=NumReg','inner');
			$this->db->where('Estatus =',0);
			//$this->db->order_by('Razon');
			$this->db->order_by('RemisionR');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablarem);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getCertificadosasic($filter){
			//"Select FechaR,RemisionR,Tipo,NumCliR,Razon,Zona,Estatus,NumRegR,Cancelacion,NumCerR,NumCer 
			//FROM clientes 
			//INNER JOIN (certificados inner join r14 on r14.NumCerR=certificados.NumReg) ON r14.NumCliR=clientes.Numero 
			//where NumRegR='".$claveR."' order by r14.RemisionR ";
			//$this->db->join('clientes', 'NumCliR=Numero','inner');
			//$this->db->join('certificados', 'NumCerR=NumReg','inner');
			$this->db->select('NumReg,NumCer,IniVig,FinVig,CanPos,(select sum(CantidadRR) from r17 where NumCerR=NumReg and Estatus=0) as entregados');
			//$this->db->where('NumReg >',0);
			//$this->db->where('Estatus =',0);
			$this->db->order_by('NumReg','DESC');
			$result = $this->db->get($this->tabla);
			$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				if($row->NumReg>0){
					$row->IniVig = date("d-m-Y",strtotime($row->IniVig));
					$row->FinVig = date("d-m-Y",strtotime($row->FinVig));
					$row->pos = number_format(($row->CanPos), 3, '.', ',');
				}else{ $row->IniVig = "";$row->FinVig = "";$row->pos = "";	}
				if($row->entregados>0){$row->ent = number_format(($row->entregados), 3, '.', ',');}else{$row->ent="";}
				$row->dif = number_format(($row->CanPos-$row->entregados), 3, '.', ',');
				$data[] = $row;
			endforeach;
			
			}
			return $data;
		}
		function getNumRowsasic($filter){
			//$this->db->join('clientes', 'NumCliR=Numero','inner');
			//$this->db->join('certificados', 'NumCerR=NumReg','inner');
			//$this->db->where('Estatus =',0);
			//$this->db->order_by('Razon');
			//$this->db->order_by('RemisionR');
			//if($filter['where']!='')
			//	$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			//$this->db->where('Estatus =',0);
			$this->db->select('NumReg,NumCer,IniVig,FinVig,CanPos,(select sum(CantidadRR) from r17 where NumCerR=NumReg and Estatus=0) as entregados');
			//$this->db->where('NumReg >',0);
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function verCertificado(){
			
			$query=$this->db->get($this->tabla);
			return $query->result();			
		}
		public function getCertificadosNum($cer){			
			$this->db->where($this->id,$cer);
			$query = $this->db->get($this->tabla);
			return $query->result();
			
		}
		function verRemision(){
			$this->db->where('NumRegR >',0);			
			$this->db->where('RemisionR !=',0);
			$this->db->order_by($this->rem);
			$query=$this->db->get($this->tablarem);
			return $query->result();			
		}
	 	function actualizar($id,$cer){
			$data=array($this->cer=>$cer);
			$this->db->where($this->rem,$id);
			$this->db->update($this->tablarem,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function actualizarg($cer,$ini,$fin){
			while($ini <= $fin){					
				$data=array($this->cer=>$cer);		
				$this->db->where($this->rem,$ini);	
				$this->db->update($this->tablarem,$data);
				$ini=$ini+1;	
			}
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
    }
    
?>