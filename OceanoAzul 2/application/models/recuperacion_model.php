<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Recuperacion_model extends CI_Model {        
        public $id="NumReg";
        public $numcer="NumCer";
        public $iv="IniVig";
        public $fv="FinVig";
        public $cp="CanPos";
        public $cn="CanNau";
		public $cer="NumCerR";
		public $rem="NumRegR";
        public $tabla="certificados";
		public $tablarem="r17";
		
		function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		function getRecuperacion($filter){
			$tbl=17;
			$CaT1=0;$CaT=0;$eT1=0;$eT=0;$dT1=0;$dT=0;
			$this->db->select('MAX(NumRegR) as ultimo');
			$result = $this->db->get('r'.$tbl);
			foreach($result->result() as $row):
				$ultimo = $row->ultimo+1; 
			endforeach;
			if($ultimo>0) {$ciclo="r".$tbl; $ciclo1=$ciclo; $dec=$tbl;}	
		 	else{
			 $dec=date("Y"); $dec=explode("0",$dec,2); $dec=(int)$dec[1]; $ciclo="r".$dec; $tbl=$dec;
			 }
			 //$aplicar=2013;
			if ($tbl>=10) { $aplicar="20".$tbl;} else { $aplicar="200".$tbl;}
	 		$contador=1;$iniciar=2008; $cont=0; $tPLS=0;$tFAC=0;$tCAR=0;$tEFE=0;$tDES=0;
	 		while($iniciar<=$aplicar){ $cont+=1; $iniciar=$iniciar+1;} 
	 			while($contador<=$cont){
					$row->cic=$aplicar;	 
					//$sqlConsulta="SELECT sum(CantidadR-(CantidadR*DescuentoR)) as canti From ".$ciclo." WHERE Estatus=0 and Tipo=1 AND NumRegR>0 GROUP BY Tipo";
	   				//$result=consultaSQL($sqlConsulta,$conexion); $registro=mysql_fetch_array($result);$cantidad=($registro["canti"]);$tPLS+=$cantidad;
					$this->db->select('sum(CantidadR-(CantidadR*DescuentoR)) as canti');
					$this->db->where('Estatus =',0);$this->db->where('Tipo =',1);$this->db->where('NumRegR >',0);
					$this->db->group_by('Tipo');
					$result = $this->db->get($ciclo);
					foreach($result->result() as $row1):
						$tPLS+=$row1->canti;$row->pls = number_format(($row1->canti), 3, '.', ',');  
					endforeach;	 
	    			//$sqlConsulta="SELECT sum((CantidadR-(CantidadR*DescuentoR))*PrecioR) as canti From ".$ciclo." WHERE Estatus=0 and Tipo=1 AND NumRegR>0  GROUP BY Tipo";
					//$result=consultaSQL($sqlConsulta,$conexion); $registro=mysql_fetch_array($result);$cantidadV=($registro["canti"]);$tFAC+=$cantidadV;
					$this->db->select('sum((CantidadR-(CantidadR*DescuentoR))*PrecioR) as canti');
					$this->db->where('Estatus =',0);$this->db->where('Tipo =',1);$this->db->where('NumRegR >',0);
					$this->db->group_by('Tipo');
					$result = $this->db->get($ciclo);
					foreach($result->result() as $row2):
						$tFAC+=$row2->canti;$row->impo = number_format(($row2->canti), 2, '.', ','); $cantidadV=$row2->canti; 
					endforeach;
					//$sqlConsulta="select sum(depositos.ImporteD) as canti from clientes INNER JOIN depositos ON depositos.NRC=clientes.Numero where Zona!='Varias' and Zona!='-' and Aplicar='".$aplicar."' and Des=0 and Car=-1";
					//$result=consultaSQL($sqlConsulta,$conexion); $registro=mysql_fetch_array($result);$CaT=($registro["canti"]);$tCAR+=$CaT;
					$this->db->select('sum(depositos.ImporteD) as canti');
					$this->db->join('clientes', 'NRC=Numero','inner');
					$this->db->where('Zona !=','Varias');$this->db->where('Zona !=','-');$this->db->where('Aplicar =',$aplicar);$this->db->where('Des =',0);$this->db->where('Car =',-1);
					$result = $this->db->get('depositos');
					foreach($result->result() as $row3):
						$tCAR+=$row3->canti;$row->carg = number_format(($row3->canti), 2, '.', ',');  
					endforeach;
					$row->totvtas = number_format(($row2->canti+$row3->canti), 2, '.', ','); 
					//$sqlConsulta="select sum(depositos.ImporteD) as canti from clientes INNER JOIN depositos ON depositos.NRC=clientes.Numero where Zona!='Varias' and Zona!='-' and Aplicar='".$aplicar."' and Des=0 and Car=0"; 
					//$result=consultaSQL($sqlConsulta,$conexion); $registro=mysql_fetch_array($result);$eT=($registro["canti"]);$tEFE+=$eT;
					$this->db->select('sum(depositos.ImporteD) as canti');
					$this->db->join('clientes', 'NRC=Numero','inner');
					$this->db->where('Zona !=','Varias');$this->db->where('Zona !=','-');$this->db->where('Aplicar =',$aplicar);$this->db->where('Des =',0);$this->db->where('Car =',0);
					$result = $this->db->get('depositos');
					foreach($result->result() as $row4):
						$eT+=$row4->canti;$row->depo = number_format(($row4->canti), 2, '.', ','); $tEFE+=$eT; 
					endforeach;
					//$sqlConsulta="select sum(depositos.ImporteD) as canti from clientes INNER JOIN depositos ON depositos.NRC=clientes.Numero where Zona!='Varias' and Zona!='-' and Aplicar='".$aplicar."' and Des=-1 and Car=0";
					//$result=consultaSQL($sqlConsulta,$conexion); $registro=mysql_fetch_array($result);$dT=($registro["canti"]);$tDES+=$dT;	
					$this->db->select('sum(depositos.ImporteD) as canti');
					$this->db->join('clientes', 'NRC=Numero','inner');
					$this->db->where('Zona !=','Varias');$this->db->where('Zona !=','-');$this->db->where('Aplicar =',$aplicar);$this->db->where('Des =',-1);$this->db->where('Car =',0);
					$result = $this->db->get('depositos');
					foreach($result->result() as $row5):
						$dT+=$row5->canti;$row->dscto = number_format(($row5->canti), 2, '.', ','); $tDES+=$dT; 
					endforeach;
					$row->totrecu = number_format(($row4->canti+$row5->canti), 2, '.', ',');
				/*
						$this->db->join('clientes', 'NumCliR=Numero','inner');
						$this->db->join('certificados', 'NumCerR=NumReg','inner');
						$this->db->where('Estatus =',0);
						$this->db->order_by('RemisionR');
						if($filter['where']!='') $this->db->where($filter['where']);			
						$result = $this->db->get($this->tablarem);
						$ent=0;	$data = array();$raz='';$totgranja=0;$dom='';
						
						foreach($result->result() as $row):
							$row->Cantidad = number_format(($row->CantidadRR), 3, '.', ','); 
							$row->FechaR = date("d-m-Y",strtotime($row->FechaR));
							$data[] = $row;
						endforeach;*/
					$row->saldo ="$". number_format((($row2->canti+$row3->canti)-($row4->canti+$row5->canti)), 2, '.', ','); 
					$data[] = $row;		
					$this->db->select('MAX(NumRegR) as ultimo');
					$result = $this->db->get('r'.$tbl);
					foreach($result->result() as $row):
						//$ultimo = $row->ultimo+1; 
					endforeach;	
					//}
					if($ciclo1==$ciclo){ $CaT1=$CaT;$eT1=$eT;$dT1=$dT;}
					$contador=$contador+1;$cantidadT=0;	$dec-=1; if($dec<10) {$dec="0".$dec; } $ciclo="r".$dec;$aplicar="20".$dec;
			}	
			$row->cic="Total";
			$row->pls =$row->pls = number_format(($tPLS), 3, '.', ',');
			$row->impo ="$". number_format(($tFAC), 2, '.', ',');
			$row->carg ="$". number_format(($tCAR), 2, '.', ',');
			$row->totvtas ="$". number_format(($tFAC+$tCAR), 2, '.', ',');
			$row->depo ="$". number_format(($eT), 2, '.', ',');
			$row->dscto ="$". number_format(($dT), 2, '.', ',');
			$row->totrecu ="$". number_format(($eT+$dT), 2, '.', ',');
			$row->saldo ="$". number_format((($tFAC+$tCAR)-($eT+$dT)), 2, '.', ',');
			$data[] = $row;
			return $data;
		}
		function getNumRowsRecu($filter){
			/*$this->db->join('clientes', 'NumCliR=Numero','inner');
			$this->db->join('certificados', 'NumCerR=NumReg','inner');
			$this->db->where('Estatus =',0);
			//$this->db->order_by('Razon');
			$this->db->order_by('RemisionR');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tablarem);//En este caso no es necesario limitar los registros*/
			return 6; //$result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
		function getRecuperacionA($filter,$ciclo,$cer,$zon){
			//"select Zona,Razon,Numero,Saldo 
			//From clientes 
			//right join depositos on  NRC=Numero 
			//where Zona!='Varias' and aplicar='".$txt1."' 
			//group by Zona,Razon,Saldo 
			/*$this->db->select('Zona,Razon,Numero,Saldo');
			$this->db->join('depositos', 'NRC=Numero','right');
			$this->db->where('Zona!=','Varias');
			$this->db->where('aplicar=',$cer);
			$this->db->get('clientes');*/
			
			//union select Zona,Razon,Numero,Saldo 
			//From clientes 
			//inner join (".$ciclo." left join depositos on NRC=NumCliR) on  NumCliR=Numero
			// where Zona!='Varias' and Estatus=0 
			//group by Zona,Razon,Saldo order by Zona,Saldo DESC,Razon";
			if($zon!='null' && $zon!='0'){
			//if($zon=="null"){
					$query=$this->db->query("SELECT Zona,Razon,Numero,Saldo
									 FROM clientes 
									 RIGHT JOIN depositos ON NRC=Numero 
									 WHERE Zona!='Varias' AND aplicar=$cer AND Zona='$zon'
									 GROUP BY Zona,Razon,Saldo 
									 UNION SELECT Zona,Razon,Numero,Saldo 
									 FROM clientes 
									 INNER JOIN ($ciclo 
									 LEFT JOIN depositos ON NRC=NumCliR) ON NumCliR=Numero 
									 WHERE Zona!='Varias' AND Estatus=0  AND Zona='$zon'
									 GROUP BY Zona,Razon 
									 ORDER BY Zona,Razon");
			
			}
			else{
			$query=$this->db->query("SELECT Zona,Razon,Numero,Saldo
									 FROM clientes 
									 RIGHT JOIN depositos ON NRC=Numero 
									 WHERE Zona!='Varias' AND aplicar=$cer 
									 GROUP BY Zona,Razon,Saldo 
									 UNION SELECT Zona,Razon,Numero,Saldo 
									 FROM clientes 
									 INNER JOIN ($ciclo 
									 LEFT JOIN depositos ON NRC=NumCliR) ON NumCliR=Numero 
									 WHERE Zona!='Varias' AND Estatus=0 
									 GROUP BY Zona,Razon 
									 ORDER BY Zona,Razon");
			}
			$gt=1;
			$cli='';$entro=0;$plsZ=0;$impZ=0;$carZ=0;$depZ=0;$desZ=0;$camZ=0;$totZ=0;
			$plsZ1=0;$impZ1=0;$carZ1=0;$depZ1=0;$desZ1=0;$camZ1=0;$totZ1=0;
			foreach($query->result() as $row):
				$zona=$row->Zona;$numcli=$row->Numero;
				if($cli!=$zona){
					$row->zon=$row->Zona;$cli=$row->Zona; 
					if($entro>0){ 
						//aqui se mete el total segun la zona
						$gt+=1;
						$this->db->select('max(RemisionR)');
						$resultZ = $this->db->get($this->tablarem);			
						foreach ($resultZ->result() as $rowT):				
							$rowT->zon = "Total";$rowT->raz = "";
							if($plsZ!=0){$rowT->pls= number_format($plsZ, 3, '.', ',');}else{$rowT->pls='';}
							if($impZ!=0){$rowT->imp1="$".number_format($impZ, 2, '.', ',');}else{$rowT->imp1='';}
							if($carZ!=0){$rowT->car1="$".number_format($carZ, 2, '.', ',');}else{$rowT->car1='';}
							if($depZ!=0){$rowT->dep1="$".number_format($depZ, 2, '.', ',');}else{$rowT->dep1='';}
							if($desZ!=0){$rowT->des1="$".number_format($desZ, 2, '.', ',');}else{$rowT->des1='';}
							if($camZ!=0){$rowT->cam1="$".number_format($camZ, 2, '.', ',');}else{$rowT->cam1='';}
							if($totZ!=0){$rowT->saldo="$".number_format($totZ, 2, '.', ',');}else{$rowT->saldo='';}
							$data[] = $rowT;	
						endforeach;
						$plsZ1+=$plsZ;$impZ1+=$impZ;$carZ1+=$carZ;$depZ1+=$depZ;$desZ1+=$desZ;$camZ1+=$camZ;$totZ1+=$totZ;
						$entro=0;$plsZ=0;$impZ=0;$carZ=0;$depZ=0;$desZ=0;$camZ=0;$totZ=0;
					}
				}else{ $row->zon="";} 	
				$row->raz=$row->Razon;$entro=1;
				//obtener la cantidad en pls y el importe de la venta
				$querypls=$this->db->query("SELECT sum((CantidadR-(CantidadR*DescuentoR))) as pls,sum((CantidadR-(CantidadR*DescuentoR))*PrecioR) as imp 
											From $ciclo inner join clientes on Numero=NumCliR WHERE Numero=$numcli and Estatus=0 and Tipo=1 AND NumRegR>0");
				foreach($querypls->result() as $rowpls):
					if($rowpls->pls>0){$row->pls = number_format($rowpls->pls, 3, '.', ','); $plsZ+=$rowpls->pls;}else{$row->pls ='';}
					if($rowpls->imp>0){$row->imp1 = "$".number_format($rowpls->imp, 2, '.', ',');$impZ+=$rowpls->imp;}else{$row->imp1 ='';}
				endforeach;
				//obtener cargos
				$querycar=$this->db->query("SELECT sum(ImporteD) as cargo  FROM depositos WHERE NRC=$numcli and Des=0 and Car=-1 and Aplicar=$cer");
				foreach($querycar->result() as $rowcar):
					if($rowcar->cargo>0){$row->car1 = "$".number_format($rowcar->cargo, 2, '.', ',');$carZ+=$rowcar->cargo;}else{$row->car1 ='';}
				endforeach;
				//obtener depositos
				$querydep=$this->db->query("SELECT sum(ImporteD) as depositos  FROM depositos WHERE NRC=$numcli and Des=0 and Car=0 and Aplicar=$cer");
				foreach($querydep->result() as $rowdep):
					if($rowdep->depositos>0){$row->dep1 = "$".number_format($rowdep->depositos, 2, '.', ',');$depZ+=$rowdep->depositos;}else{$row->dep1 ='';}
				endforeach;	
				//obtener descuentos
				$querydes=$this->db->query("SELECT sum(ImporteD) as descuentos  FROM depositos WHERE NRC=$numcli and Des=-1 and Car=0 and Cam=0 and Aplicar=$cer");
				foreach($querydes->result() as $rowdes):
					if($rowdes->descuentos>0){$row->des1 = "$".number_format($rowdes->descuentos, 2, '.', ',');$desZ+=$rowdes->descuentos;}else{$row->des1 ='';}
				endforeach;
				//obtener camarones
				$querycam=$this->db->query("SELECT sum(ImporteD) as camaron  FROM depositos WHERE NRC=$numcli and Des=-1 and Car=0 and Cam=-1 and Aplicar=$cer");
				foreach($querycam->result() as $rowcam):
					if($rowcam->camaron>0){$row->cam1 = "$".number_format($rowcam->camaron, 2, '.', ',');$camZ+=$rowcam->camaron;}else{$row->cam1 ='';}
				endforeach;	
				$totZ+=(($rowpls->imp+$rowcar->cargo)-($rowdep->depositos+$rowdes->descuentos+$rowcam->camaron));
				$row->saldo= "$".number_format((($rowpls->imp+$rowcar->cargo)-($rowdep->depositos+$rowdes->descuentos+$rowcam->camaron)), 2, '.', ',');	
				$data[] = $row;	
			endforeach;
			if($entro>0){ 
				//aqui se mete el total segun la zona
				$this->db->select('max(RemisionR)');
				$resultZ = $this->db->get($this->tablarem);			
				foreach ($resultZ->result() as $rowT):				
					$rowT->zon = "Total";$rowT->raz = "";
					if($plsZ!=0){$rowT->pls= number_format($plsZ, 3, '.', ',');}else{$rowT->pls='';}
					if($impZ!=0){$rowT->imp1="$".number_format($impZ, 2, '.', ',');}else{$rowT->imp1='';}
					if($carZ!=0){$rowT->car1="$".number_format($carZ, 2, '.', ',');}else{$rowT->car1='';}
					if($depZ!=0){$rowT->dep1="$".number_format($depZ, 2, '.', ',');}else{$rowT->dep1='';}
					if($desZ!=0){$rowT->des1="$".number_format($desZ, 2, '.', ',');}else{$rowT->des1='';}
					if($camZ!=0){$rowT->cam1="$".number_format($camZ, 2, '.', ',');}else{$rowT->cam1='';}
					if($totZ!=0){$rowT->saldo="$".number_format($totZ, 2, '.', ',');}else{$rowT->saldo='';}
					$data[] = $rowT;	
					$plsZ1+=$plsZ;$impZ1+=$impZ;$carZ1+=$carZ;$depZ1+=$depZ;$desZ1+=$desZ;$camZ1+=$camZ;$totZ1+=$totZ;
				endforeach;
			
			}
			if($gt>1){ 
				//aqui se mete el total segun la zona
				$this->db->select('max(RemisionR)');
				$resultZ = $this->db->get($this->tablarem);			
				foreach ($resultZ->result() as $rowT):				
					$rowT->zon = "Gral Ciclo";$rowT->raz = "";
					if($plsZ1!=0){$rowT->pls= number_format($plsZ1, 3, '.', ',');}else{$rowT->pls='';}
					if($impZ1!=0){$rowT->imp1="$".number_format($impZ1, 2, '.', ',');}else{$rowT->imp1='';}
					if($carZ1!=0){$rowT->car1="$".number_format($carZ1, 2, '.', ',');}else{$rowT->car1='';}
					if($depZ1!=0){$rowT->dep1="$".number_format($depZ1, 2, '.', ',');}else{$rowT->dep1='';}
					if($desZ1!=0){$rowT->des1="$".number_format($desZ1, 2, '.', ',');}else{$rowT->des1='';}
					if($camZ1!=0){$rowT->cam1="$".number_format($camZ1, 2, '.', ',');}else{$rowT->cam1='';}
					if($totZ1!=0){$rowT->saldo="$".number_format($totZ1, 2, '.', ',');}else{$rowT->saldo='';}
					$data[] = $rowT;	
				endforeach;
			
			}
			return $data;
		}
		function getNumRowsRecuA($filter,$ciclo,$cer,$zon){
			if($zon!='null' && $zon!='0'){
			//if($zon=="null"){
					$query=$this->db->query("SELECT Zona,Razon,Numero,Saldo
									 FROM clientes 
									 RIGHT JOIN depositos ON NRC=Numero 
									 WHERE Zona!='Varias' AND aplicar=$cer AND Zona='$zon'
									 GROUP BY Zona,Razon,Saldo 
									 UNION SELECT Zona,Razon,Numero,Saldo 
									 FROM clientes 
									 INNER JOIN ($ciclo 
									 LEFT JOIN depositos ON NRC=NumCliR) ON NumCliR=Numero 
									 WHERE Zona!='Varias' AND Estatus=0  AND Zona='$zon'
									 GROUP BY Zona,Razon 
									 ORDER BY Zona,Razon");
			
			}
			else{
			$query=$this->db->query("SELECT Zona,Razon,Numero,Saldo
									 FROM clientes 
									 RIGHT JOIN depositos ON NRC=Numero 
									 WHERE Zona!='Varias' AND aplicar=$cer 
									 GROUP BY Zona,Razon,Saldo 
									 UNION SELECT Zona,Razon,Numero,Saldo 
									 FROM clientes 
									 INNER JOIN ($ciclo 
									 LEFT JOIN depositos ON NRC=NumCliR) ON NumCliR=Numero 
									 WHERE Zona!='Varias' AND Estatus=0 
									 GROUP BY Zona,Razon 
									 ORDER BY Zona,Razon");
			}
			foreach($query->result() as $row):
				//$zona=$row->Zona;$numcli=$row->Numero;
					
			endforeach;	
			return $query->num_rows();
		}
		function getElementsC($where){
			$this->db->select('zona,zona as val');			
			$this->db->join('clientes', 'NumCliR=Numero', 'inner');			
			$this->db->where('Estatus =',0);
			$this->db->group_by('Zona');			
			if($where['actual']==2008){$result = $this->db->get('r08');}
			if($where['actual']==2009){$result = $this->db->get('r09');}
			if($where['actual']==2010){$result = $this->db->get('r10');}
			if($where['actual']==2011){$result = $this->db->get('r11');}
			if($where['actual']==2012){$result = $this->db->get('r12');}
			if($where['actual']==2013){$result = $this->db->get('r13');}
			if($where['actual']==2014){$result = $this->db->get('r14');}
			if($where['actual']==2015){$result = $this->db->get('r15');}
			if($where['actual']==2016){$result = $this->db->get('r16');}
			if($where['actual']==2017){$result = $this->db->get($this->tablarem);}
			$data = array();  
		    foreach($result->result() as $row):
        	    $data[] = $row;
        	endforeach;        
        	return $data;		
    	}
		
    }
    
?>