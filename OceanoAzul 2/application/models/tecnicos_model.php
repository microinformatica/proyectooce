<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Tecnicos_model extends CI_Model {
        public $ciclo="r14";
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function verTecnico(){
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$this->db->order_by('nombio');
			$query=$this->db->get('biologo');
			return $query->result();			
		}
		function verTecnico1($nt){
			$this->db->where('numbio =',$nt);
			$query=$this->db->get('biologo');
			return $query->result();			
		}
		function getTecnicos($filter){
			$veces=10;//$veces=6;$contador=1;	
			$mex=1;		$primera=1;
			$data = array();
	 		while($mex<=$veces){
	 			if($primera==1) $mex=12;
		 		//select day(FechaR) as dia,month(FechaR) as mes,RemisionR from r13 inner join solicitud on NumRemS=NumRegR where NumBioS=29 and FechaR>0 and month(FechaR)=1 group by FechaR,RemisionR
				$this->db->select('day(FechaR) as dia,month(FechaR) as mes,RemisionR');
				$this->db->join('solicitud', 'NumRemS=NumRegR','inner');	
				if($filter['where']!=''){$this->db->where($filter['where']);}
				$this->db->where('month(FechaR) =',$mex);
				$this->db->where('FechaR >',0);
				$this->db->where('NomDesS !=','Improvistos');
				$this->db->where('NomDesS !=','Complemento Gasto');
				$this->db->group_by(array("FechaR", "RemisionR"));
				$result = $this->db->get($this->ciclo);	
				$cantidadre=$result->num_rows();
				if($cantidadre>=1){
					$ini=1; while($ini<=31){$datosd [$ini]="";$datosr [$ini]="";$ini+=1;}
					$ini=1;	$tot=0;
					foreach ($result->result() as $row):
						$dia=$row->dia;$mes=$row->mes;$datosd [$dia]=$row->dia;
						//$datosr [$dia]="*";
						$datosr [$dia]=$row->RemisionR;$tot+=1;
					endforeach;
					switch($mes){
						case 1: $row->mes = "Ene"; break;	case 2: $row->mes = "Feb"; break;	case 3: $row->mes = "Mar"; break;
						case 4: $row->mes = "Abr"; break;	case 5: $row->mes = "May"; break;	case 6: $row->mes = "Jun"; break;
						case 7: $row->mes = "Jul"; break;	case 8: $row->mes = "Ago"; break;	case 9: $row->mes = "Sep"; break;
						case 10: $row->mes = "Oct"; break;  case 12: $row->mes = "Dic"; break;	
					}	
					if($cantidadre>=1 && $mex==12){$primera=2;$mex=0;}
					$ini=1;
					while($ini<=31){
						switch($ini){
							case 1: if($datosd [$ini]==$ini){ $row->d1 = $datosr [$ini];} else {  $row->d1 ="";} break;
							case 2: if($datosd [$ini]==$ini){ $row->d2 = $datosr [$ini];} else {  $row->d2 ="";} break;
							case 3: if($datosd [$ini]==$ini){ $row->d3 = $datosr [$ini];} else {  $row->d3 ="";} break;
							case 4: if($datosd [$ini]==$ini){ $row->d4 = $datosr [$ini];} else {  $row->d4 ="";} break;
							case 5: if($datosd [$ini]==$ini){ $row->d5 = $datosr [$ini];} else {  $row->d5 ="";} break;
							case 6: if($datosd [$ini]==$ini){ $row->d6 = $datosr [$ini];} else {  $row->d6 ="";} break;
							case 7: if($datosd [$ini]==$ini){ $row->d7 = $datosr [$ini];} else {  $row->d7 ="";} break;
							case 8: if($datosd [$ini]==$ini){ $row->d8 = $datosr [$ini];} else {  $row->d8 ="";} break;
							case 9: if($datosd [$ini]==$ini){ $row->d9 = $datosr [$ini];} else {  $row->d9 ="";} break;
							case 10: if($datosd [$ini]==$ini){ $row->d10 = $datosr [$ini];} else {  $row->d10 ="";} break;
							case 11: if($datosd [$ini]==$ini){ $row->d11 = $datosr [$ini];} else {  $row->d11 ="";} break;
							case 12: if($datosd [$ini]==$ini){ $row->d12 = $datosr [$ini];} else {  $row->d12 ="";} break;
							case 13: if($datosd [$ini]==$ini){ $row->d13 = $datosr [$ini];} else {  $row->d13 ="";} break;
							case 14: if($datosd [$ini]==$ini){ $row->d14 = $datosr [$ini];} else {  $row->d14 ="";} break;
							case 15: if($datosd [$ini]==$ini){ $row->d15 = $datosr [$ini];} else {  $row->d15 ="";} break;
							case 16: if($datosd [$ini]==$ini){ $row->d16 = $datosr [$ini];} else {  $row->d16 ="";} break;
							case 17: if($datosd [$ini]==$ini){ $row->d17 = $datosr [$ini];} else {  $row->d17 ="";} break;
							case 18: if($datosd [$ini]==$ini){ $row->d18 = $datosr [$ini];} else {  $row->d18 ="";} break;
							case 19: if($datosd [$ini]==$ini){ $row->d19 = $datosr [$ini];} else {  $row->d19 ="";} break;
							case 20: if($datosd [$ini]==$ini){ $row->d20 = $datosr [$ini];} else {  $row->d20 ="";} break;
							case 21: if($datosd [$ini]==$ini){ $row->d21 = $datosr [$ini];} else {  $row->d21 ="";} break;
							case 22: if($datosd [$ini]==$ini){ $row->d22 = $datosr [$ini];} else {  $row->d22 ="";} break;
							case 23: if($datosd [$ini]==$ini){ $row->d23 = $datosr [$ini];} else {  $row->d23 ="";} break;
							case 24: if($datosd [$ini]==$ini){ $row->d24 = $datosr [$ini];} else {  $row->d24 ="";} break;
							case 25: if($datosd [$ini]==$ini){ $row->d25 = $datosr [$ini];} else {  $row->d25 ="";} break;
							case 26: if($datosd [$ini]==$ini){ $row->d26 = $datosr [$ini];} else {  $row->d26 ="";} break;
							case 27: if($datosd [$ini]==$ini){ $row->d27 = $datosr [$ini];} else {  $row->d27 ="";} break;
							case 28: if($datosd [$ini]==$ini){ $row->d28 = $datosr [$ini];} else {  $row->d28 ="";} break;
							case 29: if($datosd [$ini]==$ini){ $row->d29 = $datosr [$ini];} else {  $row->d29 ="";} break;
							case 30: if($datosd [$ini]==$ini){ $row->d30 = $datosr [$ini];} else {  $row->d30 ="";} break;
							case 31: if($datosd [$ini]==$ini){ $row->d31 = $datosr [$ini];} else {  $row->d31 ="";} break;
						}
						$ini+=1;
					}
					$row->tot = $tot;
					$data[] = $row;
				}
				if($primera==1){$primera=2;$mex=0;}
				$mex+=1;
			}
			return $data;
		}
		function getNumRowsT($filter){
			return 10;
		}
		function getTotet($filter){
			$this->db->select('NumBio,NomBio');
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$this->db->order_by('nombio');
			$result = $this->db->get('biologo');
			$data = array();
			foreach ($result->result() as $row):
				//select count(*) from r13 inner join solicitud on NumRemS=NumRegR where NumBioS=52 and FechaR>0 and CarAbo=1 and NomDesS!='Improvistos' and NomDesS!='Complemento Gasto' group by NumBioS,NumRemS,RemisionR
				$bio=$row->NumBio;
				$veces=12;//$veces=6;$contador=1;	
				$mex=1; 		$cont=0;		
				while($mex<=$veces){
		 			//select day(FechaR) as dia,month(FechaR) as mes,RemisionR from r13 inner join solicitud on NumRemS=NumRegR where NumBioS=29 and FechaR>0 and month(FechaR)=1 group by FechaR,RemisionR
					$this->db->select('day(FechaR) as dia,month(FechaR) as mes,RemisionR');
					$this->db->join('solicitud', 'NumRemS=NumRegR','inner');	
					$this->db->where('NumBioS',$bio);
					$this->db->where('month(FechaR) =',$mex);
					$this->db->where('FechaR >',0);
					$this->db->where('NomDesS !=','Improvistos');
					$this->db->where('NomDesS !=','Complemento Gasto');
					$this->db->group_by(array("FechaR", "RemisionR"));
					$result1 = $this->db->get($this->ciclo);	
					$cantidadre=$result1->num_rows();
					if($cantidadre>=1){
						foreach ($result1->result() as $row1):
							$cont+=1;
						endforeach;
					}
					$mex+=1;
				}
				$row->viajes=$cont;
				$data[] = $row;
			endforeach;	
			return $data;
		}	
		function getNumRowsTotet($filter){
			$this->db->where('numbio >',1);
			$this->db->where('activo =',0);
			$this->db->order_by('nombio');
			$result = $this->db->get('biologo');	
			return $result->num_rows();
		}
		function getTecnicosdet($filter){
				//select day(FechaR) as dia,month(FechaR) as mes,RemisionR from r13 inner join solicitud on NumRemS=NumRegR where NumBioS=29 and FechaR>0 and month(FechaR)=1 group by FechaR,RemisionR
				$this->db->select('day(FechaR) as dia,month(FechaR) as mes,RemisionR,FechaR,Razon,Zona');
				$this->db->join('solicitud', 'NumRemS=NumRegR','inner');	
				$this->db->join('clientes', 'Numero=NumCliR','inner');
				if($filter['where']!=''){$this->db->where($filter['where']);}
				$this->db->where('FechaR >',0);
				$this->db->where('NomDesS !=','Improvistos');
				$this->db->where('NomDesS !=','Complemento Gasto');
				$this->db->group_by(array("FechaR", "RemisionR"));
				$result = $this->db->get($this->ciclo);	
				$cantidadre=$result->num_rows();
				$data = array(); $fec=new Libreria();
				if($cantidadre>=1){
					$mex=0; $cont=0;
					foreach ($result->result() as $row):
						$mes=$row->mes;$act=$row->FechaR;
						$row->FechaR1 = $fec->fecha($row->FechaR);
						switch($mes){
							case 1: $row->mesi = "Ene"; break;	case 2: $row->mesi = "Feb"; break;	case 3: $row->mesi = "Mar"; break;
							case 4: $row->mesi = "Abr"; break;	case 5: $row->mesi = "May"; break;	case 6: $row->mesi = "Jun"; break;
							case 7: $row->mesi = "Jul"; break;	case 8: $row->mesi = "Ago"; break;	case 9: $row->mesi = "Sep"; break;	
							case 10: $row->mesi = "Oct"; break; case 12: $row->mesi = "Dic"; break;
						}
						if($mex!=$mes){	$mex=$row->mes;	}else{ $row->mesi="";}
						if($cont==0){$row->diasi=''; $ant=$row->FechaR;}
						else{
							$act = strtotime($act);  //strtotime("2011-02-03 00:00:00");
							$ant = strtotime($ant); //strtotime("1964-10-30 00:00:00");
							$row->diasi= round(abs($act-$ant)/60/60/24);
							$ant=$row->FechaR;
						}
						$cont+=1;
						$data[] = $row;	
					endforeach;
				}
			return $data;
		}
		function getNumRowsTdet($filter){
			$this->db->join('solicitud', 'NumRemS=NumRegR','inner');	
			if($filter['where']!=''){$this->db->where($filter['where']);}
			$this->db->where('FechaR >',0);
			$this->db->where('NomDesS !=','Improvistos');
			$this->db->where('NomDesS !=','Complemento Gasto');
			$this->db->group_by(array("FechaR", "RemisionR"));
			$result = $this->db->get($this->ciclo);	
			return $result->num_rows();
		}
    }    
?>