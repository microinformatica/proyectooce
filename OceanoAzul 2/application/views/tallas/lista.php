<?php $this->load->view('header_cv'); ?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
/*ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
} */  
#export_datag { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datav { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#tallas" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/camarones.png" width="20" height="20" border="0"> Tallas </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 550px"  >
		<div id="tallas" class="tab_content" style="margin-top: -10px">
			<div class="ajaxSorterDiv" id="tabla_vta" name="tabla_vta" style="width: 955px; margin-left: -10px"  >
				<div class="ajaxpager" style="margin-top: 1px;" > 
            		<ul class="order_list" style="width: 930px; text-align: left" >
            			N.C.<input type="text" name="idt" id="idt" style="width: 20px; border: none;" readonly="true" >
            			Nombre<input type="text" name="nomt" id="nomt" style="width: 120px" />
            			Grupo <input type="hidden" name="grupo" id="grupo"/><input type="hidden" name="gpogrl" id="gpogrl"/>
            			<select id="gpoid" style="font-size:10px; margin-top: 1px" >
                    			<option value="0">Talla</option>
          						<?php	
          						$data['result']=$this->tallas_model->getTallasg();
								foreach ($data['result'] as $row):?>
           						<option value="<?php echo $row->gpoid;?>"><?php echo $row->grupo;?></option>
           						<?php endforeach;?>
   						</select>
            			Precios:
            			Fecha <input size="12%" type="text" name="prefec" id="prefec" class="fecha redondo mUp" style="text-align: center;  " >
            			Menudeo $<input type="text" name="premen" id="premen" style="width: 40px" />
            			Mayoreo $<input type="text" name="premay" id="premay" style="width: 40px" />
            			<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez" ){ ?>
            				<input style="font-size: 12px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
							<input style="font-size: 12px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
						<?php }?>
            			<button id="export_datav" type="button" style="width:25px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: 1px; margin-left: 1px"  ></button>
						<form id="data_tablev" action="<?= base_url()?>index.php/tallas/pdfrepvta" method="POST">
							<input type="hidden" name="tablav" id="html_vtav"/> 
						</form>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#export_datav').click(function(){									
									$('#html_vtav').val($('#mytablaTalla').html());
									$('#data_tablev').submit();
									$('#html_vtav').val('');
								});
							});
						</script>       
                   	</ul>
 				</div>                 			
				<span class="ajaxTable" style="height: 480px; width:955px" >
                	<table id="mytablaTalla" name="mytablaTalla" class="ajaxSorter" border="1" style="width:935px" >
                		<thead >
                			<tr>     
                   			<th rowspan="2" data-order = "idt" >Número de Control</th>                          
                           	<th rowspan="2" data-order = "nomt" >Talla</th>
                           	<th rowspan="2" data-order = "grupo" >Grupo</th>
                           	<th colspan="3" >Precios Actuales</th>
                           	</tr>
                           	<tr>
                           	<th data-order = "prefec1" >Fecha</th>
                           	<th data-order = "premen" >Menudeo</th>
                           	<th data-order = "premay" >Mayoreo</th>
                           	</tr>
                    	</thead>
                    	<tbody title="Seleccione para realizar cambios" style="font-size: 18px; text-align: center">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
		</div>	 	
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#gpoid").change( function(){	
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/tallas/buscargpo", 
			data: "gpo="+$("#gpoid").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#gpogrl").val(obj.gpogrl);$("#grupo").val(obj.grupo);
   			} 
	});	
  //$("#sobp").focus();
	$("#camm2").focus();
	return true;
});
$("#nuevo").click( function(){	
	$("#idt").val('');$("#nomt").val('');$("#prefec").val('');$("#premen").val('');$("#premay").val('');
	$("#grupo").val('');$("#gpoid").val('0');$("#gpogrl").val('');
	$("#nomt").focus();
 return true;
})

$("#aceptar").click( function(){	
	numero=$("#idt").val();
	nom=$("#nomt").val();
	
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/tallas/actualizar", 
						data: "id="+$("#idt").val()+"&nom="+$("#nomt").val()+"&dia="+$("#prefec").val()+"&pmen="+$("#premen").val()+"&pmay="+$("#premay").val()+"&gpo="+$("#grupo").val()+"&gpoid="+$("#gpoid").val()+"&gpogrl="+$("#gpogrl").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaTalla").trigger("update");
										$("#nuevo").click();																											
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
		}else{
			if(nom!=''){
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/tallas/agregar", 
						data: "nom="+$("#nomt").val()+"&dia="+$("#prefec").val()+"&pmen="+$("#premen").val()+"&pmay="+$("#premay").val()+"&gpo="+$("#grupo").val()+"&gpoid="+$("#gpoid").val()+"&gpogrl="+$("#gpogrl").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Registrados correctamente");
										$("#mytablaTalla").trigger("update");
										$("#nuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
				}else{
			alert("Error: Registre Nombre de Talla");	
			$("#nomt").focus();
			return false;
		}
		}		
		
});

$("#prefec").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2019:+0",	
});	
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  
$(document).ready(function(){
	$("#tabla_vta").ajaxSorter({
		url:'<?php echo base_url()?>index.php/tallas/tabla',  
		//filters:['estsal'],
        //active:['Edo','Sinaloa'],  
       // multipleFilter:true,
        sort:false,
        onRowClick:function(){
           	$("#idt").val($(this).data('idt'));
			$("#nomt").val($(this).data('nomt'));
			$("#prefec").val($(this).data('prefec'));
			$("#premen").val($(this).data('premen'));
			$("#premay").val($(this).data('premay'));
			$("#grupo").val($(this).data('grupo'));
			$("#gpoid").val($(this).data('gpoid'));
			$("#gpogrl").val($(this).data('gpogrl'));
			$("#nomt").focus();			
        },
        onSuccess:function(){ 
				
		},     
	});
	
}); 



</script>
