<?php $this->load->view('header_cv'); ?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
/*ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
} */  
#export_datag { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#facoa" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/logo.png" width="20" height="20" border="0"> Facturas de Alimento </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 650px"  >
		<div id="facoa" class="tab_content" style="margin-top: 1px">
			<table width="780px" class="ajaxSorter"  border="1px">
					<tr>
						<td colspan="4">Proveedor:
							<input type="hidden" id="idag" name="idag" />
							<datalist id="prov" style="font-size:11px;width: 450px" >
                    					<option value="-"></option>
          						<?php	
          						//echo ucwords(strtolower($row->Razon));
           							$data['result']=$this->facoa_model->getProves();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->Razon;?>"></option>
           							<?php endforeach;?>
   									</datalist>
   									<input list="prov" id="prov2" type="text" style="font-size:11px;width: 470px">   
						</td>
						<td colspan="3" >Obs:
						<input type="text" name="facsal" id="facsal" style="width: 50px;"></td>
						<td rowspan="3">
							<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez"){ ?>
						<input style="font-size: 18px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
						<?php }?>
						</center>
						</td>
					</tr>
					
    				<tr>
						<td>Fecha</td><td>Factura</td><td>Descripcion</td><td>Medida (mm)</td><td>Cantidad (Ton)</td><td>T.C.</td>
						<td>Día Pago</td>
					</tr>
					<tr>
						<td style="font-size: 14px; background-color: white"><input type="text" name="fecsal" id="fecsal" style="width: 80px; border: none;" readonly="true" ></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="folsal" id="folsal" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="nomt" id="nomt" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="cajsal" id="cajsal" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="presal" id="presal" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="kgssal" id="kgssal" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="prekgs" id="prekgs" style="width: 50px;"></td>						
					</tr>    			
    		</table> 		
			<div class="ajaxSorterDiv" id="tabla_facoa" name="tabla_facoa" style="width: 920px"  >
				<div class="ajaxpager" style="margin-top: 1px;" > 
            		<ul class="order_list" style="width: 650px; text-align: left" >
            			Granja
        				<select id="estsal" style="font-size:13px; margin-top: 1px" >
                   			<option value="0">Sel</option>
                   			<option value="2">OA- Kino</option>
          					<option value="4">OA- Ahome</option>	
   						</select>      
                   	</ul>
                   	
 				</div>                 			
				<span class="ajaxTable" style="height: 480px;" >
                	<table id="mytablaVFacoa" name="mytablaFacoa" class="ajaxSorter" border="1" style="width:910px" >
                		<thead >     
                   			<th data-order = "fecsal" >Fecha</th>
                           	<th data-order = "folsal" >Folio</th>
                           	<th data-order = "noma" >Almacen</th> 
                           	<th data-order = "nomc" >Cliente</th> 
                           	<th data-order = "nomt" >Talla</th> 
                           	<!--
                           	<th data-order = "cajsal" >Cajas</th>	
                           	<th data-order = "presal" >Presentación</th>
                           	-->  
                           	<th data-order = "kgssal" >Kilogramos</th>  
                           	<th data-order = "prekgs1" >Precio M.N.</th>  
                           	<th data-order = "impvta" >Importe</th> 
                           	<th data-order = "facsal" >Factura</th>                          
                    	</thead>
                    	<tbody title="Seleccione para realizar cambios" style="font-size: 12px;">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
            	
		</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#aceptar").click( function(){	
	numero=$("#idsal").val();
	pre=$("#prekgs").val();
	if(numero!=''){
		if(pre!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/facoa/actualizar", 
						data: "id="+$("#idsal").val()+"&pre="+$("#prekgs").val()+"&fac="+$("#facsal").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaFacoa").trigger("update");
										$("#idsal").val('');$("#noma").val('');$("#nomc").val('');$("#fecsal").val('');$("#folsal").val('');$("#nomt").val('');$("#cajsal").val('');
										$("#presal").val('');$("#kgssal").val('');$("#prekgs").val('');$("#facsal").val('');																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
		}else{
			alert("Error: Registre Precio de Venta");	
			$("#prekgs").focus();
			return false;
		}			
		}else{
			alert("Error: Seleccione Venta para poder actualizar");	
			return false;
		}
});


$(document).ready(function(){
	$("#tabla_facoa").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facoa/tabla',  
		filters:['estsal'],
        //active:['Edo','Sinaloa'],  
       // multipleFilter:true,
        //sort:false,
        onRowClick:function(){
           	$("#idsal").val($(this).data('idsal'));
			$("#noma").val($(this).data('noma'));
			$("#nomc").val($(this).data('nomc'));
			$("#fecsal").val($(this).data('fecsal'));
			$("#folsal").val($(this).data('folsal'));
			$("#nomt").val($(this).data('nomt'));
			$("#cajsal").val($(this).data('cajsal'));
			$("#presal").val($(this).data('presal'));
			$("#kgssal").val($(this).data('kgssal'));
			$("#prekgs").val($(this).data('prekgs'));
			$("#facsal").val($(this).data('facsal'));
			$("#prekgs").focus();			
        }   
	});

}); 



</script>