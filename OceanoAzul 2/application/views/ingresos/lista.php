<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   

</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li style="font-size: 20px"><a href="#todos"><img src="<?php echo base_url();?>assets/images/menu/ingresos.png" width="25" height="25" border="0"> Ingresos</a> </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="todos" class="tab_content" style="height: 439px" >
			<div align="left" style="margin-left: 25px">Concentrado de Depósitos</div>
			<div class="ajaxSorterDiv" id="tabla_ing" name="tabla_ing" style="  width: 900px" >
				<span class="ajaxTable" style="height: 335px; width: 898px" >
                  	<table id="mytablaI" name="mytablaI" class="ajaxSorter" >
                        	<thead title="Presione las columnas para ordenarlas como requiera">                            
                            	<th data-order = "mes"  >Mes/Ciclo</th>                                                        							                                                                                                              
                            	<th data-order = "a"  >2016</th>
                            	<th data-order = "b"  >2015</th>
                            	<th data-order = "c"  >2014</th>
                            	<th data-order = "d"  >2013</th>
                            	<th data-order = "e"  >2012</th>
                            	<th data-order = "f"  >2011</th>
                            	<th data-order = "g"  >2010</th>
                            	<th data-order = "h"  >2009</th>
                            	<th data-order = "i"  >2008</th>
                           </thead>
                        	<tbody style="font-size: 12px">
                        	</tbody>                        	
                    	</table>
                </span>
           </div>
            	
	 	</div>
	 	<div align="left" style="margin-left: 25px">Nota: Denominación de Modena en Dólares Americanos.</div>
				
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
	$("#tabla_ing").ajaxSorter({
		url:'<?php echo base_url()?>index.php/ingresos/tabla',  	
		sort:false,
    	onSuccess:function(){
    		$('#tabla_ing tbody tr').map(function(){
		    	if($(this).data('mes')=='Total:'){
		    		$(this).css('background','lightblue');$(this).css('font-weight','bold');
		    	}
	    	})
	    	$('#tabla_ing tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold'); })
    		$('#tabla_ing tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_ing tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })	    	
	    	$('#tabla_ing tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_ing tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_ing tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_ing tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_ing tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_ing tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_ing tbody tr td:nth-child(10)').map(function(){ $(this).css('text-align','right'); })
    	},     
	});
</script>