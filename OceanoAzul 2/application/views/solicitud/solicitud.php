    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 20px 70px 20px; }
</style>  
<title>Recomendacion</title>
</head>
<body>
	<?php 
		$f=explode("-",$fecha); 
      	$nummes=(int)$f[1]; 
      	$mes1="0-Enero-Febrero-Marzo-Abril-Mayo-Junio-Julio-Agosto-Septiembre-Octubre-Noviembre-Diciembre"; 
      	$mes1=explode("-",$mes1); 
      	$desfecha="$f[2]-$mes1[$nummes]-$f[0]"; 
	?>	
<div style="margin-left: -25px">
	<table style="font-family: Verdana; font-size: 10">
		<tr>
			<th colspan="2" style="text-align: right">Mazatlán, Sinaloa  <?= $f[2]." de ".$mes1[$nummes]. " de ".$f[0]?>.</th>	
		</tr>
		<tr>
			<th colspan="2" style="text-align: left"><?= $nomsa?><br />DIRECCIÓN GENERAL DE SALUD ANIMAL<br />SENASICA</th>
		</tr>
		<tr>
			<th colspan="2" style="text-align: right"><?= $nomap?><br />DIRECCÓN DE SANIDA ACUÍCOLA Y PESQUERA<br />DGSA-SENASICA</th>
		</tr>
		<tr>
			<th colspan="2" >
				<p style="text-align: justify"> 
					CARMEN LETICIA LIZARRAGA VALDEZ, en nombre y representación de la Sociedad Mercantil denominada AQUAPACIFIC, S.A. de C.V. RFC AQU-031003-CC9 personalidad debidamente acreditada y reconocida en el expediente administrativo que esa unidad administrativa lleva de mi representada, con domicilio para recibir todo tipo de notificaciones el ubicado en Av. Emilio Barragán No.63-103 Col. Lázaro Cárdenas, Mazatlán, Sinaloa. Tel. y Fax (669) 985-6446, correo electrónico facturacion@aquapacific.com.mx , autorizando para tales efectos, así como para comparecer y firmar documentos en defensa de mi representada a los CC. Lic. Gerardo Arturo Alvarado Granados, C.P. Concepción Lerma Reyes y Lic. Fernanda Lissette Alvarado Álvarez, Tel (669) 990-38-84, correos electrónicos lisalvarado@yahoo.com y anplac@yahoo.com.mx; atentamente comparezco y expongo: <br />
					<br />De conformidad con lo establecido en el fundamento legal de los Artículos 103, 105, 107, 108, 109, 110, 111, 112, 113, 114 y 115 de la Ley General de Pesca y Acuacultura Sustentables; 35 fracciones IV y XXII de la Ley Orgánica de la  Administración Pública Federal; 15-A y 19 de la Ley Federal de Procedimiento Administrativo; 191-A fracción III de la Ley Federal de Derechos; 130 y 131 del Reglamento de la Ley de Pesca y 4.9 y 4.9.1. de la  NOM-030-PESC-2000. Por este conducto atentamente solicito la expedición del  Certificado de Sanidad Acuícola para la Movilización de 400’000,000 (cuatrocientos millones) de postlarva  de camarón blanco (Litopenaeus vannamei), producido en las instalaciones de mi representada, ubicadas en Playa El Caimanero s/n Agua Verde municipio de Rosario, Sinaloa, las granjas receptoras son:  	
					<?= $relacion?>
				</p>
				<p style="text-align: justify">
				Para tal efecto, adjunto los resultados de los análisis de postlarvas y de camarón adulto practicados por el Instituto Tecnológico de Sonora, con No. de folio: A140627229PCR y A140606178CR, respectivamente, debidamente sellados por el Comité Estatal de Sanidad Acuícola de Sinaloa, A.C. así como el comprobante del pago de derechos, Referencia No.454002245 de fecha 08 de Julio de 2014.<br />
				<br />Agradeciendo de antemano la atención que se sirva dar a la presente, quedo de usted.
				</p>				
			</th>	
		</tr>
		<tr>
			<th colspan="2" >
				<p style="text-align: center">  	
				Atentamente <br /><br /><br /><br />
				___________________________________________<br />
				CARMEN LETICIA LIZARRAGA VALDEZ<br />                                  	
				REPRESENTANTE LEGAL
				</p>
			</th>	
		</tr>	
	</table>
</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>