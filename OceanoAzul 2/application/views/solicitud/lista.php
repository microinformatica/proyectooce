<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}
#exports_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/solicitud.png" width="25" height="25" border="0"> Solicitudes para Certificados de Movilización</li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px" >
		<table style="width: 850px;" border="2px">
			<thead style="background-color: #DBE5F1">
				<th style="font-size: 14px">Registros</th>
				<th style="font-size: 14px">Datos</th>																	
				<th style="font-size: 14px">Clientes Asignados a solicitud</th>
			</thead>
			<tbody style="background-color: #F7F7F7;font-size: 12px;">
				<tr>
					<th rowspan="8">
						<div class="ajaxSorterDiv" id="tabla_solmov" name="tabla_solmov" style="width: 140px;height: 400px;"  >                 			
							<span class="ajaxTable" style="height: 400px; margin-top: 1px; " >
                    			<table id="mytablaSolMov" name="mytablaSolMov" class="ajaxSorter" border="1" style="width: 123px" >
                        			<thead>                            
                            			<th data-order = "idsolmov" >No.</th>
                            			<th data-order = "fecsol" >Fecha</th>                            			
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
					</th>
					<th style="font-size: 14px; text-align: left">
						No Solicitud:<input style="border: none" size="4%" name="idsolmov" id="idsolmov"/>
						Fecha:	<input size="10%" type="text" name="fecsol" id="fecsol" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
					</th>
					<th rowspan="5">
						<div class="ajaxSorterDiv" id="tabla_solmovcli" name="tabla_solmovcli" style="width: 390px"  >                 			
							<span class="ajaxTable" style="height: 270px; margin-top: 1px; " >
                    			<table id="mytablaSolMovCli" name="mytablaSolMovCli" class="ajaxSorter" border="1" style="width: 373px" >
                        			<thead>                            
                            			<th data-order = "Razon" >Cliente</th>
                            			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
									<input type="hidden" name="tablascli" id="html_scli"/>
							</div>       
	                	</div>
					</th>	
								
				</tr>
				<tr>
					<th style="text-align: center; width: 600px;">AT´N.<input size="50%" type="text" name="nomsa" id="nomsa" style="text-align: center;" value="MVZ JOAQUIN BRAULIO DELGADILLO ALVAREZ"><br />
						DIRECCIÓN GENERAL DE SALUD ANIMAL <br /> SENASICA
					</th>
				</tr>
				<tr>
					<th style="text-align: center; width: 600px;">AT´N.<input size="50%" type="text" name="nomap" id="nomap" style="text-align: center;" value="MVZ MAURICIO FLORES VILLASUSO"><br />
						DIRECCIÓN GENERAL DE SANIDAD ACUÍCOLA Y PESQUERA <br /> DGSA-SENASICA
					</th>
				</tr>					
				<tr>
					<th style="font-size: 14px; text-align: center ">Gestion de Procesos</th>	
				</tr>
				<tr>
					<th style="font-size: 14px; text-align: left">Cantidad de Postlarvas: <input size="13%" type="text" name="cansol" id="cansol" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" style="text-align: right" ></th>	
				</tr>
				<tr>
					<th  style="font-size: 14px; text-align: left">Expidió Resultados de Análisis:
						<select name="inspcr" id="inspcr" style="font-size: 12px; margin-top: 1px;  " >        						
          					<option value="Instituto Tecnológico de Sonora">Instituto Tecnológico de Sonora</option>
      					</select> <br />
          				Folios:<input size="58%" type="text" name="folpcr" id="folpcr" style="text-align: center;"> 
					</th>	
					<th rowspan="3">
						<div class="ajaxSorterDiv" id="tabla_clisol" name="tabla_clisol" style="width: 390px; margin-top: 1px;">                
               			     <span class="ajaxTable" style=" height: 90px; background-color: white">
                    			<table id="mytablacli" name="mytablacli" class="ajaxSorter">
                    					<thead>                            
                    						<th data-order = "Razon" >Razon Social</th>                                                                                    
                    					</thead>
                    					<tbody title="Seleccione para asignar cliente a Solicitud" style="font-size: 11px">                                                 	  
                    					</tbody>
                    				</table>
                			 </span> 
            	 		</div>   
            	 		 <input type="hidden" size="3%" name="numcli" id="numcli" style="text-align: center;">
            	 	</th>	
				</tr>
				<tr>
					<th style="font-size: 14px; text-align: left">Pago de Derechos, Ref. No:<input size="20%" type="text" name="refpag" id="refpag" style="text-align: center;"> <br /> 
					de Fecha:	<input size="10%" type="text" name="fecpag" id="fecpag" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
					</th>					
				</tr>
			</tbody>
   			<tfoot style="background-color: #DBE5F1">
   				<th  colspan="2" style="text-align: right">
   					<button id="exports_data" type="button" style=" text-align: right; width: 110px;cursor: pointer; background-color: lightyellow" >Generar Solicitud</button>
   					<input style="font-size: 14px" type="submit" id="nuevo" name="nuevo" value="Nuevo" />
					<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
					<input style="font-size: 14px" type="submit" id="borrar" name="borrar" value="Borrar" />
   					
   					<form id="data_tablasoli" action="<?= base_url()?>index.php/solicitud/pdfrepsoli" method="POST" >							
    	    	  		 <input type="hidden" name="relacion" id="relacion" style="text-align: center;">
    	    	  	  	 <input type="hidden" style="font-size: 10px" size="40%" type="text" name="fecsoli" id="fecsoli">
    	    	  	  	 <input type="hidden" style="font-size: 10px" size="40%" type="text" name="nom1" id="nom1">
    	    	  	  	 <input type="hidden" style="font-size: 10px" size="40%" type="text" name="nom2" id="nom2">
    	    	  	
    	    	  	</form>
   	    	  		<script type="text/javascript">
							$(document).ready(function(){
								$('#exports_data').click(function(){	
									$('#fecsoli').val($('#fecsol').val());
									$('#nom1').val($('#nomsa').val());
									$('#nom2').val($('#nomap').val());								
									$('#data_tablasoli').submit();
								});
							});
					</script> 
				</th>
				<th  style="text-align: center">
					<input style="font-size: 14px" type="submit" id="agrega" name="agrega" value="+" title="AGREGAR CLIENTE" />
					<input size="65%" type="text" name="nombre" id="nombre" style="text-align: center;">
					<input type="hidden" size="3%" type="text" name="idcm" id="idcm" style="text-align: center;">
					<input style="font-size: 14px" type="submit" id="quita" name="quita" value="-" title="QUITAR CLIENTE" />
				</th>		
			</tfoot>
			</table>			
		
		
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
	
function cerrar(sel){
	$(this).removeClass('used');
	$('#ver_mas_cer').hide();
}

$("#nuevo").click( function(){
	$("#idsolmov").val('');$("#fecsol").val('');$("#fecpag").val('');$("#cansol").val('');$("#folpcr").val('');$("#refpag").val('');
	$("#mytablaSolMovCli").trigger("update");
	$("#mytablacli").trigger("update");
	return true;
	
});

$("#quita").click( function(){	
	numero=$("#idcm").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/solicitud/quitar", 
						data: "id="+$("#idcm").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#numcli").val($(this).data(''));
										$("#nombre").val($(this).data(''));	
										$("#idcm").val($(this).data(''));												
										$("#mytablaSolMovCli").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Cliente Asignado para poder Eliminarlo");
		return false;
	}
});
$("#agrega").click( function(){	
	solicitud=$("#idsolmov").val();
	numero=$("#numcli").val();
	if(solicitud!=''){
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/solicitud/agrega", 
						data: "id="+$("#numcli").val()+"&sol="+$("#idsolmov").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#numcli").val($(this).data(''));
										$("#nombre").val($(this).data(''));	
										$("#idcm").val($(this).data(''));												
										$("#mytablaSolMovCli").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Razon Social para Asignar a Solicitud");
		return false;
	}
	}else{
		alert("Error: Necesita seleccionar SOLICITUD para Asignar Razon Social");
		return false;
	}
});

function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#aceptar").click( function(){		
	fecsol=$("#fecsol").val();
	numero=$("#idsolmov").val();
	if( fecsol!=''){
	  if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/solicitud/actualizar", 
						data: "id="+$("#idsolmov").val()+"&can="+$("#cansol").val()+"&fso="+$("#fecsol").val()+"&fpa="+$("#fecpag").val()+"&nsa="+$("#nomsa").val()+"&nap="+$("#nomap").val()+"&ins="+$("#inspcr").val()+"&fol="+$("#folpcr").val()+"&ref="+$("#refpag").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevo").click();
										$("#mytablaSolMov").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/solicitud/agregar", 
						data: "can="+$("#cansol").val()+"&fso="+$("#fecsol").val()+"&fpa="+$("#fecpag").val()+"&nsa="+$("#nomsa").val()+"&nap="+$("#nomap").val()+"&ins="+$("#inspcr").val()+"&fol="+$("#folpcr").val()+"&ref="+$("#refpag").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Solicitud registrada correctamente");
										$("#nuevo").click();
										$("#mytablaSolMov").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
	}else{
		alert("Error: Registre Fecha de solicitud");	
		$("#fecsol").focus();
		return false;
	}
});

/*$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});*/

$("#fecsol").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	
});
$("#fecpag").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
		
});	

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  


function noNumbers(e){
		var keynum;
		var keychar;
		var numcheck;
		if(window.event){
			jeynum = e.keyCode;
		}
		else if(e.which){
			heynum = eArray.which;
		}
		keychar = String.formCharccode(keynum);
		numcheck = /\d/;
		return !numcheck.test(keychar);
	}
		
$(document).ready(function(){ 
	$("#tabla_solmov").ajaxSorter({
		url:'<?php echo base_url()?>index.php/solicitud/tablasoli',  		
		sort:false,              
    	onRowClick:function(){
    		if($(this).data('idsolmov')>'0'){
    			$("#idsolmov").val($(this).data('idsolmov'));
   				$("#nomsa").val($(this).data('nomsa')); 
   				$("#nomap").val($(this).data('nomap'));
   				$("#fecsol").val($(this).data('fecsol'));
   				$("#fecpag").val($(this).data('fecpag'));
   				$("#cansol").val($(this).data('cansol'));
   				$("#inspcr").val($(this).data('inspcr'));
   				$("#folpcr").val($(this).data('folpcr'));
   				$("#refpag").val($(this).data('refpag'));
   				$("#tabla_solmovcli").ajaxSorter({
					url:'<?php echo base_url()?>index.php/solicitud/tablasolcsm',
					filters:['idsolmov'],	
					onRowClick:function(){
    		    		$("#numcli").val($(this).data('numero'));
						$("#nombre").val($(this).data('razon'));
						$("#idcm").val($(this).data('idclimov'));
					},
					onSuccess:function(){
						$('#tabla_solmovcli tbody tr').map(function(){
	    					$("#relacion").val($(this).data('cli'));    		
	    				});
    				}, 	
				});		
   			}
    	}, 
    	onSuccess:function(){
    	},  
	});
	
	
	
	$("#tabla_clisol").ajaxSorter({
			url:'<?php echo base_url()?>index.php/clientes/tabla', 
			multipleFilter:true, 		
			onRowClick:function(){
    		    $("#numcli").val($(this).data('numero'));
				$("#nombre").val($(this).data('razon'));
				$("#idcm").val($(this).data(''));
		    }	
	});
});  	
</script>