<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/JsBarcode-master/src/JsBarcode.all.min.js"></script>
<script>

	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 20px;
	/*background: none;*/		
}   
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>
<div style="height:550px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<?php if($usuario=="Miguel Penuelas" || $usuario=="Anita Espino"){ ?>	
		<li><a href="#registro"><img src="<?php echo base_url();?>assets/images/menu/entradas.png" width="25" height="25" border="0">Suministro Gral.</a></li>			
		<li><a href="#salida"><img src="<?php echo base_url();?>assets/images/menu/salida.png" width="25" height="25" border="0">Salidas</a></li>
		<li><a href="#deps"><img src="<?php echo base_url();?>assets/images/menu/salida.png" width="25" height="25" border="0">Deptos.</a></li>
		<li><a href="#depm"><img src="<?php echo base_url();?>assets/images/menu/salida.png" width="25" height="25" border="0">Mes</a></li>
		<?php } ?>
		<li><a href="#inv"><img src="<?php echo base_url();?>assets/images/menu/salida.png" width="25" height="25" border="0">Ent-Sal</a></li>
		<?php if($usuario=="Miguel Penuelas" || $usuario=="Anita Espino"){ ?>
		<li><a href="#ali"><img src="<?php echo base_url();?>assets/images/menu/salida.png" width="25" height="25" border="0">Alimento</a></li>
		<?php } ?>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<?php if($usuario=="Miguel Penuelas" || $usuario=="Anita Espino"){ ?>
		<div id="registro" class="tab_content" style="height: 500px">
			<div id = "accordion" style="margin-top: -10px; margin-left: -10px; width: 958px; font-size: 12px;" >
					<h3 align="left" style="font-size: 12px" ><a href="#">Registro Códigos de Barra</a></h3>
					<table style="margin-top: -10px">
					<tr>
	                	<td >	
	                		<input type="hidden" id="NumMat" name="NumMat">
	                    		CB
                				<input style="font-size:12px; width: 130px" type="text" id="CB" name="CB" autofocus >
                				Des.
                				<input type="text" name="NomMat" id="NomMat" style="text-align: left; margin-left: 2px;font-size:12px; width: 300px">
                				<!--Gpo
                				<select name="Grupo" id="Grupo" style="font-size: 10px;height: 23px;  margin-top: -10px; margin-left:5px;">
      									<option value=""  > Sel. </option>
      									<option value="airacion"  > airacion </option>
      									<option value="alimento"  > alimento </option>
      									<option value="combustible"  > combustible </option>
      									<option value="ferreteria"  > ferreteria </option>
      									<option value="laboratorio"  > laboratorio </option>
      									<option value="limpieza"  > limpieza </option>
      									<option value="oficina"  > oficina </option>
      									<option value="quimicos"  > quimicos </option>
      									<option value="vigilancia"  > vigilancia </option>
    							</select>--> 
    							Analizar
    							<select name="Analizar" id="Analizar" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
      										<option value="0" > No </option>
      										<option value="1"  > Ali </option>      									
      										<option value="2"  > Her </option>
      										<option value="3"  > Var </option>
    								  </select>	
                				<input style="font-size: 12px;cursor: pointer" type="submit" id="agregacb" name="agregacb" value="+" title="AGREGAR y ACTUALIZAR CÓDIGO DE BARRA" />
								<input style="font-size: 12px;cursor: pointer" type="submit" id="quitacb" name="quitacb" value="-" title="QUITAR CÓDIGO DE BARRA" />
								<input style="font-size: 12px;cursor: pointer" type="submit" id="nuevocb" name="nuevocb" value="Nuevo" title="REGISTRAR NUEVO CÓDIGO DE BARRA" />
								
							</td>
						</tr>					
						<tr>
							<td>
								<div >
						<div class="ajaxSorterDiv" id="tabla_cb" name="tabla_cb" style="height: 285px">                
                		<span class="ajaxTable" style="height: 250px; margin-top: 8px" >	
                		<table id="mytablaCB" name="mytablaCB" class="ajaxSorter" border="1" >
                        	<thead>  
                        		<th data-order = "CB">Código de Barras</th>
                            	<th data-order = "NomMat">Descripción</th>
                            	<th data-order = "Existencia" >Existencia</th>
                            	<!--<th data-order = "Grupo">Grupo</th>-->
                            </thead>
                        	<tbody style="font-size: 12px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="margin-top: -15px;">        
	                    	                                     
    	                	<form id="data_tables" method="post" action="<?= base_url()?>index.php/almaceninv/pdfrep" >
        	            		 <img style="cursor: pointer; margin-top: 5px" title="Enviar datos de tabla en archivo PDF" id="export_data"  src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            		 <!--<img style="cursor: pointer" style="margin-top: 6px" id="export_data" src="<?= base_url()?>assets/img/sorter/export.png" />-->
                    	    	<input type="hidden" name="prod" id="prod" style="text-align: left; margin-left: 2px;font-size:12px; width: 300px"> 
                    	    	<input type="hidden" name="tablapro" id="html_pro"/>
								<input type="hidden" name="tabladet" id="html_det"/>                     		                        		                                                      
 							</form>   
                		</div>                                      
            			</div>  
            			  <script type="text/javascript">
							$(document).ready(function(){
								$('#export_data').click(function(){									
									$('#html_pro').val($('#mytablaCB').html());
									$('#html_det').val($('#mytablaMov').html());									
									$('#data_tables').submit();
									$('#html_pro').val('');
									$('#html_det').val('');
								});
							});
						</script>          		
            	 	</div> 
							</td>
							
						</tr>
						<tr>
							<td>
								<!--
								<div class="ajaxSorterDiv" id="tabla_con" name="tabla_con" style=" height:120px; width: 920px; margin-left: -10px; ">                
                					<span class="ajaxTable" style=" height:120px;width: 900px; margin-top:1px;"  >
                    				<table id="mytablaCon" name="mytablaCon" class="ajaxSorter" border="1"  >
                        				<thead title="Presione las columnas para ordenarlas como requiera">                            
                            				<th style="text-align: center" data-order = "FecE" >Fecha</th>
                            				<th style="text-align: center" data-order = "prov" >Proveedor</th>
                            				<th style="text-align: center" data-order = "FacE" >Factura</th>
                            				<th style="text-align: center" data-order = "CanE" >Cantidad</th>
                            				<th style="text-align: center" data-order = "pres" >Medida</th>
                            				<th style="text-align: center" data-order = "PreE" >M.N.</th>                            	
											<th style="text-align: center" data-order = "DolE" >U.S.D.</th>
                            				<th style="text-align: center" data-order = "Exis" >Existencia</th>
                            			</thead>
                        				<tbody title="Seleccione para realizar cambios" style="font-size: 11px;">
                        				</tbody>                        	
                    				</table>
                					</span> 
             			         </div>
             			        -->
             			         <div class="ajaxSorterDiv" id="tabla_mov" name="tabla_mov" style=" height:120px; width: 920px; margin-left: -10px; ">                
                					<span class="ajaxTable" style=" height:120px;width: 900px; margin-top:1px;"  >
                    				<table id="mytablaMov" name="mytablaMov" class="ajaxSorter" border="1"  >
                        				<thead title="Presione las columnas para ordenarlas como requiera">                            
                            				<th style="text-align: center" data-order = "prov" >Proveedor</th>
                            				<th style="text-align: center" data-order = "FecE" >Fecha</th>
                            				<th style="text-align: center" data-order = "FacE" >Factura</th>
                            				<th style="text-align: center" data-order = "ent" >Entrada</th>
                            				<th style="text-align: center" data-order = "sal" >Salida</th>
                            				<th style="text-align: center" data-order = "Exis" >Existencia</th>
                            			</thead>
                        				<tbody title="Seleccione para realizar cambios" style="font-size: 12px;">
                        				</tbody>                        	
                    				</table>
                					</span> 
             			         </div>
							</td>
						</tr>
					</table>
					
        			<h3 align="left" style="font-size: 12px"><a href="#">Entradas Almacen</a></h3>
		       	 	<div >
		       	 		<table  class="ajaxSorter" border="1" width="890px" style="margin-top: -10px;" >
		       	 			   <thead>
		       	 			   	<td>Fecha</td>
		       	 			   	<td colspan="2">Código de Barras<input style="font-size:12px; width: 130px" type="text" id="CBE" name="CBE" autofocus >
		       	 			   		<input type="hidden" id="NumEnt" name="NumEnt">
		       	 			   		
		       	 			   	</td>
		       	 			   	<td>Proveedor</td>
		       	 			   </thead> 
		       	 			   <tr>
		       	 			   	<td><input type="text" name="FecE" id="FecE" class="fecha redondo mUp" readonly="true" style="text-align: center; width: 80px" ></td>
		       	 			   	<td colspan="2">
		       	 			   	<input readonly="true" type="text" name="NomMatE" id="NomMatE" style="text-align: left; margin-left: 2px;font-size:12px; width: 300px; border: none"></td>
		       	 			   	<td>
		       	 			   		<datalist id="prov" style="font-size:11px;width: 450px" >
                    					<option value="-"></option>
          						<?php	
          						//echo ucwords(strtolower($row->Razon));
           							$data['result']=$this->almaceninv_model->getProves();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->Razon;?>"></option>
           							<?php endforeach;?>
   									</datalist>
   									<input list="prov" id="prov2" type="text" style="font-size:11px;width: 470px">      									
		       	 			   	</td>
		       	 			   	</tr>
		       	 	</table>		   	
		       	 	<table  class="ajaxSorter" border="1" width="890px" >		   	
		       	 			   <thead>	
		       	 			   		<td>Factura</td>
		       	 			   		<td>Cantidad</td>
		       	 			   		<td>Presentación</td>
		       	 			   		<td>M.N.</td>
		       	 			   		<td>Dólar</td>
		       	 			   		<td>T.C.</td>
		       	 			   	 	<td></td>
		       	 			   </thead>  
		       	 			   <tr>
		       	 			   		<td><input style="font-size:12px; width: 80px" type="text" id="FacE" name="FacE"></td>
		       	 			   		<td><input style="font-size:12px; width: 50px" type="text" id="CanE" name="CanE"></td>
		       	 			   		<td>
		       	 			   			<datalist id="pres" style="width: 80px">
		       	 			   				<option value="-">Sin Registro</option>
		       	 			   				<option value="barra"></option><option value="bolsa"></option><option value="caja"></option>
		       	 			   				<option value="cilindro"></option><option value="cubeta"></option><option value="cuñete"></option><option value="frasco"></option>
		       	 			   				<option value="g"></option><option value="galon"></option><option value="kg"></option><option value="lata"></option>
		       	 			   				<option value="lts"></option><option value="ml"></option><option value="mts"></option><option value="paquete"></option>
		       	 			   				<option value="par"></option><option value="pza"></option><option value="rollo"></option><option value="saco">saco</option>
		       	 			   				<option value="sobre"></option><option value="tanque"><option value="tramo"></option><option value="viaje"></option>
		       	 			   			</datalist>
		       	 			   			<input list="pres" id="pres2" type="text" style="width: 100px">		       	 			   			
		       	 			   		</td>
		       	 			   		<td>$ <input style="font-size:12px; width: 50px" type="text" id="PreE" name="PreE"></td>
		       	 			   		<td>$ <input style="font-size:12px; width: 50px" type="text" id="DolE" name="DolE"></td>
		       	 			   		<td>$ <input style="font-size:12px; width: 60px" type="text" id="TcE" name="TcE"></td>
		       	 			   		<td>
		       	 			   			<input style="font-size: 12px;cursor: pointer" type="submit" id="agregae" name="agregae" value="+" title="AGREGAR y ACTUALIZAR ENTRADAS ALMACEN" />
										<input style="font-size: 12px;cursor: pointer" type="submit" id="quitae" name="quitae" value="-" title="QUITAR  ENTRADAS ALMACEN" />
										<input style="font-size: 12px;cursor: pointer" type="submit" id="nuevoe" name="nuevoe" value="Nuevo" title="REGISTRAR NUEVA ENTRADAS ALMACEN" />
		       	 			   		</td>
		       	 			   		
		       	 			   </tr>     	
						</table> 
						<div class="ajaxSorterDiv" id="tabla_ent" name="tabla_ent" style=" height: 350px; width: 920px; margin-left: -10px; ">               
                		<div class="ajaxpager" >        
	                    	<ul class="order_list"> 
	                    		
	                    	</ul> 
 							<form style="margin-top:1px" method="post" action="<?= base_url()?>index.php/almaceninv/pdfrep/" >
        	            			<img style="cursor: pointer; margin-left:1px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    		<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        		                        		                                                      
 							</form>
                		</div>    
                		<span class="ajaxTable" style="height: 320px;; width: 900px; margin-top: -25px;"  >
                    	<table id="mytablaEnt" name="mytablaEnt" class="ajaxSorter" border="1"  >
                        	<thead title="Presione las columnas para ordenarlas como requiera">                            
                            	<th style="text-align: center" data-order = "prov" >Proveedor</th>
                            	<th style="text-align: center" data-order = "NomMat" >Descripcion</th>
                            	<th style="text-align: center" data-order = "FacE" >Factura</th>
                            	<th style="text-align: center" data-order = "CanE" >Cantidad</th>
                            	<th style="text-align: center" data-order = "pres" >Medida</th>
                            	<th style="text-align: center" data-order = "PreE" >Precio</th>                            	
                          	 </thead>
                        	<tbody title="Seleccione para realizar cambios" style="font-size: 10px;">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			                                  
            			</div>
        			</div>
	 		</div>
					
					 
 	 	</div>
		<div id="salida" class="tab_content" style="height: 500px" >	
			
						<table class="ajaxSorter" border="1" width="890px" style="margin-top: -8px;">
							 <tr>	
								<td rowspan="3" style="width: 200px; margin-top: 1px">
									 <div id='ver_mas_cb' class='hide'  >
										<canvas id="barcode"></canvas>
									</div>
								</td>
								<td>Fecha</td>
								<td ><input type="text" name="FecS" id="FecS" class="fecha redondo mUp" readonly="true" style="text-align: center; width: 80px" ></td>
								<td>Cantidad</td>
								<td> <input type="text" name="CanS" id="CanS" style="font-size:12px; width: 30px"><input type="hidden" name="vta" id="vta">
		    						 <input readonly="true" type="text" name="Med" id="Med" style="font-size:12px;border: none; width: 50px">
		    					</td>
								<td> 
									<input style="font-size: 12px;cursor: pointer" type="submit" id="agregas" name="agregas" value="Agregar" title="AGREGAR y ACTUALIZAR SALIDAS ALMACEN" />
								</td>
							 </tr>  
							<tr>
								<td rowspan="2">CB</td>
								<td > <input id="CBS" name="CBS" type="text" value="CB" placeholder="Código" autofocus></td>
								<td>Precio</th>
								<td> $ <input readonly="true" type="text" name="PreS" id="PreS" style="font-size:12px;border: none; width: 50px">
		    					</td>
		    					<td> 
									<input style="font-size: 12px;cursor: pointer" type="submit" id="quitas" name="quitas" value="Eliminar" title="QUITAR SALIDAS ALMACEN" />
								</td>
							</tr>
							<tr>
								<td> <input type="hidden"  name="NumSal" id="NumSal"><input type="hidden" name="NumMatE" id="NumMatE">
		    		   				 <input readonly="true" type="text" name="NomMatS" id="NomMatS" style="text-align: left; margin-left: 2px;font-size:12px; width: 300px; border: none">
								</td>
								<td>Departamento</td>
								<td> <select name="NDepS" id="NDepS" style="font-size: 10px;height: 25px;  margin-top: 1px; margin-left:1px;">
	          						<option value="0">Seleccione</option>
    	      							<?php	
        								$data['result']=$this->almaceninv_model->getDeptos();
										foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NDepA;?>" ><?php echo $row->NomDepA;?></option>
           								<?php endforeach;?>
   									</select> 
		    					</td>
		    					<td> 
									<input style="font-size: 12px;cursor: pointer" type="submit" id="nuevos" name="nuevos" value="Nuevo" title="REGISTRAR NUEVA SALIDA ALMACEN" />
								</td>
							</tr>
							
							
						</table>
				
			 <div id='ver_mas_exis' class='hide'  >
		     <div class="ajaxSorterDiv" id="tabla_exis" name="tabla_exis" style="width: 530px; margin-top: -5px">                
               	<span class="ajaxTable" style="margin-left: -5px; background-color: white; height: 135px">
                	<table id="mytablaExis" name="mytablaExis" class="ajaxSorter">
                		<thead >                            
                			<th data-order = "FecE" >Entrada</th>
                			<th data-order = "FacE" >Fac</th>                                                                                    
                			<th data-order = "prov" >Proveedor</th>
                			<th data-order = "Exis" >Existencia</th>
                			<th data-order = "pres" >Medida</th>
                			<th data-order = "PreE" >Precio</th>
                		</thead>
                        <tbody title="Seleccione para asignar salida" style="font-size: 11px">                                                 	  
                        </tbody>
                    </table>
                </span> 
            </div>
            </div>
             <div id='ver_mas_sal' class='hide'  >
            <div class="ajaxSorterDiv" id="tabla_sal" name="tabla_sal" style="width: 830px; margin-top: -5px">                
               	<span class="ajaxTable" style="background-color: white; height: 400px;width: 825px;">
                	<table id="mytablaSal" name="mytablaSal" class="ajaxSorter" border="1">
                		<thead >                            
                			<th data-order = "FecS1" >Fecha</th>                                                                                    
                			<th data-order = "NomDepA" >Departamento</th>
                			<th data-order = "NomMat" >Descripción</th>
                			<th data-order = "CanS1" >Cantidad</th>
                			<th data-order = "PreE1" >Precio</th>
                			<th data-order = "tot" >Total</th>
                		</thead>
                        <tbody title="Seleccione para asignar salida" style="font-size: 11px">                                                 	  
                        </tbody>
                    </table>
                </span> 
            </div>
             </div>
		</div>	
		<div id="deps" class="tab_content" style="height: 500px" >
			 <div class="ajaxSorterDiv" id="tabla_dep" name="tabla_dep" style="width: 830px; margin-top: -5px">  
			 	<div style=" text-align:left; font-size: 12px;" class='filter' >
                		Departamento:
                		<select name="deptos" id="deptos" style="font-size: 10px;height: 25px;  margin-top: 1px; margin-left:1px;">
	          				<option value="0">Seleccione</option>
    	      				<?php	
        					$data['result']=$this->almaceninv_model->getDeptos();
							foreach ($data['result'] as $row):?>
           					<option value="<?php echo $row->NDepA;?>" ><?php echo $row->NomDepA;?></option>
           					<?php endforeach;?>
   						</select>    
   						Del día
   						<input type="text" name="ini" id="ini" class="fecha redondo mUp" readonly="true" style="text-align: center; width: 80px" >
   						Al 
   						<input type="text" name="fin" id="fin" class="fecha redondo mUp" readonly="true" style="text-align: center; width: 80px" >
   						Insumo
   						<select name='insumo' id="insumo" style="font-size: 11px;height: 25px;margin-top: 1px;"  ></select>   						
                </div> 
			 	          
               	<span class="ajaxTable" style="background-color: white; height: 400px;width: 825px;">
                	<table id="mytablaDep" name="mytablaDep" class="ajaxSorter" border="1">
                		<thead >                            
                			<th data-order = "NomDepA" >Departamento</th>
                			<th data-order = "FecS1" >Fecha</th>                                                                                    
                			<th data-order = "NomMat" >Descripción</th>
                			<th data-order = "CanS1" >Cantidad</th>
                			<th data-order = "PreE1" >Precio</th>
                			<th data-order = "tot" >Total</th>
                		</thead>
                        <tbody title="Seleccione para asignar salida" style="font-size: 11px">                                                 	  
                        </tbody>
                    </table>
                </span> 
            </div>
		</div>			
		<div id="depm" class="tab_content" style="height: 500px" >
			 <div class="ajaxSorterDiv" id="tabla_depm" name="tabla_depm" style="width: 830px; margin-top: -5px">  
			 	<div style=" text-align:left; font-size: 12px;" class='filter' >
                	<div class="ajaxpager" style="margin-top: 1px; text-align: left">        
	                   	<ul style="width: 300px; text-align: left" class="order_list">
	                   		Detalle de Salidas de Almacen Mensual
	                   	</ul>                                            
    	               	<form method="post" action="<?= base_url()?>index.php/almaceninv/pdfrepmes/" >
    	               		Mes:
    	              	 		<select name="cmbMesS" id="cmbMesS" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
		   							<option value="11" >Noviembre</option>
   									<option value="12" >Diciembre</option>
   									<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   									<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   									<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   									<option value="10" >Octubre</option>
   								</select>	
   							Departamento:
                				<select name="deptosm" id="deptosm" style="font-size: 10px;height: 25px;  margin-top: 1px; margin-left:1px;">
	          						<option value="0">Todos</option>
    	      						<?php	
        								$data['result']=$this->almaceninv_model->getDeptos();
										foreach ($data['result'] as $row):?>
           							<option value="<?php echo $row->NDepA;?>" ><?php echo $row->NomDepA;?></option>
           							<?php endforeach;?>
   								</select>  
   								<select name="cmbTD" id="cmbTD" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
		   							<option value="0" >Totales</option>
   									<option value="1" >Detalle</option>   									
   								</select>
        	            	<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    <input type="hidden" name="tablames" value ="" class="htmlTable"/>                        		                        		                                                      
 						</form>
 						   
                	</div>	
                </div> 
			   	<span class="ajaxTable" style="background-color: white; height: 450px;width: 825px; margin-top: -10px">
                	<table id="mytablaDepM" name="mytablaDepM" class="ajaxSorter" border="1">
                		<thead >                            
                			<th data-order = "NomDepA" >Departamento</th>
                			<th data-order = "NomMat" >Descripción</th>
                			<th data-order = "CanS1" >Salidas</th>
                			<th data-order = "DolE1" >U.S.D.</th>
                			<th data-order = "TcE1" >T.C.</th>
                			<th data-order = "PreE1" >M.N.</th>
                			<th data-order = "tot" >Total</th>
                		</thead>
                        <tbody style="font-size: 11px">                                                 	  
                        </tbody>
                    </table>
                    
                </span> 
            </div>
		</div>
		<?php } ?>
		<div id="inv" class="tab_content" style="height: 500px; width: 955px" >
			 <div class="ajaxSorterDiv" id="tabla_inv" name="tabla_inv" style="width: 950px; margin-top: -5px; margin-left: -5px">  
			 	<div style=" text-align:left; font-size: 12px;" class='filter' >
                	<div class="ajaxpager" style="margin-top: 1px; text-align: left">        
	                   	<ul style="width: 300px; text-align: left" class="order_list">
	                   		Existencia Actual
	                   	</ul>                                            
    	               	<!--<form method="post" action="<?= base_url()?>index.php/almaceninv/pdfrepinv/" >-->
    	               	<form method="post" action="<?= base_url()?>index.php/almaceninv/reportedet">	
    	               		Ciclo:
    	               			<select name="cmbCicI" id="cmbCicI" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">
    	               				<option value="2022" >2022</option>
    	               			</select>	
    	               		Mes:
    	              	 		<select name="cmbMesI" id="cmbMesI" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
		   							<option value="11" >Noviembre</option>
   									<option value="12" >Diciembre</option>
   									<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   									<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   									<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   									<option value="10" >Octubre</option>
   								</select>	
   							<select name="cmbTDE" id="cmbTDE" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
		   							<option value="1" >Detalle</option>
		   							<option value="0" >Totales</option>   									   									
   							</select>
   							Solo Alimentos
   							<select name="cmbSA" id="cmbSA" style="font-size: 10px;height: 23px; width: 50px;  margin-top: 1px; margin-left:4px;">   						
		   							<option value="0" >No</option>
		   							<option value="1" >Si</option>   									   									
   							</select>	
   							<!--<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />-->
                    	     <img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
                    	    <input type="hidden" name="tablainv" value ="" class="htmlTable"/>                        		                        		                                                      
 						</form>
 					</div>	
                </div> 
			   	<span class="ajaxTable" style="background-color: white; height: 450px;width: 945px; margin-top: -10px">
                	<table id="mytablaInv" name="mytablaInv" class="ajaxSorter" border="1">
                		<thead style="font-size: 14px">                            
                			<!--<th data-order = "Grupo" >Grupo</th>-->
                			<th data-order = "NomMat" >Descripción</th>
                			<th data-order = "prov" >Fecha - Proveedor</th>
                			<th data-order = "fac" >Fac</th>
                			<th data-order = "pre" style="width: 60px" >Costo<br />Unitario</th>
                			<th data-order = "CanE1" >Entradas</th>
                			<th data-order = "CanS1" >Salidas</th>
                			<th data-order = "cm" >Costo del <br />Movimiento</th>
                			<th data-order = "ca"style="width: 65px" >Costo del <br />Almacen</th>
                			<!--<th data-order = "Exis" >Existencia</th>-->
                		</thead>
                        <tbody style="font-size: 11px; text-align: right">                                                 	  
                        </tbody>
                    </table>
                    
                </span> 
            </div>
		</div>
		<?php if($usuario=="Miguel Penuelas" || $usuario=="Anita Espino"){ ?>
		<div id="ali" class="tab_content" style="height: 500px; width: 955px" >
			<div class="ajaxSorterDiv" id="tabla_ali" name="tabla_ali" style="width: 950px; margin-top: -20px; margin-left: -5px;">  
			 	<div style=" text-align:left; font-size: 12px;" class='filter' >
                	<div class="ajaxpager" style="margin-top: 1px; text-align: left">        
	                   	<ul style="width: 950px; text-align: left; height: 35px" class="order_list">
	                   		Consumo de Alimento
	                                                              
    	               	<form method="post" action="<?= base_url()?>index.php/almaceninv/reporte">
    	               		Mes:
    	              	 		<select name="cmbMesA" id="cmbMesA" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px; width: 80px">   						
		   							<option value="11" >Noviembre</option>
   									<option value="12" >Diciembre</option>
   									<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   									<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   									<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   									<option value="10" >Octubre</option>
   								</select>	
   							Granja:
   							<select name="cmbGranja" id="cmbGranja" style="font-size: 10px; margin-top: 1px; height: 25px; width: 65px " >								
    							<option value="4">Ahome</option>
    						</select>	
   							Sección:	
   							<select name="secc" id="secc" style="font-size: 10px; height: 23px;margin-top: 1px; width: 45px  " >								
    								<option value="0">Sel.</option>
    								<option value="41">1</option>		
    								<option value="42">2</option>
    								<option value="43">3</option>
    								<option value="44">4</option>
    						</select>
    						Alimento:
    						<select name="alimento" id="alimento" style="font-size: 10px;height: 25px;  margin-top: 1px; margin-left:1px; width: 350px">
	          						<option value="0">Todos</option>
    	      						<?php	
        								$data['result']=$this->almaceninv_model->getAlimento();
										foreach ($data['result'] as $row):?>
           							<option value="<?php echo $row->NumMat;?>" ><?php echo $row->NomMat.'- Fac:'.$row->FacE.'- Exis: '.$row->Exis;?></option>
           							<?php endforeach;?>
   							</select>  
   							<!--<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />-->
                    	   <img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
                    	    <input type="hidden" name="tablaali" value ="" class="htmlTable"/>                        		                        		                                                      
 						</form>
 					</ul> 
 					</div>	
                </div> 
			   	<span class="ajaxTable" style="background-color: white; height: 480px;width: 945px; margin-top: -1px">
                	<table id="mytablaAli" name="mytablaAli" class="ajaxSorter" border="1">
                		<thead style="font-size: 9px">                            
                			<th style="font-size: 9px" data-order = "pisg" >Est</th>
                			<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
           					<th style="font-size: 9px" data-order = "tot" >Tot</th>
                		</thead>
                        <tbody style="font-size: 10px">                                                 	  
                        </tbody>
                    </table>
                    
                </span> 
            </div>
		</div>	
		<?php } ?>
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#CBS").change( function(){	
	cb1=$("#CBS").val();
 	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/almaceninv/buscarcb", 
			data: "cb="+$("#CBS").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
  					if(obj.des!=0){	
						$("#NomMatS").val(obj.des);
						$('#ver_mas_sal').hide();$('#ver_mas_exis').show();$('#ver_mas_cb').show();
						 
					}
					else{
						//alert('Sin Registro de Código de Barras');
						$('#ver_mas_exis').hide();$('#ver_mas_sal').show();
						$("#CBS").val('');$("#NomMatS").val('');$('#ver_mas_cb').hide();
						cb1=' ';	
						
					}
					$("#mytablaSal").trigger("update");
					$("#mytablaExis").trigger("update");
					$("#barcode").JsBarcode( cb1,
						{width: 1,height: 40,quite: 10,format: "CODE128",displayValue: true,fontOptions: "",font:"monospace",textAlign:"center",fontSize: 10,backgroundColor:"",
						lineColor:"#000"});		
						//CODE128 - EAN13	
								
   			} 
	});			 
});
$("#nuevos").click( function(){	
	 $("#CBS").val('');$("#CBS").focus();$("#CBS").change(); 
	$('#ver_mas_exis').hide();$('#ver_mas_sal').show();	
	$("#CBS").removeAttr("disabled"); 
	
	$("#NumSal").val('');$("#NumMatE").val('');
    $("#CanS").val(''); //$("#NDepS").val('');
    $("#NomMatS").val('');$("#Med").val('');$("#PreS").val('');
    var f = new Date();
    $("#FecS").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
   
    return true;
});
$("#quitas").click( function(){	
	numero=$("#NumSal").val();
	if(numero!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almaceninv/borrar", 
				data: "id="+$("#NumSal").val()+"&mat="+$("#NumMatE").val()+"&can="+$("#CanS").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								alert("Datos eliminados correctamente");										
								$("#mytablaSal").trigger("update");
								$("#mytablaExis").trigger("update");
								$('#ver_mas_sal').show();
								$("#nuevos").click(); 															
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder eliminar datos");				
		return false;
	}	 
});
$("#agregas").click( function(){	
	num=$("#NumSal").val();cb=$("#CBS").val();can=$("#CanS").val();pre=$("#PreS").val();
	if(cb!=''){
	  if(can!=''){
	  	if(pre!=''){
			if(num!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/almaceninv/actualizars", 
						data: "id="+$("#NumSal").val()+"&fec="+$("#FecS").val()+"&mat="+$("#NumMatE").val()+"&can="+$("#CanS").val()+"&dep="+$("#NDepS").val()+"&sal="+$("#vta").val(),
						success: 	
								function(msg){	
									obj=null;
  									var obj = jQuery.parseJSON( msg );
  									if(obj.msg==1){															
										alert("Datos actualizados correctamente");										
										$("#mytablaSal").trigger("update");
										$("#mytablaExis").trigger("update");
										$('#ver_mas_sal').show();
										$("#nuevos").click();
																																				
									}else{
										if(obj.msg==0)
										alert("Cantidad de Salida Sobrepasa la Existencia");
										else
										alert("CANTIDAD INCORRECTA");
										$("#CanS").focus();
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/almaceninv/agregars", 
						data: "&fec="+$("#FecS").val()+"&mat="+$("#NumMatE").val()+"&can="+$("#CanS").val()+"&dep="+$("#NDepS").val()+"&cb="+$("#CBS").val(),
						success: 
								function(msg){	
									obj=null;
  									var obj = jQuery.parseJSON( msg );
  									if(obj.msg==1){															
									//if(msg!=0){														
										alert("Salida en Almacen Registrada Con Éxito");
										$("#mytablaSal").trigger("update");
										$("#mytablaExis").trigger("update");
										$('#ver_mas_sal').show();		
										$("#nuevos").click();											
									}else{
										if(obj.msg==0)
										alert("Cantidad de Salida Sobrepasa la Existencia");
									}					
								}		
				});		
			}
		}else{
		alert("Error: Seleccione de la Tabla de Existencia");$("#PreS").focus();return false;
	  }		
	  }else{
		alert("Error: Registre Cantidad");$("#CanS").focus();return false;
	  }			
	}else{
		alert("Error: Registre Código de Barras");$("#CBS").focus();return false;
	}
});



$("#nuevoe").click( function(){	
	$('#ver_mas_pres').show();$('#ver_mas_prov').show();		
	$("#NumEnt").val('');$("#CBE").val('');$("#NomMatE").val('');$("#prov2").val('');$("#prov1").val('');$("#FecE").val('');
    $("#FacE").val('');$("#CanE").val('');$("#pres2").val('');$("#pres1").val('');$("#PreE").val('');
    $("#DolE").val('');$("#TcE").val('');
    $('#ver_mas_pres').hide();$('#ver_mas_pres1').show();$('#ver_mas_prov').hide();$('#ver_mas_prov1').show();
	return true;
});

$("#agregae").click( function(){	
	num=$("#NumEnt").val();cb=$("#CBE").val();pro=$("#prov").val();can=$("#CanE").val();
	if(cb!=''){
	 //if(pro!=''){
	  if(can!=''){
			if(num!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/almaceninv/actualizare", 
						data: "id="+$("#NumEnt").val()+"&fec="+$("#FecE").val()+"&pro="+$("#prov2").val()+"&fac="+$("#FacE").val()+"&can="+$("#CanE").val()+"&pres="+$("#pres2").val()+"&pre="+$("#PreE").val()+"&dol="+$("#DolE").val()+"&tc="+$("#TcE").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaEnt").trigger("update");
										$("#nuevoe").click();																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/almaceninv/agregare", 
						data: "&cb="+$("#CBE").val()+"&fec="+$("#FecE").val()+"&pro="+$("#prov2").val()+"&fac="+$("#FacE").val()+"&can="+$("#CanE").val()+"&pres="+$("#pres2").val()+"&pre="+$("#PreE").val()+"&dol="+$("#DolE").val()+"&tc="+$("#TcE").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Entrada en Almacen Registrada Con Éxito");
										$("#mytablaEnt").trigger("update");
										$("#nuevoe").click();											
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});		
			}
	  }else{
		alert("Error: Registre Cantidad");$("#CanE").focus();return false;
	  }		
	}else{
		alert("Error: Registre Código de Barras");$("#CBE").focus();return false;
	}
});



$("#nuevocb").click( function(){	
	$("#NumMat").val('');$("#CB").val('');$("#NomMat").val('');$("#Analizar").val('');$("#pro").val('');
	$("#CB").removeAttr("disabled");
    return true;
});
$("#agregacb").click( function(){	
	num=$("#NumMat").val();cb=$("#CB").val();nom=$("#NomMat").val();
	if(cb!=''){
		if(nom!=''){
			if(num!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/almaceninv/actualizarcb", 
						data: "id="+$("#NumMat").val()+"&nom="+$("#NomMat").val()+"&ana="+$("#Analizar").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaCB").trigger("update");
										$("#nuevocb").click();																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				//Buscar que el codigo de barras no este duplicado antes de ser registrado
				$.ajax({
					type: "POST",//Envio
					url: "<?=base_url()?>index.php/almaceninv/buscarcb", 
					data: "cb="+$("#CB").val(),
					success: function(datos){
						obj=null;
  						var obj = jQuery.parseJSON( datos );
  						if(obj.des!=0){	
							//$("#NomMatE").val(obj.des);
							$("#CB").focus();
							alert('Código de Barras ya REGISTRADO - '+obj.des);
						}else{
							$.ajax({
								type: "POST",//Envio
								url: "<?=base_url()?>index.php/almaceninv/agregarcb", 
								data: "&cb="+$("#CB").val()+"&nom="+$("#NomMat").val()+"&ana="+$("#Analizar").val(),
								success: 
									function(msg){															
										if(msg!=0){														
											alert("CB registrado correctamente");
											$("#mytablaCB").trigger("update");
											$("#nuevocb").click();											
										}else{
											alert("Error con la base de datos o usted no ha ingresado nada");
										}					
									}		
							});
							
						}					
   					} 
				});		
				
			}
						
		}else{
			alert("Error: Registre Descripción");$("#NomMat").focus();return false;
		}
	}else{
		alert("Error: Registre Código de Barras");$("#CB").focus();return false;
	}
});





$("#CBE").change( function(){	
 	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/almaceninv/buscarcb", 
			data: "cb="+$("#CBE").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
  					if(obj.des!=0){	
						$("#NomMatE").val(obj.des);
					}
					else{
						alert('Sin Registro de Código de Barras');
						$("#CBE").val('');$("#NomMatE").val('');
					}					
   			} 
	});			 
});


$("#FecE").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$('#tabla_ent').trigger('update');
	}
});
$("#ini").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
	onSelect: function( selectedDate ){
		$("#fin").datepicker( "option", "minDate", selectedDate );
		$('#tabla_dep').trigger('update');
		/*$('#tabla_cvimp').trigger('update')*/
	}
});
$("#fin").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
	onSelect: function( selectedDate ){
		$("#ini").datepicker( "option", "maxDate", selectedDate );
		$('#tabla_dep').trigger('update');
		/*$('#tabla_cvimp').trigger('update')*/
	}
});
$("#FecS").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$('#tabla_sal').trigger('update');
		$('#tabla_vta').trigger('update');
	}
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});

sumaFecha = function(d, fecha){
 var Fecha = new Date();
 var sFecha = fecha || (Fecha.getDate() + "-" + (Fecha.getMonth() +1) + "-" + Fecha.getFullYear());
 var sep = sFecha.indexOf('-') != -1 ? '-' : '-'; 
 var aFecha = sFecha.split(sep);
 var fecha = aFecha[2]+'-'+aFecha[1]+'-'+aFecha[0];
 fecha= new Date(fecha);
 fecha.setDate(fecha.getDate()+parseInt(d));
 var anno=fecha.getFullYear();
 var mes= fecha.getMonth()+1;
 var dia= fecha.getDate();
 mes = (mes < 10) ? ("0" + mes) : mes;
 dia = (dia < 10) ? ("0" + dia) : dia;
 var fechaFinal = anno+sep+mes+sep+dia;
 return (fechaFinal);
}
$(document).ready(function(){
	var f = new Date(); 
    $("#cmbMesS").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));
    $("#cmbMesI").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));
    $("#cmbMesA").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));
	$('#ver_mas_pres1').hide();$('#ver_mas_prov1').hide();$('#ver_mas_exis').hide();
	$("#CBS").change();$("#CBS").focus();
	var f = new Date();
	$("#FecE").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#FecS").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	
    //Restar dia a la fecha actual
	var fecha = sumaFecha(-6,f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
	$("#ini").val(fecha);$("#fin").val($("#FecS").val());
	$("#tabla_cb").ajaxSorter({
	url:'<?php echo base_url()?>index.php/almaceninv/tabla',  
	multipleFilter:true, 
   	//onLoad:false,
   	sort:false,		
	//filters:['cmbCiclo','cmbPM','cmbMU','buscar'],	
	active:['Analizar',1,'>='],
    onRowClick:function(){
    	if($(this).data('nummat')>'0'){
        	$("#NumMat").val($(this).data('nummat'));
        	$("#CB").val($(this).data('cb'));
        	$("#NomMat").val($(this).data('nommat'));$("#prod").val($(this).data('nommat'));
        	$("#Grupo").val($(this).data('grupo'));
        	$("#Analizar").val($(this).data('analizar'));
        	$("#CB").attr('disabled', 'disabled');
        	$("#tabla_mov").ajaxSorter({
				url:'<?php echo base_url()?>index.php/almaceninv/tablamov',  
				sort:false,		
				filters:['CB'],	
				 onSuccess:function(){
    				$('#tabla_mov tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })   		
	    			$('#tabla_mov tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','center'); })
	    			$('#tabla_mov tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','center'); })
				},  
   			});
        	
		}		
    }, 
    onSuccess:function(){
    		$('#tabla_cb tbody tr').map(function(){
	    		if($(this).data('anterior')=='')
	    			$(this).css('background','lightblue'); //$(this).css('font-weight','bold');   		
	    	});
	    	
	},     
	});
	$("#tabla_ent").ajaxSorter({
	url:'<?php echo base_url()?>index.php/almaceninv/tablaent',  
	sort:false,		
	filters:['FecE'],	
	onRowClick:function(){
    	if($(this).data('nument')>'0'){
        	$("#NumEnt").val($(this).data('nument'));$("#CBE").val($(this).data('cbe'));$("#NomMatE").val($(this).data('nommat'));
        	$("#prov2").val($(this).data('prov'));$("#FecE").val($(this).data('fece'));
        	$("#FacE").val($(this).data('face'));$("#CanE").val($(this).data('cane'));$("#pres2").val($(this).data('pres'));
        	$("#PreE").val($(this).data('pree'));$("#DolE").val($(this).data('dole'));
        	$("#TcE").val($(this).data('tce'));$('#ver_mas_pres').hide();$('#ver_mas_pres1').show();
        	$('#ver_mas_prov').hide();$('#ver_mas_prov1').show();
		}		
    }, 
   });
	$("#tabla_exis").ajaxSorter({
	url:'<?php echo base_url()?>index.php/almaceninv/tablaexis',  
	sort:false,		
	filters:['CBS'],	
	onRowClick:function(){
    	if($(this).data('nument')>'0'){
        	$("#FacS").val($(this).data('face'));//$("#CanS").val($(this).data('exis'));
        	$("#PreS").val($(this).data('pree'));$("#Med").val($(this).data('pres'));$("#NumMatE").val($(this).data('nument'));
        	$('#ver_mas_exis').hide();   
        	$("#CanS").focus();     	
		}		
    }, 
    
   });
   $("#tabla_sal").ajaxSorter({
	url:'<?php echo base_url()?>index.php/almaceninv/tablasal',  
	sort:false,		
	filters:['FecS'],	
	onRowClick:function(){
    	if($(this).data('numsal')>'0'){
        	//$("#FacS").val($(this).data('face'));
        	$("#CanS").val($(this).data('cans'));$("#CBS").val($(this).data('cbs')); $("#CBS").change();
        	$("#PreS").val($(this).data('pree'));$("#Med").val($(this).data('pres'));$("#NDepS").val($(this).data('ndepa'));
        	$("#NumSal").val($(this).data('numsal'));$("#NumMatE").val($(this).data('nummate'));$("#FecS").val($(this).data('fecs'));
        	$("#NomMatS").val($(this).data('nommat'));$("#CBS").attr('disabled', 'disabled');$("#vta").val($(this).data('cans'));
        	$('#ver_mas_sal').hide();
        	$('#ver_mas_exis').hide();        	
        	
		}		
    }, 
   });
   $("#tabla_inv").ajaxSorter({
	url:'<?php echo base_url()?>index.php/almaceninv/tablainv',  
	sort:false,		
	filters:['cmbCicI','cmbMesI','cmbTDE','cmbSA'],	
	onSuccess:function(){
			$('#tabla_inv tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','left'); })
			$('#tabla_inv tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','left'); })
    		$('#tabla_inv tbody tr').map(function(){
		    	if($(this).data('nommat')!='') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    		}
	    	});
	},  
   });
   $("#tabla_dep").ajaxSorter({
	url:'<?php echo base_url()?>index.php/almaceninv/tabladep',  
	sort:false,		
	filters:['deptos','ini','fin','insumo'],	
	onSuccess:function(){
    		$('#tabla_dep tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
	},  
   });
   $("#tabla_depm").ajaxSorter({
	url:'<?php echo base_url()?>index.php/almaceninv/tabladepm',  
	sort:false,		
	filters:['cmbMesS','deptosm','cmbTD'],	
	onSuccess:function(){
    		$('#tabla_depm tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold'); })
    		$('#tabla_depm tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold'); })
    		$('#tabla_depm tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_depm tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_depm tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_depm tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_depm tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_depm tbody tr').map(function(){
		    	if($(this).data('nomdepa')=='Total:') {	    				    			
	    			$(this).css('background','lightgray');
	    			$(this).css('font-weight','bold');
	    			//$(this).css('text-align','center');
	    		}
	    		if($(this).data('pree1')=='') {	    				    			
	    			//$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','right');
	    		}
	    		if($(this).data('co')=='1') {	    				    			
	    			$(this).css('background','lightblue');
	    			
	    		}
		    });
	},  
   });
   $("#tabla_ali").ajaxSorter({
	url:'<?php echo base_url()?>index.php/almaceninv/tablaali',  
	sort:false,		
	filters:['cmbMesA','cmbGranja','secc','alimento'],	
	onSuccess:function(){
		$('#tabla_ali tbody tr td:nth-child(1)').map(function(){$(this).css('background','lightblue'); $(this).css('font-weight','bold'); })
		$('#tabla_ali tbody tr td:nth-child(33)').map(function(){ $(this).css('background','lightblue');$(this).css('font-weight','bold'); })
   		 $('#tabla_ali tbody tr').map(function(){
		   	if($(this).data('pisg')=='Tot') {	    				    			
	    		$(this).css('background','lightblue');
	    		$(this).css('font-weight','bold');
	    	}
	    });		
	},  
   });

   $('#insumo').boxLoader({
            url:'<?php echo base_url()?>index.php/almaceninv/combosecc',
            //equal:{id:'seccp',value:'#seccp'},
            equal:{id:'deptos',value:'#deptos'},
            select:{id:'CBS',val:'val'},      
            all:"Sel."
    });

});


</script>