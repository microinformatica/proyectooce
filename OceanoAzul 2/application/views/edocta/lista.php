<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
#export_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/abonos.png" width="25" height="25" border="0"> Estado de Cuenta </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 650px">
		<div id="ventas" class="tab_content" style="margin-top: -10px">
			<div class="ajaxSorterDiv" id="tabla_ect" name="tabla_ect" style="width: 920px"  >
				<div class="ajaxpager" style="margin-top: 1px;" > 
					<ul class="order_list" style="width: 650px; text-align: left; " >
            			Cliente:
        				<select <?php if($usuario!="Jesus Benítez" && $usuario!="Efrain Lizárraga" && $usuario!="Zuleima Benitez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="clisal" id="clisal" style="font-size: 12px;height: 25px;  margin-top: 1px;">
                    		<option value="0">Seleccione</option>
          						<?php	
           						$data['result']=$this->edocta_model->verClientes();
								foreach ($data['result'] as $row):?>
           							<option value="<?php echo $row->Numero;?>" ><?php echo $row->Razon;?></option>
           						<?php endforeach;?>
   						</select> 
   						Mes:
    	              	<select name="cmbMes" id="cmbMes" style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
    	              		<option value="0" >Todos</option>   						
		   					<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   							<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   							<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   							<option value="10" >Octubre</option><option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   						</select>
   						<button id="export_data" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right;  margin-left: -5px"  >Imprimir</button>	       
                   	</ul>
                   	<form id="data_tableedc" action="<?= base_url()?>index.php/edocta/pdfrepedc" method="POST">
                   		<input type="hidden" name="tabla" id="html_edc"/>
                   		<input type="hidden" name="cli" id="cli"/><input type="hidden" name="mes" id="mes"/>
					</form>
					
					<script type="text/javascript">
					$(document).ready(function(){
						$('#export_data').click(function(){									
								$('#html_edc').val($('#mytablaEdc').html());
								$('#data_tableedc').submit();
								$('#html_edc').val('');
								
						});
					});
				</script>
 				</div>                 			
				<span class="ajaxTable" style="height: 450px;width:920px;" >
                	<table id="mytablaEdc" name="mytablaEdc" class="ajaxSorter" border="1" style="width:900px;" >
                		<thead >     
                			<tr>
                   			
                   			 
                           	<th colspan="5">VENTAS</th> 
                           	<th colspan="2"></th>
                           	
                          
                           <!--	<th colspan="4">USD</th>--> 
                           	</tr>  
                           	<tr>
                           	<th data-order = "feccos" >Fecha</th>
                           	<th data-order = "folio" >Folio</th>
                           	<!--<th rowspan="2" data-order = "talla" >Talla</th>-->
                           	<th data-order = "kgscos" >Kgs</th>
                   			<th data-order = "prebas" >Precio Prom</th> 
                           	<!--
                           	<th data-order = "fac" >Factura</th>
                           	-->
                           	<th data-order = "car" >Importe</th> 
                            <th data-order = "abo" >Depósito</th>  
                           	<th data-order = "sal" >Saldo</th>
                           	<!--
                           	<th data-order = "usdkgs" >Precio</th> 
                           	<!--
                           	<th data-order = "fac" >Factura</th>
                           	
                           	<th data-order = "caru" >Venta</th> 
                           	<th data-order = "abou" >Depósito</th>  
                           	<th data-order = "salu" >Saldo</th>-->
                           	</tr>
                        </thead>
                    	<tbody title="Seleccione para realizar cambios" style="font-size: 12px; text-align: center">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
             
       			<table  style="width: 875px;" border="2px" >
							<thead style="background-color: #DBE5F1">
								<th colspan="4" height="15px" style="font-size: 16px">Registro y Actualización de Descuentos 
									Aplicados a Granja:
									<select id="cmbGra" style="font-size:13px; margin-top: 1px" >
                   						<option value="0">Sel.</option>
                   						<option value="2">OA- Kino</option>
                   						<option value="4">OA- Ahome</option>
                   						<option value="10">OA- Topolobampo</option>
   										<option value="6" >OA- Huatabampo</option>
          							</select> 
								<input size="8%" type="hidden"  name="idd" id="idd"/>																
								</th>
								<th colspan="2">
									<center>
									<?php if($usuario=="Jesus Benítez" || $usuario=="Lety Lizárraga" || $usuario=="Efrain Lizárraga"  || $usuario=="Zuleima Benitez"){ ?>
									<input style="font-size: 14px" type="submit" id="nuevod" name="nuevod" value="Nuevo" />
									<input style="font-size: 14px" type="submit" id="aceptard" name="aceptard" value="Guardar" />									
									<?php }?>	
									<?php if($usuario=="Jesus Benítez" || $usuario=="Lety Lizárraga" || $usuario=="Zuleima Benitez"){ ?>									
									<input style="font-size: 14px" type="submit" id="borrard" name="borrard" value="Borrar" />
									<?php }?>	
									</center>	
								</th>
							 </thead>
							 <tbody style="background-color: #F7F7F7">
								<tr>
									<th><label style="margin: 2px; font-size: 14px" for="txtFecha">Fecha</label></th>
									<th><label style="margin: 2px; font-size: 14px" for="mn">Pesos $</label></th>
									<th><label style="margin: 2px; font-size: 14px;" for="usd" >Dólares $</label></th>	
									<th><label style="margin: 2px; font-size: 14px" for="tc">Tipo de Cambio</label></th>
									<th><label style="margin: 2px; font-size: 14px" for="cta">Cuenta</label></th>																	
									<th><label style="margin: 2px; font-size: 14px" for="pro">Aplicar a Ciclo</label></th>
								</tr>
    							<tr style="font-size: 14px; background-color: white;">
									<th><input size="14%" type="text" name="txtFechad" id="txtFechad" class="fecha redondo mUp" readonly="true" style="text-align: center; margin-left: 10px" ></th>								
									<th><input size="14%" type="text" name="mn" id="mn" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right; margin-left: 10px"></th>
									<th><input size="14%" type="text" name="usd" id="usd" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right; margin-left: 10px"></th>									
									<th><input size="8%" type="text" name="tc" id="tc" style="text-align: right; margin-left: 10px"></th>
									<th><input size="10%" type="text" name="cta" id="cta" value="" style="text-align: center; margin-left: 10px"></th>
									<th style="font-size: 13px;padding-bottom: 1px;">									  																	
									    <select name="lstCiclo" id="lstCiclo" style="font-size: 10px; margin-top: 1px; margin-left: 12px; height: 23px;" >
											<?php $ciclof=2018; $actual=date("Y"); 
											$mes=date("m");
											if($mes>=11) $actual+=1;											
											while($actual >= $ciclof){?>
												<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            									<?php $actual-=1; } ?>
          								</select>   								  
									</th>
								</tr>
    							<tr>
									<th ><label style="margin: 2px; font-size: 14px" for="obsd">Estatus <input size="2%" type="hidden"  name="est" id="est" readonly="true" ></label>
									<th colspan="5" style="font-size: 11px; background-color: white">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"  && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="rbLugar" type="radio" value="0" onclick="radios(2,0)" />Depósito
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"  && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="rbLugar" type="radio" value="1" onclick="radios(2,1)" />Cargo									
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"  && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="rbLugar" type="radio" value="2" onclick="radios(2,-1)" />Descuento
										
									</th>	
								</tr>
								<tr>									
									<th ><label style="margin: 2px; font-size: 14px" for="obsd">Observaciones</label>
									<th colspan="5" style="font-size: 11px; background-color: white"><input size="100%" type="text" name="obsd" id="obsd" value="" style="text-align: left;"></th>									
								</tr>
								</tbody>
    							<tfoot style="background-color: #DBE5F1">									
									<tr>									
										<th style=" text-align: right; margin: 2px; font-size: 14px;" colspan="6">Procesar Datos en calidad de Cancelación de Pago 
											<select name="rblugar1" id="rblugar1" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
      											<option style="color: blue" value="0" > No </option>
      											<option style="color: red" value="-1"  > Si </option>      									
    										</select>					
    									</th>
									</tr>
								</tfoot>					
    					</table>
	 				
		</div>	 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#nuevod").click( function(){	
	$("#txtFechad").val('');$("#mn").val('');$("#usd").val('');$("#tc").val('');$("#cta").val('');$("#est").val('');$("#obsd").val('');
	$("#cmbGra").val('0');$("#idd").val('');
	$('input:radio[name=rbLugar]:nth(0)').attr('checked',false);
	$('input:radio[name=rbLugar]:nth(1)').attr('checked',false);
	$('input:radio[name=rbLugar]:nth(2)').attr('checked',false);
	$("#cmbGra").focus();
 return true;
})

function radios(radio,dato){
	$("#est").val(dato);
	if(radio==2) { $
		if(dato==-1) { 
		
			$('input:radio[name=rbLugar]:nth(2)').attr('checked',true); 
		}
	}
}
$("#aceptard").click( function(){	
	//nombre=$("#razd").val();
	fec=$("#txtFechad").val();
	mn=$("#mn").val();
	usd=$("#usd").val();
	numero=$("#idd").val();
	gra=$("#cmbGra").val();
	
	if(mn==""){ mn=0; }
	if(usd==""){ usd=0; }
	if(gra!='0'){
		if( fec!=''){
		  if(mn!=0 || usd!=0){	
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edocta/actualizarD", 
						data: "id="+$("#idd").val()+"&fec="+$("#txtFechad").val()+"&usd="+$("#usd").val()+"&tc="+$("#tc").val()+"&cta="+$("#cta").val()+"&obs="+$("#obsd").val()+"&mn="+$("#mn").val()+"&nrc="+$("#cli").val()+"&ciclo="+$("#lstCiclo").val()+"&est="+$("#est").val()+"&descam="+$("#descam").val()+"&cancelar="+$("#rblugar1").val()+"&gra="+$("#cmbGra").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos de Depósito actualizados correctamente");
										$("#mytablaEdc").trigger("update");
										
   										$("#tabla").val('');$("#ciclo").val('');
										$("#nuevod").click();																											
									}else{
										alert("Error con la base de datos o usted no ha ACTUALIZADO nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edocta/agregarD", 
						data: "fec="+$("#txtFechad").val()+"&usd="+$("#usd").val()+"&tc="+$("#tc").val()+"&cta="+$("#cta").val()+"&obs="+$("#obsd").val()+"&mn="+$("#mn").val()+"&nrc="+$("#cli").val()+"&ciclo="+$("#lstCiclo").val()+"&est="+$("#est").val()+"&descam="+$("#descam").val()+"&cancelar="+$("#rblugar1").val()+"&impd="+$("#impd").val()+"&gra="+$("#cmbGra").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos de Depósito actualizados correctamente");
										$("#mytablaEdc").trigger("update");
										
   										$("#tabla").val('');$("#ciclo").val('');
										$("#nuevod").click();																											
									}else{
										alert("Error con la base de datos o usted no ha AGREGADO nada");
									}					
								}		
				});
			}
			}else{
					alert("Error: Necesita registrar cantidad en Pesos o Dólares");
					$("#mn").focus();
					return false;
			}			
		}else{
				alert("Error: Fecha no válida");	
				$("#txtFechad").focus();
				return false;
		}
	}else{
			alert("Error: Seleccione Granja");
			$("#cmbGra").focus();
			return false;
	}	
});
$("#borrard").click( function(){	
	numero=$("#idd").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edocta/borrarD", 
						data: "id="+$("#idd").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Depósito Eliminado correctamente");
										$("#mytablaEdc").trigger("update");	
										
   										$("#tabla").val('');$("#ciclo").val('');
										$("#nuevod").click();																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Depósito para poder Eliminarlo");
		$("#accordion").accordion( "activate",0 );
		return false;
	}	
});




$("#clisal").change( function(){
	$("#cli").val($("#clisal").val());
});
$("#cmbMes").change( function(){
	$("#mes").val($("#cmbMes").val());
});

$("#txtFechad").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  
$(document).ready(function(){
	var f = new Date(); 
    //$("#cmbMes").val((f.getMonth() +1));
	$("#tabla_ect").ajaxSorter({
		url:'<?php echo base_url()?>index.php/edocta/tabla',  		
		filters:['clisal','cmbMes'],
		sort:false,            
    	active:['Des',-1], 
    	onRowClick:function(){   
    		if($(this).data('des')==-1){ 						
    		$("#txtFechad").val($(this).data('fecha'));
    		$("#mn").val($(this).data('pesos'));
    		$("#usd").val($(this).data('imported'));
    		$("#tc").val($(this).data('tc'));
    		$("#cta").val($(this).data('cuenta'));
    		$("#idd").val($(this).data('nd'));
    		$("#obsd").val($(this).data('obs'));
			$("#cmbGra").val($(this).data('ngra'));
			$serx=$(this).data('des');
			if($serx==0 || $serx==1 || $serx==-1 ){
				if($serx==0){ $('input:radio[name=rbLugar]:nth(0)').attr('checked',true); $("#est").val(0);}
				if($serx==1){ $('input:radio[name=rbLugar]:nth(1)').attr('checked',true); $("#est").val(1);}
				if($serx==-1){ $('input:radio[name=rbLugar]:nth(2)').attr('checked',true); $("#est").val(-1);}
			}else{
				$('input:radio[name=rbLugar]:nth(0)').attr('checked',false);$('input:radio[name=rbLugar]:nth(1)').attr('checked',false);
				$('input:radio[name=rbLugar]:nth(2)').attr('checked',false);
			}
			}			
    	}, 
    	onSuccess:function(){ 
    		$('#tabla_ect tbody tr').map(function(){
		    	if($(this).data('feccos')=='Total:') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			//$(this).css('text-align','right');
	    		}
		    }) 	   	
	    },	
	});
	
});  	
</script>