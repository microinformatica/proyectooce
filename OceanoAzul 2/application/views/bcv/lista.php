<?php $this->load->view('header_cv');

?>
        
  
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
/*.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}*/
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	/*background: none;*/		
}   
.hide{
      display: none;
}
</style>

<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#entregas"><img src="<?php echo base_url();?>assets/images/menu/bcv.png" width="25" height="25" border="0"> Técnico - Chofer - Unidad </a></li>					
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="entregas" class="tab_content" >
			<div  style="margin-left: -10px; width: 958px;" >
					<div style="height: 350px;">		
						<table>
							<th>		
								<div class="ajaxSorterDiv" id="tabla_bcv" name="tabla_bcv" style="height: 425px;width: 270px ">                
                					<div style=" text-align:left; font-size: 12px;" class='filter'  >
                						<div align="left">	                			
                							<select name="cmbBCV" id="cmbBCV"  style="font-size: 11px;height: 25px; width: 120px">
												<option value="biologo">Técnicos</option>
												<option value="chofer">Chofer</option>
												<option value="unidad">Unidad</option>
   											</select>
										</div>	  
									</div>        			
                					<span class="ajaxTable" style="height: 340px; background-color: white;" >
                    					<table id="mytablaBCV" name="mytablaBCV" class="ajaxSorter" style="width: 250px" >
                        					<thead title="">                          		                        
                            					<th data-order = "Numero" style="width: 30px" >No.</th>
                            					<th data-order = "Nombre" style="width: 200px">Nombre</th>
                            				</thead>
                        					<tbody title="Seleccione para actualizar" style="font-size: 11px"></tbody>                        	
                    					</table>
                					</span> 
             						<div class="ajaxpager" style="">        
                    					<ul class="order_list"></ul>     
                    					<form method="post" action="<?= base_url()?>index.php/bcv/pdfrep" >                    	             	                                                
                        					<img title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        					<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 										</form>
 									</div>   
 							</th>
 							<th style="width: 25px"></th>
 							<th style="width: 365px">
 							   <div id='ver_mas_bcv' class='hide' style="margin-top: 4px">
 								<table style="width: 350px;" border="2px">
									<thead style="background-color: #DBE5F1">
										<th colspan="2" height="15px" style="font-size: 20px">Datos de <input size="10%" type="text" name="tablas" id="tablas" style=" background: none;  font-size: 20px; border: none; ">
											<input size="8%" type="hidden"  name="id" id="id"/>													
										</th>
									</thead>
									<tbody style="background-color: #F7F7F7">
									<tr>
										<th><label style="margin: 2px; font-size: 14px" for="ali">Nombre</label></th>
										<th><label style="margin: 2px; font-size: 14px" for="com">Activo</label></th>								
									</tr>							
    								<tr style="background-color: white"> 
											<th style="font-size: 13px;"><input size="40%" type="text" name="nom" id="nom"></th>
											<th style="background-color: white;">	
												<select name="rblugar" id="rblugar" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
      												<option style="color: blue" value="0" > Si </option>
      												<option style="color: red" value="-1"  > No </option>      									
    								  		</select>	
    									</th>									
									</tr>
									</tbody>
									<tfoot style="background-color: #DBE5F1">
										<th colspan="2">
											<center>								
												<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
												&nbsp;<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
												&nbsp;<input style="font-size: 14px" type="submit" id="borrar" name="borrar" value="Borrar" />																	
											</center>
										</th>	
									</tfoot>   							
    							</table>
    							</div>
 							</th>
 					</table>
                </div>         	
            		</div>
	 		</div>	
	 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#cmbBCV").change( function(){		
	$("#tablas").val($("#cmbBCV").val());	
 return true;
});
$("#nuevo").click( function(){	
	$("#id").val('');	        
	$("#nom").val('');
	$("#rblugar").val('');
		
 return true;
});
$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/bcv/borrar", 
						data: "id="+$("#id").val()+"&tablas="+$("#tablas").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Eliminado correctamente");
										$("#nuevo").click();
										$("#mytablaBCV").trigger("update");
										$('#ver_mas_bcv').hide();
										//$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar para poder Eliminar");
		$("#accordion").accordion( "activate",0 );
		return false;
	}	
});
$("#aceptar").click( function(){		
	nom=$("#nom").val();
	numero=$("#id").val();
	if( nom!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/bcv/actualizar", 
						data: "id="+$("#id").val()+"&nom="+$("#nom").val()+"&act="+$("#rblugar").val()+"&tablas="+$("#tablas").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevo").click();
										$("#mytablaBCV").trigger("update");
										$('#ver_mas_bcv').hide();
										//$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/bcv/agregar", 
						data: "&nom="+$("#nom").val()+"&act="+$("#rblugar").val()+"&tablas="+$("#tablas").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Registrado correctamente");
										$("#nuevo").click();
										$("#mytablaBCV").trigger("update");
										$('#ver_mas_bcv').hide();
										//$("#cmbBCV").val('');
										//$("#accordion").accordion( "activate",0 );												
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Registre nombre ");	
			$("#nom").focus();
				return false;
		}
});
$('#cmbBCV').change(function(){
    $('#ver_mas_bcv').hide();
    $("#id").val('');
    $("#nom").val('');
	$("#rblugar").val('');
});
$(document).ready(function(){ 
	  
	$("#tabla_bcv").ajaxSorter({
		url:'<?php echo base_url()?>index.php/bcv/tabla',  
		filters:['cmbBCV'],
		active:['Activo',-1],
		sort:false,
		onRowClick:function(){
			if($(this).data('numero')>'0'){
    			//$("#accordion").accordion( "activate",1 );
    			if($("#cmbBCV").val()=='biologo'){$("#tablas").val('técnico');}else{$("#tablas").val($("#cmbBCV").val());}
    			$('#ver_mas_bcv').show();
    			$("#id").val($(this).data('numero'));
       			$("#nom").val($(this).data('nombre'));
				$("#rblugar").val($(this).data('activo'));			
								
			}
    	},
    	   
	});
	
});
</script>
