<?php $this->load->view('header_cv'); ?>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/accessibility.js"></script>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}

</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#parametros" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/oximetro.png" width="20" height="20" border="0"> Parametros </a></li>
		</strong>
		Año:
        <select name="ano" id="ano" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-4px;">
        	<option value="21" >2021</option>
        </select>
        Ciclo:
        <select name="ciclo" id="ciclo" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-4px;">
        	<option value="0" >Todos</option><option value="1" >1</option><option value="2" >2</option>
        </select>
        Granja:
       	<select name="cmbGra" id="cmbGra" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">	   	
   			<option value="4" >Ahome</option>   									
   		</select>   		
   		Parámetro:
       	<select name="tipo" id="tipo" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">	   	
   			<option value="1" >Oxígeno</option>
   			<option value="2" >Temperatura</option>   									
   		</select>   		
   		Estanque:
   		<select name="cmbEst" id="cmbEst" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">	   	
   			<?php	
          		$cont=0;
				while($cont<80){ $cont+=1;?>
           		<option style="font-size: 9px" value ="<?php echo $cont;?>" ><?php echo $cont;?></option>
           	<?php } ?>	
   		</select>  
   		Mes
   		<select name="mes" id="mes" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">	   	
   			<option value="0" >Todos</option>
   			<option value="1" >Ene</option><option value="2" >Feb</option><option value="3" >Mar</option>
   			<option value="4" >Abr</option><option value="5" >May</option><option value="6" >Jun</option>
   			<option value="7" >Jul</option><option value="8" >Ago</option><option value="9" >Sep</option>
   			<option value="10" >Oct</option><option value="11" >Nov</option><option value="12" >Dic</option>
   		</select>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 650px"  >
		<div id="parametros" class="tab_content" style="margin-top: 1px">  
			<div id='ver_parametros' class='hide' >
			<div class="ajaxSorterDiv" id="tabla_par" name="tabla_par" style="width: 910px; margin-top: -15px;"  >
				<span class="ajaxTable" style="height:120px;" >
                	<table id="mytablaPar" name="mytablaPar" class="ajaxSorter" border="1" style="width:909px; margin-left: -1PX;"" >
                		<thead >  
                			<th data-order = "dia" ></th> <!--<th data-order = "vtaim" >Importe</th>-->
                   			<th data-order = "mini" >min</th>
                   			<th data-order = "maxi" >max</th>
                   			<th data-order = "lim" >limite</th>
                      	</thead>
                    	<tbody  style="font-size: 12px; text-align: center;">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
            <div class="ajaxSorterDiv" id="tabla_par2" name="tabla_par2" style="width: 910px; margin-top: -15px;"  >
				<span class="ajaxTable" style="height:120px;" >
                	<table id="mytablaPar2" name="mytablaPar2" class="ajaxSorter" border="1" style="width:909px; margin-left: -1PX;"" >
                		<thead >  
                			<th data-order = "dia" ></th> <!--<th data-order = "vtaim" >Importe</th>-->
                   			<th data-order = "mini" >min</th>
                   			<th data-order = "maxi" >max</th>
                   			<th data-order = "lim" >limite</th>
                      	</thead>
                    	<tbody  style="font-size: 12px; text-align: center;">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
            </div>
            <figure class="highcharts-figure" style="margin-top: -10px;">
    			<div id="grafica1" style="height: 325px;"></div>
    			<div id="grafica2" style="height: 325px; margin-top: -5px;"></div>
    		</figure>
     	</div>       
	</div>
<?php $this->load->view('footer_cv'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#ver_parametros').hide();
		$("#tabla_par").ajaxSorter({
			url:'<?php echo base_url()?>index.php/parametros/tablap',  
			filters:['ano','ciclo','cmbGra','tipo','cmbEst','mes'],
			sort:false,  
        	onSuccess:function(){ 
        		if($("#ciclo").val()>'0') ciclo='-'+$("#ciclo").val(); else ciclo='';
        		if($("#tipo").val()=='1'){
        		Highcharts.chart('grafica1', {
				    data: {table: 'mytablaPar'},
				    chart: {type: 'spline'},
				    title: {text: 'OXIGENO: Ciclo: [20'+$("#ano").val()+ciclo+'] Estanque ['+$("#cmbEst").val()+']'},
				    yAxis: {
				        allowDecimals: false,
				        title: {text: 'Oxígeno (O2)'}
				    },
				    tooltip: {
				        formatter: function () {
				            return '<b>' + this.series.name + '</b><br/>' +
				                this.point.y + ' ' + this.point.name.toLowerCase();
				        }
				    },
				    series: [{
		                name: 'max',
		                dataLabels: {enabled: true},
		                zones: [{ value: 2, color: 'red'}, { value: 4, color: 'orange'}, {color: 'lime'}],
		            }, {
		                name: 'min',
		                dataLabels: {enabled: true},
		                zones: [{ value: 2, color: 'red'}, { value: 4, color: 'orange'}, {color: 'lime'}],
		            }, {
	            		name: 'lim',
	            		color:  'red',	                	
	                }]
				});
        	}else{
        		Highcharts.chart('grafica1', {
				    data: {table: 'mytablaPar'},
				    chart: {type: 'spline'},
				    title: {text: 'TEMPERATURA: Ciclo: [20'+$("#ano").val()+ciclo+'] Estanque ['+$("#cmbEst").val()+']'},
				    yAxis: {
				        allowDecimals: false,
				        title: {text: 'Temperatura (T°C)'}
				    },
				    tooltip: {
				        formatter: function () {
				            return '<b>' + this.series.name + '</b><br/>' +
				                this.point.y + ' ' + this.point.name.toLowerCase();
				        }
				    },
				    series: [{
		                name: 'max',
		                dataLabels: {enabled: true}
		            }, {
		                name: 'min',
		                dataLabels: {enabled: true}
		            }]
				});
        	}
			},       
		});
		$("#tabla_par2").ajaxSorter({
			url:'<?php echo base_url()?>index.php/parametros/tablap2',  
			filters:['ano','ciclo','cmbGra','tipo','cmbEst','mes'],
			sort:false,  
        	onSuccess:function(){ 
        		ano=$("#ano").val()-1;
        		if($("#ciclo").val()>'0') ciclo='-'+$("#ciclo").val(); else ciclo='';
        		if($("#tipo").val()=='1'){
        		Highcharts.chart('grafica2', {
				    data: {table: 'mytablaPar2'},
				    chart: {type: 'spline'},
				    title: {text: 'OXIGENO: Ciclo: [20'+ano+ciclo+'] Estanque ['+$("#cmbEst").val()+']'},
				    yAxis: {
				        allowDecimals: false,
				        title: {text: 'Oxígeno (O2)'}
				    },
				    tooltip: {
				        formatter: function () {
				            return '<b>' + this.series.name + '</b><br/>' +
				                this.point.y + ' ' + this.point.name.toLowerCase();
				        }
				    },
				    series: [{
		                name: 'max',
		                dataLabels: {enabled: true},
		                zones: [{ value: 2, color: 'red'}, { value: 4, color: 'orange'}, {color: 'lime'}],
		            }, {
		                name: 'min',
		                dataLabels: {enabled: true},
		                zones: [{ value: 2, color: 'red'}, { value: 4, color: 'orange'}, {color: 'lime'}],
		            }, {
	            		name: 'lim',
	            		color:  'red',	                	
	                }]
				});
        	}else{
        		Highcharts.chart('grafica2', {
				    data: {table: 'mytablaPar2'},
				    chart: {type: 'spline'},
				    title: {text: 'TEMPERATURA: Ciclo: [20'+ano+ciclo+'] Estanque ['+$("#cmbEst").val()+']'},
				    yAxis: {
				        allowDecimals: false,
				        title: {text: 'Temperatura (T°C)'}
				    },
				    tooltip: {
				        formatter: function () {
				            return '<b>' + this.series.name + '</b><br/>' +
				                this.point.y + ' ' + this.point.name.toLowerCase();
				        }
				    },
				    series: [{
		                name: 'max',
		                dataLabels: {enabled: true}
		            }, {
		                name: 'min',
		                dataLabels: {enabled: true}
		            }]
				});
        	}
			},       
		});
	}); 
</script>
