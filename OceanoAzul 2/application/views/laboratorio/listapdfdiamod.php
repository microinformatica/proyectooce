<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 80px 50px 20px; }	
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: navy; }	
</style> 
<title>Reporte de Analisis en Fresco</title>
</head>
<body>
	<?php 
		$f=explode("-",$fecha); 
      	$nummes=(int)$f[1]; 
      	$mes1="0-Enero-Febrero-Marzo-Abril-Mayo-Junio-Julio-Agosto-Septiembre-Octubre-Noviembre-Diciembre"; 
      	$mes1=explode("-",$mes1); 
      	$desfecha="$f[2]-$mes1[$nummes]-$f[0]"; 
	?>	
	<div id="header"> 
		<table style="width: 770px;">
			<tr>
				<td width="200px">
					<p style="font-size: 8px;"  >
						<img src="<?php echo base_url();?>assets/images/aquapacificlogotipo.jpg" width="200" height="60" border="0">
						SIA - <?= $usuario?> - <?= $perfil?> <br />
					</p>
				</td>	
				<td align="center" style="font-size: 14px; color: navy"><strong>RESULTADOS DE ANALISIS EN FRESCO CORRESPONDIENTE <br /> AL DÍA: <?= $f[2]." de ".$mes1[$nummes]. " de ".$f[0]?>. - <?= $estatus ?></strong></td>			
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: white;">
				<td width="160px">www.aquapacific.com.mx</td>	
				<td width="490">
					Av. Emilio Barragán No.63-103, Col. Lázaro Cárdenas, Mazatlán, Sin. México, RFC:AQU-031003-CC9, CP:82040 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
	</div>
<div style="margin-left: -25px">
	
	 <?php 
	  $this->db->select('Nombre,pila,fec,plf');
	  $this->db->join('raceways', 'NumPila=pila','inner');
	  $this->db->where('fec =',$fecha);
	  $this->db->group_by('pila');
	  $this->db->group_by('Nombre');
	  $this->db->group_by('fec');
	  $this->db->group_by('plf');
	  $resultt = $this->db->get('fisiologiaanalisis');
	  $pilas=0;
	  foreach($resultt->result() as $rowt):
		$pilas+=1;		
		$f=explode("-",$rowt->fec); 
      	$nummes=(int)$f[1]; 
      	$mes1="0-Enero-Febrero-Marzo-Abril-Mayo-Junio-Julio-Agosto-Septiembre-Octubre-Noviembre-Diciembre"; 
      	$mes1=explode("-",$mes1); 
		  // datos de las pilas del dia generales
		  ?>
		  <br />
		  <div style="height: 290px">
		  	
		  <table border="1" width="770px"  style="font-size: 18px">
		  <tr>
		  	<td colspan="4">Aquapacific S.A. de C.V. - Control de Calidad [Resultados de Análisis en Fresco]</td>
		  </tr>	    	 
    	  <tr>
    	  	<td colspan="2">Fecha: <?= $f[2]." de ".$mes1[$nummes]. " de ".$f[0]?></td>
    	  	<td colspan="2">Pila:<?= ' ['.$rowt->Nombre.']' ?> - Estadio: <?= 'Pl ['.$rowt->plf.']' ?></td>
    	  </tr>
    	 
    	  <tr>
    	  	<td>Branquias</td>
    	  	<td>Intestino</td>
    	  	<td>Exoesqueleto</td>
    	  	<td>Hepatopáncreas</td>
    	  </tr>
    	  <tr>
    	  	<?php
    	  	$this->db->select('count(*) as ani');	
			$this->db->where('pila',$rowt->pila);
			$resulti = $this->db->get('fisiologiaanalisis');				
			foreach($resulti->result() as $rowti):
				$ani=$rowti->ani;
			endforeach;	
    	  	$this->db->select('count(*) as bra1');	
			$this->db->where('bra',1);	$this->db->where('pila',$rowt->pila);	
			$this->db->where('fec =',$fecha);	
			$resulti = $this->db->get('fisiologiaanalisis');				
			foreach($resulti->result() as $rowti):
				if($rowti->bra1/$ani*100 >= 50)	$bra='X-'.number_format($rowti->bra1/$ani*100, 1, '.', ',').'%';
				else $bra='OK';
			endforeach;
			?>
    	  	<td><?= $bra?><br /> <br /></td>
    	  	<?php
    	  	$this->db->select('count(*) as inte1');	
			$this->db->where('inte',2);	$this->db->where('pila',$rowt->pila);		
			$this->db->where('fec =',$fecha);	
			$resulti = $this->db->get('fisiologiaanalisis');					
			foreach($resulti->result() as $rowti):
				if($rowti->inte1/$ani*100 >= 50)	$rowt->int='X-'.number_format($rowti->inte1/$ani*100, 1, '.', ',').'%';
				else $int='OK';
			endforeach;    	  	
			?>
    	  	<td><?= $int?><br /> <br /></td>
    	  	<?php
    	  	$this->db->select('count(*) as exo1');	
			$this->db->where('exo',1);	$this->db->where('pila',$rowt->pila);		
			$this->db->where('fec =',$fecha);	
			$resulti = $this->db->get('fisiologiaanalisis');					
			foreach($resulti->result() as $rowti):
				if($rowti->exo1/$ani*100 >= 50)	$exo='X-'.number_format($rowti->exo1/$ani*100, 1, '.', ',').'%';
				else $exo='OK';
			endforeach;    	  	
			?>			
    	  	<td><?= $exo?><br /> <br /></td>
    	  	<?php
    	  	$gral=0;
    	  	$this->db->select('count(*) as hep11');	
			$this->db->where('hep1',3);	$this->db->where('pila',$rowt->pila);		
			$this->db->where('fec =',$fecha);	
			$resulti = $this->db->get('fisiologiaanalisis');					
			foreach($resulti->result() as $rowti):
				if($rowti->hep11/$ani*100 >= 30){$hep1='G1-2: '.number_format($rowti->hep11/$ani*100, 1, '.', ',').'%';
					$gral=1;$he1=$rowti->hep11/$ani*100;
				}
				else $hep1='OK';
			endforeach;    	  				
			$this->db->select('count(*) as hep21');	
			$this->db->where('hep2',3);	$this->db->where('pila',$rowt->pila);		
			$this->db->where('fec =',$fecha);	
			$resulti = $this->db->get('fisiologiaanalisis');				
			foreach($resulti->result() as $rowti):
				if($rowti->hep21/$ani*100 >= 30){$hep2='G1-2: '.number_format($rowti->hep21/$ani*100, 1, '.', ',').'%';
					$gral+=1;$he2=$rowti->hep21/$ani*100;
				}
				else $hep2='OK';
			endforeach;
			$this->db->select('count(*) as hep31');	
			$this->db->where('hep3',3);	$this->db->where('pila',$rowt->pila);		
			$this->db->where('fec =',$fecha);	
			$resulti = $this->db->get('fisiologiaanalisis');				
			foreach($resulti->result() as $rowti):
				if($rowti->hep31/$ani*100 >= 30){$hep3='G1-2: '.number_format($rowti->hep31/$ani*100, 1, '.', ',').'%';
					$gral+=1;	$he3=$rowti->hep31/$ani*100;				
				}
				else $hep3='OK';
			endforeach;
			$this->db->select('obs');	
			$this->db->where('pila',$rowt->pila);		
			$this->db->where('fec =',$fecha);	
			$resulti = $this->db->get('fisiologiaanalisis');$obs='';				
			foreach($resulti->result() as $rowti):
				$obs=$obs.' '.$rowti->obs;
			endforeach;	
			if($gral>1){	
				if($bra!='OK' || $int!='OK'|| $exo!='OK' || $hep1!='OK' || $hep2!='OK'|| $hep3!='OK') {																	
					if($gral==3){ $kpi='Gral: '.number_format(($he1+$he2+$he3)/$gral, 1, '.', ',').'%'; } 
						elseif($gral==2){ $kpi='Gral: '.number_format(($he1+$he2)/$gral, 1, '.', ',').'%';$hep3='';} 
							elseif($gral==1){  $kpi='Gral: '.number_format(($he1), 1, '.', ',').'%';$hep3='';$hep2='';	}	
								$he1=0;$he2=0;$he3=0;
				}
			}else{$kpi='';
				if($gral==1){$hep2='';$hep3='';}
				elseif($gral==2){$hep3='';	}
				else{$hep2='';$hep3='';}
			}
			
			?>
    	  	<td style="font-size: 18px" ><?= $hep1.' '.$hep2.' '.$hep3.' '.$kpi?> <br /> <br /> </td>
    	  </tr>
    	  <tr>
    	  	<td colspan="4">Observaciones</td>    	  	
    	  </tr>
    	  <tr>
    	  	<td colspan="4"><?= $obs?> <br /> <br /> </td>     	  	
    	  </tr>
	</table>	
	</div>	
	 <?php 
	  endforeach;
	  $cont=1; //$pilas=4;
	  while ($cont<=$pilas){?>
	  	
	
	<?php $cont+=1;}?>
</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>