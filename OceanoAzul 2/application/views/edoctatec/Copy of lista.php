<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/abonos.png" width="25" height="25" border="0"> Estado de Cuenta </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion">
			<h3 align="left"><a href="">Saldo Actual</a></h3>
        	<div  style="height:348px; text-align: right; font-size: 12px;">                                                                      
            <div class="ajaxSorterDiv" id="tabla_ect" name="tabla_ect" align="left" style=" height: 398px; margin-top: -10px; " >             	
            	<div style="margin-top: 0px;">
            	Técnico: <select name="enc" id="enc" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
            				<option value="0">Seleccione Encargado</option>
          					<?php	
          					//$this->load->model('edoctatec_model');
          					$data['result']=$this->edoctatec_model->verTecnico();
							foreach ($data['result'] as $row):?>
           					<option value="<?php echo $row->NumBio;?>" ><?php echo $row->NomBio;?></option>
           					<?php endforeach;?>
   						</select>
   						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Estatus Actual:<select id="cmbEstatus" style="font-size: 10px;">
						<option value="10">Todos</option>
						<option value="1">Cargos</option>
						<option value="2">Abonos</option>						
						<option value="3">Improvistos</option>						
					</select>
					Cierre de Cuentas:
					<select name="corte" id="corte" style="font-size: 10px;margin-left:10px; margin-top: 1px;">
      								<option value="0"> Todos </option>
      								<option value="1"> Corte 1 </option>
      								<option value="2"> Corte 2 </option>      									
      								<option value="3"> Corte 3 </option>
    				</select>
    				Mes:
    				<select name="mes" id="mes" style="font-size: 10px;margin-left:10px; margin-top: 1px;">
      								<option value="0"> Todos </option>
      								<option value="01"> Enero </option>
      								<option value="02"> Febrero </option>      									
      								<option value="03"> Marzo </option>
      								<option value="04"> Abril </option>
      								<option value="05"> Mayo </option>
      								<option value="06"> Junio </option>
      								<option value="07"> Julio </option>
      								<option value="08"> Agosto </option>
      								<option value="09"> Septiembre </option>
      								<option value="10"> Octubre </option>
      								<option value="11"> Noviembre </option>
      								<option value="12"> Diciembre </option>
    				</select>
				</div>	       
                <span class="ajaxTable" style=" height: 320px; width: 880px ">
                    <table id="mytablaECT" name="mytablaECT" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px"> 
                        	<th data-order = "FechaS1" style="width: 55px">Fecha</th>
                            <th data-order = "RemisionR" style="width: 30px">Remisión</th>                                                        
                            <th data-order = "Razon" style="width: 250px">Razón Social / Concepto</th>                            
                            <th data-order = "Cargo1" style="width: 50px">Cargo</th>
                            <th data-order = "Abono1" style="width: 50px;">Abono</th>
                            <th data-order = "saldofila" style="width: 50px" >Saldo</th>                                                       
                        </thead>
                        <?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino"){ ?>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        
                        <?php } else {?>
                        <tbody title="Seleccione para analizar" style="font-size: 10px">	
                        <?php }?>
                        </tbody>
                    </table>
                    
                </span> 
             	<div class="ajaxpager" style="margin-top: -10px">        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/edoctatec/pdfrep" >
                    	<input type="hidden" id="tecnico" name="tecnico" />
                        <img style="cursor: pointer" style="margin-top: 6px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                                                                          
 					</form>   
 					
                </div>  
                                                 
            </div>             
        </div>        
        <?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino"){ ?>
        <h3 align="left"><a href="" >Agregar, Actualizar o Eliminar Abonos</a></h3>
        <?php } else {?>	
        <h3 align="left"><a href="" >Analizar Abono Seleccionado</a></h3>
        <?php }?>
       	<div style="height: 150px;">
			<table style="width: 824px;" border="2px">
				<thead style="background-color: #DBE5F1">
					<th colspan="5" height="15px" style="font-size: 20px">Datos del Abono <select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> name="enc1" id="enc1" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          			<option value="0">Seleccione Encargado</option>
          						<?php	
          							//$this->load->model('edoctatec_model');
									$data['result']=$this->edoctatec_model->verTecnico();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumBio;?>" ><?php echo $row->NomBio;?></option>
           							<?php endforeach;?>
   						</select>
					<input size="8%" type="hidden"  name="id" id="id"/>													
					</th>
				</thead>	
				<tbody style="background-color: #F7F7F7">							
				<tr>
					<th><label style="margin: 2px; font-size: 14px" for="txtFecha">Fecha</label></th>
					<th><label style="margin: 2px; font-size: 14px;" for="des" >Concepto</label></th>
					<th><label style="margin: 2px; font-size: 14px" for="cho">Chofer</label></th>
					<th><label style="margin: 2px; font-size: 14px" for="uni">Unidad</label></th>																										
					<th><label style="margin: 2px; font-size: 14px;" for="rem">Remision</label></th>										
				</tr>
    			<tr style="font-size: 14px; background-color: white;">										
					<th><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> size="13%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>								
					<th>
						<input  name="gto" type="text" id="gto" size="35" />
					</th>
					<th>
						<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> name="cho" id="cho" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
           							//$this->load->model('edoctatec_model');
									$data['result']=$this->edoctatec_model->verChofer();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumCho;?>" ><?php echo $row->NomCho;?></option>
           							<?php endforeach;?>
   						</select>
					</th>																		
					<th>
						<?php //if($des!="Improvistos" and $des!="Complemento Gasto"){ ?>
						<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> name="uni" id="uni" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
           							//$this->load->model('edoctatec_model');
									$data['result']=$this->edoctatec_model->verUnidad();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumUni;?>" ><?php echo $row->NomUni;?></option>
           							<?php endforeach;?>
   						</select>
						<?php // } ?>
					</th>
					<th>
						<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> name="rem" id="rem"  style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
           						<?php	
           							//$this->load->model('edoctatec_model');
									$data['result']=$this->edoctatec_model->verRemision();
									?>
									<option value="0" ><?php echo "SN";?></option>
									<?php
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumRegR;?>" ><?php if($row->NumRegR==0) { echo "SN";} else { echo $row->RemisionR;}?></option>
           							<?php endforeach;?>
    					</select>
					</th>
				</tr>
			</tbody>	
			</table>
			<table style="width: 824px; margin-top: -15px" border="2px">
    			<thead style="background-color: #DBE5F1">
					<th colspan="6">Conceptos</th>
				</thead>
				<tbody style="background-color: #F7F7F7">
				<tr>
					<th><label style="margin: 2px; font-size: 14px" for="ali">Alimentación</label></th>
					<th><label style="margin: 2px; font-size: 14px" for="com">Combustible</label></th>
					<th><label style="margin: 2px; font-size: 14px;" for="cas" >Casetas</label></th>										
					<th><label style="margin: 2px; font-size: 14px" for="hos">Hospedaje</label></th>																										
					<th><label style="margin: 2px; font-size: 14px;" for="fit">Fitosanitaria</label></th>										
					<th><label style="margin: 2px; font-size: 14px;" for="sub">Subtotal</label></th>
				</tr>
    			<tr style="background-color: white"> 
    				<form>   								
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> size="14%" type="text" name="ali" id="ali" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> size="14%" type="text" name="com" id="com" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> size="14%" type="text" name="cas" id="cas" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> size="14%" type="text" name="hos" id="hos" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> size="14%" type="text" name="fit" id="fit" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>									
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> size="14%" readonly="true" type="text" name="sub" id="sub" style="text-align: right;"></th>
					</form>									
				</tr>
				</tbody>    							
    		</table>
    		<table style="width: 824px; margin-top: -15px" border="2px">
    			<thead style="background-color: #DBE5F1">
    				<th colspan="6">Otros Conceptos</th>
				</thead>
				<tbody style="background-color: #F7F7F7">
				<tr>
					<th colspan="3" style="font-size: 13px"><label style="margin: 2px; font-size: 14px" for="rep">Reposición :
						<input size="2%" type="hidden"  name="repos" id="repos" readonly="true" >
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick="radios(1,1)"  name="repo" value="1" /> Otros							
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick="radios(1,2)"  name="repo" value="2" /> Improvistos
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick="radios(1,3)"  name="repo" value="3" /> Gasto
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> size="16%" type="text" name="rep" id="rep" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;">
						</label>
					</th>					
					<th colspan="3"><label style="margin: 2px; font-size: 14px;" for="cas" >TOTAL
						$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> size="16%" type="text" name="tot" id="tot" value="" style="text-align: right;">
						</label></th>
																								
				</tr>
				</tbody>
				<tfoot style="background-color: #DBE5F1">
					<tr style="background-color: white"> 
								<th colspan="6" style="font-size: 13px;"><textarea <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> readonly="true" <?php } ?> name="obs" id="obs" rows="2" cols="90" style="resize: none"></textarea></th>
							</tr>  
					<th colspan="6">
						<center>
							<?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino"){ ?>
								Aplicar a:
								<select name="rblugar" id="rblugar" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
      								<option value="1"> Corte 1 </option>
      								<option value="2"> Corte 2 </option>      									
      								<option value="3"> Corte 3 </option>
    							</select>
								<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
								<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
								<input style="font-size: 14px" type="submit" id="borrar" name="borrar" value="Borrar" />
							<?php }?>										
						</center>
					</th>	
				</tfoot>
    		</table>       	 
        </div> 	
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#enc").change( function(){		
	$("#ecn1").val($("#enc").val());	
	$("#tecnico").val($("#enc").val());
 return true;
});	
function radios(radio,dato){
	if(radio==1) { $("#repos").val(dato); }	
}
$("#nuevo").click( function(){	
	$("#id").val('');
	//$("#enc1").val('');
    $("#txtFecha").val('');
	$("#gto").val('');
	$("#cho").val('');
	$("#uni").val('');
	$("#rem").val('0');	
	$("#ali").val('');
	$("#com").val('');
	$("#cas").val('');
	$("#hos").val('');
	$("#fit").val('');
	$("#sub").val('');			
	$("#rep").val('');
	$("#obs").val('');
	$("#tot").val('');
	$("#gto").val('');
	$("#nomdess").val('');
	$("#rblugar").val('');
	$('input:radio[name=repo]:nth(0)').attr('checked',false);$('input:radio[name=repo]:nth(1)').attr('checked',false);
	$('input:radio[name=repo]:nth(2)').attr('checked',false);
 return true;
})

$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edoctatec/borrar", 
						data: "id="+$("#id").val(), //+"&usd="+$("#usd").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Abono Eliminado correctamente");
										$("#nuevo").click();
										$("#mytablaECT").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Abono para poder Eliminarlo");
		$("#accordion").accordion( "activate",0 );
		return false;
	}
	
	 // return false;
});


function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#aceptar").click( function(){		
	enc=$("#enc1").val();
	fec=$("#txtFecha").val();
	numero=$("#id").val();
	rem=$("#rem").val();
	if( enc>'0'){
	if( fec!=''){
	  if( rem>'0'){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edoctatec/actualizar", 
						data: "&id="+$("#id").val()+"&fec="+$("#txtFecha").val()+"&rem="+$("#rem").val()+"&enc="+$("#enc1").val()+"&des="+$("#gto").val()+"&cho="+$("#cho").val()+"&uni="+$("#uni").val()+"&ali="+$("#ali").val()+"&com="+$("#com").val()+"&cas="+$("#cas").val()+"&hos="+$("#hos").val()+"&fit="+$("#fit").val()+"&rep="+$("#rep").val()+"&obs="+$("#obs").val()+"&repos="+$("#repos").val()+"&corte="+$("#rblugar").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Abono actualizado correctamente");
										$("#nuevo").click();
										$("#mytablaECT").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edoctatec/agregar", 
						data: "&fec="+$("#txtFecha").val()+"&rem="+$("#rem").val()+"&enc="+$("#enc1").val()+"&des="+$("#gto").val()+"&cho="+$("#cho").val()+"&uni="+$("#uni").val()+"&ali="+$("#ali").val()+"&com="+$("#com").val()+"&cas="+$("#cas").val()+"&hos="+$("#hos").val()+"&fit="+$("#fit").val()+"&rep="+$("#rep").val()+"&obs="+$("#obs").val()+"&repos="+$("#repos").val()+"&corte="+$("#rblugar").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Abono registrado correctamente");
										$("#nuevo").click();
										$("#mytablaECT").trigger("update");
										$("#accordion").accordion( "activate",0 );												
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Seleccione remision para aplicar Abono");	
			$("#rem").focus();
			return false;
			}
		}else{
			alert("Error: Fecha no valido");	
			$("#txtFecha").focus();
				return false;
		}
		}else{
			alert("Error: Seleccione Encargado ");	
			$("#enc1").focus();
				return false;
		}
});

$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  


function noNumbers(e){
		var keynum;
		var keychar;
		var numcheck;
		if(window.event){
			jeynum = e.keyCode;
		}
		else if(e.which){
			heynum = eArray.which;
		}
		keychar = String.formCharccode(keynum);
		numcheck = /\d/;
		return !numcheck.test(keychar);
	}
		
$(document).ready(function(){
	$("#nuevo").click(); 
	$("#ecn1").val($("#enc").val());
	$("#tecnico").val($("#enc").val());	
	$("#tabla_ect").ajaxSorter({
		url:'<?php echo base_url()?>index.php/edoctatec/tabla',  		
		filters:['enc','cmbEstatus','corte','mes'],
		active:['CarAbo',2],
    	//multipleFilter:true, 
    	sort:false,            
    	//onLoad:false,  
    	onRowClick:function(){    						
    		if($(this).data('carabo')!=2 && $(this).data('fechas1')==''){
    			$("#enc").val($(this).data('remisionr'));
    			$("#enc1").val($(this).data('remisionr'));
    			$("#mytablaECT").trigger("update");
    		}		
			if($(this).data('carabo')==2){
				$("#accordion").accordion( "activate",1 );
       			$("#id").val($(this).data('numsol'));
       			$("#txtFecha").val($(this).data('fechas'));
       			$("#enc").val($(this).data('numbios'));
       			$("#enc1").val($(this).data('numbios'));
       			$("#cho").val($(this).data('numchos'));
       			$("#uni").val($(this).data('numunis'));
       			$("#gto").val($(this).data('nomdess'));
       			gtos=$(this).data('nomdess');
				if(gtos=='Otros' || gtos=='Improvistos'){
					if(gtos=='Otros'){ $('input:radio[name=repo]:nth(0)').attr('checked',true); $("#repos").val(1);}
					if(gtos=='Improvistos'){ $('input:radio[name=repo]:nth(1)').attr('checked',true); $("#repos").val(2);}
					//if(gtos=='Improvistos-Gasto'){ $('input:radio[name=repo]:nth(2)').attr('checked',true); $("#repos").val(3); }
				}else{
					$('input:radio[name=repo]:nth(2)').attr('checked',true); $("#repos").val(3);
				//	$('input:radio[name=repo]:nth(0)').attr('checked',false);$('input:radio[name=repo]:nth(1)').attr('checked',false);
				//	$('input:radio[name=repo]:nth(2)').attr('checked',false); 
				}
       			$("#rem").val($(this).data('numrems'));	
				$("#ali").val($(this).data('ali'));
				$("#com").val($(this).data('com'));
				$("#cas").val($(this).data('cas'));
				$("#hos").val($(this).data('hos'));
				$("#fit").val($(this).data('fit'));
				$("#sub").val($(this).data('sub'));			
				$("#rep").val($(this).data('rep'));
				$("#obs").val($(this).data('obser'));
				$("#tot").val($(this).data('tot'));
				$("#rblugar").val($(this).data('corte'));
			}			
    	}, 
    	onSuccess:function(){
    		//tc = 0;ta = 0;    	   	
	    	$('#tabla_ect tbody tr').map(function(){
	    		if($(this).data('fechas1')=='Total'){	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold'); 
	    		}
	    		//tc+=($(this).data('cargo2')>0?parseFloat($(this).data('cargo2')):0);
	    		//ta+=($(this).data('abono2')>0?parseFloat($(this).data('abono2')):0);
	    	});
	    	$('#tabla_ect tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','center');  })
	    	$('#tabla_ect tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center');  })
	    	$('#tabla_ect tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');  })
	    	$('#tabla_ect tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');  })
	    	$('#tabla_ect tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right');  })
	    	/*tr='<tr><td colspan=3>Saldo</td><td>$ '+(parseFloat(tc)).toFixed(2)+'</td><td>$ '+(parseFloat(ta)).toFixed(2)+'</td><td>$ '+(parseFloat(tc-ta)).toFixed(2)+'</td></tr>';	    	
	    	$('#tabla_ect tbody').append(tr);*/	    	
    	},  
	});
	
	
		
});  	
</script>