<?php $this->load->view('header_cv');?>
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.js"></script>-->
 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.simplecolorpicker.js"></script>
 
<link rel="stylesheet" href="<?php echo base_url();?>assets/styles/jquery.simplecolorpicker.css">
<!--
<link rel="stylesheet" href="<?php echo base_url();?>assets/styles/styles.css">
-->


<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
#imprimir_pdf { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }

.simplecolorpicker{
 max-width: 300px;
}
.simplecolorpicker span.color[data-selected]:after {
  content: '\2714'; /* Ok/check mark */
  width: 12px;
  display: block;
  padding-left: 4px
}

/*t-shirt*/

#est_container{
 position: relative; width: 610px; height: 550px;
}

#est_container .est_layer{
 position: absolute; display: table;  width: 610px; height: 550px;
}


#est_layer_1{
 z-index: 1;
}
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#kpi" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/kpi.png" width="25" height="25" border="0"> KPI </a></li>
		
		<li><a href="#dias" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/kpi.png" width="25" height="25" border="0"> Salud </a></li>
		
		<li><a href="#salud" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/kpi.png" width="25" height="25" border="0"> Estatus </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 630px" >
		<div id="kpi" class="tab_content" style="margin-top: -15px;" >
		  <div class="ajaxSorterDiv" id="tabla_kpi" name="tabla_kpi" style="margin-top: 1px;height: 610px">   
               <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 850px;" >
            		 	<form action="<?= base_url()?>index.php/kpioce/pdfrepgral" method="POST" >
            		 	Reporte de Indicadores Principales de Poducción Ciclo: 
            		 	<select name="cickpi" id="cickpi" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      						<?php $ciclof=21; $actual=date("y"); //$actual+=1;
								while($actual >= $ciclof){?>
									<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
									<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
									
												<?php  $actual-=1; } ?>      									
    					</select>	
            		 	al día:
   					 	<input size="10%" type="text"  name="dia" id="dia" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
   					 	Lote
   					 	<select name="lote" id="lote" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="1">1</option>		
    									<option value="2">2</option>
    									<option value="3">3</option>
    									<option value="4">4</option>
    					</select>
   					 	
   					 	<?php if($usuario=="Antonio Valdez" || $usuario=="Antonio Hernandez" || $usuario=="Zuelima Benitez"){ ?>
   					 		Sección:	 	
				 					<select name="secck" id="secck" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="41">I</option>		
    									<option value="42">II</option>
    									<option value="43">III</option>
    									<option value="44">IV</option>
    								</select>
    							<?php } ?>
    							
   					 	<input type="hidden" name="tablainddia" value ="" class="htmlTable"/> 
						</form>  
                   	</ul>
                   	<img style="cursor: pointer; " title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
 				</div>  
             	<span class="ajaxTable" style="height: 565px; background-color: white; width: 910px; margin-top: -15px" >
            	   	<table id="mytablakpi" name="mytablakpi" class="ajaxSorter" border="1" >
                       	<thead >
                       		<tr >
                       			<th rowspan="3" data-order = "lote">Lote</th>
                       			<th rowspan="3" data-order = "pisge">Est</th>		
                       			<th rowspan="2"colspan="3" style="background-color: navy; color: white;text-align: center">POBLACIÓN</th>	
                       			<th colspan="5" style="background-color: blue; color: white;text-align: center">AGUA</th>
                       			<th colspan="5" style="background-color: brown; color: white;text-align: center">ALIMENTO</th>
                       		</tr>
                       		<tr style="background-color: blue; color: white;">
                       			<th colspan="2" style="text-align: center">O2</th>	
                       			<th colspan="2" style="text-align: center">T°C</th>
                       			<th style="text-align: center">%</th>
                       			<th  style="background-color: brown; color: white;text-align: center" colspan="3" >Kg/Ha/Dia</th>
                       			<th  style="background-color: brown; color: white;text-align: center" colspan="2" ></th>
                       		</tr>
                       		<tr>
                       			<th  style="text-align: center" data-order = "pp">Peso (grs)</th>	
                       			<th  style="text-align: center" data-order = "pk">Kg/Ha</th>
                       			<th  style="text-align: center" data-order = "ps">Salud</th>
                       			<th  style="text-align: center" data-order = "o2">Max</th>
                       			<th  style="text-align: center" data-order = "o1">Min</th>
                       			<th  style="text-align: center" data-order = "t2">Max</th>
                       			<th  style="text-align: center" data-order = "t1">Min</th>
                       			<th  style="text-align: center" data-order = "reca">Rec</th>
                       			<th  style="text-align: center" data-order = "ab">Balanceado</th>
                       			<th  style="text-align: center" data-order = "as">Soya</th>
                       			<th  style="text-align: center" data-order = "ak">Bal+Soy</th>
                       			<th  style="text-align: center" data-order = "ac">Consumo %</th>
                       			<th  style="text-align: center" data-order = "bw">%BW</th>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 12px">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>	
 		</div>
		<div id="dias" class="tab_content" style="margin-top: -15px;" >
			<div class="ajaxSorterDiv" id="tabla_dias" name="tabla_dias" style="margin-top: 1px;height: 610px; margin-left: -5px; width: 950px;">   
               <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 800px; text-align: left;" >
            		 	<form action="<?= base_url()?>index.php/kpioce/pdfrepgral" method="POST" >
            		 		Ciclo:
            		 	<select name="cickpid" id="cickpid" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      						<?php $ciclof=19; $actual=date("y"); //$actual+=1;
								while($actual >= $ciclof){?>
									<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
									<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            						<?php  $actual-=1; } ?>      									
    					</select>
            		 	INCIDENCIAS EN LOS ULTIMOS DIAS:
   					 	<input size="10%" type="text"  name="diad" id="diad" class="fecha redondo mUp"  style="text-align: center;" >
   					 	Lote
   					 	<select name="loted" id="loted" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="1">1</option>		
    									<option value="2">2</option>
    									<option value="3">3</option>
    									<option value="4">4</option>
    					</select>
   					 	
   					 	<?php if($usuario=="Antonio Valdez" || $usuario=="Antonio Hernandez" || $usuario=="Zuelima Benitez"){ ?>
   					 	Sección:		 	
				 					<select name="seccd" id="seccd" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="41">I</option>		
    									<option value="42">II</option>
    									<option value="43">III</option>
    									<option value="44">IV</option>
    								</select>
    							<?php } ?>
    							
   					 	<input type="hidden" name="tablainddia" value ="" class="htmlTable"/> 
						</form>  
                   	</ul>
                   	<img style="cursor: pointer; " title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
 				</div>  
             	<span class="ajaxTable" style="height: 565px; background-color: white; width: 940px; margin-top: -15px" >
            	   	<table id="mytabladias" name="mytabladias" class="ajaxSorter" border="1" >
                       	<thead >
                       		<tr >
                       			<th style="font-size: 9px" rowspan="3" data-order = "lote">Lot</th>
                       			<th style="font-size: 9px" rowspan="3" data-order = "pisgd">Est</th>		
                       			<?php	
          						$cont=-1; $fec=new Libreria();
								//$fin = $ultimodia;
								$hoy=date ( 'Y-m-d');
								$ini = '';
								//$ini = strtotime ( '-9 day' , strtotime ( $fin ) ) ;$ini= date ( 'Y-m-d' , $ini);
								while($cont<30){ $cont+=1;
									$dia=$fec->fecha21(date ( 'Y-m-d' , ((strtotime ( '-'.$cont.' day' , strtotime ( $hoy ) )))));?>
           							<th style="font-size: 9px;" data-order ="<?php echo 'd'.$cont;?>" ><?php echo $dia;?></th>
           					<?php } ?>
                       		</tr>
                       		
                       	</thead>
                       	<tbody style="font-size: 10px">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>	
		</div>		
		<div id="salud" class="tab_content" style="margin-top: -10px;" >
			<table  style="margin-top: 5px" >
				<tr>
				<th rowspan="14">
					<div id="est_container" style="width: 610px; height: 560px;">
						<img style="width: 600px; height: 550px;" src="<?php echo base_url();?>assets/est/GOA.jpg" class="est_layer" id="est_layer_1" >
								<?php if($usuario=="Antonio Valdez" ||$usuario=="Antonio Hernandez" || $usuario=="Zuleima Benitez"){ ?>	 	
				 					<?php	
          								$cont=0;
										while($cont<77){ $cont+=1;?>
           			 						<img style=" width: 600px; height: 550px; z-index:<?php echo $cont+1;?> ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;" src="<?php echo base_url();?>assets/est/GOA<?php echo $cont;?>.png" class="est_layer" id="est_layer_<?php echo $cont+1;?>" >
           							<?php } ?>
    							<?php } ?>
					</div>					
				</th>
				<th rowspan="14" style="width: 5px"></th>
				<th>
					Día		
				</th>
				<th colspan="2">
					<input size="10%" type="text"  name="dia2" id="dia2" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
				</th>
				
				</tr>
				<tr>
					<th >Sección</th>
					<th colspan="2">
						<?php if($usuario=="Antonio Valdez" || $usuario=="Antonio Hernandez" || $usuario=="Zuleima Benitez"){ ?>	 	
				 					<select name="seccb" id="seccb" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="41">I</option>		
    									<option value="42">II</option>
    									<option value="43">III</option>
    									<option value="44">IV</option>
    								</select>
    							<?php } ?>
    				</th>					
				</tr>
				<tr>
					<th colspan="3" style="background-color: lightblue">SALUD</th>
				</tr>
				<tr>
					<th colspan="2">No. Estanques</th>
					<th>%</th>
					
				</tr>
				<tr>
					<th style="background-color: #0F0"></th>
					<th><input type="text" name='totv' id="totv" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					<th><input type="text" name='porv' id="porv" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					
				</tr>
				<tr>
					<th style="background-color: orange"></th>
					<th><input type="text" name='totn' id="totn" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					<th><input type="text" name='porn' id="porn" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					
				</tr>
				<tr>
					<th style="background-color: red"></th>
					<th><input type="text" name='totr' id="totr" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					<th><input type="text" name='porr' id="porr" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
				</tr>
				<tr>
					<th style="background-color: purple"></th>
					<th><input type="text" name='totp' id="totp" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					<th><input type="text" name='porp' id="porp" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
				</tr>
				<tr>
					<th colspan="4" style="background-color: lightblue">POBLACION</th>
				</tr>
				<tr>
					<th colspan="2">No. Estanques</th>
					<th>%</th>
					<th>kg/Ha</th>
				</tr>
				<tr>
					<th style="background-color: #0F0"> </th>
					<th><input type="text" name='totvp' id="totvp" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					<th><input type="text" name='porvp' id="porvp" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					<th style="background-color: #0F0; color: white"> < 1,300 </th>
				</tr>
				<tr>
					<th style="background-color: orange">  </th>
					<th><input type="text" name='totnp' id="totnp" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					<th><input type="text" name='pornp' id="pornp" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					<th style="background-color: orange; color: white"> > 1,300 y < 1500 </th>
				</tr>
				<tr>
					<th style="background-color: red">   </th>
					<th><input type="text" name='totrp' id="totrp" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					<th><input type="text" name='porrp' id="porrp" style="font-size: 16px;width: 50px; text-align: center; border: none"></th>
					<th style="background-color: red; color: white"> > 1,500 </th>
				</tr>
				<tr>
					<th colspan="3">
						<br /><br /><br /><br /><br /><br /><br /><br />
					</th>
				</tr>
			</table>
						
 		</div>
 </div>		



<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#secck").change( function(){	
	$("#est_container").empty();
	//$("#est_container").remove();
	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA.jpg', 'id': 'est_layer_1', 'class':'est_layer', 'style':'width: 600px; height: 550px;'  }).appendTo('#est_container');
	$("#seccb").val($("#secck").val());
	if($("#seccb").val()=='0'){
		 est = 0; est = 0;
  		 while ( est<77 ) {
  		 	est+=1;est1=est+1;
  		 	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA'+est+'.png', 'id': 'est_layer_'+est1+'', 'class':'est_layer', 'style':'width: 600px; height: 550px; z-index:'+est1+' ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;'  }).appendTo('#est_container');
   		 }
	}
	if($("#seccb").val()=='41'){
		 est = 0; est = 0;
  		 while ( est<=19 ) {
  		 	est+=1;est1=est+1;
  		 	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA'+est+'.png', 'id': 'est_layer_'+est1+'', 'class':'est_layer', 'style':'width: 600px; height: 550px; z-index:'+est1+' ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;'  }).appendTo('#est_container');
   		 }
	}
	if($("#seccb").val()=='42'){
		 est = 20; est = 0;
  		 while ( est<39 ) {
  		 	est+=1;est1=est+1;
  		 	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA'+est+'.png', 'id': 'est_layer_'+est1+'', 'class':'est_layer', 'style':'width: 600px; height: 550px; z-index:'+est1+' ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;'  }).appendTo('#est_container');
   		 }
	}
	if($("#seccb").val()=='43'){
		est = 40;  est = 0;
  		 while ( est<59 ) {
  		 	est+=1;est1=est+1;
  		 	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA'+est+'.png', 'id': 'est_layer_'+est1+'', 'class':'est_layer', 'style':'width: 600px; height: 550px; z-index:'+est1+' ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;'  }).appendTo('#est_container');
   		 }
	}
	if($("#seccb").val()=='44'){
		 est = 60; est = 0;
  		 while ( est<77 ) {
  		 	est+=1;est1=est+1;
  		 	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA'+est+'.png', 'id': 'est_layer_'+est1+'', 'class':'est_layer', 'style':'width: 600px; height: 550px; z-index:'+est1+' ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;'  }).appendTo('#est_container');
   		 }
	}
	$("#mytablakpi").trigger("update");
});
$("#seccb").change( function(){	
	$("#est_container").empty();
	//$("#est_container").remove();
	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA.jpg', 'id': 'est_layer_1', 'class':'est_layer', 'style':'width: 600px; height: 550px;'  }).appendTo('#est_container');
	if($("#seccb").val()=='0'){
		 est = 0; est = 0;
  		 while ( est<77 ) {
  		 	est+=1;est1=est+1;
  		 	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA'+est+'.png', 'id': 'est_layer_'+est1+'', 'class':'est_layer', 'style':'width: 600px; height: 550px; z-index:'+est1+' ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;'  }).appendTo('#est_container');
   		 }
	}
	if($("#seccb").val()=='41'){
		 est = 0; est = 0;
  		 while ( est<=19 ) {
  		 	est+=1;est1=est+1;
  		 	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA'+est+'.png', 'id': 'est_layer_'+est1+'', 'class':'est_layer', 'style':'width: 600px; height: 550px; z-index:'+est1+' ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;'  }).appendTo('#est_container');
   		 }
	}
	if($("#seccb").val()=='42'){
		 est = 20; est = 0;
  		 while ( est<39 ) {
  		 	est+=1;est1=est+1;
  		 	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA'+est+'.png', 'id': 'est_layer_'+est1+'', 'class':'est_layer', 'style':'width: 600px; height: 550px; z-index:'+est1+' ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;'  }).appendTo('#est_container');
   		 }
	}
	if($("#seccb").val()=='43'){
		 est = 40;  est = 0;
  		 while ( est<59 ) {
  		 	est+=1;est1=est+1;
  		 	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA'+est+'.png', 'id': 'est_layer_'+est1+'', 'class':'est_layer', 'style':'width: 600px; height: 550px; z-index:'+est1+' ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;'  }).appendTo('#est_container');
   		 }
 	}
	if($("#seccb").val()=='44'){
		 est = 60; est = 0;
  		 while ( est<77 ) {
  		 	est+=1;est1=est+1;
  		 	$('<img />', { 'src': '/OceanoAzul/assets/est/GOA'+est+'.png', 'id': 'est_layer_'+est1+'', 'class':'est_layer', 'style':'width: 600px; height: 550px; z-index:'+est1+' ; mix-blend-mode: multiply; -webkit-mix-blend-mode: multiply; background-color: none;'  }).appendTo('#est_container');
   		 }
	}
	$("#secck").val($("#seccb").val());
	$("#mytablakpi").trigger("update");		
	
});
$("#dia").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
	onSelect: function( selectedDate ){
		$("#dia2").val($("#dia").val());
		$("#mytablakpi").trigger("update");	
		
	}
});	

$("#diad").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
	onSelect: function( selectedDate ){	
		$("#mytabladias").trigger("update");			
	}
});	

$("#dia2").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
	onSelect: function( selectedDate ){
		//$(est_container).empty();
		//$(est_container).remove();
		$("#dia").val($("#dia2").val());
		$("#mytablakpi").trigger("update");	
		
	}
});	



jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  


$(document).ready(function(){ 
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   if((f.getDate() )<=9) dia="0" + (f.getDate());
   //$("#diaa").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());	
   $("#dia").val(f.getFullYear() + "-" + mes + "-" + dia);$("#dia2").val(f.getFullYear() + "-" + mes + "-" + dia);$("#diad").val(f.getFullYear() + "-" + mes + "-" + dia);
   $( "#dia" ).datepicker( "option", "maxDate", "+0m +0d" );
   $( "#dia2" ).datepicker( "option", "maxDate", "+0m +0d" );
   //$("#dia").val(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
   $("#tabla_dias").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpioce/tabladiasd',  
		filters:['cickpid','diad','loted','seccd'],	
        sort:false,
        onRowClick:function(){
    		
		}, 
		 onSuccess:function(){
		 	if($("#cickpid").val()=='2021-1'){
		 		c1='rgb(185,95,39)';c2='rgb(123,122,123)';c3='rgb(162,191,226)';c4='rgb(234,177,137)';
		 	}
	     	$('#tabla_dias tbody tr td:nth-child(1)').map(function(){
		 		$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		if($(this).html()==1){$(this).css("background-color", c1);$(this).css("color", "white");}
	    		if($(this).html()==2){$(this).css("background-color", c2);$(this).css("color", "white");}
	    		if($(this).html()==3){$(this).css("background-color", c3);$(this).css("color", "white");}
	    		if($(this).html()==4){$(this).css("background-color", c4);$(this).css("color", "white");}
	    	});
		 	$('#tabla_dias tbody tr td:nth-child(2)').map(function(){{$(this).css('font-weight','bold');}});
		 	con=3
		 	while(con<=33){
		 	$('#tabla_dias tbody tr td:nth-child('+con+')').map(function(){ 
				if($(this).html() == 1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
	    			if($(this).html() == 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "red");
        			}
        			if($(this).html() == 2 && $(this).html() != '' ) {
              				$(this).css("background-color", "purple"); $(this).css("color", "purple");
        			}
        			
	    	});
	    	con=con+1;
    		};
    	},
    });
	$("#tabla_kpi").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpioce/tablakpi', 
		filters:['cickpi','dia','lote','secck'],
		sort:false,	
		onSuccess:function(){  
			if($("#cickpi").val()=='2021-1'){
		 		c1='rgb(185,95,39)';c2='rgb(123,122,123)';c3='rgb(162,191,226)';c4='rgb(234,177,137)';
		 	}else{
		 		c1='rgb(185,95,39)';c2='rgb(123,122,123)';c3='rgb(162,191,226)';c4='rgb(234,177,137)';
		 	}
			$('#tabla_kpi tr').map(function(){	    		
	    		$(this).css('text-align','center');
	    	});	
	    	$('#tabla_kpi tbody tr td:nth-child(1)').map(function(){
	    		$(this).css('font-weight','bold');
	    		if($(this).html()==1){$(this).css("background-color", c1);$(this).css("color", "white");}
	    		if($(this).html()==2){$(this).css("background-color", c2);$(this).css("color", "white");}
	    		if($(this).html()==3){$(this).css("background-color", c3);$(this).css("color", "white");}
	    		if($(this).html()==4){$(this).css("background-color", c4);$(this).css("color", "white");}
	    	});
	    	$('#tabla_kpi tbody tr td:nth-child(2)').map(function(){{$(this).css('font-weight','bold');}});
	    	$('#tabla_kpi tbody tr td:nth-child(4)').map(function(){ 
				$(this).css('font-weight','bold');
	    			
	    			if($(this).html() >= 1300 && $(this).html() <= 1499 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); 
            		}
	    			if($(this).html() >= 1500 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); 
        			}
        			
	    	}),    					
			$('#tabla_kpi tbody tr td:nth-child(5)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() == 1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
	    			if($(this).html() == 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "red");
        			}
        			if($(this).html() == 2 && $(this).html() != '' ) {
              				$(this).css("background-color", "purple"); $(this).css("color", "purple");
        			}
        			
	    	}),
	    	$('#tabla_kpi tbody tr td:nth-child(7)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 3.1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); 
            		}
	    			if($(this).html() >= 2.5 && $(this).html() <= 3.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); 
            		}
	    			if($(this).html() <= 2.4 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); 
        			}
        			
	    	}),
	    	$('#tabla_kpi tbody tr td:nth-child(14)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 5 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); 
            		}
	    			if($(this).html() >= -4.99 && $(this).html() <= 4.99 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); 
            		}
            		if($(this).html() == 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "yellow"); 
            		}
	    			if($(this).html() <= -5 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); 
        			}
        			
	    	});
	    	$('#tabla_kpi tbody tr').map(function(){
		    	if($(this).data('pisge')=='Prom:') {	    				    			
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    			$(this).css('font-size','14px');
	    			$(this).css("background-color", "lightblue"); 
	    		}
		    });
	    	$('#tabla_kpi tbody tr').map(function(){
	    		$("#totv").val($(this).data('totv'));
	    		$("#totn").val($(this).data('totn'));
	    		$("#totr").val($(this).data('totr'));
	    		$("#totp").val($(this).data('totp')); 
	    		$("#porv").val($(this).data('porv'));
	    		$("#porn").val($(this).data('porn'));
	    		$("#porr").val($(this).data('porr'));
	    		$("#porp").val($(this).data('porp'));
	    		$("#totvp").val($(this).data('totvp'));
	    		$("#totnp").val($(this).data('totnp'));
	    		$("#totrp").val($(this).data('totrp')); 
	    		$("#porvp").val($(this).data('porvp'));
	    		$("#pornp").val($(this).data('pornp'));
	    		$("#porrp").val($(this).data('porrp'));
	    		$('#est_layer_2').css('background-color', $(this).data('est1'));	    		
	    		$('#est_layer_3').css('background-color', $(this).data('est2'));
	    		$('#est_layer_4').css('background-color', $(this).data('est3'));
	    		$('#est_layer_5').css('background-color', $(this).data('est4'));
	    		$('#est_layer_6').css('background-color', $(this).data('est5'));
	    		$('#est_layer_7').css('background-color', $(this).data('est6'));
	    		$('#est_layer_8').css('background-color', $(this).data('est7'));
	    		$('#est_layer_9').css('background-color', $(this).data('est8'));
	    		$('#est_layer_10').css('background-color', $(this).data('est9'));
	    		$('#est_layer_11').css('background-color', $(this).data('est10'));
	    		$('#est_layer_12').css('background-color', $(this).data('est11'));	    		
	    		$('#est_layer_13').css('background-color', $(this).data('est12'));
	    		$('#est_layer_14').css('background-color', $(this).data('est13'));
	    		$('#est_layer_15').css('background-color', $(this).data('est14'));
	    		$('#est_layer_16').css('background-color', $(this).data('est15'));
	    		$('#est_layer_17').css('background-color', $(this).data('est16'));
	    		$('#est_layer_18').css('background-color', $(this).data('est17'));
	    		$('#est_layer_19').css('background-color', $(this).data('est18'));
	    		$('#est_layer_20').css('background-color', $(this).data('est19'));
	    		$('#est_layer_21').css('background-color', $(this).data('est20'));
	    		$('#est_layer_22').css('background-color', $(this).data('est21'));	    		
	    		$('#est_layer_23').css('background-color', $(this).data('est22'));
	    		$('#est_layer_24').css('background-color', $(this).data('est23'));
	    		$('#est_layer_25').css('background-color', $(this).data('est24'));
	    		$('#est_layer_26').css('background-color', $(this).data('est25'));
	    		$('#est_layer_27').css('background-color', $(this).data('est26'));
	    		$('#est_layer_28').css('background-color', $(this).data('est27'));
	    		$('#est_layer_29').css('background-color', $(this).data('est28'));
	    		$('#est_layer_30').css('background-color', $(this).data('est29'));
	    		$('#est_layer_31').css('background-color', $(this).data('est30'));
	    		$('#est_layer_32').css('background-color', $(this).data('est31'));
	    		$('#est_layer_33').css('background-color', $(this).data('est32'));
	    		$('#est_layer_34').css('background-color', $(this).data('est33'));
	    		$('#est_layer_35').css('background-color', $(this).data('est34'));
	    		$('#est_layer_36').css('background-color', $(this).data('est35'));
	    		$('#est_layer_37').css('background-color', $(this).data('est36'));
	    		$('#est_layer_38').css('background-color', $(this).data('est37'));
	    		$('#est_layer_39').css('background-color', $(this).data('est38'));
	    		$('#est_layer_40').css('background-color', $(this).data('est39'));
	    		$('#est_layer_41').css('background-color', $(this).data('est40'));
	    		$('#est_layer_42').css('background-color', $(this).data('est41'));
	    		$('#est_layer_43').css('background-color', $(this).data('est42'));
	    		$('#est_layer_44').css('background-color', $(this).data('est43'));
	    		$('#est_layer_45').css('background-color', $(this).data('est44'));
	    		$('#est_layer_46').css('background-color', $(this).data('est45'));
	    		$('#est_layer_47').css('background-color', $(this).data('est46'));
	    		$('#est_layer_48').css('background-color', $(this).data('est47'));
	    		$('#est_layer_49').css('background-color', $(this).data('est48'));
	    		$('#est_layer_50').css('background-color', $(this).data('est49'));
	    		$('#est_layer_51').css('background-color', $(this).data('est50'));
	    		$('#est_layer_52').css('background-color', $(this).data('est51'));
	    		$('#est_layer_53').css('background-color', $(this).data('est52'));
	    		$('#est_layer_54').css('background-color', $(this).data('est53'));
	    		$('#est_layer_55').css('background-color', $(this).data('est54'));
	    		$('#est_layer_56').css('background-color', $(this).data('est55'));
	    		$('#est_layer_57').css('background-color', $(this).data('est56'));
	    		$('#est_layer_58').css('background-color', $(this).data('est57'));
	    		$('#est_layer_59').css('background-color', $(this).data('est58'));
	    		$('#est_layer_60').css('background-color', $(this).data('est59'));
	    		$('#est_layer_61').css('background-color', $(this).data('est60'));
	    		$('#est_layer_62').css('background-color', $(this).data('est61'));
	    		$('#est_layer_63').css('background-color', $(this).data('est62'));
	    		$('#est_layer_64').css('background-color', $(this).data('est63'));
	    		$('#est_layer_65').css('background-color', $(this).data('est64'));
	    		$('#est_layer_66').css('background-color', $(this).data('est65'));
	    		$('#est_layer_67').css('background-color', $(this).data('est66'));
	    		$('#est_layer_68').css('background-color', $(this).data('est67'));
	    		$('#est_layer_69').css('background-color', $(this).data('est68'));
	    		$('#est_layer_70').css('background-color', $(this).data('est69'));
	    		$('#est_layer_71').css('background-color', $(this).data('est70'));
	    		$('#est_layer_72').css('background-color', $(this).data('est71'));
	    		$('#est_layer_73').css('background-color', $(this).data('est72'));
	    		$('#est_layer_74').css('background-color', $(this).data('est73'));
	    		$('#est_layer_75').css('background-color', $(this).data('est74'));
	    		$('#est_layer_76').css('background-color', $(this).data('est75'));
	    		$('#est_layer_77').css('background-color', $(this).data('est76'));
	    		$('#est_layer_78').css('background-color', $(this).data('est77'));
	    	});		
		},	
	});
	
		// asigna valor por defecto
    $('select[name="colorpicker-shortlist"]').simplecolorpicker('selectColor', '#7bd148');
    /*$('#est_layer_2').css('background-color', '#0F0');
    $('#est_layer_3').css('background-color', 'orange');
	$('#est_layer_4').css('background-color', 'red');
	$('#est_layer_5').css('background-color', 'orange');
	$('#est_layer_6').css('background-color', '#0F0');
	$('#est_layer_7').css('background-color', '#0F0');
	$('#est_layer_8').css('background-color', '#0F0');    
	$('#est_layer_9').css('background-color', '#0F0');
	$('#est_layer_10').css('background-color', '#0F0');
	$('#est_layer_11').css('background-color', '#0F0');
	$('#est_layer_12').css('background-color', '#0F0');
	$('#est_layer_13').css('background-color', '#0F0');
	$('#est_layer_14').css('background-color', '#0F0');
	*/
    //cambia el color según la selección
    /*
    $('select[name="colorpicker-shortlist"]').on('change', function() {
    	$('#est_layer_1').css('background-color', $('select[name="colorpicker-shortlist"]').val());
    	$('#est_layer_2').css('background-color', $('select[name="colorpicker-shortlist"]').val());
    	$('#est_layer_3').css('background-color', $('select[name="colorpicker-shortlist"]').val());
    });
 */
});
</script>