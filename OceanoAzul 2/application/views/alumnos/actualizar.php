<html>
<head>
<title>Lista de Alumnos</title>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-1.7.2.min.js"></script>
</head>
<body>
<div>
<form method="post" action="<?=base_url()?>index.php/alumnos/actualizar" id="form">
	<input type="hidden" value="<?= $resultado->id?>" name="id" id="id"/>
	<label for="nombre">Nombre:</label>
    <input type="text" name="nombre" id="nombre" value="<?=$resultado->nombre?>"><br/>
    <label for="edad">Edad:</label>
    <input type="text" name="edad" id="edad" value="<?=$resultado->edad?>"><br/>
    <button name="boton" value="1">Agregar alumno</button>
</form>
</div>
<div><a href="<?= base_url()?>index.php/alumnos">lista</a></div>
</body>
</html>
<script>
// Este evento verifica que el valor sea numero
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
// El evento submit sucede cuando se envía la información del formulario para lo cual se realizó
// Un  script  que valida que exista un nombre capturado y que la edad sea un valor numérico
// si todo esta correo envía un  valor de verdadero y se hace el envio
$("#form").submit(function(){
	nombre=$("#nombre").val();
	edad=$("#edad").val();
	if(nombre!=''){
		if( is_int(edad)){
			return true;
		}else{
			alert("Error: edad no valido");	
				return false;
		}
	}else{
		alert("Error: nombre no valido");
			return false;
	}
	return false;	
});
</script>