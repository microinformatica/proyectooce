<?php $this->load->view('header_cv'); ?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   

</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/cliente.png" width="25" height="25" border="0"> Almacenes </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion" style="font-size: 12px">
			<h3 align="left" style="font-size: 12px" ><a href="#"> Registrados </a></h3>
        	<div  style="height:345px; text-align: right; font-size: 12px;"> 
        		Filtrar aquellos que les falte la captura de: 
        		<select name="cmbSindatos" id="cmbSindatos" style="font-size: 10px; height: 25px;  margin-top: -15px;" >
          				<option value="0">Elija Opcion</option>
          				<option value="1">Domicilio</option><option value="2">Localidad</option><option value="3">Estado</option>
          				<option value="4">RFC</option><option value="5">CP</option><option value="6">Zona</option>
          		</select>                                                                   
            	<div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" style="margin-top: 1px; height: 367px; " >                
                	<span class="ajaxTable" style="height: 301px; width: 882px">
                    	<table id="mytabla" name="mytabla" class="ajaxSorter" width="880px">
                        	<thead title="" >                            
                            	<th data-order = "Zona" >Zona</th> <th data-order = "RFC" >RFC</th> 	<th data-order = "noma" >Razon Social</th>                                                        
                            	<th data-order = "Dom" >Domicilio</th>  <th data-order = "Loc" >Localidad</th>
                            	<th data-order = "Edo" >Estado</th>                                 
                        	</thead>
                        	<tbody title="Seleccione para realizar cambios" style="font-size: 11px">  	</tbody>
                    	</table>
                	</span> 
             		<div class="ajaxpager" style="margin-top: -5px">        
                    	<ul class="order_list"></ul>     
                    	<form method="post" action="<?= base_url()?>index.php/almacenes/pdfrep" >                    	             	                                                
                        	<img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                        	<img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                        	<input type="text" class="pagedisplay" size="3" /> /
                        	<input type="text" class="pagedisplayMax" size="3" readonly="1" disabled="disabled"/>
                        	<img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                        	<img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                        	<select class="pagesize" style="font-size: 10px; height: 23px">
                                <option selected="selected" value="10">10</option>
                                <option value="20">20</option>                            
                                <option value='0'>Gral</option>                                          
                        	</select>                                                
                        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        	<!--<button id="imprimirB" name="imprimirB" style="height: 28px; width: 25px;"></button>-->
                        	<!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 						</form>   
 					</div>                                      
            </div>    
        </div>
        <h3 align="left" style="font-size: 12px" ><a href="#"> Agregar Nuevo </a></h3>
       	<div style="height:150px; ">  
        	<table  border="1px">
				<thead style="background-color: #DBE5F1" ><th colspan="4" height="18px" style="font-size: 20px"><center>Datos de Almacen</center>
						<input type="hidden"  name="id" id="id"/></th>
				</thead>
				<tbody style="background-color: #F7F7F7">
					<tr>
						<th ><label style="margin: 2px; font-size: 16px" for="nombre">Razón Social</label></th>
						<th colspan="3" style="font-size: 14px; background-color: white"><input size="80%"  type="text" name="nombre" id="nombre" ></th>
					</tr>
					<tr>
						<th style="text-align: right"><label style="margin: 2px; font-size: 16px;" for="dom">Domicilio</label></th>
						<th colspan="3" style="font-size: 14px; background-color: white"><input size="80%" type="text" name="dom" id="dom" value=""></th>
					</tr>
					
    				<tr>
						<th style="text-align: right"><label style="margin: 2px; font-size: 16px" for="loc">Localidad</label></th>
						<th colspan="3" style="font-size: 14px; background-color: white"><input size="80%" type="text" name="loc" id="loc" value=""></th>
					</tr>
    				<tr>
						<th style="text-align: right"><label style="margin: 2px; font-size: 16px" for="edo">Estado</label></th>
						<th style="font-size: 14px; background-color: white">
						<select name="edo" id="edo" style="margin-top: 1px">
    						<option value="-" >-</option> <option value="Aguascalientes">Aguascalientes</option>
    						<option value="Baja California">Baja California</option> <option value="Baja California Sur" >Baja California Sur</option>	
    						<option value="Campeche">Campeche</option> <option value="Chiapas"  >Chiapas</option>	
    						<option value="Chihuahua">Chihuahua</option> <option value="Coahulia" >Coahuila</option>
    						<option value="Colima" >Colima</option> <option value="Distrito Federal" >Distrito Federal</option>	
    						<option value="Durango" >Durango</option> <option value="Guanajuato" >Guanajuato</option>	
    						<option value="Guerrero" >Guerrero</option> <option value="Hidalgo" >Hidalgo</option>	
    						<option value="Jalisco" >Jalisco</option> <option value="Estado de Mexico" >Estado de M&eacute;xico</option>
    						<option value="Michoacan" >Michoac&aacute;n</option> <option value="Morelos" >Morelos</option>		
    						<option value="Nayarit" >Nayarit</option> <option value="Nuevo Leon" >Nuevo Le&oacute;n</option>			
    						<option value="Oaxaca" >Oaxaca</option> <option value="Puebla" >Puebla</option>
    						<option value="Queretaro" >Queretaro</option> <option value="Quintana Roo" >Quintana Roo</option>
    						<option value="San Luis Potosi" >San Luis Potos&iacute;</option> <option value="Sinaloa" >Sinaloa</option>
    						<option value="Sonora" >Sonora</option> <option value="Tamaulipas" >Tamaulipas</option>
    						<option value="Tabasco" >Tabasco</option> <option value="Tlaxcala" >Tlaxcala</option>
    						<option value="Veracruz" >Veracruz</option> <option value="Yucatan" >Yucat&aacute;n</option>
    						<option value="Zacatecas" >Zacatecas</option>
  						</select>
						</th>
						<th style="text-align: right"><label style="margin: 2px; font-size: 16px" for="zona">Zona</label></th>
						<th style="font-size: 14px; background-color: white">										
						<select name="zona" id="zona" style="margin-top: 1px">
    						<option value="-">-</option>	<option value="Angostura">Angostura</option>
    						<option value="Campeche">Campeche</option> <option value="Chiapas"  >Chiapas</option> <option value="Colima">Colima</option>	
    						<option value="El Dorado">El Dorado</option> <option value="Elota">Elota</option>	
							<option value="Guasave">Guasave</option> <option value="Guaymas">Guaymas</option> <option value="Guerrero">Guerrero</option>	
    						<option value="Hermosillo">Hermosillo</option> <option value="Mochis">Mochis</option>
    						<option value="Navojoa">Navojoa</option>  <option value="Navolato">Navolato</option>	
    						<option value="Nayarit">Nayarit</option>  <option value="Obregón">Obregón</option>	
    						<option value="Sur Sinaloa">Sur Sinaloa</option>  <option value="Tamaulipas">Tamaulipas</option>	
    						<option value="Tabasco">Tabasco</option>  <option value="Varias">Varias</option>
    						<option value="Veracruz">Veracruz</option>	<option value="Yucatán">Yucatán</option>		
  						</select>
  						 
						</th>
					</tr>
    				<tr>
						<th style="text-align: right"><label style="margin: 2px; font-size: 16px" for="rfc">RFC</label></th>
						<th style="font-size: 14px; background-color: white"><input type="text" name="rfc" id="rfc" value=""></th>
						<th style="text-align: right"><label style="margin: 2px; font-size: 16px" for="cp">CP</label></th>
						<th style="font-size: 14px; background-color: white"><input type="text" size="5%" name="cp" id="cp" value=""></th>
					</tr>
    				
					
					<tr>
						<th colspan="4">Otros</th>
					</tr>
					<tr>
						<th><label style="margin: 2px; font-size: 16px" for="con">Contacto</label></th>
						<th style="font-size: 14px; background-color: white"><input  style="width: 450px" type="text" name="con" id="con" value=""></th>
						<th><label style="margin: 2px; font-size: 16px" for="tel">Teléfono</label></th>
						<th style="font-size: 14px; background-color: white"><input type="text" size="25%" name="tel" id="tel" value=""></th>
					</tr>
					<tr>
						<th><label style="margin: 2px; font-size: 16px" for="cor">Correo</label></th>
						<th colspan="3" style="font-size: 14px; background-color: white"><input size="80%" type="text" name="cor" id="cor" value=""></th>
					</tr>
					<tr>
						<th colspan="4">Traslado</th>
					</tr>
					<tr>
    					<th ><label style="margin: 2px; font-size: 16px" for="mun">Enc. Ofi. Pesca</label></th>
						<th style="font-size: 14px; background-color: white"><input name="pesnom" id="pesnom" style="width: 450px">							
						</th>
    					<th ><label style="margin: 2px; font-size: 16px" for="guia">Puesto</label></th>
						<th style="font-size: 14px; background-color: white"><input name="pespue" id="pespue"></th>
						
					</tr>
					<tr>
    					<th ><label style="margin: 2px; font-size: 16px" for="mun">Aviso Cosecha</label></th>
						<th style="font-size: 14px; background-color: white"><input name="avisoc" id="avisoc" style="width: 80px">
							Fecha:<input size="10%" type="text" name="fecavi" id="fecavi" class="fecha redondo mUp" readonly="true" style="text-align: center;font-size: 12px;" >
						</th>
    					<th ><label style="margin: 2px; font-size: 16px" for="guia">Guia</label></th>
						<th style="font-size: 14px; background-color: white"><input name="guia" id="guia"></th>
						
					</tr>
					<tr>
						<th colspan="4">Transporte</th>
					</tr>
					<tr>
    					
    					<th colspan="4" style="font-size: 14px; background-color: white">
    						Flete:<input name="flete" id="flete"  style="width:200px">
    						Chofer:<input name="chofer" id="chofer"  style="width:350px"><br />
    						Tipo:<input style="width: 120px" name="tipo" id="tipo">
							Marca:<input name="marca" id="marca">
							Modelo:<input style="width: 40px" name="modelo" id="modelo">
							Placas:<input style="width: 90px" name="placas" id="placas">
							Color:<input name="color" id="color">
							
						</th>
						
					</tr>
				</tbody>
				<tfoot style="background-color:#DBE5F1">
					<th colspan="4"><center>
						<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez" || $usuario=="Efrain Lizárraga"){ ?>
							<input style="font-size: 18px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
						<?php }?>
						<input style="font-size: 18px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
						<?php if($usuario=="Jesus Benítez" || $usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez" ){ ?>
						<input style="font-size: 18px;" type="submit" id="pborrar" name="pborrar" value="Borrar" />
						<?php }?>
						</center></th>
				</tfoot>
    		</table>
        </div>	
	 </div> 	
 	</div> 	
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#fecavi").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",		
});


jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

$("#pborrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/almacenes/borrara", 
						data: "id="+$("#id").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Cliente Eliminado");
										$("#nuevo").click();
										$("#mytabla").trigger("update");
										$("#accordion").accordion( "activate",0 );
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Cliente para poder Eliminarlo");
		return false;
	}
});

$("#nuevo").click( function(){	
	$("#id").val('');$("#nombre").val('');$("#dom").val('');$("#loc").val('');$("#edo").val('');$("#rfc").val('');
	$("#cp").val('');$("#con").val('');$("#cor").val('');$("#tel").val('');$("#pesnom").val('');$("#pespue").val('');
	$("#zona").val('');$("#avisoc").val('');$("#fecavi").val('');$("#flete").val('');
	$("#tipo").val('');$("#marca").val('');$("#modelo").val('');$("#color").val('');$("#placas").val('');$("#chofer").val('');$("#guia").val('');
	$("#nombre").focus();
 return true;
}
)
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#aceptar").click( function(){	
	nombre=$("#nombre").val();
	cp=$("#cp").val();
	if(cp==""){ cp=0; }
	numero=$("#id").val();
	if(nombre!=''){
		if( is_int(cp)){
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/almacenes/actualizar", 
						data: "id="+$("#id").val()+"&nombre="+$("#nombre").val()+"&dom="+$("#dom").val()+"&loc="+$("#loc").val()+"&edo="+$("#edo").val()+"&rfc="+$("#rfc").val()+"&cp="+$("#cp").val()+"&zona="+$("#zona").val()+"&con="+$("#con").val()+"&cor="+$("#cor").val()+"&tel="+$("#tel").val()+"&avi="+$("#avisoc").val()+"&fec="+$("#fecavi").val()+"&tip="+$("#tipo").val()+"&mar="+$("#marca").val()+"&mod="+$("#modelo").val()+"&col="+$("#color").val()+"&pla="+$("#placas").val()+"&cho="+$("#chofer").val()+"&gui="+$("#guia").val()+"&fle="+$("#flete").val()+"&pes="+$("#pesnom").val()+"&pue="+$("#pespue").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytabla").trigger("update");
										$("#accordion").accordion( "activate",0 );	
										$("#nuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/almacenes/agregar", 
						data: "&nombre="+$("#nombre").val()+"&dom="+$("#dom").val()+"&loc="+$("#loc").val()+"&edo="+$("#edo").val()+"&rfc="+$("#rfc").val()+"&cp="+$("#cp").val()+"&zona="+$("#zona").val()+"&con="+$("#con").val()+"&cor="+$("#cor").val()+"&tel="+$("#tel").val()+"&avi="+$("#avisoc").val()+"&fec="+$("#fecavi").val()+"&tip="+$("#tipo").val()+"&mar="+$("#marca").val()+"&mod="+$("#modelo").val()+"&col="+$("#color").val()+"&pla="+$("#placas").val()+"&cho="+$("#chofer").val()+"&gui="+$("#guia").val()+"&fle="+$("#flete").val()+"&pes="+$("#pesnom").val()+"&pue="+$("#pespue").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Cliente registrado correctamente");
										$("#mytabla").trigger("update");
										$("#accordion").accordion( "activate",0 );		
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Codigo Postal no valido");	
			$("#cp").focus();
				return false;
		}
	}else{
		alert("Error: Nombre de Cliente no valido");
		$("#nombre").focus();
			return false;
	}
	 // return false;
});

$(document).ready(function(){
	$("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/almacenes/tabla',  
		filters:['cmbSindatos'],
        //active:['Edo','Sinaloa'],  
        multipleFilter:true,
        //sort:false,
        onRowClick:function(){
            $("#accordion").accordion( "activate",1 );
           	$("#id").val($(this).data('ida'));$("#nombre").val($(this).data('noma'));$("#dom").val($(this).data('dom'));$("#loc").val($(this).data('loc'));
			$("#edo").val($(this).data('edo'));$("#rfc").val($(this).data('rfc'));$("#cp").val($(this).data('cp'));$("#con").val($(this).data('cona'));
			$("#cor").val($(this).data('cor'));$("#tel").val($(this).data('tel'));$("#zona").val($(this).data('zona'));$("#avisoc").val($(this).data('avisoc'));
			$("#fecavi").val($(this).data('fecavi'));$("#tipo").val($(this).data('tipo'));$("#marca").val($(this).data('marca'));$("#modelo").val($(this).data('modelo'));
			$("#color").val($(this).data('color'));$("#placas").val($(this).data('placas'));$("#chofer").val($(this).data('chofer'));$("#guia").val($(this).data('guia'));
			$("#flete").val($(this).data('flete'));$("#pesnom").val($(this).data('pesnom'));$("#pespue").val($(this).data('pespue'));
			$("#nombre").focus();			
        }   	
	});
});
</script>