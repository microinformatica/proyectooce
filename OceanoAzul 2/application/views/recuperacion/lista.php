<?php $this->load->view('header_cv');?>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/vault.png" width="25" height="25" border="0"> Recuperación </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px" >
		    <div class="ajaxSorterDiv" id="tabla_recu" name="tabla_recu" align="left" style="height: 235px"  >       
		    	<div class="ajaxpager" >        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/recuperacion/pdfrep" >
                    	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                    </form>   
 				</div>      	
            	<span class="ajaxTable" style=" height: 233px; width: 935px; background-color: white; margin-top: -25px ">
                    <table id="mytablaRecu" name="mytablaRecu" class="ajaxSorter" border="1">
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <tr>
                            	<th rowspan="2" data-order = "cic" style="width: 30px" >Ciclo</th>                            
                            	<th colspan="4">Ventas</th>
                            	<th colspan="3">Recuperaciones</th>
                            	<th ></th>
                            </tr>
                            <tr>
                            	<th data-order = "pls">Postlarvas</th>
                            	<th data-order = "impo">Importe Pls</th>
                            	<th data-order = "carg">+ Cargos</th>
                            	<th data-order = "totvtas">Total</th>
                            	<th data-order = "depo">Depósitos</th>
                            	<th data-order = "dscto">+ Desctos</th>
                            	<th data-order = "totrecu">Total</th>
                            	<th data-order = "saldo" >Saldo</th>	
                            </tr>
                        </thead>
                        <tbody title="Seleccione para analizar" style="font-size: 11px">
                        </tbody>
                        
                    </table>
                 </span>
                 
 			</div>
 			<div id='ver_mas_det' class='hide' style="height: 240px;" >
 			<div class="ajaxSorterDiv" id="tabla_reca" name="tabla_reca" align="left" style="height: 240px; margin-top: -10px"  > 
 				           	
            	<span class="ajaxTable" style=" height: 197px; width: 935px; background-color: white;  ">
                    <table id="mytablaReca" name="mytablaReca" class="ajaxSorter" border="1">
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <tr>
                            	<th colspan="2" >Clientes  <input size="8%" name="ciclosel" id="ciclosel" style="border: none; text-align: center; font-weight: bold"/>
                           		</th>
                            	<th colspan="3">Ventas</th>
                            	<th colspan="3">Recuperaciones</th>
                            	<th ></th>
                            </tr>
                            <tr>
                            	<th data-order = "zon" >Zona</th>
                            	<th data-order = "raz" >Razón Social</th>
                            	<th data-order = "pls">Postlarvas</th>
                            	<th data-order = "imp1">Importe Pls</th>
                            	<th data-order = "car1">+ Cargos</th>
                            	<th data-order = "dep1">Depósitos</th>
                            	<th data-order = "des1">+ Desctos</th>
                            	<th data-order = "cam1">Camarones</th>
                            	<th data-order = "saldo" >Saldo</th>	
                            </tr>
                        </thead>
                        <tbody title="" style="font-size: 11px">
                        </tbody>
                        
                    </table>
                 </span>
                 <div class="ajaxpager" style="margin-top: -15px">        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/recuperacion/pdfrep" >
                    	<select name="cmbCiclo" id="cmbCiclo" style="width: 5px; visibility: hidden"  >
                    		<option value="0">Seleccione Ciclo</option>
                    		<?php $ciclof=2008; $actual=date("Y"); 
							if ($actual<2016) $actual+=1;  //cuando se entregan en diciembre se suma uno, 2015 se cambia en diciembre a 2016
							while($actual >= $ciclof){?>
							<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            					<?php  $actual-=1; } ?>
          				</select>
                        Zona: <select name="cmbZonas" id="cmbZonas" style="font-size: 10px; height: 25px; margin-top: -10px "></select>
                    	<img style="cursor: pointer;" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/>
                        <input type="hidden" size="3%" name="ciclo" id="ciclo" /></th> 
                    </form>   
 				</div> 
 			</div>
 			</div>
   </div>             
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#cmbCiclo").change( function(){		
	$("#cmbZonas").val(0);	
 return true;
});

$(document).ready(function(){ 
	$(this).removeClass('used');
	$('#ver_mas_det').hide();
	$("#tabla_recu").ajaxSorter({
		url:'<?php echo base_url()?>index.php/recuperacion/tabla',  		
		//filters:['cer'],
    	sort:false,
    	onRowClick:function(){
    		if($(this).data('cic')!='Total'){
    			$(this).removeClass('used');
				$('#ver_mas_det').show();
    			$("#ciclosel").val('Ciclo-'+$(this).data('cic'));
    			$("#ciclo").val($(this).data('cic'));
    			$("#cmbCiclo").val($(this).data('cic'));
    			$("#cmbZonas").boxLoader({
        		    url:'<?php echo base_url()?>index.php/recuperacion/combo',
            		equal:{id:'actual',value:'#cmbCiclo'},
            		select:{id:'zona',val:'val'},
            		all:"Todos",
            	});
    			$("#tabla_reca").ajaxSorter({
					url:'<?php echo base_url()?>index.php/recuperacion/tablaa',
					filters:['ciclo','cmbZonas'],  		
			    	sort:false,
    				onSuccess:function(){
						$('#tabla_reca tbody tr').map(function(){
		    				if($(this).data('zon')=='Total'){
		    					$(this).css('background','lightblue');	$(this).css('font-size','12px');$(this).css('font-weight','bold');
		    				}
	    					$(this).css('text-align','right');
	    				})
	    				$('#tabla_reca tbody tr').map(function(){
		    				if($(this).data('zon')=='Gral Ciclo'){
		    					$(this).css('background','lightcoral');	$(this).css('font-size','12px');$(this).css('font-weight','bold');
		    				}
	    					$(this).css('text-align','right');
	    				})
	    				$('#tabla_reca tbody tr td:nth-child(1)').map(function(){ $(this).css('font-size','12px'); $(this).css('text-align','left'); $(this).css('font-weight','bold'); })	    				
	    				$('#tabla_reca tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','left'); })
	    				$('#tabla_reca tbody tr td:nth-child(10)').map(function(){ $(this).css('font-size','12px'); $(this).css('font-weight','bold'); })
	    			},
				});
			}		
    	}, 
    	onSuccess:function(){
			$('#tabla_recu tbody tr').map(function(){
		    	if($(this).data('cic')=='Total'){
		    		$(this).css('background','lightblue');	$(this).css('font-size','12px');$(this).css('font-weight','bold');
		    	}
	    		$(this).css('text-align','right');
	    	})
	    	$('#tabla_recu tbody tr td:nth-child(1)').map(function(){ $(this).css('font-size','12px'); $(this).css('text-align','center'); $(this).css('font-weight','bold'); })	    				
	    	$('#tabla_recu tbody tr td:nth-child(9)').map(function(){ $(this).css('font-size','12px'); $(this).css('font-weight','bold'); })
	    },
	});
});  	
</script>