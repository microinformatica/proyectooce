<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
#imprimir_pdf { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li ><a href="#entregas" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/truck.png" width="25" height="25" border="0"> Entregas  </a></li>
		<li ><a href="#resultados" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/feliz.gif" width="25" height="25" border="0"> Evaluaciones  </a></li>			
		</strong>
	</ul>
	
	<div class="tab_container" id="tabPrincipal" style="height: 472px"  >	
		<div id="entregas" class="tab_content" style="margin-top: -5px;">
		<div id="accordion" style="width: 959px;  margin-left: -10px; margin-top: -5px; font-size: 12px; background-color: none;">
			<h3 align="left"><a href="">Relación de Entregas a Clientes</a></h3>
        	<div  style="height:350px;">                                                                      
            <div class="ajaxSorterDiv" id="tabla_ent" name="tabla_ent" align="left" style=" height: 396px; margin-top: -10px; " >             	
            	<div style="margin-top: -10px;">
            	Procesar del Día <input size="12%" type="text" name="txtFI" id="txtFI" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                 al <input size="12%" type="text" name="txtFF" id="txtFF"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Seleccion por estatus Actual:<select id="cmbEstatus" style="font-size: 10px; height: 22px; ">
						<option value="10">Todas</option>
						<option value="1">Por Procesar</option>
						<option value="-1">Verificadas</option>						
						<option value="0">Pendientes</option>
						<option style=" color: white;  background-color: #A62A2A" value="2">Críticas</option>
						<option style=" color: white;  background-color: gray" value="3">Equipo</option>
					</select>
				</div>	       
                <span class="ajaxTable" style=" height: 335px; width: 880px ">
                    <table id="mytablaE" name="mytablaE" class="ajaxSorter" ">
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px"> 
                        	<th data-order = "FechaR" style="width: 55px">Fecha</th>
                            <th data-order = "Zona" style="width: 70px">Zona</th>                                                        
                            <th data-order = "Razon1" style="width: 150px">Cliente</th>                            
                            <th data-order = "Folio" style="width: 50px">Folio</th>
                            <th data-order = "RemisionR" style="width: 50px;">Remision</th>
                            <th data-order = "CantidadRR" style="width: 70px" >Facturado</th>  
                            <th data-order = "Obs" style="width: 70px" >Observaciones</th>                                                     
                            <!--<th data-order = "totd" style="width: 70px" >Dia</th>--> 
                        </thead>
                        <?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino"){ ?>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        
                        <?php } else {?>
                        <tbody title="Seleccione para analizar" style="font-size: 10px">	
                        <?php }?>
                        </tbody>
                    </table>
                    
                </span> 
             	<div class="ajaxpager" style="margin-top: -15px">        
                    <ul  style="width: 450px" class="order_list">En Total:  <input size="2%"  type="text" name="tote" id="tote" style="text-align: left;  border: 0; background: none"> 
                    	<sup>(Fila Gris) Observaciones en Herremientas de transportación.</sup> 
                    </ul>                          
                    <form method="post" action="<?= base_url()?>index.php/entregas/pdfrep" >
                    	
                                                                        
                        <img style="cursor: pointer" style="margin-top: 6px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                                                                          
 					</form>   
 					
                </div>  
                                                 
            </div>             
        </div>
        <?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino"){ ?>
        <h3 align="left"><a href="" >Actualizar o Eliminar</a></h3>
       
        <?php } else {?>
        <h3 align="left"><a href="" >Analizar Solicitud</a></h3>		
        <?php } ?>	
       	<div style="height: 150px;">
		       	 <table style="width: 750px; margin-top: -5px" border="2px">
    						<thead style="background-color: #DBE5F1">
								<th colspan="6">PROCESO DE EVALUACION <input size="2%" type="hidden" name="id" id="id" readonly="true" >
									<input size="2%" type="hidden" name="nr" id="nr" readonly="true" >
									<input size="2%" type="hidden" name="nc" id="nc" readonly="true" >
									<br>
									Cliente: <input size="70%" type="text" name="nom" id="nom" readonly="true" >
									Técnico: <select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> name="enc" id="enc" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
          							$this->load->model('entregas_model');
									$data['result']=$this->entregas_model->verTecnico();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumBio;?>" ><?php echo $row->NomBio;?></option>
           							<?php endforeach;?>
   									</select>
   									Remisión: <input size="4%" type="text" name="rem" id="rem" readonly="true" style="text-align: center;" >									
								</th>
							</thead>
							<tbody style="background-color: #F7F7F7">
							<tr>
								<td colspan="3" align="center" style="font-size: 14px;"> Folio
									<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> readonly="true" <?php } ?> size="5%" type="text" name="fol" id="fol" style="text-align: center;">
								</td>
								<td style="background-color: lightgray"><label style="margin: 2px; font-size: 14px; color: yellow" ><strong>Excelente</strong></label>
									
								</td>																										
								<td style="background-color: lightgray"><label style="margin: 2px; font-size: 14px; color: blue">Regular</label>
									
								</td>										
								<td style="background-color: lightgray"><label style="margin: 2px; font-size: 14px; color: red">Malo</label>
									
								</td>
							</tr>
							<tr style="background-color: lightblue"> 
									<th colspan="6" style="font-size: 13px;">Cliente - Empresa</th>
							</tr>
							<tr style="background-color: white"> 
									<td colspan="3" style="font-size: 13px;" align="right">Servicio<input size="2%" type="hidden" name="ser" id="ser" readonly="true" ></td>
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> name="rbSer" type="radio" value="2" onclick="radios(1,2)" /></td>
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> name="rbSer" type="radio" value="1" onclick="radios(1,1)" /></td>									
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> name="rbSer" type="radio" value="-2" onclick="radios(1,-2)" /></td>
							</tr>
							<tr style="background-color: lightblue"> 
									<th colspan="6" style="font-size: 13px;">Cliente - Técnico</th>
							</tr>
							<tr style="background-color: white"> 
									<td colspan="3" style="font-size: 13px;" align="right">Actitud<input size="2%" type="hidden" name="act" id="act" readonly="true" ></td>
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbAct" name="rbAct" type="radio" value="2" onclick="radios(2,2)" /></td>
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbAct" name="rbAct" type="radio" value="1" onclick="radios(2,1)" /></td>									
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbAct" name="rbAct" type="radio" value="-2" onclick="radios(2,-2)" /></td>
							</tr>	
							<tr style="background-color: white"> 
									<td colspan="3" style="font-size: 13px;" align="right">Presentación<input size="2%" type="hidden" name="pre" id="pre" readonly="true" ></td>
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbPre" name="rbPre" type="radio" value="2" onclick="radios(3,2)" /></td>
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbPre" name="rbPre" type="radio" value="1" onclick="radios(3,1)" /></td>									
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbPre" name="rbPre" type="radio" value="-2" onclick="radios(3,-2)" /></td>
							</tr>
							<tr style="background-color: white"> 
									<td colspan="3" style="font-size: 13px;" align="right">Eficiencia<input size="2%" type="hidden" name="efi" id="efi" readonly="true" ></td>
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbEfi" name="rbEfi" type="radio" value="2" onclick="radios(4,2)" /></td>
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbEfi" name="rbEfi" type="radio" value="1" onclick="radios(4,1)" /></td>									
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbEfi" name="rbEfi" type="radio" value="-2" onclick="radios(4,-2)" /></td>
							</tr>
							<tr style="background-color: lightblue"> 
									<th colspan="6" style="font-size: 13px;">Administración - Técnico</th>
							</tr>
							<tr style="background-color: white"> 
									<td colspan="3" style="font-size: 13px;" align="right">Administración de Recurso<input size="2%" type="hidden" name="adm" id="adm" readonly="true" ></td>
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbAdm" name="rbAdm" type="radio" value="2" onclick="radios(5,2)" /></td>
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbAdm" name="rbAdm" type="radio" value="1" onclick="radios(5,1)" /></td>									
									<td align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> disabled="true" <?php } ?> id="rbAdm" name="rbAdm" type="radio" value="-2" onclick="radios(5,-2)" /></td>
							</tr>
							<tr style="background-color: lightblue"> 
									<th colspan="6" style="font-size: 13px;">Comentarios</th>
							</tr>
							<tr style="background-color: white"> 
								<th colspan="6" style="font-size: 13px;"><textarea <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino"){ ?> readonly="true" <?php } ?> name="obs" id="obs" rows="2" cols="90" style="resize: none"></textarea></th>
							</tr>  
							<tr style="background-color: lightblue"> 
									<td colspan="2" style="font-size: 13px;"  >
										<input size="2%" type="hidden" name="verifica" id="verifica" readonly="true" >
										<input size="2%" type="hidden" name="alerta" id="alerta" readonly="true" >
										<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino"){ ?>
										Quiere marcar esta información como alerta: 
										&nbsp;&nbsp;&nbsp;<input name="rbAlerta" type="radio" value="-1" onclick="radios(6,-1)"/><span style="color: red">Critica</span> 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="rbAlerta" type="radio" value="1" onclick="radios(6,1)"/><span style="color: green">Buena</span>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="rbAlerta" type="radio" value="0" onclick="radios(6,0)"/><span>Normal</span>
										</br>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="rbAlerta" type="radio" value="2" onclick="radios(6,2)"/><span style="color: gray">Obs en Equipo: Bombas, Transporte, etc.</span>
										<?php } ?>
										<?php if($usuario=="Daniel Lizárraga"){ ?>
										Procesar Datos Como Verificados: 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="rbVerifica" type="radio" value="-1" onclick="radios(7,-1)"/><span>Si</span>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="rbVerifica" type="radio" value="0" onclick="radios(7,0)"/><span>No</span>
										<?php } ?>
        							</td>
        							<th colspan="4" >
							<center>								
									<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Daniel Lizárraga"){ ?>
									<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
									<?php } ?>
									<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino"){ ?>
									&nbsp;&nbsp;<input style="font-size: 14px" type="submit" id="borrar" name="borrar" value="Borrar" />
									<?php } ?>																	
									<input style="font-size: 14px" type="submit" id="cerrar" name="cerrar" value="Cerrar" />
							</center>
							</th>	
							</tr> 
							</tbody> 		
    				</table>
        		</div>
        </div> 		 	
 	</div> 	
 	<div id="resultados" class="tab_content" style="margin-top: -5px" >
			<div style="height: 350px;">	
			<div id='ver_mas_det' class='hide' style="height: 350px;" >					
				<div class="ajaxSorterDiv" id="tabla_tecres" name="tabla_tecres" style="height: 470px; ">                
               		<div class="ajaxpager" style="margin-top: 0px; text-align: left;"  >        
	                    
	                   	<ul class="order_list" style="width: 500px" >Resultados Actuales de evaluaciones procesadas</ul>  
	                   	
   						<form method="post" action="<?= base_url()?>index.php/entregas/pdfreptcontec" >
                			<input type="hidden" id="mez" name="mez" />
                			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png"  />                                                                      
                        	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                             
 						</form>   
                	</div>  
               		<span class="ajaxTable" style="height: 430px; background-color: white; width: 915px; margin-top: -23px" >
            	       	<table id="mytablaTRes" name="mytablaTRes" class="ajaxSorter" border="1" >
                        	<thead >
                        		<tr>
                        			<th rowspan="3" style="text-align: center" data-order = "NomBio">Encargado</th>
                        			<th rowspan="3" style="text-align: center" data-order = "entregas">Entregas</th>
                        			<th rowspan="2" style="text-align: center" colspan="2">Cliente<br />Servicio</th>
                        			<th colspan="6" style="text-align: center">Cliente-Técnico</th>
                        			<th rowspan="2" style="text-align: center" colspan="2">Administración<br />Técnico</th>
                        			<th rowspan="2" style="text-align: center" colspan="2">General<br />Técnico</th>
                        		</tr>
                        		<tr>
                        			
                        			<th style="text-align: center" colspan="2">Actitud</th>
                        			<th style="text-align: center" colspan="2">Presentación</th>
                        			<th style="text-align: center" colspan="2">Eficiencia</th>
                        			
                        		</tr>
                        		<tr>
                        			<th style="text-align: center" data-order = "ser1">Puntos</th>
                        			<th style="text-align: center" data-order = "ser">%</th>	
                        			<th style="text-align: center" data-order = "act1">Puntos</th>
                        			<th style="text-align: center" data-order = "act">%</th>
                        			<th style="text-align: center" data-order = "pre1">Puntos</th>
                        			<th style="text-align: center" data-order = "pre">%</th>
                        			<th style="text-align: center" data-order = "efi1">Puntos</th>
                        			<th style="text-align: center" data-order = "efi">%</th>
                        			<th style="text-align: center" data-order = "adm1">Puntos</th>
                        			<th style="text-align: center" data-order = "adm">%</th>
                        			<th style="text-align: center" data-order = "gra1">Puntos</th>
                        			<th style="text-align: center" data-order = "gra">%</th>
                        		</tr>
                        	</thead>
                        	<tbody style="font-size: 11px" title="seleccione para ver historial de evaluaciones">
                        	</tbody> 
                        </table>
                    	<sup>Nota: para aclaraciones o dudas esta a disponibilidad la documentación recibida.</sup>
                	</span>
                </div>  
                </div>
                <div id='ver_mas_detres' class='hide' style="height: 250px;" >	
                	<table border="1"  >
                   	<thead>
                     <tr style="background-color: lightblue">
                     	<th rowspan="3" style="text-align: center" >Encargado</th>
                     	<th rowspan="3" style="text-align: center" >Entregas</th>
                     	<th rowspan="2" style="text-align: center" colspan="2">Cliente<br />Servicio</th>
                     	<th colspan="6" style="text-align: center">Cliente-Técnico</th>
                     	<th rowspan="2" style="text-align: center" colspan="2">Administración<br />Técnico</th>
                     	<th rowspan="2" style="text-align: center" colspan="2">General<br />Técnico</th>
                     	<th  style="text-align: center" ><input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(1)" /></th>
                     </tr>
                     <tr style="background-color: lightblue">
                     	<th style="text-align: center" colspan="2">Actitud</th>
                     	<th style="text-align: center" colspan="2">Presentación</th>
                     	<th style="text-align: center" colspan="2">Eficiencia</th>
                     </tr>
                     <tr style="background-color: lightblue">
                     	<th style="text-align: center" >Puntos</th>
                     	<th style="text-align: center" >%</th>	
                        <th style="text-align: center" >Puntos</th>
                        <th style="text-align: center" >%</th>
                        <th style="text-align: center" >Puntos</th>
                        <th style="text-align: center" >%</th>
                        <th style="text-align: center" >Puntos</th>
                        <th style="text-align: center" >%</th>
                        <th style="text-align: center" >Puntos</th>
                        <th style="text-align: center" >%</th>
                        <th style="text-align: center" >Puntos</th>
                        <th style="text-align: center" >%</th>
                      </tr>
                    </thead>
                    <tbody style="font-size: 11px">
                      <tr>
                      	<th><input style="text-align: center; border: none" id="nomrespon" name="nomrespon" /><input type="hidden" id="respon" name="respon" /></th>
                        <th style="text-align: center"><input style="text-align: center; border: none" size="4%" id="ent" name="ent" /></th>
                      	<th><input style="text-align: center; border: none; background-color: lightyellow" size="6%" id="ser1" name="ser1" /></th>
                        <th><input style="text-align: center; border: none; background-color: lightyellow" size="6%" id="serc" name="serc" /></th>	
                        <th><input style="text-align: center; border: none" size="6%" id="act1" name="act1" /></th>
                        <th><input style="text-align: center; border: none" size="6%" id="acta" name="acta" /></th>
                        <th><input style="text-align: center; border: none; background-color: lightyellow" size="6%" id="pre1" name="pre1" /></th>
                        <th><input style="text-align: center; border: none; background-color: lightyellow" size="6%" id="prep" name="prep" /></th>
                        <th><input style="text-align: center; border: none" size="6%" id="efi1" name="efi1" /></th>
                        <th><input style="text-align: center; border: none" size="6%" id="efie" name="efie" /></th>
                        <th><input style="text-align: center; border: none; background-color: lightyellow" size="6%" id="adm1" name="adm1" /></th>
                        <th><input style="text-align: center; border: none; background-color: lightyellow" size="6%" id="adma" name="adma" /></th>
                        <th><input style="text-align: center; border: none" size="6%" id="gra1" name="gra1" /></th>
                        <th><input style="text-align: center; border: none" size="6%" id="grag" name="grag" /></th>
                      </tr>
                    </tbody>
                  </table>                	
                	<div class="ajaxSorterDiv" id="tabla_tecresdet" name="tabla_tecresdet" style="height: 470px; margin-top: -25px ">                
               		<span class="ajaxTable" style="height: 350px; background-color: white; width: 915px; " >
            	       	<table id="mytablaEvaTec" name="mytablaEvaTec" class="ajaxSorter" border="1" >
                        	<thead >
                        		<tr>
                        			<th rowspan="3" style="text-align: center" data-order = "Razon">Cliente</th>
                        			<th rowspan="3" style="text-align: center" data-order = "Folio">Folio</th>
                        			<th rowspan="2" style="text-align: center" colspan="3">Cliente<br />Servicio</th>
                        			<th colspan="9" style="text-align: center">Cliente-Técnico</th>
                        			<th rowspan="2" style="text-align: center" colspan="3">Admón<br />Técnico</th>
                        			<!--<th rowspan="2" style="text-align: center" colspan="3">General<br />Técnico</th>-->
                        			<th rowspan="2" style="text-align: center" >Observaciones
                        			</th>
                        		</tr>
                        		<tr>
                        			
                        			<th style="text-align: center" colspan="3">Actitud</th>
                        			<th style="text-align: center" colspan="3">Presentación</th>
                        			<th style="text-align: center" colspan="3">Eficiencia</th>
                        		</tr>
                        		<tr>
                        			<th style="text-align: center" data-order = "ce">E</th>
                        			<th style="text-align: center" data-order = "cr">R</th>	
                        			<th style="text-align: center" data-order = "cm">M</th>
                        			<th style="text-align: center" data-order = "ae">E</th>
                        			<th style="text-align: center" data-order = "ar">R</th>	
                        			<th style="text-align: center" data-order = "am">M</th>
                        			<th style="text-align: center" data-order = "pe">E</th>
                        			<th style="text-align: center" data-order = "pr">R</th>	
                        			<th style="text-align: center" data-order = "pm">M</th>
                        			<th style="text-align: center" data-order = "ee">E</th>
                        			<th style="text-align: center" data-order = "er">R</th>	
                        			<th style="text-align: center" data-order = "em">M</th>
                        			<th style="text-align: center" data-order = "oe">E</th>
                        			<th style="text-align: center" data-order = "ro">R</th>	
                        			<th style="text-align: center" data-order = "om">M</th>
                        			<th style="text-align: center" data-order = "Obs"></th>
                        		</tr>
                        	</thead>
                        	<tbody style="font-size: 11px">
                        	</tbody> 
                        </table>
                   </span>
                   <div class="ajaxpager" style="margin-top: 0px; text-align: left;"  >        
	                    <ul class="order_list" style="width: 700px" >
	                   		
   				       	</ul>  
   						<form id="data_tablaevatec" action="<?= base_url()?>index.php/entregas/pdfrepeva" method="POST">
                			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" id="export_data" src="<?= base_url()?>assets/img/sorter/pdf.png"  />
                			<input type="hidden" id="tablaevatec" name="tablaevatec" value ="" class="htmlTable"/> 
                			<input type="hidden" name="tablaevatec" id="html_res"/>
                			<input type="hidden" id="inomrespon" name="inomrespon" /> 	<input type="hidden" size="4%" id="ient" name="ient" />
                      		<input type="hidden" size="6%" id="iser1" name="iser1" />  	<input type="hidden" size="6%" id="iserc" name="iserc" />	
                        	<input type="hidden" size="6%" id="iact1" name="iact1" />  	<input type="hidden" size="6%" id="iacta" name="iacta" />
                        	<input type="hidden" size="6%" id="ipre1" name="ipre1" />  	<input type="hidden" size="6%" id="iprep" name="iprep" />
                        	<input type="hidden" size="6%" id="iefi1" name="iefi1" />  	<input type="hidden" size="6%" id="iefie" name="iefie" />
                        	<input type="hidden" size="6%" id="iadm1" name="iadm1" />	<input type="hidden" size="6%" id="iadma" name="iadma" />
                        	<input type="hidden" size="6%" id="igra1" name="igra1" /> 	<input type="hidden" size="6%" id="igrag" name="igrag" />                                                         
 						</form> 
 					</div> 
 				</div> 
                
             	</div>
             	<script type="text/javascript">
					$(document).ready(function(){
						$('#export_data').click(function(){									
							$('#html_res').val($('#mytablaEvaTec').html());
							$('#data_tablaevatec').submit();
							$('#html_res').val('');
						});
					});
				</script> 
            </div>            
 		</div>
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
function cerrar(sel){
	$(this).removeClass('used');
	if(sel==1){
		$('#ver_mas_detres').hide();	
    	$('#ver_mas_det').show();
	}

}	
function radios(radio,dato){
	if(radio==1) { $("#ser").val(dato);}
	if(radio==2) { $("#act").val(dato);}
	if(radio==3) { $("#pre").val(dato);}
	if(radio==4) { $("#efi").val(dato);}
	if(radio==5) { $("#adm").val(dato);}
	if(radio==6) { $("#alerta").val(dato);}
	if(radio==7) { $("#verifica").val(dato);}
}
$("#nuevo").click( function(){	
	$("#ser").val('');
	$("#act").val('');
	$("#pre").val('');
	$("#efi").val('');
	$("#adm").val('');
	$("#alerta").val('');
	$("#verifica").val('');
 return true;
})

$("#cerrar").click( function(){	
	$("#accordion").accordion( "activate",0 );	
 	return true;
})

$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/entregas/borrar", 
						data: "id="+$("#id").val(), //+"&usd="+$("#usd").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Evaluacion Eliminada correctamente");
										$("#nuevo").click();
										$("#mytablaE").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Evaluacion para poder Eliminarla");
		$("#accordion").accordion( "activate",0 );
		return false;
	}
	
	 // return false;
});


function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#aceptar").click( function(){		
	fol=$("#fol").val();
	numero=$("#id").val();
	if( fol!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/entregas/actualizar", 
						data: "id="+$("#id").val()+"&enc="+$("#enc").val()+"&fol="+$("#fol").val()+"&ser="+$("#ser").val()+"&act="+$("#act").val()+"&pre="+$("#pre").val()+"&efi="+$("#efi").val()+"&adm="+$("#adm").val()+"&alerta="+$("#alerta").val()+"&obs="+$("#obs").val()+"&veri="+$("#verifica").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevo").click();
										$("#mytablaE").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/entregas/agregar", 
						data: "enc="+$("#enc").val()+"&fol="+$("#fol").val()+"&ser="+$("#ser").val()+"&act="+$("#act").val()+"&pre="+$("#pre").val()+"&efi="+$("#efi").val()+"&adm="+$("#adm").val()+"&alerta="+$("#alerta").val()+"&obs="+$("#obs").val()+"&nr="+$("#nr").val()+"&nc="+$("#nc").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Evaluacion registrada correctamente");
										$("#nuevo").click();
										$("#mytablaE").trigger("update");
										$("#accordion").accordion( "activate",0 );												
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Folio no valido");	
			$("#fol").focus();
				return false;
		}
});

$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

$("#txtFI").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFF").datepicker( "option", "minDate", selectedDate );
		$("#mytablaE").trigger("update");		
	}	
});
$("#txtFF").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFI").datepicker( "option", "maxDate", selectedDate );
		$("#mytablaE").trigger("update");	
	}	
});	

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  


function noNumbers(e){
		var keynum;
		var keychar;
		var numcheck;
		if(window.event){
			jeynum = e.keyCode;
		}
		else if(e.which){
			heynum = eArray.which;
		}
		keychar = String.formCharccode(keynum);
		numcheck = /\d/;
		return !numcheck.test(keychar);
	}
		
$(document).ready(function(){ 
	//$("#nuevo").click();	
	$(this).removeClass('used');
	$('#ver_mas_detres').hide();
	$("#tabla_ent").ajaxSorter({
		url:'<?php echo base_url()?>index.php/entregas/tabla',  		
		filters:['cmbEstatus','txtFI','txtFF'],
		active:['Verificacion',-1],
    	//multipleFilter:true, 
    	sort:false,              
    	onRowClick:function(){
    		if($(this).data('numclir')>'0'){
    		$("#accordion").accordion( "activate",1 );
    		$("#id").val($(this).data('numsol'));
       		$("#nr").val($(this).data('numregr'));
       		$("#nc").val($(this).data('numclir'));
       		$("#rem").val($(this).data('remisionr'));
			$("#nom").val($(this).data('razon'));
			$("#enc").val($(this).data('numbios'));
			$("#fol").val($(this).data('folio'));
			$("#ser").val($(this).data('ser'));
			$("#act").val($(this).data('act'));
			$("#pre").val($(this).data('pre'));
			$("#efi").val($(this).data('efi'));
			$("#adm").val($(this).data('adm'));
			$("#alerta").val($(this).data('foco'));
			$("#verifica").val($(this).data('verificacion'));
			ser=$(this).data('ser');
			if(ser==2 || ser==1 || ser==-2){
				if(ser==2){ $('input:radio[name=rbSer]:nth(0)').attr('checked',true); }
				if(ser==1){ $('input:radio[name=rbSer]:nth(1)').attr('checked',true); }
				if(ser==-2){ $('input:radio[name=rbSer]:nth(2)').attr('checked',true); }
			}else{
				$('input:radio[name=rbSer]:nth(0)').attr('checked',false);$('input:radio[name=rbSer]:nth(1)').attr('checked',false);
				$('input:radio[name=rbSer]:nth(2)').attr('checked',false); 
			}
			act=$(this).data('act');
			if(act==2 || act==1 || act==-2){
				if(act==2){ $('input:radio[name=rbAct]:nth(0)').attr('checked',true); }
				if(act==1){ $('input:radio[name=rbAct]:nth(1)').attr('checked',true); }
				if(act==-2){ $('input:radio[name=rbAct]:nth(2)').attr('checked',true); }
			}else{
				$('input:radio[name=rbAct]:nth(0)').attr('checked',false);$('input:radio[name=rbAct]:nth(1)').attr('checked',false);
				$('input:radio[name=rbAct]:nth(2)').attr('checked',false);
			}
			pre=$(this).data('pre');
			if(pre==2 || pre==1 || pre==-2){
				if(pre==2){ $('input:radio[name=rbPre]:nth(0)').attr('checked',true); }
				if(pre==1){ $('input:radio[name=rbPre]:nth(1)').attr('checked',true); }
				if(pre==-2){ $('input:radio[name=rbPre]:nth(2)').attr('checked',true); }
			}else{
				$('input:radio[name=rbPre]:nth(0)').attr('checked',false);$('input:radio[name=rbPre]:nth(1)').attr('checked',false);
				$('input:radio[name=rbPre]:nth(2)').attr('checked',false);
			}	
			efi=$(this).data('efi');
			if(efi==2 || efi==1 || efi==-2){
				if(efi==2){ $('input:radio[name=rbEfi]:nth(0)').attr('checked',true); }
				if(efi==1){ $('input:radio[name=rbEfi]:nth(1)').attr('checked',true); }
				if(efi==-2){ $('input:radio[name=rbEfi]:nth(2)').attr('checked',true); }
			}else{
				$('input:radio[name=rbEfi]:nth(0)').attr('checked',false);$('input:radio[name=rbEfi]:nth(1)').attr('checked',false);
				$('input:radio[name=rbEfi]:nth(2)').attr('checked',false);
			}
			adm=$(this).data('adm');
			if(adm==2 || adm==1 || adm==-2){
				if(adm==2){ $('input:radio[name=rbAdm]:nth(0)').attr('checked',true); }
				if(adm==1){ $('input:radio[name=rbAdm]:nth(1)').attr('checked',true); }
				if(adm==-2){ $('input:radio[name=rbAdm]:nth(2)').attr('checked',true); }
			}else{
				$('input:radio[name=rbAdm]:nth(0)').attr('checked',false);$('input:radio[name=rbAdm]:nth(1)').attr('checked',false);
				$('input:radio[name=rbAdm]:nth(2)').attr('checked',false);
			}
			$("#obs").val($(this).data('obs'));
			foc=$(this).data('foco');
			if(foc==-1 || foc==1 || foc==0 || foc==2){
				if(foc==-1){ $('input:radio[name=rbAlerta]:nth(0)').attr('checked',true); }
				if(foc==1){ $('input:radio[name=rbAlerta]:nth(1)').attr('checked',true); }
				if(foc==0){ $('input:radio[name=rbAlerta]:nth(2)').attr('checked',true); }
				if(foc==2){ $('input:radio[name=rbAlerta]:nth(3)').attr('checked',true); }
			}else{
				$('input:radio[name=rbAlerta]:nth(0)').attr('checked',false);$('input:radio[name=rbAlerta]:nth(1)').attr('checked',false);
				$('input:radio[name=rbAlerta]:nth(2)').attr('checked',false);$('input:radio[name=rbAlerta]:nth(3)').attr('checked',false);
			}
			verifica=$(this).data('verificacion');
			if(verifica==-1 || verifica==0){
				if(verifica==-1){ $('input:radio[name=rbVerifica]:nth(0)').attr('checked',true); }
				if(verifica==0){ $('input:radio[name=rbVerifica]:nth(1)').attr('checked',true); }				
			}else{
				$('input:radio[name=rbVerifica]:nth(0)').attr('checked',false);$('input:radio[name=rbVerifica]:nth(1)').attr('checked',false);				
			}
			}
    	}, 
    	onSuccess:function(){
    		//total = 0; 
    		tot=0;   	   	
	    	$('#tabla_ent tbody tr').map(function(){
	    		//total+=($(this).data('total')>0?parseFloat($(this).data('total')):0);
	    		if($(this).data('foco')==-1){ $(this).css('background','#A62A2A');	}
	    		if($(this).data('foco')==2){ $(this).css('background','gray');	}
	    		if($(this).data('fechar')!="Total"){tot+=1;}
    	   		if(tot>0){ $("#tote").val(tot); } else { $("#tote").val('');  }
	    	});
	    	//tr='<tr><td>Totales</td><td colspan=4></td><td>'+(parseFloat(total)).toFixed(3)+'</td></tr>';	    	
	    	//$('#tabla_ent tbody').append(tr);	    	
    	},  
	});
	
	$("#tabla_tecres").ajaxSorter({
		url:'<?php echo base_url()?>index.php/entregas/tablatecres',  		
		//filters:['cmbEnc','cmbRZ'],
		sort:false,  
		onRowClick:function(){
    		if($(this).data('numbio')>'0'){
        		$("#respon").val($(this).data('numbio'));$("#nomrespon").val($(this).data('nombio'));  $("#ent").val($(this).data('entregas'));
        		$("#ser1").val($(this).data('ser1'));$("#serc").val($(this).data('ser'));
        		$("#act1").val($(this).data('act1'));$("#acta").val($(this).data('act'));
        		$("#pre1").val($(this).data('pre1'));$("#prep").val($(this).data('pre'));
        		$("#efi1").val($(this).data('efi1'));$("#efie").val($(this).data('efi'));
        		$("#adm1").val($(this).data('adm1'));$("#adma").val($(this).data('adm'));
        		$("#gra1").val($(this).data('gra1'));$("#grag").val($(this).data('gra'));
        		$("#inomrespon").val($(this).data('nombio'));  $("#ient").val($(this).data('entregas'));
        		$("#iser1").val($(this).data('ser1'));$("#iserc").val($(this).data('ser'));
        		$("#iact1").val($(this).data('act1'));$("#iacta").val($(this).data('act'));
        		$("#ipre1").val($(this).data('pre1'));$("#iprep").val($(this).data('pre'));
        		$("#iefi1").val($(this).data('efi1'));$("#iefie").val($(this).data('efi'));
        		$("#iadm1").val($(this).data('adm1'));$("#iadma").val($(this).data('adm'));
        		$("#igra1").val($(this).data('gra1'));$("#igrag").val($(this).data('gra'));
        		$('#ver_mas_det').hide(); 
        		$('#ver_mas_detres').show();   
        		$("#cmbEncD").val($(this).data('numbio')); 
        		
        		$("#tabla_tecresdet").ajaxSorter({
					url:'<?php echo base_url()?>index.php/entregas/tablatecresdet',  		
					filters:['respon'],
					sort:false,              
    				onSuccess:function(){
    					$('#tabla_tecresdet tbody tr').map(function(){
		    				$(this).css('text-align','center');
		    			})
		    			$('#tabla_tecresdet tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','left'); })
		    			$('#tabla_tecresdet tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center'); })
    					$('#tabla_tecresdet tbody tr td:nth-child(3)').map(function(){ $(this).css('background','lightyellow'); })
    					$('#tabla_tecresdet tbody tr td:nth-child(4)').map(function(){ $(this).css('background','lightyellow'); })
    					$('#tabla_tecresdet tbody tr td:nth-child(5)').map(function(){ $(this).css('background','lightyellow'); })
    					$('#tabla_tecresdet tbody tr td:nth-child(9)').map(function(){ $(this).css('background','lightyellow'); })
    					$('#tabla_tecresdet tbody tr td:nth-child(10)').map(function(){ $(this).css('background','lightyellow'); })
    					$('#tabla_tecresdet tbody tr td:nth-child(11)').map(function(){ $(this).css('background','lightyellow'); })
    					$('#tabla_tecresdet tbody tr td:nth-child(15)').map(function(){ $(this).css('background','lightyellow'); })
    					$('#tabla_tecresdet tbody tr td:nth-child(16)').map(function(){ $(this).css('background','lightyellow'); })
    					$('#tabla_tecresdet tbody tr td:nth-child(17)').map(function(){ $(this).css('background','lightyellow'); })
    					$('#tabla_tecresdet tbody tr td:nth-child(18)').map(function(){ $(this).css('text-align','justify'); })
    	       		},
				}); 		  		
			}		
    	},              
    	onSuccess:function(){
    		$('#tabla_tecres tbody tr').map(function(){
		    	if($(this).data('nombio')=='Total General:'){
		    		$(this).css('background','lightblue');	$(this).css('font-size','12px');$(this).css('font-weight','bold');
		    	}	    		
	    	})	
	    	$('#tabla_tecres tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold'); })
			$('#tabla_tecres tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center'); })
			$('#tabla_tecres tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','center'); })
			$('#tabla_tecres tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
			$('#tabla_tecres tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','center'); })
			$('#tabla_tecres tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
			$('#tabla_tecres tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','center'); })
			$('#tabla_tecres tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right'); })
			$('#tabla_tecres tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','center'); })
			$('#tabla_tecres tbody tr td:nth-child(10)').map(function(){ $(this).css('text-align','right'); })
			$('#tabla_tecres tbody tr td:nth-child(11)').map(function(){ $(this).css('text-align','center'); })
			$('#tabla_tecres tbody tr td:nth-child(12)').map(function(){ $(this).css('text-align','right'); })
			$('#tabla_tecres tbody tr td:nth-child(13)').map(function(){ $(this).css('text-align','center'); })
			$('#tabla_tecres tbody tr td:nth-child(14)').map(function(){ $(this).css('text-align','right'); $(this).css('font-weight','bold');})
		},
	});
		
});  	
</script>