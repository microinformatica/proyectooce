<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#raceways"><img src="<?php echo base_url();?>assets/images/menu/10.png" width="25" height="25" border="0"> Raceways </a></li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="raceways" class="tab_content" style="height: 452px" >
			<div align="left">
				<table class="tablaSorter" border="0px" style="margin-top: -10px; margin-left: -7px;">
					<thead>
							<th colspan="7"  >
								<?php $hoy=date("Y-m-d"); ?>
								<input type="hidden" style="border: none" size="12%" name="hoy" id="hoy" value="<?php echo set_value('hoy',$hoy); ?>">
								<input type="hidden" size="12%" name="ini1" id="ini1" value="1"><input type="hidden" size="12%" name="fin1" id="fin1" value="20">
								<input type="hidden" size="12%" name="ini2" id="ini2" value="21"><input type="hidden" size="12%" name="fin2" id="fin2" value="40">
								<input type="hidden" size="12%" name="ini3" id="ini3" value="41"><input type="hidden" size="12%" name="fin3" id="fin3" value="60">
								<input type="hidden" size="12%" name="ini4" id="ini4" value="61"><input type="hidden" size="12%" name="fin4" id="fin4" value="78">
								<input type="hidden" size="12%" name="ini5" id="ini5" value="87"><input type="hidden" size="12%" name="fin5" id="fin5" value="90">
								<input type="hidden" size="2%" name="tablasel" id="tablasel">
							</th>
							<!--<tr>
								<th colspan="2">Exteriores I</th>
								<th >Exteriores II</th>
								<th >Genética</th>
								<th >Reservas</th>
								<th >Agrupaciones</th>
								<th  ></th>
								
							</tr>-->
						
					</thead>
					<tbody>
						<tr >
							<th style="width: 350px"  >
								Existencia de Postlarvas al día:
								<input style="border: none" size="12%" name="hoy1" id="hoy1" value="<?php echo set_value('hoy1',date("d-m-Y",strtotime($hoy))); ?>">
								<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" ){ ?>
									<input style="font-size: 12px" type="submit" id="his" name="his" value="Historial" />
								<?php }?>
								
								<div class="ajaxSorterDiv" id="tabla_ras7" name="tabla_ras7" style="margin-top: -10px; " >                
            						<span class="ajaxTable" style="height: 177px; background-color: white; width: 350px;" >
                   						<table id="mytablaR7" name="mytablaR7" class="ajaxSorter" >
                       						<thead style="font-size: 20px">                            
                           						<th data-order = "PL1">Generales en Raceways</th>	<th data-order = "totpl" >Cantidad</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 20px">
                       						</tbody>                        	
                   						</table>
            						</span>           
            					</div>
            				
								<div id='ver_mas_pila' class='hide' >
								<table class="ajaxSorter" border="1px" >
            						<thead >
            							<th style="font-size: 18px; text-align: center" rowspan="2">Pila</th>
            							<th style="font-size: 18px; text-align: center" colspan="2">Datos de Siembra</th>
            							<th style="font-size: 18px; text-align: center" rowspan="2">Cantidad <br>Actual</th>
            							<tr>
            								<th style="font-size: 18px; text-align: center">Fecha</th>	<th style="font-size: 18px; text-align: center">Pl</th>	
            							</tr>
            						</thead>
            						<tbody>
            							<th style="font-size: 18px; text-align: center"><input readonly="true" size="6%" type="text" name="pila" id="pila" style="text-align: center; border: none; color: red;" ></th>
            							<th style="font-size: 16px; text-align: center"><input size="13%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="text-align: center; border: none;" ></th>
            							<th style="font-size: 16px; text-align: center"><input size="2%" type="text" name="pl" id="pl" style="text-align: center; border: none;" ></th>
            							<th style="font-size: 16px; text-align: center"><input size="3%" type="text" name="can" id="can" style="text-align: center; border: none;" ></th>
            						</tbody>
            						<tfoot>
            							<th style="font-size: 18px" colspan="4">Pila Actualmente Vacía?
            								&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Enrique Sánchez"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="-1" onclick="radios(1,-1)" />Si</td>
											&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Enrique Sánchez"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="0" onclick="radios(1,0)" />No</td>
            							</th>
            							<tr>
            								<th colspan="4" style="text-align: right">
            									<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Enrique Sánchez"){ ?>
													<input type="hidden" size="4%"  name="id" id="id"/>
													<input style="font-size: 18px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
												<?php }?>
												<input style="font-size: 18px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(6)" />
												<input type="hidden" size="3%"  name="cancela" id="cancela" />
            								</th>
            							</tr>
            						</tfoot>
            					</table>
            					</div>
            					
            					
							</th>
							<th rowspan="2">
								<div class="ajaxSorterDiv" id="tabla_ras1" name="tabla_ras1" style="margin-top: -5px" >                
            						<span class="ajaxTable" style="height: 461px; background-color: white;" >
                   						<table id="mytablaR1" name="mytablaR1" class="ajaxSorter" style="width: 100px;" >
                       						<thead style="font-size: 8px;">                            
                           						<th data-order = "Nombre" >Ext</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px;" >
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th rowspan="2">
								<div class="ajaxSorterDiv" id="tabla_ras2" name="tabla_ras2" style="margin-top: -5px">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 100px; " >
                   						<table id="mytablaR2" name="mytablaR2" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" >Ext</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span> 
            					</div>
							</th>
							<th rowspan="2">
								<div class="ajaxSorterDiv" id="tabla_ras3" name="tabla_ras3" style="margin-top: -5px">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 100px; " >
                   						<table id="mytablaR3" name="mytablaR3" class="ajaxSorter" >
                       						<thead style="font-size: 9px; ">                            
                           						<th data-order = "Nombre" >Ext</th>	<th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>                                      
            					</div>
							</th>
							<th rowspan="2">
								<div class="ajaxSorterDiv" id="tabla_ras4" name="tabla_ras4" style="margin-top: -5px">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 100px; " >
                   						<table id="mytablaR4" name="mytablaR4" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" >Gen</th>	<th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>                                      
            					</div>
							</th>
							<th rowspan="2">
								<div class="ajaxSorterDiv" id="tabla_ras5" name="tabla_ras5" style="margin-top: -5px">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 100px; " >
                   						<table id="mytablaR5" name="mytablaR5" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" >Res</th>	<th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span> 
            					</div>
            				</th>
							<th rowspan="2" >
								<div class="ajaxSorterDiv" id="tabla_ras6" name="tabla_ras6" style="margin-top: -5px" >                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 80px;" >
                   						<table id="mytablaR6" name="mytablaR6" class="ajaxSorter"  >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "PL1">Est</th>	<th data-order = "totpl" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>           
            					</div>
							</th>
							
							
							
						</tr>
					</tbody>
				</table>
			</div>
        </div>
			
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
function cerrar(sel){
	$(this).removeClass('used');
	if(sel==5)	$('#ver_mas_larvi').hide(); else $('#ver_mas_pila').hide(); 	
    
}
$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

function radios(radio,dato){
	if(radio==1) { $("#cancela").val(dato);}
}
$("#his").click( function(){	
 if(confirm('Esta Seguro de Realizar el proceso de historial para el día de Hoy?')){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/raceways/historial", 
			success: 	
					function(msg){															
						if(msg!=0){
							if(msg==1){alert("ya habias actualizado... hasta mañana podras de nuevo hacerlo");}		
							if(msg==2){alert("Historial Actualizado");}		
						}else{
							alert("Error con la base de datos o usted no ha actualizado nada");
						}					
					}			
	});
 }		 
});
$("#aceptar").click( function(){	
	sel=$("#tablasel").val();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/raceways/actualizar", 
			data: "id="+$("#id").val()+"&fec="+$("#txtFecha").val()+"&pl="+$("#pl").val()+"&can="+$("#can").val()+"&vacio="+$("#cancela").val(),
			success: 	
					function(msg){															
						if(msg!=0){			
							if(sel==1){$("#mytablaR1").trigger("update");}											
							if(sel==2){$("#mytablaR2").trigger("update");}
							if(sel==3){$("#mytablaR3").trigger("update");}
							if(sel==4){$("#mytablaR4").trigger("update");}
							if(sel==5){$("#mytablaR5").trigger("update");}
							$("#mytablaR6").trigger("update");
							$("#mytablaR7").trigger("update");
							$(this).removeClass('used');
							$('#ver_mas_pila').hide();																
						}else{
							alert("Error con la base de datos o usted no ha actualizado nada");
						}					
					}	
	});	 
});

$(document).ready(function(){ 
	$('#ver_mas_pila').hide();
	$("#tabla_ras1").ajaxSorter({
		url:'<?php echo base_url()?>index.php/raceways/tablaras',
		filters:['hoy','ini1','fin1'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(1);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_ras2").ajaxSorter({
		url:'<?php echo base_url()?>index.php/raceways/tablaras',
		filters:['hoy','ini2','fin2'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(2);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_ras3").ajaxSorter({
		url:'<?php echo base_url()?>index.php/raceways/tablaras',
		filters:['hoy','ini3','fin3'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(3);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_ras4").ajaxSorter({
		url:'<?php echo base_url()?>index.php/raceways/tablaras',
		filters:['hoy','ini4','fin4'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(4);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},	
	});
	$("#tabla_ras5").ajaxSorter({
		url:'<?php echo base_url()?>index.php/raceways/tablaras',
		filters:['hoy','ini5','fin5'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(5);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_ras6").ajaxSorter({
		url:'<?php echo base_url()?>index.php/raceways/tablarasA',
		filters:['hoy'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').hide();
        },
		onSuccess:function(){    
    		$('#tabla_ras6 tbody tr').map(function(){
	    		if($(this).data('pl1')=='Tot:'){	    				    			
	    			$(this).css('background','lightblue');
	    		}
	    	});	
    	}, 	
	});
	$("#tabla_ras7").ajaxSorter({
		url:'<?php echo base_url()?>index.php/raceways/tablarasAT',
		filters:['hoy'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').hide();
        },
		onSuccess:function(){    
    		$('#tabla_ras7 tbody tr').map(function(){
	    		if($(this).data('pl1')=='Total:'){	    				    			
	    			$(this).css('background','lightblue');
	    		}
	    	});	
    	}, 
	});
});
</script>
