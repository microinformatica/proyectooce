<?php $this->load->view('header_cv'); ?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
/*ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
} */  
#export_datag { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datav { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#ventas" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/salida.jpg" width="20" height="20" border="0"> Ventas </a></li>
		<li><a href="#ventasg" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/salida.png" width="20" height="20" border="0"> General </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 650px"  >
		<div id="ventas" class="tab_content" style="margin-top: 1px">
			<div class="ajaxSorterDiv" id="tabla_vta" name="tabla_vta" style="margin-top: -15px">   
             	  <div class="ajaxpager" > 
            		<ul class="order_list" style="width: 800px; text-align: left; " >
            			Ventas de Camaron Fresco efectuadas -
            			Ciclo
            			<select name="cicloG" id="cicloG" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;"> 	<option value="22" >2022</option>						
   							<option value="21" >2021</option><option value="20" >2020</option>
   							<option value="19" >2019</option><option value="18" >2018</option>
   						</select>
            			Granja
            			<select name="numgrabgb" id="numgrabgb" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   							<option value="0" >Seleccione</option>
   							<option value="4" >OA-Ahome</option><option value="10" >OA-Topolobampo</option>
   							<option value="6">OA- Huatabampo</option>
   							<option value="5" >Oce. Azul</option><option value="2" >OA-Kino</option>
   						</select>  
   						<button id="export_datav" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
						<form id="data_tableg" action="<?= base_url()?>index.php/ventas/pdfrepcosb" method="POST">
							<input type="hidden" name="tabla" id="html_borg"/> <input type="hidden" name="gra" id="gra"/>
						</form>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#export_datav').click(function(){									
									$('#html_borg').val($('#mytablaVta').html());
									$('#data_tableg').submit();
									$('#html_borg').val('');
								});
							});
						</script>          			
            		</ul>      
                   	</ul>
 				</div>                 			
				<span class="ajaxTable" style="height: 530px;" >
                	<table id="mytablaVta" name="mytablaVta" class="ajaxSorter" border="1" style="text-align: center" >
                       	<thead >
                       		<tr style="font-size: 12px">
                       			<th data-order = "feccos1">Dia</th>
                       			<th data-order = "folio1">Folio</th>
                       			<th data-order = "Razon">Granja - Cliente</th>
                       			<th data-order = "grs">PP</th>
                       			<th data-order = "kgs">Kgs</th>
                       			<th data-order = "factura">Factura</th>
                       			<th data-order = "aviso">Aviso</th>
                       			<th data-order = "imp">Importe</th>
                       			<th data-order = "totk">Acum. Kgs.</th>
                       			<th data-order = "toti">Acum. Imp.</th>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 12px; text-align: center">
                       	</tbody>                        	
                   	</table>
                </span> 
            </div>
            <table width="780px" class="ajaxSorter"  border="1px">
					<tr>
						<td><input type="hidden" id="idcos" name="idcos" />Cliente:</td>
						<td>Fecha</td><td>Folio</td><td>PP</td><td>Kgs</td><td>Factura</td><td>Aviso</td>
						<center>
						<td rowspan="2">
							<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez" ){ ?>
							<input style="font-size: 18px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
							<?php }?>
						</center>
						</td>
					</tr>
					<tr>
						<td style="font-size: 14px; background-color: white"><input type="text" name="cli" id="cli" style="width: 380px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="dia" id="dia" style="width: 80px; border: none;" readonly="true" ></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="folio" id="folio" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="grs" id="grs" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="kgs" id="kgs" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="factura" id="factura" style="width: 140px;"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="aviso" id="aviso" style="width: 70px" /></td>
					</tr>    			
    		</table> 			
		</div>
		
	 	<div id="ventasg" class="tab_content" style="margin-top: 1px">
			<div class="ajaxSorterDiv" id="tabla_vtag" name="tabla_vtag" style="width: 920px"  >
				<div class="ajaxpager" style="margin-top: 1px;" > 
            		<ul class="order_list" style="width: 650px; text-align: left" >
            			Ventas a Clientes de acuerdo a su salida de inventarios en Granja 
            			<select id="cmbGra" style="font-size:13px; margin-top: 1px" >
                   			<option value="0">Todas</option>
                   			<option value="4">OA- Ahome</option><option value="10" >OA- Topolobampo</option>
                   			<option value="6">OA- Huatabampo</option>
                   			<option value="2">OA- Kino</option>
          				</select> 
          				<button id="export_datag" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
						<form id="data_tablegr" action="<?= base_url()?>index.php/ventas/pdfrepvtasg" method="POST">
							<input type="hidden" name="tabla" id="html_vtag"/> <input type="hidden" name="gra1" id="gra1"/>
						</form>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#export_datag').click(function(){									
									$('#html_vtag').val($('#mytablaVtag').html());
									$('#data_tablegr').submit();
									$('#html_vtag').val('');
								});
							});
						</script>       
                   	</ul>
 				</div>                 			
				<span class="ajaxTable" style="height:560px;" >
                	<table id="mytablaVtag" name="mytablaVtag" class="ajaxSorter" border="1" style="width:910px" >
                		<thead >     
                   			<th data-order = "nomg" >Granja</th>
                           	<th data-order = "Razon" >Cliente</th>
                           	<th data-order = "kgsvta" >Kgs</th> 
                           	<th data-order = "impvta" >Venta</th> 
                           	<th data-order = "depvta" >Depósitos</th> 
                           	<th data-order = "salvta" >Saldo</th>
                    	</thead>
                    	<tbody title="Seleccione para realizar cambios" style="font-size: 12px;">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
         
          </div>   			
		
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#numgrabgb").change( function(){
	$("#numgrab").val($("#numgrabgb").val());$("#gra").val($("#numgrabgb").val());	
});		

$("#cmbGra").change( function(){
	$("#gra1").val($("#cmbGra").val());	
});		

$("#aceptar").click( function(){	
	numero=$("#folio").val();
	if(numero!=''){
		//if(pre!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/ventas/actualizar", 
						data: "id="+$("#folio").val()+"&fac="+$("#factura").val()+"&avi="+$("#aviso").val()+"&cic="+$("#cicloG").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaVta").trigger("update");
										$("#idcos").val('');$("#cli").val('');$("#feccos").val('');$("#folio").val('');
										$("#grs").val('');$("#kgs").val('');$("#factura").val('');$("#aviso").val('');																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
		/*}else{
			alert("Error: Registre Precio de Venta");	
			$("#prekgs").focus();
			return false;
		}	*/		
		}else{
			alert("Error: Seleccione Venta para poder actualizar");	
			return false;
		}
});


$(document).ready(function(){
	$("#tabla_vta").ajaxSorter({
		url:'<?php echo base_url()?>index.php/ventas/tablavtas', 
		filters:['cicloG','numgrabgb'],
		sort:false,	
		onRowClick:function(){
			$("#idcos").val($(this).data('idcos'));
			$("#feccos").val($(this).data('feccos'));
			$("#dia").val($(this).data('feccos'));
			$("#folio").val($(this).data('folio1'));$("#fol").val($(this).data('folio'));
			$("#cli").val($(this).data('razon'));
			$("#grs").val($(this).data('grs'));
			$("#kgs").val($(this).data('kgs'));
			$("#factura").val($(this).data('factura'));
			$("#aviso").val($(this).data('aviso'));
			$("#gra").val($(this).data('numgrab'));$("#numgrab").val($(this).data('numgrab'));
			$("#clicos").val($(this).data('clicos'));
			//$("#mytablagralb").trigger("update");
       	}, 
		onSuccess:function(){ 
			$('#tabla_gral tbody tr').map(function(){
		    	if($(this).data('pisg')=='Total') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    })		
		},	
	});
	
	
	$("#tabla_vtag").ajaxSorter({
		url:'<?php echo base_url()?>index.php/ventas/tablag',  
		filters:['cicloG','cmbGra'],
		//active:['salvta1','0','<='],  
        sort:false,  
        onSuccess:function(){ 
			$('#tabla_vtag tbody tr').map(function(){
		    	if($(this).data('nomg')=='Gran Total') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		}
	    		if($(this).data('razon')=='Total Granja') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		}
		    });
		    $('#tabla_vtag tbody tr td:nth-child(1)').map(function(){ 
	    		if( $(this).html() != '' ) {
              			$(this).css('background','lightblue');$(this).css('font-weight','bold');
        			}
	    	});
	    	$('#tabla_vtag tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right');});
	    	$('#tabla_vtag tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');});
	    	$('#tabla_vtag tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');});
	    	$('#tabla_vtag tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right');});  		
		},       
	});
	
}); 



</script>
