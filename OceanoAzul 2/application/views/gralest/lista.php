<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts-more.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/export-data.js"></script>



<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
	
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
	&::a{
		z-index: -1;
	}
}

#export_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datag { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#containers {
    	max-width: 900px;
    	min-width: 380px;
    	height: 380px;
    	margin: 1em auto;
	}
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#concentrado" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/grafest.png" width="20" height="20" border="0"> General </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 630px" >
		<div id="concentrado" class="tab_content" style="margin-top: -15px" >
 			<div class="ajaxSorterDiv" id="tabla_gral" name="tabla_gral" style="margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: -5px;" > 
            		<ul class="order_list" style="width: 860px; text-align: left; " >
            			Reporte General 
            			- Granja:	
            			<select name="gracg" id="gracg" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    						<option value="4">Ahome</option>		
    					</select>
            			- Ciclo:
            			<select name="ciccosg" id="ciccosg" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:5px;">
      						<?php $ciclof=20; $actual=date("y"); //$actual+=1;
								while($actual >= $ciclof){?>
									<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
									<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            				<?php  $actual-=1; } ?>      									
    					</select>
    					- Sección:	
            			<select name="seccg" id="seccg" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    						<option value="0">Todas</option>
    						<option value="41">20-30</option>		
    						<option value="42">Doble Ciclo</option>
    					</select>
    					
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 570px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablagral" name="mytablagral" class="ajaxSorter" border="1" style="text-align: center;" >
                       	<thead >
                       		
                       		<tr style="font-size: 12px">
                       			<th data-order = "sem">Sem</th>
                       			<?php	
          						$cont=0;
								while($cont<77){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "e".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
                       		</tr>
                       		
                       	</thead>
                       	<tbody style="font-size: 10px; text-align: center">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<button id="export_datag" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
				<form id="data_tablesg" action="<?= base_url()?>index.php/cosechas/pdfrepcosg" method="POST">
					<input type="hidden" name="tablag" id="html_gral"/>
				</form>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#export_datag').click(function(){									
								$('#html_gral').val($('#mytablagral').html());
								$('#data_tablesg').submit();
								$('#html_gral').val('');
								
						});
					});
				</script>
 			
 		</div>
 	</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">


$("#fecdia").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",		
});

$("#feccos").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",	
	onSelect: function( selectedDate ){
		$("#mytablacosechas").trigger("update");
		$("#nue").click();$("#idcos").val('');$("#tipcos").val('');$("#numcos").val('');$("#clicos").val('Selectos del Mar S.A. de C.V.');
		$("#dia").val($("#feccos").val());
	}
});


jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  



$(document).ready(function(){ 
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	$("#feccos").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#dia").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#tip").val(0);$("#prebase").val(0);
	
	$("#tabla_gral").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gralest/tablagral', 
		filters:['gracg','ciccosg','seccg'],
		sort:false,	
		onSuccess:function(){ 
			//$('#tabla_gral tbody tr').map(function(){
			col=2;	
			while(col<=78){
			$('#tabla_gral tbody tr td:nth-child('+col+')').map(function(){ 	
		    	if($(this).html() != '' && $(this).html() > 0) {
                	$(this).css("background-color", "#0F0"); $(this).css("color", "black");
              	}
            	if($(this).html() != '' && $(this).html() <= 0 && $(this).html() >= -2) {
                	$(this).css("background-color", "orange"); $(this).css("color", "black");
              	}
              	if($(this).html() != '' && $(this).html() < -2 ) {
                	$(this).css("background-color", "red"); $(this).css("color", "black");
              	}
		    });
		    col+=1;		
		   }
		},	
	});
	
});
</script>