<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts/js/modules/exporting.js"></script>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 20px;
	/*background: none;*/		
}   
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino" or $usuario=="Zuleima Benitez" or $usuario=="Joaquin"){ ?>	
		<li style="font-size: 20px"><a href="#vales"><img src="<?php echo base_url();?>assets/images/menu/combustible.png" width="25" height="25" border="0"> Combustible</a></li>
		<?php }else{ ?>
			 <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px;margin-top: 1px; margin-left:4px;" >
                    	<?php $ciclof=13; $actual=date("y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo '20'.$actual;?> </option>
            				<?php  $actual-=1; } ?>
          			</select>
		<?php } ?>
		<li style="font-size: 20px"><a href="#general"><img src="<?php echo base_url();?>assets/images/menu/combustibleg.png" width="25" height="25" border="0"> Laboratorio</a></li>
		<li style="font-size: 20px"><a href="#zonas"><img src="<?php echo base_url();?>assets/images/mexicopais.png" width="25" height="25" border="0"> Zonas </a></li>
		<li style="font-size: 20px"><a href="#adquisiciones"><img src="<?php echo base_url();?>assets/images/menu/adquisiciones.png" width="25" height="25" border="0"> Costos Pipa</a></li>
		<li style="font-size: 20px"><a href="#grafica"><img src="<?php echo base_url();?>assets/images/grafica.png" width="25" height="25" border="0"> Pipa</a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino"  or $usuario=="Zuleima Benitez" or $usuario=="Joaquin"){ ?>
		<div id="vales" class="tab_content"  >	
			<div style="width: 900px">
			<table class="ajaxSorter" border="2" style="width: 850px" >
				<thead>
						<th>Ciclo</th><th>Fecha</th><th>Vale</th><th>Unidad</th><th>Litros</th><th >Observaciones</th>
						
				</thead>
				<tbody>
					<th>                 
                    <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px;margin-top: 1px; margin-left:4px;" >
                    	<?php $ciclof=13; $actual=date("y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo '20'.$actual;?> </option>
            				<?php  $actual-=1; } ?>
          			</select>
          			</th>
						<th style="text-align: center">
							<input type="hidden" readonly="true" size="2%"  type="text" name="idc" id="idc">
							<input size="10%" type="text" name="cfec" id="cfec" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>
						<th ><input size="5%"  type="text" name="cvale" id="cvale"></th>	
						<th style="text-align: center">
							<select name="uni" id="uni" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
           							$this->load->model('combustible_model');
									$data['result']=$this->combustible_model->verUnidad();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumUni;?>" ><?php echo $row->NomUni;?></option>
           							<?php endforeach;?>
   							</select>
   						</th>
						<th style="text-align: center">
							<input name="rbSel" type="radio" value="1" onclick="radios(6,1)" /> Die
							<input name="rbSel" type="radio" value="2" onclick="radios(6,2)"/> Gas
							<input type="hidden" readonly="true" size="2%"  type="text" name="cdg" id="cdg">
							<input style="text-align: right" size="10%"  type="text" name="clts" id="clts" >
							<br />
							Importe: $<input onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right" size="14%"  type="text" name="cimporte" id="cimporte" >
						</th>
						<th ><input size="35%"  type="text" name="cobs" id="cobs"></th>
						
				</tbody>
				<tfoot>
					<th colspan="6" style="text-align: center">
						<input type="submit" id="cnuevo" name="cnuevo" value="Nuevo"  />
							<input type="submit" id="caceptar" name="caceptar" value="Guardar" />
							<input type="submit" id="cborrar" name="cborrar" value="Borrar" />
					</th>
				</tfoot>
			</table>
			</div>
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_com" name="tabla_com" style="width: 860px"  >                 			
							<span class="ajaxTable" style="height: 355px; margin-top: 1px; " >
                    			<table id="mytablaCom" name="mytablaCom" class="ajaxSorter" border="1" style="width: 843px" >
                        			<thead>                            
                            			<!--<th data-order = "cap" >Estatus</th>-->
                            			<th data-order = "cfec" >Fecha</th>
                            			<th data-order = "cvale" >Vale</th>
                            			<th data-order = "NomUni"  >Unidad</th>
                            			<th data-order = "cltsd"   >Lts Diesel</th>
                            			<th data-order = "cltsg"   >Lts Gasolina</th>
                            			<th data-order = "cobs" >Observaciones</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tables" action="<?= base_url()?>index.php/combustible/pdfrepdia" method="POST" >							
    	    	            		<ul class="order_list" style="width: 350px; margin-top: 1px">
    	    	            			Fecha de Movimientos:<!-- <input size="13%" type="text" name="pfecd" id="pfecd" class="fecha redondo mUp" readonly="true" style="text-align: center;" >-->
    	    	            			<input size="10%" type="text" name="txtFI" id="txtFI" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                								&nbsp; al <input size="10%" type="text" name="txtFF" id="txtFF"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
    	    	            			</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tabladps" value ="" class="htmlTable"/> 
								</form>   
                			</div>       	                	
	                	</div>
	                	
            		</th>
            	</tr>
			</table>	
		</div>
		<?php } ?>
		<div id="general" class="tab_content"  >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_comg" name="tabla_comg" style="width: 450px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaComG" name="mytablaComG" class="ajaxSorter" border="1" style="width: 433px" >
                        			<thead>                                                        			
                            			<th data-order = "NomUni" >Unidad</th>
                            			<th data-order = "diesel" >Diesel</th>
                            			<th data-order = "gasolina"  >Gasolina</th>
                            		</thead>
                        			<tbody title="Seleccione para visualizar detalle" style="font-size: 14px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablesg" action="<?= base_url()?>index.php/combustible/pdfrepcomg" method="POST" >							
    	    	            		<ul class="order_list" style="width:380px; margin-top: 1px">
    	    	            			Mes:
	                   					<select name="cmbMesT" id="cmbMesT" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
				   							<option value="0" >Todos</option>
				   							<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   											<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   											<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   											<option value="10" >Octubre</option><option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   										</select>
    	    	            			Consumos actuales según vales: Ciclo- <input size="3%" type="text" name="cic" id="cic" readonly="true" style="border: none"  >    	    	            			
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablacomg" value ="" class="htmlTable"/> 
								</form>   
                			</div>       
	                	
	                	</div>
	                	
            		</th>
            		<th>
            			<div class="ajaxSorterDiv" id="tabla_comgd" name="tabla_comgD" style="width: 440px;  margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 445px;" >
                    			<table id="mytablaComGD" name="mytablaComGD" class="ajaxSorter" border="1" style="width: 423px" >
                        			<thead>                            
                            			<th data-order = "cfec" >Fecha</th>
                            			<th data-order = "cvale" >Folio</th>
                            			<th data-order = "diesel" >Diesel</th>
                            			<th data-order = "gasolina"  >Gasolina</th>
                            			<th data-order = "cimporte"  >Subtotal</th>
                            			<th data-order = "cobs"  >Observaciones</th>
                            		</thead>
                        			<tbody style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablesgd" action="<?= base_url()?>index.php/combustible/pdfrepcomd" method="POST" >							
    	    	            		<ul class="order_list" style="width: 250px; margin-top: 1px">Unidad: <input size="20%" type="text" name="unidad" id="unidad" readonly="true" style="border: none"  >
            							<input type="hidden" name="idcu" id="idcu"   >    	    	            			
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablacomd" value ="" class="htmlTable"/>  
								</form>   
                			</div>       
	                	
	                	</div>
            		</th>
            		</tr>
            	
			</table>	
		</div>
		<div id="zonas" class="tab_content" style="height: 485px;" >
			<!--<div align="left" style="margin-left: 25px">Concentrado de Depósitos</div>-->
			<table width="898px" style="margin-top: -10px">
			<tr>
				<th rowspan="2">
				<div class="ajaxSorterDiv" id="tabla_zon" name="tabla_zon" style="margin-top: -10px; width: 435px" >
					<span class="ajaxTable" style="height: 502px; width: 433px" " >
                  		<table id="mytablaZ" name="mytablaZ" class="ajaxSorter" >
                        		<thead style="font-size: 10px" >                            
                            		<th style="width: 30px" data-order = "zon"  >Zona</th>
                            		<th style="width: 20px" data-order = "por"  >% Pl´s</th>                                                        							                                                                                                              
                            		<th style="width: 30px" data-order = "a"  >2016 Pl´s</th>
                            		<th style="width: 35px" data-order = "ac"  >Combustible</th>
                            		<th style="width: 30px" data-order = "b"  >2015 Pl´s</th>        
                            		<th style="width: 35px" data-order = "bc"  >Combustible</th>                    	
                           		</thead>
                        		<tbody style="font-size: 11px">
                        		</tbody>                        	
                    	</table>
                	</span>
           		</div>
           		</th>
           		<th>
           			Zona:<input style="border: none" name="zona" id="zona" /> 
           		</th>
           </tr>
           <tr>
           		<th>
           			<div class="ajaxSorterDiv" id="tabla_cd" name="tabla_cd" style="margin-top: -10px;width: 455px" >
					<span class="ajaxTable" style="height: 238px; width: 453px" >
                  		<table id="mytablaCD" name="mytablaCD" class="ajaxSorter" >
                        		<thead style="font-size: 11px" >                            
                            		<th data-order = "Razon"  >Razon</th>
                            		<th data-order = "RemisionR"  >Rem</th>                   							                                                                                                              
                            		<th data-order = "NomUni"  >Veh</th>
                            		<th data-order = "Com"  >Importe</th>
                            	</thead>
                        		<tbody style="font-size: 11px">
                        		</tbody>                        	
                    	</table>
                	</span>
           		</div>
           		<div class="ajaxSorterDiv" id="tabla_cd1" name="tabla_cd1" style="margin-top: -10px;width: 455px" >
					<span class="ajaxTable" style="height: 238px; width: 453px" >
                  		<table id="mytablaCD1" name="mytablaCD1" class="ajaxSorter" >
                        		<thead style="font-size: 11px" >                            
                            		<th data-order = "Razon"  >Razon</th>
                            		<th data-order = "RemisionR"  >Rem</th>                   							                                                                                                              
                            		<th data-order = "NomUni"  >Veh</th>
                            		<th data-order = "Com"  >Importe</th>
                            	</thead>
                        		<tbody style="font-size: 11px">
                        		</tbody>                        	
                    	</table>
                	</span>
           		</div>
           		</th>
           </tr>
           
           </table>	
	 	</div>
		<div id="adquisiciones" class="tab_content"  >	
			Registro de compras mensuales de combustibles Diesel y Gasolina
			<div class="ajaxSorterDiv" id="tabla_vta" name="tabla_vta" style="height: 270px; ">                
                		
                		<span class="ajaxTable" style="height: 200px; background-color: white; width: 870px; margin-top: 1px" >
                    	<table id="mytablaV" name="mytablaV" class="ajaxSorter" >
                        	<thead title="Presione las columnas para ordenarlas como requiera">
                        		<th data-order = "cic">Ciclo</th><th data-order = "ene">Ene</th>
                        		<th data-order = "feb">Feb</th><th data-order = "mar">Mar</th><th data-order = "abr">Abr</th>
                        		<th data-order = "may">May</th><th data-order = "jun">Jun</th><th data-order = "jul">Jul</th>
                        		<th data-order = "ago">Ago</th><th data-order = "sep">Sep</th><th data-order = "oct">Oct</th>
                        		<th data-order = "nov">Nov</th><th data-order = "dic">Dic</th>
                        		<th data-order = "tot">Total</th>
                        	</thead>
                        	<tbody title="Seleccione para analizar comparativo" style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="margin-top: -10px">        
	                    	<ul class="order_list"></ul>     
                   			 <form method="post" action="<?= base_url()?>index.php/combustible/pdfrepCommes" >  
                    			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png"  />
                    			<!--<img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />-->                                                                      
                        		<!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        		<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                             
 							</form>   
                		</div>                                      
            			</div>             	
            			<div align="left" style="font-size: 14px; margin-top: -15px">
			 				<input size="3%" name="ciclosel" id="ciclosel" style="border: none"/> vs &nbsp; <input size="3%" name="ciclosel2" id="ciclosel2" style="border: none"/>
			 				Comparativos Anuales actualizados al mes en curso.
			 			</div>
			 			<div class="ajaxSorterDiv" id="tabla_vtac" name="tabla_vtac" style="height: 162px" >                
                  		<span class="ajaxTable" style="height: 120px; background-color: white; width: 870px" >
                    	<table id="mytablaVC" name="mytablaVC" class="ajaxSorter" >
                        	<thead title="Presione las columnas para ordenarlas como requiera">
                        		<th data-order = "cic">Ciclo</th><th data-order = "ene">Ene</th>                                                        
                            	<th data-order = "feb">Feb</th><th data-order = "mar">Mar</th><th data-order = "abr">Abr</th>
                            	<th data-order = "may">May</th><th data-order = "jun">Jun</th><th data-order = "jul">Jul</th>
                            	<th data-order = "ago">Ago</th><th data-order = "sep">Sep</th><th data-order = "oct">Oct</th>
                            	<th data-order = "nov">Nov</th><th data-order = "dic">Dic</th>
                            	<th data-order = "tot">Total</th>
                        	</thead>
                        	<tbody title="" style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="margin-top: -10px">        
	                    	<ul class="order_list"></ul>     
                   			 <form method="post" action="<?= base_url()?>index.php/combustible/pdfrepCompa" >  
                    			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />                                                                      
                        		<!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        		<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                             
 							</form>   
                		</div>                                  
            			</div>	
		</div>
		<div id="grafica" class="tab_content"  >
			<div id="containere" style="min-width: 940px; height: 462px; margin: 0 auto" ></div>
			<input type="hidden" id="a1"/><input type="hidden" id="a2"/><input type="hidden" id="a3"/><input type="hidden" id="a4"/><input type="hidden" id="a5"/><input type="hidden" id="a6"/>
			<input type="hidden" id="a7"/><input type="hidden" id="a8"/><input type="hidden" id="a9"/><input type="hidden" id="a10"/><input type="hidden" id="a11"/><input type="hidden" id="a12"/>
			<input type="hidden" id="b1"/><input type="hidden" id="b2"/><input type="hidden" id="b3"/><input type="hidden" id="b4"/><input type="hidden" id="b5"/><input type="hidden" id="b6"/>
			<input type="hidden" id="b7"/><input type="hidden" id="b8"/><input type="hidden" id="b9"/><input type="hidden" id="b10"/><input type="hidden" id="b11"/><input type="hidden" id="b12"/>
			<input type="hidden" id="c1"/><input type="hidden" id="c2"/><input type="hidden" id="c3"/><input type="hidden" id="c4"/><input type="hidden" id="c5"/>
			<input type="hidden" id="c6"/><input type="hidden" id="c7"/><input type="hidden" id="c8"/><input type="hidden" id="c9"/><input type="hidden" id="c10"/><input type="hidden" id="c11"/>
			<input type="hidden" id="d1"/><input type="hidden" id="d2"/><input type="hidden" id="d3"/><input type="hidden" id="d4"/><input type="hidden" id="d5"/>
			<input type="hidden" id="d6"/><input type="hidden" id="d7"/><input type="hidden" id="d8"/><input type="hidden" id="d9"/><input type="hidden" id="d10"/><input type="hidden" id="d11"/>
			<input type="hidden" id="e1"/><input type="hidden" id="e2"/><input type="hidden" id="e3"/><input type="hidden" id="e4"/><input type="hidden" id="e5"/>
			<input type="hidden" id="e6"/><input type="hidden" id="e7"/><input type="hidden" id="e8"/><input type="hidden" id="e9"/><input type="hidden" id="e10"/><input type="hidden" id="e11"/>
			<input type="hidden" id="f1"/><input type="hidden" id="f2"/><input type="hidden" id="f3"/><input type="hidden" id="f4"/><input type="hidden" id="f5"/>
			<input type="hidden" id="f6"/><input type="hidden" id="f7"/><input type="hidden" id="f8"/><input type="hidden" id="f9"/><input type="hidden" id="f10"/><input type="hidden" id="f11"/>
		</div>		
				
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
//busca datos de pila
$("#uni").change( function(){	
 	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/combustible/buscar", 
			data: "uni="+$("#uni").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					cdg1=obj.dg;
					$("#cdg").val(obj.dg);
					if(cdg1==1 || cdg1==2){
						if(cdg1==1){ $('input:radio[name=rbSel]:nth(0)').attr('checked',true); }
						if(cdg1==2){ $('input:radio[name=rbSel]:nth(1)').attr('checked',true); }								
					}else{
						$('input:radio[name=rbSel]:nth(0)').attr('checked',false);$('input:radio[name=rbSel]:nth(1)').attr('checked',false);				
					}					
   			} 
	});			 
});

function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}

$("#cmbCiclo").change( function(){	
	$("#cic").val('20'+$("#cmbCiclo").val());
	return true;
});

$("#txtFI").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFF").datepicker( "option", "minDate", selectedDate );
		$("#mytablaCom").trigger("update");		
	}	
});
$("#txtFF").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFI").datepicker( "option", "maxDate", selectedDate );
		$("#mytablaCom").trigger("update");	
	}	
});	
$("#cfec").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});
function radios(radio,dato){
	if(radio==6) { $("#cdg").val(dato); $("#clts").focus();}
}
$("#cnuevo").click( function(){	
	$("#idc").val('');$("#cimporte").val('');
	$("#cvale").val('');	$("#clts").val('');	$("#cobs").val('');$("#cvale").focus();
 return true;
})
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#cborrar").click( function(){	
	id=$("#idc").val();
	if(id!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/combustible/cborrar", 
				data: "id="+$("#idc").val()+"&ciclo="+$("#cmbCiclo").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								alert("Datos Eliminados correctamente");
								$("#mytablaCom").trigger("update");
								$("#mytablaComG").trigger("update");
								$("#mytablaComGD").trigger("update");
								$("#mytablaV").trigger("update");
								$("#cnuevo").click();					
							}else{
								alert("Error con la base de datos o usted no ha eliminado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder eliminar datos");				
		return false;
	}	 
});

$("#caceptar").click( function(){	
	imp=$("#clts").val();
	id=$("#idc").val();
	numero=$("#cvale").val();
	if(numero!=''){
	  if(imp!=''){
	  	if(id!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/combustible/cactualizar", 
						data: "idc="+$("#idc").val()+"&cfec="+$("#cfec").val()+"&clts="+$("#clts").val()+"&cvale="+$("#cvale").val()+"&cdg="+$("#cdg").val()+"&cobs="+$("#cobs").val()+"&uide="+$("#uni").val()+"&ciclo="+$("#cmbCiclo").val()+"&imp="+$("#cimporte").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaCom").trigger("update");
										$("#mytablaComG").trigger("update");
										$("#mytablaComGD").trigger("update");
										$("#mytablaV").trigger("update");
										$("#cnuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/combustible/cagregar", 
						data: "cfec="+$("#cfec").val()+"&clts="+$("#clts").val()+"&cvale="+$("#cvale").val()+"&cdg="+$("#cdg").val()+"&cobs="+$("#cobs").val()+"&uide="+$("#uni").val()+"&ciclo="+$("#cmbCiclo").val()+"&imp="+$("#cimporte").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos Registrados correctamente");
										$("#mytablaCom").trigger("update");
										$("#mytablaComG").trigger("update");
										$("#mytablaComGD").trigger("update");
										$("#mytablaV").trigger("update");
										$("#cnuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Registre Cantidad de Litros");	
			$("#clts").focus();
				return false;
		}
	}else{
		alert("Error: Registre Numero de Vale de Combustible");
		$("#cvale").focus();
			return false;
	}

});

$("#cmbFacRem").change( function(){		
	$("#ciclosel").val(2016);ciclo=2016;
 return true;
});
$(document).ready(function(){
	var f = new Date();
	$("#txtFI").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());		
	$("#cic").val('20'+$("#cmbCiclo").val());
	$("#tabla_com").ajaxSorter({
		url:'<?php echo base_url()?>index.php/combustible/tablacom',  
		filters:['cmbCiclo','txtFI','txtFF'],
        sort:false,
        onRowClick:function(){
           	$("#idc").val($(this).data('idc'));
           	$("#cfec").val($(this).data('cfec'));
           	$("#cvale").val($(this).data('cvale'));	
           	$("#clts").val($(this).data('clts'));
           	$("#uni").val($(this).data('uide'));
			$("#cobs").val($(this).data('cobs'));
			$("#cdg").val($(this).data('cdg'));
			$("#cimporte").val($(this).data('cimporte'));
			cdg=$(this).data('cdg');
			if(cdg==1 || cdg==2){
				if(cdg==1){ $('input:radio[name=rbSel]:nth(0)').attr('checked',true); }
				if(cdg==2){ $('input:radio[name=rbSel]:nth(1)').attr('checked',true); }								
			}else{
				$('input:radio[name=rbSel]:nth(0)').attr('checked',false);$('input:radio[name=rbSel]:nth(1)').attr('checked',false);				
			}	
       },       
	});
	$("#tabla_comg").ajaxSorter({
		url:'<?php echo base_url()?>index.php/combustible/tablacomg',  
		filters:['cmbCiclo','cmbMesT'],
        sort:false,
        onRowClick:function(){
           	$("#idcu").val($(this).data('uide'));
           	$("#unidad").val($(this).data('nomuni'));
			$("#tabla_comgd").ajaxSorter({
				url:'<?php echo base_url()?>index.php/combustible/tablacomgd',  
				filters:['cmbCiclo','idcu'],
        		sort:false,
        		onSuccess:function(){
        			$('#tabla_comgd tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right');  })
    				$('#tabla_comgd tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');  })
    				$('#tabla_comgd tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');  })
    			}
			});	
       },
        onSuccess:function(){
        	$('#tabla_comg tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right');  })
    		$('#tabla_comg tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right');  })
    	}
	});
	
	
	$("#ciclosel").val(2016);ciclo=2016;//cambiar cada año
	ini3=ciclo;
	$("#tabla_vta").ajaxSorter({
		url:'<?php echo base_url()?>index.php/combustible/tabla',  	
		sort:false,	
		//filters:['cmbFacRem'],
	onRowClick:function(){
		$("#ciclosel").val($(this).data('cic'));
		ciclo=$(this).data('cic');
		if(ciclo!=2013) {ciclo=ciclo-1;}
		$("#ciclosel2").val(ciclo);
		$("#tabla_vtac").ajaxSorter({
			url:'<?php echo base_url()?>index.php/combustible/tablaVC',
			filters:['ciclosel'],			
			sort:false,
						
		});  
		
    },
    onSuccess:function(){   
    	$('#tabla_vta tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','center');  $(this).css('font-weight','bold');})
	    $('#tabla_vta tbody tr td:nth-child(14)').map(function(){ $(this).css('text-align','right'); $(this).css('font-weight','bold');}) 
    	if(ciclo!=2013) {ciclo=ciclo-1;}
    	$("#ciclosel2").val(ciclo);
		$("#tabla_vtac").ajaxSorter({
			url:'<?php echo base_url()?>index.php/combustible/tablaVC',
			filters:['ciclosel'],			
			//active:['ene',>0],			
			sort:false,	
			onSuccess:function(){
				$('#tabla_vtac tbody tr').map(function(){ $(this).css('text-align','right');})	
				$('#tabla_vtac tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','center');  $(this).css('font-weight','bold');})
	    		$('#tabla_vtac tbody tr td:nth-child(14)').map(function(){ $(this).css('font-weight','bold');})
			},	 		
		}); 
		
			$('#tabla_vta tbody tr').map(function(){
				$(this).css('text-align','right');	
			if($(this).data('cic')==ini3){	
				if($(this).data('ene1')>0){ $("#a1").val($(this).data('ene1')); }else {$("#a1").val(0);}
				if($(this).data('feb1')>0){ $("#a2").val($(this).data('feb1')); }else {$("#a2").val(0);}
				if($(this).data('mar1')>0){ $("#a3").val($(this).data('mar1')); }else {$("#a3").val(0);}
				if($(this).data('abr1')>0){ $("#a4").val($(this).data('abr1')); }else {$("#a4").val(0);}
				if($(this).data('may1')>0){ $("#a5").val($(this).data('may1')); }else {$("#a5").val(0);}
				if($(this).data('jun1')>0){ $("#a6").val($(this).data('jun1')); }else {$("#a6").val(0);}
				if($(this).data('jul1')>0){ $("#a7").val($(this).data('jul1')); }else {$("#a7").val(0);}
				if($(this).data('ago1')>0){ $("#a8").val($(this).data('ago1')); }else {$("#a8").val(0);}
				if($(this).data('sep1')>0){ $("#a9").val($(this).data('sep1')); }else {$("#a9").val(0);}
				if($(this).data('oct1')>0){ $("#a10").val($(this).data('oct1')); }else {$("#a10").val(0);}
				if($(this).data('nov1')>0){ $("#a11").val($(this).data('nov1')); }else {$("#a11").val(0);}
				if($(this).data('dic1')>0){ $("#a12").val($(this).data('dic1')); }else {$("#a12").val(0);}
			}	
			if($(this).data('cic')==(ini3-1)){	
				if($(this).data('ene1')>0){ $("#b1").val($(this).data('ene1')); }else {$("#b1").val(0);}
				if($(this).data('feb1')>0){ $("#b2").val($(this).data('feb1')); }else {$("#b2").val(0);}
				if($(this).data('mar1')>0){ $("#b3").val($(this).data('mar1')); }else {$("#b3").val(0);}
				if($(this).data('abr1')>0){ $("#b4").val($(this).data('abr1')); }else {$("#b4").val(0);}
				if($(this).data('may1')>0){ $("#b5").val($(this).data('may1')); }else {$("#b5").val(0);}
				if($(this).data('jun1')>0){ $("#b6").val($(this).data('jun1')); }else {$("#b6").val(0);}
				if($(this).data('jul1')>0){ $("#b7").val($(this).data('jul1')); }else {$("#b7").val(0);}
				if($(this).data('ago1')>0){ $("#b8").val($(this).data('ago1')); }else {$("#b8").val(0);}
				if($(this).data('sep1')>0){ $("#b9").val($(this).data('sep1')); }else {$("#b9").val(0);}
				if($(this).data('oct1')>0){ $("#b10").val($(this).data('oct1')); }else {$("#b10").val(0);}
				if($(this).data('nov1')>0){ $("#b11").val($(this).data('nov1')); }else {$("#b11").val(0);}
				if($(this).data('dic1')>0){ $("#b12").val($(this).data('dic1')); }else {$("#b12").val(0);}
			}		
			/*if($(this).data('cic')==(ini3-2)){	
				if($(this).data('dic1')>0){ $("#c1").val($(this).data('dic1')); }else {$("#c1").val(0);} 
				if($(this).data('ene1')>0){ $("#c2").val($(this).data('ene1')); }else {$("#c2").val(0);}
				if($(this).data('feb1')>0){ $("#c3").val($(this).data('feb1')); }else {$("#c3").val(0);}
				if($(this).data('mar1')>0){ $("#c4").val($(this).data('mar1')); }else {$("#c4").val(0);}
				if($(this).data('abr1')>0){ $("#c5").val($(this).data('abr1')); }else {$("#c5").val(0);}
				if($(this).data('may1')>0){ $("#c6").val($(this).data('may1')); }else {$("#c6").val(0);}
				if($(this).data('jun1')>0){ $("#c7").val($(this).data('jun1')); }else {$("#c7").val(0);}
				if($(this).data('jul1')>0){ $("#c8").val($(this).data('jul1')); }else {$("#c8").val(0);}
				if($(this).data('ago1')>0){ $("#c9").val($(this).data('ago1')); }else {$("#c9").val(0);}
				if($(this).data('sep1')>0){ $("#c10").val($(this).data('sep1')); }else {$("#c10").val(0);}
				if($(this).data('oct1')>0){ $("#c11").val($(this).data('oct1')); }else {$("#c11").val(0);}
			}
			if($(this).data('cic')==(ini3-3)){	
				if($(this).data('dic1')>0){ $("#d1").val($(this).data('dic1')); }else {$("#d1").val(0);} 
				if($(this).data('ene1')>0){ $("#d2").val($(this).data('ene1')); }else {$("#d2").val(0);}
				if($(this).data('feb1')>0){ $("#d3").val($(this).data('feb1')); }else {$("#d3").val(0);}
				if($(this).data('mar1')>0){ $("#d4").val($(this).data('mar1')); }else {$("#d4").val(0);}
				if($(this).data('abr1')>0){ $("#d5").val($(this).data('abr1')); }else {$("#d5").val(0);}
				if($(this).data('may1')>0){ $("#d6").val($(this).data('may1')); }else {$("#d6").val(0);}
				if($(this).data('jun1')>0){ $("#d7").val($(this).data('jun1')); }else {$("#d7").val(0);}
				if($(this).data('jul1')>0){ $("#d8").val($(this).data('jul1')); }else {$("#d8").val(0);}
				if($(this).data('ago1')>0){ $("#d9").val($(this).data('ago1')); }else {$("#d9").val(0);}
				if($(this).data('sep1')>0){ $("#d10").val($(this).data('sep1')); }else {$("#d10").val(0);}
				if($(this).data('oct1')>0){ $("#d11").val($(this).data('oct1')); }else {$("#d11").val(0);}
			}
			if($(this).data('cic')==(ini3-4)){	
				if($(this).data('dic1')>0){ $("#e1").val($(this).data('dic1')); }else {$("#e1").val(0);} 
				if($(this).data('ene1')>0){ $("#e2").val($(this).data('ene1')); }else {$("#e2").val(0);}
				if($(this).data('feb1')>0){ $("#e3").val($(this).data('feb1')); }else {$("#e3").val(0);}
				if($(this).data('mar1')>0){ $("#e4").val($(this).data('mar1')); }else {$("#e4").val(0);}
				if($(this).data('abr1')>0){ $("#e5").val($(this).data('abr1')); }else {$("#e5").val(0);}
				if($(this).data('may1')>0){ $("#e6").val($(this).data('may1')); }else {$("#e6").val(0);}
				if($(this).data('jun1')>0){ $("#e7").val($(this).data('jun1')); }else {$("#e7").val(0);}
				if($(this).data('jul1')>0){ $("#e8").val($(this).data('jul1')); }else {$("#e8").val(0);}
				if($(this).data('ago1')>0){ $("#e9").val($(this).data('ago1')); }else {$("#e9").val(0);}
				if($(this).data('sep1')>0){ $("#e10").val($(this).data('sep1')); }else {$("#e10").val(0);}
				if($(this).data('oct1')>0){ $("#e11").val($(this).data('oct1')); }else {$("#e11").val(0);}
			}
			if($(this).data('cic')==(ini3-5)){	
				if($(this).data('dic1')>0){ $("#f1").val($(this).data('dic1')); }else {$("#f1").val(0);} 
				if($(this).data('ene1')>0){ $("#f2").val($(this).data('ene1')); }else {$("#f2").val(0);}
				if($(this).data('feb1')>0){ $("#f3").val($(this).data('feb1')); }else {$("#f3").val(0);}
				if($(this).data('mar1')>0){ $("#f4").val($(this).data('mar1')); }else {$("#f4").val(0);}
				if($(this).data('abr1')>0){ $("#f5").val($(this).data('abr1')); }else {$("#f5").val(0);}
				if($(this).data('may1')>0){ $("#f6").val($(this).data('may1')); }else {$("#f6").val(0);}
				if($(this).data('jun1')>0){ $("#f7").val($(this).data('jun1')); }else {$("#f7").val(0);}
				if($(this).data('jul1')>0){ $("#f8").val($(this).data('jul1')); }else {$("#f8").val(0);}
				if($(this).data('ago1')>0){ $("#f9").val($(this).data('ago1')); }else {$("#f9").val(0);}
				if($(this).data('sep1')>0){ $("#f10").val($(this).data('sep1')); }else {$("#f10").val(0);}
				if($(this).data('oct1')>0){ $("#f11").val($(this).data('oct1')); }else {$("#f11").val(0);}
			}*/
			var chart;
  			ini=$("#ciclosel").val();
    		a1=$("#a1").val(); a1=parseFloat(a1);a2=$("#a2").val(); a2=parseFloat(a2);a3=$("#a3").val(); a3=parseFloat(a3);a4=$("#a4").val(); a4=parseFloat(a4); 
    		a5=$("#a5").val(); a5=parseFloat(a5);a6=$("#a6").val(); a6=parseFloat(a6);a7=$("#a7").val(); a7=parseFloat(a7);a8=$("#a8").val(); a8=parseFloat(a8);
    		a9=$("#a9").val(); a9=parseFloat(a9);a10=$("#a10").val(); a10=parseFloat(a10);a11=$("#a11").val(); a11=parseFloat(a11);a12=$("#a12").val(); a12=parseFloat(a12);
    		b1=$("#b1").val(); b1=parseFloat(b1);b2=$("#b2").val(); b2=parseFloat(b2);b3=$("#b3").val(); b3=parseFloat(b3);b4=$("#b4").val(); b4=parseFloat(b4); 
    		b5=$("#b5").val(); b5=parseFloat(b5);b6=$("#b6").val(); b6=parseFloat(b6);b7=$("#b7").val(); b7=parseFloat(b7);b8=$("#b8").val(); b8=parseFloat(b8);
    		b9=$("#b9").val(); b9=parseFloat(b9);b10=$("#b10").val(); b10=parseFloat(b10);b11=$("#b11").val(); b11=parseFloat(b11);b12=$("#b12").val(); b12=parseFloat(b12);
    		/*c1=$("#c1").val(); c1=parseFloat(c1);c2=$("#c2").val(); c2=parseFloat(c2);c3=$("#c3").val(); c3=parseFloat(c3);c4=$("#c4").val(); c4=parseFloat(c4); 
    		c5=$("#c5").val(); c5=parseFloat(c5);c6=$("#c6").val(); c6=parseFloat(c6);c7=$("#c7").val(); c7=parseFloat(c7);c8=$("#c8").val(); c8=parseFloat(c8);
    		c9=$("#c9").val(); c9=parseFloat(c9);c10=$("#c10").val(); c10=parseFloat(c10);c11=$("#c11").val(); c11=parseFloat(c11);
    		d1=$("#d1").val(); d1=parseFloat(d1);d2=$("#d2").val(); d2=parseFloat(d2);d3=$("#d3").val(); d3=parseFloat(d3);d4=$("#d4").val(); d4=parseFloat(d4); 
    		d5=$("#d5").val(); d5=parseFloat(d5);d6=$("#d6").val(); d6=parseFloat(d6);d7=$("#d7").val(); d7=parseFloat(d7);d8=$("#d8").val(); d8=parseFloat(d8);
    		d9=$("#d9").val(); d9=parseFloat(d9);d10=$("#d10").val(); d10=parseFloat(d10);d11=$("#d11").val(); d11=parseFloat(d11);
    		e1=$("#e1").val(); e1=parseFloat(e1);e2=$("#e2").val(); e2=parseFloat(e2);e3=$("#e3").val(); e3=parseFloat(e3);e4=$("#e4").val(); e4=parseFloat(e4); 
    		e5=$("#e5").val(); e5=parseFloat(e5);e6=$("#e6").val(); e6=parseFloat(e6);e7=$("#e7").val(); e7=parseFloat(e7);e8=$("#e8").val(); e8=parseFloat(e8);
    		e9=$("#e9").val(); e9=parseFloat(e9);e10=$("#e10").val(); e10=parseFloat(e10);e11=$("#e11").val(); e11=parseFloat(e11);
    		f1=$("#f1").val(); f1=parseFloat(f1);f2=$("#f2").val(); f2=parseFloat(f2);f3=$("#f3").val(); f3=parseFloat(f3);f4=$("#f4").val(); f4=parseFloat(f4); 
    		f5=$("#f5").val(); f5=parseFloat(f5);f6=$("#f6").val(); f6=parseFloat(f6);f7=$("#f7").val(); f7=parseFloat(f7);f8=$("#f8").val(); f8=parseFloat(f8);
    		f9=$("#f9").val(); f9=parseFloat(f9);f10=$("#f10").val(); f10=parseFloat(f10);f11=$("#f11").val(); f11=parseFloat(f11);*/
    		$(document).ready(function() {
        		chart = new Highcharts.Chart({
            		chart: { renderTo: 'containere', type: 'column' },
            		title: { text: 'Adquisiciones Mensuales de Combustible de Diesel y Gasolina' },
            		subtitle: { text: '' },
            		xAxis: { categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ] },
            		yAxis: { min: 0, title: { text: 'Importe' }  },
            		legend: { layout: 'horizontal', backgroundColor: '#FFFFFF', align: 'left', verticalAlign: 'top', x: 70, y: 30, floating: true, shadow: true },
            		tooltip: {
                		formatter: function() {
                    		return ''+
                        		this.x +': '+ this.y +' mn';
                		}
            		},
            		plotOptions: { column: { pointPadding: 0.2, borderWidth: 0 } },
            		series: [{
                		name: ini,
                		data: [ a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12]
            		}, {
                		name: ini-1,
                		data: [ b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12]
            		} /*, {
                		name: ini-2,
                		data: [ c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11]
            		}, {
                		name: ini-3,
                		data: [ d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11]
            		}, {
                		name: ini-4,
                		data: [ e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11]
            		}, {
                		name: ini-5,
                		data: [ f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11]
            		}*/
            		]
        		});
    		});
	    });
	    
    },   
	});
	
	$("#tabla_zon").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gastosviaje/tablaZona',  	
		sort:false,
		onRowClick:function(){
    		$("#zona").val($(this).data('zon'));
    		$("#tabla_cd").ajaxSorter({
				url:'<?php echo base_url()?>index.php/gastosviaje/tablacd',  	
				filters:['zona'],	
				sort:false,
    			onSuccess:function(){    		
    				$('#tabla_cd tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })	    	
	    			
				},     
			});
			$("#tabla_cd1").ajaxSorter({
				url:'<?php echo base_url()?>index.php/gastosviaje/tablacd1',  	
				filters:['zona'],	
				sort:false,
    			onSuccess:function(){    		
    				$('#tabla_cd1 tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })	    	
	    			
				},     
			});
    	},
    	onSuccess:function(){
    		$('#tabla_zon tbody tr').map(function(){
		    	if($(this).data('zon')=='Total:'){
		    		$(this).css('background','lightblue');$(this).css('font-weight','bold');		    		
		    	}		    	
	    	})
	    	$('#tabla_zon tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold'); })
    		$('#tabla_zon tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); })
	    	$('#tabla_zon tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right');$(this).css('background','lightgreen');$(this).css('font-weight','bold'); })	    	
	    	$('#tabla_zon tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_zon tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');$(this).css('background','lightgreen');$(this).css('font-weight','bold'); })
	    	$('#tabla_zon tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_zon tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_zon tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_zon tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','right'); })	    	
    	},     
	});
});
</script>