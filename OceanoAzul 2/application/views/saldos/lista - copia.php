<?php $this->load->view('header_cv'); 
 
?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/entregado.png" width="25" height="25" border="0"> Facturado </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion">
			<h3 align="left"><a href="">Entregas Actuales</a></h3>
        <div style="height:335px;">                                                          
            <!--Tabla-->
            <div class="ajaxSorterDiv" id="tabla_fac" name="tabla_fac" >                
                <div style="text-align:left" class='filter' >                	 
                	Ciclo:
                    <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 24px;" >
						<?php $ciclof=2007; $actual=date("Y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            				<?php $actual-=1; } ?>
          			</select>
                	Seleccione Zona:
                    <select name="cmbZona" id="cmbZona" style="font-size: 10px; height: 24px">
						<option value="0">Todos</option>			<option value="1">Angostura</option>
						<option value="2">Colima</option>			<option value="3">El Dorado</option>
						<option value="4">Elota</option>			<option value="5">Guasave</option>
						<option value="6">Hermosillo</option>		<option value="7">Mochis</option>
						<option value="8">Navojoa</option>			<option value="9">Navolato</option>
						<option value="10">Nayarit</option>			<option value="11">Obregon</option>
						<option value="12">Sur Sinaloa</option>		<option value="13">Tamaulipas</option>
						<option value="14">Yucatán</option>
					</select>                	                
                </div>
                <span class="ajaxTable" style="height: 307px; background-color: white">
                    <table id="mytablaR" name="mytablaR" class="ajaxSorter">
                        <thead title="Presione las columnas para ordenarlas como requiera">                            
                            <th data-order = "Zona" style="width: 70px;">Zona</th>
                            <th data-order = "Razon" style="width: 300px;">Razón Social</th>                                                        
                            <th data-order = "CantidadRR" style="width: 70px;">Cantidad</th>                                                        
                        </thead>
                        <tbody title="Seleccione para analizar cliente" style="font-size: 11px">
                        </tbody>
                    </table>
                </span> 
             	<div class="ajaxpager" style="">        
                    <ul class="order_list"></ul>     
                    <form method="post" action="<?= base_url()?>index.php/reporter/remisiones" > 
                        	<img style="visibility: hidden" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png"  />
                        	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        	
                	</form>               
                    <form method="post" action="<?= base_url()?>index.php/remisiones/pdfrep" >                    	             	                                                
                        <select  class="pagesize" style="font-size: 10px; height: 23px; visibility: hidden;">
                                <option selected="selected" value="0">Gral</option>                                          
                        </select>                                                
                        <img title="Enviar datos de tabla en archivo PDF" class="exporterpdf" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 					</form>   
 					
                </div>                                      
            </div>    
        </div>
        
        <h3 align="left"><a href="" >Analizar Cliente</a></h3>
        <div style="height:300px; ">  
        	<table style="width: 730px; margin-top: 1px;" border="2px"  class="tablesorter">
				<thead >								
					<input  size="8%" name="numcli" id="numcli"/>									
					<th colspan="6" style="font-size: 14px;"><input style="border: 1" readonly="true" size="103%"  type="text" name="nombre" id="nombre" ></th>
				     <tr>
                         <th width="50">Fecha</th>
                         <th width="50">Remisión</th>                                                                                    
                         <th width="70">Cantidad</th>
                         <th width="50">Precio</th>
                         <th width="70">Importe</th>
                         <th width="70">Folio</th>
                    </tr>
                </thead>
               </table>
               <div style="width:730px; height: 289px;  overflow-x: hidden; margin-top: -15px; "  >
                <table style="width: 710px;font-size: 11px; "  class="tablesorter">  
                	 <tbody>             	
                	<?php
						$this->load->model('facturado_model');
						
						$data['result']=$this->facturado_model->Entregado();
						$pl=0;$imp=0;
						foreach ($data['result'] as $row): $pl+=$row->CantidadRR;$imp+=($row->CantidadRR*$row->PrecioR); ?>
						<tr >				
							<td align="center" width="50"><?php echo $row->FechaR;?></td>
							<td align="center" width="50"><?php echo $row->RemisionR;?></td>
							<td align="right" width="50"><?php echo number_format($row->CantidadRR,3);?></td>
							<td align="center" width="50"><?php echo '$ '.number_format($row->PrecioR,2);?></td>
							<td align="right" width="60"><?php echo '$ '.number_format($row->CantidadRR*$row->PrecioR,2);?></td>
							<td align="center" width="70"><?php echo $row->Folio;?></td>
						</tr>
						<?php endforeach;?>   
					</tbody>						                                              	
                </table>
                </div>
                <table style="width: 730px; margin-top: 1px;" border="2px"  class="tablesorter">
				<thead >							
					 <tr>
                         <th width="50">Total Ciclo</th>
                         <th width="50"></th >                                                                                                          
                         <th width="70"><?php echo number_format($pl,3);?></th>
                         <th width="50"></th>
                         <th width="70"><?php echo '$ '.number_format($imp,2);?></th>
                         <th width="70"></th>
                    </tr>
                </thead>
               </table>
                            
        </div>	
	 </div> 	
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#tabla_fac").ajaxSorter({
	url:'<?php echo base_url()?>index.php/facturado/tabla',  
	//filters:['cmbCiclo'],
	filters:['cmbZona'],	
	//multipleFilter:true, 	
	//filterTypeChange:false,
    onRowClick:function(){
        $("#accordion").accordion( "activate",1 );
       	$("#nombre").val($(this).data('razon'));
       	$("#zona").val($(this).data('zona'));
		$("#numcli").val($(this).data('numero'));			
    }   
});



</script>