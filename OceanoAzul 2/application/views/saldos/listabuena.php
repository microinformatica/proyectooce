<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url()?>assets/js/jquery.loader.js"></script>
<link href="<?php echo base_url()?>assets/css/jquery.loader.css" rel="stylesheet"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/ajaxSorterStyle/style.css">     
              
        
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui-1.8.23.custom.min.js"></script>                
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.ajaxsorter.js"></script>        
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.boxloader.js"></script>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 25px;
	/*background: none;*/		
}   
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
.hide{
      display: none;
}
</style>

<div style="height:698px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#entregas"><img src="<?php echo base_url();?>assets/images/menu/administracion.png" width="25" height="25" border="0"> Saldos </a></li>			
		<li style="font-size: 20px"><a href="#grafica"><img src="<?php echo base_url();?>assets/images/grafica.png" width="25" height="25" border="0"> Gráfica </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="entregas" class="tab_content" >
			<div id = "accordion" style="margin-top: -10px; margin-left: -10px; width: 958px;" >
					<h3 align="left" style="font-size: 12px" ><a href="#">  Estado de Cuenta </a></h3>
					<div style="height: 600px;" align="left">
						<input size="8%" type="hidden"  name="numcli" id="numcli"/> 
						Cliente:  <input style="border: 1" readonly="true" size="88%"  type="text" name="nombre" id="nombre" >
						<button id='btn_mas_ajaxsorter' class="continuar used">Buscar</button>
						Ciclo: 
					    <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px; width: 70px;" >
					    	<option value="0">Todos</option>
                    	<?php $ciclof=2008; $actual=date("Y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            				<?php  $actual-=1; } ?>
          				</select>
                        <div id='ver_mas_ajaxsorter' class='hide' >                			              
                			   <div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" style="width: 555px; margin-top: 1px; margin-left: 60px">                
               				      <span class="ajaxTable" style="margin-left: 0px; background-color: white; height: 70px">
                    				<table id="mytablaC" name="mytablaC" class="ajaxSorter">
                        				<thead title="Presione las columnas para ordenarlas como requiera">                            
                           					<th data-order = "Razon" >Razon Social</th>                                                                                    
                        				</thead>
                        				<tbody title="Seleccione para consultar estado de cuenta" style="font-size: 11px">                                                 	  
                        				</tbody>
                    				</table>
                				 </span> 
            					</div>
                		</div>
                		<br>REMISIONES
						<div class="ajaxSorterDiv" id="tabla_rem" name="tabla_rem" align="left">                
               				<span class="ajaxTable" style="height: 200px" >
                    			<table id="mytablaRem" class="ajaxSorter">
                        			<thead title="Presione las columnas para ordenarlas como requiera">                            
                            			<th data-order = "FechaR" style="width: 70px" >Fecha</th>
                            			<th data-order = "RemisionR" style="width: 30px">Remisión</th>    
                            			<th data-order = "especie" style="width: 30px">Producto</th>                                                      
                            			<th data-order = "CantidadR1" style="width: 70px" >Remisionado</th>                                                                                  
                            			<th data-order = "DescuentoR1" style="width: 20px" >Descto.</th>       
                            			<th data-order = "CantidadRR1" style="width: 70px" >Facturado</th>                     
                            			<th data-order = "PrecioR1" style="width: 40px" >Precio</th>
                            			<th data-order = "importe" style="width: 80px" >Importe</th>                          
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>
                    			</table>
                			</span> 
             				<div class="ajaxpager">        
                    			<ul class="order_list"></ul>                        
 					 			<form method="post" action="<?= base_url()?>index.php/saldos/pdfrep" >                    	             	                                                
                    	        	<img title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        			<input type="hidden"  name="tablaremisiones" value ="" class="htmlTable"/>                                                    
 								</form>   
                			</div>                                      
            			</div>	
            			<br>DEPÓSITOS Y CARGOS
						<div class="ajaxSorterDiv" id="tabla_dep" name="tabla_dep"  align="left">                
               				<span class="ajaxTable" style="height: 200px" >
                    			<table id="mytablaDep" class="ajaxSorter" style="white-space: 700px" >
                        			<thead title="Presione las columnas para ordenarlas como requiera">                            
                            			<th data-order = "total1" style="width: 70px" >Total</th>
                            			<th data-order = "Fecha" style="width: 70px">Fecha</th>    
                            			<th data-order = "importe1" style="width: 50px">U.S.D.</th>                                                      
                            			<th data-order = "saldo1" style="width: 80px" >Saldo</th>                                                                                  
                            			<th data-order = "tc1" style="width: 50px" >T.C.</th>       
                            			<th data-order = "pesos1" style="width: 80px" >M.N.</th>                     
                            			<th data-order = "Aplicar" style="width: 80px" >Ciclo</th>  
                            			<th data-order = "Obs" style="width: 40px" >Observaciones</th>                                              
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>
                    			</table>
                			</span> 
             				<div class="ajaxpager">        
                    			<ul class="order_list"></ul> 
                    			<form method="post" action="<?= base_url()?>index.php/saldos/pdfrep/'$('#tablaremisiones').val()">                    	             	                                                
                    	        	<img title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />                        			
                        			<input type="hidden" name="tabladepositos" value ="" class="htmlTable"/>  
                                </form>   
 					        </div>                                      
            			</div>	 	
					</div>
        			<h3 align="left" style="font-size: 12px"><a href="#">Analizar Cliente</a></h3>
		       	 	<div style="height: 150px;">
		       		    
        			</div>
	 		</div>	
	 	</div>
		<div id="grafica" class="tab_content" style="height: 690px"  >
			 				
			
		</div>	
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$(document).ready(function(){ 
	$('#btn_mas_ajaxsorter').click(function(){
    	if($(this).hasClass('used')){
    		$(this).removeClass('used');	
    		$('#ver_mas_ajaxsorter').show();
    		$(this).text('Cerrar Clientes');
    	}    			
		else{
			$(this).addClass('used');
			$('#ver_mas_ajaxsorter').hide();
			$(this).text('Buscar');
		}
    });            
	
	$("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/clientes/tabla',  
		multipleFilter:true, 
   		onload:false,
   		sort:false,	
		onRowClick:function(){
        	$("#numcli").val($(this).data('numero'));
			$("#nombre").val($(this).data('razon'));
			if($(this).data('numero')>'0'){			
				//$(this).hasClass('used');
				//$(this).removeClass('used');
				$('#ver_mas_ajaxsorter').hide();
				$('#btn_mas_ajaxsorter').text('Buscar');		
				$("#tabla_rem").ajaxSorter({
					url:'<?php echo base_url()?>index.php/saldos/tablaremisiones/'+$("#numcli").val(), 
					filters:['cmbCiclo'],
					onload:false,
   					sort:false, 
   					onSuccess:function(){
						$('#tabla_rem tbody tr').map(function(){
		    				if($(this).data('remisionr')==''){
		    					$(this).css('background','lightblue');	    				    				    			    			
	    					}
	    				})
	    			},  			   		
				});	
				$("#tabla_dep").ajaxSorter({
					url:'<?php echo base_url()?>index.php/saldos/tabladepo/'+$("#numcli").val(), 
					filters:['cmbCiclo'],
					onload:false,
   					sort:false,   			   		
				});	
			}
   		},   		
	});
	
});
</script>
