<?php $this->load->view('header_cv');?>
	<!--	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/ajaxSorterStyle/style.css">     
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui-1.8.23.custom.min.js"></script>                
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.ajaxsorter.js"></script>        
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.boxloader.js"></script> -->
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
.hide{
      display: none;
}

</style>
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/administracion.png" width="25" height="25" border="0"> Saldos</li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion">
			<h3 align="left"><a href="">ESTADO DE CUENTA</a></h3>
        	<div style="height:940px;">  
        		<div align="left" style="margin-top: -10px;height:540px; ">     
               		<input size="4%" type="hidden" name="numcli" id="numcli"/>
               		<input size="8%" type="hidden" name="tabla" id="tabla"/>
               		<input size="8%" type="hidden" name="ciclo" id="ciclo"/>
					Cliente:<input style="border:0; background-color: lightgray;"   title="click para buscar cliente" readonly="true" size="88%"  type="text" name="nombre" id="nombre" >
					<!--<button id='btn_mas_ajaxsorter' class="continuar used">Buscar</button>-->
					Ciclo: 
					<select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px; width: 65px;" >
					    	<option value="0">Todos</option>
                    	<?php $ciclof=2008; $actual=date("Y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            				<?php  $actual-=1; } ?>
          			</select>
          			
          			<select name="cmbCancelacion" id="cmbCancelacion" style="font-size: 10px; height: 25px; width:105px;" >
          				<option value="-1">Sin Cacelados</option>
          				<option value="0">Todos</option>
          			</select>
          			<div id='ver_mas_ajaxsorter' class='hide' style="height: 90px" >                			              
                		<div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" style="width: 459px; margin-top: -5px; margin-left: 44px">                
               			     <span class="ajaxTable" style="margin-left: 0px; background-color: white; height: 70px; width: 457px">
                    			<table id="mytablaC" name="mytablaC" class="ajaxSorter">
                        			<thead title="Presione las columnas para ordenarlas como requiera">                            
                        				<th data-order = "Razon" >Razon Social</th>                                                                                    
                        			</thead>
                        			<tbody title="Seleccione para consultar estado de cuenta" style="font-size: 11px">                                                 	  
                        			</tbody>
                    			</table>
                			 </span> 
            			</div>
                	</div>
                	<div id='ver_mas_remi' class='hide' style="margin-top: 4px">
                        <div style="color: blue">REMISIONES</div>
						<div class="ajaxSorterDiv" id="tabla_rem" name="tabla_rem" align="left">                
               				<span class="ajaxTable" style="height: 200px; margin-top: 1px; background-color: white; width: 865px;" >
                    			<table id="mytablaRem" name="mytablaRem" class="ajaxSorter" >
                        			<thead title="Presione las columnas para ordenarlas como requiera">                            
                            			<th data-order = "FechaR1" style="width: 70px" >Fecha</th>
                            			<th data-order = "RemisionR" style="width: 30px">Remisión</th>    
                            			<th data-order = "especie" style="width: 30px">Producto</th>                                                      
                            			<th data-order = "CantidadR1" style="width: 70px" >Remisionado</th>                                                                                  
                            			<th data-order = "DescuentoR1" style="width: 20px" >Descto.</th>       
                            			<th data-order = "CantidadRR1" style="width: 70px" >Facturado</th>                     
                            			<th data-order = "PrecioR1" style="width: 40px" >Precio</th>
                            			<th data-order = "importe" style="width: 80px" >Importe</th>                                                    			                            			                            			                            			                            			
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>
                    			</table>
                			</span> 
             				<div class="ajaxpager">        
                    			<ul class="order_list"></ul>                        
 					 			<form method="post" action="" >                    	             	                                                
                    	        	<!--<img title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />-->
                        			<input type="hidden"  id="tablaremisiones" name="tablaremisiones" value ="" class="htmlTable"/>                                                    
 								</form>   
                			</div>                                      
            			</div>
            		</div>
            		<div id='ver_mas_fac' class='hide' style="height: 238px; margin-top: 4px" align="" >
						<div style="color: blue">REMISIONES</div>
       					<table style="width: 875px;" border="2px"  class="tablesorter">
							<thead >
								<th colspan="5" height="15px" style="font-size: 20px">Datos de Facturación
								<input size="8%" type="hidden" name="id" id="id"/>								
								<!--<input size="8%"  name="numcli" id="numcli"/>-->
								<input size="8%" type="hidden" name="tabla" id="tabla"/>
								<th colspan="2">
									<center>
										<?php if($usuario=="Jesus Benítez" || $usuario=="Lety Lizárraga" || $usuario=="Efrain Lizárraga"){ ?>
										<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
										<?php }?>
										<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(1)" />
									</center>	
								</th>								
								</th>									
								<!--<input size="8%" type="hidden" name="rem1" id="rem1" value="<?php echo set_value('rem1',$txtRemision); ?>">
								<input size="8%" type="hidden" name="ur" id="ur" value="<?php echo set_value('ur',$urr); ?>">
								<input type="hidden" size="8%" type="text" name="sn" id="sn" >-->
							</thead>
							<tr>
								<th><label style="margin: 2px; font-size: 14px;" for="rem">Remision</label></th>
								<th><label style="margin: 2px; font-size: 14px" for="txtFecha">Fecha</label></th>
								<th><label style="margin: 2px; font-size: 14px" for="can">Cantidad</label></th>
								<th><label style="margin: 2px; font-size: 14px;" for="pre" >Precio $</label></th>	
								<th><label style="margin: 2px; font-size: 14px" for="dco">Descuento</label></th>
								<th><label style="margin: 2px; font-size: 14px" for="avi">Aviso</label></th>																	
								<th><label style="margin: 2px; font-size: 14px" for="pro">Producto</label></th>										
							</tr>
    						<tr style="font-size: 14px; background-color: white;">
								<th rowspan="4" style="font-size: 25px;" ><input readonly="true" size="8%" type="text" name="rem" id="rem" style="text-align: center; border: none; color: red;" ></th>	
								<th><input <?php if($usuario=="Lety Lizárraga" || $usuario=="Efrain Lizárraga"){ ?> disabled="true" <?php } ?>  size="13%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" style="text-align: center;"  readonly="true" ></th>									
								<th><input size="14%" type="text" name="can" id="can" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" style="text-align: right;"></th>
								<th><input size="8%" type="text" name="pre" id="pre" value="" style="text-align: right; margin-left: 10px"></th>									
								<th><input size="8%" type="text" name="dcor" id="dcor" style="text-align: right; margin-left: 10px"></th>
								<th><input size="10%" type="text" name="avi" id="avi" value="" style="text-align: center; margin-left: 10px"></th>
								<th style="font-size: 13px;padding-bottom: 1px;">									  																	
					    			<select name="lstProducto" id="lstProducto" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">
      									<option value="1"  > Postlarvas </option>
      									<option value="2"  > Nauplio </option>
      									<option value="3"  > Adultos </option>
    								</select>    								  
								</th>
							</tr>
    						<tr>
								<th><label style="margin: 2px; font-size: 14px" for="est">Estatus</label></th>
								<th colspan="5"><label style="margin: 2px; font-size: 14px" for="obsr">Observaciones</label></th>
							</tr>
    						<tr style="background-color: white">
								<th style="font-size: 13px;padding-bottom: 1px;">									  																	
							  		<select name="estatus" id="estatus" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:5px;">
	      								<option style="color: green" value="Facturacion" > Facturación </option>
    	  								<option style="color:white; background-color: blue" value="Reposicion"  > Reposición </option>      								
    								  </select>    								  
								</th>
								<th colspan="5" style="font-size: 11px;"><input size="100%" type="text" name="obsr" id="obsr" value="" style="text-align: left;"></th>
							</tr>
    						<tfoot>
								<tr>									
									<th style=" text-align: right; margin: 2px; font-size: 14px;" colspan="7">Procesar Datos en calidad de Cancelación de Pago 						
										<select name="rblugar" id="rblugar" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
      										<option style="color: blue" value="0" > No </option>
      										<option style="color: red" value="-1"  > Si </option>      									
    									</select>					
    								</th>
								</tr>
							</tfoot>
						</table>
					</div>
					<div id='ver_mas_remit' class='hide' style="height: 238px; margin-top: 4px" align="" >
						<div style="color: blue">REMISIONES</div>
       					<table style="width: 875px;" border="2px"  class="tablesorter">
							<thead >
								<th colspan="5" height="15px" style="font-size: 20px">Datos de Facturación Ciclo [ <input style="border: none; background: none" size="3%" readonly="true"  name="ciclor" id="ciclor"/> ]
								<th colspan="2">
									<center>
										<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(3)" />
									</center>	
								</th>								
								</th>																	
							</thead>
							<tfoot>
								<tr>									
									<th style=" text-align: center; margin: 2px; font-size: 14px;" colspan="7"> 						
										<?php if($usuario=="Jesus Benítez" || $usuario=="Lety Lizárraga" || $usuario=="Efrain Lizárraga"){ ?>
										<input style="font-size: 14px" type="submit" id="cancelarrp" name="cancelarrp" value="Cancelar por Pagos" onclick="canresremi(-1,0)"/>
										<input style="font-size: 14px" type="submit" id="restablecerrp" name="restablecerrp" value="Restablecer" onclick="canresremi(0,0)"/>
										<?php }?>				
    								</th>
								</tr>
							</tfoot>
						</table>
					</div>
            		<div id='ver_mas_depo' class='hide'>	
             			<div style="margin-top: -15px; color: blue">DEPÓSITOS Y CARGOS
						<div class="ajaxSorterDiv" id="tabla_dep" name="tabla_dep"  align="left">                
               				<span class="ajaxTable" style="height: 200px; margin-top: 1px; background-color: white; width: 865px;" >
                    			<table id="mytablaDep" name="mytablaDep" class="ajaxSorter"  >
                        			<thead title="Presione las columnas para ordenarlas como requiera">                            
                            			<th data-order = "total1" style="width: 60px" >Total</th>
                            			<th data-order = "Fecha1" style="width: 60px">Fecha</th>    
                            			<th data-order = "importe1" style="width: 70px">U.S.D.</th>                                                      
                            			<th data-order = "saldo1" style="width: 70px">Saldo</th>                                                                                  
                            			<th data-order = "tc1" >T.C.</th>       
                            			<th data-order = "pesos1" >M.N.</th>                     
                            			<th data-order = "Aplicar"  >Ciclo</th>                              			                            			
                            			<th data-order = "Obs" style="width: 280px" >Observaciones</th>                                              
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>
                    			</table>
                			</span> 
             				<div class="ajaxpager" style="margin-top: -15px">  
             					<ul class="order_list"></ul>
                    			<form method="post" action="<?= base_url()?>index.php/saldos/pdfrep/">                    				
                    				SALDO: <input size="17%" style="border: 0; text-align: right; text-decoration: underline;" readonly="true"  type="text" name="saldo" id="saldo" >
                    				<input size="150%" style="border: 0; font-size: 10px;text-decoration: underline; "  name="letras" id="letras" >
                    				<img title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />           
                    				<br> <label >Atención:<input style="border:0; background-color: lightgray" size="50%"  type="text" name="atc" id="atc" ></label>             			
                        			<input type="hidden" id="tabladepositos" name="tabladepositos" class="htmlTable"/>
                        			<input type="hidden" id="tablaremisiones1" name="tablaremisiones1" class="htmlTable"/>
                        			<input  style="border: 0" type="hidden" readonly="true" size="20%"  type="text" name="saldo1" id="saldo1" >
                        			<input type="hidden" style="border:0; background-color: lightgray" readonly="true" size="88%"  type="text" name="cli" id="cli" >
                        			<input type="hidden" style="border:0; background-color: lightgray" readonly="true" size="88%"  type="text" name="dom" id="dom" >
                		  			<input type="hidden" style="border:0; background-color: lightgray" readonly="true" size="88%"  type="text" name="lecp" id="lecp" >
                        			<!--<button id="imprimir" onclick="">Imprimir</button>--> 
                    				<script>
                    				/*$('#imprimir').click(function(){
                    					//if(exporter1!=undefined){//Exporta el contenido actual de la tabla
     								   		$(id_pager+" img.exporter").click(function(){
            									if(confirm("Desea exportar los datos visibles en la tabla?")){
               										$(id_pager + " input.htmlTable").attr('value',$("#mytablaDep").html());  
			   										$(id_pager+" form").submit();
			   										$(id_pager + " input.htmlTable").attr('value',''); 
												}	                    
        									//});  
    									//}
                    				});
                    					/*$('#imprimir').click(function(){
	                    					$.ajax({
												type: "POST",//Envio
												url:'<?php // echo base_url()?> index.php/saldos/pdfrep',
												//	data: "tablaremisiones="+$("#tabla_remisiones")." .ajaxSorter".html(),
												data: "tablaremisiones1="+$("#tabla_rem")+"&tabladepositos="+$("#tabla_dep"),
											});	
                    					});*/
                    				</script> 
                        		</form>
                            </div>                                      
            			</div>	
                 		</div>
                 	</div>  
                 	<div id='ver_mas_dep' class='hide' style="height: 253px" align="" >
						<div style="margin-top: -15px;color: blue">DEPÓSITOS Y CARGOS</div>
       					<table  style="width: 875px;" border="2px"  class="tablesorter">
							<thead >
								<th colspan="4" height="15px" style="font-size: 20px">Datos de Depósito
								<input size="8%" type="hidden"  name="idd" id="idd"/>																
								</th>
								<th colspan="2">
									<center>
									<?php if($usuario=="Jesus Benítez" || $usuario=="Lety Lizárraga" || $usuario=="Efrain Lizárraga"){ ?>
									<input style="font-size: 14px" type="submit" id="aceptard" name="aceptard" value="Guardar" />									
									<?php }?>	
									<?php if($usuario=="Jesus Benítez" || $usuario=="Lety Lizárraga"){ ?>									
									<input style="font-size: 14px" type="submit" id="borrard" name="borrard" value="Borrar" />
									<?php }?>	
									<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(2)" />									
									</center>	
								</th>
							 </thead>
								<tr>
									<th><label style="margin: 2px; font-size: 14px" for="txtFecha">Fecha</label></th>
									<th><label style="margin: 2px; font-size: 14px" for="mn">Pesos $</label></th>
									<th><label style="margin: 2px; font-size: 14px;" for="usd" >Dólares $</label></th>	
									<th><label style="margin: 2px; font-size: 14px" for="tc">Tipo de Cambio</label></th>
									<th><label style="margin: 2px; font-size: 14px" for="cta">Cuenta</label></th>																	
									<th><label style="margin: 2px; font-size: 14px" for="pro">Aplicar a Ciclo</label></th>
								</tr>
    							<tr style="font-size: 14px; background-color: white;">
									<th><input size="14%" type="text" name="txtFechad" id="txtFechad" class="fecha redondo mUp" readonly="true" style="text-align: center; margin-left: 10px" ></th>								
									<th><input size="14%" type="text" name="mn" id="mn" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right; margin-left: 10px"></th>
									<th><input size="14%" type="text" name="usd" id="usd" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right; margin-left: 10px"></th>									
									<th><input size="8%" type="text" name="tc" id="tc" style="text-align: right; margin-left: 10px"></th>
									<th><input size="10%" type="text" name="cta" id="cta" value="" style="text-align: center; margin-left: 10px"></th>
									<th style="font-size: 13px;padding-bottom: 1px;">									  																	
									    <select name="lstCiclo" id="lstCiclo" style="font-size: 10px; margin-top: 1px; margin-left: 12px; height: 23px;" >
											<?php $ciclof=2007; $actual=date("Y"); 
											$mes=date("m");
											if($mes>=11) $actual+=1;											
											while($actual >= $ciclof){?>
												<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            									<?php $actual-=1; } ?>
          								</select>   								  
									</th>
								</tr>
    							<tr>
									<th ><label style="margin: 2px; font-size: 14px" for="obsd">Estatus <input size="2%" type="hidden"  name="est" id="est" readonly="true" ></label>
									<th colspan="5" style="font-size: 11px; background-color: white">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="rbLugar" type="radio" value="0" onclick="radios(1,0)" />Depósito
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="rbLugar" type="radio" value="1" onclick="radios(1,1)" />Cargo									
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="rbLugar" type="radio" value="2" onclick="radios(1,2)" />Descuento
										&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>assets/images/camaron.gif" width="20" height="20" border="0"><input size="2%"  type="hidden" name="descam" id="descam" readonly="true" >
										&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="rbLugar2" type="radio" value="3" onclick="radios(2,3)" />Si									
										&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="rbLugar2" type="radio" value="4" onclick="radios(2,4)" />No
									</th>	
								</tr>
								<tr>									
									<th ><label style="margin: 2px; font-size: 14px" for="obsd">Observaciones</label>
									<th colspan="5" style="font-size: 11px; background-color: white"><input size="100%" type="text" name="obsd" id="obsd" value="" style="text-align: left;"></th>									
								</tr>
    							<tfoot>									
									<tr>									
										<th style=" text-align: right; margin: 2px; font-size: 14px;" colspan="6">Procesar Datos en calidad de Cancelación de Pago 
											<select name="rblugar1" id="rblugar1" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
      											<option style="color: blue" value="0" > No </option>
      											<option style="color: red" value="-1"  > Si </option>      									
    										</select>					
    									</th>
									</tr>
								</tfoot>					
    					</table>
					</div>   
					<div id='ver_mas_depot' class='hide' style="height: 253px;" align="" >
						<div style="color: blue">DEPÓSITOS Y CARGOS</div>
       					<table style="width: 875px;" border="2px"  class="tablesorter">
							<thead >
								<th colspan="5" height="15px" style="font-size: 20px">Depósitos, Cargos y Descuentos Ciclo [ <input style="border: none; background: none" size="3%" readonly="true"  name="ciclod" id="ciclod"/> ]
								<th colspan="2">
									<center>
										<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(4)" />
									</center>	
								</th>								
								</th>																	
							</thead>
							<tfoot>
								<tr>									
									<th style=" text-align: center; margin: 2px; font-size: 14px;" colspan="7"> 						
										<?php if($usuario=="Jesus Benítez" || $usuario=="Lety Lizárraga" || $usuario=="Efrain Lizárraga"){ ?>
										<input style="font-size: 14px" type="submit" id="cancelarrp" name="cancelarrp" value="Cancelar por Pagos" onclick="canresremi(-1,1)"/>										
										<?php }?>				
    								</th>
								</tr>
							</tfoot>
						</table>
					</div>   
					<div id='ver_mas_salini' class='hide' style="height: 253px" >	
       			<div style="color: blue">DEPÓSITOS Y CARGOS</div>
       			<table style="width: 875px;" border="2px"  class="tablesorter">
					<thead >
						<th colspan="5" height="15px" style="font-size: 20px">Datos de Saldo Inicial
							<input size="8%" type="hidden" name="idsi" id="idsi"/>								
							<!--<input size="8%" name="numclisi" id="numclisi"/>-->
						</th>
						<th colspan="2">	
							<?php if($usuario=="Jesus Benítez" || $usuario=="Lety Lizárraga" || $usuario=="Efrain Lizárraga"){ ?>
								<input style="font-size: 14px" type="submit" id="aceptarsi" name="aceptarsi" value="Guardar" />
							<?php }?>
							<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(5)" />
						</th>
					</thead>
					<tr>
						<th colspan="5"><label style="margin: 2px; font-size: 14px" for="can">Cantidad</label></th>
						<th colspan="2" style="font-size: 14px; background-color: white"><input size="14%" type="text" name="cansi" id="cansi" value="" readonly="true" style="text-align: right; border: 0"></th>										
					</tr>
    				
					<tfoot>						
						<tr>									
							<th style=" text-align: right; margin: 2px; font-size: 14px;" colspan="7">Procesar Datos en calidad de Cancelación de Pago 						
								<select name="rblugarsi" id="rblugarsi" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
      								<option style="color: blue" value="0" > No </option>
      								<option style="color: red" value="-1"  > Si </option>      									
    							</select>					
    						</th>
						</tr>						
					</tfoot>
				</table>
			</div>                   
             </div>  
       </div>
        
        <h3 align="left"><a href="" >Agregar Depósito, Cargo o Descuento</a></h3>
       	<div style="height:150px; "> 
       		
       			<table style="width: 730px;" border="2px"  class="tablesorter">
							<thead >
								<th colspan="6" height="15px" style="font-size: 20px">Ingrese los Datos
								<input size="8%" type="hidden" name="numclida" id="numclida"/>
								</th>
							</thead>
								<tr>
									<th><label style="margin: 2px; font-size: 14px" for="nombreda">Razón Social</label></th>
									<th colspan="5" style="font-size: 14px; background-color: white"><input style="border: 0" readonly="true" size="100%"  type="text" name="razdda" id="razdda" ></th>
								</tr>
								<tr><th><label style="margin: 2px; font-size: 14px" for="txtFechada">Fecha</label></th>
									<th><label style="margin: 2px; font-size: 14px" for="mnda">Pesos $</label></th>
									<th><label style="margin: 2px; font-size: 14px;" for="usdda" >Dólares $</label></th>	
									<th><label style="margin: 2px; font-size: 14px" for="tcda">Tipo de Cambio</label></th>
									<th><label style="margin: 2px; font-size: 14px" for="ctada">Cuenta</label></th>																	
									<th><label style="margin: 2px; font-size: 14px" for="lstCicloda">Aplicar a Ciclo</label></th>
								</tr>
    							<tr style="font-size: 14px; background-color: white;">
										
									<th><input size="14%" type="text" name="txtFechada" id="txtFechada" class="fecha redondo mUp" style="text-align: center;" ></th>								
									<th><input size="14%" type="text" name="mnda" id="mnda" value="" style="text-align: right; margin-left: 10px"></th>
									<th><input size="14%" type="text" name="usdda" id="usdda" value="" style="text-align: right; margin-left: 10px"></th>									
									<th><input size="8%" type="text" name="tcda" id="tcda" style="text-align: right; margin-left: 10px"></th>
									<th><input size="10%" type="text" name="ctada" id="ctada" value="" style="text-align: center; margin-left: 10px"></th>
									<th style="font-size: 13px;padding-bottom: 1px;">									  																	
									    <select name="lstCicloda" id="lstCicloda" style="font-size: 10px; margin-top: 1px; margin-left: 12px; height: 23px;" >
											<?php $ciclof=2007; $actual=date("Y"); 
											$mes=date("m");
											if($mes>=11) $actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            									<?php $actual-=1; } ?>
          								</select>   								  
									</th>
								</tr>
    							<tr>
									<th ><label style="margin: 2px; font-size: 14px" for="estda">Estatus <input size="2%" type="hidden"  name="estda" id="estda" readonly="true" ></label>
									<th colspan="5" style="font-size: 11px; background-color: white">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="rbLugarda" type="radio" value="0" onclick="radiosda(1,0)" />Depósito
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="rbLugarda" type="radio" value="1" onclick="radiosda(1,1)" />Cargo									
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="rbLugarda" type="radio" value="2" onclick="radiosda(1,2)" />Descuento
										&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>assets/images/camaron.gif" width="20" height="20" border="0"><input size="2%"  type="hidden" name="descamda" id="descamda" readonly="true" >
										&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="rbLugar2da" type="radio" value="3" onclick="radiosda(2,3)" />Si									
										&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Lety Lizárraga"){ ?> disabled="true" <?php } ?> name="rbLugar2da" type="radio" value="4" onclick="radiosda(2,4)" />No
									</th>	
								</tr>
								<tr>									
									<th ><label style="margin: 2px; font-size: 14px" for="obsdda">Observaciones</label>
									<th colspan="5" style="font-size: 11px; background-color: white"><input size="100%" type="text" name="obsdda" id="obsdda" value="" style="text-align: left;"></th>									
								</tr>
    							<tfoot>	
    								<th colspan="6">
										<center>
											<?php if($usuario=="Jesus Benítez" || $usuario=="Lety Lizárraga" ){ ?>
											<input style="font-size: 14px" type="submit" id="aceptardcd" name="aceptardcd" value="Guardar" />									
											<?php }?>		
											<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(6)" />					
										</center>	
									</th>																	
								</tfoot>					
    				</table>
       		
       					 	
	 	</div> 	
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript" >

$("#aceptardcd").click( function(){	
	nombre=$("#nombre").val();
	fec=$("#txtFechada").val();
	mn=$("#mnda").val();
	usd=$("#usdda").val();
	//numero=$("#idda").val();
	if(mn==""){ mn=0; }
	if(usd==""){ usd=0; }
	if(nombre!=''){
		if(fec!=''){
		  if(usd!=0){	
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/saldos/agregardcd", 
						data: "&fec="+$("#txtFechada").val()+"&usd="+$("#usdda").val()+"&tc="+$("#tcda").val()+"&cta="+$("#ctada").val()+"&obs="+$("#obsdda").val()+"&mn="+$("#mnda").val()+"&nrc="+$("#numclida").val()+"&ciclo="+$("#lstCicloda").val()+"&est="+$("#estda").val()+"&descam="+$("#descamda").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Registro realizado correctamente");
										$("#txtFechada").val('');$("#mnda").val('');$("#usdda").val('');$("#tcda").val('');$("#ctada").val('');
										$("#obsdda").val('');$("#lstCicloda").val('');
										$("#cmbCancelacion").val(-1);$("#mytablaRem").trigger("update");$("#mytablaDep").trigger("update");
										$("#accordion").accordion( "activate",0 );
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}else{
		alert("Error: Necesita registrar cantidad en Dólares");
		$("#usdda").focus();
		return false;
		}			
		}else{
			alert("Error: Fecha no válida");	
			$("#txtFechada").focus();
				return false;
		}
	}else{
		alert("Error: Nombre de Cliente no válido...Seleccione Cliente");
		$("#accordion").accordion( "activate",0 );
		$("#nombre").focus();
			return false;
	}
	
	 // return false;
});
function canresremi(val,solodep){	
	$.ajax({
			type: "POST",//Envio
			url:'<?php echo base_url()?>index.php/saldos/remidepo',
			data: "cliente="+$("#numcli").val()+"&tabla="+$("#tabla").val()+"&ciclo="+$("#ciclo").val()+"&valor="+val+"&solodep="+solodep,
			success:function(msg){												
						$("#cmbCancelacion").val(-1);$("#mytablaRem").trigger("update");$("#mytablaDep").trigger("update");												
			}
	});	
	$(this).removeClass('used');
   	if(solodep==0){
   		$('#ver_mas_remit').hide();
   		$('#ver_mas_remi').show();
   	}else{
   		$('#ver_mas_depot').hide();
   		$('#ver_mas_depo').show();
   }	
   $("#tabla").val('');$("#ciclo").val('');	   
}

function cerrar(sel){
	$(this).removeClass('used');
	if(sel==1){
		$('#ver_mas_fac').hide();	
    	$('#ver_mas_remi').show();
	}
	if(sel==2){
		$('#ver_mas_dep').hide();	
    	$('#ver_mas_depo').show();
	}
	if(sel==3){
		$('#ver_mas_remit').hide();	
    	$('#ver_mas_remi').show();
	}
	if(sel==4){
		$('#ver_mas_depot').hide();	
    	$('#ver_mas_depo').show();
	}
	if(sel==5){
		$('#ver_mas_salini').hide();	
    	$('#ver_mas_depo').show();
	}
	if(sel==6){
		$("#accordion").accordion( "activate",0 );
	}
}
function radios(radio,dato){
	if(radio==1) { $("#est").val(dato);
		if(dato==0 || dato==1) { 
			$("#descam").val(4);
			$('input:radio[name=rbLugar2]:nth(1)').attr('checked',true); 
		}
	}
	if(radio==2) { $("#descam").val(dato);
		if(dato==3) { 
			$("#est").val(2);
			$('input:radio[name=rbLugar]:nth(2)').attr('checked',true); 
		}
	}
}
function radiosda(radio,dato){
	if(radio==1) { $("#estda").val(dato);
		if(dato==0 || dato==1) { 
			$("#descamda").val(4);
			$('input:radio[name=rbLugar2da]:nth(1)').attr('checked',true); 
		}
	}
	if(radio==2) { $("#descamda").val(dato);
		if(dato==3) { 
			$("#estda").val(2);
			$('input:radio[name=rbLugarda]:nth(2)').attr('checked',true); 
		}
	}
}
$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#txtFechad").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#txtFechada").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  
function ponerdatos(datos){
	var obj=eval(datos);
	$("#letras").val('('+obj.canlet+')');
}
$('#nombre').click(function(){
    		$('#ver_mas_ajaxsorter').show();
});
$('#btn_mas_ajaxsorter').click(function(){
    	if($(this).hasClass('used')){
    		$(this).removeClass('used');	
    		$('#ver_mas_ajaxsorter').show();
    		$(this).text('Cerrar');
    	}    			
		else{
			$(this).addClass('used');
			$('#ver_mas_ajaxsorter').hide();
			$(this).text('Buscar');
		}
    });

$('#btn_mas_fac').click(function(){
    	if($(this).hasClass('used')){
    		$(this).removeClass('used');	
    		$('#ver_mas_fac').show();
    		$(this).text('Cerrar');
    	}    			
		else{
			$(this).addClass('used');
			$('#ver_mas_fac').hide();
			$(this).text('Buscar');
		}
    });
$('#btn_mas_dep').click(function(){
    	if($(this).hasClass('used')){
    		$(this).removeClass('used');	
    		$('#ver_mas_dep').show();
    		$(this).text('Cerrar');
    	}    			
		else{
			$(this).addClass('used');
			$('#ver_mas_dep').hide();
			$(this).text('Buscar');
		}
    });
$("#aceptar").click( function(){	
	//nombre=$("#nombre").val();
	fec=$("#txtFecha").val();
	//fol=$("#fol").val();
	//if(fol==""){ fol="SN"; }
	numero=$("#id").val();
	//if(nombre!=''){
		if( fec!=''){
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/saldos/actualizarR", 
						data: "id="+$("#id").val()+"&fec="+$("#txtFecha").val()+"&cli="+$("#numcli").val()+"&can="+$("#can").val()+"&pre="+$("#pre").val()+"&avi="+$("#avi").val()+"&dco="+$("#dcor").val()+"&obs="+$("#obsr").val()+"&tipo="+$("#lstProducto").val()+"&est="+$("#estatus").val()+"&rec="+$("#rblugar").val()+"&tabla="+$("#tabla").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#cmbCancelacion").val(-1);$("#mytablaRem").trigger("update");$("#mytablaDep").trigger("update");	
										$(this).removeClass('used');
   										$('#ver_mas_fac').hide();
   										$('#ver_mas_remi').show();
   											
   										$("#tabla").val('');$("#ciclo").val('');
										//$("#accordion").accordion( "activate",0 );																													
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}			
		}else{
			alert("Error: Fecha no valido");	
			$("#txtFecha").focus();
				return false;
		}
	/*}else{
		alert("Error: Nombre de Cliente no valido");
		$("#nombre").focus();
			return false;
	}*/
});

$("#aceptard").click( function(){	
	//nombre=$("#razd").val();
	fec=$("#txtFechad").val();
	mn=$("#mn").val();
	usd=$("#usd").val();
	numero=$("#idd").val();
	if(mn==""){ mn=0; }
	if(usd==""){ usd=0; }
	//if(nombre!=''){
		if( fec!=''){
		  if(mn!=0 || usd!=0){	
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/saldos/actualizarD", 
						data: "id="+$("#idd").val()+"&fec="+$("#txtFechad").val()+"&usd="+$("#usd").val()+"&tc="+$("#tc").val()+"&cta="+$("#cta").val()+"&obs="+$("#obsd").val()+"&mn="+$("#mn").val()+"&nrc="+$("#numcli").val()+"&ciclo="+$("#lstCiclo").val()+"&est="+$("#est").val()+"&descam="+$("#descam").val()+"&cancelar="+$("#rblugar1").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos de Depósito actualizados correctamente");
										$("#cmbCancelacion").val(-1);$("#mytablaRem").trigger("update");$("#mytablaDep").trigger("update");	
										$(this).removeClass('used');
   										$('#ver_mas_dep').hide();
   										$('#ver_mas_depo').show();
   										$("#tabla").val('');$("#ciclo").val('');
										//$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}
			}else{
					alert("Error: Necesita registrar cantidad en Pesos o Dólares");
					$("#mn").focus();
					return false;
			}			
		}else{
				alert("Error: Fecha no válida");	
				$("#txtFechad").focus();
				return false;
		}
	/*}else{
			alert("Error: Nombre de Cliente no válido");
			$("#razd").focus();
			return false;
	}*/	
});
$("#borrard").click( function(){	
	numero=$("#idd").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/saldos/borrarD", 
						data: "id="+$("#idd").val()+"&usd="+$("#usd").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Depósito Eliminado correctamente");
										$("#cmbCancelacion").val(-1);$("#mytablaRem").trigger("update");$("#mytablaDep").trigger("update");	
										$(this).removeClass('used');
   										$('#ver_mas_dep').hide();
   										$('#ver_mas_depo').show();
   										$("#tabla").val('');$("#ciclo").val('');
										//$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Depósito para poder Eliminarlo");
		$("#accordion").accordion( "activate",0 );
		return false;
	}	
});

$("#aceptarsi").click( function(){	
	numero=$("#idsi").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/saldos/actualizarSI", 
						data: "id="+$("#idsi").val()+"&cancelar="+$("#rblugarsi").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Actualizado correctamente");
										$("#cmbCancelacion").val(-1);$("#mytablaRem").trigger("update");$("#mytablaDep").trigger("update");	
										$("#tabla").val('');$("#ciclo").val('');
										$('#ver_mas_salini').hide();	
    									$('#ver_mas_depo').show();
										//$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar para poder Actualizar");
		$("#accordion").accordion( "activate",0 );
		return false;
	}	
});
$("#tablaremisiones").change( function(){		
	$("#tablaremisiones1").val($("#tablaremisiones").val());
 return true;
});
$(document).ready(function(){ 
	            
    $("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/clientes/tabla',  
		multipleFilter:true, 
   		//onLoad:false,
   		sort:false,	
		onRowClick:function(){
			$('#ver_mas_ajaxsorter').hide();
			$('#ver_mas_remi').show();
			$('#ver_mas_depo').show();
			numero=$(this).data('numero');			
			if(numero>'0'){
				$("#numcli").val($(this).data('numero'));
				$("#numclida").val($(this).data('numero'));
				$("#nombre").val($(this).data('razon'));
				$("#razdda").val($("#nombre").val());
				$("#cli").val($(this).data('razon'));
				$("#dom").val($(this).data('dom'));
				$("#lecp").val($(this).data('loc')+" "+$(this).data('edo')+" "+$(this).data('cp'));
				$("#tabla_rem").ajaxSorter({
					url:'<?php echo base_url()?>index.php/saldos/tablaremi', 					
					filters:['numcli','cmbCiclo','cmbCancelacion'],
					active:['Cancelacion',-1],
					//onLoad:false,
   					sort:false, 
   					onRowClick:function(){
   						//aqui es nada mas para el ciclo   						
   						//tabla=$(this).data('tabla');ciclo=$(this).data('ciclo');
   						$("#tabla").val($(this).data('tabla'));
   						$("#ciclor").val($(this).data('ciclo'));   						
   						$("#ciclo").val($(this).data('ciclo'));
   						if(($(this).data('cancelacion')!='0') && ($(this).data('cancelacion')!='-1')){
   								$(this).removeClass('used');
   								$('#ver_mas_remi').hide();	
    							$('#ver_mas_fac').hide();
   								$('#ver_mas_salini').hide();
   								$('#ver_mas_remit').show();
   								$('#ver_mas_depo').show();
   								$('#ver_mas_dep').hide();
   								$('#ver_mas_depot').hide();
   							if(($(this).data('remisionr')=='') && ($(this).data('importe')=='$0.00')){
   								/*if(confirm('RESTABLECER -->'+$(this).data('fechar')+': Facturado ['+$(this).data('cantidadrr1')+']  Importe ['+$(this).data('importe')+']')){   									
   									$.ajax({
										type: "POST",//Envio
										url:'<?php // echo base_url()?> //index.php/saldos/remidepo',
								/*		data: "cliente="+$("#numcli").val()+"&tabla="+tabla+"&ciclo="+ciclo+"&valor=0",
										success:function(msg){												
												$("#cmbCancelacion").val(-1);$("#mytablaRem").trigger("update");$("#mytablaDep").trigger("update");												
										}											
									});										
								}*/								
								$('#restablecerrp').show();$('#cancelarrp').hide();
   							}else{
   								/*if(confirm('CANCELAR POR PAGOS -->'+$(this).data('fechar')+': Facturado ['+$(this).data('cantidadrr1')+']  Importe ['+$(this).data('importe')+']')){   									
   									$.ajax({
										type: "POST",//Envio
										url:'<?php // echo base_url()?> //index.php/saldos/remidepo',
								/*		data: "cliente="+$("#numcli").val()+"&tabla="+tabla+"&ciclo="+ciclo+"&valor=-1",
										success:function(msg){												
												$("#cmbCancelacion").val(-1);$("#mytablaRem").trigger("update");$("#mytablaDep").trigger("update");												
										}
									});	
								}*/								
   								$('#cancelarrp').show();$('#restablecerrp').hide();								
   							}
   						}
   						else{
   							//$("#accordion").accordion( "activate",1 );
   								$(this).removeClass('used');
   								$('#ver_mas_remi').hide();	
    							$('#ver_mas_fac').show();
   								$('#ver_mas_dep').hide();
   								$('#ver_mas_depo').show();
   								$('#ver_mas_salini').hide();
   								$('#ver_mas_depot').hide();
       							$("#id").val($(this).data('numregr'));
       							$("#txtFecha").val($(this).data('fechar'));
       							$("#rem").val($(this).data('remisionr'));
								//$("#raz").val($("#nombre").val());
								$("#can").val($(this).data('cantidadr1'));
								//$("#pre").val($(this).data('precior'));
								$("#pre").val($(this).data('precior').toFixed(2));
								//$("#dco").val($(this).data('precior').toFixed(2));
								if($(this).data('descuentor1')!="SIN")	$("#dcor").val($(this).data('descuentor1'));
								$("#avi").val($(this).data('avisor'));
								//$("#fol").val($(this).data('folio'));
								$("#obsr").val($(this).data('obsr'));
								$("#lstProducto").val($(this).data('tipo'));
								$("#estatus").val($(this).data('estatus'));
								$("#rblugar").val($(this).data('cancelacion'));		
								//$("#sn").val($(this).data('recibida'));
								//$("#numcli").val($(this).data('numclir'));
								$("#tabla").val($(this).data('tabla'));
							}
   					},
   					onSuccess:function(){
						$('#tabla_rem tbody tr').map(function(){
		    				if($(this).data('remisionr')==''){
		    					$(this).css('background','lightblue');	    				    				    			    			
	    					}
	    				})	    				
	    			},
	    						
				});
				
				$("#tabla_dep").ajaxSorter({
					url:'<?php echo base_url()?>index.php/saldos/tabladepo', 					
					filters:['numcli','cmbCiclo','cmbCancelacion'],
					active:['Cancelacion',-1],
					//onLoad:false,
   					sort:false,
   					onRowClick:function(){
   						//aqui es nada mas para el ciclo   						
   						//tabla=''; ciclo=$(this).data('total1');  
   						$("#tabla").val('');
   						$("#ciclod").val($(this).data('total1'));  						
   						$("#ciclo").val($(this).data('total1'));
   						if(($(this).data('cancelacion')!='0') && ($(this).data('cancelacion')!='-1')){
   							if(($(this).data('Total')!='')){
   								/*if(confirm('CANCELAR POR PAGOS --> Depósitos, Cargos y Descuentos del Ciclo: '+$(this).data('total1'))){   									
   									$.ajax({
										type: "POST",//Envio
										url:'<?php // echo base_url()?> //index.php/saldos/remidepo',
										data: "cliente="+$("#numcli").val()+"&tabla="+tabla+"&ciclo="+ciclo+"&valor=-1"+"&solodep=1",
										success:function(msg){												
												$("#cmbCancelacion").val(-1);$("#mytablaRem").trigger("update");$("#mytablaDep").trigger("update");												
										}
									});
								}*/	
								$(this).removeClass('used');
   								$('#ver_mas_depo').hide();	
   								$('#ver_mas_dep').hide();
   								$('#ver_mas_remit').hide();
    							$('#ver_mas_fac').hide();
   								$('#ver_mas_salini').hide();
   								$('#ver_mas_remi').show();
   								$('#ver_mas_depot').show();
   								//$('#ver_mas_depo').show();
															
   							}   							
   						}else{
   							//$("#accordion").accordion( "activate",1 );
   							if(($(this).data('total1')!='Saldo Inicial')){
   								$(this).removeClass('used');
   								$('#ver_mas_depo').hide();
   								$('#ver_mas_fac').hide();	
    							$('#ver_mas_dep').show();
   								$('#ver_mas_remi').show();
   								$('#ver_mas_salini').hide();
   									
    							//$('#ver_mas_dep').show();
       							//$('#ver_mas_fac').hide();
       							$('#ver_mas_salini').hide();
       							$("#idd").val($(this).data('nd'));
       							$("#txtFechad").val($(this).data('fecha'));
       							//$("#zon").val($(this).data('zona'));
								//$("#razd").val($("#nombre").val());
								$("#mn").val($(this).data('pesos'));
								$("#usd").val($(this).data('imported2'));
								if($(this).data('tc')>0) {$("#tc").val($(this).data('tc').toFixed(4));}
								$("#cta").val($(this).data('cuenta'));
								$("#lstCiclo").val($(this).data('aplicar'));
								$("#obsd").val($(this).data('obs'));		
								//$("#numclid").val($(this).data('nrc'));
								$("#rblugar1").val($(this).data('cancelard'));
								$("#descam").val($(this).data('cam'));
								$des=$(this).data('des');$car=$(this).data('car');$cam=$(this).data('cam');
								//can=$(this).data('cancelacion');
								if($cam==-1){ $('input:radio[name=rbLugar2]:nth(0)').attr('checked',true); }else{ $('input:radio[name=rbLugar2]:nth(1)').attr('checked',true); }
								if($des==0 && $car==0){ $('input:radio[name=rbLugar]:nth(0)').attr('checked',true); $("#est").val(0);}
								else{
									if($car==-1){ $('input:radio[name=rbLugar]:nth(1)').attr('checked',true); $("#est").val(1);}
									else{ $('input:radio[name=rbLugar]:nth(2)').attr('checked',true); $("#est").val(2);}
								}
							}else{
								//aqui es el saldo incial
								//$("#accordion").accordion( "activate",1 );
								$(this).removeClass('used');
								$('#ver_mas_fac').hide();
								$('#ver_mas_dep').hide();
								$('#ver_mas_depo').hide();
								$('#ver_mas_salini').show();
								//$("#razsi").val($("#nombre").val());
								$("#cansi").val(($(this).data('importe1')));	
								//$("#numclisi").val(($(this).data('ncr')));
								$("#idsi").val(($(this).data('nsr')));
								$("#rblugarsi").val($(this).data('cancelarsi'));
								
							}								
						}  						
   					},  
   					onSuccess:function(){   						
						$('#tabla_dep tbody tr').map(function(){
		    				if($(this).data('saldo1')!=''){
		    					$("#saldo").val($(this).data('saldo1'));	    				    				    			    			
		    					$("#saldo1").val(($(this).data('saldo2')));		    					
	    					}
	    				})
	    				//se traduce el saldo en letras
	    				$.ajax({
							type: "POST",//Envio
							url:'<?php echo base_url()?>index.php/saldos/letrastras',
							data: "saldo1="+$("#saldo1").val(),
							success:function(msg){
								ponerdatos(msg);
							}								
						});	
						//se actualiza el saldo de cliente
	    				$.ajax({
							type: "POST",//Envio
							url:'<?php echo base_url()?>index.php/saldos/actsaldo',
							data: "saldo1="+$("#saldo1").val()+"&numcli="+$("#numcli").val(),
						});
	    				
	    			},
	    			   			   		
				}); 
	    				
			}
   		},
   		
	});		
});

</script>