<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts/js/modules/exporting.js"></script>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
#imprimir_pdf { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#kpi" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/kpi.png" width="25" height="25" border="0"> KPI </a></li>
		<li><a href="#bombas" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/bomba.png" width="20" height="20" border="0"> Mantenimiento </a></li>
		<li><a href="#puntas" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/bomba.png" width="20" height="20" border="0"> Captación </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 630px" >
		<div id="kpi" class="tab_content" style="margin-top: -15px;" >
		  <div class="ajaxSorterDiv" id="tabla_kpi2" name="tabla_kpi2" style="margin-top: 1px;height: 310px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 450px; text-align: left" >
            		Mes:
   					<select name="cmbMeskpi2" id="cmbMeskpi2" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   						<option value="12" >Diciembre</option>
   						<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   						<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   						<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   						<option value="10" >Octubre</option><option value="11" >Noviembre</option>
   						
   					</select>
   					Departamento:
   					<select name="cmbDepto2" id="cmbDepto2" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">
   						<option value="16">Mantenimiento II</option>
   					</select>		
   					<select name="cmbCiclo2" id="cmbCiclo2" style="font-size: 10px; height: 22px; visibility: hidden">
                    	<option value="17">2017</option>
                    </select> 	
                   </ul>
 				</div>  
             	<span class="ajaxTable" style="height: 265px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablakpi2" name="mytablakpi2" class="ajaxSorter" border="1" >
                       	<thead >
                       		<th data-order = "nom">Evaluación</th>
                       		<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
                       	</thead>
                       	<tbody style="font-size: 9px">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>	
 			<div name="agualab2" id="agualab2" style="min-width: 900px; height: 310px; margin: 0 auto"></div>						    
 		</div>
			
	<div id="bombas" class="tab_content" style="margin-top: -5px" >
			<div style="height: 550px;">	
				<table style="margin-top: -5px; font-size: 12px" border="2px">
					<tbody style="background-color: #F7F7F7">
						<tr style="background-color: lightblue"> 
									<th colspan="2" >Estación</th>
									<th rowspan="30"  style="width: 10px;background-color: white"></th>
									<th colspan="12" style="font-size: 13px; width: 250px; text-align: center">I.- Tratamiento de Agua A 
										<input size="2%"  name="idje" id="idje" readonly="true" type="hidden" >
										<input size="2%" type="hidden" name="tablaje" id="tablaje" readonly="true" value="aguaa" >
									</th>
									
									<th colspan="8" style="font-size: 13px; width: 250px; text-align: center">II.- Tratamiento de Agua B
										<input size="2%" name="idma" id="idma" readonly="true" type="hidden" >
										<input size="2%" type="hidden" name="tablama" id="tablama" readonly="true" value="aguab" >
									</th>
									<th rowspan="30"  style="width: 10px;background-color: white"></th>
									<th colspan="8" style="font-size: 13px; width: 250px; text-align: center">III.- Blowers
										<input size="2%" name="idze" id="idze" readonly="true" type="hidden" >
										<input size="2%" type="hidden" name="tablaze" id="tablaze" readonly="true" value="blw2" >
										
										<div id='ver_mas_pilax' class='hide'  >
										<div class="ajaxSorterDiv" id="tabla_je" name="tabla_je"  >                
            								<span class="ajaxTable" style="height: 0px;" >
                   								<table id="mytablaje" name="mytablaje" class="ajaxSorter" style="" >
                       								<tbody style="font-size: 11px">
                       								</tbody>                        	
                   								</table>
            								</span>          
            							</div>
            							<div class="ajaxSorterDiv" id="tabla_ma" name="tabla_ma"  >                
            								<span class="ajaxTable" style="height: 0px;" >
                   								<table id="mytablama" name="mytablama" class="ajaxSorter" style="" >
                       								<tbody style="font-size: 11px">
                       								</tbody>                        	
                   								</table>
            								</span>          
            							</div>
            							<div class="ajaxSorterDiv" id="tabla_ze" name="tabla_ze"  >                
            								<span class="ajaxTable" style="height: 0px;" >
                   								<table id="mytablaze" name="mytablaze" class="ajaxSorter" style="" >
                       								<tbody style="font-size: 11px">
                       								</tbody>                        	
                   								</table>
            								</span>          
            							</div>
            						</div>
										
									</th>
									
							</tr>
							<tr>
									<td rowspan="3" colspan="2"  >Puntas</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je1" id="je1" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje1" type="radio" value="1" onclick="radios2(1,1)" /></td>
									<td style="background-color: orange"><input  name="rje1" type="radio" value="0" onclick="radios2(1,0)" /></td>									
									<td style="background-color: red"><input  name="rje1" type="radio" value="-1" onclick="radios2(1,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >4<input size="2%" type="hidden" name="je4" id="je4" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje4" type="radio" value="1" onclick="radios2(2,1)" /></td>
									<td style="background-color: orange"><input  name="rje4" type="radio" value="0" onclick="radios2(2,0)" /></td>									
									<td style="background-color: red"><input  name="rje4" type="radio" value="-1" onclick="radios2(2,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >7<input size="2%" type="hidden" name="je7" id="je7" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje7" type="radio" value="1" onclick="radios2(3,1)" /></td>
									<td style="background-color: orange"><input  name="rje7" type="radio" value="0" onclick="radios2(3,0)" /></td>									
									<td style="background-color: red"><input  name="rje7" type="radio" value="-1" onclick="radios2(3,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ma1" id="ma1" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma1" type="radio" value="1" onclick="radios2(4,1)" /></td>
									<td style="background-color: orange"><input  name="rma1" type="radio" value="0" onclick="radios2(4,0)" /></td>									
									<td style="background-color: red"><input  name="rma1" type="radio" value="-1" onclick="radios2(4,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >4<input size="2%" type="hidden" name="ma4" id="ma4" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma4" type="radio" value="1" onclick="radios2(5,1)" /></td>
									<td style="background-color: orange"><input  name="rma4" type="radio" value="0" onclick="radios2(5,0)" /></td>									
									<td style="background-color: red"><input  name="rma4" type="radio" value="-1" onclick="radios2(5,-1)" /></td>
									<td colspan="4">Salas</td>
									<td style="background-color: lightgray;font-size: 10px;" >1500<input size="2%" type="hidden" name="ze5" id="ze5" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze5" type="radio" value="1" onclick="radios2(6,1)" /></td>
									<td style="background-color: orange"><input  name="rze5" type="radio" value="0" onclick="radios2(6,0)" /></td>									
									<td style="background-color: red"><input  name="rze5" type="radio" value="-1" onclick="radios2(6,-1)" /></td>
							</tr>							
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je2" id="je2" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje2" type="radio" value="1" onclick="radios2(7,1)" /></td>
									<td style="background-color: orange"><input  name="rje2" type="radio" value="0" onclick="radios2(7,0)" /></td>									
									<td style="background-color: red"><input  name="rje2" type="radio" value="-1" onclick="radios2(7,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >5<input size="2%" type="hidden" name="je5" id="je5" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje5" type="radio" value="1" onclick="radios2(8,1)" /></td>
									<td style="background-color: orange"><input  name="rje5" type="radio" value="0" onclick="radios2(8,0)" /></td>									
									<td style="background-color: red"><input  name="rje5" type="radio" value="-1" onclick="radios2(8,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >8<input size="2%" type="hidden" name="je8" id="je8" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje8" type="radio" value="1" onclick="radios2(9,1)" /></td>
									<td style="background-color: orange"><input  name="rje8" type="radio" value="0" onclick="radios2(9,0)" /></td>									
									<td style="background-color: red"><input  name="rje8" type="radio" value="-1" onclick="radios2(9,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="ma2" id="ma2" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma2" type="radio" value="1" onclick="radios2(10,1)" /></td>
									<td style="background-color: orange"><input  name="rma2" type="radio" value="0" onclick="radios2(10,0)" /></td>									
									<td style="background-color: red"><input  name="rma2" type="radio" value="-1" onclick="radios2(10,-1)" /></td>
									<td colspan="4"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1100<input size="2%" type="hidden" name="ze1" id="ze1" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze1" type="radio" value="1" onclick="radios2(11,1)" /></td>
									<td style="background-color: orange"><input  name="rze1" type="radio" value="0" onclick="radios2(11,0)" /></td>									
									<td style="background-color: red"><input  name="rze1" type="radio" value="-1" onclick="radios2(11,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1600<input size="2%" type="hidden" name="ze6" id="ze6" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze6" type="radio" value="1" onclick="radios2(12,1)" /></td>
									<td style="background-color: orange"><input  name="rze6" type="radio" value="0" onclick="radios2(12,0)" /></td>									
									<td style="background-color: red"><input  name="rze6" type="radio" value="-1" onclick="radios2(12,-1)" /></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >3<input size="2%" type="hidden" name="je3" id="je3" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje3" type="radio" value="1" onclick="radios2(13,1)" /></td>
									<td style="background-color: orange"><input  name="rje3" type="radio" value="0" onclick="radios2(13,0)" /></td>									
									<td style="background-color: red"><input  name="rje3" type="radio" value="-1" onclick="radios2(13,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >6<input size="2%" type="hidden" name="je6" id="je6" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje6" type="radio" value="1" onclick="radios2(14,1)" /></td>
									<td style="background-color: orange"><input  name="rje6" type="radio" value="0" onclick="radios2(14,0)" /></td>									
									<td style="background-color: red"><input  name="rje6" type="radio" value="-1" onclick="radios2(14,-1)" /></td>
									<td colspan="4"></td>
									<td style="background-color: lightgray;font-size: 10px;" >3<input size="2%" type="hidden" name="ma3" id="ma3" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma3" type="radio" value="1" onclick="radios2(15,1)" /></td>
									<td style="background-color: orange"><input  name="rma3" type="radio" value="0" onclick="radios2(15,0)" /></td>									
									<td style="background-color: red"><input  name="rma3" type="radio" value="-1" onclick="radios2(15,-1)" /></td>
									<td colspan="4"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1200<input size="2%" type="hidden" name="ze2" id="ze2" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze2" type="radio" value="1" onclick="radios2(16,1)" /></td>
									<td style="background-color: orange"><input  name="rze2" type="radio" value="0" onclick="radios2(16,0)" /></td>									
									<td style="background-color: red"><input  name="rze2" type="radio" value="-1" onclick="radios2(16,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1700<input size="2%" type="hidden" name="ze7" id="ze7" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze7" type="radio" value="1" onclick="radios2(17,1)" /></td>
									<td style="background-color: orange"><input  name="rze7" type="radio" value="0" onclick="radios2(17,0)" /></td>									
									<td style="background-color: red"><input  name="rze7" type="radio" value="-1" onclick="radios2(17,-1)" /></td>
							</tr>
							<tr>
									<th colspan="2"> </th>
									<td colspan="8">Maduración</td>
									<td colspan="4">Larvicultura</td>
									<td colspan="8"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1300<input size="2%" type="hidden" name="ze3" id="ze3" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze3" type="radio" value="1" onclick="radios2(18,1)" /></td>
									<td style="background-color: orange"><input  name="rze3" type="radio" value="0" onclick="radios2(18,0)" /></td>									
									<td style="background-color: red"><input  name="rze3" type="radio" value="-1" onclick="radios2(18,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1800<input size="2%" type="hidden" name="ze8" id="ze8" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze8" type="radio" value="1" onclick="radios2(19,1)" /></td>
									<td style="background-color: orange"><input  name="rze8" type="radio" value="0" onclick="radios2(19,0)" /></td>									
									<td style="background-color: red"><input  name="rze8" type="radio" value="-1" onclick="radios2(19,-1)" /></td>
							</tr>
							<tr>
									<th rowspan="2" colspan="2">Bombas</th>
									<td rowspan="2" colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je9" id="je9" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje9" type="radio" value="1" onclick="radios2(20,1)" /></td>
									<td style="background-color: orange"><input  name="rje9" type="radio" value="0" onclick="radios2(20,0)" /></td>									
									<td style="background-color: red"><input  name="rje9" type="radio" value="-1" onclick="radios2(20,-1)" /></td>
									<td rowspan="2" colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je10" id="je10" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje10" type="radio" value="1" onclick="radios2(21,1)" /></td>
									<td style="background-color: orange"><input  name="rje10" type="radio" value="0" onclick="radios2(21,0)" /></td>									
									<td style="background-color: red"><input  name="rje10" type="radio" value="-1" onclick="radios2(21,-1)" /></td>
									<td rowspan="2" colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ma5" id="ma5" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma5" type="radio" value="1" onclick="radios2(22,1)" /></td>
									<td style="background-color: orange"><input  name="rma5" type="radio" value="0" onclick="radios2(22,0)" /></td>									
									<td style="background-color: red"><input  name="rma5" type="radio" value="-1" onclick="radios2(22,-1)" /></td>
									<td rowspan="2" colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1400<input size="2%" type="hidden" name="ze4" id="ze4" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze4" type="radio" value="1" onclick="radios2(23,1)" /></td>
									<td style="background-color: orange"><input  name="rze4" type="radio" value="0" onclick="radios2(23,0)" /></td>									
									<td style="background-color: red"><input  name="rze4" type="radio" value="-1" onclick="radios2(23,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1900<input size="2%" type="hidden" name="ze9" id="ze9" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze9" type="radio" value="1" onclick="radios2(24,1)" /></td>
									<td style="background-color: orange"><input  name="rze9" type="radio" value="0" onclick="radios2(24,0)" /></td>									
									<td style="background-color: red"><input  name="rze9" type="radio" value="-1" onclick="radios2(24,-1)" /></td>
							</tr>
							<tr>
									<td colspan="4"></td>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je11" id="je11" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje11" type="radio" value="1" onclick="radios2(25,1)" /></td>
									<td style="background-color: orange"><input  name="rje11" type="radio" value="0" onclick="radios2(25,0)" /></td>									
									<td style="background-color: red"><input  name="rje11" type="radio" value="-1" onclick="radios2(25,-1)" /></td>
									<td colspan="4"></td>
									<td rowspan="2" colspan="4">Maduración Genética</td>
									<td style="background-color: lightgray;font-size: 10px;" >100<input size="2%" type="hidden" name="ze10" id="ze10" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze10" type="radio" value="1" onclick="radios2(26,1)" /></td>
									<td style="background-color: orange"><input  name="rze10" type="radio" value="0" onclick="radios2(26,0)" /></td>									
									<td style="background-color: red"><input  name="rze10" type="radio" value="-1" onclick="radios2(26,-1)" /></td>
							</tr>
							<tr>
									<td rowspan="6" colspan="2">Filtros</td>
									<td rowspan="6" colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je12" id="je12" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje12" type="radio" value="1" onclick="radios2(27,1)" /></td>
									<td style="background-color: orange"><input  name="rje12" type="radio" value="0" onclick="radios2(27,0)" /></td>									
									<td style="background-color: red"><input  name="rje12" type="radio" value="-1" onclick="radios2(27,-1)" /></td>
									<td rowspan="6" colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je18" id="je18" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje18" type="radio" value="1" onclick="radios2(28,1)" /></td>
									<td style="background-color: orange"><input  name="rje18" type="radio" value="0" onclick="radios2(28,0)" /></td>									
									<td style="background-color: red"><input  name="rje18" type="radio" value="-1" onclick="radios2(28,-1)" /></td>
									<td rowspan="6" colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ma6" id="ma6" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma6" type="radio" value="1" onclick="radios2(29,1)" /></td>
									<td style="background-color: orange"><input  name="rma6" type="radio" value="0" onclick="radios2(29,0)" /></td>									
									<td style="background-color: red"><input  name="rma6" type="radio" value="-1" onclick="radios2(29,-1)" /></td>
									<td rowspan="6" colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >200<input size="2%" type="hidden" name="ze11" id="ze11" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze11" type="radio" value="1" onclick="radios2(30,1)" /></td>
									<td style="background-color: orange"><input  name="rze11" type="radio" value="0" onclick="radios2(30,0)" /></td>									
									<td style="background-color: red"><input  name="rze11" type="radio" value="-1" onclick="radios2(30,-1)" /></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je13" id="je13" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje13" type="radio" value="1" onclick="radios2(31,1)" /></td>
									<td style="background-color: orange"><input  name="rje13" type="radio" value="0" onclick="radios2(31,0)" /></td>									
									<td style="background-color: red"><input  name="rje13" type="radio" value="-1" onclick="radios2(31,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je19" id="je19" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje19" type="radio" value="1" onclick="radios2(32,1)" /></td>
									<td style="background-color: orange"><input  name="rje19" type="radio" value="0" onclick="radios2(32,0)" /></td>									
									<td style="background-color: red"><input  name="rje19" type="radio" value="-1" onclick="radios2(32,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="ma7" id="ma7" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma7" type="radio" value="1" onclick="radios2(33,1)" /></td>
									<td style="background-color: orange"><input  name="rma7" type="radio" value="0" onclick="radios2(33,0)" /></td>									
									<td style="background-color: red"><input  name="rma7" type="radio" value="-1" onclick="radios2(33,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >ResA<input size="2%" type="hidden" name="ze12" id="ze12" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze12" type="radio" value="1" onclick="radios2(34,1)" /></td>
									<td style="background-color: orange"><input  name="rze12" type="radio" value="0" onclick="radios2(34,0)" /></td>									
									<td style="background-color: red"><input  name="rze12" type="radio" value="-1" onclick="radios2(34,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >ResC<input size="2%" type="hidden" name="ze14" id="ze14" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze14" type="radio" value="1" onclick="radios2(35,1)" /></td>
									<td style="background-color: orange"><input  name="rze14" type="radio" value="0" onclick="radios2(35,0)" /></td>									
									<td style="background-color: red"><input  name="rze14" type="radio" value="-1" onclick="radios2(35,-1)" /></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >3<input size="2%" type="hidden" name="je14" id="je14" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje14" type="radio" value="1" onclick="radios2(36,1)" /></td>
									<td style="background-color: orange"><input  name="rje14" type="radio" value="0" onclick="radios2(36,0)" /></td>									
									<td style="background-color: red"><input  name="rje14" type="radio" value="-1" onclick="radios2(36,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >3<input size="2%" type="hidden" name="je20" id="je20" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje20" type="radio" value="1" onclick="radios2(37,1)" /></td>
									<td style="background-color: orange"><input  name="rje20" type="radio" value="0" onclick="radios2(37,0)" /></td>									
									<td style="background-color: red"><input  name="rje20" type="radio" value="-1" onclick="radios2(37,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >3<input size="2%" type="hidden" name="ma8" id="ma8" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma8" type="radio" value="1" onclick="radios2(38,1)" /></td>
									<td style="background-color: orange"><input  name="rma8" type="radio" value="0" onclick="radios2(38,0)" /></td>									
									<td style="background-color: red"><input  name="rma8" type="radio" value="-1" onclick="radios2(38,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >ResB<input size="2%" type="hidden" name="ze13" id="ze13" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze13" type="radio" value="1" onclick="radios2(39,1)" /></td>
									<td style="background-color: orange"><input  name="rze13" type="radio" value="0" onclick="radios2(39,0)" /></td>									
									<td style="background-color: red"><input  name="rze13" type="radio" value="-1" onclick="radios2(39,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >ResD<input size="2%" type="hidden" name="ze15" id="ze15" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze15" type="radio" value="1" onclick="radios2(40,1)" /></td>
									<td style="background-color: orange"><input  name="rze15" type="radio" value="0" onclick="radios2(40,0)" /></td>									
									<td style="background-color: red"><input  name="rze15" type="radio" value="-1" onclick="radios2(40,-1)" /></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >4<input size="2%" type="hidden" name="je15" id="je15" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje15" type="radio" value="1" onclick="radios2(41,1)" /></td>
									<td style="background-color: orange"><input  name="rje15" type="radio" value="0" onclick="radios2(41,0)" /></td>									
									<td style="background-color: red"><input  name="rje15" type="radio" value="-1" onclick="radios2(41,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >4<input size="2%" type="hidden" name="je21" id="je21" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje21" type="radio" value="1" onclick="radios2(42,1)" /></td>
									<td style="background-color: orange"><input  name="rje21" type="radio" value="0" onclick="radios2(42,0)" /></td>									
									<td style="background-color: red"><input  name="rje21" type="radio" value="-1" onclick="radios2(42,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >4<input size="2%" type="hidden" name="ma9" id="ma9" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma9" type="radio" value="1" onclick="radios2(43,1)" /></td>
									<td style="background-color: orange"><input  name="rma9" type="radio" value="0" onclick="radios2(43,0)" /></td>									
									<td style="background-color: red"><input  name="rma9" type="radio" value="-1" onclick="radios2(43,-1)" /></td>
									<td colspan="4">Larv. y Des.Gen</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze16" id="ze16" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze16" type="radio" value="1" onclick="radios2(44,1)" /></td>
									<td style="background-color: orange"><input  name="rze16" type="radio" value="0" onclick="radios2(44,0)" /></td>									
									<td style="background-color: red"><input  name="rze16" type="radio" value="-1" onclick="radios2(44,-1)" /></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >5<input size="2%" type="hidden" name="je16" id="je16" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje16" type="radio" value="1" onclick="radios2(45,1)" /></td>
									<td style="background-color: orange"><input  name="rje16" type="radio" value="0" onclick="radios2(45,0)" /></td>									
									<td style="background-color: red"><input  name="rje16" type="radio" value="-1" onclick="radios2(45,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >5<input size="2%" type="hidden" name="je22" id="je22" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje22" type="radio" value="1" onclick="radios2(46,1)" /></td>
									<td style="background-color: orange"><input  name="rje22" type="radio" value="0" onclick="radios2(46,0)" /></td>									
									<td style="background-color: red"><input  name="rje22" type="radio" value="-1" onclick="radios2(46,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >5<input size="2%" type="hidden" name="ma10" id="ma10" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma10" type="radio" value="1" onclick="radios2(47,1)" /></td>
									<td style="background-color: orange"><input  name="rma10" type="radio" value="0" onclick="radios2(47,0)" /></td>									
									<td style="background-color: red"><input  name="rma10" type="radio" value="-1" onclick="radios2(47,-1)" /></td>
									<td rowspan="2" colspan="4">Maduración</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze17" id="ze17" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze17" type="radio" value="1" onclick="radios2(48,1)" /></td>
									<td style="background-color: orange"><input  name="rze17" type="radio" value="0" onclick="radios2(48,0)" /></td>									
									<td style="background-color: red"><input  name="rze17" type="radio" value="-1" onclick="radios2(48,-1)" /></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >6<input size="2%" type="hidden" name="je17" id="je17" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje17" type="radio" value="1" onclick="radios2(49,1)" /></td>
									<td style="background-color: orange"><input  name="rje17" type="radio" value="0" onclick="radios2(49,0)" /></td>									
									<td style="background-color: red"><input  name="rje17" type="radio" value="-1" onclick="radios2(49,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >6<input size="2%" type="hidden" name="je23" id="je23" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje23" type="radio" value="1" onclick="radios2(50,1)" /></td>
									<td style="background-color: orange"><input  name="rje23" type="radio" value="0" onclick="radios2(50,0)" /></td>									
									<td style="background-color: red"><input  name="rje23" type="radio" value="-1" onclick="radios2(50,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >6<input size="2%" type="hidden" name="ma11" id="ma11" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma11" type="radio" value="1" onclick="radios2(51,1)" /></td>
									<td style="background-color: orange"><input  name="rma11" type="radio" value="0" onclick="radios2(51,0)" /></td>									
									<td style="background-color: red"><input  name="rma11" type="radio" value="-1" onclick="radios2(51,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="ze18" id="ze18" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze18" type="radio" value="1" onclick="radios2(52,1)" /></td>
									<td style="background-color: orange"><input  name="rze18" type="radio" value="0" onclick="radios2(52,0)" /></td>									
									<td style="background-color: red"><input  name="rze18" type="radio" value="-1" onclick="radios2(52,-1)" /></td>
							</tr>
							<tr>
									<td colspan="2">Ozono</td>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je24" id="je24" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje24" type="radio" value="1" onclick="radios2(53,1)" /></td>
									<td style="background-color: orange"><input  name="rje24" type="radio" value="0" onclick="radios2(53,0)" /></td>									
									<td style="background-color: red"><input  name="rje24" type="radio" value="-1" onclick="radios2(53,-1)" /></td>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je25" id="je25" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje25" type="radio" value="1" onclick="radios2(54,1)" /></td>
									<td style="background-color: orange"><input  name="rje25" type="radio" value="0" onclick="radios2(54,0)" /></td>									
									<td style="background-color: red"><input  name="rje25" type="radio" value="-1" onclick="radios2(54,-1)" /></td>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ma12" id="ma12" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rma12" type="radio" value="1" onclick="radios2(55,1)" /></td>
									<td style="background-color: orange"><input  name="rma12" type="radio" value="0" onclick="radios2(55,0)" /></td>									
									<td style="background-color: red"><input  name="rma12" type="radio" value="-1" onclick="radios2(55,-1)" /></td>
									<td colspan="2"></td>
									<td colspan="4">Microalgas</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze19" id="ze19" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze19" type="radio" value="1" onclick="radios2(56,1)" /></td>
									<td style="background-color: orange"><input  name="rze19" type="radio" value="0" onclick="radios2(56,0)" /></td>									
									<td style="background-color: red"><input  name="rze19" type="radio" value="-1" onclick="radios2(56,-1)" /></td>
							</tr>
							<tr>
									<td colspan="2">Reservorio</td>
									<td colspan="4">Producción</td>
									<td colspan="4">Desove</td>
									<td colspan="4">Larvicultura</td>
									<td colspan="4">Microalgas</td>
									<td colspan="4">Mad. Genética</td>
									<td colspan="8" style="font-size: 13px; text-align: center; background-color: lightblue">IV.- Subestación</td>
							</tr>
							<tr>
									<td rowspan="2" colspan="2">Bombas</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je26" id="je26" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje26" type="radio" value="1" onclick="radios2(57,1)" /></td>
									<td style="background-color: orange"><input  name="rje26" type="radio" value="0" onclick="radios2(57,0)" /></td>									
									<td style="background-color: red"><input  name="rje26" type="radio" value="-1" onclick="radios2(57,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je28" id="je28" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje28" type="radio" value="1" onclick="radios2(58,1)" /></td>
									<td style="background-color: orange"><input  name="rje28" type="radio" value="0" onclick="radios2(58,0)" /></td>									
									<td style="background-color: red"><input  name="rje28" type="radio" value="-1" onclick="radios2(58,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je29" id="je29" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje29" type="radio" value="1" onclick="radios2(59,1)" /></td>
									<td style="background-color: orange"><input  name="rje29" type="radio" value="0" onclick="radios2(59,0)" /></td>									
									<td style="background-color: red"><input  name="rje29" type="radio" value="-1" onclick="radios2(59,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je31" id="je31" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje31" type="radio" value="1" onclick="radios2(60,1)" /></td>
									<td style="background-color: orange"><input  name="rje31" type="radio" value="0" onclick="radios2(60,0)" /></td>									
									<td style="background-color: red"><input  name="rje31" type="radio" value="-1" onclick="radios2(60,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je32" id="je32" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje32" type="radio" value="1" onclick="radios2(61,1)" /></td>
									<td style="background-color: orange"><input  name="rje32" type="radio" value="0" onclick="radios2(61,0)" /></td>									
									<td style="background-color: red"><input  name="rje32" type="radio" value="-1" onclick="radios2(61,-1)" /></td>
									<td colspan="4">A CFE</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze20" id="ze20" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze20" type="radio" value="1" onclick="radios2(62,1)" /></td>
									<td style="background-color: orange"><input  name="rze20" type="radio" value="0" onclick="radios2(62,0)" /></td>									
									<td style="background-color: red"><input  name="rze20" type="radio" value="-1" onclick="radios2(62,-1)" /></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je27" id="je27" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje27" type="radio" value="1" onclick="radios2(63,1)" /></td>
									<td style="background-color: orange"><input  name="rje27" type="radio" value="0" onclick="radios2(63,0)" /></td>									
									<td style="background-color: red"><input name="rje27" type="radio" value="-1" onclick="radios2(63,-1)" /></td>
									<td colspan="4"></td>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je30" id="je30" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje30" type="radio" value="1" onclick="radios2(64,1)" /></td>
									<td style="background-color: orange"><input  name="rje30" type="radio" value="0" onclick="radios2(64,0)" /></td>									
									<td style="background-color: red"><input  name="rje30" type="radio" value="-1" onclick="radios2(64,-1)" /></td>
									<td colspan="4"></td>
									<td colspan="4"></td>
									<td colspan="4">Generador de 125 kva</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze21" id="ze21" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze21" type="radio" value="1" onclick="radios2(65,1)" /></td>
									<td style="background-color: orange"><input  name="rze21" type="radio" value="0" onclick="radios2(65,0)" /></td>									
									<td style="background-color: red"><input  name="rze21" type="radio" value="-1" onclick="radios2(65,-1)" /></td>
							</tr>
							<tr>
									<td rowspan="6" colspan="2">Filtros</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je33" id="je33" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje33" type="radio" value="1" onclick="radios2(66,1)" /></td>
									<td style="background-color: orange"><input  name="rje33" type="radio" value="0" onclick="radios2(66,0)" /></td>									
									<td style="background-color: red"><input  name="rje33" type="radio" value="-1" onclick="radios2(66,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je39" id="je39" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje39" type="radio" value="1" onclick="radios2(67,1)" /></td>
									<td style="background-color: orange"><input  name="rje39" type="radio" value="0" onclick="radios2(67,0)" /></td>									
									<td style="background-color: red"><input  name="rje39" type="radio" value="-1" onclick="radios2(67,-1)" /></td>
									<td rowspan="6" colspan="4"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je41" id="je41" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje41" type="radio" value="1" onclick="radios2(68,1)" /></td>
									<td style="background-color: orange"><input  name="rje41" type="radio" value="0" onclick="radios2(68,0)" /></td>									
									<td style="background-color: red"><input  name="rje41" type="radio" value="-1" onclick="radios2(68,-1)" /></td>
									<td rowspan="6" colspan="4"></td>
									<td colspan="4">Generador de 300 kva</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze22" id="ze22" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze22" type="radio" value="1" onclick="radios2(69,1)" /></td>
									<td style="background-color: orange"><input  name="rze22" type="radio" value="0" onclick="radios2(69,0)" /></td>									
									<td style="background-color: red"><input  name="rze22" type="radio" value="-1" onclick="radios2(69,-1)" /></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je34" id="je34" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje34" type="radio" value="1" onclick="radios2(70,1)" /></td>
									<td style="background-color: orange"><input  name="rje34" type="radio" value="0" onclick="radios2(70,0)" /></td>									
									<td style="background-color: red"><input  name="rje34" type="radio" value="-1" onclick="radios2(70,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je40" id="je40" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje40" type="radio" value="1" onclick="radios2(71,1)" /></td>
									<td style="background-color: orange"><input  name="rje40" type="radio" value="0" onclick="radios2(71,0)" /></td>									
									<td style="background-color: red"><input  name="rje40" type="radio" value="-1" onclick="radios2(71,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je42" id="je42" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje42" type="radio" value="1" onclick="radios2(72,1)" /></td>
									<td style="background-color: orange"><input  name="rje42" type="radio" value="0" onclick="radios2(72,0)" /></td>									
									<td style="background-color: red"><input  name="rje42" type="radio" value="-1" onclick="radio2s(72,-1)" /></td>
									<td colspan="4">B CFE</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze23" id="ze23" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze23" type="radio" value="1" onclick="radios2(73,1)" /></td>
									<td style="background-color: orange"><input  name="rze23" type="radio" value="0" onclick="radios2(73,0)" /></td>									
									<td style="background-color: red"><input  name="rze23" type="radio" value="-1" onclick="radios2(73,-1)" /></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >3<input size="2%" type="hidden" name="je35" id="je35" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje35" type="radio" value="1" onclick="radios2(74,1)" /></td>
									<td style="background-color: orange"><input  name="rje35" type="radio" value="0" onclick="radios2(74,0)" /></td>									
									<td style="background-color: red"><input  name="rje35" type="radio" value="-1" onclick="radios2(74,-1)" /></td>
									<td rowspan="4" colspan="4"></td>
									<td style="background-color: lightgray;font-size: 10px;" >3<input size="2%" type="hidden" name="je43" id="je43" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje43" type="radio" value="1" onclick="radios2(75,1)" /></td>
									<td style="background-color: orange"><input  name="rje43" type="radio" value="0" onclick="radios2(75,0)" /></td>									
									<td style="background-color: red"><input  name="rje43" type="radio" value="-1" onclick="radios2(75,-1)" /></td>
									<td colspan="4">Generador de 187 kva</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze24" id="ze24" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze24" type="radio" value="1" onclick="radios2(76,1)" /></td>
									<td style="background-color: orange"><input  name="rze24" type="radio" value="0" onclick="radios2(76,0)" /></td>									
									<td style="background-color: red"><input  name="rze24" type="radio" value="-1" onclick="radios2(76,-1)" /></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >4<input size="2%" type="hidden" name="je36" id="je36" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje36" type="radio" value="1" onclick="radios2(77,1)" /></td>
									<td style="background-color: orange"><input  name="rje36" type="radio" value="0" onclick="radios2(77,0)" /></td>									
									<td style="background-color: red"><input  name="rje36" type="radio" value="-1" onclick="radios2(77,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >4<input size="2%" type="hidden" name="je44" id="je44" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje44" type="radio" value="1" onclick="radios2(78,1)" /></td>
									<td style="background-color: orange"><input  name="rje44" type="radio" value="0" onclick="radios2(78,0)" /></td>									
									<td style="background-color: red"><input  name="rje44" type="radio" value="-1" onclick="radios2(78,-1)" /></td>
									<td colspan="8" style="font-size: 13px; text-align: center; background-color: lightblue">V.- Sistema Eléctrico</td>								
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >5<input size="2%" type="hidden" name="je37" id="je37" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje37" type="radio" value="1" onclick="radios2(79,1)" /></td>
									<td style="background-color: orange"><input  name="rje37" type="radio" value="0" onclick="radios2(79,0)" /></td>									
									<td style="background-color: red"><input  name="rje37" type="radio" value="-1" onclick="radios2(79,-1)" /></td>
									<td rowspan="2" colspan="4"></td>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze25" id="ze25" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze25" type="radio" value="1" onclick="radios2(80,1)" /></td>
									<td style="background-color: orange"><input  name="rze25" type="radio" value="0" onclick="radios2(80,0)" /></td>									
									<td style="background-color: red"><input  name="rze25" type="radio" value="-1" onclick="radios2(80,-1)" /></td>
									<td colspan="2"></td>
							</tr>
							<tr>
									<td style="background-color: lightgray;font-size: 10px;" >6<input size="2%" type="hidden" name="je38" id="je38" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje38" type="radio" value="1" onclick="radios2(81,1)" /></td>
									<td style="background-color: orange"><input  name="rje38" type="radio" value="0" onclick="radios2(81,0)" /></td>									
									<td style="background-color: red"><input  name="rje38" type="radio" value="-1" onclick="radios2(81,-1)" /></td>
									<td colspan="8" style="font-size: 13px; text-align: center; background-color: lightblue">VI.- Sistema Aireación</td>							
							</tr>
							<tr>
									<td colspan="2">U.V.</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je45" id="je45" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje45" type="radio" value="1" onclick="radios2(82,1)" /></td>
									<td style="background-color: orange"><input  name="rje45" type="radio" value="0" onclick="radios2(82,0)" /></td>									
									<td style="background-color: red"><input  name="rje45" type="radio" value="-1" onclick="radios2(82,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je46" id="je46" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje46" type="radio" value="1" onclick="radios2(83,1)" /></td>
									<td style="background-color: orange"><input  name="rje46" type="radio" value="0" onclick="radios2(83,0)" /></td>									
									<td style="background-color: red"><input  name="rje46" type="radio" value="-1" onclick="radios2(83,-1)" /></td>
									<td colspan="4"></td>
									<td colspan="4"></td>
									<td colspan="4"></td>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze26" id="ze26" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze26" type="radio" value="1" onclick="radios2(84,1)" /></td>
									<td style="background-color: orange"><input  name="rze26" type="radio" value="0" onclick="radios2(84,0)" /></td>									
									<td style="background-color: red"><input  name="rze26" type="radio" value="-1" onclick="radios2(84,-1)" /></td>
									<td colspan="2"></td>									
							</tr>
							<tr>
									<td colspan="2">Intercambiador</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je47" id="je47" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje47" type="radio" value="1" onclick="radios2(85,1)" /></td>
									<td style="background-color: orange"><input  name="rje47" type="radio" value="0" onclick="radios2(85,0)" /></td>									
									<td style="background-color: red"><input  name="rje47" type="radio" value="-1" onclick="radios2(85,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je48" id="je48" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje48" type="radio" value="1" onclick="radios2(86,1)" /></td>
									<td style="background-color: orange"><input  name="rje48" type="radio" value="0" onclick="radios2(86,0)" /></td>									
									<td style="background-color: red"><input  name="rje48" type="radio" value="-1" onclick="radios2(86,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je49" id="je49" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje49" type="radio" value="1" onclick="radios2(87,1)" /></td>
									<td style="background-color: orange"><input  name="rje49" type="radio" value="0" onclick="radios2(87,0)" /></td>									
									<td style="background-color: red"><input  name="rje49" type="radio" value="-1" onclick="radios2(87,-1)" /></td>
									<td colspan="4"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je50" id="je50" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje50" type="radio" value="1" onclick="radios2(88,1)" /></td>
									<td style="background-color: orange"><input  name="rje50" type="radio" value="0" onclick="radios2(88,0)" /></td>									
									<td style="background-color: red"><input  name="rje50" type="radio" value="-1" onclick="radios2(88,-1)" /></td>
									<td colspan="8" style="font-size: 13px; text-align: center; background-color: lightblue">VII.- Sistema Drenaje</td>			
							</tr>
							<tr>
									<td colspan="2">Calderas</td>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je51" id="je51" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje51" type="radio" value="1" onclick="radios2(89,1)" /></td>
									<td style="background-color: orange"><input  name="rje51" type="radio" value="0" onclick="radios2(89,0)" /></td>									
									<td style="background-color: red"><input  name="rje51" type="radio" value="-1" onclick="radios2(89,-1)" /></td>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je52" id="je52" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje52" type="radio" value="1" onclick="radios2(90,1)" /></td>
									<td style="background-color: orange"><input  name="rje52" type="radio" value="0" onclick="radios2(90,0)" /></td>									
									<td style="background-color: red"><input  name="rje52" type="radio" value="-1" onclick="radios2(90,-1)" /></td>
									<td colspan="4"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je53" id="je53" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje53" type="radio" value="1" onclick="radios2(91,1)" /></td>
									<td style="background-color: orange"><input  name="rje53" type="radio" value="0" onclick="radios2(91,0)" /></td>									
									<td style="background-color: red"><input  name="rje53" type="radio" value="-1" onclick="radios2(91,-1)" /></td>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze27" id="ze27" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze27" type="radio" value="1" onclick="radios2(92,1)" /></td>
									<td style="background-color: orange"><input  name="rze27" type="radio" value="0" onclick="radios2(92,0)" /></td>									
									<td style="background-color: red"><input  name="rze27" type="radio" value="-1" onclick="radios2(92,-1)" /></td>
									<td colspan="2"></td>			
							</tr>
							<tr>
									<td colspan="2">U.V.</td>
									<td colspan="4"></td>
									<td colspan="4"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je54" id="je54" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje54" type="radio" value="1" onclick="radios2(93,1)" /></td>
									<td style="background-color: orange"><input  name="rje54" type="radio" value="0" onclick="radios2(93,0)" /></td>									
									<td style="background-color: red"><input  name="rje54" type="radio" value="-1" onclick="radios2(93,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je55" id="je55" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje55" type="radio" value="1" onclick="radios2(94,1)" /></td>
									<td style="background-color: orange"><input  name="rje55" type="radio" value="0" onclick="radios2(94,0)" /></td>									
									<td style="background-color: red"><input  name="rje55" type="radio" value="-1" onclick="radios2(94,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je56" id="je56" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje56" type="radio" value="1" onclick="radios2(95,1)" /></td>
									<td style="background-color: orange"><input  name="rje56" type="radio" value="0" onclick="radios2(95,0)" /></td>									
									<td style="background-color: red"><input  name="rje56" type="radio" value="-1" onclick="radios2(95,-1)" /></td>
									<td colspan="8" style="font-size: 13px; text-align: center; background-color: lightblue">VIII.- Captación de Agua</td>
							</tr>
							<tr>
									<td rowspan="2" colspan="2">Extractores</td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je57" id="je57" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje57" type="radio" value="1" onclick="radios2(96,1)" /></td>
									<td style="background-color: orange"><input  name="rje57" type="radio" value="0" onclick="radios2(96,0)" /></td>									
									<td style="background-color: red"><input  name="rje57" type="radio" value="-1" onclick="radios2(96,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je58" id="je58" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje58" type="radio" value="1" onclick="radios2(97,1)" /></td>
									<td style="background-color: orange"><input  name="rje58" type="radio" value="0" onclick="radios2(97,0)" /></td>									
									<td style="background-color: red"><input  name="rje58" type="radio" value="-1" onclick="radios2(97,-1)" /></td>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je60" id="je60" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje60" type="radio" value="1" onclick="radios2(98,1)" /></td>
									<td style="background-color: orange"><input  name="rje60" type="radio" value="0" onclick="radios2(98,0)" /></td>									
									<td style="background-color: red"><input  name="rje60" type="radio" value="-1" onclick="radios2(98,-1)" /></td>
									<td style="background-color: lightgray;font-size: 10px;" >2<input size="2%" type="hidden" name="je61" id="je61" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje61" type="radio" value="1" onclick="radios2(99,1)" /></td>
									<td style="background-color: orange"><input  name="rje61" type="radio" value="0" onclick="radios2(99,0)" /></td>									
									<td style="background-color: red"><input  name="rje61" type="radio" value="-1" onclick="radios2(99,-1)" /></td>
									<td colspan="2"></td>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="ze28" id="ze28" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rze28" type="radio" value="1" onclick="radios2(100,1)" /></td>
									<td style="background-color: orange"><input  name="rze28" type="radio" value="0" onclick="radios2(100,0)" /></td>									
									<td style="background-color: red"><input  name="rze28" type="radio" value="-1" onclick="radios2(100,-1)" /></td>
									<td colspan="2"></td>		
							</tr>
							<tr>
									<td colspan="2"></td>
									<td style="background-color: lightgray;font-size: 10px;" >1<input size="2%" type="hidden" name="je59" id="je59" readonly="true" ></td>
									<td style="background-color: #0F0"><input checked="true"  name="rje59" type="radio" value="1" onclick="radios2(101,1)" /></td>
									<td style="background-color: orange"><input  name="rje59" type="radio" value="0" onclick="radios2(101,0)" /></td>									
									<td style="background-color: red"><input  name="rje59" type="radio" value="-1" onclick="radios2(101,-1)" /></td>
									<td colspan="2"></td>
									<td colspan="12"></td>
									<td colspan="8"></td>
							</tr>
							<tr style="background-color: lightblue"> 
									<th colspan="2" >
										
										Día: <input size="8%" type="text" name="fecje" id="fecje" class="fecha redondo mUp" readonly="true" style="text-align: center; " >	
										<input type="hidden" size="8%" type="text" name="fecma" id="fecma" class="fecha redondo mUp" readonly="true" style="text-align: center; " >
										<input type="hidden" size="8%" type="text" name="fecblw2" id="fecblw2" class="fecha redondo mUp" readonly="true" style="text-align: center; " >
										<input style="font-size: 12px" type="button" id="his2" name="his2" value="RegRes" />
										
									</th>
        							<th colspan="12" >
										<center>								
										<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
										&nbsp;&nbsp;<input style="font-size: 14px" type="submit" id="borrar" name="borrar" value="Borrar" />
										&nbsp;&nbsp;
										</center>
									</th>	
									<th colspan="8" >
										<center>								
										<input style="font-size: 14px" type="submit" id="aceptar1" name="aceptar1" value="Guardar" />
										&nbsp;&nbsp;<input style="font-size: 14px" type="submit" id="borrar1" name="borrar1" value="Borrar" />
										&nbsp;&nbsp;											
									</center>
									</th>
									<th colspan="8" >
										<center>								
										<input style="font-size: 14px" type="submit" id="aceptarblw" name="aceptarblw" value="Guardar" />
										&nbsp;&nbsp;<input style="font-size: 14px" type="submit" id="borrarblw" name="borrarblw" value="Borrar" />
										&nbsp;&nbsp;														
									</center>
									</th>
							</tr>
							</tbody>  
						<tfoot>
							
						</tfoot>   							
    				</table>  
            </div>
 			
 		</div>
 		<div id="puntas" class="tab_content" style="margin-top: -15px" >
 			<div class="ajaxSorterDiv" id="tabla_puntas2" name="tabla_puntas2" style="margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 450px; text-align: left" >
            		Mes:
   					<select name="cmbMeskpip2" id="cmbMeskpip2" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   						<option value="12" >Diciembre</option>
   						<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   						<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   						<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   						<option value="10" >Octubre</option><option value="11" >Noviembre</option>
   					</select>
   							
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 520px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablapuntas2" name="mytablapuntas2" class="ajaxSorter" border="1" style="text-align: center;" >
                       	<thead >
                       		<tr style="font-size:10px">
                       		<th rowspan="3" data-order = "dia">Día</th>
                       		<th colspan="16" style="text-align: center">Bombeo A</th>
                       		<th>Rango</th>
                       		<th colspan="8" style="text-align: center">Bombeo B</th>
                       		<th>Rango</th>
                       		<th>Rango</th>
                       		<th style="text-align: center">Res</th>
                       		</tr>
                       		<tr style="font-size: 10px">
                       			<th>1</th><th>3hp</th>	
                       			<th>2</th><th>3hp</th>
                       			<th>3</th><th>3hp</th>
                       			<th>4</th><th>3hp</th>
                       			<th>5</th><th>3hp</th>
                       			<th>6</th><th>3hp</th>
                       			<th>7</th><th>5hp</th>
                       			<th>8</th><th>5hp</th>
                       			<th style="background-color: #0F0; text-align: center">2000</th>
                       			<th>1</th><th>3hp</th>	
                       			<th>2</th><th>5hp</th>
                       			<th>3</th><th>5hp</th>
                       			<th>4</th><th>5hp</th>
                       			<th style="background-color: #0F0; text-align: center">1500</th>
                       			<th style="background-color: #0F0; text-align: center">3500</th>
                       			<th style="background-color: #0F0; text-align: center">90</th>
                       		</tr>
                       		<tr style="font-size: 8px">
                       			<th data-order = "zba1">Seg</th>
                       			<th data-order = "zla1">Lts</th>	
                       			<th data-order = "zba2">Seg</th>
                       			<th data-order = "zla2">Lts</th>	
                       			<th data-order = "zba3">Seg</th>
                       			<th data-order = "zla3">Lts</th>
                       			<th data-order = "zba4">Seg</th>
                       			<th data-order = "zla4">Lts</th>
                       			<th data-order = "zba5">Seg</th>
                       			<th data-order = "zla5">Lts</th>
                       			<th data-order = "zba6">Seg</th>
                       			<th data-order = "zla6">Lts</th>	
                       			<th data-order = "zba7">Seg</th>
                       			<th data-order = "zla7">Lts</th>
                       			<th data-order = "zba8">Seg</th>
                       			<th data-order = "zla8">Lts</th>
                       			<th data-order = "zlta"  style="text-align: center">Total</th>
                       			<th data-order = "zbb1">Seg</th>
                       			<th data-order = "zlb1">Lts</th>		
                       			<th data-order = "zbb2">Seg</th>
                       			<th data-order = "zlb2">Lts</th>	
                       			<th data-order = "zbb3">Seg</th>
                       			<th data-order = "zlb3">Lts</th>	
                       			<th data-order = "zbb4">Seg</th>
                       			<th data-order = "zlb4">Lts</th>	
                       			<th data-order = "zltb"  style="text-align: center">Total</th>
                       			<th data-order = "zlab"  style="text-align: center">A-B</th>
                       			<th data-order = "zpor" style="text-align: center">%</th>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 9px; text-align: center">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<table class="ajaxSorter" border="1" style="width: 750px;">
 				<tr>
 					<th style="text-align: center">Fecha</th>
 					<th rowspan="2"  style="text-align: center; font-size: 25px">A</th>
 					<th style="text-align: center">1</th><th style="text-align: center">2</th><th style="text-align: center">3</th>
 					<th style="text-align: center">4</th><th style="text-align: center">5</th><th style="text-align: center">6</th>
 					<th style="text-align: center">7</th><th style="text-align: center">8</th>
 					<th rowspan="2"  style="text-align: center; font-size: 25px">B</th>
 					<th style="text-align: center">1</th><th style="text-align: center">2</th><th style="text-align: center">3</th>
 					<th style="text-align: center">4</th>
 					<th ><input style="font-size: 14px" type="submit" id="znue" name="znue" value="Nuevo" title="NUEVO" /></th>	
 				</tr>
 				<tr>
 					<td><input size="13%" type="text" name="fecbom2" id="fecbom2" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zba1" type="text" id="zba1"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zba2" type="text" id="zba2"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zba3" type="text" id="zba3"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zba4" type="text" id="zba4"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zba5" type="text" id="zba5"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zba6" type="text" id="zba6"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zba7" type="text" id="zba7"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zba8" type="text" id="zba8"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zbb1" type="text" id="zbb1"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zbb2" type="text" id="zbb2"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zbb3" type="text" id="zbb3"  size="2" /></td>
 					<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="zbb4" type="text" id="zbb4"  size="2" /></td>
 					
 					<td>
    					
    					<input style="font-size: 14px" type="submit" id="zagregas" name="zagregas" value="+" title="AGREGAR y ACTUALIZAR DETALLE" />
						<input style="font-size: 14px" type="submit" id="zquitas" name="zquitas" value="-" title="QUITAR DETALLE" />
						<input type="hidden"  size="3%" type="text" name="idbom2" id="idbom2" style="text-align: center;">
						<input type="hidden" name="zg1" id="zg1"><input type="hidden" name="zg2" id="zg2"><input type="hidden" name="zg3" id="zg3"><input type="hidden" name="zg4" id="zg4"><input type="hidden" name="zg5" id="zg5">
						<input type="hidden" name="zg6" id="zg6"><input type="hidden" name="zg7" id="zg7"><input type="hidden" name="zg8" id="zg8"><input type="hidden" name="zg9" id="zg9"><input type="hidden" name="zg10" id="zg10">
						<input type="hidden" name="zg11" id="zg11"><input type="hidden" name="zg12" id="zg12"><input type="hidden" name="zg13" id="zg13"><input type="hidden" name="zg14" id="zg14"><input type="hidden" name="zg15" id="zg15">
						<input type="hidden" name="zg16" id="zg16"><input type="hidden" name="zg17" id="zg17"><input type="hidden" name="zg18" id="zg18"><input type="hidden" name="zg19" id="zg19"><input type="hidden" name="zg20" id="zg20">
						<input type="hidden" name="zg21" id="zg21"><input type="hidden" name="zg22" id="zg22"><input type="hidden" name="zg23" id="zg23"><input type="hidden" name="zg24" id="zg24"><input type="hidden" name="zg25" id="zg25">
						<input type="hidden" name="zg26" id="zg26"><input type="hidden" name="zg27" id="zg27"><input type="hidden" name="zg28" id="zg28"><input type="hidden" name="zg29" id="zg29"><input type="hidden" name="zg30" id="zg30">
						<input type="hidden" name="zg31" id="zg31">
					</td>
 					
 					
 				</tr>
 			</table>
 		</div>	  	  
 	</div>



<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#znue").click( function(){	
	$("#zba1").val('');$("#zba2").val('');$("#zba3").val('');$("#zba4").val('');$("#zba5").val('');
	$("#zba6").val('');$("#zba7").val('');$("#zba8").val('');$("#fecbom2").val('');
	$("#zbb1").val('');$("#zbb2").val('');$("#zbb3").val('');$("#zbb4").val('');
 return true;
});
$("#zquitas").click( function(){	
	numero=$("#idbom2").val();;
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/quitarSeg2", 
						data: "id="+$("#idbom2").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablapuntas2").trigger("update");
										$("#znue").click();
										$("#idbom2").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar día de Muestra para poder Quitarlo");
		return false;
	}
});
$("#zagregas").click( function(){	
	numero=$("#idbom2").val();fecha=$("#fecbom2").val();
	if(fecha!=''){
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/actualizaSeg2", 
						data: "id="+$("#idbom2").val()+"&fecbom="+$("#fecbom2").val()+"&ba1="+$("#zba1").val()+"&ba2="+$("#zba2").val()+"&ba3="+$("#zba3").val()+"&ba4="+$("#zba4").val()+"&ba5="+$("#zba5").val()+"&ba6="+$("#zba6").val()+"&ba7="+$("#zba7").val()+"&ba8="+$("#zba8").val()+"&bb1="+$("#zbb1").val()+"&bb2="+$("#zbb2").val()+"&bb3="+$("#zbb3").val()+"&bb4="+$("#zbb4").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablapuntas2").trigger("update");
										$("#znue").click();
										$("#zba1").focus();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman/agregaSeg2", 
						data: "&fecbom="+$("#fecbom2").val()+"&ba1="+$("#zba1").val()+"&ba2="+$("#zba2").val()+"&ba3="+$("#zba3").val()+"&ba4="+$("#zba4").val()+"&ba5="+$("#zba5").val()+"&ba6="+$("#zba6").val()+"&ba7="+$("#zba7").val()+"&ba8="+$("#zba8").val()+"&bb1="+$("#zbb1").val()+"&bb2="+$("#zbb2").val()+"&bb3="+$("#zbb3").val()+"&bb4="+$("#zbb4").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablapuntas2").trigger("update");
										$("#znue").click();
										$("#idbom2").val('');
										$("#zba1").focus();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});		
		}
	}else{
		alert("Error: Necesita seleccionar Fecha");
		$("#fecbom2").focus();
		return false;
	}		
});



$("#his2").click( function(){	
 if(confirm('Esta Seguro de Realizar el proceso de historial para el día de Hoy?')){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/kpiman2/historial", 
			data: "idje="+$("#idje").val()+"&idma="+$("#idma").val()+"&fec="+$("#fecje").val()+"&idze="+$("#idze").val(),
			success: 	
					function(msg){															
						if(msg!=0){
							if(msg==1){alert("ya habias actualizado... hasta mañana podras de nuevo hacerlo");}		
							if(msg==2){alert("Historial Actualizado");
							}		
						}else{
							alert("Error con la base de datos o usted no ha actualizado nada");
						}	
						$("#mytablakpi2").trigger("update");				
					}			
	});	
 }		 
});


function radios2(radio,dato){
	switch(radio){
		case 1: $("#je1").val(dato); break;
		case 2: $("#je4").val(dato); break;
		case 3: $("#je7").val(dato); break;
		case 4: $("#ma1").val(dato); break;
		case 5: $("#ma4").val(dato); break;
		case 6: $("#ze5").val(dato); break;
		case 7: $("#je2").val(dato); break;
		case 8: $("#je5").val(dato); break;
		case 9: $("#je8").val(dato); break;
		case 10: $("#ma2").val(dato); break;
		case 11: $("#ze1").val(dato); break;
		case 12: $("#ze6").val(dato); break;
		case 13: $("#je3").val(dato); break;
		case 14: $("#je6").val(dato); break;
		case 15: $("#ma3").val(dato); break;
		case 16: $("#ze2").val(dato); break;
		case 17: $("#ze7").val(dato); break;
		case 18: $("#ze3").val(dato); break;
		case 19: $("#ze8").val(dato); break;
		case 20: $("#je9").val(dato); break;
		case 21: $("#je10").val(dato); break;
		case 22: $("#ma5").val(dato); break;
		case 23: $("#ze4").val(dato); break;
		case 24: $("#ze9").val(dato); break;
		case 25: $("#je11").val(dato); break;
		case 26: $("#ze10").val(dato); break;
		case 27: $("#je12").val(dato); break;
		case 28: $("#je18").val(dato); break;
		case 29: $("#ma6").val(dato); break;
		case 30: $("#ze11").val(dato); break;
		case 31: $("#je13").val(dato); break;
		case 32: $("#je19").val(dato); break;
		case 33: $("#ma7").val(dato); break;
		case 34: $("#ze12").val(dato); break;
		case 35: $("#ze14").val(dato); break;
		case 36: $("#je14").val(dato); break;
		case 37: $("#je20").val(dato); break;
		case 38: $("#ma8").val(dato); break;
		case 39: $("#ze13").val(dato); break;
		case 40: $("#ze15").val(dato); break;
		case 41: $("#je15").val(dato); break;
		case 42: $("#je21").val(dato); break;
		case 43: $("#ma9").val(dato); break;
		case 44: $("#ze16").val(dato); break;
		case 45: $("#je16").val(dato); break;
		case 46: $("#je22").val(dato); break;
		case 47: $("#ma10").val(dato); break;
		case 48: $("#ze17").val(dato); break;
		case 49: $("#je17").val(dato); break;
		case 50: $("#je23").val(dato); break;
		case 51: $("#ma11").val(dato); break;
		case 52: $("#ze18").val(dato); break;
		case 53: $("#je24").val(dato); break;
		case 54: $("#je25").val(dato); break;
		case 55: $("#ma12").val(dato); break;
		case 56: $("#ze19").val(dato); break;
		case 57: $("#je26").val(dato); break;
		case 58: $("#je28").val(dato); break;
		case 59: $("#je29").val(dato); break;
		case 60: $("#je31").val(dato); break;
		case 61: $("#je32").val(dato); break;
		case 62: $("#ze20").val(dato); break;
		case 63: $("#je27").val(dato); break;
		case 64: $("#je30").val(dato); break;
		case 65: $("#ze21").val(dato); break;
		case 66: $("#je33").val(dato); break;
		case 67: $("#je39").val(dato); break;
		case 68: $("#je41").val(dato); break;
		case 69: $("#ze22").val(dato); break;
		case 70: $("#je34").val(dato); break;
		case 71: $("#je40").val(dato); break;
		case 72: $("#je42").val(dato); break;
		case 73: $("#ze23").val(dato); break;
		case 74: $("#je35").val(dato); break;
		case 75: $("#je43").val(dato); break;
		case 76: $("#ze24").val(dato); break;
		case 77: $("#je36").val(dato); break;
		case 78: $("#je44").val(dato); break;
		case 79: $("#je37").val(dato); break;
		case 80: $("#ze25").val(dato); break;
		case 81: $("#je38").val(dato); break;
		case 82: $("#je45").val(dato); break;
		case 83: $("#je46").val(dato); break;
		case 84: $("#ze26").val(dato); break;
		case 85: $("#je47").val(dato); break;
		case 86: $("#je48").val(dato); break;
		case 87: $("#je49").val(dato); break;
		case 88: $("#je50").val(dato); break;
		case 89: $("#je51").val(dato); break;
		case 90: $("#je52").val(dato); break;
		case 91: $("#je53").val(dato); break;
		case 92: $("#ze27").val(dato); break;
		case 93: $("#je54").val(dato); break;
		case 94: $("#je55").val(dato); break;
		case 95: $("#je56").val(dato); break;
		case 96: $("#je57").val(dato); break;
		case 97: $("#je58").val(dato); break;
		case 98: $("#je60").val(dato); break;
		case 99: $("#je61").val(dato); break;
		case 100: $("#ze28").val(dato); break;
		case 101: $("#je59").val(dato); break;
	}
}
$("#nuevo").click( function(){	
	$("#nje").val('');$("#fecje").val('');	
 return true;
})

$("#nuevo1").click( function(){	
	$("#nma").val('');$("#fecje").val('');
 return true;
})

$("#nuevo2").click( function(){	
	$("#nblw2").val('');$("#fecje").val('');
 return true;
})
$("#borrar").click( function(){	
	numero=$("#idje").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/borrar", 
						data: "id="+$("#idje").val(), //+"&usd="+$("#usd").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaje").trigger("update");
										$("#nuevo").click();																																				
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Evaluacion para poder Eliminarla");
		$("#accordion").accordion( "activate",0 );
		return false;
	}
});

$("#borrar1").click( function(){	
	numero=$("#idma").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/borrar1", 
						data: "id="+$("#idma").val(), //+"&usd="+$("#usd").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablama").trigger("update");
										$("#nuevo1").click();																																				
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Evaluacion para poder Eliminarla");
		$("#accordion").accordion( "activate",0 );
		return false;
	}
});

$("#borrarblw").click( function(){	
	numero=$("#idze").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/borrarblw", 
						data: "id="+$("#idze").val(), //+"&usd="+$("#usd").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaze").trigger("update");
										$("#nuevo2").click();																																				
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Evaluacion para poder Eliminarla");
		$("#accordion").accordion( "activate",0 );
		return false;
	}
});

$("#aceptar").click( function(){		
	fec=$("#fecje").val();
	numero=$("#idje").val();
	if( fec!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/actualizar", 
						data: "id="+$("#idje").val()+"&je1="+$("#je1").val()+"&je2="+$("#je2").val()+"&je3="+$("#je3").val()+"&je4="+$("#je4").val()+"&je5="+$("#je5").val()+"&je6="+$("#je6").val()+"&je7="+$("#je7").val()+"&je8="+$("#je8").val()+"&je9="+$("#je9").val()+"&je10="+$("#je10").val()+"&je11="+$("#je11").val()+"&je12="+$("#je12").val()+"&je13="+$("#je13").val()+"&je14="+$("#je14").val()+"&je15="+$("#je15").val()+"&je16="+$("#je16").val()+"&je17="+$("#je17").val()+"&je18="+$("#je18").val()+"&je19="+$("#je19").val()+"&je20="+$("#je20").val()
						+"&je21="+$("#je21").val()+"&je22="+$("#je22").val()+"&je23="+$("#je23").val()+"&je24="+$("#je24").val()+"&je25="+$("#je25").val()+"&je26="+$("#je26").val()+"&je27="+$("#je27").val()+"&je28="+$("#je28").val()+"&je29="+$("#je29").val()+"&je30="+$("#je30").val()
						+"&je31="+$("#je31").val()+"&je32="+$("#je32").val()+"&je33="+$("#je33").val()+"&je34="+$("#je34").val()+"&je35="+$("#je35").val()+"&je36="+$("#je36").val()+"&je37="+$("#je37").val()+"&je38="+$("#je38").val()+"&je39="+$("#je39").val()+"&je40="+$("#je40").val()
						+"&je41="+$("#je41").val()+"&je42="+$("#je42").val()+"&je43="+$("#je43").val()+"&je44="+$("#je44").val()+"&je45="+$("#je45").val()+"&je46="+$("#je46").val()+"&je47="+$("#je47").val()+"&je48="+$("#je48").val()+"&je49="+$("#je49").val()+"&je50="+$("#je50").val()
						+"&je51="+$("#je51").val()+"&je52="+$("#je52").val()+"&je53="+$("#je53").val()+"&je54="+$("#je54").val()+"&je55="+$("#je55").val()+"&je56="+$("#je56").val()+"&je57="+$("#je57").val()+"&je58="+$("#je58").val()+"&je59="+$("#je59").val()+"&je60="+$("#je60").val()
						+"&je61="+$("#je61").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										//$("#nuevo").click();			
										$("#mytablaje").trigger("update");
										//$("#mytablaalm").trigger("update");																		
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/agregar", 
						data: "fec="+$("#fecje").val()+"&je1="+$("#je1").val()+"&je2="+$("#je2").val()+"&je3="+$("#je3").val()+"&je4="+$("#je4").val()+"&je5="+$("#je5").val()+"&je6="+$("#je6").val()+"&je7="+$("#je7").val()+"&je8="+$("#je8").val()+"&je9="+$("#je9").val()+"&je10="+$("#je10").val()
						+"&je11="+$("#je11").val()+"&je12="+$("#je12").val()+"&je13="+$("#je13").val()+"&je14="+$("#je14").val()+"&je15="+$("#je15").val()+"&je16="+$("#je16").val()+"&je17="+$("#je17").val()+"&je18="+$("#je18").val()+"&je19="+$("#je19").val()+"&je20="+$("#je20").val()
						+"&je21="+$("#je21").val()+"&je22="+$("#je22").val()+"&je23="+$("#je23").val()+"&je24="+$("#je24").val()+"&je25="+$("#je25").val()+"&je26="+$("#je26").val()+"&je27="+$("#je27").val()+"&je28="+$("#je28").val()+"&je29="+$("#je29").val()+"&je30="+$("#je30").val()
						+"&je31="+$("#je31").val()+"&je32="+$("#je32").val()+"&je33="+$("#je33").val()+"&je34="+$("#je34").val()+"&je35="+$("#je35").val()+"&je36="+$("#je36").val()+"&je37="+$("#je37").val()+"&je38="+$("#je38").val()+"&je39="+$("#je39").val()+"&je40="+$("#je40").val()
						+"&je41="+$("#je41").val()+"&je42="+$("#je42").val()+"&je43="+$("#je43").val()+"&je44="+$("#je44").val()+"&je45="+$("#je45").val()+"&je46="+$("#je46").val()+"&je47="+$("#je47").val()+"&je48="+$("#je48").val()+"&je49="+$("#je49").val()+"&je50="+$("#je50").val()
						+"&je51="+$("#je51").val()+"&je52="+$("#je52").val()+"&je53="+$("#je53").val()+"&je54="+$("#je54").val()+"&je55="+$("#je55").val()+"&je56="+$("#je56").val()+"&je57="+$("#je57").val()+"&je58="+$("#je58").val()+"&je59="+$("#je59").val()+"&je60="+$("#je60").val()
						+"&je61="+$("#je61").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#nuevo").click();
										$("#mytablaje").trigger("update");
										//$("#mytablaalm").trigger("update");																					
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Seleccione Fecha de Captura");	
			$("#fecje").focus();
				return false;
		}
});

$("#aceptar1").click( function(){		
	fec=$("#fecje").val();
	numero=$("#idma").val();
	if( fec!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/actualizar1", 
						data: "id="+$("#idma").val()+"&ma1="+$("#ma1").val()+"&ma2="+$("#ma2").val()+"&ma3="+$("#ma3").val()+"&ma4="+$("#ma4").val()+"&ma5="+$("#ma5").val()+"&ma6="+$("#ma6").val()+"&ma7="+$("#ma7").val()+"&ma8="+$("#ma8").val()+"&ma9="+$("#ma9").val()+"&ma10="+$("#ma10").val()+"&ma11="+$("#ma11").val()+"&ma12="+$("#ma12").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										//$("#nuevo1").click();		
										//$("#mytablaamce").trigger("update");
										$("#mytablama").trigger("update");																			
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/agregar1", 
						data: "fec="+$("#fecje").val()+"&ma1="+$("#ma1").val()+"&ma2="+$("#ma2").val()+"&ma3="+$("#ma3").val()+"&ma4="+$("#ma4").val()+"&ma5="+$("#ma5").val()+"&ma6="+$("#ma6").val()+"&ma7="+$("#ma7").val()+"&ma8="+$("#ma8").val()+"&ma9="+$("#ma9").val()+"&ma10="+$("#ma10").val()+"&ma11="+$("#ma11").val()+"&ma12="+$("#ma12").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#nuevo1").click();
										//$("#mytablaamce").trigger("update");
										$("#mytablama").trigger("update");																					
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Seleccione Fecha de Captura");	
			$("#fecje").focus();
				return false;
		}
});

$("#aceptarblw").click( function(){		
	fec=$("#fecje").val();
	numero=$("#idze").val();
	if( fec!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/actualizarblw", 
						data: "id="+$("#idze").val()+"&ze1="+$("#ze1").val()+"&ze2="+$("#ze2").val()+"&ze3="+$("#ze3").val()+"&ze4="+$("#ze4").val()+"&ze5="+$("#ze5").val()+"&ze6="+$("#ze6").val()+"&ze7="+$("#ze7").val()+"&ze8="+$("#ze8").val()+"&ze9="+$("#ze9").val()+"&ze10="+$("#ze10").val()+"&ze11="+$("#ze11").val()+"&ze12="+$("#ze12").val()+"&ze13="+$("#ze13").val()+"&ze14="+$("#ze14").val()+"&ze15="+$("#ze15").val()+"&ze16="+$("#ze16").val()+"&ze17="+$("#ze17").val()+"&ze18="+$("#ze18").val()+"&ze19="+$("#ze19").val()+"&ze20="+$("#ze20").val()+"&ze21="+$("#ze21").val()+"&ze22="+$("#ze22").val()+"&ze23="+$("#ze23").val()+"&ze24="+$("#ze24").val()+"&ze25="+$("#ze25").val()+"&ze26="+$("#ze26").val()+"&ze27="+$("#ze27").val()+"&ze28="+$("#ze28").val()+"&ze29="+$("#ze29").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										//$("#nuevo2").click();		
										//$("#mytablaamce").trigger("update");
										$("#mytablaze").trigger("update");																			
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/kpiman2/agregarblw", 
						data: "fec="+$("#fecje").val()+"&ze1="+$("#ze1").val()+"&ze2="+$("#ze2").val()+"&ze3="+$("#ze3").val()+"&ze4="+$("#ze4").val()+"&ze5="+$("#ze5").val()+"&ze6="+$("#ze6").val()+"&ze7="+$("#ze7").val()+"&ze8="+$("#ze8").val()+"&ze9="+$("#ze9").val()+"&ze10="+$("#ze10").val()+"&ze11="+$("#ze11").val()+"&ze12="+$("#ze12").val()+"&ze13="+$("#ze13").val()+"&ze14="+$("#ze14").val()+"&ze15="+$("#ze15").val()+"&ze16="+$("#ze16").val()+"&ze17="+$("#ze17").val()+"&ze18="+$("#ze18").val()+"&ze19="+$("#ze19").val()+"&ze20="+$("#ze20").val()+"&ze21="+$("#ze21").val()+"&ze22="+$("#ze22").val()+"&ze23="+$("#ze23").val()+"&ze24="+$("#ze24").val()+"&ze25="+$("#ze25").val()+"&ze26="+$("#ze26").val()+"&ze27="+$("#ze27").val()+"&ze28="+$("#ze28").val()+"&ze29="+$("#ze29").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#nuevo2").click();
										//$("#mytablaamce").trigger("update");
										$("#mytablaze").trigger("update");																					
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Seleccione Fecha de Captura");	
			$("#fecje").focus();
				return false;
		}
});
$("#fecje").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",	
	onSelect: function( selectedDate ){
		//$("#fecma").val( selectedDate );
		//$("#fecblw2").val( selectedDate );
		$("#mytablaje").trigger("update");
		$("#mytablama").trigger("update");
		$("#mytablaze").trigger("update");
	}
});

$("#fecbom").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",	
	onSelect: function( selectedDate ){
		
	}
});
$("#fecbom2").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",	
	onSelect: function( selectedDate ){
		
	}
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

	

$("#cmbMeskpi2").change( function(){
	$("#cmbMeskpip2").val($("#cmbMeskpi2").val());$("#mytablapuntas2").trigger("update");
});	
$("#cmbMeskpip2").change( function(){
	$("#cmbMeskpi2").val($("#cmbMeskpip2").val());$("#mytablakpi2").trigger("update");
});
$(document).ready(function(){ 
	var f = new Date(); 
    $("#cmbMeskpi2").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));
    $("#cmbMeskpip2").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));
	$("#nuevo").click();	$("#nuevo1").click(); $('#ver_mas_pilax').hide();
	
	$("#tabla_kpi2").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpiadm/tablakpi', 
		filters:['cmbMeskpi2','cmbDepto2'],
		sort:false,	
		onSuccess:function(){   						
			//ilumina el dia actual del año en curso
			if($("#cmbMeskpi2").val()==(f.getMonth() +1)){
				$('#tabla_kpi2 tbody tr td:nth-child('+((f.getDate())+1)+')').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			}
			$('#tabla_kpi2 tr').map(function(){	    		
	    		$(this).children("td").each(function(){
       				if($(this).html() >= 90.0 && $(this).html() <= 100.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 60.0 && $(this).html() <= 89.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
            		if($(this).html() >= 2 &&  $(this).html() <= 59.9 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "red");
            		}            		
        		})
        	});
	    	$('#tabla_kpi2 tr').map(function(){	    		
	    		if($(this).data('nom')=='C.V.') {
	    		$(this).children("td").each(function(){
       				if($(this).html() >= 0.1 && $(this).html() <= 3.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 3.01 && $(this).html() <= 4.99 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
            		if($(this).html() >= 5 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "red");
            		}  
        		})
        		}
        		
	    	});
	    	$('#tabla_kpi2 tr').map(function(){	  
	    	if($(this).data('nom')=='KPI') {
	    		$(this).children("td").each(function(){
       				if($(this).html() >= 90.0 && $(this).html() <= 100.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 60.0 && $(this).html() <= 89.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
            		if($(this).html() >= 2 &&  $(this).html() <= 59.9 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "red");
            		}         		 
        		})
        		}
        	});	
		},	
	});

	$("#tabla_je").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpiman2/tabla',
		filters:['tablaje','fecje'],
		sort:false,	
		onSuccess:function(){   						
			$('#tabla_je tr').map(function(){
				$("#idje").val($(this).data('nje'));$("#nadr1").val($(this).data('nje'));
				$("#je1").val($(this).data('je1'));je1=$(this).data('je1');
				if(je1==1){ $('input:radio[name=rje1]:nth(0)').attr('checked',true); }
				else if(je1==0){ $('input:radio[name=rje1]:nth(1)').attr('checked',true); }
				else if(je1==-1){ $('input:radio[name=rje1]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje1]:nth(0)').attr('checked',false);$('input:radio[name=rje1]:nth(1)').attr('checked',false);$('input:radio[name=rje1]:nth(2)').attr('checked',false); }
				$("#je2").val($(this).data('je2'));je2=$(this).data('je2');
				if(je2==1){ $('input:radio[name=rje2]:nth(0)').attr('checked',true); }
				else if(je2==0){ $('input:radio[name=rje2]:nth(1)').attr('checked',true); }
				else if(je2==-1){ $('input:radio[name=rje2]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje2]:nth(0)').attr('checked',false);$('input:radio[name=rje2]:nth(1)').attr('checked',false);$('input:radio[name=rje2]:nth(2)').attr('checked',false); }
				$("#je3").val($(this).data('je3'));je3=$(this).data('je3');
				if(je3==1){ $('input:radio[name=rje3]:nth(0)').attr('checked',true); }
				else if(je3==0){ $('input:radio[name=rje3]:nth(1)').attr('checked',true); }
				else if(je3==-1){ $('input:radio[name=rje3]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje3]:nth(0)').attr('checked',false);$('input:radio[name=rje3]:nth(1)').attr('checked',false);$('input:radio[name=rje3]:nth(2)').attr('checked',false); }
				$("#je4").val($(this).data('je4'));je4=$(this).data('je4');
				if(je4==1){ $('input:radio[name=rje4]:nth(0)').attr('checked',true); }
				else if(je4==0){ $('input:radio[name=rje4]:nth(1)').attr('checked',true); }
				else if(je4==-1){ $('input:radio[name=rje4]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje4]:nth(0)').attr('checked',false);$('input:radio[name=rje4]:nth(1)').attr('checked',false);$('input:radio[name=rje4]:nth(2)').attr('checked',false); }
				$("#je5").val($(this).data('je5'));je5=$(this).data('je5');
				if(je5==1){ $('input:radio[name=rje5]:nth(0)').attr('checked',true); }
				else if(je5==0){ $('input:radio[name=rje5]:nth(1)').attr('checked',true); }
				else if(je5==-1){ $('input:radio[name=rje5]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje5]:nth(0)').attr('checked',false);$('input:radio[name=rje5]:nth(1)').attr('checked',false);$('input:radio[name=rje5]:nth(2)').attr('checked',false); }
				$("#je6").val($(this).data('je6'));je6=$(this).data('je6');
				if(je6==1){ $('input:radio[name=rje6]:nth(0)').attr('checked',true); }
				else if(je6==0){ $('input:radio[name=rje6]:nth(1)').attr('checked',true); }
				else if(je6==-1){ $('input:radio[name=rje6]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje6]:nth(0)').attr('checked',false);$('input:radio[name=rje6]:nth(1)').attr('checked',false);$('input:radio[name=rje6]:nth(2)').attr('checked',false); }
				$("#je7").val($(this).data('je7'));je7=$(this).data('je7');
				if(je7==1){ $('input:radio[name=rje7]:nth(0)').attr('checked',true); }
				else if(je7==0){ $('input:radio[name=rje7]:nth(1)').attr('checked',true); }
				else if(je7==-1){ $('input:radio[name=rje7]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje7]:nth(0)').attr('checked',false);$('input:radio[name=rje7]:nth(1)').attr('checked',false);$('input:radio[name=rje7]:nth(2)').attr('checked',false); }
				$("#je8").val($(this).data('je8'));je8=$(this).data('je8');
				if(je8==1){ $('input:radio[name=rje8]:nth(0)').attr('checked',true); }
				else if(je8==0){ $('input:radio[name=rje8]:nth(1)').attr('checked',true); }
				else if(je8==-1){ $('input:radio[name=rje8]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje8]:nth(0)').attr('checked',false);$('input:radio[name=rje8]:nth(1)').attr('checked',false);$('input:radio[name=rje8]:nth(2)').attr('checked',false); }
				$("#je9").val($(this).data('je9'));je9=$(this).data('je9');
				if(je9==1){ $('input:radio[name=rje9]:nth(0)').attr('checked',true); }
				else if(je9==0){ $('input:radio[name=rje9]:nth(1)').attr('checked',true); }
				else if(je9==-1){ $('input:radio[name=rje9]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje9]:nth(0)').attr('checked',false);$('input:radio[name=rje9]:nth(1)').attr('checked',false);$('input:radio[name=rje9]:nth(2)').attr('checked',false); }
				$("#je10").val($(this).data('je10'));je10=$(this).data('je10');
				if(je10==1){ $('input:radio[name=rje10]:nth(0)').attr('checked',true); }
				else if(je10==0){ $('input:radio[name=rje10]:nth(1)').attr('checked',true); }
				else if(je10==-1){ $('input:radio[name=rje10]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje10]:nth(0)').attr('checked',false);$('input:radio[name=rje10]:nth(1)').attr('checked',false);$('input:radio[name=rje10]:nth(2)').attr('checked',false); }
				$("#je11").val($(this).data('je11'));je11=$(this).data('je11');
				if(je11==1){ $('input:radio[name=rje11]:nth(0)').attr('checked',true); }
				else if(je11==0){ $('input:radio[name=rje11]:nth(1)').attr('checked',true); }
				else if(je11==-1){ $('input:radio[name=rje11]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje11]:nth(0)').attr('checked',false);$('input:radio[name=rje11]:nth(1)').attr('checked',false);$('input:radio[name=rje11]:nth(2)').attr('checked',false); }
				$("#je12").val($(this).data('je12'));je12=$(this).data('je12');
				if(je12==1){ $('input:radio[name=rje12]:nth(0)').attr('checked',true); }
				else if(je12==0){ $('input:radio[name=rje12]:nth(1)').attr('checked',true); }
				else if(je12==-1){ $('input:radio[name=rje12]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje12]:nth(0)').attr('checked',false);$('input:radio[name=rje12]:nth(1)').attr('checked',false);$('input:radio[name=rje12]:nth(2)').attr('checked',false); }
				$("#je13").val($(this).data('je13'));je13=$(this).data('je13');
				if(je13==1){ $('input:radio[name=rje13]:nth(0)').attr('checked',true); }
				else if(je13==0){ $('input:radio[name=rje13]:nth(1)').attr('checked',true); }
				else if(je13==-1){ $('input:radio[name=rje13]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje13]:nth(0)').attr('checked',false);$('input:radio[name=rje13]:nth(1)').attr('checked',false);$('input:radio[name=rje13]:nth(2)').attr('checked',false); }
				$("#je14").val($(this).data('je14'));je14=$(this).data('je14');
				if(je14==1){ $('input:radio[name=rje14]:nth(0)').attr('checked',true); }
				else if(je14==0){ $('input:radio[name=rje14]:nth(1)').attr('checked',true); }
				else if(je14==-1){ $('input:radio[name=rje14]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje14]:nth(0)').attr('checked',false);$('input:radio[name=rje14]:nth(1)').attr('checked',false);$('input:radio[name=rje14]:nth(2)').attr('checked',false); }
				$("#je15").val($(this).data('je15'));je15=$(this).data('je15');
				if(je15==1){ $('input:radio[name=rje15]:nth(0)').attr('checked',true); }
				else if(je15==0){ $('input:radio[name=rje15]:nth(1)').attr('checked',true); }
				else if(je15==-1){ $('input:radio[name=rje15]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje15]:nth(0)').attr('checked',false);$('input:radio[name=rje15]:nth(1)').attr('checked',false);$('input:radio[name=rje15]:nth(2)').attr('checked',false); }
				$("#je16").val($(this).data('je16'));je16=$(this).data('je16');
				if(je16==1){ $('input:radio[name=rje16]:nth(0)').attr('checked',true); }
				else if(je16==0){ $('input:radio[name=rje16]:nth(1)').attr('checked',true); }
				else if(je16==-1){ $('input:radio[name=rje16]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje16]:nth(0)').attr('checked',false);$('input:radio[name=rje16]:nth(1)').attr('checked',false);$('input:radio[name=rje16]:nth(2)').attr('checked',false); }
				$("#je17").val($(this).data('je17'));je17=$(this).data('je17');
				if(je17==1){ $('input:radio[name=rje17]:nth(0)').attr('checked',true); }
				else if(je17==0){ $('input:radio[name=rje17]:nth(1)').attr('checked',true); }
				else if(je17==-1){ $('input:radio[name=rje17]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje17]:nth(0)').attr('checked',false);$('input:radio[name=rje17]:nth(1)').attr('checked',false);$('input:radio[name=rje17]:nth(2)').attr('checked',false); }
				$("#je18").val($(this).data('je18'));je18=$(this).data('je18');
				if(je18==1){ $('input:radio[name=rje18]:nth(0)').attr('checked',true); }
				else if(je18==0){ $('input:radio[name=rje18]:nth(1)').attr('checked',true); }
				else if(je18==-1){ $('input:radio[name=rje18]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje18]:nth(0)').attr('checked',false);$('input:radio[name=rje18]:nth(1)').attr('checked',false);$('input:radio[name=rje18]:nth(2)').attr('checked',false); }
				$("#je19").val($(this).data('je19'));je19=$(this).data('je19');
				if(je19==1){ $('input:radio[name=rje19]:nth(0)').attr('checked',true); }
				else if(je19==0){ $('input:radio[name=rje19]:nth(1)').attr('checked',true); }
				else if(je19==-1){ $('input:radio[name=rje19]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje19]:nth(0)').attr('checked',false);$('input:radio[name=rje19]:nth(1)').attr('checked',false);$('input:radio[name=rje19]:nth(2)').attr('checked',false); }
				$("#je20").val($(this).data('je20'));je20=$(this).data('je20');
				if(je20==1){ $('input:radio[name=rje20]:nth(0)').attr('checked',true); }
				else if(je20==0){ $('input:radio[name=rje20]:nth(1)').attr('checked',true); }
				else if(je20==-1){ $('input:radio[name=rje20]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje20]:nth(0)').attr('checked',false);$('input:radio[name=rje20]:nth(1)').attr('checked',false);$('input:radio[name=rje20]:nth(2)').attr('checked',false); }
				$("#je21").val($(this).data('je21'));je21=$(this).data('je21');
				if(je21==1){ $('input:radio[name=rje21]:nth(0)').attr('checked',true); }
				else if(je21==0){ $('input:radio[name=rje21]:nth(1)').attr('checked',true); }
				else if(je21==-1){ $('input:radio[name=rje21]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje21]:nth(0)').attr('checked',false);$('input:radio[name=rje21]:nth(1)').attr('checked',false);$('input:radio[name=rje21]:nth(2)').attr('checked',false); }
				$("#je22").val($(this).data('je22'));je22=$(this).data('je22');
				if(je22==1){ $('input:radio[name=rje22]:nth(0)').attr('checked',true); }
				else if(je22==0){ $('input:radio[name=rje22]:nth(1)').attr('checked',true); }
				else if(je22==-1){ $('input:radio[name=rje22]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje22]:nth(0)').attr('checked',false);$('input:radio[name=rje22]:nth(1)').attr('checked',false);$('input:radio[name=rje22]:nth(2)').attr('checked',false); }
				$("#je23").val($(this).data('je23'));je23=$(this).data('je23');
				if(je23==1){ $('input:radio[name=rje23]:nth(0)').attr('checked',true); }
				else if(je23==0){ $('input:radio[name=rje23]:nth(1)').attr('checked',true); }
				else if(je23==-1){ $('input:radio[name=rje32]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje23]:nth(0)').attr('checked',false);$('input:radio[name=rje23]:nth(1)').attr('checked',false);$('input:radio[name=rje23]:nth(2)').attr('checked',false); }
				$("#je24").val($(this).data('je24'));je24=$(this).data('je24');
				if(je24==1){ $('input:radio[name=rje24]:nth(0)').attr('checked',true); }
				else if(je24==0){ $('input:radio[name=rje24]:nth(1)').attr('checked',true); }
				else if(je24==-1){ $('input:radio[name=rje24]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje24]:nth(0)').attr('checked',false);$('input:radio[name=rje24]:nth(1)').attr('checked',false);$('input:radio[name=rje24]:nth(2)').attr('checked',false); }
				$("#je25").val($(this).data('je25'));je25=$(this).data('je25');
				if(je25==1){ $('input:radio[name=rje25]:nth(0)').attr('checked',true); }
				else if(je25==0){ $('input:radio[name=rje25]:nth(1)').attr('checked',true); }
				else if(je25==-1){ $('input:radio[name=rje25]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje25]:nth(0)').attr('checked',false);$('input:radio[name=rje25]:nth(1)').attr('checked',false);$('input:radio[name=rje25]:nth(2)').attr('checked',false); }
				$("#je26").val($(this).data('je26'));je26=$(this).data('je26');
				if(je26==1){ $('input:radio[name=rje26]:nth(0)').attr('checked',true); }
				else if(je26==0){ $('input:radio[name=rje26]:nth(1)').attr('checked',true); }
				else if(je26==-1){ $('input:radio[name=rje26]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje26]:nth(0)').attr('checked',false);$('input:radio[name=rje26]:nth(1)').attr('checked',false);$('input:radio[name=rje26]:nth(2)').attr('checked',false); }
				$("#je27").val($(this).data('je27'));je27=$(this).data('je27');
				if(je27==1){ $('input:radio[name=rje27]:nth(0)').attr('checked',true); }
				else if(je27==0){ $('input:radio[name=rje27]:nth(1)').attr('checked',true); }
				else if(je27==-1){ $('input:radio[name=rje27]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje27]:nth(0)').attr('checked',false);$('input:radio[name=rje27]:nth(1)').attr('checked',false);$('input:radio[name=rje27]:nth(2)').attr('checked',false); }
				$("#je28").val($(this).data('je28'));je28=$(this).data('je28');
				if(je28==1){ $('input:radio[name=rje28]:nth(0)').attr('checked',true); }
				else if(je28==0){ $('input:radio[name=rje28]:nth(1)').attr('checked',true); }
				else if(je28==-1){ $('input:radio[name=rje28]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje28]:nth(0)').attr('checked',false);$('input:radio[name=rje28]:nth(1)').attr('checked',false);$('input:radio[name=rje28]:nth(2)').attr('checked',false); }
				$("#je29").val($(this).data('je29'));je29=$(this).data('je29');
				if(je29==1){ $('input:radio[name=rje29]:nth(0)').attr('checked',true); }
				else if(je29==0){ $('input:radio[name=rje29]:nth(1)').attr('checked',true); }
				else if(je29==-1){ $('input:radio[name=rje29]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje29]:nth(0)').attr('checked',false);$('input:radio[name=rje29]:nth(1)').attr('checked',false);$('input:radio[name=rje29]:nth(2)').attr('checked',false); }
				$("#je30").val($(this).data('je30'));je30=$(this).data('je30');
				if(je30==1){ $('input:radio[name=rje30]:nth(0)').attr('checked',true); }
				else if(je30==0){ $('input:radio[name=rje30]:nth(1)').attr('checked',true); }
				else if(je30==-1){ $('input:radio[name=rje30]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje30]:nth(0)').attr('checked',false);$('input:radio[name=rje30]:nth(1)').attr('checked',false);$('input:radio[name=rje30]:nth(2)').attr('checked',false); }
				$("#je31").val($(this).data('je31'));je31=$(this).data('je31');
				if(je31==1){ $('input:radio[name=rje31]:nth(0)').attr('checked',true); }
				else if(je31==0){ $('input:radio[name=rje31]:nth(1)').attr('checked',true); }
				else if(je31==-1){ $('input:radio[name=rje31]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje31]:nth(0)').attr('checked',false);$('input:radio[name=rje31]:nth(1)').attr('checked',false);$('input:radio[name=rje31]:nth(2)').attr('checked',false); }
				$("#je32").val($(this).data('je32'));je32=$(this).data('je32');
				if(je32==1){ $('input:radio[name=rje32]:nth(0)').attr('checked',true); }
				else if(je32==0){ $('input:radio[name=rje32]:nth(1)').attr('checked',true); }
				else if(je32==-1){ $('input:radio[name=rje32]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje32]:nth(0)').attr('checked',false);$('input:radio[name=rje32]:nth(1)').attr('checked',false);$('input:radio[name=rje32]:nth(2)').attr('checked',false); }
				$("#je33").val($(this).data('je33'));je33=$(this).data('je33');
				if(je33==1){ $('input:radio[name=rje33]:nth(0)').attr('checked',true); }
				else if(je33==0){ $('input:radio[name=rje33]:nth(1)').attr('checked',true); }
				else if(je33==-1){ $('input:radio[name=rje33]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje33]:nth(0)').attr('checked',false);$('input:radio[name=rje33]:nth(1)').attr('checked',false);$('input:radio[name=rje33]:nth(2)').attr('checked',false); }
				$("#je34").val($(this).data('je34'));je34=$(this).data('je34');
				if(je34==1){ $('input:radio[name=rje34]:nth(0)').attr('checked',true); }
				else if(je34==0){ $('input:radio[name=rje34]:nth(1)').attr('checked',true); }
				else if(je34==-1){ $('input:radio[name=rje34]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje34]:nth(0)').attr('checked',false);$('input:radio[name=rje34]:nth(1)').attr('checked',false);$('input:radio[name=rje34]:nth(2)').attr('checked',false); }
				$("#je35").val($(this).data('je35'));je35=$(this).data('je35');
				if(je35==1){ $('input:radio[name=rje35]:nth(0)').attr('checked',true); }
				else if(je35==0){ $('input:radio[name=rje35]:nth(1)').attr('checked',true); }
				else if(je35==-1){ $('input:radio[name=rje35]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje35]:nth(0)').attr('checked',false);$('input:radio[name=rje35]:nth(1)').attr('checked',false);$('input:radio[name=rje35]:nth(2)').attr('checked',false); }
				$("#je36").val($(this).data('je36'));je36=$(this).data('je36');
				if(je36==1){ $('input:radio[name=rje36]:nth(0)').attr('checked',true); }
				else if(je36==0){ $('input:radio[name=rje36]:nth(1)').attr('checked',true); }
				else if(je36==-1){ $('input:radio[name=rje36]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje36]:nth(0)').attr('checked',false);$('input:radio[name=rje36]:nth(1)').attr('checked',false);$('input:radio[name=rje36]:nth(2)').attr('checked',false); }
				$("#je37").val($(this).data('je37'));je37=$(this).data('je37');
				if(je37==1){ $('input:radio[name=rje37]:nth(0)').attr('checked',true); }
				else if(je37==0){ $('input:radio[name=rje37]:nth(1)').attr('checked',true); }
				else if(je37==-1){ $('input:radio[name=rje37]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje37]:nth(0)').attr('checked',false);$('input:radio[name=rje37]:nth(1)').attr('checked',false);$('input:radio[name=rje37]:nth(2)').attr('checked',false); }
				$("#je38").val($(this).data('je38'));je38=$(this).data('je38');
				if(je38==1){ $('input:radio[name=rje38]:nth(0)').attr('checked',true); }
				else if(je38==0){ $('input:radio[name=rje38]:nth(1)').attr('checked',true); }
				else if(je38==-1){ $('input:radio[name=rje38]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje38]:nth(0)').attr('checked',false);$('input:radio[name=rje38]:nth(1)').attr('checked',false);$('input:radio[name=rje38]:nth(2)').attr('checked',false); }
				$("#je39").val($(this).data('je39'));je39=$(this).data('je39');
				if(je39==1){ $('input:radio[name=rje39]:nth(0)').attr('checked',true); }
				else if(je39==0){ $('input:radio[name=rje39]:nth(1)').attr('checked',true); }
				else if(je39==-1){ $('input:radio[name=rje39]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje39]:nth(0)').attr('checked',false);$('input:radio[name=rje39]:nth(1)').attr('checked',false);$('input:radio[name=rje39]:nth(2)').attr('checked',false); }
				$("#je40").val($(this).data('je40'));je40=$(this).data('je40');
				if(je40==1){ $('input:radio[name=rje40]:nth(0)').attr('checked',true); }
				else if(je40==0){ $('input:radio[name=rje40]:nth(1)').attr('checked',true); }
				else if(je40==-1){ $('input:radio[name=rje40]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje40]:nth(0)').attr('checked',false);$('input:radio[name=rje40]:nth(1)').attr('checked',false);$('input:radio[name=rje40]:nth(2)').attr('checked',false); }
				$("#je41").val($(this).data('je41'));je41=$(this).data('je41');
				if(je41==1){ $('input:radio[name=rje41]:nth(0)').attr('checked',true); }
				else if(je41==0){ $('input:radio[name=rje41]:nth(1)').attr('checked',true); }
				else if(je41==-1){ $('input:radio[name=rje41]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje41]:nth(0)').attr('checked',false);$('input:radio[name=rje41]:nth(1)').attr('checked',false);$('input:radio[name=rje41]:nth(2)').attr('checked',false); }
				$("#je42").val($(this).data('je42'));je42=$(this).data('je42');
				if(je42==1){ $('input:radio[name=rje42]:nth(0)').attr('checked',true); }
				else if(je42==0){ $('input:radio[name=rje42]:nth(1)').attr('checked',true); }
				else if(je42==-1){ $('input:radio[name=rje42]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje42]:nth(0)').attr('checked',false);$('input:radio[name=rje42]:nth(1)').attr('checked',false);$('input:radio[name=rje42]:nth(2)').attr('checked',false); }
				$("#je43").val($(this).data('je43'));je43=$(this).data('je43');
				if(je43==1){ $('input:radio[name=rje43]:nth(0)').attr('checked',true); }
				else if(je43==0){ $('input:radio[name=rje43]:nth(1)').attr('checked',true); }
				else if(je43==-1){ $('input:radio[name=rje43]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje43]:nth(0)').attr('checked',false);$('input:radio[name=rje43]:nth(1)').attr('checked',false);$('input:radio[name=rje43]:nth(2)').attr('checked',false); }
				$("#je44").val($(this).data('je44'));je44=$(this).data('je44');
				if(je44==1){ $('input:radio[name=rje44]:nth(0)').attr('checked',true); }
				else if(je44==0){ $('input:radio[name=rje44]:nth(1)').attr('checked',true); }
				else if(je44==-1){ $('input:radio[name=rje44]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje44]:nth(0)').attr('checked',false);$('input:radio[name=rje44]:nth(1)').attr('checked',false);$('input:radio[name=rje44]:nth(2)').attr('checked',false); }
				$("#je45").val($(this).data('je45'));je45=$(this).data('je45');
				if(je45==1){ $('input:radio[name=rje45]:nth(0)').attr('checked',true); }
				else if(je45==0){ $('input:radio[name=rje45]:nth(1)').attr('checked',true); }
				else if(je45==-1){ $('input:radio[name=rje45]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje45]:nth(0)').attr('checked',false);$('input:radio[name=rje45]:nth(1)').attr('checked',false);$('input:radio[name=rje45]:nth(2)').attr('checked',false); }
				$("#je46").val($(this).data('je46'));je46=$(this).data('je46');
				if(je46==1){ $('input:radio[name=rje46]:nth(0)').attr('checked',true); }
				else if(je46==0){ $('input:radio[name=rje46]:nth(1)').attr('checked',true); }
				else if(je46==-1){ $('input:radio[name=rje46]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje46]:nth(0)').attr('checked',false);$('input:radio[name=rje46]:nth(1)').attr('checked',false);$('input:radio[name=rje46]:nth(2)').attr('checked',false); }
				$("#je47").val($(this).data('je47'));je47=$(this).data('je47');
				if(je47==1){ $('input:radio[name=rje47]:nth(0)').attr('checked',true); }
				else if(je47==0){ $('input:radio[name=rje47]:nth(1)').attr('checked',true); }
				else if(je47==-1){ $('input:radio[name=rje47]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje47]:nth(0)').attr('checked',false);$('input:radio[name=rje47]:nth(1)').attr('checked',false);$('input:radio[name=rje47]:nth(2)').attr('checked',false); }
				$("#je48").val($(this).data('je48'));je48=$(this).data('je48');
				if(je48==1){ $('input:radio[name=rje48]:nth(0)').attr('checked',true); }
				else if(je48==0){ $('input:radio[name=rje48]:nth(1)').attr('checked',true); }
				else if(je48==-1){ $('input:radio[name=rje48]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje48]:nth(0)').attr('checked',false);$('input:radio[name=rje48]:nth(1)').attr('checked',false);$('input:radio[name=rje48]:nth(2)').attr('checked',false); }
				$("#je49").val($(this).data('je49'));je49=$(this).data('je49');
				if(je49==1){ $('input:radio[name=rje49]:nth(0)').attr('checked',true); }
				else if(je49==0){ $('input:radio[name=rje49]:nth(1)').attr('checked',true); }
				else if(je49==-1){ $('input:radio[name=rje49]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje49]:nth(0)').attr('checked',false);$('input:radio[name=rje49]:nth(1)').attr('checked',false);$('input:radio[name=rje49]:nth(2)').attr('checked',false); }
				$("#je50").val($(this).data('je50'));je50=$(this).data('je50');
				if(je50==1){ $('input:radio[name=rje50]:nth(0)').attr('checked',true); }
				else if(je50==0){ $('input:radio[name=rje50]:nth(1)').attr('checked',true); }
				else if(je50==-1){ $('input:radio[name=rje50]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje50]:nth(0)').attr('checked',false);$('input:radio[name=rje50]:nth(1)').attr('checked',false);$('input:radio[name=rje50]:nth(2)').attr('checked',false); }
				$("#je51").val($(this).data('je51'));je51=$(this).data('je51');
				if(je51==1){ $('input:radio[name=rje51]:nth(0)').attr('checked',true); }
				else if(je51==0){ $('input:radio[name=rje51]:nth(1)').attr('checked',true); }
				else if(je51==-1){ $('input:radio[name=rje51]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje51]:nth(0)').attr('checked',false);$('input:radio[name=rje51]:nth(1)').attr('checked',false);$('input:radio[name=rje51]:nth(2)').attr('checked',false); }
				$("#je52").val($(this).data('je52'));je52=$(this).data('je52');
				if(je52==1){ $('input:radio[name=rje52]:nth(0)').attr('checked',true); }
				else if(je52==0){ $('input:radio[name=rje52]:nth(1)').attr('checked',true); }
				else if(je52==-1){ $('input:radio[name=rje52]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje52]:nth(0)').attr('checked',false);$('input:radio[name=rje52]:nth(1)').attr('checked',false);$('input:radio[name=rje52]:nth(2)').attr('checked',false); }
				$("#je53").val($(this).data('je53'));je53=$(this).data('je53');
				if(je53==1){ $('input:radio[name=rje53]:nth(0)').attr('checked',true); }
				else if(je53==0){ $('input:radio[name=rje53]:nth(1)').attr('checked',true); }
				else if(je53==-1){ $('input:radio[name=rje53]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje53]:nth(0)').attr('checked',false);$('input:radio[name=rje53]:nth(1)').attr('checked',false);$('input:radio[name=rje53]:nth(2)').attr('checked',false); }
				$("#je54").val($(this).data('je54'));je54=$(this).data('je54');
				if(je54==1){ $('input:radio[name=rje54]:nth(0)').attr('checked',true); }
				else if(je54==0){ $('input:radio[name=rje54]:nth(1)').attr('checked',true); }
				else if(je54==-1){ $('input:radio[name=rje54]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje54]:nth(0)').attr('checked',false);$('input:radio[name=rje54]:nth(1)').attr('checked',false);$('input:radio[name=rje54]:nth(2)').attr('checked',false); }
				$("#je55").val($(this).data('je55'));je55=$(this).data('je55');
				if(je55==1){ $('input:radio[name=rje55]:nth(0)').attr('checked',true); }
				else if(je55==0){ $('input:radio[name=rje55]:nth(1)').attr('checked',true); }
				else if(je55==-1){ $('input:radio[name=rje55]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje55]:nth(0)').attr('checked',false);$('input:radio[name=rje55]:nth(1)').attr('checked',false);$('input:radio[name=rje55]:nth(2)').attr('checked',false); }
				$("#je56").val($(this).data('je56'));je56=$(this).data('je56');
				if(je56==1){ $('input:radio[name=rje56]:nth(0)').attr('checked',true); }
				else if(je56==0){ $('input:radio[name=rje56]:nth(1)').attr('checked',true); }
				else if(je56==-1){ $('input:radio[name=rje56]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje56]:nth(0)').attr('checked',false);$('input:radio[name=rje56]:nth(1)').attr('checked',false);$('input:radio[name=rje56]:nth(2)').attr('checked',false); }
				$("#je57").val($(this).data('je57'));je57=$(this).data('je57');
				if(je57==1){ $('input:radio[name=rje57]:nth(0)').attr('checked',true); }
				else if(je57==0){ $('input:radio[name=rje57]:nth(1)').attr('checked',true); }
				else if(je57==-1){ $('input:radio[name=rje57]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje57]:nth(0)').attr('checked',false);$('input:radio[name=rje57]:nth(1)').attr('checked',false);$('input:radio[name=rje57]:nth(2)').attr('checked',false); }
				$("#je58").val($(this).data('je58'));je58=$(this).data('je58');
				if(je58==1){ $('input:radio[name=rje58]:nth(0)').attr('checked',true); }
				else if(je58==0){ $('input:radio[name=rje58]:nth(1)').attr('checked',true); }
				else if(je58==-1){ $('input:radio[name=rje58]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje58]:nth(0)').attr('checked',false);$('input:radio[name=rje58]:nth(1)').attr('checked',false);$('input:radio[name=rje58]:nth(2)').attr('checked',false); }
				$("#je59").val($(this).data('je59'));je59=$(this).data('je59');
				if(je59==1){ $('input:radio[name=rje59]:nth(0)').attr('checked',true); }
				else if(je59==0){ $('input:radio[name=rje59]:nth(1)').attr('checked',true); }
				else if(je59==-1){ $('input:radio[name=rje59]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje59]:nth(0)').attr('checked',false);$('input:radio[name=rje59]:nth(1)').attr('checked',false);$('input:radio[name=rje59]:nth(2)').attr('checked',false); }
				$("#je60").val($(this).data('je60'));je60=$(this).data('je60');
				if(je60==1){ $('input:radio[name=rje60]:nth(0)').attr('checked',true); }
				else if(je60==0){ $('input:radio[name=rje60]:nth(1)').attr('checked',true); }
				else if(je60==-1){ $('input:radio[name=rje60]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje60]:nth(0)').attr('checked',false);$('input:radio[name=rje60]:nth(1)').attr('checked',false);$('input:radio[name=rje60]:nth(2)').attr('checked',false); }
				$("#je61").val($(this).data('je61'));je61=$(this).data('je61');
				if(je61==1){ $('input:radio[name=rje61]:nth(0)').attr('checked',true); }
				else if(je61==0){ $('input:radio[name=rje61]:nth(1)').attr('checked',true); }
				else if(je61==-1){ $('input:radio[name=rje61]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rje61]:nth(0)').attr('checked',false);$('input:radio[name=rje61]:nth(1)').attr('checked',false);$('input:radio[name=rje61]:nth(2)').attr('checked',false); }
	    	});
	      }
		
	});
	$("#tabla_ma").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpiman2/tabla',
		filters:['tablama','fecje'],
		sort:false,	
		onSuccess:function(){   						
			$('#tabla_ma tr').map(function(){
				$("#idma").val($(this).data('nma'));$("#nadr2").val($(this).data('nma'));
				$("#ma1").val($(this).data('ma1'));ma1=$(this).data('ma1');
				if(ma1==1){ $('input:radio[name=rma1]:nth(0)').attr('checked',true); }
				else if(ma1==0){ $('input:radio[name=rma1]:nth(1)').attr('checked',true); }
				else if(ma1==-1){ $('input:radio[name=rma1]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma1]:nth(0)').attr('checked',false);$('input:radio[name=rma1]:nth(1)').attr('checked',false);$('input:radio[name=rma1]:nth(2)').attr('checked',false); }
				$("#ma2").val($(this).data('ma2'));ma2=$(this).data('ma2');
				if(ma2==1){ $('input:radio[name=rma2]:nth(0)').attr('checked',true); }
				else if(ma2==0){ $('input:radio[name=rma2]:nth(1)').attr('checked',true); }
				else if(ma2==-1){ $('input:radio[name=rma2]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma2]:nth(0)').attr('checked',false);$('input:radio[name=rma2]:nth(1)').attr('checked',false);$('input:radio[name=rma2]:nth(2)').attr('checked',false); }
				$("#ma3").val($(this).data('ma3'));ma3=$(this).data('ma3');
				if(ma3==1){ $('input:radio[name=rma3]:nth(0)').attr('checked',true); }
				else if(ma3==0){ $('input:radio[name=rma3]:nth(1)').attr('checked',true); }
				else if(ma3==-1){ $('input:radio[name=rma3]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma3]:nth(0)').attr('checked',false);$('input:radio[name=rma3]:nth(1)').attr('checked',false);$('input:radio[name=rma3]:nth(2)').attr('checked',false); }
				$("#ma4").val($(this).data('ma4'));ma4=$(this).data('ma4');
				if(ma4==1){ $('input:radio[name=rma4]:nth(0)').attr('checked',true); }
				else if(ma4==0){ $('input:radio[name=rma4]:nth(1)').attr('checked',true); }
				else if(ma4==-1){ $('input:radio[name=rma4]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma4]:nth(0)').attr('checked',false);$('input:radio[name=rma4]:nth(1)').attr('checked',false);$('input:radio[name=rma4]:nth(2)').attr('checked',false); }
				$("#ma5").val($(this).data('ma5'));ma5=$(this).data('ma5');
				if(ma5==1){ $('input:radio[name=rma5]:nth(0)').attr('checked',true); }
				else if(ma5==0){ $('input:radio[name=rma5]:nth(1)').attr('checked',true); }
				else if(ma5==-1){ $('input:radio[name=rma5]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma5]:nth(0)').attr('checked',false);$('input:radio[name=rma5]:nth(1)').attr('checked',false);$('input:radio[name=rma5]:nth(2)').attr('checked',false); }
				$("#ma6").val($(this).data('ma6'));ma6=$(this).data('ma6');
				if(ma6==1){ $('input:radio[name=rma6]:nth(0)').attr('checked',true); }
				else if(ma6==0){ $('input:radio[name=rma6]:nth(1)').attr('checked',true); }
				else if(ma6==-1){ $('input:radio[name=rma6]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma6]:nth(0)').attr('checked',false);$('input:radio[name=rma6]:nth(1)').attr('checked',false);$('input:radio[name=rma6]:nth(2)').attr('checked',false); }
				$("#ma7").val($(this).data('ma7'));ma7=$(this).data('ma7');
				if(ma7==1){ $('input:radio[name=rma7]:nth(0)').attr('checked',true); }
				else if(ma7==0){ $('input:radio[name=rma7]:nth(1)').attr('checked',true); }
				else if(ma7==-1){ $('input:radio[name=rma7]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma7]:nth(0)').attr('checked',false);$('input:radio[name=rma7]:nth(1)').attr('checked',false);$('input:radio[name=rma7]:nth(2)').attr('checked',false); }
				$("#ma8").val($(this).data('ma8'));ma8=$(this).data('ma8');
				if(ma8==1){ $('input:radio[name=rma8]:nth(0)').attr('checked',true); }
				else if(ma8==0){ $('input:radio[name=rma8]:nth(1)').attr('checked',true); }
				else if(ma8==-1){ $('input:radio[name=rma8]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma8]:nth(0)').attr('checked',false);$('input:radio[name=rma8]:nth(1)').attr('checked',false);$('input:radio[name=rma8]:nth(2)').attr('checked',false); }
				$("#ma9").val($(this).data('ma9'));ma9=$(this).data('ma9');
				if(ma9==1){ $('input:radio[name=rma9]:nth(0)').attr('checked',true); }
				else if(ma9==0){ $('input:radio[name=rma9]:nth(1)').attr('checked',true); }
				else if(ma9==-1){ $('input:radio[name=rma9]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma9]:nth(0)').attr('checked',false);$('input:radio[name=rma9]:nth(1)').attr('checked',false);$('input:radio[name=rma9]:nth(2)').attr('checked',false); }
				$("#ma10").val($(this).data('ma10'));ma10=$(this).data('ma10');
				if(ma10==1){ $('input:radio[name=rma10]:nth(0)').attr('checked',true); }
				else if(ma10==0){ $('input:radio[name=rma10]:nth(1)').attr('checked',true); }
				else if(ma10==-1){ $('input:radio[name=rma10]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma10]:nth(0)').attr('checked',false);$('input:radio[name=rma10]:nth(1)').attr('checked',false);$('input:radio[name=rma10]:nth(2)').attr('checked',false); }
				$("#ma11").val($(this).data('ma11'));ma11=$(this).data('ma11');
				if(ma11==1){ $('input:radio[name=rma11]:nth(0)').attr('checked',true); }
				else if(ma11==0){ $('input:radio[name=rma11]:nth(1)').attr('checked',true); }
				else if(ma11==-1){ $('input:radio[name=rma11]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma11]:nth(0)').attr('checked',false);$('input:radio[name=rma11]:nth(1)').attr('checked',false);$('input:radio[name=rma11]:nth(2)').attr('checked',false); }
				$("#ma12").val($(this).data('ma12'));ma12=$(this).data('ma12');
				if(ma12==1){ $('input:radio[name=rma12]:nth(0)').attr('checked',true); }
				else if(ma12==0){ $('input:radio[name=rma12]:nth(1)').attr('checked',true); }
				else if(ma12==-1){ $('input:radio[name=rma12]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rma12]:nth(0)').attr('checked',false);$('input:radio[name=rma12]:nth(1)').attr('checked',false);$('input:radio[name=rma12]:nth(2)').attr('checked',false); }
	    	});
	      }
		
	});
	$("#tabla_ze").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpiman2/tabla',
		filters:['tablaze','fecje'],
		sort:false,	
		onSuccess:function(){   						
			$('#tabla_ze tr').map(function(){
				$("#idze").val($(this).data('nblw2'));$("#nadr3").val($(this).data('nblw2'));
				$("#ze1").val($(this).data('ze1'));ze1=$(this).data('ze1');
				if(ze1==1){ $('input:radio[name=rze1]:nth(0)').attr('checked',true); }
				else if(ze1==0){ $('input:radio[name=rze1]:nth(1)').attr('checked',true); }
				else if(ze1==-1){ $('input:radio[name=rze1]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze1]:nth(0)').attr('checked',false);$('input:radio[name=rze1]:nth(1)').attr('checked',false);$('input:radio[name=rze1]:nth(2)').attr('checked',false); }
				$("#ze2").val($(this).data('ze2'));ze2=$(this).data('ze2');
				if(ze2==1){ $('input:radio[name=rze2]:nth(0)').attr('checked',true); }
				else if(ze2==0){ $('input:radio[name=rze2]:nth(1)').attr('checked',true); }
				else if(ze2==-1){ $('input:radio[name=rze2]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze2]:nth(0)').attr('checked',false);$('input:radio[name=rze2]:nth(1)').attr('checked',false);$('input:radio[name=rze2]:nth(2)').attr('checked',false); }
				$("#ze3").val($(this).data('ze3'));ze3=$(this).data('ze3');
				if(ze3==1){ $('input:radio[name=rze3]:nth(0)').attr('checked',true); }
				else if(ze3==0){ $('input:radio[name=rze3]:nth(1)').attr('checked',true); }
				else if(ze3==-1){ $('input:radio[name=rze3]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze3]:nth(0)').attr('checked',false);$('input:radio[name=rze3]:nth(1)').attr('checked',false);$('input:radio[name=rze3]:nth(2)').attr('checked',false); }
				$("#ze4").val($(this).data('ze4'));ze4=$(this).data('ze4');
				if(ze4==1){ $('input:radio[name=rze4]:nth(0)').attr('checked',true); }
				else if(ze4==0){ $('input:radio[name=rze4]:nth(1)').attr('checked',true); }
				else if(ze4==-1){ $('input:radio[name=rze4]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze4]:nth(0)').attr('checked',false);$('input:radio[name=rze4]:nth(1)').attr('checked',false);$('input:radio[name=rze4]:nth(2)').attr('checked',false); }
				$("#ze5").val($(this).data('ze5'));ze5=$(this).data('ze5');
				if(ze5==1){ $('input:radio[name=rze5]:nth(0)').attr('checked',true); }
				else if(ze5==0){ $('input:radio[name=rze5]:nth(1)').attr('checked',true); }
				else if(ze5==-1){ $('input:radio[name=rze5]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze5]:nth(0)').attr('checked',false);$('input:radio[name=rze5]:nth(1)').attr('checked',false);$('input:radio[name=rze5]:nth(2)').attr('checked',false); }
				$("#ze6").val($(this).data('ze6'));ze6=$(this).data('ze6');
				if(ze6==1){ $('input:radio[name=rze6]:nth(0)').attr('checked',true); }
				else if(ze6==0){ $('input:radio[name=rze6]:nth(1)').attr('checked',true); }
				else if(ze6==-1){ $('input:radio[name=rze6]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze6]:nth(0)').attr('checked',false);$('input:radio[name=rze6]:nth(1)').attr('checked',false);$('input:radio[name=rze6]:nth(2)').attr('checked',false); }
				$("#ze7").val($(this).data('ze7'));ze7=$(this).data('ze7');
				if(ze7==1){ $('input:radio[name=rze7]:nth(0)').attr('checked',true); }
				else if(ze7==0){ $('input:radio[name=rze7]:nth(1)').attr('checked',true); }
				else if(ze7==-1){ $('input:radio[name=rze7]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze7]:nth(0)').attr('checked',false);$('input:radio[name=rze7]:nth(1)').attr('checked',false);$('input:radio[name=rze7]:nth(2)').attr('checked',false); }
				$("#ze8").val($(this).data('ze8'));ze8=$(this).data('ze8');
				if(ze8==1){ $('input:radio[name=rze8]:nth(0)').attr('checked',true); }
				else if(ze8==0){ $('input:radio[name=rze8]:nth(1)').attr('checked',true); }
				else if(ze8==-1){ $('input:radio[name=rze8]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze8]:nth(0)').attr('checked',false);$('input:radio[name=rze8]:nth(1)').attr('checked',false);$('input:radio[name=rze8]:nth(2)').attr('checked',false); }
				$("#ze9").val($(this).data('ze9'));ze9=$(this).data('ze9');
				if(ze9==1){ $('input:radio[name=rze9]:nth(0)').attr('checked',true); }
				else if(ze9==0){ $('input:radio[name=rze9]:nth(1)').attr('checked',true); }
				else if(ze9==-1){ $('input:radio[name=rze9]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze9]:nth(0)').attr('checked',false);$('input:radio[name=rze9]:nth(1)').attr('checked',false);$('input:radio[name=rze9]:nth(2)').attr('checked',false); }
				$("#ze10").val($(this).data('ze10'));ze10=$(this).data('ze10');
				if(ze10==1){ $('input:radio[name=rze10]:nth(0)').attr('checked',true); }
				else if(ze10==0){ $('input:radio[name=rze10]:nth(1)').attr('checked',true); }
				else if(ze10==-1){ $('input:radio[name=rze10]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze10]:nth(0)').attr('checked',false);$('input:radio[name=rze10]:nth(1)').attr('checked',false);$('input:radio[name=rze10]:nth(2)').attr('checked',false); }
				$("#ze11").val($(this).data('ze11'));ze11=$(this).data('ze11');
				if(ze11==1){ $('input:radio[name=rze11]:nth(0)').attr('checked',true); }
				else if(ze11==0){ $('input:radio[name=rze11]:nth(1)').attr('checked',true); }
				else if(ze11==-1){ $('input:radio[name=rze11]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze11]:nth(0)').attr('checked',false);$('input:radio[name=rze11]:nth(1)').attr('checked',false);$('input:radio[name=rze11]:nth(2)').attr('checked',false); }
				$("#ze12").val($(this).data('ze12'));ze12=$(this).data('ze12');
				if(ze12==1){ $('input:radio[name=rze12]:nth(0)').attr('checked',true); }
				else if(ze12==0){ $('input:radio[name=rze12]:nth(1)').attr('checked',true); }
				else if(ze12==-1){ $('input:radio[name=rze12]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze12]:nth(0)').attr('checked',false);$('input:radio[name=rze12]:nth(1)').attr('checked',false);$('input:radio[name=rze12]:nth(2)').attr('checked',false); }
				$("#ze13").val($(this).data('ze13'));ze13=$(this).data('ze13');
				if(ze13==1){ $('input:radio[name=rze13]:nth(0)').attr('checked',true); }
				else if(ze13==0){ $('input:radio[name=rze13]:nth(1)').attr('checked',true); }
				else if(ze13==-1){ $('input:radio[name=rze13]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze13]:nth(0)').attr('checked',false);$('input:radio[name=rze13]:nth(1)').attr('checked',false);$('input:radio[name=rze13]:nth(2)').attr('checked',false); }
				$("#ze14").val($(this).data('ze14'));ze14=$(this).data('ze14');
				if(ze14==1){ $('input:radio[name=rze14]:nth(0)').attr('checked',true); }
				else if(ze14==0){ $('input:radio[name=rze14]:nth(1)').attr('checked',true); }
				else if(ze14==-1){ $('input:radio[name=rze14]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze14]:nth(0)').attr('checked',false);$('input:radio[name=rze14]:nth(1)').attr('checked',false);$('input:radio[name=rze14]:nth(2)').attr('checked',false); }
				$("#ze15").val($(this).data('ze15'));ze15=$(this).data('ze15');
				if(ze15==1){ $('input:radio[name=rze15]:nth(0)').attr('checked',true); }
				else if(ze15==0){ $('input:radio[name=rze15]:nth(1)').attr('checked',true); }
				else if(ze15==-1){ $('input:radio[name=rze15]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze15]:nth(0)').attr('checked',false);$('input:radio[name=rze15]:nth(1)').attr('checked',false);$('input:radio[name=rze15]:nth(2)').attr('checked',false); }
				$("#ze16").val($(this).data('ze16'));ze16=$(this).data('ze16');
				if(ze16==1){ $('input:radio[name=rze16]:nth(0)').attr('checked',true); }
				else if(ze16==0){ $('input:radio[name=rze16]:nth(1)').attr('checked',true); }
				else if(ze16==-1){ $('input:radio[name=rze16]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze16]:nth(0)').attr('checked',false);$('input:radio[name=rze16]:nth(1)').attr('checked',false);$('input:radio[name=rze16]:nth(2)').attr('checked',false); }
				$("#ze17").val($(this).data('ze17'));ze17=$(this).data('ze17');
				if(ze17==1){ $('input:radio[name=rze17]:nth(0)').attr('checked',true); }
				else if(ze17==0){ $('input:radio[name=rze17]:nth(1)').attr('checked',true); }
				else if(ze17==-1){ $('input:radio[name=rze17]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze17]:nth(0)').attr('checked',false);$('input:radio[name=rze17]:nth(1)').attr('checked',false);$('input:radio[name=rze17]:nth(2)').attr('checked',false); }
				$("#ze18").val($(this).data('ze18'));ze18=$(this).data('ze18');
				if(ze18==1){ $('input:radio[name=rze18]:nth(0)').attr('checked',true); }
				else if(ze18==0){ $('input:radio[name=rze18]:nth(1)').attr('checked',true); }
				else if(ze18==-1){ $('input:radio[name=rze18]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze18]:nth(0)').attr('checked',false);$('input:radio[name=rze18]:nth(1)').attr('checked',false);$('input:radio[name=rze18]:nth(2)').attr('checked',false); }
				$("#ze19").val($(this).data('ze19'));ze19=$(this).data('ze19');
				if(ze19==1){ $('input:radio[name=rze19]:nth(0)').attr('checked',true); }
				else if(ze19==0){ $('input:radio[name=rze19]:nth(1)').attr('checked',true); }
				else if(ze19==-1){ $('input:radio[name=rze19]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze19]:nth(0)').attr('checked',false);$('input:radio[name=rze19]:nth(1)').attr('checked',false);$('input:radio[name=rze19]:nth(2)').attr('checked',false); }
				$("#ze20").val($(this).data('ze20'));ze20=$(this).data('ze20');
				if(ze20==1){ $('input:radio[name=rze20]:nth(0)').attr('checked',true); }
				else if(ze20==0){ $('input:radio[name=rze20]:nth(1)').attr('checked',true); }
				else if(ze20==-1){ $('input:radio[name=rze20]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze20]:nth(0)').attr('checked',false);$('input:radio[name=rze20]:nth(1)').attr('checked',false);$('input:radio[name=rze20]:nth(2)').attr('checked',false); }
				$("#ze21").val($(this).data('ze21'));ze21=$(this).data('ze21');
				if(ze21==1){ $('input:radio[name=rze21]:nth(0)').attr('checked',true); }
				else if(ze21==0){ $('input:radio[name=rze21]:nth(1)').attr('checked',true); }
				else if(ze21==-1){ $('input:radio[name=rze21]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze21]:nth(0)').attr('checked',false);$('input:radio[name=rze21]:nth(1)').attr('checked',false);$('input:radio[name=rze21]:nth(2)').attr('checked',false); }
				$("#ze22").val($(this).data('ze22'));ze22=$(this).data('ze22');
				if(ze22==1){ $('input:radio[name=rze22]:nth(0)').attr('checked',true); }
				else if(ze22==0){ $('input:radio[name=rze22]:nth(1)').attr('checked',true); }
				else if(ze22==-1){ $('input:radio[name=rze22]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze22]:nth(0)').attr('checked',false);$('input:radio[name=rze22]:nth(1)').attr('checked',false);$('input:radio[name=rze22]:nth(2)').attr('checked',false); }
				$("#ze23").val($(this).data('ze23'));ze23=$(this).data('ze23');
				if(ze23==1){ $('input:radio[name=rze23]:nth(0)').attr('checked',true); }
				else if(ze23==0){ $('input:radio[name=rze23]:nth(1)').attr('checked',true); }
				else if(ze23==-1){ $('input:radio[name=rze23]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze23]:nth(0)').attr('checked',false);$('input:radio[name=rze23]:nth(1)').attr('checked',false);$('input:radio[name=rze23]:nth(2)').attr('checked',false); }
				$("#ze24").val($(this).data('ze24'));ze24=$(this).data('ze24');
				if(ze24==1){ $('input:radio[name=rze24]:nth(0)').attr('checked',true); }
				else if(ze24==0){ $('input:radio[name=rze24]:nth(1)').attr('checked',true); }
				else if(ze24==-1){ $('input:radio[name=rze24]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze24]:nth(0)').attr('checked',false);$('input:radio[name=rze24]:nth(1)').attr('checked',false);$('input:radio[name=rze24]:nth(2)').attr('checked',false); }
				$("#ze25").val($(this).data('ze25'));ze25=$(this).data('ze25');
				if(ze25==1){ $('input:radio[name=rze25]:nth(0)').attr('checked',true); }
				else if(ze25==0){ $('input:radio[name=rze25]:nth(1)').attr('checked',true); }
				else if(ze25==-1){ $('input:radio[name=rze25]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze25]:nth(0)').attr('checked',false);$('input:radio[name=rze25]:nth(1)').attr('checked',false);$('input:radio[name=rze25]:nth(2)').attr('checked',false); }
				$("#ze26").val($(this).data('ze26'));ze26=$(this).data('ze26');
				if(ze26==1){ $('input:radio[name=rze26]:nth(0)').attr('checked',true); }
				else if(ze26==0){ $('input:radio[name=rze26]:nth(1)').attr('checked',true); }
				else if(ze26==-1){ $('input:radio[name=rze26]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze26]:nth(0)').attr('checked',false);$('input:radio[name=rze26]:nth(1)').attr('checked',false);$('input:radio[name=rze26]:nth(2)').attr('checked',false); }
				$("#ze27").val($(this).data('ze27'));ze27=$(this).data('ze27');
				if(ze27==1){ $('input:radio[name=rze27]:nth(0)').attr('checked',true); }
				else if(ze27==0){ $('input:radio[name=rze27]:nth(1)').attr('checked',true); }
				else if(ze27==-1){ $('input:radio[name=rze27]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze27]:nth(0)').attr('checked',false);$('input:radio[name=rze27]:nth(1)').attr('checked',false);$('input:radio[name=rze27]:nth(2)').attr('checked',false); }
				$("#ze28").val($(this).data('ze28'));ze28=$(this).data('ze28');
				if(ze28==1){ $('input:radio[name=rze28]:nth(0)').attr('checked',true); }
				else if(ze28==0){ $('input:radio[name=rze28]:nth(1)').attr('checked',true); }
				else if(ze28==-1){ $('input:radio[name=rze28]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rze28]:nth(0)').attr('checked',false);$('input:radio[name=rze28]:nth(1)').attr('checked',false);$('input:radio[name=rze28]:nth(2)').attr('checked',false); }				
	    	});
	      }
		
	});
	
	$("#tabla_puntas2").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpiman2/tablakpip2', 
		filters:['cmbMeskpip2'],
		sort:false,	
		onRowClick:function(){
			$("#idbom2").val($(this).data('idbom2'));
			$("#fecbom2").val($(this).data('fecbom2'));
       		$("#zba1").val($(this).data('zba1'));
       		$("#zba2").val($(this).data('zba2'));
       		$("#zba3").val($(this).data('zba3'));
       		$("#zba4").val($(this).data('zba4'));
       		$("#zba5").val($(this).data('zba5'));
       		$("#zba6").val($(this).data('zba6'));
       		$("#zba7").val($(this).data('zba7'));
       		$("#zba8").val($(this).data('zba8'));
       		$("#zbb1").val($(this).data('zbb1'));
       		$("#zbb2").val($(this).data('zbb2'));
       		$("#zbb3").val($(this).data('zbb3'));
       		$("#zbb4").val($(this).data('zbb4'));
       		
   		},   
		onSuccess:function(){ 
			$('#tabla_puntas2 tbody tr td:nth-child(18)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 1800.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 1400.0 && $(this).html() <= 1799.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() <= 1399.9 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "white");
        			}
        			
	    	})
	    	$('#tabla_puntas2 tbody tr td:nth-child(27)').map(function(){ 
	    		$(this).css('font-weight','bold');
	    			if($(this).html() >= 1350.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 1050.0 && $(this).html() <= 1349.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() <= 1049.9 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "white");
        			}
        			
	    	}) 
	    	$('#tabla_puntas2 tbody tr td:nth-child(28)').map(function(){ 
	    		$(this).css('font-weight','bold');
	    			if($(this).html() >= 3150.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 2450.0 && $(this).html() <= 3149.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() <= 2449.9 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "white");
        			}
        			
	    	})  
			$('#tabla_puntas2 tbody tr td:nth-child(29)').map(function(){ 
	    			$(this).css('font-weight','bold'); 
	    			if($(this).html() >= 90.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
            		if($(this).html() >= 70.0 && $(this).html() <= 89.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
            		if($(this).html() >= 2 &&  $(this).html() <= 69.9 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "white");
            		}   
        			
	    	})
			//grafica
			$("#zg1").val(0);$("#zg2").val(0);$("#zg3").val(0);$("#zg4").val(0);$("#zg5").val(0);$("#zg6").val(0);$("#zg7").val(0);$("#zg8").val(0);
			$("#zg9").val(0);$("#zg10").val(0);
			$("#zg11").val(0);$("#zg12").val(0);$("#zg13").val(0);$("#zg14").val(0);$("#zg15").val(0);$("#zg16").val(0);$("#zg17").val(0);$("#zg18").val(0);
			$("#zg19").val(0);$("#zg20").val(0);
			$("#zg21").val(0);$("#zg22").val(0);$("#zg23").val(0);$("#zg24").val(0);$("#zg25").val(0);$("#zg26").val(0);$("#zg27").val(0);$("#zg28").val(0);
			$("#zg29").val(0);$("#zg30").val(0);$("#zg31").val(0);
	    	$('#tabla_puntas2 tbody tr').map(function(){
	    		if($(this).data('zg1')>0){$("#zg1").val($(this).data('zg1'));}
	    		if($(this).data('zg2')>0){$("#zg2").val($(this).data('zg2'));}
	    		if($(this).data('zg3')>0){$("#zg3").val($(this).data('zg3'));}
	    		if($(this).data('zg4')>0){$("#zg4").val($(this).data('zg4'));}
	    		if($(this).data('zg5')>0){$("#zg5").val($(this).data('zg5'));}
	    		if($(this).data('zg6')>0){$("#zg6").val($(this).data('zg6'));}
	    		if($(this).data('zg7')>0){$("#zg7").val($(this).data('zg7'));}
	    		if($(this).data('zg8')>0){$("#zg8").val($(this).data('zg8'));}
	    		if($(this).data('zg9')>0){$("#zg9").val($(this).data('zg9'));}
	    		if($(this).data('zg10')>0){$("#zg10").val($(this).data('zg10'));}
	    		if($(this).data('zg11')>0){$("#zg11").val($(this).data('zg11'));}
	    		if($(this).data('zg12')>0){$("#zg12").val($(this).data('zg12'));}
	    		if($(this).data('zg13')>0){$("#zg13").val($(this).data('zg13'));}
	    		if($(this).data('zg14')>0){$("#zg14").val($(this).data('zg14'));}
	    		if($(this).data('zg15')>0){$("#zg15").val($(this).data('zg15'));}
	    		if($(this).data('zg16')>0){$("#zg16").val($(this).data('zg16'));}
	    		if($(this).data('zg17')>0){$("#zg17").val($(this).data('zg17'));}
	    		if($(this).data('zg18')>0){$("#zg18").val($(this).data('zg18'));}
	    		if($(this).data('zg19')>0){$("#zg19").val($(this).data('zg19'));}
	    		if($(this).data('zg20')>0){$("#zg20").val($(this).data('zg20'));}
	    		if($(this).data('zg21')>0){$("#zg21").val($(this).data('zg21'));}
	    		if($(this).data('zg22')>0){$("#zg22").val($(this).data('zg22'));}
	    		if($(this).data('zg23')>0){$("#zg23").val($(this).data('zg23'));}
	    		if($(this).data('zg24')>0){$("#zg24").val($(this).data('zg24'));}
	    		if($(this).data('zg25')>0){$("#zg25").val($(this).data('zg25'));}
	    		if($(this).data('zg26')>0){$("#zg26").val($(this).data('zg26'));}
	    		if($(this).data('zg27')>0){$("#zg27").val($(this).data('zg27'));}
	    		if($(this).data('zg28')>0){$("#zg28").val($(this).data('zg28'));}
	    		if($(this).data('zg29')>0){$("#zg29").val($(this).data('zg29'));}
	    		if($(this).data('zg30')>0){$("#zg30").val($(this).data('zg30'));}
	    		if($(this).data('zg31')>0){$("#zg31").val($(this).data('zg31'));}
	    		m29=0;m30=0;m31=0;
	    		m1=parseFloat($(this).data('m1'));m2=parseFloat($(this).data('m2'));m3=parseFloat($(this).data('m3'));m4=parseFloat($(this).data('m4'));m5=parseFloat($(this).data('m5'));
	    		m6=parseFloat($(this).data('m6'));m7=parseFloat($(this).data('m7'));m8=parseFloat($(this).data('m8'));m9=parseFloat($(this).data('m9'));m10=parseFloat($(this).data('m10'));
	    		m11=parseFloat($(this).data('m11'));m12=parseFloat($(this).data('m12'));m13=parseFloat($(this).data('m13'));m14=parseFloat($(this).data('m14'));m15=parseFloat($(this).data('m15'));
	    		m16=parseFloat($(this).data('m16'));m17=parseFloat($(this).data('m17'));m18=parseFloat($(this).data('m18'));m19=parseFloat($(this).data('m19'));m20=parseFloat($(this).data('m20'));
	    		m21=parseFloat($(this).data('m21'));m22=parseFloat($(this).data('m22'));m23=parseFloat($(this).data('m23'));m24=parseFloat($(this).data('m24'));m25=parseFloat($(this).data('m25'));
	    		m26=parseFloat($(this).data('m26'));m27=parseFloat($(this).data('m27'));m28=parseFloat($(this).data('m28'));
	    		if($(this).data('m29')>0){m29=parseFloat($(this).data('m29'));}
	    		if($(this).data('m30')>0){m30=parseFloat($(this).data('m30'));}
	    		if($(this).data('m31')>0){m31=parseFloat($(this).data('m31'));}
	    	});		
				var chart
			g1=$("#zg1").val(); g1=parseFloat(g1);g2=$("#zg2").val(); g2=parseFloat(g2);g3=$("#zg3").val(); g3=parseFloat(g3);g4=$("#zg4").val(); g4=parseFloat(g4);g5=$("#zg5").val(); g5=parseFloat(g5);
			g6=$("#zg6").val(); g6=parseFloat(g6);g7=$("#zg7").val(); g7=parseFloat(g7);g8=$("#zg8").val(); g8=parseFloat(g8);g9=$("#zg9").val(); g9=parseFloat(g9);;g10=$("#zg10").val(); g10=parseFloat(g10);
			g11=$("#zg11").val(); g11=parseFloat(g11);g12=$("#zg12").val(); g12=parseFloat(g12);g13=$("#zg13").val(); g13=parseFloat(g13);g14=$("#zg14").val(); g14=parseFloat(g14);g15=$("#zg15").val(); g15=parseFloat(g15);
			g16=$("#zg16").val(); g16=parseFloat(g16);g17=$("#zg17").val(); g17=parseFloat(g17);g18=$("#zg18").val(); g18=parseFloat(g18);g19=$("#zg19").val(); g19=parseFloat(g19);g20=$("#zg20").val(); g20=parseFloat(g20);
			g21=$("#zg21").val(); g21=parseFloat(g21);g22=$("#zg22").val(); g22=parseFloat(g22);g23=$("#zg23").val(); g23=parseFloat(g23);g24=$("#zg24").val(); g24=parseFloat(g24);g25=$("#zg25").val(); g25=parseFloat(g25);
			g26=$("#zg26").val(); g26=parseFloat(g26);g27=$("#zg27").val(); g27=parseFloat(g27);g28=$("#zg28").val(); g28=parseFloat(g28);g29=$("#zg29").val(); g29=parseFloat(g29);;g30=$("#zg30").val(); g30=parseFloat(g30);
			g31=$("#zg31").val(); g31=parseFloat(g31);
			chart = new Highcharts.Chart({
            chart: {
                renderTo: 'agualab2',
                zoomType: 'xy',
                //animation: {
         		//	    duration: 1500,
           		//		easing: 'easeOutBounce'
        		//}
        		
            },
            title: { text: '' },
            subtitle: { text: '' },
            xAxis: [{
                categories: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
                text: 'Días de Cultivo'
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    formatter: function() {  return this.value +'lts';  },
                    style: {  color: '#89A54E' }
                },
                title: { //Demanda
                    text: '',
                    style: { color: '#0F0'}
                },
                opposite: true
    
            }, { // Secondary yAxis Actual
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {  color: '#4572A7' }
                },
                labels: {
                    formatter: function() {  return this.value +' lts';  },
                    style: { color: '#4572A7' }
                }
    		}, { // Meta
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: { color: 'blue' }
                },
            }, { // Correctivo
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: { color: '#AA4643' }
                },
           }, { // Preventivo
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: { color: 'orange' }
                },     
                
            
            }, { // Marea
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: { color: 'black'  }
                },     
                labels: {
                    formatter: function() {
                        return this.value +' cms';
                    },
                    style: { color: '#AA4643' }
                },
                opposite: true
            }],
            tooltip: {
                formatter: function() {
                    var unit = {
                        'Actual': 'lts', 'Meta': 'lts', 'Demanda': 'lts','Preventivo': 'lts', 'Correctivo': 'lts',
                        'Marea': 'cms', 'Alta': 'cms', 'Baja': 'cms',
                    }[this.series.name];
    
                    return ''+
                        this.x +': '+ this.y +' '+ unit;
                }
            },
           
            series: [{
                name: 'Actual',
                color: '#4572A7',
                type: 'area',
                yAxis: 1,
                data: [ g1, g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12, g13, g14, g15, g16, g17, g18, g19, g20, g21, g22, g23, g24, g25, g26, g27, g28, g29, g30, g31],
    			dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: 'black',
                    align: 'right',
                    x: 8,
                    y: 5,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
               	},
                animation: {
                	duration: 2000,
                	easing: 'easeInOutBounce',
	            },
               
               }, {
           	    	name: 'Marea',
            	    color: 'lightblue',
                	type: 'spline',
                	yAxis: 2,
                	data: [	m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16, m17, m18, m19, m20, m21, m22, m23, m24, m25, m26, m27, m28, m29, m30, m31],
                	marker: {
                    enabled: false,
                    },
                    dataLabels: {
                    	enabled: true,
                    	rotation: -90,
                    	color: 'black',
                    	align: 'right',
                    	style: {
                        	fontSize: '7px',
                        	fontFamily: 'Verdana, sans-serif'
                    	}
               		},
                	dashStyle: 'largedot'
           		},{
                name: 'Correctivo',
                type: 'spline',
                color: '#AA4643',
                yAxis: 1,
               data: [	2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450, 2450],
                marker: {
                    enabled: false
                },
                dashStyle: 'largedot'
    		 	}, {
                name: 'Preventivo',
                type: 'spline',
                color: 'orange',
                yAxis: 1,
                data: [	3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150],
                marker: {
                    enabled: false
                },
                dashStyle: 'largedot'
            	}, {
                name: 'Demanda',
                color: '#0F0',
                type: 'spline',
                yAxis: 1,
                data: [	3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500, 3500],
            	marker: {
                    enabled: false
                },
                dashStyle: 'largedot'
            }, {
           	    	name: 'Meta',
            	    color: 'blue',
                	type: 'spline',
                	yAxis: 1,
                	data: [	4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000],
                	marker: {
                    enabled: false
                },
                dashStyle: 'largedot'
            }, {
           	    	name: 'Alta',
            	    color: 'lightblue',
                	type: 'spline',
                	yAxis: 2,
                	data: [	120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120],
                	marker: {
                    enabled: false
                },
                dashStyle: 'largedot'
           	}, {
           	    	name: 'Baja',
            	    color: 'lightblue',
                	type: 'spline',
                	yAxis: 2,
                	data: [	60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
                	marker: {
                    enabled: false
                },
                dashStyle: 'largedot'
           	}    
           
            ]
        });	
	    	
												
		},	
		
		
	});		
});
</script>