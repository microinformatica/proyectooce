<?php $this->load->view('header_cv');

?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 25px;
	/*background: none;*/		
}   

</style>

<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li style="font-size: 20px"><a href="#entregas"><img src="<?php echo base_url();?>assets/images/menu/tecnicos.png" width="25" height="25" border="0"> Periodicidad de Entregas </a></li>					
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="entregas" class="tab_content" >
			<div style="height: 350px;">				
				<div class="ajaxSorterDiv" id="tabla_teci" name="tabla_teci" style="height: 290px; ">                
               		<div class="ajaxpager" style="margin-top: 0px" >        
	                   	<ul class="order_list" >En Total: <input size="2%"  type="text" name="tote" id="tote" style="text-align: left;  border: 0; background: none"> </ul>     
	                   	Responsable:
                		<select name="cmbEnc" id="cmbEnc" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<?php	
          					//$this->load->model('edoctatec_model');
          					$data['result']=$this->tecnicos_model->verTecnico();
							foreach ($data['result'] as $row):?>
           					<option value="<?php echo $row->NumBio;?>" ><?php echo $row->NomBio;?></option>
           					<?php endforeach;?>
   						</select>
   						
                		<form method="post" action="<?= base_url()?>index.php/tecnicos/pdfrep" >
                			<input type="hidden" name="totel" id="totel">
                			<input type="hidden" id="tecnico" name="tecnico" />
                			<img title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png"  />                                                                      
                        	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                             
 						</form>   
                	</div>  
               		<span class="ajaxTable" style="height: 240px; background-color: white; width: 870px; margin-top: -23px" >
            	       	<table id="mytablaT" name="mytablaT" class="ajaxSorter" >
                        	<thead title="Presione las columnas para ordenarlas como requiera">
                        		<th data-order = "mes">Mes</th>
                        		<th data-order = "d1">1</th><th data-order = "d2">2</th><th data-order = "d3">3</th>
                        		<th data-order = "d4">4</th><th data-order = "d5">5</th><th data-order = "d6">6</th>
                        		<th data-order = "d7">7</th><th data-order = "d8">8</th><th data-order = "d9">9</th><th data-order = "d10">10</th>
                        		<th data-order = "d11">11</th><th data-order = "d12">12</th><th data-order = "d13">13</th>
                        		<th data-order = "d14">14</th><th data-order = "d15">15</th><th data-order = "d16">16</th>
                        		<th data-order = "d17">17</th><th data-order = "d18">18</th><th data-order = "d19">19</th><th data-order = "d20">20</th>
                        		<th data-order = "d21">21</th><th data-order = "d22">22</th><th data-order = "d23">23</th>
                        		<th data-order = "d24">24</th><th data-order = "d25">25</th><th data-order = "d26">26</th>
                        		<th data-order = "d27">27</th><th data-order = "d28">28</th><th data-order = "d29">29</th><th data-order = "d30">30</th>
                        		<th data-order = "d31">31</th><th data-order = "tot">Tot</th>
                        	</thead>
                        	<tbody title="Seleccione para analizar comparativo" style="font-size: 9px">
                        	</tbody>                        	
                    	</table>
                	</span> 
             		
                	                                     
            	</div>  
            	<table border="0" height="180px" width="870px" style="margin-top: -18px">
            		<tr>
            			<th style="width: 210px">
            				<div class="ajaxSorterDiv" id="tabla_totet" name="tabla_totet" s>
            					<span class="ajaxTable" style="height: 185px; background-color: white; width: 200px; margin-top: 1px" >
            	       				<table id="mytablaTotet" name="mytablaTotet" class="ajaxSorter" >
                        				<thead >
                        					<th data-order= "NomBio">Reponsable</th> <th data-order= "viajes">Entregas</th>
                        				</thead>
                        				<tbody title="Seleccione para analizar comparativo" style="font-size: 9px">
                        				</tbody>                        	
                    				</table>
                				</span> 
                			</div>
            			</th>
            			<th style="width: 210px">
            				<div class="ajaxSorterDiv" id="tabla_totdeti" name="tabla_totdeti" s>
            				    <div class="ajaxpager" style="margin-top: 0px;" > 
            						<ul class="order_list" ></ul>
            						Detalle de Entregas Totales --
            						<input type="hidden" size="50%" value="Detalle de Entregas Totales" style="margin-top: 5px; border: none" >
            						Mes:
   									<select name="cmbMes" id="cmbMes" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
   										<option value="0" >Todos</option>
   										<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   										<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   										<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   									</select>	
									<form method="post" action="<?= base_url()?>index.php/tecnicos/pdfrepdet" >
                						<input type="hidden" name="toteld" id="toteld">
                						<input type="hidden" id="tecnico1" name="tecnico1" />
                						<img title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png"  />                                                                      
                        				<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                             
 									</form> 
 								</div>  
                			
            					<span class="ajaxTable" style="height: 155px; background-color: white; width: 630px; margin-top: -23px" >
            	       				<table id="mytablaTdeti" name="mytablaTdeti" class="ajaxSorter" >
                        				<thead >
                        					<th data-order= "mesi">Mes</th> <th data-order= "RemisionR">Remision</th>
                        					<th data-order= "FechaR1">Fecha</th> <th data-order= "diasi">Dias</th>
                        					<th data-order= "Zona">Zona</th> <th data-order= "Razon">Cliente</th>
                        				</thead>
                        				<tbody title="Seleccione para analizar comparativo" style="font-size: 9px">
                        				</tbody>                        	
                    				</table>
                				</span>
                			</div>	
            			</th>
            		</tr>
            	</table>          	
            </div> 
	 	</div>
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#cmbEnc").change( function(){		
	$("#tecnico").val($("#cmbEnc").val()); $("#tecnico1").val($("#cmbEnc").val());
 return true;
});	
$(document).ready(function(){	
	$("#tecnico").val($("#cmbEnc").val());	$("#tecnico1").val($("#cmbEnc").val());
	$("#tabla_teci").ajaxSorter({
		url:'<?php echo base_url()?>index.php/tecnicos/tabla',  		
		filters:['cmbEnc'],
		sort:false,              
    	onSuccess:function(){
    		tot = 0;
    		$('#tabla_teci tbody tr').map(function(){
    			tot+=($(this).data('tot')!=' '?parseFloat($(this).data('tot')):0);
    	   		if(tot>0){ $("#tote").val(tot); $("#totel").val(tot); $("#toteld").val(tot);	} else { $("#tote").val(''); $("#totel").val(''); $("#toteld").val(''); } 
    	   });
    	},
	});
   	$("#tabla_totet").ajaxSorter({
		url:'<?php echo base_url()?>index.php/tecnicos/tablatotet',  		
		sort:false,  
		onRowClick:function(){
    		if($(this).data('numbio')>'0'){
        		$("#cmbEnc").val($(this).data('numbio'));
        		$("#cmbMes").val(0);
        		$("#tabla_teci").trigger("update");
        		$("#tabla_totdeti").trigger("update");
        		$("#tecnico").val($("#cmbEnc").val()); $("#tecnico1").val($("#cmbEnc").val());
			}		
    	},            
	}); 
	$("#tabla_totdeti").ajaxSorter({
		url:'<?php echo base_url()?>index.php/tecnicos/tablatotdeti',  		
		filters:['cmbEnc','cmbMes'],
		sort:false,
	});
});
</script>
