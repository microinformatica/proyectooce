    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 80px 50px 20px; }
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: orange; }	
</style>
<title>Reporte de Maquila</title>
</head>
<body>
	<div id="header"> 
		<table style="width: 700px;">
			<tr>
				<td width="200px">
					<p style="font-size: 8px;"  >
						<img src="<?php echo base_url();?>assets/images/bonattologotipo.jpg" width="200" height="60" border="0">
						SIA - <?= $usuario?> - <?= $perfil?> <br />
					</p>
				</td>	
				<td align="center" style="font-size: 14px; color: navy"><strong>Productos Selectos del Pacifico S.A. de C.V. <br/>Camarón Procesado</strong></td>	
					
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: navy;">
				<td width="160px">www.bonatto.com.mx</td>	
				<td width="490">
					Av. Reforma No.2007-B Int. 803, Col. Flamingos, Mazatlán, Sin. México, RFC:AQU-031003-CC9, CP:82149 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
	</div>
  
<div style="margin-left: -25px">
	<table  width="770px" border="1" >
		<tr>
			<td colspan="4" style="background-color: lightblue">I.- Registro</td>
		</tr>
		<tr>
			<td>Día</td><td>Almacen</td><td>Procedencia</td><td>Precosecha</td>
		</tr>
		<tr>
			<td style="text-align: center"><?= $dia?></td><td style="text-align: center">Selectos del Mar</td>
			<td style="text-align: center"><?= $gra?></td><td style="text-align: center"><?= $cic?></td>
		</tr>
		<tr style="height: 5px">
			<td colspan="4" style="height: 5px; background-color: lightgray"> </td>
		</tr>
		<tr>
			<td colspan="4" style="background-color: lightblue">Información de Producto</td>
		</tr>
		<tr style="background-color: lightblue">
			<td colspan="2">II.-  Recibido en Fresco</td>
			<td colspan="2">III.- Procesado</td>
		</tr>
		<tr>
			<td colspan="2">
				<table border="1"  style="font-size: 14px; text-align: right">
    	  			<?= $tablaf?>
				</table>
			</td>
			<td colspan="2" >
				<table border="1"  style="font-size: 14px; text-align: right">
    	  			<?= $tablap?>
				</table>
			</td>
		</tr>
		<tr style="height: 5px">
			<td colspan="4" style="height: 5px; background-color: lightgray"> </td>
		</tr>
		<tr>
			<td colspan="4" style="background-color: lightblue">IV.- Resultados</td>
		</tr>
		<tr>
			<td>Fresco</td><td>Procesado</td><td>Rendimiento</td><td>Importe</td>
		</tr>
		<tr>
			<td style="text-align: center"><?= $fre?></td><td style="text-align: center"><?= $pro?></td><td style="text-align: center"><?= $ren?></td><td style="text-align: center"><?= $imp?></td>
		</tr>
	</table>
	
	
</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>