<? $this->load->view('header_cv');?>


<script src="<?=base_url();?>assets/js/jquery.loader.js"></script>
<link href="<?=base_url();?>assets/css/jquery.loader.css" rel="stylesheet" />
<!--Loader-->	
<style>
.center{
	text-align: center;
}
.right{
	text-align: right;
}
.left{
	text-align: left;
}
.redondo{
	border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px; /* Borde redondo */ 
}
.foraneo{
	color:#013ADF;
}
.td_seleccion{
width:80px
}
.tdr_seleccion{
text-align: center;
}
.lista_filtro{
	background-color: #F2E0F7;
	margin-right: 10px;
	border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px; /* Borde redondo */ 
	padding:5px;
} 
.equis{
	padding-right:14px;
	padding-bottom:1px;	
	background-image:url('<?= base_url()?>assets/images/icono-cerrar.png') ;
	background-repeat:no-repeat;
}
.mUp{
	margin-top:-25px;
	margin-left:-5px;
}
.tablaNew{
	margin-left:-20px;
}
table td
{
	border-top:none
}
</style>			
<div style=" border:1px solid" class="redondo"> 
    <div style="font-size:2em; margin-bottom:-5px;" class="redondo" align="center">Reporte aplicador bancario</div>
    <div align="center" style="margin-left:10px; margin-top:10px">
		<table class="tablaNew">
			<tr>
				<td>Concepto:</td>
				<td>
					<div style="display:inline;">
						<div style="float:left">Sub concepto:</div>
						<div id="repDetalle" style="float:right;margin-right:10px">Detallado:<input style="vertical-align:middle" type="checkbox" id="detallado" align="center" /></div>
					</div>
				</td>
				<td>Tipo Filtro:</td>
				<td>Aplicador:</td>
				<td>Desde:</td>
				<td>Hasta:</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>
					<select class="mUp" name="cmbConcepto" id="cmbConcepto" title="Seleccione un concepto" onchange="javascript:getSubconceptos($('#cmbConcepto').val())">
					    <option value="-1" title="Todos" selected="true">Todos</option>
					    <? if($super)foreach($super as $sub){  ?>
			            	 <option value="<?=$sub->id_superconcepto?>" title="<?=$sub->nombre?>"><?=$sub->nombre?></option>
			            <? }?>
					</select>
				</td>
				<td>
					<select class="mUp" name="cmbSubConcepto" id="cmbSubConcepto" style="width:183px;" onchange="javascript:checaIndex()";>
					    <option value="0" title="Todos">Todos</option>
					</select>
				</td>
				<td>
					<select class="mUp" name="cmbTipoFiltro" id="cmbTipoFiltro">
					    <option value="0" title="Fecha de Pago">Fecha de Pago</option>
					    <option value="1" title="Fecha de Aplicación">Fecha de Aplicación</option>
					</select>
				</td>
				<td>
					<select class="mUp" name="cmbAplicador" id="cmbAplicador">
					    <option value="0" title="Fecha de Pago">Todos</option>
					    <? if($aplicadores)foreach($aplicadores as $sub){  ?>
			            	 <option value="<?=$sub->idusuario?>" title="Aplicado por el usuario <?=$sub->usuario?>"><?=$sub->usuario?></option>
			            <? }?>
					</select>
				</td>
				<td>
					<input type="text" id="dpDesde" class="fecha redondo mUp" readonly="true" style="padding-left:3px;border: 1px #008B8B solid;width:60px;height:22px;"/>
				</td>
				<td>
					<input type="text" id="dpHasta" class="fecha redondo mUp" readonly="true" style="padding-left:3px;border: 1px #008B8B solid;width:60px;height:22px;"/>
				</td>
				<td>
					<button style="margin-bottom:5px" id="btnBusca" onClick="javascript:generar()">Generar</button>
				</td>
				<td>
					<button class="mUp" id="btnImprimir" onClick="javascript:imprSelec()" >Imprimir</button>
				</td>
			</tr>
		</table>
	</div>
</div>
<div style="height:400px;overflow-x: hidden;border:1px solid;" class="redondo">
<div id="tablaImprimir">
<style>
#asp_head tr{
	background-color:#E6EEEE;
	color:#294F88;
	border:#E6EEEE 1px solid;
}
#asp_Body tr:nth-child(odd){
	background-color:#F0F0F6;
	color:#000000;
	border:#E6EEEE 1px solid;
}
#asp td:{

}
#total_head tr{
	background-color:#E6EEEE;
	color:#294F88;
	border:#E6EEEE 1px solid;
}
#total_Body tr:nth-child(odd){
	background-color:#F0F0F6;
	color:#000000;
	border:#E6EEEE 1px solid;
}
#total td:{
	
}
.center{
	text-align: center;
}
.right{
	text-align: right;
}
.left{
	text-align: left;
}
.redondo{
	border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px; /* Borde redondo */ 
}
table td
{
	border-top:none
}
</style>	
<br>
<table id="asp" class="tablesorter" style="width:800px">
	<thead id="asp_head">
		<tr style="display:none" id="cabeza">
			<th colspan="7"><img src='<?=base_url();?>assets/images/cabecera.png'></th>	
		</tr>
		<tr>
			<th style="width:10px" id="numeracion"></th>
			<th style="width:70px">Referencia</th>
            <th>Concepto</th>
            <th style="width:110px" id="tipoFecha">Fecha de Pago</th>	
            <th style="width:60px">Importe</th>	
		</tr>
	</thead>
	<tbody id="asp_Body">
	</tbody>
</table>
<br>
<center>
	<table  id="total" class="tablesorter" style="width:300px;">
		<thead id="total_head">
			<tr>
				<th class="center">Concepto</th>
				<th id="totalIngresos">Total</th> 
			</tr>
		</thead>
		<tbody  id="total_Body">
		</tbody>
	</table >
</center>
	<table style="display: none" id="pie">
		<tr>
			<td>
				<div style="display:inline">
					<div id="nIngresos" style="float:right;margin-right:1px;padding-left:5px;padding-right: 5px" >No. de registros:</div>
				    <div id="fecha" style="display:none;float:left;margin-left: 1px;padding-left:5px;padding-right: 5px;"; align="left" ><?="Fecha: ".date("d/m/Y");;?></div>
				</div>	
			</td>
		</tr>
		<tr><td><img src="<?=base_url();?>assets/images/piedepagina.png"></td></tr>
	</table>
</div>

</div>
<div id="dialog-confirm" title="Limite de fechas invalido" hidden>
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><div align="center" id='msgErrFecha'>Seleccione un limite de fechas valido.</div></p>
</div>

<?= $this->load->view('footer_cv');?>
<script>
//Formato para datapicker
$("#dpDesde").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2012:+0",
	onSelect: function( selectedDate ){
		$("#dpHasta").datepicker( "option", "minDate", selectedDate );
	}
});
$("#dpHasta").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2012:+0",
	onSelect: function( selectedDate ){
		$("#dpDesde").datepicker( "option", "maxDate", selectedDate );
	}
});
$.loader({
	className:"black",
	content:''
 });
first=true;
num_seleccionados=0;
$("#asp").tablesorter({widgets: ['zebra']});
$("#total").tablesorter({widgets: ['zebra']});
$(document).ready(
	function(){
		console.debug("Termino de cargar...............");
		//buscar("");
		$('#cmbConcepto').val(-1);
		$("#btnImprimir").hide();
		$("#total").hide();
		$("#nIngresos").hide();
		$("#repDetalle").hide();
		$('#detallado').removeAttr('checked');
		$.loader('close');
	}
);
function checaIndex(){
	if($("#cmbSubConcepto").val()!=0){
		$("#repDetalle").fadeOut(300);
		$('#detallado').removeAttr('checked');
	}else{
		$("#repDetalle").fadeIn(2000);
	}
}
function getSubconceptos(id){
	if(id!=-1){
		$('#cmbSubConcepto').fadeOut(300).fadeIn(1000);
		$.ajax({
		  	type: "POST",
		    url: "<?php echo base_url().""; ?>index.php/cxc/getSubConceptos",
		    data: "superConcepto="+id,
		    async: false,
			statusCode:{500:function(){alert('error 500');}},
		    success: function(msg){
		    	//console.debug(msg);
		    	var obj=JSON.parse(msg);
		    	if(obj.length!=undefined){
		    		$('#cmbSubConcepto').html('');
		    		$('#cmbSubConcepto').append("<option value='0' title='Todos'>Todos</option>");
		    		var i;
		    		for(i=0;i<obj.length;i++){
		    			$('#cmbSubConcepto').append("<option value='"+obj[i].id+"' title='"+obj[i].descripcion+"'>"+obj[i].descripcion+"</option>");
		    		}
		    		if(i>2)
		    			$("#repDetalle").fadeIn(2000);
		    		else{
		    			$("#repDetalle").fadeOut(300);
		    			$('#detallado').removeAttr('checked');
		    		}
		    	}else{
		    		$('#cmbSubConcepto').html('');
		    		$('#cmbSubConcepto').append("<option value='0' title='Todos'>Todos</option>");
		    		$("#repDetalle").fadeOut(300);
		    		$('#detallado').removeAttr('checked');
		    	}
		    }
		});
	}else{
		$('#cmbSubConcepto').fadeOut(300).fadeIn(1000);
		$('#cmbSubConcepto').html('');
		$('#cmbSubConcepto').append("<option value='0' title='Todos'>Todos</option>");
		$("#repDetalle").fadeOut(300);
    	$('#detallado').removeAttr('checked');
	}
}
var arrConceptos=new Array();
var arrImportes=new Array();
var lugar=0;
function generar(){
	$('#tipoFecha').html($("#cmbTipoFiltro option:selected").attr("title"));
	var detallado=$('#detallado').is(':checked');
	if(detallado==true){
		detallado=1;
	}else{
		detallado=0;
	}
	var superConceptoN=$('#cmbConcepto  option:selected').attr('title');
	var superConcepto=$('#cmbConcepto').val();
	var subConcepto=$('#cmbSubConcepto').val();
	var cmbTipoFiltro=$('#cmbTipoFiltro').val();
	var aplicador=$('#cmbAplicador').val();
	var desde=$('#dpDesde').val();
	var hasta=$('#dpHasta').val();
	var ok=true;
	console.debug('desde->'+desde+".....hasta"+hasta);	
	if(desde!="" && hasta==""){
		errorFecha('Seleccione el rango de fecha final para la busqueda.');
		ok=false;
	}
	if(hasta!="" && desde==""){
		errorFecha('Seleccione el rango de fecha de inicio para la busqueda.');
		ok=false;
	}
	if(ok){
		$.loader({
			className:"black",
			content:''
		});
		$.ajax({
		  	type: "POST",
		    url: "<?php echo base_url().""; ?>index.php/cxc/getReporte",
		    data: "superConcepto="+superConcepto+"&detallado="+detallado+"&superConceptoN="+superConceptoN+"&subConcepto="+subConcepto+"&tipoFiltro="+cmbTipoFiltro+"&aplicador="+aplicador+"&desde="+desde+"&hasta="+hasta,
		    async: false,
			statusCode:{500:function(){alert('error 500');}},
		    success: function(msg){
		        var obj=JSON.parse(msg);
		        var x="";
		        $("#asp_Body").html("");
				//alert("whh");
		        $("#asp").trigger("update");
				//$("#docente").trigger("update");
		        if(obj.length==undefined || obj=="0"){
					x="<tr><td></td><td></td><td></td><td></td><td></td></tr>";
		        }else{
					arrConceptos= [];
					arrImportes=[];
					lugar=0;
					var total=0;
					var nIngresos=0;
					var nameL=0;
					var conceptoL=0;
					for(var i =0 ; i<obj.length ; i++){
						//Busco si ya esta el el concepto en el arreglo
						if(arrConceptos.length==0){arrConceptos[lugar]=obj[i].concepto;lugar+=1;}//Conceptos
						var repetido = comprobarRepetido(obj[i].concepto, lugar,arrConceptos);
						if(repetido){arrConceptos[lugar]=obj[i].concepto;lugar+=1;}
						//
						var l=buscaConcepto(obj[i].concepto, lugar,arrConceptos);
						if($.isNumeric(arrImportes[l])==false){console.debug("entro");arrImportes[l]=0;}
						arrImportes[l]+=parseInt(obj[i].importe);					
						//Obtengo el nombre mas largo
						if(obj[i].nombre.length>nameL){
							nameL=obj[i].nombre.length;
						}
						if(obj[i].concepto.length>conceptoL){
							conceptoL=obj[i].concepto.length;
						}
						//
						x+="<tr id='asp_"+obj[i].id+"'><td>"+(i+1)+"</td><td class='left' title='Referencia'>"+obj[i].referencia+"</td><td class='left' title='Concepto'>"+obj[i].concepto+"&nbsp&nbsp&nbsp&nbsp"+obj[i].nombre+"</td><td class='center' title='Fecha'>"+obj[i].fecha+"</td><td class='right' title='Importe'>$"+obj[i].importe+"</td></tr>";
						total+=parseInt(obj[i].importe);
						nIngresos+=1;
					}
				}
				//console.debug(arrImportes);
				//console.debug(x);
				$("#asp_Body").html(x);
				//alert("Concepto:"+conceptoL+"-Nombre:"+nameL+"-Suma:"+(conceptoL+nameL));
				//var w=((conceptoL+nameL)*7.87)+250;
				//$("#asp").css('width',w+"px");
				$("#asp").trigger("update");
				if($("#asp_Body").html()=='<tr><td></td><td></td><td></td><td></td><td></td></tr>'){
					$("#btnImprimir").hide('slow');
					$("#nIngresos").hide('slow');
					$("#total").hide('slow');
				}else{
					$("#btnImprimir").show(1000);
					$("#nIngresos").html('No. de registros: '+nIngresos);
					$("#nIngresos").show(100);
					//Totales
					$("#total").show(1000);
					y="";
					for(var i=0;i<arrConceptos.length;i++){
						y+="<tr><td>"+arrConceptos[i]+"</td><td class='right'>$"+arrImportes[i]+"</td></tr>";
					}
					y+="<tr><td>IMPORTE TOTAL</td><td class='right'>$"+total+"</td></tr>";
					console.debug(y);
					$("#total_Body").html('');
					$("#total_Body").html(y);
					$("#total").trigger("update");
				}
				//Clicks en las tablas
				if(first){
					$('#numeracion').click();
					$('#totalIngresos').click();
					first=false;
				}else{
					$('#numeracion').click();
					$('#numeracion').click();
					$('#totalIngresos').click();
					$('#totalIngresos').click();
				}
			}
		});
		$.loader('close');
	}
	//console.debug(arrConceptos);
}
function pause(ms) {
	ms += new Date().getTime();
	while (new Date() < ms){}
} 
function errorFecha(texto){
	$("#msgErrFecha").html(texto);
	$( "#dialog-confirm" ).dialog({
		resizable: false,
		height:16,
		width:250,
		modal: true,
		buttons: {
			"Continuar": function() {
				$( this ).dialog( "close" );
			}
		}
	});
}
function comprobarRepetido(valor, lugar,arreglo){
	var repetir;
	var n = valor;
	var p = lugar;
	var arr = arreglo;
	//Recorro el cartilla, si hay alguna repetición
	//cancelo recorrido y avizo de la repetición
	for (var j=0;j<p;j++){
		if (n==arr[j]){
			//repetir=true;
			repetir=false;
			break;
		}else{
			//repetir=false;
			repetir=true;
		}
	}
	return repetir;
}
function buscaConcepto(valor,lugar,arreglo){
	var arrL;
	var n = valor;
	var p = lugar;
	var arr = arreglo;
	//Recorro el cartilla, si hay alguna repetición
	//cancelo recorrido y avizo de la repetición
	for (var j=0;j<p;j++){
		if (n==arr[j]){
			//repetir=true;
			arrL=j;
			break;
		}else{
			//repetir=false;
			arrL=lugar;
		}
	}
	return arrL;
}
function imprSelec(){
	if($("#asp_Body").html()!=""){
		$("#cabeza").css("display","");
		$("#pie").css("display","");
		$("#fecha").css("display","");
		var ficha=document.getElementById('tablaImprimir');
		var ventimp=window.open(' ','popimpr');
		ventimp.document.write(ficha.innerHTML);
		ventimp.document.close();ventimp.print();
		ventimp.close();
			$("#cabeza").css("display","none");
			$("#pie").css("display","none");
			$("#fecha").css("display","none");
	}
}
$("#dpDesde").keydown(function(e){
    if (e.keyCode == 46 || e.keyCode == 8) {
        //Delete and backspace clear text 
        $(this).val(''); //Clear text
        $(this).datepicker("hide"); //Hide the datepicker calendar if displayed
        $(this).blur(); //aka "unfocus"
    }

    //Prevent user from manually entering in a date - have to use the datepicker box
    e.preventDefault();
});
$("#dpHasta").keydown(function(e){
    if (e.keyCode == 46 || e.keyCode == 8) {
        //Delete and backspace clear text 
        $(this).val(''); //Clear text
        $(this).datepicker("hide"); //Hide the datepicker calendar if displayed
        $(this).blur(); //aka "unfocus"
    }

    //Prevent user from manually entering in a date - have to use the datepicker box
    e.preventDefault();
});
	
	
</script>
