<?php $this->load->view('header_cv'); ?>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url()?>assets/css/layout2.css">
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-latest.js" ></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.4.2.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui-1.8.1.custom.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jqgrid/js/i18n/grid.locale-sp.js"></script>
<script src="<?php echo base_url()?>assets/js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.ui.datepicker.js"></script> 
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px; font-size: 1.2em; height: 452px;
}
ul.tabs li {
	border-top: none; border-left: none; border-right: none; border-bottom: 1px ; font-size: 25px; background: none;		
}
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<form name="frmNueDeposito" id="frmNueDeposito" method="post" action="<?= base_url()?>index.php/depositos/agregar">
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/banco.png" width="25" height="25" border="0"> Depósitos - Agregar</li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal">
		<div id="accordion">
			<h3 align="left"><a href="#">Datos del Depósito</a></h3>
				<div style="height: 386px; overflow: hidden" >
				<table border="2px" cellpadding="0" cellspacing="0" class="tablesorter" style="margin-left: -15px;">
				<thead><th rowspan="2"><label style="margin: 2px; font-size: 20px" for="nombre">Razón Social:</label></th>
					<th colspan="3" style="font-size: 20px; "><input size="75%" type="text" name="txtNombre" id="txtNombre" readonly="true">
					</th>
				<tr align="right">
					<th colspan="3" align="right" style="text-align: right">
						<label style="margin: 2px; font-size: 20px; visibility: hidden" for="Numero">Número de Cliente o Razon Social a la que puede pe:</label><input style="font-size: 20px" type="hidden" name="txtNumero" id="txtNumero" />
						<label style="margin: 2px; font-size: 20px" for="Zona">Zona:</label><input style="font-size: 20px" type="text" name="txtZona" id="txtZona" readonly="true">
					</th>
				</tr>
				</thead>
				<tr>
					<th><label style="margin: 2px; font-size: 20px" for="txtFecha">Fecha:</label></th>
					<th style="font-size: 20px; background-color: white"><input size="12%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center"/></th>
					<th><label style="margin: 2px; font-size: 20px" for="txtTC">T.C.:</label></th>
					<th style="font-size: 20px; background-color: white">$<input size="8%" style="text-align: right" type="text" name="txtTC" id="txtTC"></th>			
				</tr>
    			<tr>
					<th><label style="margin: 2px; font-size: 20px" for="txtPesos">M.N.:</label></th>
					<th style="font-size: 20px; background-color: white">$<input size="15%" style="text-align: right" type="text" name="txtPesos" id="txtPesos"></th>
					<th><label style="margin: 2px; font-size: 20px" for="edo">U.S.D.:</label></th>
					<th style="font-size: 20px; background-color: white; width: 25%">$<input size="15%" style="text-align: right" type="text" name="txtDolar" id="txtDolar"></th>
				</tr>
    			<tr>
					<th><label style="margin: 2px; font-size: 20px" for="txtCta">Cuenta:</label></th>
					<th style="font-size: 20px; background-color: white;" ><input size="15%" type="text" name="txtCta" id="txtCta" style="margin-left: 12px;"></th>
					<th><label style="margin: 2px; font-size: 20px;" for="lstciclo">Aplicar Depósito a Ciclo:</label></th>
					<th style="font-size: 20px; background-color: white">
						<select name="lstciclo" id="lstciclo" style="font-size: 18px; margin-top: 1px; margin-left: 12px; height: 29px;" >
							<?php $ciclof=2007; $actual=date("Y"); $actual+=1;
						while($actual >= $ciclof){?>
							<option value="<?php echo $actual;?>" <?php if(date("Y")==$actual) echo "Selected='selected'";?> > <?php echo $actual;?> </option>
            				<?php $actual-=1; } ?>
          				</select>
					</th>
				</tr>
    			<tr style="font-size: 20px"><th colspan="4">Observaciones</th></tr>
      			<tr style="font-size: 18px">
        			<th colspan="4" bgcolor="white"><div align="center"><textarea style="font-size: 20px" id ="txtobs" name="txtobs" cols="78" wrap="virtual"></textarea></div></th>        							
      			</tr>
				<tfoot>
					<th colspan="3"></th><th ><center><button name="boton" value="1" style="font-size: 20px">Guardar</button></center></th>
				</tfoot>
    		</table>   
			</div>
			<h3 align="left"><a href="#">Buscar Cliente</a></h3>
			<div style="height: 100px; overflow: hidden">
				<label for="fil1" style="visibility:hidden"></label><input style="visibility:hidden" type="radio" name="rad" id="fil1" />
       			<label for="fil2" style="visibility:hidden"></label><input style="visibility:hidden" type="radio" name="rad" id="fil2" />
       			<label for="fil3" style="visibility:hidden"></label><input style="visibility:hidden" type="radio" name="rad" id="fil3" checked="checked" />
				<div id="barraRem">
				<table id="myTabla" cellspacing="1" class="tablesorter" width="40%" border="2px" style="" >
					<thead>				
						<tr>
							<th width="35%"><input type="button" width="100" value="Escriba aquí su busqueda..." onClick="selecciona(this,0)" onKeyPress="selecciona(this,0);" /></th>					
						</tr>
					</thead>
					<tbody>
						<?php
						$this->load->model('depositos_model');
						$data['result']=$this->depositos_model->verActivosC();
						foreach ($data['result'] as $row):?>
						<tr style="cursor: pointer"  onclick="parent.cargadatos('<?=$row->Numero?> ');">				
							<td width="35%"><?php echo $row->Razon;?></td>
						</tr>
						<?php endforeach;?>
					</tbody>
				</table>	
				</div>
			</div>
		</div>
	</div>
	</form>
	</div>
	<a style="color: white;" href="<?=base_url()?>index.php/depositos">LISTA DE DEPÓSITOS</a>		
</div>
<?php $this->load->view('footer_cv'); ?>
<script>

$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

// Este evento verifica que el valor sea numero
function cargadatos($id){
	$.ajax({
		type: "POST",
		url:"<?= base_url()?>index.php/depositos/datosCli",
		data:"id="+$id,
		success:function(msg){ //alert($id);
			Ponerdatos(msg);
		}
	
	});
}
function Ponerdatos(datos){
	//alert(datos);
	$("#accordion").accordion( "activate",0 );
	var obj=JSON.parse(datos); 
	if(obj.razon!="" && obj.zona!=""){
		$("#txtNombre").val(obj.razon);
		$("#txtZona").val(obj.zona);
		$("#txtNumero").val(obj.numero);
		
	}
	else{
		alert("Registro Inexistente");
	}
}

function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}

$("#frmNueDeposito").submit(function(){
	cli=$("#txtNombre").val();
	fec=$("#txtFecha").val();
	if(cli!=''){
		if( fec!=''){
			return true;
		}else{
			alert("Error: Seleccione Fecha");	
				return false;
		}
	}else{
		alert("Error: Elija Cliente");
			return false;
	}
	return false;	
});

function validar2(e){
tecla_codigo = (document.all) ? e.keyCode : e.which;
if (tecla_codigo==8)return false;
return true;
}

cant=0;
val_ant='';
function validar(e){
if(valor.length<=cant){
document.getElementById('textarea2').value=val_ant;
return;
}
cant++;
val_ant=valor;

}
var colum=0; // columna por la que se filtrar�
var valor; // value del bot�n que se ha pulsado
 
function selecciona(obj,num) {
  t = document.getElementById('myTabla');
  filas = t.getElementsByTagName('tr');
  // Deseleccionar columna anterior
  for (i=1; ele=filas[i]; i++) 
    ele.getElementsByTagName('td')[colum].className='';
  // Seleccionar columna actual
  colum=num;
  for (i=1; ele=filas[i]; i++)
    ele.getElementsByTagName('td')[colum].className='celdasel';
  // Cambiar bot�n por cuadro de texto
  valor = obj.value;
  celda = obj.parentNode;
  celda.removeChild(obj);
  txt = document.createElement('input');
  celda.appendChild(txt);
  txt.focus();
  txt.size = 62;  
  txt.onblur = function() {ponerBoton(this,num)};
  txt.onkeyup = function() {filtra(this.value)};
  // Desactivar los dem�s botones
  for (i=0; ele=t.getElementsByTagName('input')[i]; i++)
    if (ele.type == 'button') ele.disabled=true;
}
 
function ponerBoton(obj,num) {
  celda = obj.parentNode;
  celda.removeChild(obj);
  boton = document.createElement('input');
  boton.type = 'button';  
  boton.value = valor;
  boton.onclick = function() {selecciona(this,num)}
  boton.onkeypress = function() {selecciona(this,num)}  
  celda.appendChild(boton);
  // Activar botones
  for (i=0; ele=t.getElementsByTagName('input')[i]; i++)
    ele.disabled=false;
}
 
function filtra(txt) {
  t = document.getElementById('myTabla');
  filas = t.getElementsByTagName('tr');
  for (i=1; ele=filas[i]; i++) {
    texto = ele.getElementsByTagName('td')[colum].innerHTML.toUpperCase();
    for (j=0; ra=document.forms[0].rad[j]; j++) // Comprobar radio seleccionado
      if (ra.checked) num = j;      
    if (num==0) posi = (texto.indexOf(txt.toUpperCase()) == 0);
    else if (num==1) posi = (texto.lastIndexOf(txt.toUpperCase()) == texto.length-txt.length);
    else posi = (texto.indexOf(txt.toUpperCase()) != -1);
    ele.style.display = (posi) ? '' : 'none';
  } 
}
</script>