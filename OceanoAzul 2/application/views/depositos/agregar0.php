<?php $this->load->view('header_cv'); ?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>
<form method="post" action="" id="form" ></form>
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/banco.png" width="25" height="25" border="0"> Depósitos - Agregar</li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion">
			<h3 align="left"><a href="#">Datos del Depósito</a></h3>
        <div  style="height:350px; ">                                                          
            <!--Tabla-->
            <table border="2px" cellpadding="0" cellspacing="0" class="tablesorter" style="margin-left: -15px;">
				<thead><th rowspan="2"><label style="margin: 2px; font-size: 20px" for="nombre">Razón Social:</label></th>
					<th colspan="3" style="font-size: 20px; "><input size="75%" type="text" name="txtNombre" id="txtNombre" readonly="true">
					</th>
				<tr align="right">
					<th colspan="3" align="right" style="text-align: right">
						<label style="margin: 2px; font-size: 20px; visibility: hidden" for="Numero">Número de Cliente o Razon Social a la que puede pe:</label><input style="font-size: 20px" type="hidden" name="txtNumero" id="txtNumero" />
						<label style="margin: 2px; font-size: 20px" for="Zona">Zona:</label><input style="font-size: 20px" type="text" name="txtZona" id="txtZona" readonly="true">
					</th>
				</tr>
				</thead>
				<tr>
					<th><label style="margin: 2px; font-size: 20px" for="txtFecha">Fecha:</label></th>
					<th style="font-size: 20px; background-color: white"><input size="12%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center"/></th>
					<th><label style="margin: 2px; font-size: 20px" for="txtTC">T.C.:</label></th>
					<th style="font-size: 20px; background-color: white">$<input size="8%" style="text-align: right" type="text" name="txtTC" id="txtTC"></th>			
				</tr>
    			<tr>
					<th><label style="margin: 2px; font-size: 20px" for="txtPesos">M.N.:</label></th>
					<th style="font-size: 20px; background-color: white">$<input size="15%" style="text-align: right" type="text" name="txtPesos" id="txtPesos"></th>
					<th><label style="margin: 2px; font-size: 20px" for="edo">U.S.D.:</label></th>
					<th style="font-size: 20px; background-color: white; width: 25%">$<input size="15%" style="text-align: right" type="text" name="txtDolar" id="txtDolar"></th>
				</tr>
    			<tr>
					<th><label style="margin: 2px; font-size: 20px" for="txtCta">Cuenta:</label></th>
					<th style="font-size: 20px; background-color: white;" ><input size="15%" type="text" name="txtCta" id="txtCta" style="margin-left: 12px;"></th>
					<th><label style="margin: 2px; font-size: 20px;" for="lstciclo">Aplicar Depósito a Ciclo:</label></th>
					<th style="font-size: 20px; background-color: white">
						<select name="lstciclo" id="lstciclo" style="font-size: 18px; margin-top: 1px; margin-left: 12px; height: 29px;" >
							<?php $ciclof=2007; $actual=date("Y"); $actual+=1;
						while($actual >= $ciclof){?>
							<option value="<?php echo $actual;?>" <?php if(date("Y")==$actual) echo "Selected='selected'";?> > <?php echo $actual;?> </option>
            				<?php $actual-=1; } ?>
          				</select>
					</th>
				</tr>
    			<tr style="font-size: 20px"><th colspan="4">Observaciones</th></tr>
      			<tr style="font-size: 18px">
        			<th colspan="4" bgcolor="white"><div align="center"><textarea style="font-size: 20px" id ="txtobs" name="txtobs" cols="78" wrap="virtual"></textarea></div></th>        							
      			</tr>
				<tfoot>
					<th colspan="3"></th><th ><center><button name="boton" value="1" style="font-size: 20px">Guardar</button></center></th>
				</tfoot>
    		</table>    
        </div>
        <h3 align="left"><a href="" >Buscar Cliente</a></h3>
       	<div style="height:150px; ">  
        	<div class="ajaxSorterDiv" id="tabla_k" style="margin-top: -21px">                
                <div style=" text-align:center" class='filter' >   
                	Proceso de Busqueda donde:
                    <select class='filter_type' title="Seleccione el campo para su filtrado"></select> 
                    es
                    <select class='filter_equ' title="Seleccione un tipo de comparacion">
                        <option value="">Igual</option>
                        <option value="like">Parecido</option>
                        <option value=">">Mayor</option>
                        <option value="<">Menor</option>
                        <option value="!=">Diferente</option>
                    </select>                               
                    a
                    <input id="buscar" name="buscar" class="filter_text" maxlength="15" type="text" title="Ingrese el texto deseado para buscar"/>                             
                    <input type="checkbox" class="keep_filter" title="Mantener filtros previos"/>                
                </div>
                <span class="ajaxTable" style=" width:750px;   height: 319px">
                    <table id="mytabla" class="ajaxSorter">
                        <thead title="Presione las columnas para ordenarlas como requiera">
                            <th data-order = "Zona" style="width: 150px">Zona</th>
                            <th data-order = "Razon" style="width: 600px">Razon Social</th>                                                        
                        </thead>
                        <tbody title="Seleccione para realizar cambios">                            
                        </tbody>
                    </table>
                </span>   
             	<div class="ajaxpager" style="">        
                    <ul class="order_list"></ul>                    
                    <form method="post" action="<?= base_url()?>index.php/reporter/clientes" >                    	             	                        
                        <img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                        <input type="text" class="pagedisplay" size="3"/> /
                        <input type="text" class="pagedisplayMax" size="3" readonly="1" disabled="disabled"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                        <select class="pagesize">
                                <option selected="selected" value="13">13</option>
                                <option value="25">25</option>                            
                                <option value='0'>Gral</option>                                          
                        </select>                              
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/>
                    </form>                    
                </div>                  
            </div>
        </div>	
	 </div> 	
 	</div> 	
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

$("#tabla_k").ajaxSorter({
		url:'<?php echo base_url()?>index.php/clientes/tabla',  
        //active:['Edo','Sinaloa'],  
        onRowClick:function(){
            $("#accordion").accordion( "activate",0 );
           	$("#id").val($(this).data('numero'));
			$("#txtNombre").val($(this).data('razon'));
			$("#txtZona").val($(this).data('zona'));
			$("#nombre").focus();
        }   
});
$("#nuevo").click( function(){	
	/*$("#id").val('');
	$("#nombre").val('');
	$("#dom").val('');
	$("#loc").val('');
	$("#edo").val('');
	$("#rfc").val('');
	$("#cp").val('');
	$("#zona").val('');
	$("#nombre").focus();*/
 return true;
}
)
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#aceptar").click( function(){	
	nombre=$("#nombre").val();
	cp=$("#cp").val();
	numero=$("#id").val();
	if(nombre!=''){
		if( is_int(cp)){
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/clientes/actualizar", 
						data: "id="+$("#id").val()+"&nombre="+$("#nombre").val()+"&dom="+$("#dom").val()+"&loc="+$("#loc").val()+"&edo="+$("#edo").val()+"&rfc="+$("#rfc").val()+"&cp="+$("#cp").val()+"&zona="+$("#zona").val(),
						success: 
								function(){ 
									document.location.href="<?= base_url()?>index.php/clientes";
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/clientes/agregar", 
						data: "&nombre="+$("#nombre").val()+"&dom="+$("#dom").val()+"&loc="+$("#loc").val()+"&edo="+$("#edo").val()+"&rfc="+$("#rfc").val()+"&cp="+$("#cp").val()+"&zona="+$("#zona").val(),
						success: 
								function(){ 
									document.location.href="<?= base_url()?>index.php/clientes";
								}		
				});
			}
			$("mytabla").trigger("update");
			//return true;
		}else{
			alert("Error: Codigo Postal no valido");	
			$("#cp").focus();
				return false;
		}
	}else{
		alert("Error: Nombre de Cliente no valido");
		$("#nombre").focus();
			return false;
	}
	 // return false;
});
		

</script></script>