<?php $this->load->view('header_cv'); ?>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.tablesorter.js" ></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.tablesorter.pager.js"></script>
 
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}
</style>

<div style="height:500px" >
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/banco.png" width="25" height="25" border="0"> Depósitos </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal">
			<div id="clientes" class="tab_content" style="overflow-x:hidden;" >
				<div style="height:420px; margin-top: -15px">
				<form method="post" action="<?= base_url()?>index.php/depositos/buscar" >						
					<table id="myTablaE" class="tablesorter" border="2px" >
						<thead>
							<tr><th>DEPÓSITOS EFECTUADOS DEL DÍA
								<input type="text" name="dpDesde" id="dpDesde" class="fecha redondo mUp" readonly="true" value="<?= $dpDesde?>" style="padding-left:3px;border: 1px #008B8B solid;width:60px;height:14px;"/>
								AL DÍA <input type="text" name="dpHasta" id="dpHasta"  class="fecha redondo mUp" readonly="true" value="<?= $dpHasta?>" style="padding-left:3px;border: 1px #008B8B solid;width:60px;height:14px;"/>
								Razon Social: <input type="text" name="busqueda" id="busqueda" size="50%"  />
								<button style="margin-bottom:5px; id="btnBusca" onClick="javascript:generar()">Generar</button>
								</th>
							</tr>				
						</thead>		
					</table>	
				</form>
				<?php 
					if($result>0){ ?>
				<table id="myTabla" cellspacing="1" class="tablesorter" border="2px" style="margin-top: -15px" >
					<thead>				
						<tr>
							<th width="10px">Fecha</th>
							<th width="200px">Razon Social</th>
							<th width="50px">Camarón</th>
							<th width="50px">U.S.D.</th>
							<th width="50px">M.N.</th>					
							<th width="50px">U.S.D.</th>
						</tr>
					</thead>
					<tbody>
						<?php						
						$feci=""; $usac=0; $usa=0; $camaron=0; $mex=0; $dolar=0; foreach ($result as $row):
							$cantidadR=$row->ImporteD;$cantidadRC=$cantidadR;
							$mn=$row->Pesos;$camaron=$row->Cam;
							if($camaron==0){
								if($row->Pesos==0){ $dolar=$dolar+$cantidadR; } else { $mex=$mex+$row->Pesos; }
								$usa=$usa+$cantidadR; 
							} else {
								$usac=$usac+$cantidadRC; 
							}
						?>
						<tr>
							<?php
					 		if($feci!=$row->Fecha){ $feci=$row->Fecha;?>
							<td width="10%"><strong><?php echo $row->Fecha; ?></strong></td>
							<?php }else{ ?>
							<td width="10px" style="color: grey" align="right"><?php echo $row->Fecha; ?></td>	
							<?php } ?>	
							<?php if($usuario=="Lety Lizárraga" or $usuario=="Jesus Benítez"){ ?>
										<td width="200px"><a href="<?=base_url()?>index.php/depositos/actualizar/<?=$row->ND?>"><?php echo $row->Razon;?></a></td>
									<?php } else {?> 
										<td width="200px" "><?php echo $row->Razon ?><?php ?></td> 
									<?php }?>				
							
							<td width="50px" align="right" style="color:#0033FF">
         						 <?php if($camaron==-1){ echo "$ ".number_format($cantidadRC, 2, '.', ','); }?>
        					</td>
        					<td width="50px" align="right" style="color:#009900" >
          						<?php if(($mn==0) and ($camaron==0)){ echo "$ ".number_format($cantidadR, 2, '.', ','); }?>
        					</td>
        					<td width="50px" align="right" style="color:#FF0000">
          						<?php if(($mn>0) and ($camaron==0)){ echo "$ ".number_format($mn, 2, '.', ','); }?>
        					</td>
        					<td width="50px" align="right" ><?php if($camaron==0){ echo "$ ".number_format($cantidadR, 2, '.', ','); }?></td>
						</tr>
						<?php endforeach;?>						
					</tbody>
					<tfoot>
						<tr>
        					<th colspan="2" width="210px" style="color:#000000">Totales:</th>
        					<th width="50px"><div align="right"  style="color:#0033FF"><?php echo "$ ".number_format($usac, 2, '.', ',');?></div></th>
        					<th width="50px"><div align="right" style="color:#009900"><?php echo "$ ".number_format($dolar, 2, '.', ',');?></div></th>
        					<th width="50px"><div align="right" style="color:#FF0000"><?php echo "$ ".number_format($mex, 2, '.', ',');?></div></th>
        					<th width="50px"><div align="right" style="color:#000000"><?php echo "$ ".number_format($usa, 2, '.', ',');?></div></th>
   						</tr>
   						<tr>
        					<th align="right" colspan="5" width="360px" style="color:#000000">Ingresos Totales con Pago de Camaron:</th>
        					<th width="50px"><div align="right" style="color:#000000"><?php echo "$ ".number_format(($usa+$usac), 2, '.', ',');?></div></th>
   						</tr>
					</tfoot>
				</table>
				<?php	}else{ ?>
		<table id="myTable" class="tablesorter" width="40%" border="5px" >
			<thead>
				<tr>
					<th width="40%" colspan="3" style="color: red">LO SENTIMOS PERO NO HAY DEPÓSITOS REGISTRADOS EN LAS FECHAS SELECCIONADAS...</th>					
				</tr>
			</thead>			
		</table>			
	<?php }?>	
	
			</div>
		</div>
	</div>	
</div>
<div  id="pager" class="pager"  >
	<form>
		<img style="margin-top: 0px" src="<?php echo base_url()?>assets/addons/pager/icons/first.png" class="first"/>
		<img src="<?php echo base_url()?>assets/addons/pager/icons/prev.png" class="prev"/>
		<input type="text" class="pagedisplay" size="4px" readonly="true"/>
		<img src="<?php echo base_url()?>assets/addons/pager/icons/next.png" class="next"/>
		<img src="<?php echo base_url()?>assets/addons/pager/icons/last.png" class="last"/>
		<select style="margin-top: 1px" class="pagesize">
			<option selected="selected" value="10">10</option>
			<option value="15">15</option>													
		</select>		
		<a style="color: white" href="<?=base_url()?>index.php/depositos/agregar">AGREGAR DEPÓSITO</a>
	</form>			
</div>			
</div>
<?php $this->load->view('footer_cv'); ?>
<script>
//Formato para datapicker
$("#dpDesde").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#dpHasta").datepicker( "option", "minDate", selectedDate );
	}
});
$("#dpHasta").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#dpDesde").datepicker( "option", "maxDate", selectedDate );
	}
});	
	
	
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

function generar(){
	/*$('#tipoFecha').html($("#cmbTipoFiltro option:selected").attr("title"));
	var detallado=$('#detallado').is(':checked');
	if(detallado==true){
		detallado=1;
	}else{
		detallado=0;
	}
	var superConceptoN=$('#cmbConcepto  option:selected').attr('title');
	var superConcepto=$('#cmbConcepto').val();
	var subConcepto=$('#cmbSubConcepto').val();
	var cmbTipoFiltro=$('#cmbTipoFiltro').val();
	var aplicador=$('#cmbAplicador').val();*/
	var desde=$('#dpDesde').val();
	var hasta=$('#dpHasta').val();
	var ok=true;
	console.debug('desde->'+desde+".....hasta"+hasta);	
	if(desde!="" && hasta==""){
		errorFecha('Seleccione el rango de fecha final para la busqueda.');
		ok=false;
	}
	if(hasta!="" && desde==""){
		errorFecha('Seleccione el rango de fecha de inicio para la busqueda.');
		ok=false;
	}
	if(ok){
		$.loader({
			className:"black",
			content:''
		});

		$.ajax({
		  	type: "POST",
		    url: "<?php echo base_url().""; ?>index.php/depositos/verActivos",
		    data: "desde="+desde+"&hasta="+hasta,
		    async: false,
			statusCode:{500:function(){alert('error 500');}},
		    success: function(msg){ 
		        var obj=JSON.parse(msg);
		        var x="";
		        //$("#asp_Body").html("");
						//alert("whh");
		        //$("#asp").trigger("update");
						//$("#docente").trigger("update");
		        if(obj.length==undefined || obj=="0"){
					x="<tr><td></td><td></td><td></td><td></td><td></td></tr>";
		        }else{
					arrConceptos= [];
					arrImportes=[];
					lugar=0;
					var total=0;
					var nIngresos=0;
					var nameL=0;
					var conceptoL=0;
					for(var i =0 ; i<obj.length ; i++){
						//Busco si ya esta el el concepto en el arreglo
						if(arrConceptos.length==0){arrConceptos[lugar]=obj[i].concepto;lugar+=1;}//Conceptos
						var repetido = comprobarRepetido(obj[i].concepto, lugar,arrConceptos);
						if(repetido){arrConceptos[lugar]=obj[i].concepto;lugar+=1;}
						//
						var l=buscaConcepto(obj[i].concepto, lugar,arrConceptos);
						if($.isNumeric(arrImportes[l])==false){console.debug("entro");arrImportes[l]=0;}
						arrImportes[l]+=parseInt(obj[i].importe);					
						//Obtengo el nombre mas largo
						if(obj[i].nombre.length>nameL){
							nameL=obj[i].nombre.length;
						}
						if(obj[i].concepto.length>conceptoL){
							conceptoL=obj[i].concepto.length;
						}
						//
						x+="<tr id='asp_"+obj[i].id+"'><td>"+(i+1)+"</td><td class='left' title='Referencia'>"+obj[i].referencia+"</td><td class='left' title='Concepto'>"+obj[i].concepto+"&nbsp&nbsp&nbsp&nbsp"+obj[i].nombre+"</td><td class='center' title='Fecha'>"+obj[i].fecha+"</td><td class='right' title='Importe'>$"+obj[i].importe+"</td></tr>";
						total+=parseInt(obj[i].importe);
						nIngresos+=1;
					}
				}
						//console.debug(arrImportes);
						//console.debug(x);
				$("#asp_Body").html(x);
						//alert("Concepto:"+conceptoL+"-Nombre:"+nameL+"-Suma:"+(conceptoL+nameL));
						//var w=((conceptoL+nameL)*7.87)+250;
						//$("#asp").css('width',w+"px");
				//$("#asp").trigger("update");
				if($("#asp_Body").html()=='<tr><td></td><td></td><td></td><td></td><td></td></tr>'){
					$("#btnImprimir").hide('slow');
					$("#nIngresos").hide('slow');
					$("#total").hide('slow');
				}else{
					$("#btnImprimir").show(1000);
					$("#nIngresos").html('No. de registros: '+nIngresos);
					$("#nIngresos").show(100);
					//Totales
					$("#total").show(1000);
					y="";
					for(var i=0;i<arrConceptos.length;i++){
						y+="<tr><td>"+arrConceptos[i]+"</td><td class='right'>$"+arrImportes[i]+"</td></tr>";
					}
					y+="<tr><td>IMPORTE TOTAL</td><td class='right'>$"+total+"</td></tr>";
					console.debug(y);
					$("#total_Body").html('');
					$("#total_Body").html(y);
					//$("#total").trigger("update");
				}
				//Clicks en las tablas
				/*if(first){
					$('#numeracion').click();
					$('#totalIngresos').click();
					first=false;
				}else{
					$('#numeracion').click();
					$('#numeracion').click();
					$('#totalIngresos').click();
					$('#totalIngresos').click();
				}*/
			}
		});
		$.loader('close');
	}
	//console.debug(arrConceptos);
}

$(document).ready(function(){ 			
		$("#myTabla").tablesorter ({headers:{}, widthFixed: true, widgets: ['zebra']}).tablesorterPager ({container: $("#pager")});
    });


</script>
