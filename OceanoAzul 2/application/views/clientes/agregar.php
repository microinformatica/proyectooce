<?php $this->load->view('header_cv'); ?>
<style>        
.tab_content {
	padding: 10px; font-size: 1.2em; height: 452px;
}
ul.tabs li {
	border-top: none; border-left: none; border-right: none; border-bottom: 1px ; font-size: 25px; background: none;		
}
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/cliente.png" width="25" height="25" border="0"> Clientes - Agregar</li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal">
			<div id="clientes" class="tab_content" style="overflow-x:hidden;" >
				<div style="height:420px; margin-top: -15px">
					<form method="post" action="<?= base_url()?>index.php/clientes/agregar" id="form">			
			<table border="2px" border="5px" class="tablesorter">
				<thead><th colspan="4" height="25px" style="font-size: 25px"><center>Ingresar Cliente</center></th></thead>
				<tr>
					<th><label style="margin: 2px; font-size: 25px" for="nombre">Razón Social:</label></th>
					<th colspan="3" style="font-size: 20px; background-color: white"><input size="78%" type="text" name="nombre" id="nombre"></th>
				</tr>
				<tr>
					<th><label style="margin: 2px; font-size: 25px" for="dom">Domicilio:</label></th>
					<th colspan="3" style="font-size: 20px; background-color: white"><input size="78%" type="text" name="dom" id="dom"></th>
				</tr>
    			<tr>
					<th><label style="margin: 2px; font-size: 25px" for="loc">Localidad:</label></th>
					<th colspan="3" style="font-size: 20px; background-color: white"><input size="78%" type="text" name="loc" id="loc"></th>
				</tr>
    			<tr>
					<th><label style="margin: 2px; font-size: 25px" for="edo">Estado:</label></th>
					<th style="font-size: 20px; background-color: white"><input type="text" name="edo" id="edo"></th>
					<th><label style="margin: 2px; font-size: 25px" for="edo">Municipio:</label></th>
					<th style="font-size: 20px; background-color: white"><input type="text" name="edo" id="edo"></th>
				</tr>
    			<tr>
					<th><label style="margin: 2px; font-size: 25px" for="rfc">RFC:</label></th>
					<th style="font-size: 20px; background-color: white"><input type="text" name="rfc" id="rfc"></th>
					<th><label style="margin: 2px; font-size: 25px" for="cp">CP:</label></th>
					<th style="font-size: 20px; background-color: white"><input type="text" name="cp" id="cp"></th>
				</tr>
    			<tr>
					<th ><label style="margin: 2px; font-size: 25px" for="zona">Zona:</label></th>
					<th colspan="3" style="font-size: 20px; background-color: white"><input type="text" name="zona" id="zona"></th>
				</tr>
				<tfoot>
					<th colspan="3"></th>
					<th ><center><button name="boton" value="1" style="font-size: 25px">Guardar</button></center></th>
				</tfoot>
    		</table>    		
    			
		</form>
			</div>
		</div>
	</div>	
</div>
<a style="color: white;" href="<?=base_url()?>index.php/clientes">LISTA DE CLIENTES</a>		
</div>
<?php $this->load->view('footer_cv'); ?>
<script>
// Este evento verifica que el valor sea numero
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
// El evento submit sucede cuando se envía la información del formulario para lo cual se realizó
// Un  script  que valida que exista un nombre capturado y que la edad sea un valor numérico
// si todo esta correo envía un  valor de verdadero y se hace el envio
$("#form").submit(function(){
	nombre=$("#nombre").val();
	cp=$("#cp").val();
	if(nombre!=''){
		if( is_int(cp)){
			return true;
		}else{
			alert("Error: Codigo Postal no valido");	
				return false;
		}
	}else{
		alert("Error: Nombre de Cliente no valido");
			return false;
	}
	return false;	
});
</script>