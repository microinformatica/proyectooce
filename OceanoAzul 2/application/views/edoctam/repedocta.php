<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 70px 50px 20px; }
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: orange; }	
	/*#header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }*/ 
	/*#footer .page:after { content: counter(page, upper-roman); }*/ 
</style> 
<!--
<body> 
	<div id="header"> <h1>Widgets Express</h1> </div> 
	<div id="footer"> <p class="page">Page </p> </div> 
	<div id="content"> <p>the first page</p> 
	<p style="page-break-before: always;">the second page</p> 
	</div> 
</body> 
</html> 
--> 
<title>Estado de Cuenta</title>
</head>
<body>
	<div id="header"> 
		<table style="width: 770px; margin-left: -20px">
			<tr>
				<td width="610px">
					<p><img src="<?php echo base_url();?>assets/images/oceanoazul.jpg" width="200" height="60" border="0"></p>
				</td>	
				<td style="font-size: 14px; color: navy"><strong>ESTADO DE CUENTA</strong></td>			
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: navy;">
				<td width="160px">www.acuicolaocenaoazul.mx</td>	
				<td width="490">
					Av. Reforma No.2007-B Int. 803, Col. Flamingos, Mazatlán, Sin. México, RFC:AOA-180206-SI2, CP:82149 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
	</div>
	<?php 
	function weekday($fechas){
		    $fechas=str_replace("/","-",$fechas);
		    list($dia,$mes,$anio)=explode("-",$fechas);
		    return (((mktime ( 0, 0, 0, $mes, $dia, $anio) - mktime ( 0, 0, 0, 7, 17, 2006))/(60*60*24))+700000) % 7;
		}
		$fechas=date("m");
		if($fechas == 1 ) { $mes = "Enero"; }
		if($fechas == 2 ) { $mes = "Febrero"; }
		if($fechas == 3 ) { $mes = "Marzo"; }
		if($fechas == 4 ) { $mes = "Abril"; }
		if($fechas == 5 ) { $mes = "Mayo"; }
		if($fechas == 6 ) { $mes = "Junio"; }
		if($fechas == 7 ) { $mes = "Julio"; }
		if($fechas == 8 ) { $mes = "Agosto"; }
		if($fechas == 9 ) { $mes = "Septiembre"; }
		if($fechas == 10 ) { $mes = "Octubre"; }
		if($fechas == 11 ) { $mes = "Noviembre"; }
		if($fechas == 12 ) { $mes = "Diciembre"; }
		$diaN=weekday(date("d/m/Y"));
		//$diaN=weekday($mes);
		if ( $diaN == 0 ) { $dia="Lunes";}
		if ( $diaN == 1 ) { $dia="Martes";}
		if ( $diaN == 2 ) { $dia="Miércoles";}
		if ( $diaN == 3 ) { $dia="Jueves";}
		if ( $diaN == 4 ) { $dia="Viernes";}
		if ( $diaN == 5 ) { $dia="Sábado";}
		if ( $diaN == 6 ) { $dia="Domingo";}
	?>
<!--<label  style="font-size: 8px; margin-top: -15px">SIA - <?= $usuario?> - <?= $perfil?></label>-->
<table border="0" width="725px" style="margin-top: 5px">
	<tr> <td style="font-size: 16px; " colspan="2">	<?= $cli?></td> </tr>
	<tr> <td style="font-size: 12px; " colspan="2"> <?= $dom?></td>	</tr>
	<tr> <td style="font-size: 12px; " colspan="2"> <?= $lecp?></td> </tr>
	<tr> <td style="font-size: 14px; ">	<?php if($atc!=''){ ?> At´n:<u> <?= $atc?></u> <?php } ?> </td>
		 <td align="right" style="font-size: 13px; ">Detalle de movimientos al día <?= $dia.", ".date("d")." de ".$mes. " de ".date("Y")."."?></td>
	</tr>
</table>



	<table border="1"  style="font-size: 12px" width="725px">
    	  <?= $tabla?>    	  
	</table>



	<p>
	<table style="font-size: 12px;" border="0" width="725px">
		<thead>			
			<tr> <th colspan="8" align="justify" style="font-size: 14px">Sin más por el momento quedamos a sus ordenes para cualquier aclaración o duda al respecto.
				</th>
			</tr>
			<tr> <th colspan="8" align="right" style="font-size: 14px"><br /><?= $usuario?> <div style="font-size: 10px"></div>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr> <th colspan="7" align="left" style="font-size: 14px">ACUICOLA OCEANO AZUL S.A. DE C.V.</th></tr>	
			<tr ><th style="text-align: left" colspan="2">Banco</th><th style="width: 70px">Cuenta</th><th align="left">No. Clabe</th><th></th><th rowspan="3"></th><th align="right" style="width: 295px"><?= $perfil?></th></tr>
			<tr ><th rowspan="2" align="left" style="width: 90px">BBVA Bancomer</th><th align="right" style="width: 105px">Moneda Nacional</th><th style="width: 70px;">0111540734</th><th align="left">012744001115407346</th><th rowspan="2"> <?= $ref?></th><th align="right" style="width: 295px">Atención Telefónica: <?= $tel?>  </th></tr>
			<tr><th align="right" style="width: 105px">Dólares Americanos</th><th style="width: 70px">0111843508</th><th align="left">012744001118435085</th><th align="right" style="width: 295px">e-mail: <?= $correo?></th></tr>
		</tbody>
	</table>
	</p>	

<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(0,0,0);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>