<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 80px 50px 20px; }
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: navy; }	
</style>
<title>Banco</title>
</head>
<body>
	<?php 
		$f=explode("-",$fecha); 
      	$nummes=(int)$f[1]; 
      	$mes1="0-Enero-Febrero-Marzo-Abril-Mayo-Junio-Julio-Agosto-Septiembre-Octubre-Noviembre-Diciembre"; 
      	$mes1=explode("-",$mes1);       	
	?>	
	<div id="header"> 
		<table style="width: 770px;">
			<tr>
				<td width="200px">
					<p style="font-size: 8px;"  >
						<img src="<?php echo base_url();?>assets/images/aquapacificlogotipo.jpg" width="200" height="60" border="0">
					</p>
				</td>	
				<td align="center" style="font-size: 14px; color: navy"><strong></strong></td>			
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: white;">
				<td width="160px">www.aquapacific.com.mx</td>	
				<td width="490">
					Av. Emilio Barragán No.63-103, Col. Lázaro Cárdenas, Mazatlán, Sin. México, RFC:AQU-031003-CC9, CP:82040 - Tel:(669)985-64-46 
				</td>	
			</tr>
		</table> 
	</div>


<div style="margin-left: -25px">
	
	<table style="font-family: Verdana; font-size: 13">
		<tr>
			<th colspan="2" style="text-align: right">Mazatlán, Sinaloa  <?= $f[2]." de ".$mes1[$nummes]. " de ".$f[0]?>.<br /><br /> <br /></th>	
		</tr>
		<tr>
			<th colspan="2" style="text-align: left">AT’N. BANCOMER, S.A. <br /> EJECUTIVO DE CUENTA<br /><br /><br /></th>
		</tr>
		<tr>
			<th></th>
			<th style="width: 680px">
				<p style="text-align: justify">  	
				POR MEDIO DE LA PRESENTE SOLICITAMOS APERTURA DE CUENTA DE NOMINA DEL EMPLEADO QUE A CONTINUACION SE DETALLA AL AMPARO DEL CONTRATO DE PAGOS ELECTRONICOS No. 43595 <br /><br />
				<?= $empleado?> <br />
				<?= $dom;?> <?php if($col!=''){ ?> <?=', Col. '.$col;?> <?php } ?><br />
				<?= $ciu?>, <?= $edo?>. <br />
				RFC: <?= $rfc?> <br />
				TIPO DE IDENTIFICACION: <u><?= $ide?></u>  <br /><br /><br />
				Nombre del Beneficiario: <u><?= $ben?></u>  <br />
				Parentesco: <u><?= $par?></u>  y  Porcentaje <u>100 %</u> <br />
				Fecha de Nacimiento: _____________________________ <br /><br /><br />
				SIN MAS POR EL MOMENTO Y AGRADECIENDO DE ANTEMANO SU ATENCION. <br /><br /><br />
				</p>
				<br /><br />
			</th>	
		</tr>
		<tr>
			<th></th>
			<th style="width: 480px">
				<p style="text-align: center">  	
				Atentamente <br /><br /><br /><br /><br />
				___________________________________________<br />
				<?= $firma?><br />                                  	
				<?= $puesto?><br />
				AQUAPACIFIC S.A. de C.V.<br />
				CTA. 0141693174
				</p>
				<br />
			</th>	
		</tr>	
	</table>
	</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>