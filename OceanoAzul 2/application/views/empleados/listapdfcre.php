    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 20px 70px 20px; }
</style>  
<title>Credencial de Trabajo</title>
</head>
<body>
<div style="margin-left: -25px">
	<table border="1" style="font-size: 10px" width="680px" height="200px">
		<tr><th colspan="2"  rowspan="2" style="width: 337px; text-align: left" ><img style="margin-top: 1px;" src="<?php echo base_url();?>assets/images/oceanoazul.jpg" width="115" height="40" border="0" > <div style="margin-top: -40px; margin-left: 125px; font-size: 11px">OCEANO AZUL, S.A. DE C.V. <br />Avenida Reforma #2007-B 803  <br />Col. Flamingos, Mazatlán, Sin. CP 82149 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></th><th style="width: 88px" >I.M.S.S.</th><th style="width: 107px">R.F.C.</th><th style="width: 130px">C.U.R.P.</th></tr>
		<tr><th style="font-size: 10px; background-color: lightgrey"><?= $imss ?></th><th style="font-size: 10px; background-color: lightgrey"><?= $rfc ?></th><th style="font-size: 10px; background-color: lightgrey"><?= $curp ?></th></tr>
		<tr><th rowspan="5" style="border-bottom: none" >
			 
			 <?php if($fot==0) {?><img style="margin-left: -60px; margin-top: -20px" src="<?php echo base_url();?>assets/credencial/logoc.jpg" width="100" height="120" border="0"> <?php }else{?><img style="margin-left: -60px; margin-top: -20px" src="<?php echo base_url();?>assets/credencial/<?php echo $empl ?>" width="100" height="120" border="0"> <?php }?>
			 	
			</th><th style="text-align: left;">Nombre</th>
			<th colspan="3" rowspan="5" style="text-align: justify; font-size: 9px"> 
		<div style="font-size: 11px; text-align: center; margin-top: -10px ">"La calidad del producto es el reflejo de la calidad de las personas" </div>	
		<div style="width: 325px; margin-left: 5px" >
		Integridad: Actuamos con honradez, congruencia y rectitud. <br />
		Respeto: Nos tratamos con dignidad y confianza para propiciar la iniciativa y la creatividad.<br />
		Compromiso: Hacemos bien nuestro trabajo con dedicación y entrega para lograr resultados efectivos. <br />
		Trabajo en Equipo: Interactuamos mediante una comunicación sincera, compartiendo ideas y sentido de colaboración hacia una visión común. <br />
		Mejora Continua: Nos superamos mediante el continuo aprendizaje con el fin de alcanzar una mayor eficiencia y estabilidad a largo plazo. <br>
		Servicio: Estamos siempre dispuestos a servir efizcamente. 
		
		</div>
		</th>
			
		</tr>
		
		<tr><th style="font-size: 10px; background-color: lightgrey"><?= $nom ?></th></tr>
		<tr><th style="text-align: left; ">Domicilio</th></tr>
		<tr><th style="text-align: justify; font-size: 10px; background-color: lightgrey"><?= $dom ?></th></tr>
		<tr><th style="text-align: left; ">Departamento-Puesto</th></tr>
		<tr><th style="font-style: 6px; border-top:none; text-align: left" >Firma
			
		<!--	<img style="margin-top: -20px" src="<?php echo base_url();?>assets/credencial/<?php echo $firemp ?>" width="120" height="30" border="0">-->
		</th>
			</th><th style="font-size: 10px; background-color: lightgrey"><?= $depa.' - '. $pues ?></th><th colspan="3" style="font-size: 7px; text-align: right;" >
			
			
		<?php
		$fecha = date('Y-m-j');
		$nuevafecha = strtotime ( '+2 year' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'Y' , $nuevafecha );
		?>
			Validez: Diciembre de <?= $nuevafecha."."?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Firma Autorización<br /> 
			Este Documento es Intransferible, no es válido si presenta tachaduras o enmendaduras
				
			</th></tr>
	</table>
	<!--<img style="margin-top: -60px; margin-left: 600px" src="<?php echo base_url();?>assets/credencial/285f.jpg" width="70" height="30" border="0">-->
</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 7;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>