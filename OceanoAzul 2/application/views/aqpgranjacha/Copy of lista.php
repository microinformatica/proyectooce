<?php $this->load->view('header_cv');?>

<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 25px;
	/*background: none;*/		
}   
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>
<div style="height:600px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li style="font-size: 14px"><a href="#todos"><img src="<?php echo base_url();?>assets/images/menu/camarones.png" width="20" height="20" border="0"> Siembra</a> </li>
		<li style="font-size: 14px"><a href="#fac"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">Ajustes</a></li>
		<li style="font-size: 14px"><a href="#dia"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">Día</a></li>		
		<li style="font-size: 14px"><a href="#gral"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">General</a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="todos" class="tab_content"  style="height: 555px">
			<div id = "accordion" style="margin-top: -10px; margin-left: -10px; width: 958px; " >
					<h3 align="left" style="font-size: 12px;" ><a href="#"> Estanques </a></h3>
					<div style="height: 750px;">				
						<div class="ajaxSorterDiv" id="tabla_pis" name="tabla_pis" style="height: 508px;margin-top: -21px; ">                                		
                		<span class="ajaxTable" style="height: 468px; width: 860px" >
                    	<table id="mytablaPis" name="mytablaPis" class="ajaxSorter" style="height: 506px;">
                        	<thead title="Presione las columnas para ordenarlas como requiera">                            
                            	<th data-order = "pisg" >Estanque</th>                                                        							                                                                                                              
                            	<th data-order = "hasg" >Has</th>
                            	<th data-order = "espg">Especie</th> 
                            	<th data-order = "orgg">Organismos</th>  
                            	<th data-order = "den" >Densidad</th>                            
                            	<th data-order = "prog" >Procedencia</th>
                            	<th data-order = "fecg1" >Fecha</th>
                            	<th data-order = "plg" >Estadío</th>
                        	</thead>
                        	<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style=" font-size: 10px; margin-top: -10px ">     
             				   
	                    	<form method="post" action="<?= base_url()?>index.php/aqpgranja/pdfrep/"  >
	                    		<ul class="order_list" style="width: 420px; margin-top: -10px ">
             					Granja 
             					<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
             						<select name="cmbGranja" id="cmbGranja" style="font-size: 10px; height: 25px; " >								
    									<option value="2">Gran Kino</option>		
    									<option value="3">Jazmin</option>
    									<option value="4">Ahome</option>
    								</select>
    							<?php } elseif($usuario=="Diana Peraza"){ ?>
    								<select name="cmbGranja" id="cmbGranja" style="font-size: 10px; height: 25px;  " >								
    									<option value="2">Gran Kino</option>		
    									<option value="4">Ahome</option>
    								</select>
    							<?php }elseif($usuario=="Antonio Valdez"){ ?>	 
    								<select name="cmbGranja" id="cmbGranja" style="font-size: 10px; height: 25px;  " >								
    									<option value="4">Ahome</option>
    								</select>
    							<?php }else{ ?>
    								<select name="cmbGranja" id="cmbGranja" style="font-size: 10px; height: 25px;  " >								
    									<option value="2">Gran Kino</option>
    								</select>
    							<?php } ?>	
             					 		
    							Ciclo
    							<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
    								<select name="cmbCiclop" id="cmbCiclop" style="font-size: 10px;height: 25px; ">
      										<?php $ciclof=15; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>
    							<?php } else{ ?>
    								<select name="cmbCiclop" id="cmbCiclop" style="font-size: 10px;height: 25px; ">
      										<?php $ciclof=17; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>	
    							<?php } ?>	    
						    	</ul>  
    	                		<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tabla" value ="" class="htmlTable"/>
                    	    	                        		                        		                                                      
 							</form>   
                		</div>                                      
            			</div>             	
            		
            	 	</div> 
            	 	<?php if($usuario=="Joel Lizárraga"){ ?>
        			<h3 align="left" style="font-size: 12px"><a href="#">Información Estanque</a></h3>
        			<?php }else{ ?>
        			<h3 align="left" style="font-size: 12px"><a href="#">Actualizar</a></h3>	
        			<?php } ?>
		       	 	<div style="height: 150px;">
		       	 		<table style="width: 458px" border="2px"  >
							<thead style="background-color: #DBE5F1" >
								<th colspan="8" height="15px" style="font-size: 14px">Datos de Siembra - Granja:
									<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
             						<select name="cmbGranjas" id="cmbGranjas" style="font-size: 10px; height: 25px; " >								
    									<option value="2">Gran Kino</option>		
    									<option value="3">Jazmin</option>
    									<option value="4">Ahome</option>
    								</select>
    							<?php } elseif($usuario=="Diana Peraza"){ ?>
    								<select name="cmbGranjas" id="cmbGranjas" style="font-size: 10px; height: 25px;  " >								
    									<option value="2">Gran Kino</option>		
    									<option value="4">Ahome</option>
    								</select>
    							<?php }elseif($usuario=="Antonio Valdez"){ ?>	 
    								<select name="cmbGranjas" id="cmbGranjas" style="font-size: 10px; height: 25px;  " >								
    									<option value="4">Ahome</option>
    								</select>
    							<?php }else{ ?>
    								<select name="cmbGranjas" id="cmbGranjas" style="font-size: 10px; height: 25px;  " >								
    									<option value="2">Gran Kino</option>
    								</select>
    							<?php } ?>	
									<input type="hidden" size="4%" type="text" name="idpis" id="idpis" value="" style="text-align: right;  border: 0"></th>
							</thead>
							<tbody style="background-color: #F7F7F7">
								<tr >
									<th style="text-align: center">Ciclo</th>
									<th style="text-align: center">Estanque</th>
									<th style="text-align: center">Has</th>
									<th style="text-align: center">Especie</th>
									<th style="text-align: center">Organismos</th>
									<th style="text-align: center">Procedencia</th>
									<th style="text-align: center">Fecha</th>
									<th style="text-align: center">Estadío</th>
																												
      							</tr>															
								<tr style="background-color: white; font-size: 12px">
									
									<th style="text-align: center" align="center">
									<select name="cica" id="cica" style=" background-color: red;  font-size: 12px;height: 25px; margin-top: 1px;">
      										<?php $ciclof=15; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>
    								</th>
    								<th style="text-align: center"><input size="3%" type="text" name="pisg" id="pisg" value="" style="text-align: right;  border: 0"></th>
    								<th style="text-align: center"><input size="5%" type="text" name="hasg" id="hasg" value="" style="text-align: right;  border: 0"></th>
									<th style="text-align: center"><input size="18%" type="text" name="espg" id="espg" style="text-align: center; border: 0" value="Litopenaeus vannamei"></th>
									<th style="text-align: center"><input size="13%" type="text" name="orgg" id="orgg" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" value="" style="text-align: right;  border: 0"></th>
									<th style="text-align: center"><input size="20%" type="text" name="prog" id="prog" style="text-align: center; border: 0" value="Aquapacfic S.A. de C.V."></th>
									<th style="text-align: center"><input size="12%" type="text" name="fecg" id="fecg" class="fecha redondo mUp" style="text-align: center;  border: 0" ></th>								
									<th style="text-align: center"><input size="4%" type="text" name="plg" id="plg" style="text-align: center; border: 0"></th>
								</tr>
								</tbody>
								<tfoot style="background-color: #DBE5F1">
									<th colspan="8" >
										<center>
										<?php if($usuario=="Jesus Benítez"){ ?>
											<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
											<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
										<?php }?>	
										<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar()" />
										</center>	
									</th>
								</tfoot>
								
    					</table>  
        			</div>
	 		</div>	
	 	</div>
		<div id="fac" class="tab_content" style="height: 555px" >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
					<th>			
						<input type="hidden" readonly="true" size="2%"  type="text" name="idfac" id="idfac">
						<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
             						<select name="cmbGranjaf" id="cmbGranjaf" style="font-size: 10px; height: 25px; " >								
    									<option value="2">Gran Kino</option>		
    									<option value="3">Jazmin</option>
    									<option value="4">Ahome</option>
    								</select>
    							<?php } elseif($usuario=="Diana Peraza"){ ?>
    								<select name="cmbGranjaf" id="cmbGranjaf" style="font-size: 10px; height: 25px;  " >								
    									<option value="2">Gran Kino</option>		
    									<option value="4">Ahome</option>
    								</select>
    							<?php }elseif($usuario=="Antonio Valdez"){ ?>	 
    								<select name="cmbGranjaf" id="cmbGranjaf" style="font-size: 10px; height: 25px;  " >								
    									<option value="4">Ahome</option>
    								</select>
    							<?php }else{ ?>
    								<select name="cmbGranjaf" id="cmbGranjaf" style="font-size: 10px; height: 25px;  " >								
    									<option value="2">Gran Kino</option>
    								</select>
    							<?php } ?>	
						Res: <input readonly="true" size="2%"  type="text" name="resfac" id="resfac">
						% Téc: <input onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right" size="4%"  type="text" name="porbio" id="porbio" />
    	    	        <?php if($usuario=="Jesus Benítez" || $usuario=="Julio Lizarraga" || $usuario=="Antonio Valdez"){ ?>
							<input type="submit" id="aceptarfac" name="aceptarfac" value="Guardar" />
						<?php }?>		 		
                 		<div class="ajaxSorterDiv" id="tabla_fac" name="tabla_fac" style="width: 305px;  "  >                 			
							<span class="ajaxTable" style="margin-top: 1px;height: 480px; " >
                    			<table id="mytablaFac" name="mytablaFac" class="ajaxSorter" border="1" style="width: 303px;" >
                        			<thead>                            
                            			<th data-order = "resfac" >Resultado</th>
                            			<th data-order = "porfac" >Sistema</th>
                            			<th data-order = "porbio"  >Tecnico</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form action="<?= base_url()?>index.php/aqpgranja/pdfrepfac" method="POST" >							
    	    	            		<ul class="order_list" style="width: 250px; margin-top: 1px">
    	    	            				
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablafac" value ="" class="htmlTable"/> 
								</form>   
                			</div>       	                	
	                	</div>
	           		</th>
            	</tr>
			</table>	
		</div>
		<div id="dia" class="tab_content" style="height: 555px"  >
			<table border="1" style="margin-top: -5px" >
				 			<tr>
				 				<th rowspan="2" style="text-align: center">Granja</th><th rowspan="2" style="text-align: center">Ciclo</th>
				 				<th rowspan="2" style="text-align: center">Est</th><th rowspan="2" style="text-align: center">Día</th><th rowspan="2" style="text-align: center">Turno</th>
				 				<th colspan="6"  style="text-align: center">Charolas</th><th rowspan="2" style="text-align: center">Téc Kg</th>
				 				<th rowspan="3">
				 					Grafica: <button type="button" id="aligra" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Alimento</button>
				 					 <br /> <br />
				 					<input type="submit" id="aceptarcha" name="aceptarcha" value="Guardar" />
									<input type="submit" id="borrarcha" name="borrarcha" value="Borrar" />
									<input type="submit" id="nuevocha" name="nuevocha" value="Nuevo"  /> 
								</th>
				 			</tr>
				 			<tr>
				 				<th style="text-align: center">1</th><th style="text-align: center">2</th><th style="text-align: center">3</th>
				 				<th style="text-align: center">4</th><th style="text-align: center">5</th><th style="text-align: center">6</th>
				 			</tr>
				 			<tr>
				 				<th>
				 					<input type="hidden" readonly="true" size="2%"  type="text" name="idcha" id="idcha">
				 					<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
             						<select name="numgracha" id="numgracha" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="2">Gran Kino</option>		
    									<option value="3">Jazmin</option>
    									<option value="4">Ahome</option>
    								</select>
    							<?php } elseif($usuario=="Diana Peraza"){ ?>
    								<select name="numgracha" id="numgracha" style="font-size: 10px; height: 25px;  margin-top: 1px  " >								
    									<option value="2">Gran Kino</option>		
    									<option value="4">Ahome</option>
    								</select>
    							<?php }elseif($usuario=="Antonio Valdez"){ ?>	 
    								<select name="numgracha" id="numgracha" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="4">Ahome</option>
    								</select>
    							<?php }else{ ?>
    								<select name="numgracha" id="numgracha" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="2">Gran Kino</option>
    								</select>
    							<?php } ?>	
				 				</th>
				 				<th>
				 					<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
    					<select name="ciccha" id="ciccha" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      						<?php $ciclof=15; $actual=date("y"); //$actual+=1;
								while($actual >= $ciclof){?>
									<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
									<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            						<?php  $actual-=1; } ?>      									
    					</select>
    					<?php } else{ ?>
    					<select name="ciccha" id="ciccha" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      						<?php $ciclof=17; $actual=date("y"); //$actual+=1;
								while($actual >= $ciclof){?>
									<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
									<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            						<?php  $actual-=1; } ?>      									
    					</select>	
    					<?php } ?>	
				 				</th>
				 				<th>
				 					<select name='idpischa' id="idpischa" style="font-size: 10px;height: 25px;margin-top: 1px;"  ></select>
				 				</th>
				 				<th>
				 					<input size="10%" type="text" name="feccha" id="feccha" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
				 				</th>
				 				<th>
				 					<select name="turno" id="turno" style="font-size: 10px; height: 25px;margin-top: 1px;  " >								
    									<option value="1">a.m.</option>		
    									<option value="2">p.m.</option>
    								</select>
				 				</th>
				 				<th><input onKeyPress="return valida(event)" onFocus="style.backgroundColor='black';style.color='white'" onBlur="style.backgroundColor='green';style.color='white'" style="font-size:12px; width: 14px; background:blue; color:white; text-align: center" type="text" name="ch1"  id="ch1" value="0" /></th>
				 				<th><input onKeyPress="return valida(event)" onFocus="style.backgroundColor='black';style.color='white'" onBlur="style.backgroundColor='green';style.color='white'" style="font-size:12px; width: 14px; background:blue; color:white; text-align: center" type="text" name="ch2"  id="ch2" value="0"/></th>
				 				<th><input onKeyPress="return valida(event)" onFocus="style.backgroundColor='black';style.color='white'" onBlur="style.backgroundColor='green';style.color='white'" style="font-size:12px; width: 14px; background:blue; color:white; text-align: center" type="text" name="ch3"  id="ch3" value="0"/></th>
				 				<th><input onKeyPress="return valida(event)" onFocus="style.backgroundColor='black';style.color='white'" onBlur="style.backgroundColor='green';style.color='white'" style="font-size:12px; width: 14px; background:blue; color:white; text-align: center" type="text" name="ch4"  id="ch4" value="0"/></th>
				 				<th><input onKeyPress="return valida(event)" onFocus="style.backgroundColor='black';style.color='white'" onBlur="style.backgroundColor='green';style.color='white'" style="font-size:12px; width: 14px; background:blue; color:white; text-align: center" type="text" name="ch5"  id="ch5" value="0"/></th>
				 				<th><input onKeyPress="return valida(event)" onFocus="style.backgroundColor='black';style.color='white'" onBlur="style.backgroundColor='green';style.color='white'" style="font-size:12px; width: 14px; background:blue; color:white; text-align: center" type="text" name="ch6"  id="ch6" value="0"/></th>
				 				<th><input onKeyPress="return valida1(event)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='blue'; style.color='white'" style="font-size:12px; width: 40px" type="text" name="kgt"  id="kgt"/></th>
				 				<script>
									function valida(e){
    									tecla = (document.all) ? e.keyCode : e.which;
									    //Tecla de retroceso para borrar, siempre la permite
    									if (tecla==8){
        									return true;
    									}
        							    // Patron de entrada, en este caso solo acepta numeros
        							    if (tecla>=48 && tecla<=51){
    									patron =/[0-3]/;
    									tecla_final = String.fromCharCode(tecla);
    									return patron.test(tecla_final);
    									}else{
    										return false;
    									}
									}
									function valida1(e){
    									tecla = (document.all) ? e.keyCode : e.which;
									    //Tecla de retroceso para borrar, siempre la permite
    									if (tecla==8){
        									return true;
    									}
        							    // Patron de entrada, en este caso solo acepta numeros
        							    if((tecla>=48 && tecla<=57) || (tecla==46)){
    									patron =/[0-9,.]/;
    									tecla_final = String.fromCharCode(tecla);
    									return patron.test(tecla_final);
    									}else{
    										return false;
    									}
									}
								</script>
				 			</tr>
				 		</table>
				 		   		
             <div id='ver_informacion' class='hide' >			
			<table border="0" style="margin-left: -5px; margin-top: -20px; ">
				<tr>
				 	<th >	
				 					 		
                 		<div class="ajaxSorterDiv" id="tabla_diacha" name="tabla_diacha" style="width: 940px"  >                 			
							<span class="ajaxTable" style="height: 475px; margin-top: 1px; " >
                    			<table id="mytablaDiaCha" name="mytablaDiaCha" class="ajaxSorter" border="1" style="width: 920px" >
                        			<thead>     
                        				<tr style="font-size: 12px">
                        					<th data-order = "pisg" rowspan="2" >Est</th>
                        					<th colspan="2" >Alimento Anterior</th>
                        					<th >Día</th>
                        					<th colspan="7">Lecturas Charolas A.M.</th>
                        					
                        					<th colspan="2" >Sistema</th>
                        					<th colspan="2" >Técnico</th>
                        					<th colspan="3" >Ajuste - Has
                        						<input id='hasga' name="hasga" style="width: 40px; border: 0; background: none"  />
                        					</th>     
                        					<th >A.M.</th>
                            				<th >P.M.</th>    
                            				<!--<th >Alimento Neto</th>-->   
                            				<th colspan="7">Lecturas Charolas P.M.</th>
                        				</tr>                       
                            			<tr style="font-size: 9px">
                            				<th style="width: 38px" data-order = "fecant" >Día</th>
                            				<th data-order = "aliant" >Kg</th>
                            				<th style="width: 38px" data-order = "feccha1">Actual</th>
                            				<th data-order = "ch1">1</th>
                            				<th data-order = "ch2">2</th>
                            				<th data-order = "ch3">3</th>
                            				<th data-order = "ch4">4</th>
                            				<th data-order = "ch5">5</th>
                            				<th data-order = "ch6">6</th>
                            				<th data-order = "res">Res</th>
                            				
                            				<th style="width: 42px" data-order = "facsis">% [+/-]</th>
                            				<th data-order = "kgssis">Kg</th>
                            				<th style="width: 42px" data-order = "facbio">% [+/-]</th>
                            				<th data-order = "kgsbio">Kg</th>
                            				<th style="width: 42px" data-order = "pr">% [+/-]</th>
                            				<th data-order = "kgstec">Kg</th>
                            				<th data-order = "kgsha">Kg/Ha</th>
                            				<?php if($usuario!="Julio Lizárraga"){ ?>
                            				<th data-order = "kgsam">30%</th>
                            				<th data-order = "kgspm">70%</th>
                            				<?php }else{ ?>
                            				<th data-order = "kgsam">20%</th>
                            				<th data-order = "kgspm">80%</th>
                            				<?php } ?>
                            				<!--<th data-order = "totkgd">Kg Día</th>-->
                            				<th data-order = "cp1">1</th>
                            				<th data-order = "cp2">2</th>
                            				<th data-order = "cp3">3</th>
                            				<th data-order = "cp4">4</th>
                            				<th data-order = "cp5">5</th>
                            				<th data-order = "cp6">6</th>
                            				<th data-order = "resp">Res</th>
                            			</tr>
                            			
                            		</thead>
                        			<tbody style="font-size: 12px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style="font-size: 12px; margin-top: -10px;text-align: left">
             			 		 
								<form action="<?= base_url()?>index.php/aqpgranjacha/pdfdiacha" method="POST" >	
									<ul class="order_list" style="margin-top: 1px; width: 830px; text-align: left">
									Alimento Acumulado por Sección: 1 <input id='sec1' name="sec1" style="width: 80px; text-align: center; background-color: lightgray"/> 2 <input id='sec2' name="sec2" style="width: 80px; text-align: center; background-color: #695" /> 3 <input id='sec3' name="sec3" style="width: 80px; text-align: center;background-color: #799" /> 
									<input type="hidden" id='granjaa' name="granjaa" />
        	            	    	<input type="hidden" id="diaali" name="diaali" class="fecha redondo mUp" readonly="true" />
									</ul>						
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablacha" value ="" class="htmlTable"/>
                    	    		
								</form>   
                			</div>  
                    	</div>
	        		</th>
            	</tr>
			</table>
			</div>
			
			<div id='ver_aligra' class='hide' style="height: 90px" >
            	<div name="aligraf" id="aligraf" style="min-width: 900px; height: 430px; margin: 0 auto"></div>
            	<button type="button" id="aligra1" style="cursor: pointer; background-color: gray" class="continuar used" >Cerrar</button>
            </div>
		</div>
		<div id="gral" class="tab_content" style="height: 555px" >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
					<th>			
						<div class="ajaxSorterDiv" id="tabla_gral" name="tabla_gral" style="width: 725px;  "  >    
							<span class="ajaxTable" style="margin-top: 1px;height: 530px; " >
                    			<table id="mytablaGral" name="mytablaGral" class="ajaxSorter" border="1" style="width: 703px; font-size: 18px" >
                        			<thead> 
                        				<tr> <th colspan="11" style="text-align: center">Consumo de Alimentos </th> </tr>                           
                        				<tr>
                            			<th rowspan="2" data-order = "diam" >Día</th>
                            			<th colspan="4" style="text-align: center">Sistema</th>
                            			<th colspan="4" style="text-align: center">Real</th>
                            			<th colspan="2" style="text-align: center">KG Incremento</th>
                            			</tr>
                            			<tr>
                            			<th data-order = "sistema" style="text-align: center">Kg</th>
                            			<th data-order = "incsis" style="text-align: center">Inc Día</th>
                            			<th data-order = "porsis" style="text-align: center">%</th>
                            			<th data-order = "kgsis" style="text-align: center">Kg/Ha</th>
                            			<th data-order = "diario" style="text-align: center">Kg</th>
                            			<th data-order = "inctec" style="text-align: center">Inc Día</th>
                            			<th data-order = "portec" style="text-align: center">%</th>
                            			<th data-order = "kgtec" style="text-align: center">Kg/Ha</th>
                            			<th data-order = "diferencia" style="text-align: center">Sis VS Real</th>
                            			<th data-order = "acumulado" style="text-align: center">Acumulado</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 14px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form action="<?= base_url()?>index.php/aqpgranjacha/pdfrepgral" method="POST" >							
    	    	            		<ul class="order_list" style="width: 450px; margin-top: 1px">
    	    	            			Granja
							   <?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
             						<select name="numgrachat" id="numgrachat" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="2">Gran Kino</option>		
    									<option value="3">Jazmin</option>
    									<option value="4">Ahome</option>
    								</select>
    							<?php } elseif($usuario=="Diana Peraza"){ ?>
    								<select name="numgrachat" id="numgrachat" style="font-size: 10px; height: 25px;  margin-top: 1px  " >								
    									<option value="2">Gran Kino</option>		
    									<option value="4">Ahome</option>
    								</select>
    							<?php }elseif($usuario=="Antonio Valdez"){ ?>	 
    								<select name="numgrachat" id="numgrachat" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="4">Ahome</option>
    								</select>
    							<?php }else{ ?>
    								<select name="numgrachat" id="numgrachat" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="2">Gran Kino</option>
    								</select>
    							<?php } ?>	
				 				Ciclo
				 					<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
    					<select name="cicchat" id="cicchat" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      						<?php $ciclof=15; $actual=date("y"); //$actual+=1;
								while($actual >= $ciclof){?>
									<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
									<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            						<?php  $actual-=1; } ?>      									
    					</select>
    					<?php } else{ ?>
    					<select name="cicchat" id="cicchat" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      						<?php $ciclof=17; $actual=date("y"); //$actual+=1;
								while($actual >= $ciclof){?>
									<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
									<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            						<?php  $actual-=1; } ?>      									
    					</select>	
    					<?php } ?>	   
    					Mes:
   						<select name="cmbMesgral" id="cmbMesgral" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   							<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   							<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   							<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   							<option value="10" >Octubre</option><option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   						</select>
    					Has   <input disabled="true"  id='ha' name="ha" style="width: 50px" />    
    	    	            			
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablagral" value ="" class="htmlTable"/> 
								</form>   
                			</div>       	                	
	                	</div>
	           		</th>
            	</tr>
			</table>	
		</div>
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$('#aligra').click(function(){$(this).removeClass('used');$('#ver_informacion').hide();$('#ver_aligra').show();});
$('#aligra1').click(function(){$(this).removeClass('used');$('#ver_aligra').hide();$('#ver_informacion').show();});
$("#aceptarfac").click( function(){		
	por=$("#porbio").val();	numero=$("#idfac").val();
	if(por!=''){
	  	$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/aqpgranjacha/actualizarfac", 
				data: "id="+$("#idfac").val()+"&por="+$("#porbio").val()+"&gra="+$("#cmbGranjaf").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#idfac").val('');$("#resfac").val('');$("#porbio").val('');
								$("#mytablaFac").trigger("update");	
								$("#mytablaGral").trigger("update");
								$("#mytablaDiaCha").trigger("update");
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}		
			});
	}else{
		alert("Error: Seleccione Resultado de Tabla de Ajuste");		
		return false;
	}
});


//busca datos del turno
$("#turno").change( function(){	
 	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/aqpgranjacha/buscar", 
			data: "pil="+$("#idcha").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#ch1").val(obj.cp1);$("#ch2").val(obj.cp2);
					$("#ch3").val(obj.cp3);$("#ch4").val(obj.cp4);
					$("#ch5").val(obj.cp5);$("#ch6").val(obj.cp6);
   			} 
	});			 
});

$("#nuevocha").click( function(){	
	$("#idcha").val(''); //$("#idpischa").val('');
	$("#ch1").val('0');$("#ch2").val('0');$("#ch3").val('0');$("#ch4").val('0');$("#ch5").val('0');$("#ch6").val('0');
	$("#kgt").val('');
	$("#idsob").focus();
	return true;
});

$("#aceptarcha").click( function(){		
	fec=$("#feccha").val();est=$("#idpischa").val();
	numero=$("#idcha").val();
	if( est!='0'){
	  if( fec!=''){
	  	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjacha/actualizardia", 
						data: "id="+$("#idcha").val()+"&pis="+$("#idpischa").val()+"&fec="+$("#feccha").val()+"&cic="+$("#ciccha").val()+"&gra="+$("#numgracha").val()+"&ch1="+$("#ch1").val()+"&ch2="+$("#ch2").val()+"&ch3="+$("#ch3").val()+"&ch4="+$("#ch4").val()+"&ch5="+$("#ch5").val()+"&ch6="+$("#ch6").val()+"&tur="+$("#turno").val()+"&kgt="+$("#kgt").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevocha").click();
										$("#mytablaDiaCha").trigger("update");	$("#mytablaGral").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjacha/agregardia", 
						data: "pis="+$("#idpischa").val()+"&fec="+$("#feccha").val()+"&cic="+$("#ciccha").val()+"&gra="+$("#numgracha").val()+"&ch1="+$("#ch1").val()+"&ch2="+$("#ch2").val()+"&ch3="+$("#ch3").val()+"&ch4="+$("#ch4").val()+"&ch5="+$("#ch5").val()+"&ch6="+$("#ch6").val()+"&tur="+$("#turno").val()+"&kgt="+$("#kgt").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevocha").click();
										$("#mytablaDiaCha").trigger("update");	$("#mytablaGral").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		
		}else{
			alert("Error: Seleccione Fecha");
			$("#feccha").focus();
			return false;
		}					
	}else{
		alert("Error: Seleccione Estanque");		
		$("#idpischa").focus();
		return false;
	}
});

$("#borrarcha").click( function(){	
	numero=$("#idcha").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjacha/borrarcha", 
						data: "id="+$("#idcha").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#nuevocha").click();
										$("#mytablaDiaCha").trigger("update");		$("#mytablaGral").trigger("update");																							
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Estanque para poder Eliminarlo");
		return false;
	}
});

$("#numgracha").change( function(){	
	$("#mytablaDiaCha").trigger("update");	
	$("#granjaa").val($("#numgracha").val());
});

$("#ciccha").change( function(){	
	$("#cicchad").val($("#ciccha").val());
	return true;
});

$("#idpischa").change( function(){	
	$("#ested").val($("#idEst").val());pil=$("#idEst").val();
	$("#hasga").val('');
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/aqpgranjacha/buscarhas", 
			data: "pil="+$("#idpischa").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#hasga").val(obj.hasga);
   			} 
	});	
	
});




$("#feccha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
	onSelect: function( selectedDate ){
		$("#mytablaDiaCha").trigger("update");	
		$("#diaali").val($("#feccha").val());	
	}
});	


jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  



$(document).ready(function(){
	$('#ver_informacion').show();
	$('#ver_aligra').hide();
	if($("#numgracha").val()==2){$("#granjasel").val('Granja Gran kino');$("#ha").val('331.800');}else{$("#granjasel").val('Granja Ahome');$("#ha").val('332.902');}
	$("#granjaa").val($("#numgracha").val());
	$("#granja").val($("#cmbGranja").val());
	var f = new Date();
	$("#feccha").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#cmbMesgral").val((f.getMonth() +1));	
	$("#diaali").val($("#feccha").val());
	$("#tabla_pis").ajaxSorter({
	url:'<?php echo base_url()?>index.php/aqpgranja/tabla',  	
	filters:['cmbGranja','cmbCiclop'],		
    sort:false,
    onRowClick:function(){
    	if($(this).data('idpis')>'0'){
        	$("#accordion").accordion( "activate",1 );
        	$("#idpis").val($(this).data('idpis'));
        	$("#pisg").val($(this).data('pisg'));
       		$("#hasg").val($(this).data('hasg'));       	
			$("#espg").val($(this).data('espg'));
       		$("#orgg").val($(this).data('orgg'));       	
			$("#prog").val($(this).data('prog'));
			$("#fecg").val($(this).data('fecg'));
			$("#plg").val($(this).data('plg'));
			$("#cica").val($(this).data('cicg'));
			$("#fecgc").val($(this).data('fecgc'));
			$("#biogc").val($(this).data('biogc'));
			$("#ppgc").val($(this).data('ppgc'));
			$("#obsc").val($(this).data('obsc'));
			$("#cmbGranjas").val($(this).data('numgra'));
		}		
    }, 
    onSuccess:function(){
    		$('#tabla_pis tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_pis tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_pis tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
	    	 $('#tabla_pis tr').map(function(){	
	    		$("#ha").val($(this).data('hasg'));
	    	});	
    },     
	});
	$("#tabla_diacha").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjacha/tabladiacha',  
		filters:['numgracha','ciccha','feccha','idpischa'],
        sort:false,
         onRowClick:function(){
    	if($(this).data('idpischa')>'0'){
        	$("#idcha").val($(this).data('idcha'));
        	$("#numgracha").val($(this).data('numgracha'));
        	$("#ciccha").val($(this).data('ciccha'));
        	$("#idpischa").val($(this).data('idpischa'));
        	$("#feccha").val($(this).data('feccha'));
			$("#turno").val($(this).data('turno'));
			$("#ch1").val($(this).data('ch1'));
			$("#ch2").val($(this).data('ch2'));
			$("#ch3").val($(this).data('ch3'));
			$("#ch4").val($(this).data('ch4'));
			$("#ch5").val($(this).data('ch5'));
			$("#ch6").val($(this).data('ch6'));
			$("#kgt").val($(this).data('kgt'));
			
		}		
    }, 
        onSuccess:function(){
        	$('#tabla_diacha tbody tr td:nth-child(4)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
	    	$('#tabla_diacha tr').map(function(){	
	    		pis=$(this).data('idpischa');inc=0;est=$(this).data('pisg1');
	    		t1=$(this).data('kgstec'); has=$(this).data('tothas'); tota=$(this).data('aliant');inc=$(this).data('inc');hasa=$(this).data('tothasa');
	    		$("#sec1").val($(this).data('totkgds1'));s1=$(this).data('totkgds1');   		
	    		$("#sec2").val($(this).data('totkgds2'));s2=$(this).data('totkgds2'); 
	    		$("#sec3").val($(this).data('totkgds3'));s3=$(this).data('totkgds3'); 
	    		Tex='Incremento';
	    		if(parseFloat(inc)<0)  Tex='Decremento';
	    	});	
        	if(pis>0){
        	tr='<tr ><td colspan=15 style=background-color:lightblue>ALIMENTO TOTAL</td><td colspan=3 style=background-color:lightgray>'+s1+'</td><td colspan=2 style=background-color:lightgray>Sección 1</td> <td colspan=7 style=background-color:lightblue> </td></tr>';
	    	$('#tabla_diacha tbody').append(tr); 
	    	tr='<tr ><td colspan=4>Anterior</td><td colspan=7>'+Tex+'</td><td colspan=4>Actual</td><td colspan=3 style=background-color:#695>'+s2+'</td><td colspan=2 style=background-color:#695>Sección 2</td><td colspan=7 style=background-color:lightblue> </td> </tr>';
	    	$('#tabla_diacha tbody').append(tr);
	    	tr='<tr ><td colspan=4>'+tota+' kg - '+hasa+' kg/ha</td><td colspan=7>'+inc+'</td><td colspan=4>'+t1+' kg - '+has+' kg/ha</td><td colspan=3 style=background-color:#799>'+s3+'</td><td colspan=2 style=background-color:#799>Sección 3</td><td colspan=7 style=background-color:lightblue> </td> </tr>';
	    	$('#tabla_diacha tbody').append(tr);
	    	}
        	$('#tabla_diacha tr').map(function(){$(this).css('text-align','center');});	
    	   	$('#tabla_diacha tbody tr td:nth-child(3)').map(function(){$(this).css('font-weight','bold');})
    	   	
    	   	$('#tabla_diacha tbody tr td:nth-child(11)').map(function(){$(this).css('font-weight','bold');})
	    	$('#tabla_diacha tbody tr td:nth-child(16)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
	    	$('#tabla_diacha tbody tr td:nth-child(17)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
	    	$('#tabla_diacha tbody tr td:nth-child(18)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
	    	$('#tabla_diacha tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=1 && $(this).html()<=19){$(this).css("background-color", "lightgray");}
	    		if($(this).html()>=20 && $(this).html()<=39){$(this).css("background-color", "#695");}
	    		if($(this).html()>=40 && $(this).html()<=56){$(this).css("background-color", "#799");}
	    	});	
	    	
	    	
	    	$('#tabla_diacha tbody tr td:nth-child(11)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 0 && $(this).html() <= 2 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 3 && $(this).html() <= 4 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 5 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "white");
        			}
        			
	    	});
	    	g1=0;g2=0;g3=0;g4=0;g5=0;g6=0;g7=0;g8=0;g9=0;g10=0;gp1=0;gp2=0;gp3=0;gp4=0;gp5=0;gp6=0;gp7=0;gp8=0;gp9=0;gp10=0;
	    	d1='';d2='';d3='';d4='';d5='';d6='';d7='';d8='';d9='';d10='';
	    	$('#tabla_diacha tbody tr').map(function(){
	    		if($(this).data('gram1')>0){g1=$(this).data('gram1');}
	    		if($(this).data('gram2')>0){g2=$(this).data('gram2');}
	    		if($(this).data('gram3')>0){g3=$(this).data('gram3');}
	    		if($(this).data('gram4')>0){g4=$(this).data('gram4');}
	    		if($(this).data('gram5')>0){g5=$(this).data('gram5');}
	    		if($(this).data('gram6')>0){g6=$(this).data('gram6');}
	    		if($(this).data('gram7')>0){g7=$(this).data('gram7');}
	    		if($(this).data('gram8')>0){g8=$(this).data('gram8');}
	    		if($(this).data('gram9')>0){g9=$(this).data('gram9');}
	    		if($(this).data('gram10')>0){g10=$(this).data('gram10');}
	    		if($(this).data('grap1')>0){gp1=$(this).data('grap1');}
	    		if($(this).data('grap2')>0){gp2=$(this).data('grap2');}
	    		if($(this).data('grap3')>0){gp3=$(this).data('grap3');}
	    		if($(this).data('grap4')>0){gp4=$(this).data('grap4');}
	    		if($(this).data('grap5')>0){gp5=$(this).data('grap5');}
	    		if($(this).data('grap6')>0){gp6=$(this).data('grap6');}
	    		if($(this).data('grap7')>0){gp7=$(this).data('grap7');}
	    		if($(this).data('grap8')>0){gp8=$(this).data('grap8');}
	    		if($(this).data('grap9')>0){gp9=$(this).data('grap9');}
	    		if($(this).data('grap10')>0){gp10=$(this).data('grap10');}
	    		if($(this).data('fec1')!=''){d1=$(this).data('fec1');}
	    		if($(this).data('fec2')!=''){d2=$(this).data('fec2');}
	    		if($(this).data('fec3')!=''){d3=$(this).data('fec3');}
	    		if($(this).data('fec4')!=''){d4=$(this).data('fec4');}
	    		if($(this).data('fec5')!=''){d5=$(this).data('fec5');}
	    		if($(this).data('fec6')!=''){d6=$(this).data('fec6');}
	    		if($(this).data('fec7')!=''){d7=$(this).data('fec7');}
	    		if($(this).data('fec8')!=''){d8=$(this).data('fec8');}
	    		if($(this).data('fec9')!=''){d9=$(this).data('fec9');}
	    		if($(this).data('fec10')!=''){d10=$(this).data('fec10');}
	    	});		
			var chart
			g1=parseFloat(g1);g2=parseFloat(g2);g3=parseFloat(g3);g4=parseFloat(g4);g5=parseFloat(g5);g6=parseFloat(g6);g7=parseFloat(g7);
			g8=parseFloat(g8);g9=parseFloat(g9);g10=parseFloat(g10);
			gp1=parseFloat(gp1);gp2=parseFloat(gp2);gp3=parseFloat(gp3);gp4=parseFloat(gp4);gp5=parseFloat(gp5);gp6=parseFloat(gp6);gp7=parseFloat(gp7);
			gp8=parseFloat(gp8);gp9=parseFloat(gp9);gp10=parseFloat(gp10);
		
    Highcharts.chart('aligraf', {
    	chart: {type: 'column'},
    	title: {text: 'Alimento Estanque ['+est+']'},
    	xAxis: {categories: [ d1, d2, d3, d4, d5, d6, d7, d8, d9, d10]},
    	yAxis: {
        	min: 0,
        	title: {text: 'KG Totales'},
        	stackLabels: {
            	enabled: true,
            	style: {
                	fontWeight: 'bold',
                	color: 'gray'
            	}
        	}
    	},
    	legend: {
        	align: 'right',	x: -30,	verticalAlign: 'top', y: 15, floating: true, backgroundColor: 'white',
        	borderColor: '#CCC', borderWidth: 1,shadow: false
    	},
    	tooltip: {
        	headerFormat: '<b>{point.x}</b><br/>',
        	pointFormat: '{series.name}: {point.y} kg<br/>Total: {point.stackTotal} kg'
    	},
    	plotOptions: {
        	column: {
            	stacking: 'normal',
            	dataLabels: {
                	enabled: true,
                	color: 'white'
            	}
        	}
    	},
   		series: [{
        	name: 'P.M.',
        	data: [gp1, gp2, gp3, gp4, gp5, gp6, gp7, gp8, gp9, gp10],
    	}, {
        	name: 'A.M.',
        	data: [g1, g2, g3, g4, g5, g6, g7, g8, g9, g10]
    	}  //, {
    		//    name: 'Joe',
    	//    data: [ 3, 4, 4, 2, 5]
    	//}
    	]
	});
       
   	},
    });
	
	 
    $('#idpischa').boxLoader({
            url:'<?php echo base_url()?>index.php/aqpgranjacha/combob',
            equal:{id:'numgra',value:'#numgracha'},
            select:{id:'idpis',val:'val'},
            all:"Sel."
    });
    
    $("#tabla_fac").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjacha/tablafac',  
		filters:['cmbGranjaf'],	
        sort:false,
        onRowClick:function(){
    		$("#idfac").val($(this).data('idfac'));
        	$("#resfac").val($(this).data('resfac'));
        	$("#porbio").val($(this).data('porbio'));
		}, 
		 onSuccess:function(){
    		importe = 0;
    	   	$('#tabla_fac tr').map(function(){	    		
	    		$(this).css('text-align','center');
	    	});	
	    	    	
    	},
    });
    $("#tabla_gral").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjacha/tablagral',  
		filters:['numgrachat','cicchat','cmbMesgral','ha'],	
        sort:false,
        onSuccess:function(){
    		importe = 0;
    	   	$('#tabla_gral tr').map(function(){	    		
	    		$(this).css('text-align','right');
	    	});	
	    	$('#tabla_gral tbody tr').map(function(){
		    	if($(this).data('diam')=='Total:') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    });
		    $('#tabla_gral tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold'); $(this).css('background','lightblue');});
	    	$('#tabla_gral tbody tr td:nth-child(6)').map(function(){ $(this).css('font-weight','bold'); $(this).css('background','lightblue');});
	    	$('#tabla_gral tbody tr td:nth-child(10)').map(function(){ $(this).css('font-weight','bold'); }); 
	    	$('#tabla_gral tbody tr td:nth-child(11)').map(function(){ $(this).css('font-weight','bold'); });   	
    	},
    });
    
    
    
    
});


</script>

