<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 16px;
	/*background: none;*/		
}   
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>
<div style="height:500px">
<div class="container" id="principal"  >	
	<ul class="tabs">
		<strong>
		<li><a href="#todos"><img src="<?php echo base_url();?>assets/images/menu/facturas.png" width="25" height="25" border="0">Facturas</a> </li>
		<li><a href="#conanu"><img src="<?php echo base_url();?>assets/images/menu/facturas2.png" width="25" height="25" border="0">Análisis General</a></li>			
		<li><a href="#consultas"><img src="<?php echo base_url();?>assets/images/menu/consultas.png" width="25" height="25" border="0">Consultas</a></li>
		<li><a href="#conceptos"><img src="<?php echo base_url();?>assets/images/menu/conceptos.png" width="25" height="25" border="0">Conceptos</a></li>
		<li><a href="#orden"><img src="<?php echo base_url();?>assets/images/menu/orden.png" width="25" height="25" border="0">Orden de Compra</a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="todos" class="tab_content" >
			<div id = "accordion" style="margin-top: -10px; margin-left: -10px; width: 958px;" >
					<h3 align="left" style="font-size: 12px;" ><a href="#">Registro</a></h3>
						<div  style="height:348px; text-align: right; font-size: 12px;" >
						<div class="ajaxSorterDiv" id="tabla_fcc" name="tabla_fcc" style="height: 387px; width: 890px" >
						<div class="ajaxpager" style=" font-size: 12px;"> 
							
							Seleccione Ciclo:                    		
							<select name="cmbCiclo" id="cmbCiclo">
								<option value="fc16"> 2016 </option>
                 				<?php if($usuario=="Jesus Benítez"){ ?>
                 				<option value="fc15"> 2015 </option>	
                 				<option value="fc14"> 2014 </option>
                 				<?php }?>
            				</select>
							Consultar:
							<select name="cmbTipo" id="cmbTipo">
								<option value="0"> Selecciona </option>
								<option value="1"> Productos </option>
                 				<option value="2"> Materiales </option>
            				</select>
							Dia de Elaboración: <input size="10%" type="text" name="txtFecDia" id="txtFecDia" class="fecha redondo mUp" style="text-align: center; font-size: 10px" >
							<form method="post" action="<?= base_url()?>index.php/faccompras/pdfrepf/" >
        	            		<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        		                        		                                                      
 							</form>   
                		</div>     
                		<span class="ajaxTable" style="height: 310px; width: 888px" >
                    	<table id="mytablaF" name="mytablaF" class="ajaxSorter" >
                        	<thead title="Presione las columnas para ordenarlas como requiera">                            
                            	<th data-order = "Razon"  >Proveedor</th>                                                        							                                                                                                              
                            	<th data-order = "fec1" >Fecha</th>
                            	<th data-order = "fac" >Factura</th> 
                            	<th data-order = "des" >Concepto</th>  
                            	<th data-order = "can" >Cantidad</th>                            
                            	<th data-order = "uni" >Uni</th>
                            	<th data-order = "pre1" >P.U.</th>
                            	<th data-order = "tot" >Total</th>
                        	</thead>
                        	<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			       
	                    	                                 
            			</div>             	
            		</div>
            	 	 
        			<h3 align="left" style="font-size: 12px"><a href="#">Agregar, Actualizar o Eliminar</a></h3>
		       	 	<div style="height: 150px;">
		       	 		<input size="8%" type="hidden" name="id" id="id"/>
		       			<table style="width: 458px" border="2px"  >
							<thead style="background-color: #DBE5F1" >
								<th colspan="5" height="15px" style="font-size: 20px">Información de Factura
									<input type="hidden" size="3%" name="idf" id="idf" /><input type="hidden" size="3%" name="idp" id="idp" /><input type="hidden" size="3%" name="idc" id="idc" />
								</th>
							</thead>
							<tbody style="background-color: #F7F7F7">
								<tr>
									<th><label style="margin: 2px; font-size: 17px" for="nombre">Proveedor</label></th>
									<th colspan="4" style="font-size: 17px; background-color: white"><input style="border: 0" readonly="true" size="65%"  type="text" name="pro" id="pro" ></th>
								</tr>
								<tr>
									<th><label style="margin: 2px; font-size: 17px" for="nombre">Concepto</label></th>
									<th colspan="4" style="font-size: 17px; background-color: white"><input style="border: 0" readonly="true" size="65%"  type="text" name="con" id="con" ></th>
								</tr>
								
								<tr >
									<th style="font-size: 17px; text-align: center">Tipo</th>
									<th style="font-size: 17px; text-align: center">Fecha</th>
									<th style="font-size: 17px; text-align: center">Factura</th>
									<th style="font-size: 17px; text-align: center">Cantidad</th>
									<th style="font-size: 17px; text-align: center">Precio Unitario</th>																			
								</tr>															
								
    							<tr style="background-color: white;">
									<th style="text-align: center">
										<select name="cmbTip" id="cmbTip">
                 							<option value="1"> Productos </option>
                 							<option value="2"> Materiales </option>
            							</select>
									</th>
									<th style="text-align: center"><input size="12%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>
									<th style="text-align: center"><input size="10%" type="text" name="fac" id="fac" style="text-align: center;"></th>
									<th style="text-align: center"><input size="10%" type="text" name="can" id="can" style="text-align: center;">
										<select name="cmbMed" id="cmbMed">
                 							<option value="pza"> pza </option>
                 							<option value="lts"> lts </option>
                 							<option value="galon"> galon </option>
                 							<option value="kg"> kg </option>
                 							<option value="grs"> grs </option>
                 							<option value="saco"> saco </option>
                 							<option value="cubeta"> cubeta </option>
                 							<option value="cuñete"> cuñete </option>
                 							<option value="frasco"> frasco </option>
                 							<option value="lata"> lata </option>
                 							<option value="porrones"> porrones </option>
            							</select>
									</th>
									<th style="text-align: center">$<input size="10%" type="text" name="pre" id="pre" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>																			
																																			
								</tr>						
    							
								</tbody>
								<tfoot style="background-color: #DBE5F1">
									<th colspan="5" >
										<center>
											<input style="font-size: 14px" type="submit" id="nuevoF" name="nuevoF" value="Nuevo" />
											<input style="font-size: 14px" type="submit" id="aceptarF" name="aceptarF" value="Guardar" />
											<input style="font-size: 14px" type="submit" id="borrarF" name="borrarF" value="Borrar" />
											<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar()" />
										</center>	
									</th>
								</tfoot>
    					</table> 
    					<table>
    							<th>
    								<div id='ver_mas_pro' class='hide' >
    								<table style="width: 350px; margin-top: -5px">
 										<tbody style="background-color: #F7F7F7">
 										<tr>
 											<th  style="text-align: center">
											<div  >                			              
                								<div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" >                
               			     						<span class="ajaxTable" style="margin-left: 0px; height: 100px; ">
                    									<table id="mytablaP" name="mytablaP" class="ajaxSorter">
                        									<thead >                            
                        										<th style="font-size: 10px" data-order = "Razon" >Proveedor</th>                                                                                    
                        									</thead>
                        									<tbody style="font-size: 11px"></tbody>
                    									</table>
                			 						</span> 
            									</div>
                							</div> 							
 											</th>
	 									</tr>
 										</tbody>
 									</table>			
 									</div>
    						</th>
    						<th>
    							<div id='ver_mas_con' class='hide' >
    							<table style="width: 350px; margin-top: -5px">
 										<tbody style="background-color: #F7F7F7">
 										<tr>
 											<th  style="text-align: center">
											<div >                			              
                								<div class="ajaxSorterDiv" id="tabla_con" name="tabla_con" >                
               			     						<span class="ajaxTable" style="margin-left: 0px; height: 100px; ">
                    									<table id="mytablaC" name="mytablaC" class="ajaxSorter">
                        									<thead >                            
                        										<th style="font-size: 10px" data-order = "des" >Conceptos</th>                                                                                    
                        									</thead>
                        									<tbody style="font-size: 11px"></tbody>
                    									</table>
                			 						</span> 
            									</div>
                							</div> 							
 											</th>
	 									</tr>
 										</tbody>
 									</table>
 									</div>
    						</th>
 						</table> 
        			</div>
	 		</div>	
	 	</div>
	 	<div id="conanu" class="tab_content" >
					<h3 align="left" style="font-size: 12px" ><a href="#">Comparativo de adquisiciones actuales en ciclo </a></h3>
					<div style="height: 415px; width: 980px; margin-left: -20px">				
						<div class="ajaxSorterDiv" id="tabla_cgral" name="tabla_cgral" style="margin-top: -21px; ">                
                		<div style=" text-align:left; font-size: 12px;" class='filter'  >
                		<div align="right">	
                			Buscar Descripcion:
                    		<input id="buscar" name="buscar" maxlength="15" type="text" title="Ingrese el texto deseado para buscar"/>
                			Categoria:                    		
							<select name="cmbPM" id="cmbPM" style="font-size: 10px; height: 25px; width: 110px" >
          						<option value="0">Todas</option>
          						<option value="1">Larvicultura</option>
          						<option value="2">Maduración</option>
          						<option value="3">Microalgas</option>
          						<option value="4">Insumos Varios</option>
          					</select>
          					</div>
                		</div>
                		<span class="ajaxTable" style="height: 390px; width: 960px" >
                		<table id="mytablaCG" name="mytablaCG" class="ajaxSorter" border="1" >
                        	<thead>  
                        		<tr>
                        			<th rowspan="2" data-order = "des">Descripción</th> 
                        			<th colspan="3" style="text-align: center">Ciclo Anterior</th>
                        			<th colspan="10" style="text-align: center">Ciclo Actual</th>
                        			
                        			<!--<th colspan="2" style="text-align: center">Proyección Ciclo Actual</th>-->
                        		</tr>
                        	    <th data-order = "anterior">Cantidad</th>
                            	<th data-order = "ppant">Precio</th>
                            	<th data-order = "impant">Importe</th>  
                            	<th data-order = "cantidad">Compras</th>
                            	<th data-order = "eximzt">Exi-Mzt</th>
                            	<th data-order = "consumo">Consumo</th>
                            	<th data-order = "exilab">Exi-Lab</th>
                            	<th data-order = "ppact">Precio</th>
                            	<th data-order = "importe">Importe</th>                                                                                                          
                            	<th data-order = "pca">% <sub>(1)</sub></th>
                            	<th data-order = "inc">Inc. Uni</th>
                            	<th data-order = "incp">Inc. Pre</th>
                            	<th data-order = "incd">Inc. $</th>
                            	
                            	<!--<th data-order = "proyeccion">Cantidad</th>
                            	<th data-order = "pp">% <sub>(2)</sub></th>-->
                            	 
                            </thead>
                        	<tbody style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="margin-top: -10px">        
	                    	<ul style="width: 430px" class="order_list"><sup>(fila roja) incremento en precio - (1) % adquisición actual vs ciclo anterior.</sup><!--<sup>(2) % adquisición actual vs proyección actual.</sup>--></ul>                                            
    	                	<form method="post" action="<?= base_url()?>index.php/faccomprasgral/pdfrep/" >
        	            		<input type="hidden" size="8%" name="catego" id="catego" />                      	             	                                                                                           
                	        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        		                        		                                                      
 							</form>   
                		</div>                                      
            			</div>             	
            		
            	 	</div> 
 	 	</div>
	 	<div id="consultas" class="tab_content"  >	
			<table border="0" width="950px" style="margin-left: -5px; margin-top: -9px">
				<tr>
					<th style="width: 350px">
					<input name="rbSel" type="radio" value="1" onclick="radios(1)" /> Proveedor
					<input name="rbSel" type="radio" value="2" onclick="radios(2)" /> Producto o Material
            		<input type="hidden" readonly="true" size="2%"  type="text" name="idpsc" id="idpsc">
            		<input type="hidden" readonly="true" size="2%"  type="text" name="tip" id="tip">
            		</th>
            		<th>Detalle
            			<input style="border: 1; background-color: lightblue" readonly="true" size="47%"  type="text" name="selc1" id="selc1" >
            		</th>
				</tr>
				<tr>
					<th style="width: 350px">
						<div id='ver_mas_proS' class='hide'>
							<div class="ajaxSorterDiv" id="tabla_pro" name="tabla_pro" style="margin-top: 0px; "  >                
               					<span class="ajaxTable" style=" height: 100px; ">
                					<table id="mytablaPro" name="mytablaPro" class="ajaxSorter" >
                						<thead >                            
                							<th style="font-size: 10px" data-order = "Razon" >Proveedor</th>                                                                                    
                						</thead>
                						<tbody style="font-size: 11px"></tbody>
                    				</table>
                				</span>                 	
            				</div>
            				<input style="border: 1; background-color: lightblue" readonly="true" size="47%"  type="text" name="selc" id="selc" >
            				<div class="ajaxSorterDiv" id="tabla_ps" name="tabla_ps" style="margin-top: -9px;  " >                
               					<span class="ajaxTable" style="margin-left: 0px; height: 280px; ">
                					<table id="mytablaPS" name="mytablaPS" class="ajaxSorter">
                						<thead >                            
                							<th style="font-size: 10px" data-order = "des" >Concepto</th>                                                                                    
                							<th style="font-size: 10px" data-order = "cant" >Cantidad</th>
                							<!--<th style="font-size: 10px" data-order = "uni" >Unidad</th>-->
                							<th style="font-size: 10px" data-order = "pret" >Total</th>
                						</thead>
                						<tbody style="font-size: 11px"></tbody>
                    				</table>
                				</span>                 	
            				</div>
            			</div>
            			<div id='ver_mas_conS' class='hide' >
            				<div class="ajaxSorterDiv" id="tabla_conc" name="tabla_conc" >                
               					<span class="ajaxTable" style="margin-left: 0px; height: 415px; ">
                					<table id="mytablaCon" name="mytablaCon" class="ajaxSorter">
                						<thead >                            
                							<th style="font-size: 10px" data-order = "des" >Conceptos</th>                                                                                    
                						</thead>
                						<tbody style="font-size: 11px"></tbody>
                					</table>
                				</span> 
            				</div>
            			</div>
            		</th>
                 	<th >
                 		<div id='ver_mas_dps' class='hide' >
            			<div class="ajaxSorterDiv" id="tabla_dps" name="tabla_dps"  style="width: 640px;">
							<span class="ajaxTable" style="height: 385px; width: 620px; margin-top: 1px; " >
                    			<table id="mytablaDPS" name="mytablaDPS" class="ajaxSorter"  >
                        			<thead>                            
                            			<th data-order = "des" >Concepto</th>
                            			<th data-order = "fec1" style="width: 65px;">Fecha</th>
                            			<th data-order = "fac" style="width: 40px;">Factura</th> 
                            			<th data-order = "can" style="width: 60px;">Cantidad</th>                            
                            			<!--<th data-order = "uni" style="width: 20px;"></th>-->
                            			<th data-order = "pre" style="width: 35px;">P.U.</th>
                            			<th data-order = "tot" style="width: 70px;">Total</th>
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px;"> 
								<!--<form method="post" action="<?= base_url()?>index.php/faccompras/pdfrep/" ></form>-->		
								<form id="data_tables" action="<?= base_url()?>index.php/faccompras/pdfrep" method="POST">							
    	    	            		<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" id="export_data" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<!--<input type="hidden" name="tabla" value ="" class="htmlTable"/>-->  
        	            	    	<input type="hidden" readonly="true" size="88%"  type="text" name="dess" id="dess" >
        	            	    	<input type="hidden" name="tabladps" id="html_dps"/>
								</form>   
                			</div>       
	                	</div>
	                	<script type="text/javascript">
							$(document).ready(function(){
								$('#export_data').click(function(){									
									$('#html_dps').val($('#mytablaDPS').html());
									$('#data_tables').submit();
									$('#html_dps').val('');
								});
							});
						</script>
	                	</div>
	                	<div id='ver_mas_dms' class='hide' >
            			<div class="ajaxSorterDiv" id="tabla_dms" name="tabla_dms"  style="width: 640px;">
							<span class="ajaxTable" style="height: 385px; width: 630px; margin-top: 1px; " >
                    			<table id="mytablaDMS" name="mytablaDMS" class="ajaxSorter"  >
                        			<thead>                            
                            			<th data-order = "Razon">Proveedor</th>
                            			<th data-order = "fec1">Fecha</th>
                            			<th data-order = "fac" >Factura</th> 
                            			<th data-order = "can" >Cantidad</th>                            
                            			<!--<th data-order = "uni" style="width: 20px;"></th>-->
                            			<th data-order = "pre" >P.U.</th>
                            			<th data-order = "tot" >Total</th>
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px;"> 
								<form id="data_tablesm" action="<?= base_url()?>index.php/faccompras/pdfreps" method="POST">							
    	    	            		<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" id="export_datam" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" readonly="true" size="88%"  type="text" name="desm" id="desm" >
        	            	    	<input type="hidden" name="tabladms" id="html_dms"/>
								</form>    
                			</div>       
	                	</div>
	                	<script type="text/javascript">
							$(document).ready(function(){
								$('#export_datam').click(function(){									
									$('#html_dms').val($('#mytablaDMS').html());
									$('#data_tablesm').submit();
									$('#html_dms').val('');
								});
							});
						</script>
	                	</div>
            		</th>
            	</tr>
			</table>	
		</div>
		<div id="conceptos" class="tab_content"  >	
			<table>
			<th>
			<div class="ajaxSorterDiv" id="tabla_conR" name="tabla_conR" style="width: 300px;" >                
            	<span class="ajaxTable" style="margin-left: 0px; height: 420px;  ">
            		<table id="mytablaCR" name="mytablaCR" class="ajaxSorter">
            			<thead > <th style="font-size: 12px" data-order = "des" >Conceptos</th>
            					<th style="font-size: 12px" data-order = "pm" >Tipo</th>
            			</thead>
                        <tbody style="font-size: 11px"></tbody>
                    </table>
                </span> 
            </div>
            </th>
            <th style="width: 200px"></th>
            <th style="width: 500px">
            	<table style="width: 458px" border="2px"  >
							<thead style="background-color: #DBE5F1" >
								<th colspan="5" height="15px" style="font-size: 20px">
									Registro y Actualización de	Información de Concepto</th>
							</thead>
							<tbody style="background-color: #F7F7F7">
								<tr>
									<th ><label style="margin: 2px; font-size: 17px" for="nombre">Concepto</label></th>
									<th colspan="4" style="font-size: 17px; background-color: white"><input style="border: 0" size="65%"  type="text" name="conR" id="conR" >
										
									</th>
								</tr>
								<tr>
									<th style="text-align: right" colspan="5">Categoria:
										<input type="hidden" size="2%" name="cat" id="cat" readonly="true" >
										<input <?php if($usuario!="Jesus Benítez" && $usuario!="Javier Morán"){ ?> disabled="true" <?php } ?> name="rbCat" type="radio" value="1" onclick="radiosa(2,1)" />Larvicultura
										<input <?php if($usuario!="Jesus Benítez" && $usuario!="Javier Morán"){ ?> disabled="true" <?php } ?> name="rbCat" type="radio" value="2" onclick="radiosa(2,2)" />Maduración
										<input <?php if($usuario!="Jesus Benítez" && $usuario!="Javier Morán"){ ?> disabled="true" <?php } ?> name="rbCat" type="radio" value="3" onclick="radiosa(2,3)" />Microalgas
										<input <?php if($usuario!="Jesus Benítez" && $usuario!="Javier Morán"){ ?> disabled="true" <?php } ?> name="rbCat" type="radio" value="4" onclick="radiosa(2,4)" />Insumos Varios
									</th>
								</tr>
								<tr>	
									<th colspan="5">
									<label style="margin: 2px; " for="nombre">Cantidad Proyectada para Ciclo Actual:</label>
									<input style="border: 1;text-align: right;" size="12%"  type="text" name="conCP" id="conCP" onKeyPress="return(currencyFormat(this,',','.',event,'P'));"> 
									<label style="margin: 2px; " >Existencia Mazatlán:</label>
									<input style="border: 1;text-align: right;" size="12%"  type="text" name="eximzt" id="eximzt" onKeyPress="return(currencyFormat(this,',','.',event,'P'));"><br />
									<?php if($usuario=="Jesus Benítez"){ ?>
									<label style="margin: 2px; " >Consumo:</label>
									<input style="border: 1;text-align: right;" size="12%"  type="text" name="consumo" id="consumo" onKeyPress="return(currencyFormat(this,',','.',event,'P'));">
									<label style="margin: 2px; " >Existencia Laboratorio:</label>
									<input style="border: 1;text-align: right;" size="12%"  type="text" name="exilab" id="exilab" onKeyPress="return(currencyFormat(this,',','.',event,'P'));">
									<?php } ?>
									</th>									
								</tr>
								
								<tr>
									<th colspan="5">
										<label style="margin: 2px; " for="nombre">Adquisiciones Ciclo Anterior</label><br />
										<label style="margin: 2px; " for="nombre">Cantidad:</label>
										<input style="border: 1;text-align: right;" size="12%"  type="text" name="conCA" id="conCA" onKeyPress="return(currencyFormat(this,',','.',event,'P'));">
										Equivalentes a $<input style="border: 1;text-align: right;" size="12%"  type="text" name="conPA" id="conPA" onKeyPress="return(currencyFormat(this,',','.',event,'P'));">
										<input type="hidden"  size="3%" name="idDP" id="idDP" />
										<input <?php if($usuario!="Jesus Benítez" && $usuario!="Javier Morán"){ ?> disabled="true" <?php } ?> name="rbDP" type="radio" value="1" onclick="radiosa(3,1)" />Dólares Americanos
										<input <?php if($usuario!="Jesus Benítez" && $usuario!="Javier Morán"){ ?> disabled="true" <?php } ?> name="rbDP" type="radio" value="2" onclick="radiosa(3,2)" />Pesos Mexicanos
									</th>
								</tr>
								
								
								<tr >
									<th style="text-align: center">Tipo</th>
									<th colspan="2" style="background-color: white">
										<select style="" name="cmbTipR" id="cmbTipR">
                 							<option value="Producto"> Productos </option>
                 							<option value="Material"> Materiales </option>
            							</select>
            							<input type="hidden"  size="3%" name="idcR" id="idcR" />
            							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            							Analizar en Reportes General Anual:<input type="hidden" size="2%" name="ser" id="ser" readonly="true" >
            							<th style="color: red"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Javier Morán"){ ?> disabled="true" <?php } ?> name="rbSer" type="radio" value="2" onclick="radiosa(1,2)" />No</th>
            							<th><input <?php if($usuario!="Jesus Benítez" && $usuario!="Javier Morán"){ ?> disabled="true" <?php } ?> name="rbSer" type="radio" value="1" onclick="radiosa(1,1)" />Si</th>
            						</th>
								</tr>															
								</tbody>
								<tfoot style="background-color: #DBE5F1">
									<th colspan="5" >
										<center>
											<input style="font-size: 14px" type="submit" id="aceptarC" name="aceptarC" value="Guardar" />
											<input style="font-size: 14px" type="submit" id="nuevoC" name="nuevoC" value="Nuevo" />
										</center>	
									</th>
								</tfoot>
    					</table>
            </th>
			</table>	
		</div>
		<div id="orden" class="tab_content"  >	
			<table>
			<th>
			
			<div class="ajaxSorterDiv" id="tabla_ord" name="tabla_ord" style="width: 300px;" >                
            	<span class="ajaxTable" style="margin-left: 0px; height: 420px;  ">
            		<table id="mytablaO" name="mytablaO" class="ajaxSorter">
            			<thead > <th style="font-size: 12px" data-order = "orden" >Orden</th>
            					<th style="font-size: 12px" data-order = "Razon" >Proveedor</th>
            					<th style="font-size: 12px" data-order = "fechao" >Fecha</th>
            			</thead>
                        <tbody style="font-size: 11px"></tbody>
                    </table>
                </span> 
            </div>
            </th>
            <th style="width: 200px"></th>
            <th style="width: 500px">
            	<table style="width: 458px" border="2px"  >
							<thead style="background-color: #DBE5F1" >
								<th colspan="5" height="15px" style="font-size: 20px">
									Registro y Actualización de	Información de Orden 
											<input style="font-size: 14px" type="submit" id="aceptarO" name="aceptarO" value="Guardar" />
											<input style="font-size: 14px" type="submit" id="nuevoO" name="nuevoO" value="Nuevo" />
								</th>
							</thead>
							<tbody style="background-color: #F7F7F7">
								<tr>
									<th ><label style="margin: 2px; font-size: 17px" for="nombre">Orden:</label></th>
									<th style="font-size: 17px; background-color: white"><input style="border: 0" size="5%"  type="text" name="ordend" id="ordend" ></th>
									<th><label style="margin: 2px; font-size: 17px" for="fecha">Fecha</label></th>
									<th colspan="2" ><input size="10%" type="text" name="FecDia" id="FecDia" class="fecha redondo mUp" style="text-align: center;">
										<input type="hidden"  size="3%" name="idpo" id="idpo" />
									</th>	
									
								</tr>
								<tr>
									<th><label style="margin: 2px; font-size: 17px" for="nombre">Proveedor</label></th>
									<th colspan="4" style="font-size: 14px; background-color: white"><input style="border: 1;width: 500px" type="text" name="nrpo" id="nrpo">
									</th>
								</tr>
								<tr >
									<th colspan="5"style="font-size: 17px; text-align: center">Detalle</th>
									
								</tr>
								<tr >
									<th colspan="5"style="font-size: 12px; text-align: center">
										<div id='ver_mas_deto' class='hide' >
										<div class="ajaxSorterDiv" id="tabla_det" name="tabla_det" >                
               			     				<span class="ajaxTable" style="margin-left: 0px; height: 213px; ">
                    							<table id="mytablaD" name="mytablaD" class="ajaxSorter">
                        							<thead >                            
                        								<th style="font-size: 10px" data-order = "canc" >Cantidad</th>                                                                                    
                        								<th style="font-size: 10px" data-order = "nrc" >Descripción</th>
                        								<th style="font-size: 10px" data-order = "obsc" >Observaciones</th>
                        							</thead>
                        							<tbody style="font-size: 11px"></tbody>
                    							</table>
                			 				</span> 
            								<div class="ajaxpager" style="">    
            									<form id="data_tableo" action="<?= base_url()?>index.php/faccompras/pdfrepOrden" method="POST">							
    	    	            						<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" id="export_datao" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    					<input type="hidden" size="8%" name="ordsel" id="ordsel" />
        	            							<input type="hidden" size="12%" type="text" name="fecsel" id="fecsel" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
        	            							<input type="hidden" name="prosel" id="prosel" />    
        	            	    					<input type="hidden" name="tablao" id="html_o"/>
												</form>        
	                    						<!--<form method="post" action="<?= base_url()?>index.php/faccompras/pdfrepOrden/" >
        	            							<input type="hidden" size="8%" name="ordsel" id="ordsel" />
        	            							<input type="hidden" size="12%" type="text" name="fecsel" id="fecsel" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
        	            							<input type="hidden" name="prosel" id="prosel" />                      	             	                                                                                           
                	        						<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    						<input type="hidden" name="tablao" value ="" class="htmlTable"/>                        		                        		                                                      
 												</form>-->   
                							</div>
            							</div>
            							<script type="text/javascript">
											$(document).ready(function(){
												$('#export_datao').click(function(){									
													$('#html_o').val($('#mytablaD').html());
													$('#data_tableo').submit();
													$('#html_o').val('');
												});
											});
										</script>
                						</div> 
                						<div id='ver_mas_proo' class='hide' >
                							<div class="ajaxSorterDiv" id="tabla_proo" name="tabla_proo" >                
               			     					<span class="ajaxTable" style="margin-left: 0px; height: 250px; ">
                    								<table id="mytablaPo" name="mytablaPo" class="ajaxSorter">
                        								<thead >                            
                        									<th style="font-size: 10px" data-order = "Razon" >Proveedor</th>                                                                                    
                        								</thead>
                        								<tbody style="font-size: 11px"></tbody>
                    								</table>
                			 					</span> 
            								</div>
            							</div>
									</th>
								</tr>	
																					
								<tr >
									<th style="font-size: 12px; text-align: center">Cantidad</th>
									<th colspan="2" style="font-size: 12px; text-align: center">Descripción</th>
									<th colspan="2" style="font-size: 12px; text-align: center">Observaciones</th>
								</tr>
								<tr >
									<th style="font-size: 12px; text-align: center"><input style="width: 100px" name="canord" id="canord" /></th>
									<th colspan="2" style="font-size: 12px; text-align: center"><input style="width: 250px" name="desord" id="desord" /></th>
									<th colspan="2" style="font-size: 12px; text-align: center"><input style="width: 250px" name="obsord" id="obsord" /></th>
								</tr>
								</tbody>
								<tfoot style="background-color: #DBE5F1">
									<th colspan="5" >
										<center>
											Actualizar Detalles de Orden
											<input style="font-size: 14px" type="submit" id="aceptarD" name="aceptarD" value="Guardar" />
											<input style="font-size: 14px" type="submit" id="nuevoD" name="nuevoD" value="Nuevo" />
											<input style="font-size: 14px" type="submit" id="borrarD" name="borrarD" value="Borrar" />
											<input type="hidden" size="3%" name="idod" id="idod" />
										</center>	
									</th>
								</tfoot>
    					</table>
            </th>
			</table>	
		</div>		
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
function radiosa(radio,dato){
	if(radio==1) { $("#ser").val(dato);}	
	if(radio==2) { $("#cat").val(dato);}
	if(radio==3) { $("#idDP").val(dato);}
}
function radios(radio){
	if(radio==1) { $(this).removeClass('used');
   					$('#ver_mas_proS').show();
   					$('#ver_mas_dps').show();
   					$('#ver_mas_conS').hide();
   					$('#ver_mas_dms').hide();
   					$("#selc").val('');$("#selc1").val('');
   					$("#idpsc").val('');
   					$("#tip").val('p');   
   					$("#mytablaPS").trigger("update");
   					$("#mytablaDPS").trigger("update");
   					$("#mytablaDMS").trigger("update");
					return true;}
	if(radio==2) { $(this).removeClass('used');
   					$('#ver_mas_proS').hide();
   					$('#ver_mas_dps').hide();
   					$('#ver_mas_conS').show();
   					$('#ver_mas_dms').show();
   					$("#selc").val('');$("#selc1").val('');
   					$("#idpsc").val('');
   					$("#tip").val('c');
   					$("#mytablaPS").trigger("update");
   					$("#mytablaDPS").trigger("update");   
   					$("#mytablaDMS").trigger("update");
					return true;}	
}
$("#nuevoO").click( function(){	
	$("#ordend").val('');	
	$("#nrpo").val('');
	$("#idpo").val('');
	var f = new Date();
	$("#FecDia").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#tabla_det").ajaxSorter({
		url:'<?php echo base_url()?>index.php/faccompras/tabladetalle',  
   		filters:['ordend'],
   		sort:false,
	});	
	return true;
});
$("#nuevoC").click( function(){	
	$("#conR").val('');	$("#cmbTipR").val('');$("#ser").val('');$("#cat").val('');$("#conCP").val('');$("#conCA").val('');
	$("#conPA").val('');$("#idDP").val('');$("#eximzt").val('');$("#exilab").val('');$("#consumo").val('');
	$('input:radio[name=rbSer]:nth(0)').attr('checked',false);$('input:radio[name=rbSer]:nth(1)').attr('checked',false);
	$('input:radio[name=rbDP]:nth(0)').attr('checked',false);$('input:radio[name=rbDP]:nth(1)').attr('checked',false);
	$('input:radio[name=rbCat]:nth(0)').attr('checked',false);$('input:radio[name=rbCat]:nth(1)').attr('checked',false);
	$('input:radio[name=rbCat]:nth(2)').attr('checked',false);$('input:radio[name=rbCat]:nth(3)').attr('checked',false);
	$("#idcR").val('');
	return true;
});
$("#nuevoD").click( function(){	
	$("#canord").val('');	
	$("#desord").val('');
	$("#obsord").val('');
	$("#idod").val('');
	return true;
});
$("#nuevoF").click( function(){	
	$("#pro").val('');
    $("#con").val('');
    $("#cmbTip").val('');
    $("#txtFecha").val('');
    $("#fac").val('');
    $("#can").val('');
    $("#pre").val('');
    $("#cmbMed").val('');
    $("#idp").val('');       	
    $("#idc").val('');			
    $("#idf").val('');
	return true;
});
$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#txtFecDia").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#fecsel").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#FecDia").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#fecsel").val( selectedDate );	
	}	
});


jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});
function cerrar(){
		$("#accordion").accordion( "activate",0 );
	
}
$("#borrarD").click( function(){	
	numero=$("#idod").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/faccompras/borrarD", 
						data: "id="+$("#idod").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Detalle de Orden Eliminado");
										$("#mytablaD").trigger("update");
										$("#nuevoD").click();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Detalle para poder Eliminarlo");
		return false;
	}
});
$("#borrarF").click( function(){	
	numero=$("#idf").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/faccompras/borrarF", 
						data: "id="+$("#idf").val()+"&ciclo="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Factura Eliminada");
										$("#mytablaF").trigger("update");
										$("#nuevoF").click();
										$("#accordion").accordion( "activate",0 );
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Factura para poder Eliminarla");
		return false;
	}
});
$("#aceptarD").click( function(){	
	numero=$("#idod").val();
	orden=$("#ordend").val();
	if(orden!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/faccompras/actualizarD", 
						data: "id="+$("#idod").val()+"&can="+$("#canord").val()+"&des="+$("#desord").val()+"&obs="+$("#obsord").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaD").trigger("update");
										$("#nuevoD").click();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/faccompras/agregarD", 
						data: "can="+$("#canord").val()+"&des="+$("#desord").val()+"&obs="+$("#obsord").val()+"&nro="+$("#ordend").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos agregados correctamente");										
										$("#mytablaD").trigger("update");
										$("#nuevoD").click();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});				
		}		
	}else{
		alert("Error: Necesita Registrar primeramente Orden ");
		$("#ordend").focus();	
		return false;
	}	
});
$("#aceptarO").click( function(){	
	numero=$("#ordend").val();
	pro=$("#idpo").val();
	if(pro!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/faccompras/actualizarO", 
						data: "id="+$("#ordend").val()+"&pro="+$("#idpo").val()+"&fec="+$("#FecDia").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaO").trigger("update");
										$("#nuevoO").click();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/faccompras/agregarO", 
						data: "pro="+$("#idpo").val()+"&fec="+$("#FecDia").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos agregados correctamente");										
										$("#mytablaO").trigger("update");
										$("#nuevoO").click();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada55");
									}					
								}		
				});				
		}		
	}else{
		alert("Error: Necesita Registrar Proveedor");
		$("#nrpo").focus();	
		return false;
	}	
});

$("#aceptarC").click( function(){	
	numero=$("#idcR").val();
	con=$("#conR").val();
	if(con!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/faccompras/actualizarR", 
						data: "id="+$("#idcR").val()+"&con="+$("#conR").val()+"&tip="+$("#cmbTipR").val()+"&ana="+$("#ser").val()+"&cat="+$("#cat").val()+"&pro="+$("#conCP").val()+"&ant="+$("#conCA").val()+"&impant="+$("#conPA").val()+"&dp="+$("#idDP").val()+"&eximzt="+$("#eximzt").val()+"&exilab="+$("#exilab").val()+"&consumo="+$("#consumo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");										
										$("#mytablaCR").trigger("update");
										$("#mytablaC").trigger("update");
										$("#mytablaCG").trigger("update");
										$("#nuevoC").click();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/faccompras/agregarR", 
						data: "con="+$("#conR").val()+"&tip="+$("#cmbTipR").val()+"&ana="+$("#ser").val()+"&cat="+$("#cat").val()+"&pro="+$("#conCP").val()+"&ant="+$("#conCA").val()+"&impant="+$("#conPA").val()+"&dp="+$("#idDP").val()+"&eximzt="+$("#eximzt").val()+"&exilab="+$("#exilab").val()+"&consumo="+$("#consumo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos agregados correctamente");										
										$("#mytablaCR").trigger("update");
										$("#mytablaC").trigger("update");
										$("#mytablaCG").trigger("update");
										$("#nuevoC").click();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});				
		}		
	}else{
		alert("Error: Necesita Registrar Concepto para Agregarlo o seleccionarlo para poder actualizarlo");
		$("#conR").focus();	
		return false;
	}	
});
$("#aceptarF").click( function(){	
	numero=$("#idf").val();	con=$("#idc").val();pro=$("#idp").val();
	if(pro!=''){
 	 if(con!=''){	
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/faccompras/actualizarF", 
						data: "id="+$("#idf").val()+"&con="+$("#idc").val()+"&pro="+$("#idp").val()+"&tip="+$("#cmbTip").val()+"&fac="+$("#fac").val()+"&can="+$("#can").val()+"&pre="+$("#pre").val()+"&med="+$("#cmbMed").val()+"&fec="+$("#txtFecha").val()+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaF").trigger("update");
										$("#mytablaCG").trigger("update");
										$("#nuevoF").click();
										$("#accordion").accordion( "activate",0 );
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/faccompras/agregarF", 
						data: "con="+$("#idc").val()+"&pro="+$("#idp").val()+"&tip="+$("#cmbTip").val()+"&fac="+$("#fac").val()+"&can="+$("#can").val()+"&pre="+$("#pre").val()+"&med="+$("#cmbMed").val()+"&fec="+$("#txtFecha").val()+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos agregados correctamente");										
										$("#mytablaF").trigger("update");
										$("#mytablaCG").trigger("update");
										$("#nuevoF").click();
										$("#accordion").accordion( "activate",0 );
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});				
		}		
	}else{
		alert("Error: Necesita Registrar Concepto para Agregarlo o seleccionarlo para poder actualizarlo");
		$("#con").focus();	
		return false;
	}
	}else{
		alert("Error: Necesita Registrar Proveedor para Agregarlo o seleccionarlo para poder actualizarlo");
		$("#pro").focus();	
		return false;
	}	
});
$("#pro").click( function(){	
	$(this).removeClass('used');
   	$('#ver_mas_pro').show();
   	$('#ver_mas_con').hide();
	return true;
});

$("#nrpo").click( function(){	
	$(this).removeClass('used');
   	$('#ver_mas_deto').hide();
   	$('#ver_mas_proo').show();
	return true;
});
$("#con").click( function(){	
	$(this).removeClass('used');
	$('#ver_mas_con').show();
   	$('#ver_mas_pro').hide();
	return true;
});
$("#cmbPM").change( function(){		
	$("#catego").val($("#cmbPM").val());	
 return true;
});
$(document).ready(function(){
	$(this).removeClass('used');
	$('#ver_mas_proS').hide();
	$('#ver_mas_dps').hide();
	$('#ver_mas_dms').hide();
	$('#ver_mas_conS').hide();
   	$('#ver_mas_pro').hide();
   	$('#ver_mas_con').hide();
    $('#ver_mas_proo').hide();
	var f = new Date();
	//$("#txtFecDia").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#FecDia").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/faccompras/tablaclientes',  
		multipleFilter:true, 
   		sort:false,	
   		onRowClick:function(){
   			$("#pro").val($(this).data('razon'));
   			$("#idp").val($(this).data('numero'));
   			$('#ver_mas_pro').hide();
   		},	
   	});
   	$("#tabla_pro").ajaxSorter({
		url:'<?php echo base_url()?>index.php/faccompras/tablaproves',  
		filters:['cmbCiclo'],
		multipleFilter:true, 
   		sort:false,	
   		onRowClick:function(){
   			$("#selc").val($(this).data('razon'));$("#selc1").val($(this).data('razon'));$("#dess").val($(this).data('razon'));
   			$("#idpsc").val($(this).data('numero'));
   			$("#tip").val('p'); 
   			$("#tabla_ps").ajaxSorter({
				url:'<?php echo base_url()?>index.php/faccompras/tablaps',  
				filters:['cmbCiclo','idpsc'],
				sort:false,
				onRowClick:function(){
   					
   				},
				onSuccess:function(){
    				$('#tabla_ps tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
	    			$('#tabla_ps tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })	    	
    			},  	
   			});  	
   			$("#tabla_dps").ajaxSorter({
				url:'<?php echo base_url()?>index.php/faccompras/tabladps',  
				filters:['cmbCiclo','idpsc'],
				sort:false,
				onSuccess:function(){
    					$('#tabla_dps tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
			   			$('#tabla_dps tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })	    	
			   			$('#tabla_dps tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
    			},  	
   			});		
   		},	
   	});
   	$("#tabla_proo").ajaxSorter({
		url:'<?php echo base_url()?>index.php/faccompras/tablaclientes',  
		multipleFilter:true, 
   		sort:false,	
   		onRowClick:function(){
   			$("#nrpo").val($(this).data('razon'));$("#prosel").val($(this).data('razon'));
   			$("#idpo").val($(this).data('numero'));
   			$('#ver_mas_proo').hide();
   			$('#ver_mas_deto').show();
   		},	
   	});
   	$("#tabla_con").ajaxSorter({
		url:'<?php echo base_url()?>index.php/faccompras/tablaconceptos',  
		multipleFilter:true, 
   		sort:false,	
   		onRowClick:function(){
   			$("#con").val($(this).data('des'));
   			$("#idc").val($(this).data('nrc'));//mp=$(this).data('pm');
   			$("#cmbTip").val('2');
   			if($(this).data('pm')!="Material") $("#cmbTip").val('1');
   			$('#ver_mas_con').hide();
   			
   		},	
   	});	
   	$("#tabla_conc").ajaxSorter({
		url:'<?php echo base_url()?>index.php/faccompras/tablaconceps',  
		filters:['cmbCiclo'],
		multipleFilter:true, 
   		sort:false,	
   		onRowClick:function(){
   			$("#selc1").val($(this).data('des'));$("#desm").val($(this).data('des'));
   			$("#idpsc").val($(this).data('nrc'));
   			//pm=$(this).data('pm')
   			//if($(this).data('pm')=="Material") $("#cmbTip").val(1); else $("#cmbTip").val(2); 
   			$("#tip").val('c');
   			$("#tabla_dms").ajaxSorter({
				url:'<?php echo base_url()?>index.php/faccompras/tabladms',  
				filters:['cmbCiclo','idpsc'],
				sort:false,
				onSuccess:function(){
    					$('#tabla_dms tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
			   			$('#tabla_dms tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })	    	
			   			$('#tabla_dms tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
    			},  	
   			});	
   		},	
   	});	
   	$("#tabla_ord").ajaxSorter({
		url:'<?php echo base_url()?>index.php/faccompras/tablaorden',  
		multipleFilter:true, 
   		sort:false,	
   		onRowClick:function(){
   			$("#ordend").val($(this).data('orden'));$("#ordsel").val($(this).data('orden'));
   			$("#FecDia").val($(this).data('fechao'));$("#fecsel").val($(this).data('fechao'));
   			$("#nrpo").val($(this).data('razon'));$("#prosel").val($(this).data('razon'));
   			$("#idpo").val($(this).data('nrp'));
   			$('#ver_mas_proo').hide();
   			$('#ver_mas_deto').show();
   			$("#tabla_det").ajaxSorter({
				url:'<?php echo base_url()?>index.php/faccompras/tabladetalle',  
		   		filters:['ordend'],
		   		sort:false,
		   		onRowClick:function(){
   					$('#ver_mas_proo').hide();
   					$('#ver_mas_deto').show();
   					$("#canord").val($(this).data('canc'));
   					$("#desord").val($(this).data('nrc'));
   					$("#obsord").val($(this).data('obsc'));
   					$("#idod").val($(this).data('nrd'));
   					
   				},	
   			});
   		},	
   	});	 
   	$("#tabla_conR").ajaxSorter({
		url:'<?php echo base_url()?>index.php/faccompras/tablaconceptos',  
		multipleFilter:true, 
   		sort:false,	
   		active:['analizar','2'],
   		onRowClick:function(){
   			$("#conR").val($(this).data('des'));
   			$("#idcR").val($(this).data('nrc'));   			
   			$("#cmbTipR").val($(this).data('pm'));
   			$("#conCA").val($(this).data('anterior'));
   			$("#conCP").val($(this).data('proyeccion'));
   			$("#conPA").val($(this).data('impant'));
   			$("#ser").val($(this).data('analizar'));
   			$("#cat").val($(this).data('categoria'));
   			$("#idDP").val($(this).data('dp'));
   			$("#eximzt").val($(this).data('eximzt'));
   			$("#exilab").val($(this).data('exilab'));
   			$("#consumo").val($(this).data('consumo'));
   			ser=$(this).data('analizar');
			if(ser==2 || ser==1 ){
				if(ser==2){ $('input:radio[name=rbSer]:nth(0)').attr('checked',true); }
				if(ser==1){ $('input:radio[name=rbSer]:nth(1)').attr('checked',true); }
			}else{
				$('input:radio[name=rbSer]:nth(0)').attr('checked',false);$('input:radio[name=rbSer]:nth(1)').attr('checked',false); 
			}
			dp=$(this).data('dp');
			if(dp==1 || dp==2 ){
				if(dp==1){ $('input:radio[name=rbDP]:nth(0)').attr('checked',true); }
				if(dp==2){ $('input:radio[name=rbDP]:nth(1)').attr('checked',true); }
			}else{
				$('input:radio[name=rbDP]:nth(0)').attr('checked',false);$('input:radio[name=rbDP]:nth(1)').attr('checked',false); 
			}
			cat=$(this).data('categoria');
			if(cat>=1 || cat<=4 ){
				if(cat==1){ $('input:radio[name=rbCat]:nth(0)').attr('checked',true); }
				if(cat==2){ $('input:radio[name=rbCat]:nth(1)').attr('checked',true); }
				if(cat==3){ $('input:radio[name=rbCat]:nth(2)').attr('checked',true); }
				if(cat==4){ $('input:radio[name=rbCat]:nth(3)').attr('checked',true); }
			}else{
				$('input:radio[name=rbCat]:nth(0)').attr('checked',false);$('input:radio[name=rbCat]:nth(1)').attr('checked',false); 
				$('input:radio[name=rbCat]:nth(2)').attr('checked',false);$('input:radio[name=rbCat]:nth(3)').attr('checked',false);
			}
   		},	
   	});
	$("#tabla_fcc").ajaxSorter({
		url:'<?php echo base_url()?>index.php/faccompras/tabla',  	
		//filters:['cmbCiclo','cmbTipo'],
		filters:['cmbCiclo','cmbTipo','txtFecDia'],
		multipleFilter:true,		
    	sort:false,
    	onRowClick:function(){
    		if($(this).data('nrf')>'0'){
        		$("#accordion").accordion( "activate",1 );
        		$("#pro").val($(this).data('razon'));
        		$("#con").val($(this).data('des'));
        		$("#cmbTip").val($(this).data('tipo'));
       			$("#txtFecha").val($(this).data('fec'));
       			$("#fac").val($(this).data('fac'));
       			$("#can").val($(this).data('can'));
       			$("#pre").val($(this).data('pre'));
       			$("#cmbMed").val($(this).data('uni'));
       			$("#idp").val($(this).data('prov'));       	
       			$("#idc").val($(this).data('con'));			
       			$("#idf").val($(this).data('nrf'));
			}		
    	}, 
    	onSuccess:function(){
    		$('#tabla_fcc tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_fcc tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_fcc tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right'); })	    	
    	},     
	});
	$("#catego").val($("#cmbPM").val());	
	$("#tabla_cgral").ajaxSorter({
	url:'<?php echo base_url()?>index.php/faccompras/tablacgral',  	
	filters:['cmbCiclo','cmbPM','buscar'],	
	active:['incp1', 0.09,'>='],
    sort:false,
    onSuccess:function(){
    		/*$('#tabla_cgral tbody tr').map(function(){
	    		if($(this).data('incp1')>=0.09)
	    			//$('#tabla_cgral tbody tr td:nth-child(7)').map(function(){ $(this).css('color','red'); })	 
	    			//$(this).css('background','lightblue');$(this).css('font-weight','bold');   		
	    	});*/
	    	$('#tabla_cgral tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(10)').map(function(){ $(this).css('text-align','right'); $(this).css('font-weight','bold');})
	    	$('#tabla_cgral tbody tr td:nth-child(11)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(12)').map(function(){ $(this).css('text-align','right'); })
	},     
	});
});
</script>
