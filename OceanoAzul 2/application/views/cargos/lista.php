<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/cargo.png" width="25" height="25" border="0"> Cargos </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion">
			<h3 align="left"><a href="">Depósitos solicitados para transportación de producto</a></h3>
        	<div   style="height:348px; text-align: right; font-size: 12px;">                  
        	<div class="ajaxSorterDiv" id="tabla_car" name="tabla_car" style=" margin-top: -12px; height: 400px; "  align="left"> 
        		  <?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino" or $usuario=="Zuleima Benitez"){ ?>
                  	Ciclo:                 
                    	<select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 22px;" >
                    		<option value="solicitud">2018</option>
                    		<?php $ciclof=2010; $actual=date("Y")-1; //$actual+=1; se suma uno si hay entregas en diciembre
								while($actual >= $ciclof){?>
									<option value="<?php echo 'solicitud_'.($actual-2000);?>" > <?php echo $actual;?> </option>
            					<?php  $actual-=1; } ?>
          				</select>      
                        
                   <?php } else {?>
                        <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 22px;" >
                    		<option value="solicitud">2022</option>
                    	</select> 	
                    <?php } ?>
        		  
        		   Procesar del Día <input size="12%" type="text" name="txtFI" id="txtFI" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                	&nbsp; al <input size="12%" type="text" name="txtFF" id="txtFF"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >           	 
                	Mostrar:
                	<select name="asigna" id="asigna" style="font-size: 10px; height: 22px;" >
                    		<option value="0">Todos</option>
                    		<option value="1">Por Asignar</option>
                    </select> 
                	<!--<button id="filtrar" onclick="">Procesar</button> 
                    <script>
                    	$('#filtrar').click(function(){
                    		$('#tabla_car').trigger('update')
                    	});
                    </script>-->
                    <!--
            		<select name="tecnico" id="tecnico" style="font-size: 11px;height: 25px;   margin-left:4px;">
          						<?php	
          							$this->load->model('cargos_model');
									$data['result']=$this->cargos_model->verTecnico(); ?>
									<option value="" ></option>
									<?php foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumBio;?>" ><?php echo $row->NomBio;?></option>
           							<?php endforeach;?>
   						</select>   
   						<select name="destino" id="destino"  style="font-size: 11px;height: 25px;   margin-left:4px;">
								<option value=""></option>
								<option value="Improvistos">Improvistos</option>
								<option value="Complemento Gasto" >Complemento Gasto</option>
           						<?php	              							
           							$this->load->model('cargos_model');
									$data['result']=$this->cargos_model->verDestino();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NomDes;?>" ><?php echo $row->NomDes;?></option>
           							<?php endforeach;?>
   						</select> 
   						<select name="unidad" id="unidad" style="font-size: 11px;height: 25px;  margin-left:4px;">
          						<?php	
           							$this->load->model('cargos_model');
									$data['result']=$this->cargos_model->verUnidad();?>
									<option value="" ></option>
									<?php
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumUni;?>" ><?php echo $row->NomUni;?></option>
           							<?php endforeach;?>
   						</select>  
   					-->      
   				
                <span class="ajaxTable" style=" height: 310px; width: 882px;">
                    <table id="mytablaC" name="mytablaC" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px"> 
                        	<th data-order = "FechaS1" style="width: 55px">Fecha</th>
                            <th data-order = "NomBio" style="width: 100px">Encargado</th>                                                        
                            <th data-order = "NomDesS" style="width: 100px">Destino</th>                            
                            <th data-order = "NomUni" style="width: 100px">Unidad</th>
                            <?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino" or  $usuario=="Zuleima Benitez"){ ?>
                            <!--<th data-order = "Viatico">Viático</th>-->
                            <?php }?>
                            <th data-order = "Ali1" style="width: 50px; font-size: 10px">Alimentos</th>
                            <th data-order = "Com1" style="width: 50px; font-size: 10px">Combustible</th>
                            <th data-order = "Cas1" style="width: 50px; font-size: 10px">Casetas</th>
                            <th data-order = "Hos1" style="width: 50px; font-size: 10px">Hospedaje</th>
                            <th data-order = "Fit1" style="width: 50px; font-size: 10px">Fitosanitaria</th>
                            <th data-order = "Cargo" style="width: 70px" >Total</th>                                                       
                             
                        </thead>
                        <?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino" or  $usuario=="Zuleima Benitez"){ ?>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 12px">
                        
                        <?php } else {?>
                        <tbody title="Seleccione para analizar" style="font-size: 12px">	
                        <?php }?>
                        </tbody>
                    </table>
                    
                </span> 
             	<div class="ajaxpager" style="margin-top: -10px" >        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/cargos/pdfrep" >
                    	<img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                        <input type="text" class="pagedisplay" size="3" /> /
                        <input type="text" class="pagedisplayMax" size="3" readonly="1" disabled="disabled"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                        <select class="pagesize" style="font-size: 10px; height: 22px">
                                <option selected="selected" value="10">10</option>
                                <option value="20">20</option>                            
                                <option value="0">Gral</option>                                          
                        </select>                                                
                        <img style="cursor: pointer" style="margin-top: 6px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                                                                          
 					</form>   
 					
                </div>  
                                                 
            </div>             
        </div>
        <?php if($usuario=="Lety Lizárraga"){ ?>
        <h3 align="left"><a href="" >Analizar Solicitud</a></h3>
        <?php }?>
        <?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino" or  $usuario=="Zuleima Benitez"){ ?>
        <h3 align="left"><a href="" >Agregar, Actualizar o Eliminar</a></h3>
       
        <?php }?>	
       	<div style="height:150px; ">  
       		 <div class="ajaxSorterDiv" id="tabla_cargo" name="tabla_cargo" align="left" style=" height: 93px; margin-top: -15px; " >             	
            	Destino <select name="cmbDestino" id="cmbDestino"  style="font-size: 11px;height: 25px;   margin-left:4px;">
					<option value="Todos">Todos</option>
					<!--<option value="Improvistos">Improvistos</option>
					<option value="Complemento Gasto" >Complemento Gasto</option>-->
           					<?php	              							
           						$this->load->model('cargos_model');
								$data['result']=$this->cargos_model->verDestino();
								foreach ($data['result'] as $row):?>
           							<option value="<?php echo $row->NomDes;?>" ><?php echo $row->NomDes;?></option>
           						<?php endforeach;?>
   				</select>
       			Unidad <select id="cmbUnidad" style="font-size: 11px;height: 25px;"></select>        
            <span class="ajaxTable" style="width: 880px">
            <table id="mytablaCA" name="mytablaCA" class="ajaxSorter" >
                <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px"> 
                   	<th data-order = "Alimentacion" style="width: 50px; font-size: 10px">Alimentos</th>
                    <th data-order = "Combustible" style="width: 50px; font-size: 10px">Combustible</th>
                    <th data-order = "Casetas" style="width: 50px; font-size: 10px">Casetas</th>
                    <th data-order = "Hospedaje" style="width: 50px; font-size: 10px">Hospedaje</th>
                    <th data-order = "Fitosanitaria" style="width: 50px; font-size: 10px">Fitosanitaria</th>
                    <th data-order = "totals" style="width: 70px" >Total</th>                                                           
                </thead>
                <tbody title="Seleccione para cargar datos" style="font-size: 10px">	
                </tbody>
            </table>
            </span>                                    
            </div>
        	<table style="width: 750px;" border="2px" >
				<thead style="background-color: #DBE5F1">
					<th colspan="5" height="15px" style="font-size: 20px">Datos del Recurso
					<input size="8%" type="hidden"  name="id" id="id"/>													
					</th>
				</thead>	
				<tbody style="background-color: #F7F7F7">							
				<tr>
					<th><label style="margin: 2px; font-size: 14px" for="txtFecha">Fecha</label></th>
					<th><label style="margin: 2px; font-size: 14px" for="enc">Encargado</label></th>
					<th><label style="margin: 2px; font-size: 14px;" for="des" >Destino</label></th>										
					<th><label style="margin: 2px; font-size: 14px" for="uni">Unidad</label></th>																										
					<th><label style="margin: 2px; font-size: 14px;" for="rem">Remision</label></th>										
				</tr>
    			<tr style="font-size: 14px; background-color: white;">										
					<th><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="13%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>								
					<th>
						<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="enc" id="enc" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<option value="0">Seleccione Encargado</option>
          						<?php	
          							$this->load->model('cargos_model');
									$data['result']=$this->cargos_model->verTecnico();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumBio;?>" ><?php echo $row->NomBio;?></option>
           							<?php endforeach;?>
   						</select>
					</th>
					<th>
						<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="des" id="des"  style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
								<option value="Improvistos">Improvistos</option>
								<option value="Complemento Gasto" >Complemento Gasto</option>
								<option value="Saldo Ciclo Anterior" >Saldo Ciclo Anterior</option>
           						<?php	              							
           							$this->load->model('cargos_model');
									$data['result']=$this->cargos_model->verDestino();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NomDes;?>" ><?php echo $row->NomDes;?></option>
           							<?php endforeach;?>
   						</select>
					</th>																		
					<th>
						<?php //if($des!="Improvistos" and $des!="Complemento Gasto"){ ?>
						<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="uni" id="uni" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
           							$this->load->model('cargos_model');
									$data['result']=$this->cargos_model->verUnidad();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumUni;?>" ><?php echo $row->NomUni;?></option>
           							<?php endforeach;?>
   						</select>
						<?php // } ?>
					</th>
					<th>
						<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="rem" id="rem"  style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
           						<?php	
           							$this->load->model('cargos_model');
									$data['result']=$this->cargos_model->verRemision();
									?>
									<option value="0" ><?php echo "SN";?></option>
									<?php
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumRegR;?>" ><?php if($row->NumRegR==0) { echo "SN";} else { echo $row->RemisionR;}?></option>
           							<?php endforeach;?>
    					</select>
					</th>
				</tr>
				</tbody>
			</table>
			<table style="width: 750px; margin-top: -15px" border="2px">
    			<thead style="background-color: #DBE5F1">
					<th colspan="6">Conceptos</th>
				</thead>
				<tbody  style="background-color: #F7F7F7">
				<tr>
					<th><label style="margin: 2px; font-size: 14px" for="ali">Alimentación</label></th>
					<th><label style="margin: 2px; font-size: 14px" for="com">Combustible</label></th>
					<th><label style="margin: 2px; font-size: 14px;" for="cas" >Casetas</label></th>										
					<th><label style="margin: 2px; font-size: 14px" for="hos">Hospedaje</label></th>																										
					<th><label style="margin: 2px; font-size: 14px;" for="fit">Fitosanitaria</label></th>										
					<th><label style="margin: 2px; font-size: 14px;" for="sub">Subtotal</label></th>
				</tr>
    			<tr style="background-color: white"> 
    				<form>   								
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="14%" type="text" name="ali" id="ali" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="14%" type="text" name="com" id="com" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="14%" type="text" name="cas" id="cas" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="14%" type="text" name="hos" id="hos" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="14%" type="text" name="fit" id="fit" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>									
						<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="14%" readonly="true" type="text" name="sub" id="sub" style="text-align: right;"></th>
					</form>									
				</tr> 
				</tbody>   							
    		</table>
    		<table style="width: 750px; margin-top: -15px" border="2px">
    			<thead style="background-color: #DBE5F1">
    				<th colspan="6">Otros Conceptos</th>
				</thead>
				<tbody style="background-color: #F7F7F7">
				<tr>
					<th><label style="margin: 2px; font-size: 14px" for="ali">Reposición Improvistos</label></th>
					<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="16%" type="text" name="rep" id="rep" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
					<th><label style="margin: 2px; font-size: 14px" for="com">Complemento Gasto</label></th>
					<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="16%" type="text" name="cg" id="cg" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
					<th><label style="margin: 2px; font-size: 14px;" for="cas" >TOTAL</label></th>
					<th style="font-size: 13px;">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" &&  $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="16%" type="text" name="tot" id="tot" value="" style="text-align: right;"></th>																			
				</tr>
				</tbody>
				<tfoot style="background-color: #DBE5F1">
					<th colspan="6">
					<center>
							<?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino" or $usuario=="Zuleima Benitez"){ ?>
								Aplicar a:
								<select name="rblugar" id="rblugar" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
      								<option value="1"> Corte 1 </option>
      								<option value="2"> Corte 2 </option>
      								<option value="3"> Corte 3 </option>      								
    							</select>
								<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
								<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
								<input style="font-size: 14px" type="submit" id="borrar" name="borrar" value="Borrar" />
							<?php }?>										
						</center>
					</th>	
				</tfoot>
    		</table>
        </div>	
	 </div> 	
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
	


$("#nuevo").click( function(){	
	$("#id").val('');
	$("#txtFecha").val('');
    $("#rem").val('0');    	
	$("#ali").val('');
	$("#com").val('');
	$("#cas").val('');
	$("#hos").val('');
	$("#fit").val('');
	$("#enc").val('');
	$("#des").val('');
	$("#uni").val('');
	$("#sub").val('');
	$("#rep").val('');
	$("#cg").val('');
	$("#tot").val('');		
	$("#rblugar").val('');
 return true;
})
$("#ali").change( function(){	
	//ali=$("#ali").val();
	//$("#sub").val(($("#ali").val()+$("#com").val()));		
 return true;
})

$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/cargos/borrar", 
						data: "id="+$("#id").val(), //+"&usd="+$("#usd").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Cargo Eliminado correctamente");
										$("#nuevo").click();
										$("#mytablaC").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Cargo para poder Eliminarlo");
		$("#accordion").accordion( "activate",0 );
		return false;
	}
	
	 // return false;
});


function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#aceptar").click( function(){		
	fec=$("#txtFecha").val();
	numero=$("#id").val();
	if( fec!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/cargos/actualizar", 
						data: "id="+$("#id").val()+"&fec="+$("#txtFecha").val()+"&rem="+$("#rem").val()+"&enc="+$("#enc").val()+"&des="+$("#des").val()+"&uni="+$("#uni").val()+"&ali="+$("#ali").val()+"&com="+$("#com").val()+"&cas="+$("#cas").val()+"&hos="+$("#hos").val()+"&fit="+$("#fit").val()+"&tot="+$("#tot").val()+"&rep="+$("#rep").val()+"&cg="+$("#cg").val()+"&corte="+$("#rblugar").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevo").click();
										$("#mytablaC").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/cargos/agregar", 
						data: "&fec="+$("#txtFecha").val()+"&rem="+$("#rem").val()+"&enc="+$("#enc").val()+"&des="+$("#des").val()+"&uni="+$("#uni").val()+"&ali="+$("#ali").val()+"&com="+$("#com").val()+"&cas="+$("#cas").val()+"&hos="+$("#hos").val()+"&fit="+$("#fit").val()+"&tot="+$("#tot").val()+"&rep="+$("#rep").val()+"&cg="+$("#cg").val()+"&corte="+$("#rblugar").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Cargo registrado correctamente");
										$("#nuevo").click();
										$("#mytablaC").trigger("update");
										$("#cmbDestino").val('');
										$("#cmbUnidad").val('');
										$('#tabla_cargo').trigger('update');
										$("#accordion").accordion( "activate",0 );												
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Fecha no valido");	
			$("#txtFecha").focus();
				return false;
		}
});

$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

$("#txtFI").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFF").datepicker( "option", "minDate", selectedDate );
		$("#mytablaC").trigger("update");		
	}	
});
$("#txtFF").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFI").datepicker( "option", "maxDate", selectedDate );
		$("#mytablaC").trigger("update");	
	}	
});	

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  


function noNumbers(e){
		var keynum;
		var keychar;
		var numcheck;
		if(window.event){
			jeynum = e.keyCode;
		}
		else if(e.which){
			heynum = eArray.which;
		}
		keychar = String.formCharccode(keynum);
		numcheck = /\d/;
		return !numcheck.test(keychar);
	}
		
$(document).ready(function(){ 
	
	$("#nuevo").click();	
    $('#cmbUnidad').boxLoader({
            url:'<?php echo base_url()?>index.php/cargos/combo',
            equal:{id:'NomDes',value:'#cmbDestino'},
            select:{id:'NumUni',val:'val'},
            all:"Seleccione una opcion",
            onLoad:false            
    });  
      
	$("#tabla_car").ajaxSorter({
		url:'<?php echo base_url()?>index.php/cargos/tabla',  		
		filters:['cmbCiclo','asigna','txtFI','txtFF'],
		active:['NumRemS',0],
    	multipleFilter:true,
    	//onLoad:false, 
    	sort:false,              
    	onRowClick:function(){
    		$("#accordion").accordion( "activate",1 );
       		$("#id").val($(this).data('numsol'));
       		$("#txtFecha").val($(this).data('fechas'));
       		$("#rem").val($(this).data('numrems'));
       		//$("#numcli").val($(this).data('numclir'));
			$("#enc").val($(this).data('numbios'));
			$("#des").val($(this).data('nomdess'));			
			$("#uni").val($(this).data('numuni'));
			$("#ali").val($(this).data('ali'));
			$("#com").val($(this).data('com'));
			$("#cas").val($(this).data('cas'));
			$("#hos").val($(this).data('hos'));
			$("#fit").val($(this).data('fit'));
			$("#sub").val($(this).data('sub'));
			$("#rep").val($(this).data('imp'));
			$("#cg").val($(this).data('cg'));
			$("#tot").val($(this).data('tot'));
			$("#rblugar").val($(this).data('corte'));
			$("#cmbDestino").val('');
			$("#cmbUnidad").val('');
			$('#tabla_cargo').trigger('update');
    	},   
    	onSuccess:function(){
			$('#tabla_car tbody tr td:nth-child(10)').map(function(){ $(this).css('text-align','right');  $(this).css('font-weight','bold');})
	    },	 		
	});
	
	$("#tabla_cargo").ajaxSorter({
		url:'<?php echo base_url()?>index.php/cargos/tablacargos',  
		filters:['cmbDestino','cmbUnidad'],
		onLoad:false,
    	//sort:false,              
    	onRowClick:function(){
    		$("#nuevo").click();
			$("#des").val($("#cmbDestino").val());
			$("#uni").val($("#cmbUnidad").val());			
			$("#ali").val($(this).data('alimentacion'));
			$("#com").val($(this).data('combustible'));
			$("#cas").val($(this).data('casetas'));
			$("#hos").val($(this).data('hospedaje'));
			$("#fit").val($(this).data('fitosanitaria'));
			$("#sub").val($(this).data('totals'));
			$("#tot").val($(this).data('totals'));
    	}   
	});
		
});  	
</script>