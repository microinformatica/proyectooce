<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li ><a href="#larvicultura"><img src="<?php echo base_url();?>assets/images/menu/8.png" width="25" height="25" border="0"> Larvicultura </a></li>
		<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Enrique Sánchez" || $usuario=="Zuleima Benitez"){ ?>
		<li><a href="#codigos" style="font-size: 20px">Códigos Lab I </a></li>
		<li><a href="#codigos2" style="font-size: 20px">Códigos Lab II </a></li>
		<?php } ?>
		<li><a href="#raceways"><img src="<?php echo base_url();?>assets/images/menu/10.png" width="25" height="25" border="0"> Raceways </a></li>			
		<li><a href="#tratamientos" style="font-size: 20px">Tratamientos </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="larvicultura" class="tab_content" style="height: 452px;">
			<?php $hoy=date("Y-m-d"); ?>
			<div align="left" style="margin-left: 25px; margin-top: -10px" >
			Estadíos Actuales en Salas de Producción al día:<input style="border: none" size="12%" name="hoy1" id="hoy1" value="<?php echo set_value('hoy1',date("d-m-Y",strtotime($hoy))); ?>">	
			</div>
				<input type="hidden" style="border: none" size="12%" name="hoy" id="hoy" value="<?php echo set_value('hoy',$hoy); ?>">
				<input type="hidden" name="100i" id="100i" value="1"><input type="hidden" size="12%" name="100f" id="100f" value="12">
				<input type="hidden" name="200i" id="200i" value="13"><input type="hidden" size="12%" name="200f" id="200f" value="24">
				<input type="hidden" name="300i" id="300i" value="25"><input type="hidden" size="12%" name="300f" id="300f" value="36">
				<input type="hidden" name="400i" id="400i" value="37"><input type="hidden" size="12%" name="400f" id="400f" value="48">
				<input type="hidden" name="500i" id="500i" value="49"><input type="hidden" size="12%" name="500f" id="500f" value="60">
				<input type="hidden" name="600i" id="600i" value="61"><input type="hidden" size="12%" name="600f" id="600f" value="72">
				<input type="hidden" name="700i" id="700i" value="73"><input type="hidden" size="12%" name="700f" id="700f" value="84">
				<input type="hidden" name="800i" id="800i" value="85"><input type="hidden" size="12%" name="800f" id="800f" value="96">
				<input type="hidden" name="900i" id="900i" value="97"><input type="hidden" size="12%" name="900f" id="900f" value="108">
				<input type="hidden" name="1000i" id="1000i" value="109"><input type="hidden" size="12%" name="1000f" id="1000f" value="120">
				<input type="hidden" name="1100i" id="1100i" value="121"><input type="hidden" size="12%" name="1100f" id="1100f" value="132">
				<input type="hidden" name="1200i" id="1200i" value="133"><input type="hidden" size="12%" name="1200f" id="1200f" value="144">
				<input type="hidden" name="1300i" id="1300i" value="145"><input type="hidden" size="12%" name="1300f" id="1300f" value="156">
				<input type="hidden" name="1400i" id="1400i" value="157"><input type="hidden" size="12%" name="1400f" id="1400f" value="168">
				<input type="hidden" name="1500i" id="1500i" value="169"><input type="hidden" size="12%" name="1500f" id="1500f" value="180">
				<input type="hidden" name="1600i" id="1600i" value="181"><input type="hidden" size="12%" name="1600f" id="1600f" value="192">
				<input type="hidden" size="2%" name="tablasell" id="tablasell">
				<div style="overflow-y: hidden;height:310px; width: 890px">
				<table >
					<tbody>
						<tr>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar1" name="tabla_lar1" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL1" name="mytablaL1" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >100</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar2" name="tabla_lar2" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL2" name="mytablaL2" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >200</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar3" name="tabla_lar3" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL3" name="mytablaL3" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >300</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar4" name="tabla_lar4" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL4" name="mytablaL4" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >400</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar5" name="tabla_lar5" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL5" name="mytablaL5" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >500</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
            				</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar6" name="tabla_lar6" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL6" name="mytablaL6" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >600</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar7" name="tabla_lar7" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL7" name="mytablaL7" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >700</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar8" name="tabla_lar8" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL8" name="mytablaL8" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >800</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar9" name="tabla_lar9" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL9" name="mytablaL9" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >900</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar10" name="tabla_lar10" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;width: 110px;" >
                   						<table id="mytablaL10" name="mytablaL10" class="ajaxSorter">
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1000</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar11" name="tabla_lar11" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;width: 110px;" >
                   						<table id="mytablaL11" name="mytablaL11" class="ajaxSorter" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1100</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar12" name="tabla_lar12" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;width: 110px;" >
                   						<table id="mytablaL12" name="mytablaL12" class="ajaxSorter" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1200</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar13" name="tabla_lar13" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;width: 110px;" >
                   						<table id="mytablaL13" name="mytablaL13" class="ajaxSorter" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1300</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar14" name="tabla_lar14" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;width: 110px;" >
                   						<table id="mytablaL14" name="mytablaL14" class="ajaxSorter" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1400</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
            				</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar15" name="tabla_lar15" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;width: 110px;" >
                   						<table id="mytablaL15" name="mytablaL15" class="ajaxSorter" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1500</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar16" name="tabla_lar16" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;width: 110px;" >
                   						<table id="mytablaL16" name="mytablaL16" class="ajaxSorter" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1600</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
						</tr>
					</tbody>
				</table>
			</div>	
			<table style="width: 890px">
				<tr>
					<th style="width: 610px;">
						<div class="ajaxSorterDiv" id="tabla_larsie" name="tabla_larsie" align="right"  >                
            				<span class="ajaxTable" style=" width: 210px; height: 130px; background-color: white;" >
                   				<table id="mytablaLS" name="mytablaLS" class="ajaxSorter" >
                       				<thead style="font-size: 9px">
                       					<tr>
                       						<th colspan="4" style="text-align: center">Pilas en Procesos Actuales</th>
                       					</tr>
                       					<tr>                            
                           				<th data-order = "FecSie" >Fecha</th>
                           				<th data-order = "Sala" >Sala</th>
                           				<th data-order = "Nombre"  >Pila</th>
                           				<th data-order = "CanSie" >Cantidad</th>
                           				</tr>                                              	                            	
                       				</thead>
                       				<tbody style="font-size: 11px">
                       				</tbody>                        	
                   				</table>
            				</span>          
            			</div>
					</th>
					<th style="width: 280px;">
						<div id='ver_mas_larvi' class='hide' >
							<table class="ajaxSorter" border="1px" style="width: 280px; " >
           						<thead >
           							<th style="font-size: 14px; text-align: center" rowspan="2">Pila</th>
           							<th style="font-size: 14px; text-align: center" colspan="2">Datos de Siembra</th>
           							<tr>
           								<th style="font-size: 14px; text-align: center">Fecha</th>
           								<th style="font-size: 14px; text-align: center">Cantidad Actual</th>		
           							</tr>
           						</thead>
           						<tbody>
           							<th style="font-size: 14px; text-align: center"><input readonly="true" size="6%" type="text" name="pilal" id="pilal" style="text-align: center; border: none; color: red;" ></th>
           							<th style="font-size: 12px; text-align: center"><input size="13%" type="text" name="txtFechal" id="txtFechal" class="fecha redondo mUp" readonly="true" style="text-align: center; border: none;" ></th>
           							<th style="font-size: 12px; text-align: center"><input size="3%" type="text" name="canl" id="canl" style="text-align: center; border: none;" ></th>
           						</tbody>
           						<tfoot>
           							<th style="font-size: 14px" colspan="4">Pila Actualmente Vacía?
           								&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Enrique Sánchez" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="rblugarl" type="radio" value="-1" onclick="radios(2,-1)" />Si</td>
										&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Enrique Sánchez" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="rblugarl" type="radio" value="0" onclick="radios(2,0)" />No</td>
           							</th>
           							<tr>
           								<th colspan="3" style="text-align: right">
           									<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Enrique Sánchez" || $usuario=="Zuleima Benitez"){ ?>
												<input type="hidden" size="4%"  name="idl" id="idl"/>
												<input style="font-size: 14px" type="submit" id="aceptarl" name="aceptarl" value="Guardar" />
											<?php }?>
											<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(5)" />
											<input type="hidden" size="3%"  name="cancelal" id="cancelal" />
           								</th>
           							</tr>
           						</tfoot>
           					</table>
        				</div>
        			</th>
        		</tr>
		</table>	
		</div>
		<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Enrique Sánchez" || $usuario=="Zuleima Benitez"){ ?>
		<div id="codigos" class="tab_content" style="height: 452px" >
			<div align="left" >
				<table class="tablasorter" border="1px" width="850px" >
					<tbody>
						<tr>
							<th style="text-align: center;" >No.
							<input size="8%" type="hidden" name="rem1" id="rem1" value="<?php echo set_value('rem1',$txtRemision); ?>">
							<input size="8%" type="hidden" name="ur" id="ur" value="<?php echo set_value('ur',$urr); ?>">	
							</th>
							<th style="text-align: center;">Pila    		    </th>
							<th style="text-align: center;">Fecha</th>
							<th style="text-align: center;">Cantidad</th>
   							<th style="text-align: center;">PL</th>
   							<th colspan="4" style="text-align: center;">Procedencia
   								<select id="sali" style="font-size: 9px;height: 25px;  margin-top: 1px; width: 55px">
          							<option value="0" >Sala</option>
          								<?php	
          								$this->load->model('produccion_model');
          								$data['result']=$this->produccion_model->verSala();
										foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->Sala;?>" ><?php echo $row->Sala;?></option>
           								<?php endforeach;?>
   								</select>
   								
   							</th>
							<th style="text-align: center;">Nauplios</th>
							<th style="text-align: center;">Maduración</th>
							<th style="text-align: center;">Origen</th>	  						
							
								
							
						</tr>
						<tr>
							<th style="text-align: center;background-color: gray; color: white">
								<input size="8%" type="hidden" name="idcod" id="idcod"/>
								<input readonly="true" type="text" id="nse" style=" width: 30px; text-align: center; border: none; color: red;" value="<?php echo set_value('nse','rem1'); ?>">
							</th>
							<th style="text-align: center;background-color: gray; color: white">
								<select name='pil' id="pil" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;width: 65px">
          							<option value="0">Sel</option>
          						<?php	
           							$this->load->model('produccion_model');
									$data['result']=$this->produccion_model->verPila();$pili=0;
									foreach ($data['result'] as $row):
									if($row->NumPila >=1 && $row->NumPila<=40){
									?>
           								<option value="<?php echo $row->nom;?>" ><?php echo $row->Nombre;?></option>
           						<?php 
									}
           							endforeach;?>
   								</select>
							</th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="fec" class="fecha redondo mUp" readonly="true" style="text-align: center;; width: 70px" >	</th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="pls" style="text-align: right; width: 70px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="est" style="text-align: center; width: 20px" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="p1" style="text-align: center; width: 90px" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="p2" style="text-align: center; width: 90px" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="p3" style="text-align: center; width: 90px" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="p4" style="text-align: center; width: 90px" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="nau" style="text-align: right;; width: 70px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" ></th>
							<th style="text-align: center;background-color: gray; color: white"> 
								<select id="mad" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;width: 65px">
									<option value="">Sel</option>
									<option value="M1" >I</option>
   									<option value="M2" >II</option>
   									<option value="M1M2" >I-II</option>
   								</select>
							</th>
							<th style="text-align: center;background-color: gray; color: white"> 
								<select id="ori" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px; width: 95px">
									<option value="">Sel</option>
									<option value="MX" >Aquapacific</option>
   									<option value="EC" >Ecuatoriana</option>
   								</select>	
							</th>
							
						</tr>
						<tr>
							<th style="text-align: center;" colspan="12">
								<select name='pin' id="pin" style="visibility: hidden; width: 20px" ></select>
								Código Generado:
								<input id="cod" style="text-align: center;background-color: gray; color: white; border: none;width: 400px">
								<input type="submit" id="cnuevo" name="cnuevo" value="Nuevo"  />
								<input type="submit" id="caceptar" name="caceptar" value="Guardar" />
							</th>
						</tr>
					</tbody>					
				</table>
				<div class="ajaxSorterDiv" id="tabla_cod" name="tabla_cod" style="margin-top: -25px;">                
            		<span class="ajaxTable" style="height: 330px; background-color: white; margin-top: 5px  " >
                		<table id="mytablaCod" name="mytablaCod" class="ajaxSorter" border="1" >
                			<thead style="font-size: 9px;">                            
                					<th data-order = "nse" style="width: 10px">No.</th>
                					<th data-order = "Nombre" style="width: 25px">Pila</th>
                					<th data-order = "fec1" style="width: 70px">Fecha</th>
                					<th data-order = "pls" style="width: 60px">Postlarvas</th>
                					<th data-order = "est" style="width: 10px">PL</th>
                					<th data-order = "pilis">Pilas</th>
                					<th data-order = "nau" style="width: 60px">Nauplios</th>
                					<th data-order = "mad" style="width: 30px">Maduracion</th>  
                					<th data-order = "ori" style="width: 20px">Origen</th>   
                					<th data-order = "cod">Código</th>                                           	                            	
                			</thead>
                    		<tbody style="font-size: 11px">
                    		</tbody>                        	
                   		</table>
            			</span> 
            		<div class="ajaxpager" style="margin-top: -10px">        
	                    	<form method="post" action="<?= base_url()?>index.php/produccion/pdfrepcod/" >
        	            		Pila:                    		
									<select id="cmbPila" name="cmbPila" style="font-size: 10px; height: 25px; " >								
    									<option value="0">Todos</option>
          						<?php	
           							$this->load->model('produccion_model');
									$data['result']=$this->produccion_model->verPila();
									foreach ($data['result'] as $row):
									if($row->NumPila >=1 && $row->NumPila<=40){
									?>
           								<option value="<?php echo $row->nom;?>" ><?php echo $row->Nombre;?></option>
           						<?php 
									};
           							endforeach;?>
									</select> 
                	        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tc" value ="1" />  
                    	    	<input type="hidden" name="tablacod1" value ="" class="htmlTable"/>                        		                        		                                                      
 							</form>   
                		</div>	  
            	</div>
			</div>
        </div>
        <?php } ?>
        <?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Enrique Sánchez" || $usuario=="Zuleima Benitez"){ ?>
		<div id="codigos2" class="tab_content" style="height: 452px" >
			<div align="left" >
				<table class="tablasorter" border="1px" width="850px" >
					<tbody>
						<tr>
							<th style="text-align: center;" >No.
							<input size="8%" type="hidden" name="rem12" id="rem12" value="<?php echo set_value('rem12',$txtRemision2); ?>">
							<input size="8%" type="hidden" name="ur2" id="ur2" value="<?php echo set_value('ur2',$urr2); ?>">	
							</th>
							<th style="text-align: center;">Pila    		    </th>
							<th style="text-align: center;">Fecha</th>
							<th style="text-align: center;">Cantidad</th>
   							<th style="text-align: center;">PL</th>
   							<th colspan="4" style="text-align: center;">Procedencia
   								<select id="sali2" style="font-size: 9px;height: 25px;  margin-top: 1px; width: 55px">
          							<option value="0" >Sala</option>
          								<?php	
          								$this->load->model('produccion_model');
          								$data['result']=$this->produccion_model->verSala();
										foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->Sala;?>" ><?php echo $row->Sala;?></option>
           								<?php endforeach;?>
   								</select>
   								
   							</th>
							<th style="text-align: center;">Nauplios</th>
							<th style="text-align: center;">Maduración</th>
							<th style="text-align: center;">Origen</th>	  						
							
								
							
						</tr>
						<tr>
							<th style="text-align: center;background-color: gray; color: white">
								<input size="8%" type="hidden" name="idcod2" id="idcod2"/>
								<input readonly="true" type="text" id="nse2" style=" width: 30px; text-align: center; border: none; color: red;" value="<?php echo set_value('nse2','rem12'); ?>">
							</th>
							<th style="text-align: center;background-color: gray; color: white">
								<select name='pil2' id="pil2" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;width: 65px">
          							<option value="0">Sel</option>
          						<?php	
           							$this->load->model('produccion_model');
									$data['result']=$this->produccion_model->verPila();
									foreach ($data['result'] as $row):
									if($row->NumPila >=41 && $row->NumPila<=60){
									?>
           								<option value="<?php echo $row->nom;?>" ><?php echo $row->Nombre;?></option>
           						<?php  } endforeach;?>
   								</select>
							</th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="fec2" class="fecha redondo mUp" readonly="true" style="text-align: center;; width: 70px" >	</th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="pls2" style="text-align: right; width: 70px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="est2" style="text-align: center; width: 20px" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="p12" style="text-align: center; width: 90px" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="p22" style="text-align: center; width: 90px" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="p32" style="text-align: center; width: 90px" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="p42" style="text-align: center; width: 90px" ></th>
							<th style="text-align: center;background-color: gray; color: white"><input type="text" id="nau2" style="text-align: right;; width: 70px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" ></th>
							<th style="text-align: center;background-color: gray; color: white"> 
								<select id="mad2" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;width: 65px">
									<option value="">Sel</option>
									<option value="M1" >I</option>
   									<option value="M2" >II</option>
   									<option value="M1M2" >I-II</option>
   								</select>
							</th>
							<th style="text-align: center;background-color: gray; color: white"> 
								<select id="ori2" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px; width: 95px">
									<option value="">Sel</option>
									<option value="MX" >Aquapacific</option>
   									<option value="EC" >Ecuatoriana</option>
   								</select>	
							</th>
							
						</tr>
						<tr>
							<th style="text-align: center;" colspan="12">
								<select name='pin2' id="pin2" style="visibility: hidden; width: 20px" ></select>
								Código Generado:
								<input id="cod2" style="text-align: center;background-color: gray; color: white; border: none;width: 400px">
								<input type="submit" id="cnuevo2" name="cnuevo2" value="Nuevo"  />
								<input type="submit" id="caceptar2" name="caceptar2" value="Guardar" />
							</th>
						</tr>
					</tbody>					
				</table>
				<div class="ajaxSorterDiv" id="tabla_cod2" name="tabla_cod2" style="margin-top: -25px;">                
            		<span class="ajaxTable" style="height: 330px; background-color: white; margin-top: 5px " >
                		<table id="mytablaCod2" name="mytablaCod2" class="ajaxSorter" border="1" >
                			<thead style="font-size: 9px;">                            
                					<th data-order = "nse" style="width: 10px">No.</th>
                					<th data-order = "Nombre" style="width: 25px">Pila</th>
                					<th data-order = "fec1" style="width: 70px">Fecha</th>
                					<th data-order = "pls" style="width: 60px">Postlarvas</th>
                					<th data-order = "est" style="width: 10px">PL</th>
                					<th data-order = "pilis">Pilas</th>
                					<th data-order = "nau" style="width: 60px">Nauplios</th>
                					<th data-order = "mad" style="width: 30px">Maduracion</th>  
                					<th data-order = "ori" style="width: 20px">Origen</th>   
                					<th data-order = "cod">Código</th>                                           	                            	
                			</thead>
                    		<tbody style="font-size: 11px">
                    		</tbody>                        	
                   		</table>
            			</span> 
            			<div class="ajaxpager" style="margin-top: -10px">        
	                    	<form method="post" action="<?= base_url()?>index.php/produccion/pdfrepcod/" >
        	            		Pila:                    		
									<select id="cmbPila2" name="cmbPila2" style="font-size: 10px; height: 25px; " >								
    									<option value="0">Todos</option>
          						<?php	
           							$this->load->model('produccion_model');
									$data['result']=$this->produccion_model->verPila();
									foreach ($data['result'] as $row):
									if($row->NumPila >=41 && $row->NumPila<=60){
									?>
           								<option value="<?php echo $row->nom;?>" ><?php echo $row->Nombre;?></option>
           						<?php } endforeach;?>
									</select> 
                	        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tc" value ="2" />
                    	    	<input type="hidden" name="tablacod2" value ="" class="htmlTable"/>                        		                        		                                                      
 							</form>   
                		</div>	  
            	</div>            	
			</div>
        </div>
        <?php } ?>
		<div id="raceways" class="tab_content" style="height: 452px" >
			
			<div align="left" >
				<table class="tablaSorter" border="0px" style="margin-top: -10px; margin-left: -7px;">
							
								
								<input type="hidden" style="border: none" size="12%" name="hoy" id="hoy" value="<?php echo set_value('hoy',$hoy); ?>">
								<input type="hidden" size="12%" name="ini1" id="ini1" value="1"><input type="hidden" size="12%" name="fin1" id="fin1" value="20">
								<input type="hidden" size="12%" name="ini2" id="ini2" value="21"><input type="hidden" size="12%" name="fin2" id="fin2" value="40">
								<input type="hidden" size="12%" name="ini3" id="ini3" value="41"><input type="hidden" size="12%" name="fin3" id="fin3" value="60">
								<input type="hidden" size="12%" name="ini4" id="ini4" value="91"><input type="hidden" size="12%" name="fin4" id="fin4" value="110">
								<input type="hidden" size="12%" name="ini5" id="ini5" value="87"><input type="hidden" size="12%" name="fin5" id="fin5" value="90">
								<input type="hidden" size="2%" name="tablasel" id="tablasel">
							
							
					
					<tbody>
						<tr >
							<th style="width: 370px;font-size: 10px;"  >
								Existencia Postlarvas al día:
								<input style="border: none" size="12%" name="hoy1" id="hoy1" value="<?php echo set_value('hoy1',date("d-m-Y",strtotime($hoy))); ?>">
								
								
								<div class="ajaxSorterDiv" id="tabla_ras7" name="tabla_ras7"  style="width: 210px" >                
            						<span class="ajaxTable" style="background-color: white; height: 188px ; width: 208px;" >
                   						<table id="mytablaR7" name="mytablaR7" class="ajaxSorter" >
                       						<thead >                            
                           						<th style="font-size: 8px" data-order = "PL1"  >Generales en Raceways</th>	<th style="font-size: 8px" data-order = "totpl"  >Cantidad</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 10px">
                       						</tbody>
                   						</table>
                   						<?php	
           									$this->load->model('produccion_model');
											$data['result']=$this->produccion_model->UltimaAct();
											foreach ($data['result'] as $row):
           										$hoyAct=$row->ultimaact;
           									endforeach;
           								?>
           								
           								Ultima Actualización: <input style="border: none; font-size: 10px; <? if($hoy!=$hoyAct){ ?> background-color: red; color: white;<? } ?> text-align: center" size="16%" name="hoyA" id="hoyA" value="<?php echo set_value('hoyA',date("d-m-Y",strtotime($hoyAct))); ?>">
                   						<?php
                   						//if($hoy!=$hoyAct){ echo ":(";} else{ echo ":)";}
                   						?>
            						</span>
            						<img style="cursor: pointer; margin-top: -10px" title="Enviar datos de tabla en archivo PDF" id="export_data" src="<?= base_url()?>assets/img/sorter/pdf.png" />
            						<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Enrique Sánchez" || $usuario=="Zuleima Benitez"){ ?>
									<input style="font-size: 12px" type="submit" id="his" name="his" value="Actualizar Historial" />
								<?php }?>
            						<form id="data_tables" action="<?= base_url()?>index.php/produccion/pdfrep" method="POST">
												<input type="hidden" name="tablar1" id="html_r1"/>
												<input type="hidden" name="tablar2" id="html_r2"/>
												<input type="hidden" name="tablar3" id="html_r3"/>
												<input type="hidden" name="tablar4" id="html_r4"/>
												<input type="hidden" name="tablar5" id="html_r5"/>
												<input type="hidden" name="tablar6" id="html_r6"/>
												<input type="hidden" name="tablar7" id="html_r7"/>
												<input type="hidden" name="dia" id="dia"/>
												<input type="hidden" style="border: none; font-size: 10px; <? if($hoy!=$hoyAct){ ?> background-color: red; color: white;<? } ?> text-align: center" size="16%" name="diaA" id="diaA" value="<?php echo set_value('hoyA',date("d-m-Y",strtotime($hoyAct))); ?>">
											</form>           
            					</div>
								
								<script type="text/javascript">
									$(document).ready(function(){
										$('#export_data').click(function(){									
											$('#html_r1').val($('#mytablaR1').html());
											$('#html_r2').val($('#mytablaR2').html());									
											$('#html_r3').val($('#mytablaR3').html());
											$('#html_r4').val($('#mytablaR4').html());
											$('#html_r5').val($('#mytablaR5').html());
											$('#html_r6').val($('#mytablaR6').html());
											$('#html_r7').val($('#mytablaR7').html());
											$('#data_tables').submit();
											$('#html_r1').val('');
											$('#html_r2').val('');
											$('#html_r3').val('');
											$('#html_r4').val('');
											$('#html_r5').val('');
											$('#html_r6').val('');
											$('#html_r7').val('');
										});
									});
								</script>            				
								<div id='ver_mas_pila' class='hide'  >
								<table class="ajaxSorter" border="1px" width="80px">
            						<thead >
            							<th style="font-size: 10px; text-align: center" rowspan="2">Pila</th>
            							<th style="font-size: 10px; text-align: center" colspan="3">Datos de Siembra</th>
            							<th style="font-size: 10px; text-align: center" rowspan="2" >Can <br>Act</th>
            							
            							<tr>
            								<th style="font-size: 10px; text-align: center" rowspan="2">Sala</th>
            								<th style="font-size: 10px; text-align: center">Fecha</th>	<th style="font-size: 10px; text-align: center">Pl</th>	
            							</tr>
            						</thead>
            						<tbody>
            							<tr>
            							<th style="font-size: 10px; text-align: center"><input readonly="true" size="2%" type="text" name="pila" id="pila" style="text-align: center; border: none; color: red;" ></th>
            							<th style="font-size: 10px; text-align: center">
            								<select name="sal" id="sal" style="font-size: 9px;height: 25px;  margin-top: 1px; width: 55px">
          										<option value="0" >Sala</option>
          											<?php	
          											$this->load->model('produccion_model');
          											$data['result']=$this->produccion_model->verSala();
													foreach ($data['result'] as $row):?>
           												<option value="<?php echo $row->Sala;?>" ><?php echo $row->Sala;?></option>
           											<?php endforeach;?>
           											<option value="1700"  >1700</option>
           											<option value="1800"  >1800</option>
           											<option value="1900"  >1900</option>
           											<option value="2000"  >2000</option>
   											</select>
            							</th>
            							<th style="font-size: 10px; text-align: center"><input size="8%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="text-align: center; border: none;" ></th>
            							<th style="font-size: 10px; text-align: center"><input size="2%" type="text" name="pl" id="pl" style="text-align: center; border: none;" ></th>
            							<th style="font-size: 10px; text-align: center"><input size="2%" type="text" name="can" id="can" style="text-align: center; border: none;" ></th>
            							</tr>
            							<tr>
            								<th colspan="5"> Origen
            									<select name="origen" id="origen" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:5px;">
      												<option style="background-color: yellow" value="Ecuatoriana"  > Ecuador </option>
      												<option value="Aquapacific"  > México </option>
    								  			</select>
            								</th>
            							</tr>
            						</tbody>
            						<tfoot>
            							<th style="font-size: 10px" colspan="5">Pila Actualmente Vacía?
            								&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Enrique Sánchez" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="-1" onclick="radios(1,-1)" />Si</td>
											&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Enrique Sánchez" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="0" onclick="radios(1,0)" />No</td>
            							</th>
            							<tr>
            								<th colspan="5" style="text-align: right">
            									<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Enrique Sánchez" || $usuario=="Zuleima Benitez"){ ?>
													<input type="hidden" size="4%"  name="id" id="id"/>
													<input style="font-size: 12px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
												<?php }?>
												<input style="font-size: 12px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(6)" />
												<input type="hidden" size="3%"  name="cancela" id="cancela" />
            								</th>
            							</tr>
            						</tfoot>
            					</table>
            					</div>
            					
            					
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras1" name="tabla_ras1" style="margin-top: -5px; width: 130px" >                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 130px" >
                   						<table id="mytablaR1" name="mytablaR1" class="ajaxSorter"  >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 25px" >Ext</th><th data-order = "Sala" style="width: 25px" >Sal</th> <th data-order = "PL1" style="width: 35px">Est</th>	<th data-order = "CanSie" style="width: 20px" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px;" >
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras2" name="tabla_ras2" style="margin-top: -5px; width: 130px">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 130px; " >
                   						<table id="mytablaR2" name="mytablaR2" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 25px" >Ext</th><th data-order = "Sala" style="width: 25px" >Sal</th> <th data-order = "PL1" style="width: 35px">Est</th>	<th data-order = "CanSie" style="width: 20px" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span> 
            					</div>
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras3" name="tabla_ras3" style="margin-top: -5px; width: 130px;">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 130px; " >
                   						<table id="mytablaR3" name="mytablaR3" class="ajaxSorter" >
                       						<thead style="font-size: 9px; ">                            
                           						<th data-order = "Nombre" style="width: 25px" >Ext</th><th data-order = "Sala" style="width: 25px" >Sal</th> <th data-order = "PL1" style="width: 35px">Est</th>	<th data-order = "CanSie" style="width: 20px" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>                                      
            					</div>
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras4" name="tabla_ras4" style="margin-top: -5px; width: 130px;">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 130px; " >
                   						<table id="mytablaR4" name="mytablaR4" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 25px" >Ext</th><th data-order = "Sala" style="width: 25px" >Sal</th> <th data-order = "PL1" style="width: 35px">Est</th>	<th data-order = "CanSie" style="width: 20px" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>                                      
            					</div>
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras5" name="tabla_ras5" style="margin-top: -5px; width: 145px;">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 135px; " >
                   						<table id="mytablaR5" name="mytablaR5" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 30px">Res</th><th data-order = "Sala" style="width: 25px" >Sal</th>	<th data-order = "PL1" style="width: 35px">Est</th>	<th data-order = "CanSie" style="width: 20px">Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span> 
            					</div>
            				</th>
							<th rowspan="1" style="width: 90px;">
								<div class="ajaxSorterDiv" id="tabla_ras6" name="tabla_ras6" style="margin-top: -5px; width: 80px" >                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 75px"  >
                   						<table id="mytablaR6" name="mytablaR6" class="ajaxSorter"   >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "PL1" style="width: 30px">Est</th>	<th data-order = "totpl" style="width: 25px">Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>           
            					</div>
							</th>
						</tr>
					</tbody>
				</table>
			</div>
        </div>
		<div id="tratamientos" class="tab_content" style="height: 452px" >
			<div align="left" >
				<table class="tablaSorter" border="0px" style="margin-top: -10px; margin-left: -7px;">
							<input type="hidden" style="border: none" size="12%" name="hoyt" id="hoyt" value="<?php echo set_value('hoyt',$hoy); ?>">
							<input type="hidden" size="2%" name="tablaselt" id="tablaselt">
					<tbody>
						<tr >
							
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras11" name="tabla_ras11" style="margin-top: -5px; width: 230px" >                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 230px" >
                   						<table id="mytablaR11" name="mytablaR11" class="ajaxSorter"  >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 25px" >Ext</th><th data-order = "PL11" style="width: 35px">Est</th>	<th data-order = "CanSie1" style="width: 20px" >Can</th>                                                	                            	
                           						<th data-order = "tra1" style="width: 100px" >Tratamiento</th>
                       						</thead>
                       						<tbody style="font-size: 11px;" >
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras22" name="tabla_ras22" style="margin-top: -5px; width: 230px">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 230px; " >
                   						<table id="mytablaR22" name="mytablaR22" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 25px" >Ext</th> <th data-order = "PL11" style="width: 35px">Est</th>	<th data-order = "CanSie1" style="width: 20px" >Can</th>
                           						<th data-order = "tra1" style="width: 100px" >Tratamiento</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span> 
            					</div>
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras33" name="tabla_ras33" style="margin-top: -5px; width: 230px;">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 230px; " >
                   						<table id="mytablaR33" name="mytablaR33" class="ajaxSorter" >
                       						<thead style="font-size: 9px; ">                            
                           						<th data-order = "Nombre" style="width: 25px" >Ext</th><th data-order = "PL11" style="width: 35px">Est</th>	<th data-order = "CanSie1" style="width: 20px" >Can</th>
                           						<th data-order = "tra1" style="width: 100px" >Tratamiento</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>                                      
            					</div>
							</th>							
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras55" name="tabla_ras55" style="margin-top: -5px; width: 245px;">                
            						<span class="ajaxTable" style="height: 401px; background-color: white; width: 235px; " >
                   						<table id="mytablaR55" name="mytablaR55" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 30px">Res</th>	<th data-order = "PL11" style="width: 35px">Est</th>	<th data-order = "CanSie1" style="width: 20px">Can</th>
                           						<th data-order = "tra1" style="width: 100px" >Tratamiento</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span> 
            					</div>
            					
            							<img style="cursor: pointer; margin-top: -10px" title="Enviar datos de tabla en archivo PDF" id="export_datat" src="<?= base_url()?>assets/img/sorter/pdf.png" />
            						<form id="data_tablest" action="<?= base_url()?>index.php/produccion/pdfrept" method="POST">
            							Dia de Tratamiento: <input style="border: none; font-size: 10px; text-align: center" size="10%" name="diat" id="diat" value="<?php echo set_value('diat',date('d-m-Y', strtotime('+1 day'))); ?>">
										
												<input type="hidden" name="tablar11" id="html_r11"/>
												<input type="hidden" name="tablar22" id="html_r22"/>
												<input type="hidden" name="tablar33" id="html_r33"/>
												<input type="hidden" name="tablar55" id="html_r55"/>
											</form>           
            					<script type="text/javascript">
									$(document).ready(function(){
										$('#export_datat').click(function(){									
											$('#html_r11').val($('#mytablaR11').html());
											$('#html_r22').val($('#mytablaR22').html());									
											$('#html_r33').val($('#mytablaR33').html());
											$('#html_r55').val($('#mytablaR55').html());
											$('#data_tablest').submit();
											$('#html_r11').val('');
											$('#html_r22').val('');
											$('#html_r33').val('');
											$('#html_r55').val('');
										});
									});
								</script>
            				</th>
							
						</tr>
					</tbody>					
				</table>
			</div>
        </div>	
        
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#cnuevo").click( function(){	
	$("#idcod").val('');
	$("#fec").val('');$("#sali").val('');$("#pil").val('0');$("#pin").val('');$("#pls").val('');$("#est").val('');
	$("#p1").val('');$("#p2").val('');$("#p3").val('');$("#p4").val('');
	$("#nau").val('');$("#mad").val('');$("#ori").val('');$("#cod").val('');
    $("#nse").val($("#rem1").val());
 return true;
})
$("#cnuevo2").click( function(){	
	$("#idcod2").val('');
	$("#fec2").val('');$("#sali2").val('');$("#pil2").val('0');$("#pin2").val('');$("#pls2").val('');$("#est2").val('');
	$("#p12").val('');$("#p22").val('');$("#p32").val('');$("#p42").val('');
	$("#nau2").val('');$("#mad2").val('');$("#ori2").val('');$("#cod2").val('');
    $("#nse2").val($("#rem12").val());
 return true;
})
$("#caceptar").click( function(){	
	/*sali=$("#sali").val();p1=$("#p1").val();p2=$("#p2").val();p3=$("#p3").val();p4=$("#p4").val();
	mad=$("#mad").val();ori=$("#ori").val();pls=$("#pls").val();*/
	pil=$("#pil").val();
	numero=$("#idcod").val();fec=$("#fec").val();
	if( pil!='0'){
		if( fec!=''){
		/*	if( pls!=''){
				if( sali!='0'){
				if( p1!='' || p2!='' || p3!='' || p4!=''){
					if( mad!='0'){
						if( ori!='0'){*/
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/produccion/actualizarcod", 
						data: "id="+$("#idcod").val()+"&pil="+$("#pin").val()+"&fec="+$("#fec").val()+"&pls="+$("#pls").val()+"&est="+$("#est").val()+"&p1="+$("#p1").val()+"&p2="+$("#p2").val()+"&p3="+$("#p3").val()+"&p4="+$("#p4").val()+"&nau="+$("#nau").val()+"&mad="+$("#mad").val()+"&ori="+$("#ori").val()+"&cod="+$("#cod").val()+"&sala="+$("#sali").val()+"&tabla="+1,
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaCod").trigger("update");
										$("#cnuevo").click();																					
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/produccion/agregarcod", 
						data: "&pil="+$("#pin").val()+"&fec="+$("#fec").val()+"&pls="+$("#pls").val()+"&est="+$("#est").val()+"&p1="+$("#p1").val()+"&p2="+$("#p2").val()+"&p3="+$("#p3").val()+"&p4="+$("#p4").val()+"&nau="+$("#nau").val()+"&mad="+$("#mad").val()+"&ori="+$("#ori").val()+"&cod="+$("#cod").val()+"&sala="+$("#sali").val()+"&tabla="+1,
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Pila Raceways registrada correctamente");
										$("#mytablaCod").trigger("update");
										$("#cnuevo").click();										
										$("#nse").val(parseFloat($("#rem1").val())+1);
										$("#rem1").val(parseFloat($("#nse").val()));
										$("#ur").val(parseFloat($("#ur").val())+1);											
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
			
		}else{
			alert("Error: Seleccione Fecha");	
			$("#fec").focus();
				return false;
		}
		
	}else{
			alert("Error: Seleccione Pila");	
			$("#pil").focus();
				return false;
		}
});

$("#caceptar2").click( function(){	
	/*sali=$("#sali").val();p1=$("#p1").val();p2=$("#p2").val();p3=$("#p3").val();p4=$("#p4").val();
	mad=$("#mad").val();ori=$("#ori").val();pls=$("#pls").val();*/
	pil=$("#pil2").val();
	numero=$("#idcod2").val();fec=$("#fec2").val();
	if( pil!='0'){
		if( fec!=''){
		/*	if( pls!=''){
				if( sali!='0'){
				if( p1!='' || p2!='' || p3!='' || p4!=''){
					if( mad!='0'){
						if( ori!='0'){*/
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/produccion/actualizarcod", 
						data: "id="+$("#idcod2").val()+"&pil="+$("#pin2").val()+"&fec="+$("#fec2").val()+"&pls="+$("#pls2").val()+"&est="+$("#est2").val()+"&p1="+$("#p12").val()+"&p2="+$("#p22").val()+"&p3="+$("#p32").val()+"&p4="+$("#p42").val()+"&nau="+$("#nau2").val()+"&mad="+$("#mad2").val()+"&ori="+$("#ori2").val()+"&cod="+$("#cod2").val()+"&sala="+$("#sali2").val()+"&tabla="+2,
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaCod2").trigger("update");
										$("#cnuevo2").click();																					
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/produccion/agregarcod", 
						data: "&pil="+$("#pin2").val()+"&fec="+$("#fec2").val()+"&pls="+$("#pls2").val()+"&est="+$("#est2").val()+"&p1="+$("#p12").val()+"&p2="+$("#p22").val()+"&p3="+$("#p32").val()+"&p4="+$("#p42").val()+"&nau="+$("#nau2").val()+"&mad="+$("#mad2").val()+"&ori="+$("#ori2").val()+"&cod="+$("#cod2").val()+"&sala="+$("#sali2").val()+"&tabla="+2,
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Pila Raceways registrada correctamente");
										$("#mytablaCod2").trigger("update");
										$("#cnuevo2").click();										
										$("#nse2").val(parseFloat($("#rem12").val())+1);
										$("#rem12").val(parseFloat($("#nse2").val()));
										$("#ur2").val(parseFloat($("#ur2").val())+1);											
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
			
		}else{
			alert("Error: Seleccione Fecha");	
			$("#fec2").focus();
				return false;
		}
		
	}else{
			alert("Error: Seleccione Pila");	
			$("#pil2").focus();
				return false;
		}
});


function cerrar(sel){
	$(this).removeClass('used');
	if(sel==5)	$('#ver_mas_larvi').hide(); else $('#ver_mas_pila').hide(); 	
    
}
$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#txtFechal").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#fec").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

$("#fec2").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

function radios(radio,dato){
	if(radio==1) { $("#cancela").val(dato);}
	if(radio==2) { $("#cancelal").val(dato);}
}
$("#his").click( function(){	
 if(confirm('Esta Seguro de Realizar el proceso de historial para el día de Hoy?')){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/produccion/historial", 
			success: 	
					function(msg){															
						if(msg!=0){
							if(msg==1){alert("ya habias actualizado... hasta mañana podras de nuevo hacerlo");}		
							if(msg==2){alert("Historial Actualizado");
							}		
						}else{
							alert("Error con la base de datos o usted no ha actualizado nada");
						}					
					}			
	});
 }		 
});
$("#aceptar").click( function(){	
	sel=$("#tablasel").val();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/produccion/actualizar", 
			data: "id="+$("#id").val()+"&fec="+$("#txtFecha").val()+"&pl="+$("#pl").val()+"&can="+$("#can").val()+"&vacio="+$("#cancela").val()+"&sal="+$("#sal").val()+"&ori="+$("#origen").val(),
			success: 	
					function(msg){															
						if(msg!=0){			
							if(sel==1){$("#mytablaR1").trigger("update");$("#mytablaR11").trigger("update");}											
							if(sel==2){$("#mytablaR2").trigger("update");$("#mytablaR22").trigger("update");}
							if(sel==3){$("#mytablaR3").trigger("update");$("#mytablaR33").trigger("update");}
							if(sel==4){$("#mytablaR4").trigger("update");}
							if(sel==5){$("#mytablaR5").trigger("update");$("#mytablaR55").trigger("update");}
							$("#mytablaR6").trigger("update");
							$("#mytablaR7").trigger("update");
							$(this).removeClass('used');
							$('#ver_mas_pila').hide();																
						}else{
							alert("Error con la base de datos o usted no ha actualizado nada");
						}					
					}	
	});	 
});
$("#aceptarl").click( function(){	
	sel=$("#tablasell").val();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/produccion/actualizarl", 
			data: "id="+$("#idl").val()+"&fec="+$("#txtFechal").val()+"&can="+$("#canl").val()+"&vacio="+$("#cancelal").val(),
			success: 	
					function(msg){															
						if(msg!=0){			
							if(sel==1){$("#mytablaL1").trigger("update");}											
							if(sel==2){$("#mytablaL2").trigger("update");}
							if(sel==3){$("#mytablaL3").trigger("update");}
							if(sel==4){$("#mytablaL4").trigger("update");}
							if(sel==5){$("#mytablaL5").trigger("update");}
							if(sel==6){$("#mytablaL6").trigger("update");}
							if(sel==7){$("#mytablaL7").trigger("update");}
							if(sel==8){$("#mytablaL8").trigger("update");}
							if(sel==9){$("#mytablaL9").trigger("update");}
							if(sel==10){$("#mytablaL10").trigger("update");}
							if(sel==11){$("#mytablaL11").trigger("update");}
							if(sel==12){$("#mytablaL12").trigger("update");}
							if(sel==13){$("#mytablaL13").trigger("update");}
							if(sel==14){$("#mytablaL14").trigger("update");}
							if(sel==15){$("#mytablaL15").trigger("update");}
							if(sel==16){$("#mytablaL16").trigger("update");}
							$("#mytablaLS").trigger("update");
							$(this).removeClass('used');
							$('#ver_mas_larvi').hide();																
						}else{
							alert("Error con la base de datos o usted no ha actualizado nada");
						}					
					}	
	});	 
});
 
$(document).ready(function(){ 
	$('#pin').boxLoader({
            url:'<?php echo base_url()?>index.php/produccion/combo',
            equal:{id:'nom',value:'#pil'},
            select:{id:'numpila',val:'val'},
            onLoad:false            
     });
    
	$("#nse").val($("#rem1").val());
	$("#nse").change(function(){
		$("#cod").val($('input[id=nse]').val()+$('select[id=pil]').val()+'/'+$('input[id=p1]').val()+$('input[id=p2]').val()+$('input[id=p3]').val()+$('input[id=p4]').val()+'/'+$('select[id=mad]').val()+$('select[id=ori]').val());
	});
	$("#pil").change(function(){
		$("#cod").val($('input[id=nse]').val()+$('select[id=pil]').val()+'/'+$('input[id=p1]').val()+$('input[id=p2]').val()+$('input[id=p3]').val()+$('input[id=p4]').val()+'/'+$('select[id=mad]').val()+$('select[id=ori]').val());
	});
	$("#p1").change(function(){
		$("#cod").val($('input[id=nse]').val()+$('select[id=pil]').val()+'/'+$('input[id=p1]').val()+$('input[id=p2]').val()+$('input[id=p3]').val()+$('input[id=p4]').val()+'/'+$('select[id=mad]').val()+$('select[id=ori]').val());
	});
	$("#p2").change(function(){
		$("#cod").val($('input[id=nse]').val()+$('select[id=pil]').val()+'/'+$('input[id=p1]').val()+$('input[id=p2]').val()+$('input[id=p3]').val()+$('input[id=p4]').val()+'/'+$('select[id=mad]').val()+$('select[id=ori]').val());
	});
	$("#p3").change(function(){
		$("#cod").val($('input[id=nse]').val()+$('select[id=pil]').val()+'/'+$('input[id=p1]').val()+$('input[id=p2]').val()+$('input[id=p3]').val()+$('input[id=p4]').val()+'/'+$('select[id=mad]').val()+$('select[id=ori]').val());
	});
	$("#p4").change(function(){
		$("#cod").val($('input[id=nse]').val()+$('select[id=pil]').val()+'/'+$('input[id=p1]').val()+$('input[id=p2]').val()+$('input[id=p3]').val()+$('input[id=p4]').val()+'/'+$('select[id=mad]').val()+$('select[id=ori]').val());
	});
	$("#mad").change(function(){
		$("#cod").val($('input[id=nse]').val()+$('select[id=pil]').val()+'/'+$('input[id=p1]').val()+$('input[id=p2]').val()+$('input[id=p3]').val()+$('input[id=p4]').val()+'/'+$('select[id=mad]').val()+$('select[id=ori]').val());
	});
	$("#ori").change(function(){
		$("#cod").val($('input[id=nse]').val()+$('select[id=pil]').val()+'/'+$('input[id=p1]').val()+$('input[id=p2]').val()+$('input[id=p3]').val()+$('input[id=p4]').val()+'/'+$('select[id=mad]').val()+$('select[id=ori]').val());
	});
	$("#tabla_cod").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablacod',
		filters:['cmbPila'],
		sort:false,
	onRowClick:function(){
			$("#nse").val($(this).data('nse'));$("#idcod").val($(this).data('nse'));
        	$("#pil").val($(this).data('nom'));
        	$('#pin').boxLoader({
            	url:'<?php echo base_url()?>index.php/produccion/combo',
            	equal:{id:'nom',value:'#pil'},
            	select:{id:'numpila',val:'val'},
            });
        	$("#fec").val($(this).data('fec'));
        	$("#pls").val($(this).data('pls'));
       		$("#est").val($(this).data('est'));
       		$("#sali").val($(this).data('salas'));
       		$("#p1").val($(this).data('p1'));
       		$("#p2").val($(this).data('p2'));
       		$("#p3").val($(this).data('p3'));
       		$("#p4").val($(this).data('p4'));
       		$("#mad").val($(this).data('mad'));
       		$("#nau").val($(this).data('nau'));
       		$("#ori").val($(this).data('ori'));
       		$("#cod").val($(this).data('cod'));
       	},	
	onSuccess:function(){
			$('#tabla_cod tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center'); })
    		$('#tabla_cod tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_cod tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','center'); })
    		$('#tabla_cod tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_cod tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','center'); })
	    }, 	 
	});
	
	$('#pin2').boxLoader({
            url:'<?php echo base_url()?>index.php/produccion/combo',
            equal:{id:'nom',value:'#pil2'},
            select:{id:'numpila',val:'val'},
            onLoad:false            
     });
    
	$("#nse2").val($("#rem12").val());
	$("#nse2").change(function(){
		$("#cod2").val($('input[id=nse2]').val()+$('select[id=pil2]').val()+'/'+$('input[id=p12]').val()+$('input[id=p22]').val()+$('input[id=p32]').val()+$('input[id=p42]').val()+'/'+$('select[id=mad2]').val()+$('select[id=ori2]').val());
	});
	$("#pil2").change(function(){
		$("#cod2").val($('input[id=nse2]').val()+$('select[id=pil2]').val()+'/'+$('input[id=p12]').val()+$('input[id=p22]').val()+$('input[id=p32]').val()+$('input[id=p42]').val()+'/'+$('select[id=mad2]').val()+$('select[id=ori2]').val());
	});
	$("#p12").change(function(){
		$("#cod2").val($('input[id=nse2]').val()+$('select[id=pil2]').val()+'/'+$('input[id=p12]').val()+$('input[id=p22]').val()+$('input[id=p32]').val()+$('input[id=p42]').val()+'/'+$('select[id=mad2]').val()+$('select[id=ori2]').val());
	});
	$("#p22").change(function(){
		$("#cod2").val($('input[id=nse2]').val()+$('select[id=pil2]').val()+'/'+$('input[id=p12]').val()+$('input[id=p22]').val()+$('input[id=p32]').val()+$('input[id=p42]').val()+'/'+$('select[id=mad2]').val()+$('select[id=ori2]').val());
	});
	$("#p32").change(function(){
		$("#cod2").val($('input[id=nse2]').val()+$('select[id=pil2]').val()+'/'+$('input[id=p12]').val()+$('input[id=p22]').val()+$('input[id=p32]').val()+$('input[id=p42]').val()+'/'+$('select[id=mad2]').val()+$('select[id=ori2]').val());
	});
	$("#p42").change(function(){
		$("#cod2").val($('input[id=nse2]').val()+$('select[id=pil2]').val()+'/'+$('input[id=p12]').val()+$('input[id=p22]').val()+$('input[id=p32]').val()+$('input[id=p42]').val()+'/'+$('select[id=mad2]').val()+$('select[id=ori2]').val());
	});
	$("#mad2").change(function(){
		$("#cod2").val($('input[id=nse2]').val()+$('select[id=pil2]').val()+'/'+$('input[id=p12]').val()+$('input[id=p22]').val()+$('input[id=p32]').val()+$('input[id=p42]').val()+'/'+$('select[id=mad2]').val()+$('select[id=ori2]').val());
	});
	$("#ori2").change(function(){
		$("#cod2").val($('input[id=nse2]').val()+$('select[id=pil2]').val()+'/'+$('input[id=p12]').val()+$('input[id=p22]').val()+$('input[id=p32]').val()+$('input[id=p42]').val()+'/'+$('select[id=mad2]').val()+$('select[id=ori2]').val());
	});
	$("#tabla_cod2").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablacod2',
		filters:['cmbPila2'],
		sort:false,
	onRowClick:function(){
			$("#nse2").val($(this).data('nse'));$("#idcod2").val($(this).data('nse'));
        	$("#pil2").val($(this).data('nom'));
        	$('#pin2').boxLoader({
            	url:'<?php echo base_url()?>index.php/produccion/combo',
            	equal:{id:'nom',value:'#pil'},
            	select:{id:'numpila',val:'val'},
            });
        	$("#fec2").val($(this).data('fec'));
        	$("#pls2").val($(this).data('pls'));
       		$("#est2").val($(this).data('est'));
       		$("#sali2").val($(this).data('salas'));
       		$("#p12").val($(this).data('p1'));
       		$("#p22").val($(this).data('p2'));
       		$("#p32").val($(this).data('p3'));
       		$("#p42").val($(this).data('p4'));
       		$("#mad2").val($(this).data('mad'));
       		$("#nau2").val($(this).data('nau'));
       		$("#ori2").val($(this).data('ori'));
       		$("#cod2").val($(this).data('cod'));
       	},	
	onSuccess:function(){
			$('#tabla_cod2 tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center'); })
    		$('#tabla_cod2 tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_cod2 tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','center'); })
    		$('#tabla_cod2 tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_cod2 tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','center'); })
	    }, 	 
	});
	/*$("#nombre2").change(function(){
		alert($('input[id=nombre2]').val());
	});
	$(".nombre3").change(function(){
		alert($('input[class=nombre3]').val());
	});*/

	var f = new Date();
	$("#fecr").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$('#ver_mas_pila').hide();
	$('#ver_mas_larvi').hide();
	$("#tabla_ras1").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini1','fin1'],
		sort:false,	
		active:['Cancelacion',-1],
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
        	$("#sal").val($(this).data('sala'));$("#origen").val($(this).data('origen'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(1);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras1 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_ras1 tbody tr').map(function(){
	    		if($(this).data('origen')=='Ecuatoriana'){	    			
	    			$(this).css('background','yellow');
	    		}
	    	});
	    }, 
	});
	$("#tabla_ras11").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini1','fin1'],
		sort:false,	
		active:['Cancelacion',-1],
	});
	$("#tabla_ras2").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini2','fin2'],
		sort:false,	
		active:['Cancelacion',-1],
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
        	$("#sal").val($(this).data('sala'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(2);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras2 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_ras2 tbody tr').map(function(){
	    		if($(this).data('origen')=='Ecuatoriana'){	    			
	    			$(this).css('background','yellow');
	    		}
	    	});
	    },
	});
	$("#tabla_ras22").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini2','fin2'],
		sort:false,	
		active:['Cancelacion',-1],
	});
	$("#tabla_ras3").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini3','fin3'],
		sort:false,	
		active:['Cancelacion',-1],
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
        	$("#sal").val($(this).data('sala'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(3);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras3 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_ras3 tbody tr').map(function(){
	    		if($(this).data('origen')=='Ecuatoriana'){	    			
	    			$(this).css('background','yellow');
	    		}
	    	});
	    },
	});
	$("#tabla_ras33").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini3','fin3'],
		sort:false,	
		active:['Cancelacion',-1],
	});
	$("#tabla_ras4").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini4','fin4'],
		sort:false,
		active:['Cancelacion',-1],
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
        	$("#sal").val($(this).data('sala'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(4);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras4 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_ras4 tbody tr').map(function(){
	    		if($(this).data('origen')=='Ecuatoriana'){	    			
	    			$(this).css('background','yellow');
	    		}
	    	});
	    },	
	});
	
	$("#tabla_ras5").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini5','fin5'],
		sort:false,	
		active:['Cancelacion',-1],
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
        	$("#sal").val($(this).data('sala'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(5);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras5 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_ras5 tbody tr').map(function(){
	    		if($(this).data('origen')=='Ecuatoriana'){	    			
	    			$(this).css('background','yellow');
	    		}
	    	});
	    },
	});
	$("#tabla_ras55").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini5','fin5'],
		sort:false,	
		active:['Cancelacion',-1],
	});
	$("#tabla_ras6").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablarasA',
		filters:['hoy'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').hide();
        },
		onSuccess:function(){    
    		$('#tabla_ras6 tbody tr').map(function(){
	    		if($(this).data('pl1')=='Tot:'){	    				    			
	    			$(this).css('background','lightblue');
	    		}
	    	});	
	    	$('#tabla_ras6 tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
    	}, 	
	});
	$("#tabla_ras7").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablarasAT',
		filters:['hoy'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').hide();
        },
		onSuccess:function(){    
    		$('#tabla_ras7 tbody tr').map(function(){
	    		if($(this).data('pl1')=='Total:'){	    				    			
	    			$(this).css('background','lightblue');
	    		}
	    	});	
	    	$('#tabla_ras7 tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_ras7 tbody tr').map(function(){
	    		if($(this).data('pl1')=='Ecuatoriana:'){	    			
	    			$(this).css('background','yellow');
	    		}
	    	});
    	}, 
	});
	$("#tabla_lar1").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','100i','100f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(1);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar2").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','200i','200f'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(2);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},	
	});
	$("#tabla_lar3").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','300i','300f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(3);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar4").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','400i','400f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(4);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar5").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','500i','500f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(5);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar6").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','600i','600f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(6);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar7").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','700i','700f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(7);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar8").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','800i','800f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(8);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar9").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','900i','900f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(9);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar10").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','1000i','1000f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(10);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar11").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','1100i','1100f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(11);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar12").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','1200i','1200f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(12);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar13").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','1300i','1300f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(13);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar14").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','1400i','1400f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(14);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar15").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','1500i','1500f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(15);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar16").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalar',
		filters:['hoy','1600i','1600f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(16);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_larsie").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablalarsie',
		sort:false,	
	});
 	
});
</script>
