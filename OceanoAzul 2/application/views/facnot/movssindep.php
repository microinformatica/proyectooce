    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 80px 50px 20px; }
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: navy; }	
</style> 
<title>FacSinDep</title>
</head>
<body>
	<div id="header"> 
		<table style="width: 760px;">
			<tr>
				<td width="200px">
					<p style="font-size: 8px;"  >
						<img src="<?php echo base_url();?>assets/images/oceanoazul.jpg" width="200" height="60" border="0">
						SIA - <?= $usuario?> - <?= $perfil?> <br />
					</p>
				</td>	
				
				<td align="center" style="font-size: 14px; color: navy"><strong>Movimientos Generados en Facturas</strong>
					
				</td>			
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 760px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: white;">
				<td width="160px">www.acuicolaoceanoazul.mx</td>	
				<td width="490">
					Av. Reforma No.2007-B Int. 803, Col. Flamingos, Mazatlán, Sin. México, RFC:AQU-031003-CC9, CP:82149 - Tel:(669)985-64-46  
				</td>		
			</tr>
		</table> 
	</div>
	<?php
    //datos del cliente	
	  $this->db->where('idctefn =',$cliente);
	  $resultt = $this->db->get('clientesfn');
	  foreach($resultt->result() as $rowt):
		  $razon=$rowt->rasfn;
	  endforeach;
	?>	
	<br />
	Cliente: <?= $razon?>
	
	<br />
	<table border="1"  style="font-size: 9px;">
    	  <?= $tabla?>
	</table>

<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>