<?php $this->load->view('header_cv'); ?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
/*.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}*/
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	/*background: none;*/		
}   
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>

<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#entregas"><img src="<?php echo base_url();?>assets/images/menu/viatico.png" width="25" height="25" border="0"> Viaticos </a></li>					
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="entregas" class="tab_content" >
			<div id = "accordion" style="margin-top: -10px; margin-left: -10px; width: 958px;" >
					<h3 align="left" style="font-size: 12px" ><a href="#">  Registrados </a></h3>
					<div style="height: 350px;">				
						<div class="ajaxSorterDiv" id="tabla_via" name="tabla_via" style="height: 403px;margin-top: -21px; ">                
                		<div style=" text-align:left; font-size: 12px;" class='filter'  >
                		<div align="left">	                			
                			Destino <select name="cmbDestino" id="cmbDestino"  style="font-size: 11px;height: 25px; width: 120px">
										<option value="Todos">Todos</option>
										<?php	              							
           									$this->load->model('cargos_model');
											$data['result']=$this->viaticos_model->verDestino();
											foreach ($data['result'] as $row):?>
           									<option value="<?php echo $row->NomDes?>" ><?php echo $row->NomDes;?></option>
           								<?php endforeach;?>
   									</select>
       						Unidad <select id="cmbUnidad" style="font-size: 11px;height: 25px; width: 210px;">
       								<option value="Todos">Todos</option>
       							<?php	              							
           									$this->load->model('cargos_model');
											$data['result']=$this->viaticos_model->verUnidad();
											foreach ($data['result'] as $row):?>
           									<option value="<?php echo $row->NumUni?>" ><?php echo $row->NomUni;?></option>
           								<?php endforeach;?>
       						</select>    
						</div>	  
						</div>        			
                		<span class="ajaxTable" style="height: 315px; background-color: white;" >
                    	<table id="mytablaV" name="mytablaV" class="ajaxSorter" >
                        	<thead title="">                          		                        
                            	<th data-order = "NomDes" style="width: 80px" >Destino</th>
                            	<th data-order = "NomUni" style="width: 150px">Unidad</th>
                            	<th data-order = "Alimentacion" style="width: 50px; font-size: 10px">Alimentos</th>
                    			<th data-order = "Combustible" style="width: 50px; font-size: 10px">Combustible</th>
                    			<th data-order = "Casetas" style="width: 50px; font-size: 10px">Casetas</th>
                    			<th data-order = "Hospedaje" style="width: 50px; font-size: 10px">Hospedaje</th>
                    			<th data-order = "Fitosanitaria" style="width: 50px; font-size: 10px">Fitosanitaria</th>
                    			<th data-order = "totals" style="width: 70px" >Total</th>                                
                        	</thead>
                        	<tbody title="Seleccione para actualizar" style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="">        
	                    	<ul class="order_list"></ul>     
                   			 <form method="post" action="<?= base_url()?>index.php/viaticos/pdfrep" >  
                    			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />                                                                      
                        		<!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        		<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                             
 							</form>   
                		</div>                                      
            			</div>             	
            		
            	 	</div>             	 	        			        			
        			<h3 align="left" style="font-size: 12px"><a href="#" >Agregar, Actualizar o Eliminar</a></h3>           			
		       	 	<div style="height: 150px;">
		       	 		<table style="width: 750px;" border="2px">
							<thead style="background-color: #DBE5F1">
								<th colspan="6" height="15px" style="font-size: 20px">Datos del Viático
									<input size="8%" type="hidden"  name="id" id="id"/>													
								</th>
							</thead>								
						</table>
		       			<table style="width: 750px; margin-top: -15px" border="2px">
    						<thead style="background-color: #DBE5F1">
								<th colspan="6">
									Destino: <input size="20%" type="text" name="des" id="des" >
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									Unidad: <select name="uni" id="uni" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
           							$this->load->model('viaticos_model');
									$data['result']=$this->viaticos_model->verUnidad();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumUni;?>" ><?php echo $row->NomUni;?></option>
           							<?php endforeach;?>
   									</select>
									<br><br>Conceptos
								</th>
							</thead>
							<tbody style="background-color: #F7F7F7">
							<tr>
								<th><label style="margin: 2px; font-size: 14px" for="ali">Alimentación</label></th>
								<th><label style="margin: 2px; font-size: 14px" for="com">Combustible</label></th>
								<th><label style="margin: 2px; font-size: 14px;" for="cas" >Casetas</label></th>										
								<th><label style="margin: 2px; font-size: 14px" for="hos">Hospedaje</label></th>																										
								<th><label style="margin: 2px; font-size: 14px;" for="fit">Fitosanitaria</label></th>										
								<th><label style="margin: 2px; font-size: 14px;" for="sub">Subtotal</label></th>
							</tr>
							<form>
    						<tr style="background-color: white"> 
									<th style="font-size: 13px;">$ <input size="14%" type="text" name="ali" id="ali" style="text-align: right;"></th>
									<th style="font-size: 13px;">$ <input size="14%" type="text" name="com" id="com" style="text-align: right;"></th>
									<th style="font-size: 13px;">$ <input size="14%" type="text" name="cas" id="cas" style="text-align: right;"></th>
									<th style="font-size: 13px;">$ <input size="14%" type="text" name="hos" id="hos" style="text-align: right;"></th>
									<th style="font-size: 13px;">$ <input size="14%" type="text" name="fit" id="fit" style="text-align: right;"></th>									
									<th style="font-size: 13px;">$ <input size="14%" readonly="true" type="text" name="sub" id="sub" style="text-align: right;"></th>
							</tr>
							<tr>
								<th><label style="margin: 2px; font-size: 14px" for="ali">Observaciones</label></th>
								<th colspan="5" style="font-size: 13px;"><input size="100%" type="text" name="obs" id="obs" ></th>
							</tr>	
							</form> 
							</tbody>
						<tfoot style="background-color: #DBE5F1">
							<th colspan="6">
							<center>								
									<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
									&nbsp;<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
									&nbsp;<input style="font-size: 14px" type="submit" id="borrar" name="borrar" value="Borrar" />																	
							</center>
							</th>	
						</tfoot>   							
    				</table>
        		</div>
	 		</div>	
	 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#nuevo").click( function(){	
	$("#id").val('');	        
	$("#ali").val('');
	$("#com").val('');
	$("#cas").val('');
	$("#hos").val('');
	$("#fit").val('');	
	$("#des").val('');
	$("#uni").val('');
	$("#obs").val('');
	$("#sub").val('');		
 return true;
});
$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/viaticos/borrar", 
						data: "id="+$("#id").val(), //+"&usd="+$("#usd").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Viatico Eliminado correctamente");
										$("#nuevo").click();
										$("#mytablaV").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Viatico para poder Eliminarlo");
		$("#accordion").accordion( "activate",0 );
		return false;
	}	
});
$("#aceptar").click( function(){		
	des=$("#des").val();
	numero=$("#id").val();
	if( des!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/viaticos/actualizar", 
						data: "id="+$("#id").val()+"&des="+$("#des").val()+"&uni="+$("#uni").val()+"&ali="+$("#ali").val()+"&com="+$("#com").val()+"&cas="+$("#cas").val()+"&hos="+$("#hos").val()+"&fit="+$("#fit").val()+"&obs="+$("#obs").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevo").click();
										$("#mytablaV").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/viaticos/agregar", 
						data: "&des="+$("#des").val()+"&uni="+$("#uni").val()+"&ali="+$("#ali").val()+"&com="+$("#com").val()+"&cas="+$("#cas").val()+"&hos="+$("#hos").val()+"&fit="+$("#fit").val()+"&obs="+$("#obs").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Viatico registrado correctamente");
										$("#nuevo").click();
										$("#mytablaV").trigger("update");
										$("#cmbDestino").val('');
										$("#cmbUnidad").val('');										
										$("#accordion").accordion( "activate",0 );												
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Registre nombre del destino ");	
			$("#des").focus();
				return false;
		}
});
$(document).ready(function(){ 
	$("#tabla_via").ajaxSorter({
		url:'<?php echo base_url()?>index.php/viaticos/tabla',  
		filters:['cmbDestino','cmbUnidad'],
		sort:false,
		onRowClick:function(){
			if($(this).data('numdes')>'0'){
    			$("#accordion").accordion( "activate",1 );
    			$("#id").val($(this).data('numdes'));
       			$("#des").val($(this).data('nomdesa'));
				$("#uni").val($(this).data('numuni'));			
				$("#ali").val($(this).data('alimentacion'));
				$("#com").val($(this).data('combustible'));
				$("#cas").val($(this).data('casetas'));
				$("#hos").val($(this).data('hospedaje'));
				$("#fit").val($(this).data('fitosanitaria'));
				$("#obs").val($(this).data('obsdes'));
				$("#sub").val($(this).data('totals'));				
			}
    	},
    	 onSuccess:function(){
    		$('#tabla_via tbody tr').map(function(){
    	   		$(this).css('text-align','right');	
    	   });
    	   $('#tabla_via tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','left');  $(this).css('font-weight','bold');})
    	   $('#tabla_via tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','left');  $(this).css('font-weight','bold');})
    	},    
	});
		
});
</script>
