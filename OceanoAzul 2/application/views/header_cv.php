<!--<!DOCTYPE html>-->
<!DOCTYPE html PUBLIC
      "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="es">
    <head>
        <title>Oceano Azul</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/logo.png" />  
		<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>assets/css/reset.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url()?>assets/css/text.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url()?>assets/css/960.css' />
       
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/redmond/jquery-ui-1.8.21.custom.css" />
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui/css/redmond/jquery-ui-1.8.4.custom.css" />-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/main-rollup.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/login.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/style.css" />      
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/estilos.css" />      
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/formulario.css" />        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/Estilo.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ajaxSorterStyle/style.css">
		<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ajaxSorterStyle/style.css?ver=1.0">-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.loader.css" />
		<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/black-tie/jquery-ui-1.10.0.custom.css">-->

		
		
<!--		<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.js"></script>      -->
<!--		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-1.7.2.min.js"></script>
			<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui-1.8.21.custom.min.js"></script>-->
<!--     	<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-latest.js" ></script>-->
<!--		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui-1.8.1.custom.min.js"></script>-->
<!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.ajaxsorter.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.ui.datepicker.js"></script>-->		
		
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui-1.8.23.custom.min.js"></script>   
		<!--<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.tablesorter.js" ></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.tablesorter.pager.js"></script>-->              
		<!--<script type="text/javascript" src="<?php echo base_url()?>assets/js/md5-min.js"></script>-->
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.ajaxsorter.13.7.js"></script>
        
        
        <!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/acordeon.js"></script>-->            
 		<script type="text/javascript" src="<?php echo base_url();?>assets/js/Funcion.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/moneda.js"></script>	
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.loader.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.boxloader.js"></script>
		<!---->		
    </head>
    
    <body >
    	 
        <center>
            <div id="contenedor" >
                <div id="container" > <!-- la clase de este  no sirve pa' nada--></div>
                <center>
                    <div>
                        <div style="height:130px;margin-bottom:0px;border:0px"><img src="<?php echo base_url()?>assets/images/aoaencabezado.png" style="width: 960px"/></div>
                        <!--<div style="height:0px;margin-bottom:0px;border:0px"><img src="<?php echo base_url()?>assets/images/aqpcontenido2.png" style="width: 960px"/></div>-->
                        <div class="blockhead">
                            <div align="left">
                                <span style="display:inline-block; width:100%">
                                	<?php //$usuario="Jesus";?>
                                    <span style="text-align:left;float:left"><?= ($usuario)?'Bienvenido: '. $usuario . ' - ' . $perfil : "" ;?></span>
                                    
                                    <span style="text-align:right;float:right;margin:auto;width: 50%">
                                        <span >
                                            <? if($usuario) {?>
                                        <a id="salir" class="pointer"> Cerrar Sesi&oacute;n</a>
                                        <script>
            								$(document).ready(function(){
            									$('#salir').click(function(){
            										if(confirm("Desea cerrar sesión?")){
            											//window.location= "<?= base_url()?>index.php/login/log_out";
            											document.location.href="<?= base_url()?>index.php/login/log_out";
            										}	
            										return false;
            									});
            								});
            							</script>

                                        <!--Bienvenido-->
                                        <? } ?>
                                    </span>
                                </span>
                            </div>
                        </div> 
                    </div>
                </center>
