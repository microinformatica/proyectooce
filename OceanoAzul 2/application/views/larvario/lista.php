<?php $this->load->view('header_cv');?>

<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
</style>


<div class="container" id="principal" style="background-color:#FFFFFF;" >	
	<ul class="tabs">
		<strong>
		<li ><a href="#larvario"><img src="<?php echo base_url();?>assets/images/menu/8.png" width="25" height="25" border="0"> Larvicultura </a></li>
		<li ><a href="#exteriores"><img src="<?php echo base_url();?>assets/images/menu/10.png" width="25" height="25" border="0"> Exteriores </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="larvario" class="tab_content" style="height: 570px;" align="left">
			<table border="1" style="height: 568px;width: 940px; margin-top: -5px;">
				<tr>
					<td style="background-color: black; color: white">
						Número de Corrida:
                <input type="hidden" name="nrs" id="nrs">
                <input size="8%" type="hidden" name="rem1" id="rem1" value="<?php echo set_value('rem1',$uc); ?>">
				<input size="8%" type="hidden" name="ur" id="ur" value="<?php echo set_value('ur',$uc1); ?>">
				<input readonly="true" type="text" name="uc" id="uc" style="background-color: lightgray; width: 30px;text-align: center; border: none; color: navy; font-size: 20px;" value="<?php echo set_value('uc','rem1'); ?>">
				
						
					</td>
					<td colspan="30" style="background-color: black; color: white">
						Registro y Actualización de Datos de Siembra de Pilas de Corrida-Sala:
						<input readonly="true" type="text" name="corsal" id="corsal" style="width: 110px;text-align: center; border: none; color: navy; font-size: 20px;background-color: lightgray  ">
									
					</td>
					
				</tr>
				
				<tr>
					
					<td style="background-color: lightblue; ">
						Día	<input size="8%" type="text" name="fecsal" id="fecsal" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
                		Sala
						<select name="nsal" id="nsal" style="font-size: 12px;height: 23px;  margin-top: 1px; margin-left:-1px; width: 70px">
      					<option value="0"  >Sel.</option>
      						<?php	
        						$con=100;
								while($con<=2000){?>
								<option value="<?php echo $con;?>" ><?php echo $con;?></option>
							<?php	$con+=100; }?>
      					</select>
      					<?php if($usuario=="Jesus Benítez" || $usuario=="Enrique Sanchez O."){ ?>
						<input style="font-size: 16px;cursor: pointer; margin-left: -13px" type="submit" id="agregas" name="agregas" value="+" title="AGREGAR y ACTUALIZAR DATOS" />
						<input style="font-size: 16px;cursor: pointer; margin-left: -1px" type="submit" id="nuevasal" name="nuevasal" value="N" title="REGISTRAR NUEVA CORRIDA" />
						<?php } ?>
					</td>
					<td colspan="3"  style="text-align: center; background-color: lightblue">Sie</td>
					<td colspan="27" style="text-align: center; background-color: lightgray">Día
					<input type="hidden" name="nrpl" id="nrpl">	<input type="hidden"  name="nco" id="nco">
					<input size="8%" type="text" name="fecsp" id="fecsp" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
					Cantidad
					<input size="12%" type="text" name="cansie" id="cansie" style="text-align: right; margin-left: 1px; font-size: 13px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" >
					Maduración
					<select name="nmad" id="nmad" style="font-size: 12px;height: 23px;  margin-top: 1px; margin-left:-1px; width: 50px">
						<option value="0"  >Sel.</option>
						<option value="1"  >I</option>
						<option value="2"  >II</option>
					</select>	
					Pila
					<select name="tqp" id="tqp" style="font-size: 12px;height: 23px;  margin-top: 1px; margin-left:-1px; width: 50px">
						<option value="0"  >Sel.</option><option value="1"  >1</option><option value="2"  >2</option>
						<option value="3"  >3</option><option value="4"  >4</option><option value="5"  >5</option>
						<option value="6"  >6</option><option value="7"  >7</option><option value="8"  >8</option>
						<option value="9"  >9</option><option value="10"  >10</option><option value="11"  >11</option>
						<option value="12"  >12</option>
					</select>	
					<?php if($usuario=="Jesus Benítez" || $usuario=="Enrique Sanchez O."){ ?>
					   <input style="font-size: 16px;cursor: pointer; font-weight: bold" type="submit" id="agregapil" name="agregapil" value="+" title="AGREGAR y ACTUALIZAR DATOS" />
						<input style="font-size: 16px;cursor: pointer; font-weight: bold" type="submit" id="nuevopil" name="nuevopil" value="Nuevo" title="REGISTRAR NUEVA PILA" />
						<input style="font-size: 16px;cursor: pointer; font-weight: bold" type="submit" id="quitapil" name="quitapil" value="-" title="ELIMINAR PILA" />
					<?php } ?>
					</td>
				</tr>
				<tr align="center">
					<td rowspan="6">
					<div class="ajaxSorterDiv" id="tabla_SalLar" name="tabla_SalLar" style=" margin-top: -5px; margin-left: -10px; width: 250px">                
               				<span class="ajaxTable" style="margin-left: 0px; background-color: white; height: 500px;">
                				<table id="mytablaSalLar" name="mytablaSalLar" class="ajaxSorter" border="1"  >
                					<thead >
                						<tr>                            
                						<th rowspan="2" data-order = "corsal" >Sala</th>                                                                                    
                						<th colspan="2" style="text-align: center" >Siembra</th>
                						<th colspan="3" style="text-align: center" >Cosecha</th>
                						</tr>
                						<tr>
                						<th data-order = "dia" >Día</th>
                						<th data-order = "siembra" >Nauplios</th>	                            
                						<th data-order = "cosecha" >Postlarvas</th>
                						<th data-order = "soblar" >%</th>
                						</tr>
                					</thead>
                        			<tbody title="Seleccione para asignar salida" style="font-size: 11px">                                                 	  
                        			</tbody>
                    			</table>
                			</span> 
            			</div>
				</td>
				</tr>
				<tr>
					<td colspan="30">
						<div id='ver_informacion' class='hide' >
						<div class="ajaxSorterDiv" id="tabla_SalCos" name="tabla_SalCos" style="  margin-left: -5px; margin-top: -10px; width: 682px">                
               				<span class="ajaxTable" style="margin-left: 0px; background-color: white; height: 407px; width: 680px">
                				<div class="ajaxpager" style="margin-top: 1px; text-align: left">        
	                   				<ul style="width: 320px; text-align: left; font-weight: bold; margin-top: -5px" class="order_list">
	                   					Concentrado de Información
	                   					<button type="button" id="aligra" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Grafica</button>
	                   				</ul>                                            
    	               				<form method="post" action="<?= base_url()?>index.php/larvario/pdfrep/" >
    	               					<?php if($usuario=="Jesus Benítez" || $usuario=="Enrique Sanchez O."){ ?>
    	               						<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    			<?php }else{ ?>
                    	    				<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF"  src="<?= base_url()?>assets/img/sorter/pdf.png" />	
                    	    			<?php } ?>
                    	    			<input type="hidden" name="cosal" id="cosal" />
                    	    			<input type="hidden" name="imgcs" id="imgcs" />
                    	    			<input type="hidden" name="tablacos" value ="" class="htmlTable"/>                        		                        		                                                                          	    			
 									</form>
 						   		</div>	
                				<table id="mytablaSalCos" name="mytablaSalCos" class="ajaxSorter" border="1" style="margin-top: -20px">
                					<thead >
                						<tr>                            
                						<th colspan="4" style="font-size: 16px; font-weight: bold; text-align: center">Siembra</th>                                                                                    
                						<th colspan="5" style="font-size: 16px; font-weight: bold; text-align: center">Cosecha</th> 
                						<th colspan="6" style="font-size: 16px; font-weight: bold; text-align: center">Raceways</th>
                						</tr>
                						<tr>
                						<th data-order = "dias" >Día</th>
                						<th data-order = "mad" >Lab</th>
                						<th data-order = "tqp" >Pila</th>	                            
                						<th data-order = "cansie" >Cantidad</th>
                						<th style="width: 40px" data-order = "diac" >Día</th>
                						<th data-order = "pro" >Prom</th>
                						<!--
                						<th data-order = "pes" > Pesada</th>
                						<th data-order = "rep" > Repaño</th>
                						-->
                						<th data-order = "kgs" >Kgs</th>
                						<th data-order = "cancos" >Cantidad</th>
                						<th data-order = "sobpil" >Sob</th>
                						<th data-order = "te1" >Ext</th>
                						<th data-order = "ce1" >Cantidad</th>
                						<th data-order = "te2" >Ext</th>
                						<th data-order = "ce2" >Cantidad</th>
                						<th data-order = "te3" >Ext</th>
                						<th data-order = "ce3" >Cantidad</th>
                						</tr>
                					</thead>
                        			<tbody style="font-size: 11px; text-align: center">                                                 	  
                        			</tbody>
                    			</table>
                			</span> 
            			</div>
            			</div>
            			<div id='ver_aligra' class='hide' style="height: 390px" >
            				<div name="sobgraf" id="sobgraf" style="width: 680px; height: 380px; margin: 0 auto"></div>
            				<button type="button" id="aligra1" style="cursor: pointer; background-color: lightblue; margin-top: -20px" class="continuar used" >Cerrar Gráfica</button>
            			</div>
					</td>
				</tr>
				<tr>
					<td colspan="20" style="background-color: black; color: white">Registrar y Actualizar Datos de Cosecha
					<?php if($usuario=="Jesus Benítez" || $usuario=="Enrique Sanchez O."){ ?>
					<input style="font-size: 12px;cursor: pointer; font-weight: bold" type="submit" id="agregacos" name="agregacos" value="+ Agregar/Actualizar" title="AGREGAR y ACTUALIZAR DATOS" />
					<input style="font-size: 12px;cursor: pointer; font-weight: bold" type="submit" id="quitacos" name="quitacos" value="- Eliminar" title="ELIMINAR DATOS DE COSECHA DE PILA" />
					<?php } ?>
					</td>
					<td rowspan="4" colspan="5" style="background-color: black; color: white;font-size: 8px">
					E<br />X<br />T<br />E<br />R<br />I<br />O<br />R<br />E<br />S
					</td>
					<td colspan="2" style=" background-color: lightgray">Pila</td>
					<td colspan="3" style=" background-color: lightgray">Cantidad</td>
				</tr>
				<tr>
					<td colspan="3" style="text-align: left; background-color: lightblue;">Cos</td>
					<td rowspan="2" colspan="3" style="text-align: center; background-color: lightgray">Dia</td>
					<td colspan="2" style=" background-color: lightgray">Peso</td>
					<td colspan="2" style=" background-color: lightgray">1</td>
					<td colspan="2" style=" background-color: lightgray">2</td>
					<td colspan="2" style=" background-color: lightgray">3</td>
					<td colspan="6" style=" background-color: lightgray">Larvas <input size="8%" type="text" name="cancos" id="cancos" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -2px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" >
					</td>
					
					<td colspan="2">
						<select name="te1" id="te1" style="font-size: 10px;height: 22px; margin-top: -1px;">
							<option value="0"  >Sel.</option>
      						<?php $actual=1;
								while($actual <= 40){?>
							<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
								<?php  $actual+=1; } ?>  
								<option value="41">R1</option>	  									
							<option value="42">R2</option>
							<option value="43">R3</option>
							<option value="44">R4</option>    									
    					</select>
					</td>
					<td colspan="3"><input size="8%" type="text" name="ce1" id="ce1" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -5px" onKeyPress="return(currencyFormat(this,',','.',event,''));" ></td>
					
					
				</tr>
				<tr>
					<td colspan="3" style="text-align: center; background-color: lightblue">Pil</td>
					<td colspan="2" style=" background-color: lightgray">Kg</td>
					<td colspan="2"><input type="text" name="p1" id="p1" style="width: 40px;text-align: right; margin-left: -5px; font-size: 14px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" ></td>
					<td colspan="2"><input type="text" name="p2" id="p2" style="width: 40px;text-align: right; margin-left: -5px; font-size: 14px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" ></td>
					<td colspan="2"><input type="text" name="p3" id="p3" value="" style="width: 40px;text-align: right; margin-left: -5px; font-size: 14px" onKeyPress="return(currencyFormat(this,',','.',event,'P')); " ></td>
					<td colspan="3" style=" background-color: lightgray">Pesada</td>
					<td colspan="3" style=" background-color: lightgray">Repaño</td>
					<td colspan="2">
						<select name="te2" id="te2" style="font-size: 10px;height: 22px; margin-top: -1px;">
							<option value="0"  >Sel.</option>
      						<?php $actual=1;
								while($actual <= 40){?>
							<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
								<?php  $actual+=1; } ?>  
								<option value="41">R1</option>	  									
							<option value="42">R2</option>
							<option value="43">R3</option>
							<option value="44">R4</option>    									
    					</select>
					</td>
					<td colspan="3"><input size="8%" type="text" name="ce2" id="ce2" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -5px" onKeyPress="return(currencyFormat(this,',','.',event,''));" ></td>
				</tr>
				<tr>
					<td colspan="3" style="text-align: center; background-color: lightblue">
						<input readonly="true" type="text" name="pils" id="pils" style="background-color: lightblue; width: 30px;text-align: center; border: none; color: navy; font-size: 18px;" >
					</td>
					<td colspan="3"><input size="8%" type="text" name="feccp" id="feccp" class="fecha redondo mUp" readonly="true" style="text-align: center; margin-left: -5px" ></td>
					<td colspan="2" style=" background-color: lightgray">Org</td>
					<td colspan="2"><input type="text" name="o1" id="o1" style="width: 30px;text-align: center; font-size: 14px; margin-left: -5px"></td>
					<td colspan="2"><input type="text" name="o2" id="o2" style="width: 30px;text-align: center; font-size: 14px; margin-left: -5px"></td>
					<td colspan="2"><input type="text" name="o3" id="o3" style="width: 30px;text-align: center; font-size: 14px; margin-left: -5px" value="0"></td>
					<td colspan="3"><input size="4%" type="text" name="pesada" id="pesada" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -5px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" ></td>
					<td colspan="3"><input size="4%" type="text" name="repa" id="repa" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -5px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" ></td>
					<td colspan="2">
						<select name="te3" id="te3" style="font-size: 10px;height: 22px; margin-top: -1px;">
							<option value="0"  >Sel.</option>
      						<?php $actual=1;
								while($actual <= 40){?>
							<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
								<?php  $actual+=1; } ?>     
							<option value="41">R1</option>	  									
							<option value="42">R2</option>
							<option value="43">R3</option>
							<option value="44">R4</option>	 									
    					</select>
					</td>
					<td colspan="3"><input size="8%" type="text" name="ce3" id="ce3" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -5px" onKeyPress="return(currencyFormat(this,',','.',event,''));" ></td>
				</tr>
			</table>
		</div>
		
		
		
		
		<div id="exteriores" class="tab_content" style="height: 570px;" align="left">
			<table border="1" style="height: 568px;width: 940px; margin-top: -5px;">
				<tr>
					<td style="background-color: black; color: white">
						Sala
						<select name="nsale" id="nsale" style="font-size: 12px;height: 23px;  margin-top: 1px; margin-left:-1px; width: 70px">
      					<option value="0"  >Sel.</option>
      						<?php	
        						$con=100;
								while($con<=2000){?>
								<option value="<?php echo $con;?>" ><?php echo $con;?></option>
							<?php	$con+=100; }?>
      					</select>
					</td>
					<td colspan="30" style="background-color: black; color: white">
						Concentrado de Información Corrida-Sala:
						<input readonly="true" type="text" name="corsale" id="corsale" style="width: 110px;text-align: center; border: none; color: navy; font-size: 20px;background-color: lightgray  ">
						<input type="hidden"  name="procnco" id="procnco">
					</td>
				</tr>
				<tr>
					<td style="background-color: lightblue; ">
					</td>
					
					<td colspan="30"  style="text-align: center; background-color: lightblue">
					</td>
				</tr>
				<tr align="center">
					<td rowspan="6">
					<div class="ajaxSorterDiv" id="tabla_SalExt" name="tabla_SalExt" style=" margin-top: -5px; margin-left: -10px; width: 250px">                
               				<span class="ajaxTable" style="margin-left: 0px; background-color: white; height: 500px;">
                				<table id="mytablaSalExt" name="mytablaSalExt" class="ajaxSorter" border="1"  >
                					<thead >
                						<tr>                            
                						<th rowspan="2" data-order = "corsal" >Sala</th>                                                                                    
                						<th colspan="4" style="text-align: center" >Postlarvas en Raceways</th>
                						</tr>
                						<tr>
                						<!--<th data-order = "dia" >Día</th>-->
                						<th data-order = "siembrae" >Transf.</th>	                            
                						<th data-order = "cosechae" >Cosecha</th>
                						<th data-order = "soblare" >%</th>
                						</tr>
                					</thead>
                        			<tbody title="Seleccione para asignar salida" style="font-size: 11px">                                                 	  
                        			</tbody>
                    			</table>
                			</span> 
            			</div>
				</td>
				</tr>
				<tr>
					<td colspan="30">
						<div id='ver_informacione' class='hide' >
						<div class="ajaxSorterDiv" id="tabla_SalCose" name="tabla_SalCose" style="  margin-left: -5px; margin-top: -10px; width: 682px">                
               				<span class="ajaxTable" style="margin-left: 0px; background-color: white; height: 410px; width: 680px">
                				<div class="ajaxpager" style="margin-top: 1px; text-align: left">        
	                   				<ul style="width: 320px; text-align: left; font-weight: bold; margin-top: -5px" class="order_list">
	                   					
	                   					<button type="button" id="aligrae" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Grafica</button>
	                   				</ul>                                            
    	               				<form method="post" action="<?= base_url()?>index.php/larvario/pdfrep/" >
    	               					<?php if($usuario=="Jesus Benítez" || $usuario=="Enrique Sanchez O."){ ?>
    	               						<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    			<?php }else{ ?>
                    	    				<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF"  src="<?= base_url()?>assets/img/sorter/pdf.png" />	
                    	    			<?php } ?>
                    	    			<input type="hidden" name="cosal" id="cosal" />
                    	    			<input type="hidden" name="imgcs" id="imgcs" />
                    	    			<input type="hidden" name="tablacos" value ="" class="htmlTable"/>                        		                        		                                                                          	    			
 									</form>
 						   		</div>	
                				<table id="mytablaSalCose" name="mytablaSalCose" class="ajaxSorter" border="1" style="margin-top: -20px">
                					<thead >
                						<tr>                            
                						<th colspan="3" style="font-size: 16px; font-weight: bold; text-align: center">Transferencia</th>                                                                                    
                						<th colspan="6" style="font-size: 16px; font-weight: bold; text-align: center">Cosecha</th> 
                						</tr>
                						<tr>
                						<th data-order = "dias" >Día</th>
                						
                						<th data-order = "pilext" >Pila</th>	                            
                						<th data-order = "cansiee" >Cantidad</th>
                						<th style="width: 40px" data-order = "diac" >Día</th>
                						<th data-order = "pro" > Prom</th>
                						<th data-order = "pes" > Pesada</th>
                						<th data-order = "rep" > Repaño</th>
                						<th data-order = "cancos" >Cantidad</th>
                						<th data-order = "sobpil" >Sob</th>
                						</tr>
                					</thead>
                        			<tbody style="font-size: 12px; text-align: center">                                                 	  
                        			</tbody>
                    			</table>
                			</span> 
            			</div>
            			</div>
            			<div id='ver_aligrae' class='hide' style="height: 390px" >
            				<div name="sobgrafe" id="sobgrafe" style="width: 680px; height: 380px; margin: 0 auto"></div>
            				<button type="button" id="aligra1" style="cursor: pointer; background-color: lightblue; margin-top: -20px" class="continuar used" >Cerrar Gráfica</button>
            			</div>
					</td>
				</tr>
				<tr>
					<td colspan="20" style="background-color: black; color: white">Registrar y Actualizar Datos de Cosecha
					<?php if($usuario=="Jesus Benítez" || $usuario=="Enrique Sanchez O."){ ?>
					<input style="font-size: 12px;cursor: pointer; font-weight: bold" type="submit" id="agregacose" name="agregacos" value="+ Agregar/Actualizar" title="AGREGAR y ACTUALIZAR DATOS" />
					<input style="font-size: 12px;cursor: pointer; font-weight: bold" type="submit" id="quitacose" name="quitacos" value="- Eliminar" title="ELIMINAR DATOS DE COSECHA DE PILA" />
					<?php } ?>
					</td>
					<td colspan="10" style="background-color: black; color: white">Transferencia Exteriores
					
					</td>
				</tr>
				<tr>
					<td colspan="3" style="text-align: left; background-color: lightblue;">Cos</td>
					<td rowspan="2" colspan="3" style="text-align: center; background-color: lightgray">Dia</td>
					<td colspan="2" style=" background-color: lightgray">Peso</td>
					<td colspan="2" style=" background-color: lightgray">1</td>
					<td colspan="2" style=" background-color: lightgray">2</td>
					<td colspan="2" style=" background-color: lightgray">3</td>
					<td colspan="6" style=" background-color: lightgray">Larvas <input size="8%" type="text" name="cancos" id="cancos" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -2px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" >
					</td>
					<td rowspan="3" colspan="5" style=" background-color: black"></td>
					
					<td colspan="2" style=" background-color: lightgray">Pila</td>
					<td colspan="3" style=" background-color: lightgray">Cantidad</td>
					
				</tr>
				<tr>
					<td colspan="3" style="text-align: center; background-color: lightblue">Pil</td>
					<td colspan="2" style=" background-color: lightgray">Kg</td>
					<td colspan="2"><input type="text" name="p1" id="p1" style="width: 40px;text-align: right; margin-left: -5px; font-size: 14px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" ></td>
					<td colspan="2"><input type="text" name="p2" id="p2" style="width: 40px;text-align: right; margin-left: -5px; font-size: 14px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" ></td>
					<td colspan="2"><input type="text" name="p3" id="p3" value="" style="width: 40px;text-align: right; margin-left: -5px; font-size: 14px" onKeyPress="return(currencyFormat(this,',','.',event,'P')); " ></td>
					<td colspan="3" style=" background-color: lightgray">Pesada</td>
					<td colspan="3" style=" background-color: lightgray">Repaño</td>
					<td colspan="2">
						<select name="te1" id="te1" style="font-size: 10px;height: 22px; margin-top: -1px;">
							<option value="0"  >Sel.</option>
      						<?php $actual=1;
								while($actual <= 40){?>
							<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
								<?php  $actual+=1; } ?>      									
    					</select>
					</td>
					<td colspan="3"><input size="8%" type="text" name="ce1" id="ce1" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -5px" onKeyPress="return(currencyFormat(this,',','.',event,''));" ></td>
				</tr>
				<tr>
					<td colspan="3" style="text-align: center; background-color: lightblue">
						<input readonly="true" type="text" name="pils" id="pils" style="background-color: lightblue; width: 30px;text-align: center; border: none; color: navy; font-size: 18px;" >
					</td>
					<td colspan="3"><input size="8%" type="text" name="feccp" id="feccp" class="fecha redondo mUp" readonly="true" style="text-align: center; margin-left: -5px" ></td>
					<td colspan="2" style=" background-color: lightgray">Org</td>
					<td colspan="2"><input type="text" name="o1" id="o1" style="width: 30px;text-align: center; font-size: 14px; margin-left: -5px"></td>
					<td colspan="2"><input type="text" name="o2" id="o2" style="width: 30px;text-align: center; font-size: 14px; margin-left: -5px"></td>
					<td colspan="2"><input type="text" name="o3" id="o3" style="width: 30px;text-align: center; font-size: 14px; margin-left: -5px" value="0"></td>
					<td colspan="3"><input size="4%" type="text" name="pesada" id="pesada" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -5px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" ></td>
					<td colspan="3"><input size="4%" type="text" name="repa" id="repa" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -5px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" ></td>
					<td colspan="2">
						<select name="te2" id="te2" style="font-size: 10px;height: 22px; margin-top: -1px;">
							<option value="0"  >Sel.</option>
      						<?php $actual=1;
								while($actual <= 40){?>
							<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
								<?php  $actual+=1; } ?>    
							
    					</select>
					</td>
					<td colspan="3"><input size="8%" type="text" name="ce2" id="ce2" style="text-align: right; margin-left: 1px; font-size: 13px; margin-left: -5px" onKeyPress="return(currencyFormat(this,',','.',event,''));" ></td>
				</tr>
			</table>
		</div>
	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$('#aligra').click(function(){$(this).removeClass('used');$('#ver_informacion').hide();$('#ver_aligra').show();});
$('#aligra1').click(function(){$(this).removeClass('used');$('#ver_aligra').hide();$('#ver_informacion').show();});
$("#nuevapil").click( function(){	
	$("#nrpl").val($(this).data(''));
    $("#tqp").val('');
   return true;
});
$("#quitacos").click( function(){	
id=$("#nrpl").val();

				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/larvario/actualizarc", 
						data: "id="+$("#nrpl").val()+"&fec=NULL"+"&p1=0"+"&o1=0"+"&p2=0"+"&o2=0"+"&p3=0"+"&o3=0"+"&pesa="+"&repa=0"+"&cos=0",
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Registro de Datos Correctos");										
										$("#mytablaSalCos").trigger("update");
										$("#mytablaSalLar").trigger("update");
										$("#mytablaSalExt").trigger("update");
										$("#nuevapil").click();																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});			
		  
});

$("#agregacos").click( function(){	
id=$("#nrpl").val();fec=$("#feccp").val();pil1=$("#pils").val();nco1=$("#nco").val();
 if(nco1!=''){
	if(pil1!=''){
	  	if(fec!='0'){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/larvario/actualizarc", 
						data: "id="+$("#nrpl").val()+"&fec="+$("#feccp").val()+"&p1="+$("#p1").val()+"&o1="+$("#o1").val()+"&p2="+$("#p2").val()+"&o2="+$("#o2").val()+"&p3="+$("#p3").val()+"&o3="+$("#o3").val()+"&pesa="+$("#pesada").val()+"&repa="+$("#repa").val()+"&cos="+$("#cancos").val()+"&te1="+$("#te1").val()+"&ce1="+$("#ce1").val()+"&te2="+$("#te2").val()+"&ce2="+$("#ce2").val()+"&te3="+$("#te3").val()+"&ce3="+$("#ce3").val()+"&pil="+$("#tqp").val()+"&nco="+$("#nco").val()+"&sal="+$("#nsal").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Registro de Datos Correctos");										
										$("#mytablaSalCos").trigger("update");
										$("#mytablaSalLar").trigger("update");
										$("#mytablaSalExt").trigger("update");
										$("#nuevapil").click();																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});			
		  }else{
			alert("Error: Seleccione Fecha de Cosecha");$("#feccp").focus();return false;
	     }
	  }else{
	  		alert("Error: Seleccione Pila del Concentrado de Información");return false;
	  }	
  }else{
		alert("Error: Seleccione Sala");return false;
  }
});

$("#agregapil").click( function(){	
id=$("#nrpl").val();fec=$("#fecsp").val();can=$("#cansie").val();mad=$("#nmad").val();pil=$("#tqp").val();nco=$("#nco").val();
 if(nco!=''){
	if(fec!=''){
	  if(can!=''){
	  	if(mad!='0'){
	  		if(pil!='0'){
			if(id!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/larvario/actualizarp", 
						data: "id="+$("#nrpl").val()+"&fec="+$("#fecsp").val()+"&can="+$("#cansie").val()+"&mad="+$("#nmad").val()+"&pil="+$("#tqp").val()+"&nco="+$("#nco").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaSalCos").trigger("update");
										$("#mytablaSalLar").trigger("update");
										$("#mytablaSalExt").trigger("update");
										$("#nuevapil").click();																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/larvario/agregarp", 
						data: "&fec="+$("#fecsp").val()+"&can="+$("#cansie").val()+"&mad="+$("#nmad").val()+"&pil="+$("#tqp").val()+"&nco="+$("#nco").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Pila Registrada Con Éxito");
										$("#mytablaSalCos").trigger("update");
										$("#mytablaSalLar").trigger("update");
										$("#mytablaSalExt").trigger("update");
										$("#nuevapil").click();	
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});		
			}
			}else{
		alert("Error: Seleccione Pila");$("#tqp").focus();return false;
	  	}			
		}else{
		alert("Error: Seleccione Procedencia de Maduración");$("#nmad").focus();return false;
	  }		
	  }else{
		alert("Error: Seleccione Cantidad de Nauplios Sembrados");$("#cansie").focus();return false;
	  }		
	}else{
		alert("Error: Seleccione Fecha de Siembra");$("#fecsp").focus();return false;
	}
	}else{
		alert("Error: Seleccione Sala");return false;
	}	
});


$("#agregas").click( function(){	
	id=$("#nrs").val();fec=$("#fecsal").val();sal=$("#nsal").val();
	if(fec!=''){
	  if(sal!='0'){
			if(id!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/larvario/actualizars", 
						data: "id="+$("#nrs").val()+"&fec="+$("#fecsal").val()+"&sal="+$("#nsal").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaSalLar").trigger("update");
										$("#mytablaSalExt").trigger("update");
										$("#nuevasal").click();																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/larvario/agregars", 
						data: "&fec="+$("#fecsal").val()+"&sal="+$("#nsal").val()+"&uc="+$("#uc").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Sala Registrada Con Éxito");
										$("#mytablaSalLar").trigger("update");
										$("#mytablaSalExt").trigger("update");
										$("#nuevasal").click();	
										$("#uc").val(parseFloat($("#rem1").val())+1);
										$("#rem1").val(parseFloat($("#uc").val()));
										$("#ur").val(parseFloat($("#ur").val())+1);											
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});		
			}
	  }else{
		alert("Error: Seleccione Sala");$("#nsal").focus();return false;
	  }		
	}else{
		alert("Error: Seleccione Fecha de Inicio de Siembra");$("#fecsal").focus();return false;
	}
});



$("#nuevasal").click( function(){	
	$("#nrs").val($(this).data(''));
    $("#fecsal").val('');
    $("#nsal").val('');
    $("#uc").val($("#rem1").val());
   return true;
});
$("#nuevopil").click( function(){	
	$("#nrpl").val(''); $("#fecsp").val(''); $("#nmad").val(''); $("#tqp").val(''); //$("#nco").val('');
	$("#cansie").val('');
	$("#p1").val('');$("#p2").val('');$("#p3").val('');
	$("#o1").val('');$("#o2").val('');$("#o3").val('');
	$("#pesada").val('');$("#repa").val('');
	$("#te1").val('');$("#te2").val('');$("#te3").val('');
	$("#ce1").val('');$("#ce2").val('');$("#ce3").val('');
	$("#cancos").val('');$("#feccp").val('');
   return true;
});

$("#fecsal").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#fecsp").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#feccp").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  


$(document).ready(function(){ 
	$('#ver_informacion').show();$('#ver_informacione').show();
	$('#ver_aligra').hide();$('#ver_gra').hide();
	$('#ver_aligrae').hide();$('#ver_grae').hide();
	$("#nuevasal").click();$("#nuevopil").click();
	$("#uc").val($("#rem1").val());
	$("#tabla_SalLar").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvario/tablasallar',
		//filters:['hoy','100i','100f'],
		sort:false,	
		onRowClick:function(){
			$("#nrs").val($(this).data('nrs'));$("#nco").val($(this).data('nrs'));
			$("#nuevopil").click();
        	//$("#ncs").val($(this).data('ncs'));$("#uc").val($(this).data('ncs'));
        	$("#uc").val($(this).data('ncs'));
       		$("#fecsal").val($(this).data('fecsal'));
       		$("#nsal").val($(this).data('nsal'));
       		$("#corsal").val('C'+$(this).data('ncs')+'-'+$(this).data('nsal'));
       		sala='Corrida: ['+$(this).data('ncs')+'] Sala: ['+$(this).data('nsal')+']';
       		$("#cosal").val(sala);
       		$("#imgcs").val($(this).data('ncs')+'_'+$(this).data('nsal'));
       		$("#tabla_SalCos").ajaxSorter({
				url:'<?php echo base_url()?>index.php/larvario/tablapilsie',
				filters:['nco'],
				sort:false,	
				onRowClick:function(){
					if($(this).data('nrpl')>'0'){
					$("#nrpl").val($(this).data('nrpl'));$("#nco").val($(this).data('nco'));
        			$("#fecsp").val($(this).data('fecsp'));
       				$("#cansie").val($(this).data('cansie'));
       				$("#nmad").val($(this).data('nmad'));
       				$("#tqp").val($(this).data('tqp'));$("#pils").val($(this).data('tqp'));
       				$("#feccp").val($(this).data('feccp'));
       				$("#p1").val($(this).data('p1'));$("#o1").val($(this).data('o1'));
       				$("#p2").val($(this).data('p2'));$("#o2").val($(this).data('o2'));
       				$("#p3").val($(this).data('p3'));$("#o3").val($(this).data('o3'));
       				$("#pesada").val($(this).data('pesada'));
       				$("#repa").val($(this).data('repa'));
       				
       				if($(this).data('p1')==0 && $(this).data('p2')==0 && $(this).data('p3')==0){ 
       					$("#cancos").val($(this).data('cancos'));
       					$("#pesada").val('');
       					$("#repa").val('');
       				}else {$("#cancos").val('');}
       				if($(this).data('ce1')==0 && $(this).data('ce2')==0 && $(this).data('ce3')==0) { 
       					$("#te1").val('');
       					$("#ce1").val($(this).data('cancos1'));
       					$("#te2").val('');
       					$("#ce2").val('');
       					$("#te3").val('');
       					$("#ce3").val('');
       				}	
       				else{
       					$("#te1").val($(this).data('te1'));
       					$("#ce1").val($(this).data('ce1'));
       					$("#te2").val($(this).data('te2'));
       					$("#ce2").val($(this).data('ce2'));
       					$("#te3").val($(this).data('te3'));
       					$("#ce3").val($(this).data('ce3'));
       				}
       			 }
       			},
       			onSuccess:function(){
       				//$("#mytablaSalLar").trigger("update");
					$('#tabla_SalCos tbody tr').map(function(){
		    			if($(this).data('dias')=='Total') {	    				    			
	    					//$(this).css('background','lightgray');
	    					$(this).css('font-weight','bold');
	    			    	$(this).css('text-align','center');
	    			    	$(this).css('font-size','12px');
	    				}
		    		});
					$('#tabla_SalCos tbody tr td:nth-child(9)').map(function(){ 
	    				if($(this).html() <= 50.00 && $(this).html() != '' ) {
              				$(this).css("background-color", "red");$(this).css("color", "white");
        				}
        				if($(this).html() >= 50.01 && $(this).html() <= 69.99 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "white");
        				}
        				if($(this).html() >= 70.00 && $(this).html() != '' ) {
             				$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        				} 
        				$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		    });
	    			
		    		$('#tabla_SalCos tbody tr td:nth-child(3)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','center'); 
		    			if($(this).data('sobpil') <= 50.00  ) {
              				$(this).css("background-color", "red");$(this).css("color", "white");
        				}
        				if($(this).data('sobpil') >= 50.01 && $(this).data('sobpil') <= 69.99  ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "white");
        				}	
		    		});
		    	//grafica pila
		    	
       				g1=0;g2=0;g3=0;g4=0;g5=0;g6=0;g7=0;g8=0;g9=0;g10=0;g11=0;g12=0;gral=0;
	    			e1='';e2='';e3='';e4='';e5='';e6='';e7='';e8='';e9='';e10='';e11='';e12='';
	    			$('#tabla_SalCos tbody tr').map(function(){
	    				if($(this).data('sobp1')>0){g1=$(this).data('sobp1');e1=$(this).data('ex1');}
	    				if($(this).data('sobp2')>0){g2=$(this).data('sobp2');e2=$(this).data('ex2');}
	    				if($(this).data('sobp3')>0){g3=$(this).data('sobp3');e3=$(this).data('ex3');}
	    				if($(this).data('sobp4')>0){g4=$(this).data('sobp4');e4=$(this).data('ex4');}
	    				if($(this).data('sobp5')>0){g5=$(this).data('sobp5');e5=$(this).data('ex5');}
	    				if($(this).data('sobp6')>0){g6=$(this).data('sobp6');e6=$(this).data('ex6');}
	    				if($(this).data('sobp7')>0){g7=$(this).data('sobp7');e7=$(this).data('ex7');}
	    				if($(this).data('sobp8')>0){g8=$(this).data('sobp8');e8=$(this).data('ex8');}
	    				if($(this).data('sobp9')>0){g9=$(this).data('sobp9');e9=$(this).data('ex9');}
	    				if($(this).data('sobp10')>0){g10=$(this).data('sobp10');e10=$(this).data('ex10');}
	    				if($(this).data('sobp11')>0){g11=$(this).data('sobp11');e11=$(this).data('ex11');}
	    				if($(this).data('sobp12')>0){g12=$(this).data('sobp12');e12=$(this).data('ex12');}	
	    				if($(this).data('sobpil')>0){gral=$(this).data('sobpil');}    				
	    			});	
	    			
	    			var chart
	    			g1=parseFloat(g1);g2=parseFloat(g2);g3=parseFloat(g3);g4=parseFloat(g4);g5=parseFloat(g5);g6=parseFloat(g6);g7=parseFloat(g7);
					g8=parseFloat(g8);g9=parseFloat(g9);g10=parseFloat(g10);g11=parseFloat(g11);g12=parseFloat(g12);gral=parseFloat(gral);
					
					
					Highcharts.chart('sobgraf', {
    					title: {text: sala+' Sobrevivencia General: ['+gral+' %]'},
    					xAxis: {categories: ['1<br>'+e1, '2<br>'+e2, '3<br>'+e3, '4<br>'+e4, '5<br>'+e5, '6<br>'+e6, '7<br>'+e7, '8<br>'+e8, '9<br>'+e9, '10<br>'+e10, '11<br>'+e11, '12<br>'+e12]},
    					yAxis: {
        						min: 0,
        						max: 100,
        						//tickPixelInterval: 5,
        						title: {
            							text: 'Porcentaje (%)'
        						}
    					},
    					labels: {
        						items: [{
            						html: '',
            						style: {
                						left: '50px',
                						top: '18px',
                						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            						}
        						}]
    					},
   						series: [{
        							type: 'column',
        							name: 'Sob',
        							data: [g1, g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12],
        							zones: [{
            									value: 50,
            									color: 'red'
        									}, {
            									value: 70,
            									color: 'orange'
        									}, {
            									color: '#0F0'
        									}],
        							dataLabels: {
            								enabled: true,
            								rotation: -90,
            								color: '#FFFFFF',
            								align: 'right',
            								format: '{point.y:.1f}', // one decimal
            								y: 10, // 10 pixels down from the top
            								style: {
                									fontSize: '13px',
                									fontFamily: 'Verdana, sans-serif'
            								}
        							}
    							}, {
        							type: 'spline',
        							name: 'Estandar',
        							color: '#0F0',
        							data: [70, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70],
        							marker: {
            								lineWidth: 2,
            								lineColor: '#0F0',
            								fillColor: '#0F0'
        							}
    							}, {
       			 					type: 'spline',
        							name: 'Alerta',
        							color: 'orange',
        							data: [50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50],
        							marker: {
            								lineWidth: 2,
            								lineColor: 'orange',
            								fillColor: 'orange'
        							}
	    					}]
				});				
			  }, 
			});
       	},
       	onSuccess:function(){
       		$('#tabla_SalLar tbody tr td:nth-child(1)').map(function(){ 
					$(this).css('font-weight','bold');$(this).css('text-align','center');
			});
			$('#tabla_SalLar tbody tr td:nth-child(5)').map(function(){ 
					$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			if($(this).html() <= 50.00 && $(this).html() != '' ) {
              				$(this).css("background-color", "red");$(this).css("color", "white");
        			}
        			if($(this).html() >= 50.01 && $(this).html() <= 69.99 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "white");
        			}
        			if($(this).html() >= 70.00 && $(this).html() != '' ) {
             				$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        			} 
	    	});	    	
		}, 
	});
	$("#tabla_SalExt").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvario/tablasalext',
		filters:['nsale'],
		sort:false,	
		onRowClick:function(){
			$("#procnco").val($(this).data('procnco'));	
        	$("#corsale").val('C'+$(this).data('procnco')+'-'+$(this).data('nsal'));
       		//sala='Corrida: ['+$(this).data('ncs')+'] Sala: ['+$(this).data('nsal')+']';
       		//$("#cosal").val(sala);
       		//$("#imgcs").val($(this).data('ncs')+'_'+$(this).data('nsal'));
       		$("#tabla_SalCose").ajaxSorter({
				url:'<?php echo base_url()?>index.php/larvario/tablapilsiee',
				filters:['procnco'],
				sort:false,	
				onRowClick:function(){
					if($(this).data('nrpl')>'0'){
					
       				
       			 }
       			},
       			onSuccess:function(){
       				//$("#mytablaSalLar").trigger("update");
					$('#tabla_SalCose tbody tr').map(function(){
		    			if($(this).data('dias')=='Total') {	    				    			
	    					//$(this).css('background','lightgray');
	    					$(this).css('font-weight','bold');
	    			    	$(this).css('text-align','center');
	    			    	$(this).css('font-size','14px');
	    				}
		    		});
					$('#tabla_SalCose tbody tr td:nth-child(10)').map(function(){ 
	    				if($(this).html() <= 50.00 && $(this).html() != '' ) {
              				$(this).css("background-color", "red");$(this).css("color", "white");
        				}
        				if($(this).html() >= 50.01 && $(this).html() <= 69.99 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "white");
        				}
        				if($(this).html() >= 70.00 && $(this).html() != '' ) {
             				$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        				} 
        				$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		    });
	    			
		    		$('#tabla_SalCose tbody tr td:nth-child(3)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','center'); 
		    			if($(this).data('sobpil') <= 50.00  ) {
              				$(this).css("background-color", "red");$(this).css("color", "white");
        				}
        				if($(this).data('sobpil') >= 50.01 && $(this).data('sobpil') <= 69.99  ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "white");
        				}	
		    		});
		    			
								
			  }, 
			});
       	},
       	onSuccess:function(){
       		$('#tabla_SalExt tbody tr td:nth-child(1)').map(function(){ 
					$(this).css('font-weight','bold');$(this).css('text-align','center');
			});
			$('#tabla_SalExt tbody tr td:nth-child(5)').map(function(){ 
					$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			if($(this).html() <= 50.00 && $(this).html() != '' ) {
              				$(this).css("background-color", "red");$(this).css("color", "white");
        			}
        			if($(this).html() >= 50.01 && $(this).html() <= 69.99 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "white");
        			}
        			if($(this).html() >= 70.00 && $(this).html() != '' ) {
             				$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        			} 
	    	});	    	
		}, 
	});
	
});
</script>
