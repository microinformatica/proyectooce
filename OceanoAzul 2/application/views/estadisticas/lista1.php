<?php $this->load->view('header_cv'); 
 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/Estilo.css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Funcion.js"></script>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        

	.menu{
		display:inline-block;
	}
	.menu li{
		margin-right:15px;
		margin-top:15px;
		padding:0;
		float:left;
	}
	.menu li a img{
		width: 72px;
		display:inline-block;
	}
	.ui-dialog-titlebar-close{
		visibility: hidden;
	}
	.menu li a{
		display:block;
		text-align:center;
	}
	.menu li a:hover{
		opacity:1;
		text-decoration:none;
	}
	.menu li a div{
		display:inline-block;
	}
	

</style>

<div class="container" id="principal" style="background-color:#FFFFFF; height: 505px"  >	
	<form method="post" onsubmit='return false;'>
	<div id="accordion"  style="height:300px;">
		<h3 align="left"><a href=""></a></h3>
        <div  style="height:300px;"> 	
	<ul class="tabs" style="width: 895px; margin-top: -10px">
		<li style="font-size: 14px"><a href="#zona"><strong>
		<img src="<?php echo base_url();?>assets/images/menu/estadisti.png" width="25" height="25" border="0"> 	Estadistica		
		</strong> </a></li>
		<?php $cont=0; $saldot1=0; foreach ($result as $row): $saldot1+=$row->saldo;?>	
			<li style="width: 51.5px" >			
			<a href="#<?php echo $cont+=1; ?>" title="<?php echo $row->Zona; ?>" ><?php echo $cont; ?></a>
			</li>				
		<?php endforeach; ?>
					
	</ul>	
	<div class="tab_container" id="tabPrincipal" style="height: 411px; width: 895px;"   >
			<div id="zona" class="tab_content" style="height: 300px;" >
				<ul class="menu" >
					<table id="myTabla" cellspacing="1" class="tablesorter" width="250px" border="3px" style="margin-top: -5px"  >
					<thead>				
						<tr>
							<th style="font-size: 14px" width="100px" colspan="2">Zona</th>
							<th style="font-size: 14px" width="150px" colspan="2">Saldo</th>					
						</tr>
					</thead>
					<tbody>
						<?php $cont=0; $saldot=0; foreach ($result as $row): $saldot+=$row->saldo; ?>
						<tr>							
							<td style="font-size: 11px" width="20px"><strong><?php echo $cont+=1; ?></strong></td>
							<td style="font-size: 11px" width="80px"><strong><?php echo $row->Zona; ?></strong></td>
							<td style="font-size: 11px" width="100px" align="right"><?php echo '$ '.number_format($row->saldo, 2, '.', ',');?></td>
							<td style="font-size: 11px" width="50px" align="right"><?php echo number_format(($row->saldo/$saldot1)*100, 2, '.', ',').'%';?></td>
						</tr>
						<?php endforeach;?>
					</tbody>
					<tfoot>
						<tr>
							<th style="font-size: 14px" width="100px" colspan="2">Total</th>
							<th style="font-size: 14px" width="150px" colspan="2"><?php echo '$ '.number_format($saldot, 2, '.', ',');?></th>					
						</tr>
					</tfoot>
				</table>					
				</ul>
			</div>		
			<?php $zonsel="";$cont=0; $saldot=0; foreach ($result as $row): ?>	
			<div id="<?php echo $cont+=1; ?>" class="tab_content" style="overflow-x:hidden; height: 375px; width: 890px; margin-left: -8px" >
				<ul class="menu" >
					<table  cellspacing="1" class="tablesorter" border="2px" style="margin-top: -5px; ">
					<thead>				
						<tr>
							<th style="font-size: 14px" colspan="6"><?php echo $row->Zona; $zonsel=$row->Zona;?>								
								<button id="<?php echo 'I'.$cont;?>" name="<?php echo 'I'.$cont;?>" value="<?php echo 'I'.$row->Zona;?>"> Imprimir </button>
								<!--<a id="<?php echo $row->Zona;?>" name="Imprimir" value="<?php echo $row->Zona;?>">Imprimir</a>-->
								<script language="JavaScript" type="text/javascript">
								$("#<?php echo 'I'.$cont;?>").click(function(){	
									window.open("Imp_SalZon.php?&zona=<?php echo $row->Zona;?>");
									return false();
								});
								</script>	
								</th>
						</tr>	
					</thead>
					</table>
					<table id="<?php echo 'Z'.$cont;?>" cellspacing="1" class="tablesorter" border="2px" style="margin-top: -15px">
					<thead>				
						<tr>
        					<th align="center">Razon Social</th>
        					<th align="center">Saldo</th>
        					<th height="10" align="center">Fecha/Dias</th>
        					<th align="center">Importe</th>
        					<th align="center">Observaciones</th>
      					</tr>											
					</thead>
					<tbody>
						<?php
						$this->load->model('estadistica_model');
						$data['result']=$this->estadistica_model->zonaseleccionada($zonsel);$total=0;
						//$fecha($row->FecUD);						
						foreach ($data['result'] as $row): //$total+=$row->Saldo; 
						$d1=0; $dias=date($row->FecUD); $inc=$row->SI;
						if ($row->SI==-1) $saldo=0; else $saldo=$row->Saldo;
						$total+=$saldo;
						if ($dias>0){ $d1 = abs($row->diferencia);  } ?>
							<tr>							
								<?php if($usuario=="Efrain Lizárraga" or $usuario=="Jesus Benítez"){ ?>
										<td width="350px" >
										<a href="<?=base_url()?>index.php/estadistica/actualizar/<?=$row->Numero?>"><?php echo $row->Razon ?><?php ?></a>
										</td>
									<?php } else {?> 
										<td width="350px" style="<?php if($inc==-1){ ?> color:#FF0000 <?php } else {if($d1>=60){ ?> color:navy <?php }}?>">
										<?php echo $row->Razon ?><?php ?>
										</td> 
									<?php }?>								
								<td width="80px" style="<?php if($inc==-1){ ?> color:#FF0000 <?php } else {if($d1>=60){ ?> color:navy <?php }}?>" align="right"><?php echo '$ '.number_format($row->Saldo, 2, '.', ',');?></td>										
								<td width="80px" style="<?php if($inc==-1){ ?> color:#FF0000 <?php } else {if($d1>=60){ ?> color:navy <?php }}?>" ><?php if($row->ImpUD>0) echo date("d-m-Y",strtotime($row->FecUD))."</br>[".$d1."]"; ?></td>			
								<td width="80px" style="<?php if($inc==-1){ ?> color:#FF0000 <?php } else {if($d1>=60){ ?> color:navy <?php }}?>" align="right"><?php if($row->ImpUD>0) echo '$ '.number_format($row->ImpUD, 2, '.', ',');?></td>
								<td width="300px" style="<?php if($inc==-1){ ?> color:#FF0000 <?php } else {if($d1>=60){ ?> color:navy <?php }}?>" ><?php echo $row->ObsUD ?></td>
							</tr>
						<?php endforeach;	?>
					</tbody>
					<tfoot>
						<tr>
							<th >Total</th>
							<th width="80px" align="right"><?php echo '$ '.number_format($total, 2, '.', ',');?></th>
							<th colspan="3"  >
								
							</th>					
						</tr>
					</tfoot>
					</table>	
				</ul> 
					
			</div>		
			<?php endforeach;?>		
	</div>
	</div>
	<h3 align="left"><a href="">Actualizar</a></h3>
    <div  style="height:350px;">
    	 
     </div>   	
	</div>
	</form>		
</div>



<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#tabla_dep").ajaxSorter({
	url:'<?php echo base_url()?>index.php/depositos/tabla',  
	//filters:['dpDesde','dpHasta'],
    //active:array(['Tipo',3],['Tipo',2]),
    //active:['Recibida','√'],
    //active:['Tipo',2],                 
    onRowClick:function(){
        $("#accordion").accordion( "activate",1 );
       	$("#id").val($(this).data('nd'));
       	$("#txtFecha").val($(this).data('fecha'));
       	$("#zon").val($(this).data('zona'));
		$("#nombre").val($(this).data('razon'));
		$("#mn").val($(this).data('pesos'));
		$("#usd").val($(this).data('imported'));
		$("#tc").val($(this).data('tc'));
		$("#cta").val($(this).data('cuenta'));
		$("#lstCiclo").val($(this).data('aplicar'));
		$("#obs").val($(this).data('obs'));		
		$("#numcli").val($(this).data('nrc'));		
    }   
});

$("#nuevo").click( function(){	
	$("#id").val('');
	$("#txtFecha").val('');
    //$("#rem").val($("#rem1").val());    
	$("#nombre").val('');
	$("#zon").val('');
	$("#mn").val('');
	$("#usd").val('');
	$("#tc").val('');
	$("#cta").val('');
	$("#obs").val('');	
	$("#lstCiclo").val('');
	$("#numcli").val('');	
	$("#nombre").focus();
 return true;
}
)
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}

$("#aceptar").click( function(){	
	nombre=$("#nombre").val();
	fec=$("#txtFecha").val();
	mn=$("#mn").val();
	usd=$("#usd").val();
	numero=$("#id").val();
	if(mn==""){ mn=0; }
	if(usd==""){ usd=0; }
	if(nombre!=''){
		if( fec!=''){
		  if(mn!=0 || usd!=0){	
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/depositos/actualizar", 
						data: "id="+$("#id").val()+"&fec="+$("#txtFecha").val()+"&usd="+$("#usd").val()+"&tc="+$("#tc").val()+"&cta="+$("#cta").val()+"&obs="+$("#obs").val()+"&mn="+$("#mn").val()+"&nrc="+$("#numcli").val()+"&ciclo="+$("#lstCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevo").click();
										$("#mytablaD").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/depositos/agregar", 
						data: "&fec="+$("#txtFecha").val()+"&usd="+$("#usd").val()+"&tc="+$("#tc").val()+"&cta="+$("#cta").val()+"&obs="+$("#obs").val()+"&mn="+$("#mn").val()+"&nrc="+$("#numcli").val()+"&ciclo="+$("#lstCiclo").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Depósito registrado correctamente");
										$("#nuevo").click();
										$("#mytablaD").trigger("update");
										$("#accordion").accordion( "activate",0 );												
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
			}else{
		alert("Error: Necesita registrar cantidad en Pesos o Dólares");
		$("#mn").focus();
		return false;
		}			
		}else{
			alert("Error: Fecha no válida");	
			$("#txtFecha").focus();
				return false;
		}
	}else{
		alert("Error: Nombre de Cliente no válido");
		$("#nombre").focus();
			return false;
	}
	
	 // return false;
});

$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

$(document).ready(function(){        
        //$("#tabs").tabs();$("button").button();
        $("#nuevo").click();
        //$("input[name=#rblugar]:checked").val('2');
        $("#tabla_cli").ajaxSorter({
			url:'<?php echo base_url()?>index.php/clientes/tabla',  
			
		});
});
</script>