<?php $this->load->view('header_cv'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/Estilo.css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Funcion.js"></script>



<style>
	.menu{
		display:inline-block;
	}
	.menu li{
		margin-right:15px;
		margin-top:15px;
		padding:0;
		float:left;
	}
	.menu li a img{
		width: 72px;
		display:inline-block;
	}
	.ui-dialog-titlebar-close{
		visibility: hidden;
	}
	.menu li a{
		display:block;
		text-align:center;
	}
	.menu li a:hover{
		opacity:1;
		text-decoration:none;
	}
	.menu li a div{
		display:inline-block;
	}
	

</style>

<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF"  >	
	<form method="post" onsubmit='return false;'>
		
	<ul class="tabs">
		<li style="font-size: 26px"><a href="#zona"><strong>
		<img src="<?php echo base_url();?>assets/images/menu/estadisti.png" width="25" height="25" border="0"> 	Estadistica		
		</strong> </a></li>
		<?php $cont=0; $saldot1=0; foreach ($result as $row): $saldot1+=$row->saldo;?>	
			<li style="width: 51.5px" >			
			<a href="#<?php echo $cont+=1; ?>" title="<?php echo $row->Zona; ?>" ><?php echo $cont; ?></a>
			</li>				
		<?php endforeach; ?>
					
	</ul>	
	<div class="tab_container" id="tabPrincipal" >
			<div id="zona" class="tab_content" style="overflow-x:hidden;" >
				<ul class="menu" >
					<table id="myTabla" cellspacing="1" class="tablesorter" width="250px" border="3px" style="margin-top: -5px" >
					<thead>				
						<tr>
							<th style="font-size: 14px" width="100px" colspan="2">Zona</th>
							<th style="font-size: 14px" width="150px" colspan="2">Saldo</th>					
						</tr>
					</thead>
					<tbody>
						<?php $cont=0; $saldot=0; foreach ($result as $row): $saldot+=$row->saldo; ?>
						<tr>							
							<td style="font-size: 14px" width="20px"><strong><?php echo $cont+=1; ?></strong></td>
							<td style="font-size: 14px" width="80px"><strong><?php echo $row->Zona; ?></strong></td>
							<td style="font-size: 14px" width="100px" align="right"><?php echo '$ '.number_format($row->saldo, 2, '.', ',');?></td>
							<td style="font-size: 14px" width="50px" align="right"><?php echo number_format(($row->saldo/$saldot1)*100, 2, '.', ',').'%';?></td>
						</tr>
						<?php endforeach;?>
					</tbody>
					<tfoot>
						<tr>
							<th style="font-size: 14px" width="100px" colspan="2">Total</th>
							<th style="font-size: 14px" width="150px" colspan="2"><?php echo '$ '.number_format($saldot, 2, '.', ',');?></th>					
						</tr>
					</tfoot>
				</table>					
				</ul>
			</div>		
			<?php $zonsel="";$cont=0; $saldot=0; foreach ($result as $row): ?>	
			<div id="<?php echo $cont+=1; ?>" class="tab_content"  >
				<ul class="menu" >
					<table  cellspacing="1" class="tablesorter" border="2px" style="margin-top: -5px">
					<thead>				
						<tr>
							<th style="font-size: 14px" colspan="6"><?php echo $row->Zona; $zonsel=$row->Zona;?>								
								<button id="<?php echo 'I'.$cont;?>" name="<?php echo 'I'.$cont;?>" value="<?php echo 'I'.$row->Zona;?>"> Imprimir </button>
								<!--<a id="<?php echo $row->Zona;?>" name="Imprimir" value="<?php echo $row->Zona;?>">Imprimir</a>-->
								<script language="JavaScript" type="text/javascript">
								$("#<?php echo 'I'.$cont;?>").click(function(){	
									window.open("Imp_SalZon.php?&zona=<?php echo $row->Zona;?>");
									return false();
								});
								</script>	
								</th>
						</tr>	
					</thead>
					</table>
					<table id="<?php echo 'Z'.$cont;?>" cellspacing="1" class="tablesorter" border="2px" style="margin-top: -15px">
					<thead>				
													
						<tr>
        					<th align="center">Razon Social</th>
        					<th align="center">Saldo</th>
        					<th height="10" align="center">Fecha/Dias</th>
        					<th align="center">Importe</th>
        					<th align="center">Observaciones</th>
      					</tr>											
					</thead>
					<tbody>
						<?php
						$this->load->model('estadistica_model');
						$data['result']=$this->estadistica_model->zonaseleccionada($zonsel);$total=0;
						//$fecha($row->FecUD);						
						foreach ($data['result'] as $row): //$total+=$row->Saldo; 
						$d1=0; $dias=date($row->FecUD); $inc=$row->SI;
						if ($row->SI==-1) $saldo=0; else $saldo=$row->Saldo;
						$total+=$saldo;
						if ($dias>0){ $d1 = abs($row->diferencia);  } ?>
							<tr height="30px">							
								<?php if($usuario=="Efrain Lizárraga" or $usuario=="Jesus Benítez"){ ?>
										<td width="350px" >
										<a href="<?=base_url()?>index.php/estadistica/actualizar/<?=$row->Numero?>"><?php echo $row->Razon ?><?php ?></a>
										</td>
									<?php } else {?> 
										<td width="350px" style="<?php if($inc==-1){ ?> color:#FF0000 <?php } else {if($d1>=60){ ?> color:navy <?php }}?>">
										<?php echo $row->Razon ?><?php ?>
										</td> 
									<?php }?>								
								<td width="80px" style="<?php if($inc==-1){ ?> color:#FF0000 <?php } else {if($d1>=60){ ?> color:navy <?php }}?>" align="right"><?php echo '$ '.number_format($row->Saldo, 2, '.', ',');?></td>										
								<td width="80px" style="<?php if($inc==-1){ ?> color:#FF0000 <?php } else {if($d1>=60){ ?> color:navy <?php }}?>" ><?php if($row->ImpUD>0) echo $row->FecUD."</br>[".$d1."]"; ?></td>			
								<td width="80px" style="<?php if($inc==-1){ ?> color:#FF0000 <?php } else {if($d1>=60){ ?> color:navy <?php }}?>" align="right"><?php if($row->ImpUD>0) echo '$ '.number_format($row->ImpUD, 2, '.', ',');?></td>
								<td width="300px" style="<?php if($inc==-1){ ?> color:#FF0000 <?php } else {if($d1>=60){ ?> color:navy <?php }}?>" ><?php echo $row->ObsUD ?></td>
							</tr>
						<?php endforeach;	?>
					</tbody>
					<tfoot>
						<tr>
							<th colspan="2">Total</th>
							<th width="80px" align="right"><?php echo '$ '.number_format($total, 2, '.', ',');?></th>
							<th colspan="2"  >
								<div  id="<?php echo $cont;?>"  class="pager" style="float: right; height: 22px;" >
									<form style="margin-top: -7px">
										<img style="margin-top: 0px" src="<?php echo base_url()?>assets/addons/pager/icons/first.png" class="first"/>
										<img src="<?php echo base_url()?>assets/addons/pager/icons/prev.png" class="prev"/>
										<input type="text" class="pagedisplay" size="2px" readonly="true" style=""/>
										<img src="<?php echo base_url()?>assets/addons/pager/icons/next.png" class="next"/>
										<img src="<?php echo base_url()?>assets/addons/pager/icons/last.png" class="last"/>
										<select style="margin-top: 1px;  height: 5px; visibility: hidden" class="pagesize" size="1" >
											<option selected="selected" value="10">10</option>													
										</select>
									</form>	
								</div>
								<script>
									$(document).ready(function(){ 			
										$("#<?php echo 'Z'.$cont;?>").tablesorter ({headers:{2: {sorter: false},3: {sorter: false},4: {sorter: false}}, widthFixed: true, widgets: ['zebra']}).tablesorterPager ({container: $("#<?php echo $cont;?>")});
    								});
								</script>		
							</th>					
						</tr>
					</tfoot>
					</table>	
				</ul> 
					
			</div>		
			<?php endforeach;?>		
	</div>
	</form>		
</div>
</div>
<?php $this->load->view('footer_cv'); ?>
	