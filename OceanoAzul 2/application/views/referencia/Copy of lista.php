<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}

</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>	
		<li><a href="#gral" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/bancomer.png" width="25" height="25" border="0"> Referencias Bancarias </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px;" >
		<div id="gral" class="tab_content" style="margin-top: -5px" >
			<table border="2">
				<tr>
					<th  colspan="6">
						<div class="ajaxSorterDiv" id="tabla_ref" name="tabla_ref" style="width: 665px;"  >                 			
							<div class="ajaxpager" style=" font-size: 12px;  text-align: left"> 
								<form id="data_tablef" action="<?= base_url()?>index.php/referencia/pdfrepdetsal" method="POST" >							
    	    	            		<ul class="order_list" style="width: 390px; ">
    	    	            			Estado:<select name="origens" id="origens" style="font-size: 11px;height: 25px;  margin-top: 1px;">
   												<option value="0" >Todos</option>
          											<?php	
          												$data['result']=$this->referencia_model->verEdo();
														foreach ($data['result'] as $row):?>
           													<option value="<?php echo $row->idedo;?>" ><?php echo $row->nomedo;?></option>
           										<?php endforeach;?>
   											</select>
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: 5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablasala" value ="" class="htmlTable"/> 
								</form>   
                			</div>
							<span class="ajaxTable" style="height: 330px; margin-top: -15px; background-color: white " >
                    			<table id="mytablaRef" name="mytablaRef" class="ajaxSorter" border="1" style="width: 645px" >
                        			<thead>                            
                        				<th style="text-align: center" data-order = "idedo" >No.</th>
                        				<th style="text-align: center" data-order = "nomedo" >Estado</th>
                            			<th style="text-align: center" data-order = "nomzon" >Zona</th>
                            			<th style="text-align: center" data-order = "ref1" >Referencia</th>
                            			<th style="text-align: center" data-order = "ref2" >Referencia</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			       
             			</div>
					</th>
					
				</tr>
				<tr>
					<th style="text-align: center;background-color: gray; color: white" >Estado<input type="hidden" size="3%" name="ids" id="ids"></th>
					<th style="text-align: center;background-color: gray; color: white">Zona</th>
					<th style="text-align: center;background-color: gray; color: white">Referencia 1</th>
   					<th style="text-align: center;background-color: gray; color: white">Referencia 2</th>
   					
				</tr>
				<tr>
					<th style="text-align: center;background-color: gray; color: white">
						<select name="sala" id="sala" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<option value="0" >Seleccione</option>
          					<?php	
          					//$this->load->model('edoctatec_model');
          					$data['result']=$this->referencia_model->verSala();
							foreach ($data['result'] as $row):?>
           					<option value="<?php echo $row->Sala;?>" ><?php echo $row->Sala;?></option>
           					<?php endforeach;?>
   						</select>
					</th>
					<th style="text-align: center;background-color: gray; color: white">
						<select name="origenes" id="origenes" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
							<option value="Todos" >Seleccione</option>
   							<option value="Aquapacific" >Aquapacific</option>
   							<option value="Ecuatoriana" >Ecuatoriana</option>
   						</select>
					</th>
					<th style="text-align: center;background-color: gray; color: white"><input size="6%"  type="text" name="cor" id="cor" style="text-align: center"> 
						
						
						</th>
					<th style="text-align: center;background-color: gray; color: white"><input size="10%" type="text" name="fecs" id="fecs" class="fecha redondo mUp" readonly="true" style="text-align: center;" >	</th>
					<?php $mesact=date("m");
						if((date("m")>=3) && (date("m")<=9)){ $val=19;} else {$val=20;}
					 ?>
					<!--
					<th style="text-align: center;background-color: gray; color: white"><input size="2%"  type="text" name="dias" id="dias" style="text-align: center" value="<?php echo set_value('dias',$val); ?>"></th>
					-->
					<th style="text-align: center;background-color: gray; color: white"><input size="10%" type="text" name="fect" id="fect" class="fecha redondo mUp" readonly="true" style="text-align: center;" > </th>
					<th style="text-align: center;background-color: gray; color: white"><input size="6%"  type="text" name="cans" id="cans" style="text-align: center"></th>
				</tr>
				<tr>
					<th style="text-align: center; background-color: gray" colspan="6">
						<input type="submit" id="snuevo" name="snuevo" value="Nuevo"  />
						<input type="submit" id="saceptar" name="saceptar" value="Guardar" />
						<input type="submit" id="sborrar" name="sborrar" value="Borrar" />
						
					</th>
				</tr>
				
			</table>
		</div>		
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#OrigenMes").change( function(){
	$("#feccd").change();
 return true;
})
$("#cmbMesT").change( function(){
	var f = new Date();
	$("#feccd").val(f.getFullYear() + "-" + ($("#cmbMesT").val()) + "-" + f.getDate());
	$("#feccd").change();
 return true;
})
$("#feccd").change( function(){
	//busca la cantidad de acuerdo al dia y el tipo de postlarva
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/referencia/buscarcd", 
			data: "fec="+$("#feccd").val()+"&tip="+$("#OrigenMes").val()+"&cic="+$("#cmbCiclo").val(),
			success: resultado,	
	});	
});

function resultado (datos) {
  obj=null;
  var obj = jQuery.parseJSON( datos );
  if(obj!=null){
  	 //alert(msg);
  	if(obj.can != ''){
  		//alert(obj.can);
  		$("#cancd").val(obj.can);	
  		$("#idcd").val(obj.id);
  		return;
  	}else{
  		$("#cancd").val('');
  		$("#idcd").val('');
  		return;
  	}
  }
};
$("#quita").click( function(){	
	numero=$("#idcd").val();can=$("#cancd").val();
	if(numero!='' && can!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/quitarcor", 
						data: "id="+$("#idcd").val()+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaTM").trigger("update");
										$("#cancd").val('');
   										$("#idcd").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: intenta eliminar Corte Inexistente");
		return false;
	}
});
$("#agrega").click( function(){	
	can=$("#cancd").val();
	numero=$("#idcd").val();
	 if(can!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/actualizarcor", 
						data: "id="+$("#idcd").val()+"&fec="+$("#feccd").val()+"&can="+$("#cancd").val()+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaTM").trigger("update");
										$("#cancd").val('');
   										$("#idcd").val('');
   										$("#feccd").change();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/agregarcor", 
						data: "&fec="+$("#feccd").val()+"&can="+$("#cancd").val()+"&cic="+$("#cmbCiclo").val()+"&tip="+$("#OrigenMes").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaTM").trigger("update");
										$("#cancd").val('');
   										$("#idcd").val('');
   										$("#feccd").change();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		
		}
	}else{
		alert("Error: Necesita Registrar La Cantidad Actual de Corte");
		$("#cancd").focus();
		return false;
	}
	
});

$("#fecpd").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		//$("#feca").val( selectedDate );
		$("#mytablaProg").trigger("update");		
	}
});
$("#fecSi").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		//$("#feca").val( selectedDate );
		$("#mytablaSalas").trigger("update");		
	}
});
$("#feccd").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#feca").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#fecs").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#fect").datepicker( "option", "minDate", selectedDate );
		//$("#fect").val(f.getFullYear(  ) + "-" + (f.getMonth() ) + "-" + f.getDate());
		//$("#fect").datepicker($("#fecs").datepicker());
		//$("#mytablaPres").trigger("update");		
	}	
});
$("#fect").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#fecs").datepicker( "option", "maxDate", selectedDate );
		//$("#mytablaSalas").trigger("update");	
	}	
});	
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

//referenciaCION DIA
$("#pnuevo").click( function(){
	$("#idpd").val('');$("#idcte").val('');$("#canpd").val('');$("#porpd").val('5.00');$("#obspd").val('');
	$("#cliente").val('');$("#ubigra").val('');$("#con").val('');$("#tel").val(''); //$("#cmbOrigen").val('');
 return true;
})
$("#Origen").change( function(){
	$("#OrigenMes").val($("#Origen").val());
	$("#cmbOrigen").val($("#Origen").val());
	$("#mytablaTM").trigger("update");
 return true;
})
$("#pborrar").click( function(){	
	numero=$("#idpd").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/borrara", 
						data: "id="+$("#idpd").val()+"&cic="+$("#Ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Cliente Eliminado de referenciación");
										$("#pnuevo").click();
										$("#mytablaProg").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Cliente para poder Eliminarlo");
		return false;
	}
});
$("#paceptar").click( function(){		
	cli=$("#cliente").val();
	numero=$("#idpd").val();
	if( cli!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/actualizara", 
						data: "id="+$("#idpd").val()+"&fec="+$("#fecpd").val()+"&idcte="+$("#idcte").val()+"&can="+$("#canpd").val()+"&por="+$("#porpd").val()+"&obs="+$("#obspd").val()+"&tip="+$("#cmbOrigen").val()+"&cic="+$("#Ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#pnuevo").click();
										$("#mytablaProg").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/agregara", 
						data: "fec="+$("#fecpd").val()+"&idcte="+$("#idcte").val()+"&can="+$("#canpd").val()+"&por="+$("#porpd").val()+"&obs="+$("#obspd").val()+"&tip="+$("#cmbOrigen").val()+"&cic="+$("#Ciclo").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Entrega registrada correctamente");
										$("#pnuevo").click();
										$("#mytablaProg").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}	
	}else{
		alert("Error: Seleccione Cliente de la Relación");	
		return false;
	}
});
//referenciaCION SALAS
$("#snuevo").click( function(){
	$("#mytablaSalas").trigger("update");
	$("#ids").val('');$("#sala").val('');$("#cans").val(''); $("#cor").val('');
	$("#fecs").val('');$("#fect").val('');//$("#origenes").val('');
 return true;
})
$("#origens").change( function(){
	$("#origenes").val($("#origens").val());
	//$("#cmbOrigen").val($("#Origen").val());
	$("#mytablaSalas").trigger("update");
	$("#snuevo").click();
 return true;
})
$("#origenes").change( function(){
	$("#origens").val($("#origenes").val());
	//$("#cmbOrigen").val($("#Origen").val());
	$("#mytablaSalas").trigger("update");
	
 return true;
})
$("#sborrar").click( function(){	
	numero=$("#ids").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/borraras", 
						data: "id="+$("#ids").val()+"&cic="+$("#CicloS").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Sala Eliminada de referenciación");
										$("#snuevo").click();
										$("#mytablaSalas").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Sala para poder Eliminarlo");
		return false;
	}
});
$("#saceptar").click( function(){		
	sala=$("#sala").val();ori=$("#origenes").val();cor=$("#cor").val();fecs=$("#fecs").val();fect=$("#fect").val();
	numero=$("#ids").val();
	if( sala>0){
	 if( ori!='Todos'){	
	 if( cor!=''){	
	 if( fecs!=''){
	  if( fect!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/actualizaras", 
						data: "id="+$("#ids").val()+"&fecs="+$("#fecs").val()+"&fect="+$("#fect").val()+"&can="+$("#cans").val()+"&cor="+$("#cor").val()+"&sala="+$("#sala").val()+"&ori="+$("#origenes").val()+"&cic="+$("#CicloS").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#snuevo").click();
										$("#mytablaSalas").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/agregaras", 
						data: "fecs="+$("#fecs").val()+"&fect="+$("#fect").val()+"&can="+$("#cans").val()+"&cor="+$("#cor").val()+"&sala="+$("#sala").val()+"&ori="+$("#origenes").val()+"&cic="+$("#CicloS").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Sala registrada correctamente");
										$("#snuevo").click();
										$("#mytablaProg").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
	  }else{
		alert("Error: Seleccione Fecha de Transferencia");
		$("#fect").focus();	
		return false;
	  }		
	 }else{
		alert("Error: Seleccione Fecha de Siembra");
		$("#fecs").focus();	
		return false;
	 }	
	 }else{
		alert("Error: Registre Corrida");
		$("#cor").focus();	
		return false;
	 }	
	  }else{
		alert("Error: Seleccione Origen de Postlarva");
		$("#origenes").focus();	
		return false;
	 }			
	}else{
		alert("Error: Seleccione Sala");
		$("#sala").focus();	
		return false;
	}
});

$(document).ready(function(){ 
	var f = new Date();
	$("#fecpd").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());	
	$("#feccd").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#feccd").change();
	$("#cmbMesT").val((f.getMonth() +1));	
	
	$("#tabla_prog").ajaxSorter({
		url:'<?php echo base_url()?>index.php/referencia/tablareferencia',  
		filters:['Ciclo','fecpd','Origen'],
		//multipleFilter:true, 
   		sort:false,	
   		onRowClick:function(){
   			if($(this).data('idpd')>0){
   				$("#cliente").val($(this).data('razon'));$("#idcte").val($(this).data('idcte'));$("#cmbOrigen").val($(this).data('tipopl'));
   				$("#idpd").val($(this).data('idpd'));$("#canpd").val($(this).data('canpd'));$("#porpd").val($(this).data('porpd'));
   				$("#ubigra").val($(this).data('ubigra'));$("#con").val($(this).data('con'));$("#tel").val($(this).data('tel'));$("#obspd").val($(this).data('obspd'));   				
   			}
   		},	
   		onSuccess:function(){   						
			$('#tabla_prog tbody tr td:nth-child(1)').map(function(){$(this).css('background','lightgray'); $(this).css('font-weight','bold');})
			$('#tabla_prog tbody tr td:nth-child(3)').map(function(){$(this).css('text-align','center'); $(this).css('font-weight','bold');})
				    		
			
	    	$('#tabla_prog tr').map(function(){	    		
	    		if($(this).data('razon')=='Total:'){
	    			$(this).css('background','lightgray');
	    		}
	    	});
	    	$('#tabla_prog tr').map(function(){	    		
	    		if($(this).data('tipopl1')=='Total Día:'){
	    			$(this).css('background','lightgreen');
	    		}
	    	});
	    },
   	});	
   	$("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/referencia/tablacli',
		multipleFilter:true,
		onRowClick:function(){
    	 		$("#idcte").val($(this).data('numero'));
				$("#cliente").val($(this).data('razon'));
				$("#ubigra").val($(this).data('ubigra'));$("#con").val($(this).data('con'));$("#tel").val($(this).data('tel'));
				$("#canpd").focus();
		},
	});
	$("#tabla_detmes").ajaxSorter({
		url:'<?php echo base_url()?>index.php/referencia/tablaprogmes',
		filters:['cmbCiclo','cmbMesT','OrigenMes'],	
		sort:false,	
		onSuccess:function(){   						
			//ilumina el dia actual del año en curso
			if($("#cmbMesT").val()==(f.getMonth() +1)){
				$('#tabla_detmes tbody tr td:nth-child('+((f.getDate())+1)+')').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			}
			
			//ilumina los clientes de la fecha actual
	    	$('#tabla_detmes tr').map(function(){	    		
	    		if($(this).data('d'+((f.getDate())))!='' && $(this).data('razon')!='' && $("#cmbMesT").val()==(f.getMonth() +1)){
	    			$(this).css('background','lightblue');
	    		}
	    		
	    	});
			$('#tabla_detmes tr').map(function(){	    		
	    		/*if($(this).data('razon')=='referenciado'){
	    			$(this).css('font-weight','bold');
	    			$(this).css('background','Orange');
	    		}*/
	    		if($(this).data('razon')=='referenciado'|| $(this).data('razon')=='Remisionado' || $(this).data('razon')=='Deficit o Superavit'){
	    			$(this).css('font-weight','bold');
	    			$(this).css('background','lightcoral');
	    		}
	    		if($(this).data('razon')=='Fecha de siembra'){
	    			$(this).css('background','lightblue');
	    			$(this).css('font-size','6px'); 
	    		}
	    		if($(this).data('razon')=='Sala' || $(this).data('razon')=='Corrida' || $(this).data('razon')=='Producción Estimada' || $(this).data('razon')=='Producción menos entrega'){
	    			$(this).css('background','lightgray');
	    			//if($(this).data('razon')=='Fecha de siembra'){ $(this).css('font-size','6px'); }
	    		}
	    		if($(this).data('razon')=='Cliente' || $(this).data('razon')=='Corte'){
	    			$(this).css('background','lightblue');
	    		}
	    	});	    		
			$('#tabla_detmes tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');$(this).css('font-size','8px');})
			$('#tabla_detmes tbody tr td:nth-child(33)').map(function(){$(this).css('text-align','right'); $(this).css('font-weight','bold');})
	    	/*$('#tabla_detmes tr').map(function(){
				if($(this).data('razon')=='Cliente'){
	    			$(this).css('position','fixed');
	    			$(this).css('display','block');
	    			$(this).css('margin-top','-40px');
	    		}
			});*/
	    },
	});
	
	$("#tabla_sala").ajaxSorter({
		url:'<?php echo base_url()?>index.php/referencia/tablasalas',  
		filters:['CicloS','origens'],
		
		//multipleFilter:true, 
   		sort:false,	
   		onRowClick:function(){
   			if($(this).data('ids')>0){
   				$("#ids").val($(this).data('ids'));$("#sala").val($(this).data('sala'));
   				$("#origenes").val($(this).data('origens'));$("#cor").val($(this).data('corrida'));
   				$("#cans").val($(this).data('cans'));$("#fecs").val($(this).data('fecs'));$("#fect").val($(this).data('fect'));
   			}
   		},	
   		onSuccess:function(){
   			tot=0;
   			
   			$('#tabla_sala tr').map(function(){	$(this).css('text-align','center');	});
   			if($("#origenes").val()!='Todos' || $("#origens").val()!='Todos'){
   				$('#tabla_sala tbody tr').map(function(){
	    			if($(this).data('corrida')>0){tot+=1;}
    	   			if(tot>0){ $("#cor").val(tot+1); } 
	    		});  
	    	} else{
	    		$("#cor").val('');
	    	}						
			/*$('#tabla_prog tbody tr td:nth-child(1)').map(function(){$(this).css('background','lightgray'); $(this).css('font-weight','bold');})
			$('#tabla_prog tbody tr td:nth-child(3)').map(function(){$(this).css('text-align','center'); $(this).css('font-weight','bold');})
				    		
			
	    	
	    	$('#tabla_prog tr').map(function(){	    		
	    		if($(this).data('tipopl1')=='Total Día:'){
	    			$(this).css('background','lightgreen');
	    		}
	    	});*/
	    },
   	});
   	
   	
   	$("#tabla_ras1").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini1','fin1'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(1);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras1 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    }, 
	});
	$("#tabla_ras2").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini2','fin2'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(2);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras2 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    },
	});
	$("#tabla_ras3").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini3','fin3'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(3);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras3 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    },
	});
	$("#tabla_ras4").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini4','fin4'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(4);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras4 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    },	
	});
	$("#tabla_ras5").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini5','fin5'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(5);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras5 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    },
	});
	$("#tabla_ras6").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablarasA',
		filters:['hoy'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').hide();
        },
		onSuccess:function(){    
    		$('#tabla_ras6 tbody tr').map(function(){
	    		if($(this).data('pl1')=='Tot:'){	    				    			
	    			$(this).css('background','lightblue');
	    		}
	    	});	
	    	$('#tabla_ras6 tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
    	}, 	
	});
	$("#tabla_ras7").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablarasAT',
		filters:['hoy'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').hide();
        },
		onSuccess:function(){    
    		$('#tabla_ras7 tbody tr').map(function(){
	    		if($(this).data('pl1')=='Total:'){	    				    			
	    			$(this).css('background','lightblue');
	    		}
	    	});	
	    	$('#tabla_ras7 tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
    	}, 
	});    	
});  	
</script>
