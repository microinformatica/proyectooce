<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
 
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
#exportr_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>	
		<li><a href="#gral" style="font-size: 24px"> Catálago General </a></li>
		<li><a href="#asigna" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/bancomer.png" width="25" height="25" border="0"> Referencias Bancarias </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px;" >
		<div id="gral" class="tab_content" style="margin-top: -5px" >
			<table border="2">
				<tr>
					<th  colspan="4">
						<div class="ajaxSorterDiv" id="tabla_edo" name="tabla_edo" style="width: 665px;"  >                 			
							<div class="ajaxpager" style=" font-size: 12px;  text-align: left"> 
								<ul class="order_list" style="width: 450px; ">    	    	            			
   									Estado: <select name="edos" id="edos" style="font-size: 10px; height: 25px; margin-top: -10px "></select>
   									<input type="hidden" name="idedo" id="idedo" style="text-align: center; width: 15px">
   									No.:<input type="text" name="num" id="num" style="text-align: center; width: 25px">Nombre:<input type="text" name="nomedo" id="nomedo" style="text-align: center; width: 80px">
   									<input style="font-size: 14px" type="submit" id="agrega" name="agrega" value="+" title="AGREGAR y ACTUALIZAR ESTADO" />
   									<input style="font-size: 14px" type="submit" id="n" name="n" value="N" title="NUEVO ESTADO" />
    	    	            	</ul>
								<form id="data_tablef" action="<?= base_url()?>index.php/referencia/pdflistarefedozon" method="POST" >							
    	    	            		<img style="cursor: pointer; margin-top: 5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablaedo" value ="" class="htmlTable"/> 
								</form>   
                			</div>
							<span class="ajaxTable" style="height: 330px; margin-top: -15px; background-color: white " >
                    			<table id="mytablaEdo" name="mytablaEdo" class="ajaxSorter" border="1" style="width: 645px" >
                        			<thead>                            
                        				<th style="text-align: center" data-order = "num1" >No.</th>
                        				<th style="text-align: center" data-order = "nomedo1" >Estado</th>
                            			<th style="text-align: center" data-order = "nomzon" >Zona</th>
                            			<th style="text-align: center" data-order = "ref1" >Ref Zona</th>
                            			<th style="text-align: center" data-order = "ref2" >Ref Cliente</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			       
             			</div>
					</th>
					
				</tr>
				<tr>
					<th style="text-align: center;background-color: lightcoral; color: white">Zona
						<input type="hidden"  size="2%" name="idzon" id="idzon" style="text-align: center">
						
					</th>
					<th style="text-align: center;background-color: lightcoral; color: white" colspan="2">Referencias</th>
   					<th style="text-align: center; background-color: gray" rowspan="2">
						<input type="submit" id="znuevo" name="znuevo" value="Nuevo"  />
						<input type="submit" id="zaceptar" name="zaceptar" value="Guardar" />
						<input type="submit" id="zborrar" name="zborrar" value="Borrar" />
						
					</th>
				</tr>
				<tr>
					<th style="text-align: center;background-color: lightcoral; color: white">
						<select name="zon" id="zon" style="margin-top: 1px">
    						<option value="Todas">Todas</option>	<option value="Angostura">Angostura</option>
    						<option value="Campeche">Campeche</option> <option value="Chiapas"  >Chiapas</option> <option value="Colima">Colima</option>	
    						<option value="El Dorado">El Dorado</option> <option value="Elota">Elota</option>	
							<option value="Guasave">Guasave</option> <option value="Guaymas">Guaymas</option> <option value="Guerrero">Guerrero</option>	
    						<option value="Hermosillo">Hermosillo</option> <option value="Mochis">Mochis</option>
    						<option value="Navojoa">Navojoa</option>  <option value="Navolato">Navolato</option>	
    						<option value="Nayarit">Nayarit</option>  <option value="Obregón">Obregón</option>	
    						<option value="Sur Sinaloa">Sur Sinaloa</option>  <option value="Tamaulipas">Tamaulipas</option>	
    						<option value="Tabasco">Tabasco</option> 
    						<option value="Veracruz">Veracruz</option>	<option value="Yucatán">Yucatán</option>		
  						</select>
  						<!--<select name="zonas" id="zonas" style="font-size: 10px; height: 25px; margin-top: -10px "></select>-->
					</th>
					<th style="text-align: center;background-color: lightcoral; color: white">Zona:<input type="text" name="ref1" id="ref1" style="text-align: center; width: 25px"></th>
					<th style="text-align: center;background-color: lightcoral; color: white">Clientes:<input type="text" name="ref2" id="ref2" style="text-align: center; width: 25px"></th>
				</tr>
				
			</table>
		</div>	
		<div id="asigna" class="tab_content" style="margin-top: -5px" >
			<table border="2" style="width: 910px">
				<tr>
					<th rowspan="6">
						<div class="ajaxSorterDiv" id="tabla_bco" name="tabla_bco" style="width: 550px;">  
							<div style=" text-align:center; font-size: 12px;" class='filter' >  
								    

						<!--<select  class='filter_type' title="Seleccione el campo para su filtrado" style="font-size: 10px; height: 23px" ></select>
						<select class='filter_equ' title="Seleccione un tipo de comparacion" style="font-size: 10px; height: 23px">
                       		 <option value="like">Parecido</option>
                    	</select>
    	    	        <input id="buscar" name="buscar" class="filter_text" maxlength="15" type="text" title="Ingrese el texto deseado para buscar"/>-->
    	    	         </div>             			
							<div class="ajaxpager" style=" font-size: 12px;  "> 
								<ul class="order_list" style="width: 300px; text-align: left;">
    	    	            		Estado:<select name="edosb" id="edosb" style="font-size: 10px; height: 25px;margin-top: -10px;  "></select>
    	    	            		Zona:<select name="zonas" id="zonas" style="font-size: 10px; height: 25px; margin-top: -10px "></select>
    	    	            	</ul>
    	    	            	<form id="data_tablef" action="<?= base_url()?>index.php/referencia/pdflistaref" method="POST" >	
    	    	            		<img style="cursor: pointer;margin-top: 5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablaref" value ="" class="htmlTable"/> 
								</form>   
							</div>
							<span class="ajaxTable" style=" background-color: white; height: 400px; margin-top: -15px;width: 530px;" >
                    			<table id="mytablaBco" name="mytablaBco" class="ajaxSorter" border="1"  >
                        			<thead>                            
                        				<th style="text-align: center" data-order = "nomedo" >Estado</th>
                        				<th style="text-align: center" data-order = "nomzon" >Zona</th>
                        				<th style="text-align: center" data-order = "razon" >Cliente</th>
                        				<th style="text-align: center" data-order = "referencia" >Referencia</th>                            			
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			       
             			</div>
					</th>
					<th style="background-color: gray; color: white" colspan="4">Cliente
						<input type="hidden" size="3%" name="idbco" id="idbco">
						<input type="hidden" size="3%" name="idcli" id="idcli">
					</th>
				</tr>
				<tr><th colspan="4"><input style="border: 1; width: 350px" readonly="true" type="text" name="cliente" id="cliente"></th></tr>
				<tr >
					<th style="background-color: gray; color: white;text-align: center">Estado</th>
					<th style="background-color: gray; color: white;text-align: center">Zona</th>
					<th style="background-color: gray; color: white;text-align: center">Referencia Cliente</th>
					<th style="background-color: gray; color: white;text-align: center">Dígito Bancomer</th>
				</tr>
				<tr>
					<th style="text-align: center" ><input style="border: 0;text-align: center; width: 10px" readonly="true" type="text" name="refedo" id="refedo"></th>
					<th style="text-align: center"><input style="border: 0;text-align: center; width: 25px" readonly="true"  type="text" name="refzon" id="refzon"></th>
					<th style="text-align: center"><input style="border: 0;text-align: center; width: 25px" readonly="true" type="text" name="refcli" id="refcli"></th>
					<th style="text-align: center"><input style="border: 1;text-align: center; width: 15px"  type="text" name="refbco" id="refbco"></th>
				</tr>
				<tr>
					<th colspan="4" style="text-align: center; background-color: gray">
						
						<input type="submit" id="pnuevo" name="pnuevo" value="Nuevo"  />
						<input type="submit" id="paceptar" name="paceptar" value="Guardar" />
						<!--<input type="submit" id="pborrar" name="pborrar" value="Borrar" />-->
						<button id="exportr_data" type="button" style="width: 130px; cursor: pointer; background-color: lightyellow; text-align: right"  >Carta Referencia</button>
						<form id="documentoref" action="<?= base_url()?>index.php/referencia/pdfrepref" method="POST" >
							<input type="hidden" type="text" name="numcli" id="numcli" > 
							<input type="hidden" type="text" name="cli" id="cli" >
							<input type="hidden" type="text" name="dom" id="dom" >
							<input type="hidden" type="text" name="lecp" id="lecp" >
							<input type="hidden" type="text" name="ref" id="ref" >
						</form>
        						<script type="text/javascript">
									$(document).ready(function(){
										$('#exportr_data').click(function(){	
											$('#cli').val($('#cliente').val());
											$('#numcli').val($('#idcli').val());
											$('#ref').val($('#refedo').val()+$('#refzon').val()+$('#refcli').val()+$('#refbco').val());
											$('#documentoref').submit();
										});
									});
								</script>
					</th>
				</tr>
				<tr>
					<th colspan="4">
						<div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli"  >                 			
							<span class="ajaxTable" style="height: 320px;" >
                    			<table id="mytablaCli" name="mytablaCli" class="ajaxSorter" border="1"   >
                        			<thead>                            
                            			<th data-order = "referencia" >Referencia</th>
                            			<th data-order = "Razon" >Cliente</th>
                            			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
					</th>
				</tr>			
			</table>
		</div>
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

function resultado (datos) {
  obj=null;
  var obj = jQuery.parseJSON( datos );
  if(obj!=null){
  	 //alert(msg);
  	if(obj.nomedo != ''){
  		//alert(obj.can);
  		$("#nomedo").val(obj.nomedo);	
  		$("#num").val(obj.num);
  		
  		return;
  	}else{
  		$("#nomedo").val('');
  		$("#idedo").val('');$("#num").val('');
  		return;
  	}
  }
};
$("#edos").change( function(){
	$("#nomedo").val($("#edos").val()); //$("#num").val($("#edos").val());
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/referencia/buscaredo", 
			data: "id="+$("#nomedo").val(),
			success: resultado,	
	});	
 return true;
})

$("#n").click( function(){
	$("#edos").val('');$("#nomedo").val('');$("#idedo").val('');$("#num").val('');$("#mytablaEdo").trigger("update");
	return true;
});

$("#quita").click( function(){	
	numero=$("#idcd").val();can=$("#cancd").val();
	if(numero!='' && can!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/quitarcor", 
						data: "id="+$("#idcd").val()+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaTM").trigger("update");
										$("#cancd").val('');
   										$("#idcd").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: intenta eliminar Corte Inexistente");
		return false;
	}
});
$("#agrega").click( function(){	
	num=$("#num").val();nom=$("#nomedo").val();
	numero=$("#idedo").val();
	 if(num!=''){
	 if(nom!=''){	
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/actualizaredo", 
						data: "id="+$("#idedo").val()+"&num="+$("#num").val()+"&nomedo="+$("#nomedo").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#n").click(); 										
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/agregaredo", 
						data: "&num="+$("#num").val()+"&nomedo="+$("#nomedo").val(),
						success: 	
								function(msg){															
									if(msg!=0){
										$("#n").click();											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		
		}
	}else{
		alert("Error: Necesita Registrar El Nombre de Estado");
		$("#nomedo").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Registrar El Numero de Estado");
		$("#num").focus();
		return false;
	}
});
//zonas
$("#znuevo").click( function(){
	$("#idzon").val('');$("#zon").val('');$("#ref1").val('');$("#ref2").val('');$("#zonas").val('');
	return true;
});
$("#zaceptar").click( function(){	
	num=$("#num").val();nom=$("#nomedo").val();zon=$("#zon").val();r1=$("#ref1").val();r2=$("#ref2").val();
	numero=$("#idzon").val();
	 if(nom!=''){
	  if(zon!='Todas'){
	  	if(r1!=''){
	  		if(r2!=''){
	 	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/actualizarzon", 
						data: "id="+$("#idzon").val()+"&num="+$("#num").val()+"&nomzon="+$("#zon").val()+"&ref1="+$("#ref1").val()+"&ref2="+$("#ref2").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#znuevo").click(); $("#mytablaEdo").trigger("update");										
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/agregarzon", 
						data: "&num="+$("#num").val()+"&nomzon="+$("#zon").val()+"&ref1="+$("#ref1").val()+"&ref2="+$("#ref2").val(),
						success: 	
								function(msg){															
									if(msg!=0){
										$("#znuevo").click();	$("#mytablaEdo").trigger("update");										
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		
		}
		}else{
		alert("Error: Necesita Registrar Referencia de Clientes");
		$("#ref2").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Registrar Referencia de Zona");
		$("#ref1").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Registrar Nombre de Zona");
		$("#zon").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Seleccionar Estado");
		$("#nomedo").focus();
		
		return false;
	}
});
//referencias bancarias
$("#pnuevo").click( function(){
	$("#cliente").val('');$("#idbco").val('');$("#idcli").val('');$("#refbco").val('');$("#mytablaBco").trigger("update");
	return true;
});

$("#paceptar").click( function(){	
	nc=$("#idcli").val();edo=$("#edosb").val();zon=$("#zonas").val();cli=$("#refcli").val();bco=$("#refbco").val();
	numero=$("#idbco").val();
	if(edo!='0'){
	  if(zon!='0'){
	    if(nc!=''){
	  	  if(cli!=''){
	  	  	if(bco!=''){
	 			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/referencia/buscarcli", 
						data: "id="+$("#idcli").val(),
						success: 	
								function(msg){	
									obj=null;
  									var obj = jQuery.parseJSON( msg );
  									if(obj!=null){
  	 									if(obj.can != ''){
  	 										if(numero!=''){
  	 											$.ajax({
													type: "POST",//Envio
													url: "<?=base_url()?>index.php/referencia/actualizarbco", 
													data: "id="+$("#idbco").val()+"&idcli="+$("#idcli").val()+"&edo="+$("#refedo").val()+"&zon="+$("#refzon").val()+"&cli="+$("#refcli").val()+"&bco="+$("#refbco").val(),
													success: 	
														function(msg){															
															if(msg!=0){	
																alert("Datos Actualizados");$("#pnuevo").click(); //$("#mytablaEdo").trigger("update");										
															}else{
																alert("Error con la base de datos o usted no ha actualizado nada");
															}					
														}		
												});
											}else{
												$.ajax({
													type: "POST",//Envio
													url: "<?=base_url()?>index.php/referencia/agregarbco", 
													data: "&idcli="+$("#idcli").val()+"&edo="+$("#refedo").val()+"&zon="+$("#refzon").val()+"&cli="+$("#refcli").val()+"&bco="+$("#refbco").val(),
													success: 	
														function(msg){															
															if(msg!=0){
																alert("Referencia Bancaria Registrada Correctamente");$("#pnuevo").click();	//$("#mytablaEdo").trigger("update");										
															}else{
																alert("Error con la base de datos o usted no ha actualizado nada");
															}					
														}					
												});
											}
									}else{
										alert("Error Cliente ya esta asignado a una referencia... Seleccione otro");
										$("#cliente").val('');$("#idcli").val('');
									}	
  									}
  								}														
									
				});
				
		
		}else{
		alert("Error: Necesita Registrar Dígito Bancario");
		$("#refbco").focus();
		return false;
	}
		}else{
		alert("Error: Necesita Registrar Referencia de Clientes");
		$("#refcli").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Seleccionar Cliente");
		$("#cliente").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Registrar Referencia de Zona");
		$("#zonas").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Registrar Estado");
		$("#edosb").focus();
		return false;
	}
	
});
$(document).ready(function(){ 
	$("#tabla_edo").ajaxSorter({
		url:'<?php echo base_url()?>index.php/referencia/tablaestados',  
		filters:['edos'],
		sort:false,	
   		onRowClick:function(){
   			if($(this).data('num')>0){
   				
   				$("#idedo").val($(this).data('idedo'));$("#edos").val($(this).data('nomedo'));
   				$("#nomedo").val($(this).data('nomedo'));$("#num").val($(this).data('num'));
   				$("#idzon").val($(this).data('idzon'));$("#zon").val($(this).data('nomzon'));
   				$("#ref1").val($(this).data('ref1'));$("#ref2").val($(this).data('ref2'));
   				//$("#edos").change(); $("#zonas").val($(this).data('nomzon'));
   			}
   		},	
   		onSuccess:function(){
   			$('#tabla_edo tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','center'); })
   			$('#tabla_edo tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })
   			$('#tabla_edo tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','center'); })
   			/*tot=0;
   			$('#tabla_edo tbody tr').map(function(){if($(this).data('num')>0){tot+=1;}});
   			if($("#edos").val()=='0'){ 
	    	if(tot>0){$("#num").val(tot+1);}else{$("#num").val('1');}
	    	}*/
	    		
	    },
   	});
   	$("#tabla_bco").ajaxSorter({
		url:'<?php echo base_url()?>index.php/referencia/tablaestadosbco',  
		filters:['edosb','zonas'],
		sort:false,	
		//multipleFilter:true,
		active:['razon','Cliente Desconocido'],
		//onLoad:false,
   		onRowClick:function(){
   			if($(this).data('idcli')>0){
   				$("#idcli").val($(this).data('idcli'));
				$("#cliente").val($(this).data('razon'));
				$("#dom").val($(this).data('dom'));
				if($(this).data('cp')!=0){	$("#lecp").val($(this).data('loc')+" "+$(this).data('edo')+" "+$(this).data('cp'));	}
				else{ $("#lecp").val($(this).data('loc')+" "+$(this).data('edo'));}
   				$("#idbco").val($(this).data('idbco'));
   				$("#refedo").val($(this).data('refedo'));$("#refzon").val($(this).data('refzon'));
   				$("#refcli").val($(this).data('refcli'));$("#refbco").val($(this).data('refbco')); 
   				$("#edosb").val($(this).data('refedo'));
   				//$("#zonas").val($(this).data('refzon'));  
   				// $("#mytablaBco").trigger("update"); 				
   			}
   		},	
   		onSuccess:function(){
   			$('#tabla_bco tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })
   			$('#tabla_bco tbody tr').map(function(){
   				$("#refedo").val($(this).data('idzonedo'));
   				$("#refzon").val($(this).data('ref1'));   
   				$("#refcli").val($(this).data('ref2'));
   				$("#refbco").val('');					
	    	})	
   			
	    },
   	});
   	$("#edos").boxLoader({
        url:'<?php echo base_url()?>index.php/referencia/combo',
         		//equal:{id:'actual',value:'#cmbCiclo'},
      	select:{id:'nomedo',val:'nomedo'},
      	all:"Todos",
    });
    $("#edosb").boxLoader({
        url:'<?php echo base_url()?>index.php/referencia/comboez',
         		//equal:{id:'actual',value:'#refedo'},
      	select:{id:'num',val:'nomedo'},
      	all:"Todos",
    });
    $("#zonas").boxLoader({
        url:'<?php echo base_url()?>index.php/referencia/comboz',
        equal:{id:'actual',value:'#edosb'},
      	select:{id:'ref1',val:'nomzon'},
      	all:"Todos",
    });
    $("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/referencia/tablacli',
		sort:false,
		multipleFilter:true,
		onRowClick:function(){
    	 		$("#idcli").val($(this).data('numero'));
				$("#cliente").val($(this).data('razon'));
				/*$("#refedo").val($(this).data('refedo'));$("#refzon").val($(this).data('refzon'));
   				$("#refcli").val($(this).data('refcli'));$("#refbco").val($(this).data('refbco'));*/ 
				$("#refbco").focus();
				$("#dom").val($(this).data('dom'));
				if($(this).data('cp')!=0){	$("#lecp").val($(this).data('loc')+" "+$(this).data('edo')+" "+$(this).data('cp'));	}
				else{ $("#lecp").val($(this).data('loc')+" "+$(this).data('edo'));}
				
		},
	});
});  	
</script>
