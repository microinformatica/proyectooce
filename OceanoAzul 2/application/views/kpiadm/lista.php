<?php $this->load->view('header_cv'); ?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
 <style type="text/css">
            .color{
                background-color: #ff00ff;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function(){
                $("table.mitabla td").click(function(){
                    $(this).toggleClass("color");
                });
            });
        </script>
<style>     
ul.tabs li {border-bottom: 1px ;}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#kpi" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/kpi.png" width="25" height="25" border="0"> KPI </a></li>
		<li><a href="#tecnico" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/caja.png" width="20" height="20" border="0"> Viáticos </a></li>
		<li><a href="#escaner" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/caja.png" width="20" height="20" border="0"> Combustible </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px" >
		<div id="kpi" class="tab_content" style="margin-top: -15px" >
		  <div class="ajaxSorterDiv" id="tabla_kpi" name="tabla_kpi" style=" height: 470px;margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 450px; text-align: left" >
            		Mes:
   					<select name="cmbMeskpi" id="cmbMeskpi" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   						<option value="12" >Diciembre</option>
   						<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   						<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   						<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   						<option value="10" >Octubre</option><option value="11" >Noviembre</option>
   						
   					</select>
   					Departamento:
   					<?php if($usuario=="Jesus Benítez"){ ?>
   					<select name="cmbDepto" id="cmbDepto" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   						<?php	
          					$this->load->model('kpiadm_model');
							$data['result']=$this->kpiadm_model->verDepto();
							foreach ($data['result'] as $row):?>
           						<option value="<?php echo $row->NDep;?>" ><?php echo $row->NomDep;?></option>
           				<?php endforeach;?>
   					</select>
   					<?php } else{?>
   					<?php if($usuario=="Anita Espino" ){ ?>
   					<select name="cmbDepto" id="cmbDepto" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">
   						<option value="7">Administración</option>
   					</select>		
   					<?php } } ?>
   					<?php if($usuario=="Jesus Benítez"){ ?>
                  	 	<select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 22px; visibility: hidden"  >
                    		<!--<option value="solicitud">2015</option>-->
                    		<?php $ciclof=2014; $actual=date("Y"); //$actual+=1;
								while($actual >= $ciclof){?>
									<option value="<?php echo ($actual-2000);?>" > <?php echo $actual;?> </option>
            					<?php  $actual-=1; } ?>
          				</select>      
                        
                   <?php } else {?>
                        <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 22px; visibility: hidden">
                    		<option value="17">2017</option>
                    	</select> 	
                    <?php } ?>		
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 430px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablakpi" name="mytablakpi" class="ajaxSorter" border="1" >
                       	<thead >
                       		<th data-order = "nom">Evaluación</th>
                       		<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
                       	</thead>
                       	<tbody style="font-size: 9px">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>		    
 		</div>
			
	<div id="tecnico" class="tab_content" style="margin-top: -15px" >
		  <div class="ajaxSorterDiv" id="tabla_tec" name="tabla_tec" style=" height: 470px;margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 150px" >
            		Mes:
   					<select name="cmbMes" id="cmbMes" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   						<option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   						<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   						<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   						<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   						<option value="10" >Octubre</option>
   						
   					</select>	
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 430px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablaTec" name="mytablaTec" class="ajaxSorter" border="1" >
                       	<thead >
                       		<th data-order = "tec">Técnico</th>
                       		
                       		<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
                       	</thead>
                       	<tbody style="font-size: 9px">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>		    
 		</div>
 		<div id="escaner" class="tab_content" style="margin-top: -15px" >
		     <div class="ajaxSorterDiv" id="tabla_com" name="tabla_com" style=" height: 470px;margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 150px" >
            		Mes:
   					<select name="cmbMesC" id="cmbMesC" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   						<option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   						<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   						<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   						<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   						<option value="10" >Octubre</option>
   						
   					</select>	
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 430px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablaCom" name="mytablaCom" class="ajaxSorter" border="1" >
                       	<thead >
                       		<th data-order = "tec">Chofer</th>
                       		<th data-order = "por">%</th>
                       		<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
                       	</thead>
                       	<tbody style="font-size: 9px">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>
 		</div>
 		  	  
 	</div>	
</div>

<?php $this->load->view('footer_cv'); ?>



<script type="text/javascript">
function cerrar(sel){
	$(this).removeClass('used');
	if(sel==1){
		$('#ver_mas_ent').hide();
		$('#ver_mas_det').show();	
    	
	}	
}


$("#txtFIE").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFFE").datepicker( "option", "minDate", selectedDate );
		$('#tabla_entre').trigger('update')		
	}
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
		$.datepicker.setDefaults($.datepicker.regional['es']);
});  

// Función que suma o resta días a la fecha indicada
 
sumaFecha = function(d, fecha){
 var Fecha = new Date();
 var sFecha = fecha || (Fecha.getDate() + "-" + (Fecha.getMonth() +1) + "-" + Fecha.getFullYear());
 var sep = sFecha.indexOf('-') != -1 ? '-' : '-'; 
 var aFecha = sFecha.split(sep);
 var fecha = aFecha[2]+'-'+aFecha[1]+'-'+aFecha[0];
 fecha= new Date(fecha);
 fecha.setDate(fecha.getDate()+parseInt(d));
 var anno=fecha.getFullYear();
 var mes= fecha.getMonth()+1;
 var dia= fecha.getDate();
 mes = (mes < 10) ? ("0" + mes) : mes;
 dia = (dia < 10) ? ("0" + dia) : dia;
 var fechaFinal = anno+sep+mes+sep+dia;
 return (fechaFinal);
}
$(document).ready(function(){       
	var f = new Date(); 
    $("#cmbMes").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));
	$("#cmbMeskpi").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));
	$("#cmbMesC").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));
	
	$("#tabla_tec").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpiadm/tablatec', 
		filters:['cmbCiclo','cmbMes'],
		sort:false,	
		onSuccess:function(){   			
			
    					
			//ilumina el dia actual del año en curso
			if($("#cmbMes").val()==(f.getMonth() +1)){
				$('#tabla_tec tbody tr td:nth-child('+((f.getDate())+1)+')').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			}
			$('#tabla_tec tbody tr').map(function(){
		    	if($(this).data('tec')=='KPI') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    })	
				
	    	$('#tabla_tec tr').map(function(){	    		
	    		if($(this).data('d'+((f.getDate())))!='' && $("#cmbMes").val()==(f.getMonth() +1)){
	    			$(this).css('background','lightblue');
	    		}
	    		
	    		$(this).children("td").each(function(){
       				//if($(this).data('d1') < 1){	
            		if($(this).html() > 0 && $(this).html() < 1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0");
            		}
            		if($(this).html() < 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "red");
            		}
            		if($(this).html() >1 && $(this).html() < 2 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange");
            		}
            		if($(this).html() >= 90.0 && $(this).html() <= 100.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0");$(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 60.0 && $(this).html() <= 89.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange");$(this).css("color", "orange");
            		}
            		if($(this).html() >= 2 &&  $(this).html() <= 59.9 && $(this).html() != '' ) {
                		$(this).css("background-color", "red");$(this).css("color", "red");
            		}
        		})
        		
	    	});
	    	
	    	
		},		
	});
	$("#tabla_com").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpiadm/tablacom', 
		filters:['cmbCiclo','cmbMesC'],
		sort:false,	
		onSuccess:function(){   			
			
    					
			//ilumina el dia actual del año en curso
			if($("#cmbMesC").val()==(f.getMonth() +1)){
				$('#tabla_com tbody tr td:nth-child('+((f.getDate())+1)+')').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			}
			$('#tabla_com tbody tr').map(function(){
		    	if($(this).data('tec')=='KPI') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    })	
				
	    	$('#tabla_com tr').map(function(){	    		
	    		if($(this).data('d'+((f.getDate())))!='' && $("#cmbMesC").val()==(f.getMonth() +1)){
	    			$(this).css('background','lightblue');
	    		}
	    		
	    		$(this).children("td").each(function(){
       				if($(this).html() <= 5.00 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0");$(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 5.01 && $(this).html() <= 20.0 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "white");
            		}
            		if($(this).html() >= 20.01 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "white");
            		} 
        		})
        		
	    	});
	    	
	    	
		},		
	});
	
	$("#tabla_kpi").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpiadm/tablakpi', 
		filters:['cmbMeskpi','cmbDepto'],
		sort:false,	
		onSuccess:function(){   						
			//ilumina el dia actual del año en curso
			if($("#cmbMeskpi").val()==(f.getMonth() +1)){
				$('#tabla_kpi tbody tr td:nth-child('+((f.getDate())+1)+')').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			}
			$('#tabla_kpi tr').map(function(){	    		
	    		$(this).children("td").each(function(){
       				if($(this).html() >= 90.0 && $(this).html() <= 100.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 60.0 && $(this).html() <= 89.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
            		if($(this).html() >= 2 &&  $(this).html() <= 59.9 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "red");
            		}            		
        		})
	    	});
	    	$('#tabla_kpi tr').map(function(){	    		
	    		if($(this).data('nom')=='C.V.') {
	    		$(this).children("td").each(function(){
       				if($(this).html() >= 0.1 && $(this).html() <= 3.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 3.01 && $(this).html() <= 4.99 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
            		if($(this).html() >= 5 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "red");
            		}  
        		})
        		}
        		
	    	});
	    	$('#tabla_kpi tr').map(function(){	  
	    	if($(this).data('nom')=='KPI') {
	    		$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		$(this).children("td").each(function(){
       				if($(this).html() >= 90.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 60.0 && $(this).html() <= 89.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
            		if($(this).html() >= 2 &&  $(this).html() <= 59.9 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "red");
            		}         		 
        		})
        		}
        	});	
        	
        	$('#tabla_kpi tr').map(function(){	  
	    	if($(this).data('nom')=='Combustible') {
	    		$(this).children("td").each(function(){
       				if($(this).html() <= 5.00 && $(this).html() != '' ) {
              				$(this).css("background-color", "#0F0");$(this).css("color", "#0F0");
        			}
        			if($(this).html() >= 5.01 && $(this).html() <= 20.0 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "orange");
        			}
        			if($(this).html() >= 20.01 && $(this).html() != '' ) {
             				$(this).css("background-color", "red"); $(this).css("color", "red");
        			}         		 
        		})
        		}
        	});	
        	
		},	
	});
});
</script>

