<?php $this->load->view('header_cv'); ?>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/export-data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/accessibility.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/series-label.js"></script>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {padding: 10px;font-size: 1.2em;	height: 452px;}
/*ul.tabs li {
	border-top: none;border-left: none;	border-right: none;	border-bottom: 1px ;font-size: 25px;background: none;		
} */  
#export_datag { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datagd { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datagz { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#general" style="font-size: 24px"> General </a></li>
		<?php if($usuario=="Zuleima Benitez" || $usuario=="Efrain Lizárraga" || $usuario=="Joel Lizarraga A."){ ?>
		<li><a href="#diag" style="font-size: 24px"> Dia </a></li>
		<li><a href="#ventasg" style="font-size: 24px"> Ventas </a></li>
		<li><a href="#costos" style="font-size: 24px"> Costos </a></li>
		<li><a href="#granja" style="font-size: 24px"> Granja </a></li>

		
		<?php } ?>
		</strong>
		<select name="cicloG" id="cicloG" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-4px;">
        	<option value="22">2022</option>
        	<option value="21" >2021</option>
        </select>
        
        <?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez"){ ?>
        	<select name="cmbGra" id="cmbGra" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">	
        		<option value="4" >OA-Ahome</option>
        		<option value="10" >OA-Topolobampo</option>
   			</select>
   		<?php } else {?>
   			<select name="cmbGra" id="cmbGra" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">	   	
   				<option value="4" >Ahome</option>   									
   			</select>
   		<?php } ?>
   		
		<input type="text" id="dia" class="fecha redondo mUp" readonly="true" style="text-align: center;width: 70px" >
   		
   		<select name="cmbMes" id="cmbMes" style="font-size: 10px;height: 25px;  margin-top: 1px;">
            <option value="0" >Todos</option>
             <?php
             $ciclo=2022;	
           	$data['result']=$this->general_model->vermeses($ciclo);
			foreach ($data['result'] as $row):?>
           		<option value="<?php echo $row->mes;?>" ><?php echo $row->nom;?></option>
           	<?php endforeach;?>
   		</select> 
   		<input type="hidden" id="tip1" name="tip1" value="2"><input type="hidden" id="tip2" name="tip2" value="1">
        <input type="hidden" id="Grafi" name="Grafi"><input type="hidden" id="sdotot1" name="sdotot1">
		<input type="hidden" name="vtabordo" id="vtabordo"/><input type="hidden" name="vtamaquila" id="vtamaquila"/>
		<input type="hidden" name="gtoalimento" id="gtoalimento"/><input type="hidden" name="gtomaquila" id="gtomaquila"/>
		<input type="hidden" name="gtocombustible" id="gtocombustible"/><input type="hidden" name="gtosueldo" id="gtosueldo"/>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 650px"  >
		<div id="general" class="tab_content" style="margin-top: 1px">  
			<div class="ajaxSorterDiv" id="tabla_bordo" name="tabla_bordo" style="width: 910px; margin-top: -15px;"  >
				<span class="ajaxTable" style="height:120px;" >
                	<table id="mytablaBordo" name="mytablaBordo" class="ajaxSorter" border="1" style="width:909px; margin-left: -1PX;" >
                		<thead >  
                			<tr>   
                   				<th rowspan="2" data-order = "cicg" >BORDO <br>No. Ciclo</th>
                   				<th colspan="2">Siembra</th>
                   				<th colspan="4">Cosecha</th>
                   				<th colspan="5">Distribución de Kgs. Cosechados</th>
                   			</tr>
                   			<tr>
                   				<th data-order = "hasg" >Has</th> <th data-order = "orsg" >Orgs</th>
                   				<th data-order = "orcg" >Orgs</th> <th data-order = "ppg" >grs</th>
                   				<th data-order = "kgsg" >kgs</th> <th data-order = "sobg" >% Sob</th>
                   				<th data-order = "vtakg" >Ventas</th> <!--<th data-order = "vtaiv" >Importe</th>-->
                   				<th data-order = "vtapv" >%</th>
                   				<th data-order = "vtamq" >Maquila</th> <!--<th data-order = "vtaim" >Importe</th>-->
                   				<th data-order = "vtapm" >%</th>
                   				<th data-order = "vtamd" >Donación</th>
                   			</tr>
                    	</thead>
                    	<tbody  style="font-size: 12px; text-align: center;">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
            <div class="ajaxSorterDiv" id="tabla_ventas" name="tabla_ventas" style="width: 910px; margin-top: -15px;"  >
				<span class="ajaxTable" style="height:47px;" >
                	<table id="mytablaVentas" name="mytablaVentas" class="ajaxSorter" border="1" style="width:909px; margin-left: -1PX;" >
                		<thead >  
                			<tr>   
                   				<th data-order = "vta" >Ventas Bordo</th>
                   				<th data-order = "kgs" >kgs</th>
                   				<th data-order = "pre" >$ kg</th>
                   				<th data-order = "imp" >Importe</th>
                   				<th data-order = "dep" >Depósitos</th>
                   				<th data-order = "sal" >Saldo</th>
                   				<th data-order = "por" >%</th>
                   			</tr>
                    	</thead>
                    	<tbody  style="font-size: 12px; text-align: center;">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
            <div class="ajaxSorterDiv" id="tabla_maquila" name="tabla_maquila" style="width: 910px; margin-top: -15px;"  >
				<span class="ajaxTable" style="height:47px;" >
                	<table id="mytablaMaquila" name="mytablaMaquila" class="ajaxSorter" border="1" style="width:909px; margin-left: -1PX;" >
                		<thead >  
                			<tr>   
                   				<th data-order = "vta" >Maquila</th>
                   				<th data-order = "kgs" >Bordo</th>
                   				<th data-order = "maq" >Procesados</th>
                   				<th data-order = "ren" >Ren</th>
                   				<th data-order = "vtap" >Venta kg</th>
                   				<th data-order = "porv" >%</th>
                   				<th data-order = "idv" >%IVD</th>
                   				<th data-order = "pre" >$ kg</th>
                   				<th data-order = "imp" >Importe</th>
                   				<th data-order = "dep" >Depósitos</th>
                   				<th data-order = "sal" >Saldo</th>
                   				<th data-order = "por" >%</th>
                   			</tr>
                    	</thead>
                    	<tbody  style="font-size: 12px; text-align: center;">
                    	</tbody>                        	
                    </table>
                </span> 
                <div class="ajaxpager" style="margin-top: -5px;width: 940px;" > 
            		<ul class="order_list" style="width: 930px; text-align: left; font-size: 10px; margin-top: 1px;" >
            		Nota: %IVD corresponde al incremento en porcentaje de las ventas del día.
					</ul>
 				</div>  
            </div>
            
            <div class="ajaxSorterDiv" id="tabla_gastos" name="tabla_gastos" style="width: 910px; margin-top: -5px;"  >
				<span class="ajaxTable" style="height:47px;" >
                	<table id="mytablaGastos" name="mytablaGastos" class="ajaxSorter" border="1" style="width:909px; margin-left: -1PX;" >
                		<thead >  
                			<tr>   
                   				<th data-order = "gto" >COSTOS</th>
                   				<th style="border-right: none;" data-order = "kgsali" >Alimento</th>
                   				<th style="border-left: none;border-right: none;" data-order = "impali" ></th>
                   				<th style="border-left: none;" data-order = "porali" ></th>
                   				<th style="border-right: none;" data-order = "premaq" >Maquila</th>
                   				<th style="border-left: none;border-right: none;" data-order = "impmaq" ></th>
                   				<th style="border-left: none;" data-order = "pormaq" ></th>
                   				<th style="border-right: none;" data-order = "ltscom" >Combustible</th>
                   				<th style="border-left: none;border-right: none;" data-order = "impcom" ></th>
                   				<th style="border-left: none;" data-order = "porcom" ></th>
                   				<th style="border-right: none;" data-order = "impsue" >Sueldos</th>
                   				<th style="border-left: none;" data-order = "porsue" ></th>
                   				<th style="border-right: none;" data-order = "impotr" >Otros</th>
                   				<th style="border-left: none;" data-order = "porotr" ></th>
                   				<!--<th data-order = "tot" >Total</th>-->                   				
                   			</tr>
                    	</thead>
                    	<tbody  style="font-size: 12px; text-align: center;">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
         <figure class="highcharts-figure" style="margin-top: -10px;">
    		<div id="grafica" style="height: 250px;"></div>
    		<p class="highcharts-description">
        		Importes actuales de Ingresos y Costos principales en el ciclo productivo.
    		</p>
    		<table id="mytablaSaldos" name="mytablaSaldos" border="0" class="ajaxSorter" style="width: 500px; margin-top: -15px;">
    			<tr>
    				<th>Ingresos</th>
    				<th>Costos</th>
    				<th>Saldo</th>
    			</tr>
    			<tr">
					<th><input type="text" id="ingtot" name="ingtot" style="border: none; background: none; font-size: 18px; color: blue; text-align: center;"></th>
    				<th><input type="text" id="gtotot" name="gtotot" style="border: none; background: none; font-size: 18px; color: red; text-align: center;"></th>
    				<th><input type="text" id="sdotot" name="sdotot" style="border: none; background: none; font-size: 18px; text-align: center;"></th>
    			</tr>
    		</table>
    		<!--<div style="font-size: 10px; text-align: center; margin-top: 1px;">Actualización automática de la información se realiza cada minutos.</div>-->
    		<div align="left" style="margin-top: -20px;">
    		<button id="export_datag" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right;">Imprimir</button>
    		</div>
		<form id="data_tablegr" action="<?= base_url()?>index.php/general/pdfrepgral" method="POST">
			<input type="hidden" name="gra1" id="gra1"/><input type="hidden" name="dia1" id="dia1"/>
			<input type="hidden" name="tablab" id="html_vtab"/> <input type="hidden" name="tablav" id="html_vtav"/> 
			<input type="hidden" name="tablam" id="html_vtam"/> <input type="hidden" name="tablag" id="html_vtag"/> 			
			<input type="hidden" name="ing" id="ing"/> <input type="hidden" name="gto" id="gto"/> 
			<input type="hidden" name="sal" id="sal"/><input type="hidden" name="grapdf" id="grapdf"/> 
		</form>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#export_datag').click(function(){									
					$('#html_vtab').val($('#mytablaBordo').html());
					$('#html_vtav').val($('#mytablaVentas').html());
					$('#html_vtam').val($('#mytablaMaquila').html());
					$('#html_vtag').val($('#mytablaGastos').html());
					$('#ing').val($('#ingtot').val());$('#gto').val($('#gtotot').val());$('#sal').val($('#sdotot').val());
					$('#dia1').val($('#dia').val());$('#grapdf').val($('#grafica').html());
					$('#data_tablegr').submit();
					$('#html_vtag').val('');$('#html_vtav').val('');$('#html_vtam').val('');$('#html_vtag').val('');
					$('#html_saldos').val('');
				});
			});
		</script> 
		</figure> 	

     </div>  
     <?php if($usuario=="Zuleima Benitez" || $usuario=="Efrain Lizárraga"  || $usuario=="Joel Lizarraga A."){ ?>
     <div id="diag" class="tab_content" style="margin-top: 1px">   
      <table style="margin-left: -10px; margin-top: -10px;">
      	<tr>
      		<td>
      			BORDO
      		</td>
      		<td>
      		 	<div class="ajaxSorterDiv" id="tabla_bordia" name="tabla_bordia" style="width: 820px; margin-top: -15px;"  >
					<span class="ajaxTable" style="height:120px;" >
		               	<table id="mytablaBordia" name="mytablaBordia" class="ajaxSorter" border="1" style="width:800x; margin-left: -1PX;" >
		               		<thead >  
		               			<tr>   
		              				<th rowspan="2" data-order = "grad" >Granja</th>
		               				<th rowspan="2" data-order = "kgsd" >kgs. Cosechados</th>
		               				<th colspan="5">Distribución de Kgs. Cosechados</th>
		               			</tr>
		               			<tr>
		               				<th data-order = "vtakgd" >Ventas</th><th data-order = "vtapvd" >%</th>
		               				<th data-order = "vtamqd" >Maquila</th><th data-order = "vtapmd" >%</th>
		               				<th data-order = "vtamdd" >Donación</th>
		               			</tr>
		                   	</thead>
		                   	<tbody  style="font-size: 12px; text-align: center;">
		                   	</tbody>                        	
		                </table>
		             </span> 
		        </div>		
      		</td>
      	</tr>
      	<tr>
      		<td>
      			VENTAS PRODUCTO BORDO		
      		</td>
      		<td>
      			<div class="ajaxSorterDiv" id="tabla_borvdia" name="tabla_borvdia" style="width: 820px; margin-top: -15px;"  >
					<span class="ajaxTable" style="height:120px;" >
		               	<table id="mytablaBorvdia" name="mytablaBorvdia" class="ajaxSorter" border="1" style="width:800px; margin-left: -1PX;" >
		               		<thead >  
		               			<tr>
		               				<th data-order = "grabvd" >Granja</th><th data-order = "clibvd" >Cliente</th>
		               				<th data-order = "kgsbvd" >kgs</th><th data-order = "impbvd" >Importe</th>
		               			</tr>
		                   	</thead>
		                   	<tbody  style="font-size: 12px; text-align: center;">
		                   	</tbody>                        	
		                </table>
		             </span> 
		        </div>
      		</td>
      	</tr>
      	<tr>
      		<td>MAQUILA</td>
      		<td>
      			<div class="ajaxSorterDiv" id="tabla_maqdia" name="tabla_maqdia" style="width: 820px; margin-top: -15px;"  >
					<span class="ajaxTable" style="height:120px;" >
		               	<table id="mytablaMaqdia" name="mytablaMaqdia" class="ajaxSorter" border="1" style="width:800px; margin-left: -1PX;" >
		               		<thead >  
		               			<tr>   
		              				<th rowspan="2" data-order = "grad" >Granja</th>
		              				<th rowspan="2" data-order = "almd" >Almacen</th>
		               				<th colspan="1" >Fresco</th>
		               				<th colspan="4" >Resultados de Producto Maquilado</th>
		               			</tr>
		               			<tr>
		               				<th data-order = "maqfd" >kgs</th>
		               				<th data-order = "maqpd" >kgs</th><th data-order = "maqrd" >Ren</th>
		               				<th data-order = "maqcd" >$/kg</th><th data-order = "maqid" >Importe</th>
		               			</tr>
		                   	</thead>
		                   	<tbody  style="font-size: 12px; text-align: center;">
		                   	</tbody>                        	
		                </table>
		             </span> 
		        </div>		
      		</td>
      	</tr>
      	<tr>
      		<td>VENTAS PRODUCTO MAQUILADO</td>
      		<td>
      			<div class="ajaxSorterDiv" id="tabla_prodia" name="tabla_prodia" style="width: 820px; margin-top: -15px;"  >
					<span class="ajaxTable" style="height:120px;" >
		               	<table id="mytablaProdia" name="mytablaProdia" class="ajaxSorter" border="1" style="width:800px; margin-left: -1PX;" >
		               		<thead >  
		               			<tr>
		               				<th data-order = "proad" >Almacen</th><th data-order = "clipd" >Cliente</th>
		               				<th data-order = "prokd" >kgs</th><th data-order = "proid" >Importe</th>
		               				<th data-order = "prosg" >Saldo</th><th data-order = "propr" >%</th>
		               			</tr>
		                   	</thead>
		                   	<tbody  style="font-size: 12px; text-align: center;">
		                   	</tbody>                        	
		                </table>
		             </span> 
		        </div>		
      		</td>
      	</tr>
      	<tr>
      		<td style="border-bottom: none;">DEPOSITOS</td>
      		<td rowspan="2">
      			<div class="ajaxSorterDiv" id="tabla_depdia" name="tabla_depdia" style="width: 820px; margin-top: -15px;"  >
					<span class="ajaxTable" style="height:120px;" >
		               	<table id="mytablaDepdia" name="mytablaDepdia" class="ajaxSorter" border="1" style="width:800px; margin-left: -1PX;" >
		               		<thead >  
		               			<tr>
		               				<th data-order = "destd" >Destino</th>
		               				<th data-order = "Razon" >Cliente</th>
		               				<th data-order = "Pesos" >M.N.</th><th data-order = "Dolar" >U.S.D.</th>
		               			</tr>
		                   	</thead>
		                   	<tbody  style="font-size: 12px; text-align: center;">
		                   	</tbody>                        	
		                </table>
		             </span> 
		        </div>		
      		</td>
      	</tr>
      	<tr>
      		<td style="border-top: none;">
      			<div style="margin-top: -28px; margin-left: -10px; text-align: left;">
      				
      				<button id="export_datagd" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right;">Imprimir</button>
    		</div>
      			</div>
      		</td>
      	</tr>
      </table> 
     	
    	<form id="data_tablegd" action="<?= base_url()?>index.php/general/pdfrepgrald" method="POST">
			<input type="hidden" name="gra1d" id="gra1d"/><input type="hidden" name="dia1d" id="dia1d"/>
			<input type="hidden" name="tablabd" id="html_vtabd"/> <input type="hidden" name="tablabvd" id="html_vtabvd"/> 
			<input type="hidden" name="tablavd" id="html_vtavd"/> <input type="hidden" name="tablamd" id="html_vtamd"/>
			<input type="hidden" name="tabladd" id="html_vtadd"/> 	
		</form>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#export_datagd').click(function(){									
					$('#html_vtabd').val($('#mytablaBordia').html());
					$('#html_vtabvd').val($('#mytablaBorvdia').html());
					$('#html_vtavd').val($('#mytablaProdia').html());
					$('#html_vtamd').val($('#mytablaMaqdia').html());
					$('#html_vtadd').val($('#mytablaDepdia').html());
					$('#dia1d').val($('#dia').val());
					$('#data_tablegd').submit();
					$('#html_vtabd').val('');$('#html_vtabvd').val('');$('#html_vtavd').val('');
					$('#html_vtamd').val('');$('#html_vtadd').val('');
				});
			});
		</script>     
     </div>	
     <div id="ventasg" class="tab_content" style="margin-top: 1px">  
     	<input type="hidden" id="g1" name="g1" value="1"><input type="hidden" id="g2" name="g2" value="2">
     	<table border="0" style="width: 950px; margin-top: -10px; margin-left: -5px; border: 0;">
     		<tr>
     			<td >
     				<figure class="highcharts-figure" style="width: 580px; margin-top: 1px; margin-left: -15px;">
						<div style="width: 600px;" id="gravtames" name="gravtames"></div>		   
					</figure>
     			</td>
     			<td >
     				<figure class="highcharts-figure" style="width: 300px; margin-top: 1px; margin-left: -10px;">
						<div id="gravtasem" name="gravtasem"></div>		   
					</figure>
     			</td>     			
     		</tr>
     		<tr>
     			<td colspan="2">
     				<div class="ajaxSorterDiv" id="tabla_vtatalg" name="tabla_vtatalg" style="width: 940px; margin-top: -21px; margin-left: -10px;"  >
			     		<span class="ajaxTable" style="height: 240px; margin-top: 1px; " >
			         		<table id="mytablaVtatalg" name="mytablaVtatalg" class="ajaxSorter" border="1" style="width: 920px" >
			           			<thead>     
			           				<tr style="font-size: 11px">
			               				<th  data-order = "nomt">Talla</th>
			              				<th data-order = "d1"><input type="text" id="d1" name="d1" style="width: 65px; border: none; background: none;">
			              				</th>
			              				<th data-order = "d2"><input type="text" id="d2" name="d2" style="width: 65px; border: none; background: none;">
			              				</th>
			              				<th data-order = "d3"><input type="text" id="d3" name="d3" style="width: 65px; border: none; background: none;">
			              				</th>
			              				<th data-order = "d4"><input type="text" id="d4" name="d4" style="width: 65px; border: none; background: none;">
			              				</th>
			              				<th data-order = "d5"><input type="text" id="d5" name="d5" style="width: 65px; border: none; background: none;">
			              				</th>
			              				<th data-order = "d6"><input type="text" id="d6" name="d6" style="width: 65px; border: none; background: none;">
			              				</th>
			              				<th data-order = "d7"><input type="text" id="d7" name="d7" style="width: 72px; border: none; background: none; font-weight: bold;">
			              				</th>
			              				<th  data-order = "tott">Total</th>
			              				<th  data-order = "port">%</th>
			               			</tr>
			               		</thead>
			           			<tbody style="font-size: 13px; text-align: right;">
			           			</tbody>                        	
			         		</table>
			         	</span> 
        			</div>
     			</td>
     		</tr>
     	</table> 	
        <div id='ver_informacion' class='hide' >			 
        <div class="ajaxSorterDiv" id="tabla_vtatalgra" name="tabla_vtatalgra" style="width: 940px; margin-top: -21px;"  > 
     		<span class="ajaxTable" style="height: 240px; margin-top: 1px; " >
         		<table id="mytablaVtatalgra" name="mytablaVtatalgra" class="ajaxSorter" border="1" style="width: 920px" >
           			<thead>     
           				<tr style="font-size: 11px">
               				<th data-order = "nomt">Talla</th>
              				<th data-order = "port">%</th>
               			</tr>
               		</thead>
           			<tbody style="font-size: 10px;">
           			</tbody>                        	
         		</table>
         	</span> 
        </div>
        <div class="ajaxSorterDiv" id="tabla_vtatalgram" name="tabla_vtatalgram" style="width: 940px; margin-top: -21px;"  > 
     		<span class="ajaxTable" style="height: 240px; margin-top: 1px; " >
         		<table id="mytablaVtatalgram" name="mytablaVtatalgram" class="ajaxSorter" border="1" style="width: 920px" >
           			<thead>     
           				<tr style="font-size: 11px">
               				<th data-order = "fecsal"></th>
              				<th data-order = "kgs">kgs</th>
               			</tr>
               		</thead>
           			<tbody style="font-size: 10px;">
           			</tbody>                        	
         		</table>
         	</span> 
        </div>
    	</div>
     </div>
     <div id="costos" class="tab_content" style="margin-top: -15px" >
 			<!--<div class="ajaxSorterDiv" id="tabla_gralz" name="tabla_gralz" style="margin-top: 1px"></div>-->   
 			<div class="ajaxSorterDiv" id="tabla_gralz" name="tabla_gralz" style=" margin-left: -10px;  margin-top: 1px;width:955px;">
             	  <div class="ajaxpager" style="margin-top: -5px;" > 
             	  	<form action="<?= base_url()?>index.php/provta/reportet" method="POST" >
            		<ul class="order_list" style="width: 860px; text-align: left; height: 25px" >
            			<select name="cicact" id="cicact" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:5px;">
      					<?php $ciclof=22; $actual=date("y"); //$actual+=1;
						while($actual >= $ciclof){?>
						<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
						<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            			<?php  $actual-=1; } ?>      									
    					</select>
            			Reporte General 
            			Sección	
            			<!--
            			<select name="seccgz" id="seccgz" style="font-size: 10px; height: 23px;margin-top: 1px;  " >	</select>
            		-->
 						<select name="seccgz" id="seccgz" style="font-size: 12px; height: 25px;margin-top: 1px;  " >		
    						<option value="0">Sel.</option>
    						<option value="41">I</option>		
    						<option value="42">II</option>
    						<option value="43">III</option>
    						<option value="44">IV</option>
    					</select>
 						<input type="hidden" id="filest" name="filest" style="width: 40px;">
						Pls:<input id="prepl" name="prepl" style="border: none; background-color: lightblue; width: 40px; font-size: 10px" readonly="true" />
            			Ali:<input id="preal" name="preal" style="border: none; background-color: lightblue; width: 40px; font-size: 10px" readonly="true" />
            			Com:<input id="preco" name="preco" style="border: none; background-color: lightblue; width: 55px; font-size: 10px" readonly="true" />
            			TC:<input id="tc" name="tc" value="19.9703" style="border: none; background-color: lightblue; width: 40px; font-size: 10px"/>
            			<input type="hidden" name="tabla" value ="" class="htmlTable"/>        	           
            		</ul>
            	</form>	
 				</div>  
             	<span class="ajaxTable" style="height: 590px; background-color: white; width:950px; margin-top: 1px" >
            	   	<table id="mytablagralz" name="mytablagralz" class="ajaxSorter" border="1" style="text-align: center" >
                       <thead >
                       		<tr style="font-size: 10px">
                       			<th rowspan="2" data-order = "pisg">Est</th>
                       			<!--<th rowspan="2" data-order = "hasg">Has</th>-->
                       			<th rowspan="2" data-order = "ppkgs">Talla</th>
                       			<th rowspan="2" data-order = "dc">DC</th>
                       			<!--<th rowspan="2" data-order = "ali">Alimento</th>-->	
                       			<th colspan="1">Postlarvas </br></th>
                       			<th colspan="2">Alimento </br></th>
                       			<th colspan="1">Sueldos</th>	
                       			
                       			<th colspan="2">Combustible </br>
                       				
                       			</th>
                       			<th style="text-align: center">Otros</th>
                       			<th style="text-align: center">Depreciación</th>
                       			<th colspan="2" style="text-align: center">COSTO</th>
                       			<!--
                       			<th colspan="2">Kg. Actuales</th>                       			
                       			<th colspan="4">Venta Sin Cabeza</th>
                       			<th colspan="3">Camarón Bordo</th>
                       			<th style="text-align: center"> </th>
                       			<th colspan="2" style="text-align: center">RENTABILIDAD</th>
                       			<th colspan="2" style="text-align: center">Cosecha Real Actual</th>
                       			-->
                       		</tr>
                       		<tr style="font-size: 10px">
                       			<!--<th data-order = "pp1">PP</th>
                       			<th data-order = "kgs1">Kgs</th>-->
                       			<!--<th data-order = "orgg">Orgs</th>-->
                       			<!--<th data-order = "prepls">Precio</th>-->
                       			<th data-order = "imppls">Importe</th>
                       			<th data-order = "kgsali">Kgs</th>
                       			<th data-order = "impali">Importe</th>
                       			<th data-order = "impsue">Importe</th>
                       			
                       			<!--<th data-order = "preali">Precio</th>-->
                       			
                       			<th data-order = "ltscom">Lts</th>
                       			<!--<th data-order = "precom">Precio</th>-->
                       			<th data-order = "impcom">Importe</th>
                       			<th data-order = "impotc">Importe</th>
                       			<th data-order = "impdep">Importe</th>
                       			<th data-order = "costo" style="text-align: center">M.N.</th>
                       			<th data-order = "costousd" style="text-align: center">U.S.D.</th>
                       			<!--
                       			<th data-order = "ppkgs">grs</th>
                       			<th data-order = "kgskgs">Kgs</th>
                       			<th data-order = "tsc">Talla</th>
                       			<th data-order = "prekgssc">PB</th>
                       			<th data-order = "prevtasc">grs+PB</th>
                       			<th data-order = "impkgssc">Importe</th>
                       			<th data-order = "tcc">Talla</th>
                       			<th data-order = "prekgscc">PB</th>
                       			<th data-order = "prevtacc">grs+PB</th>
                       			<th data-order = "venta" style="text-align: center">VENTA</th>
                       			<th data-order = "renta">%</th>
                       			<th data-order = "imprenta">Importe</th>
                       			<th data-order = "kgsbor">Kgs</th>
                       			<th data-order = "vtabor">Importe</th>-->
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 11px; text-align: right">
                       	</tbody>                        	     	
                   	</table>
                </span> 
 			</div>
 			<button id="export_datagz" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
				<form id="data_tablesgz" action="<?= base_url()?>index.php/general/pdfrepcosg" method="POST">
					<input type="hidden" name="tablag" id="html_gralz"/> <input type="hidden" name="gra1" id="gra1"/>
				</form>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#export_datagz').click(function(){									
								$('#html_gralz').val($('#mytablagralz').html());
								$('#data_tablesgz').submit();
								$('#html_gralz').val('');			
						});
					});
				</script>

 		</div>
 		<div id="granja" class="tab_content" style="margin-top: 1px;"> 
     	<div class="ajaxSorterDiv" id="tabla_estsema" name="tabla_estsema" style="width: 960px; margin-top: -10px; margin-left: -10px;"  >  
     		<span class="ajaxTable" style="height: 615px; margin-top: -1px;width: 955px; " >
				<table id="mytablaEstSema" name="mytablaEstSema" class="ajaxSorter" border="1" >
                	<thead >     
                    	<tr style="font-size: 9px">
                    		<th data-order = "pisg" rowspan="3" >Est</th>
                    		<th data-order = "hasgg" rowspan="3" >Has</th>
                    		<th rowspan="2" colspan="2" >Siembra</th>
                    		<th rowspan="2" >Edad</th>
                    		<th rowspan="2" >Den</th>
                    		<th rowspan="2" >Pob</th>
                    		<th rowspan="2" >Sob</th>
                    		<th rowspan="1" colspan="3" >Biometría</th>
                    		<th rowspan="2" colspan="2" >Biomasa</th>
                    		<th colspan="8" >Alimento Kgs</th>
                    		<th rowspan="2" ></th>                    		
                        </tr>                       
                        <tr style="font-size: 9px">
                        	<th colspan="3" >Peso</th>                        	
                        	<th colspan="3" >Soya</th>
                        	<th colspan="3" >Balanceado</th>
                        	<th >Ambos</th>
                        	<th >kgs/</th>
                        </tr>
                        <tr style="font-size: 9px">
                        	<th data-order = "orgsm1">org/m2</th>
                            <th data-order = "fecg1">Fecha</th>
                            <th data-order = "dias">Dias</th>
                            <th data-order = "orgsm">org/m2</th>
                            <th data-order = "orgs">Orgs.</th>
                            <th data-order = "por">%</th>
                            <th data-order = "pesb">Act</th>
                            <th data-order = "pesa">Ant</th>
                            <th data-order = "incp">Inc</th>
                            <th data-order = "bioh">Kh/Ha</th>
                            <th data-order = "bio">Kg/Est</th>
                            <th data-order = "semsoy">Sem</th>
                       		<th data-order = "acusoy">AcuAnt</th>
                       		<th data-order = "totsoy">Total</th>
                       		<th data-order = "sembal">Sem</th>
                       		<th data-order = "acubal">AcuAnt</th>
                       		<th data-order = "totbal">Total</th>
                            <th data-order = "soybal">Acu</th>
                            <th data-order = "kgshas">Ha</th>
                            <th data-order = "fca">FCA</th>                            
                        </tr>
                   	</thead>
                   	<tbody style="font-size: 11px; text-align: center">
                   	</tbody>                        	
                </table>
            </span> 
        
            <div class="ajaxpager" style=" font-size: 12px; margin-top: -10px">
             	<form action="<?= base_url()?>index.php/aqpgranjac2/reportesem" method="POST" >
					
					<ul class="order_list" style="margin-top: 1px; width: 880px;"> 
						<select name="cmbCicloGra" id="cmbCicloGra" style="font-size: 10px; height: 25px;margin-top: -5px; margin-left:-3px;">
      						<?php $ciclof=22; $actual=date("y"); //$actual+=1;
								while($actual >= $ciclof){?>
								<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
								<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
            					<?php  $actual-=1; } ?>      									
    					</select>
						
    					Sección:
    						<select name="secc" id="secc" style="font-size: 12px; height: 25px;margin-top: 1px;  " >				<option value="0">Sel.</option>
    							<option value="41">I</option>		
    							<option value="42">II</option>
    							<option value="43">III</option>
    							<option value="44">IV</option>
    						</select>
    					<?php
    						$sem=0;
							$this->load->model('aqpgranjac2_model'); 
							$sem=$this->general_model->verFechat(); 
						?>
						Cierre:
             				<select name="idFec" id="idFec" style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
          				<?php	
           					$this->load->model('aqpgranjac2_model'); 
							$data['result']=$this->general_model->verFecha(); $fec=new Libreria();
							foreach ($data['result'] as $row):    $row->fecb1 = $fec->fecha($row->fecb); ?> 
           						<option value="<?php echo $sem.':'.$row->fecb;?>" ><?php echo 'Sem '.$sem.': '.$row->fecb1;?></option>
           						<?php $sem-=1;
           					endforeach; ?>
   						</select>
             		</ul>						
    	    	    <img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	        <input type="hidden" name="tablasem" value ="" class="htmlTable"/>          	            	    	   
        	    </form>   
            </div>  
       	</div>	

     </div>
     </div>
     
     
     
     
 	<?php } ?>
</div>
<?php $this->load->view('footer_cv'); ?>
<script type="text/javascript">
	$("#cmbGra").change( function(){
		$("#gra1").val($("#cmbGra").val());$("#gra1d").val($("#cmbGra").val());
	});		
	$("#Grafi").change( function(){
		$ciclo=$("#cicloG").val();
		if($("#cmbGra").val()=='4') $granja='Ahome';
		if($("#cmbGra").val()=='10') $granja='Topolobampo';
		$vtabordo=$("#vtabordo").val();$vtamaquila=$("#vtamaquila").val();
		$gtoalimento=$("#gtoalimento").val();$gtomaquila=$("#gtomaquila").val();$gtocombustible=$("#gtocombustible").val();
		$gtosueldo=$("#gtosueldo").val();
		$vtabordo=parseFloat($vtabordo);$vtamaquila=parseFloat($vtamaquila);
		$gtoalimento=parseFloat($gtoalimento);$gtomaquila=parseFloat($gtomaquila);$gtocombustible=parseFloat($gtocombustible);
		$gtosueldo=parseFloat($gtosueldo);
		$ingtot=($vtabordo+$vtamaquila);
		$gtotot=($gtoalimento+$gtomaquila+$gtocombustible+$gtosueldo);
		const options2 = { style: 'currency', currency: 'USD' };
		const numberFormat2 = new Intl.NumberFormat('en-US', options2);
		//console.log(numberFormat2.format($ingtot));
		$("#ingtot").val(numberFormat2.format($ingtot));
		$("#gtotot").val(numberFormat2.format($gtotot));
		$("#sdotot").val(numberFormat2.format($ingtot-$gtotot));
		var inputVal = document.getElementById("sdotot");
    	if (($ingtot-$gtotot) > 0) {
        	inputVal.style.backgroundColor = "#070";
        	inputVal.style.color = "white";
    	}else if(($ingtot-$gtotot) == 0){
        	inputVal.style.backgroundColor = "orange";
        	inputVal.style.color = "white";
    	}else if(($ingtot-$gtotot) < 0){
    		inputVal.style.backgroundColor = "red";
    		inputVal.style.color = "white";
    	}
		Highcharts.chart('grafica', {
    		chart: {type: 'bar'},
		    title: {text: 'Ciclo: 20'+$ciclo+' Oceano Azul '+$granja},
		    xAxis: {categories: ['Ingresos', 'Costos']
			},
		    yAxis: {
		        min: 0,
		        title: {text: 'Importes M.N.'}
		    },
		    legend: { reversed: true  },
		    plotOptions: {
		        series: {  stacking: 'normal' }
		    },
		    series: [{
		        name: 'Otros',data: [0,0],dataLabels: { enabled: true}
		    }, {
		        name: 'Sueldos',data: [0,$gtosueldo],dataLabels: { enabled: true}
		    }, {
		        name: 'Combustible',data: [0,$gtocombustible],dataLabels: { enabled: true}
		    }, {
		        name: 'Maquila',data: [0,$gtomaquila],dataLabels: { enabled: true}
		    }, {
		        name: 'Alimento',data: [0,$gtoalimento],dataLabels: { enabled: true}
		    },{
		        name: 'Vta. Maquila',data: [$vtamaquila,0],dataLabels: { enabled: true}
		    }, {
		        name: 'Vta. Bordo', data: [$vtabordo,0],dataLabels: { enabled: true}
		    } ]
		    
		});
	});			
	function refrescar(){
    	//Actualiza la página
    	location.reload();
    	//$("#mytablaBordo").trigger("update");
    	//$("#mytablaVentas").trigger("update");
    	//$("#mytablaMaquila").trigger("update");
    	//$("#mytablaGastos").trigger("update");
  	}
/*$("#dia").change( function(){	
	var f = new Date($("#dia").val());
	$("#d7").val((f.getDate() -0) + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
	$("#d6").val((f.getDate() -1) + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
	$("#d5").val((f.getDate() -2) + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
	$("#d4").val((f.getDate() -3) + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
	$("#d3").val((f.getDate() -4) + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
	$("#d2").val((f.getDate() -5) + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
	$("#d1").val((f.getDate() -6) + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
});	*/
$("#dia").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});	
$("#del").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});	

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});
	$(document).ready(function(){
		//setTimeout(refrescar, 60000);
		$('#ver_informacion').hide();
		var f = new Date();
		$("#dia").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
		$("#cmbMes").val((f.getMonth() +1));
		$("#cmbGra").change(); 
		$("#tabla_bordo").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablab',  
			filters:['cicloG','cmbGra'],
        	sort:false,  
        	onSuccess:function(){ 
        		$('#tabla_bordo tbody tr td:nth-child(6)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#200");$(this).css("color", "white");
	    		});
        		$('#tabla_bordo tbody tr td:nth-child(8)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#559");$(this).css("color", "white");
	    		});
	    		$('#tabla_bordo tbody tr td:nth-child(10)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#359");$(this).css("color", "white");
	    		});
        		$('#tabla_bordo tbody tr').map(function(){
		    	if($(this).data('cicg')=='Total') {	    				    			
	    			//$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    })
			},       
		});
		$("#tabla_ventas").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablav',  
			filters:['cicloG','cmbGra','tip1','dia'],
        	sort:false,  
        	onSuccess:function(){ 
        		$('#tabla_ventas tbody tr td:nth-child(2)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#559");$(this).css("color", "white");
	    		});
	    		$('#tabla_ventas tbody tr td:nth-child(3)').map(function(){$(this).css('font-weight','bold');});
	    		$('#tabla_ventas tbody tr td:nth-child(4)').map(function(){$(this).css('font-weight','bold');
	    		$(this).css("color", "blue");
	    		});
        		$('#tabla_ventas tbody tr td:nth-child(6)').map(function(){$(this).css('font-weight','bold');});
	    		$('#tabla_ventas tbody tr td:nth-child(7)').map(function(){ 
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			if($(this).html() >= -5.00 && $(this).html() <= 5.00 && $(this).html() != '' ) {
             			$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        			}else if($(this).html() >= 10.01 ||  $(this).html() <= -10.01 && $(this).html() != '' ) {
              			$(this).css("background-color", "red");$(this).css("color", "white");
        			}else if($(this).html() >= 5.01 || $(this).html() <= 10.0 && $(this).html() != ''){
        				$(this).css("background-color", "orange");$(this).css("color", "white");
        			}
        		});
        		$('#tabla_ventas tbody tr').map(function(){	    		
	    			$("#vtabordo").val($(this).data('impgrafica'));
	    		}); 
        		$("#Grafi").change(); 
			},       
		});
		$("#tabla_maquila").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablav',  
			filters:['cicloG','cmbGra','tip2','dia'],
        	sort:false,  
        	onSuccess:function(){ 
        		$('#tabla_maquila tbody tr td:nth-child(2)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#359");$(this).css("color", "white");
	    		});
	    		$('#tabla_maquila tbody tr td:nth-child(3)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#360");$(this).css("color", "white");
	    		});
	    		$('#tabla_maquila tbody tr td:nth-child(4)').map(function(){ 
	    		$(this).css('font-weight','bold');
	    			if($(this).html() >= 70.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 69.5 && $(this).html() <= 69.99 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() <= 69.5 && $(this).html() != '' ) {
              			$(this).css("background-color", "red"); $(this).css("color", "white");
        			}
        		}); 
        		$('#tabla_maquila tbody tr td:nth-child(6)').map(function(){$(this).css('font-weight','bold');});
        		$('#tabla_maquila tbody tr td:nth-child(8)').map(function(){$(this).css('font-weight','bold');});
        		$('#tabla_maquila tbody tr td:nth-child(9)').map(function(){$(this).css('font-weight','bold');
	    		$(this).css("color", "blue");});
        		$('#tabla_maquila tbody tr td:nth-child(11)').map(function(){$(this).css('font-weight','bold');});
	    		$('#tabla_maquila tbody tr td:nth-child(12)').map(function(){ 
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			if($(this).html() >= -5.00 && $(this).html() <= 5.00 && $(this).html() != '' ) {
             			$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        			}else if($(this).html() >= 10.01 ||  $(this).html() <= -10.01 && $(this).html() != '' ) {
              			$(this).css("background-color", "red");$(this).css("color", "white");
        			}else if($(this).html() >= 5.01 || $(this).html() <= 10.0 && $(this).html() != ''){
        				$(this).css("background-color", "orange");$(this).css("color", "white");
        			}
        		});
        		$('#tabla_maquila tbody tr').map(function(){	    		
	    			$("#vtamaquila").val($(this).data('impgrafica'));
	    		}); 
        		$("#Grafi").change(); 
			},       
		});
		$("#tabla_gastos").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablag',  
			filters:['cicloG','cmbGra'],
        	sort:false,  
        	onSuccess:function(){ 
        		$('#tabla_gastos tbody tr td:nth-child(3)').map(function(){$(this).css("color", "red");});
        		$('#tabla_gastos tbody tr td:nth-child(4)').map(function(){$(this).css('font-weight','bold');});
        		$('#tabla_gastos tbody tr td:nth-child(6)').map(function(){$(this).css("color", "red");});
        		$('#tabla_gastos tbody tr td:nth-child(7)').map(function(){$(this).css('font-weight','bold');});
        		$('#tabla_gastos tbody tr td:nth-child(9)').map(function(){$(this).css("color", "red");});
        		$('#tabla_gastos tbody tr td:nth-child(10)').map(function(){$(this).css('font-weight','bold');});
        		$('#tabla_gastos tbody tr td:nth-child(11)').map(function(){$(this).css("color", "red");});
        		$('#tabla_gastos tbody tr td:nth-child(12)').map(function(){$(this).css('font-weight','bold');});
        		$('#tabla_gastos tbody tr td:nth-child(13)').map(function(){$(this).css("color", "red");});
        		$('#tabla_gastos tbody tr td:nth-child(14)').map(function(){$(this).css('font-weight','bold');});
        		$('#tabla_gastos tbody tr td:nth-child(15)').map(function(){$(this).css('font-weight','bold');
	    		$(this).css("color", "red");});
	    		$('#tabla_gastos tbody tr').map(function(){	    		
	    			$("#gtoalimento").val($(this).data('aligrafica'));$("#gtomaquila").val($(this).data('maqgrafica'));
	    			$("#gtocombustible").val($(this).data('comgrafica'));$("#gtosueldo").val($(this).data('suegrafica'));
	    		}); 
        		$("#Grafi").change(); 
			},       
		});
		$("#tabla_bordia").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablabord',  
			filters:['cicloG','cmbGra','dia'],
        	sort:false,  
        	onSuccess:function(){ 
        		$('#tabla_bordia tbody tr td:nth-child(2)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#200");$(this).css("color", "white");
	    		});
        		$('#tabla_bordia tbody tr td:nth-child(3)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#559");$(this).css("color", "white");
	    		});
	    		$('#tabla_bordia tbody tr td:nth-child(5)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#359");$(this).css("color", "white");
	    		});
			},       
		});
		$("#tabla_borvdia").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablaborvd',  
			filters:['cicloG','cmbGra','dia'],
        	sort:false,  
        	onSuccess:function(){ 
        		$('#tabla_borvdia tbody tr td:nth-child(3)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#559");$(this).css("color", "white");
	    		});
	    		$('#tabla_borvdia tbody tr td:nth-child(4)').map(function(){$(this).css("text-align", "right");$(this).css("color", "blue");});
			},       
		});
		$("#tabla_maqdia").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablamaqd',  
			filters:['cicloG','cmbGra','dia'],
        	sort:false,  
        	onSuccess:function(){ 
        		$('#tabla_maqdia tbody tr td:nth-child(3)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#359");$(this).css("color", "white");
	    		});
	    		$('#tabla_maqdia tbody tr td:nth-child(4)').map(function(){
	    			$(this).css('font-weight','bold');
	    			$(this).css("background-color", "#360");$(this).css("color", "white");
	    		});
        		$('#tabla_maqdia tbody tr td:nth-child(5)').map(function(){ 
	    			$(this).css('font-weight','bold');
	    			if($(this).html() >= 70.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 69.5 && $(this).html() <= 69.99 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() <= 69.5 && $(this).html() != '' ) {
              			$(this).css("background-color", "red"); $(this).css("color", "white");
        			}
        			
	    		});
        		$('#tabla_maqdia tbody tr td:nth-child(7)').map(function(){$(this).css("text-align", "right");$(this).css("color", "red");});	
			},       
		});
		$("#tabla_prodia").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablaprod',  
			filters:['cicloG','cmbGra','dia'],
        	sort:false,  
        	onSuccess:function(){ 
        		$('#tabla_prodia tbody tr td:nth-child(1)').map(function(){$(this).css("font-weight", "bold");});
        		$('#tabla_prodia tbody tr td:nth-child(2)').map(function(){$(this).css("text-align", "left");});	
        		$('#tabla_prodia tbody tr td:nth-child(4)').map(function(){$(this).css("text-align", "right");$(this).css("color", "blue");});
        		$('#tabla_prodia tbody tr td:nth-child(5)').map(function(){$(this).css("text-align", "right");$(this).css("font-weight", "bold");});
        		$('#tabla_prodia tbody tr td:nth-child(6)').map(function(){ 
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			if($(this).html() > -6.00 && $(this).html() < 6.00 && $(this).html() != '' ) {
             			$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        			}else if($(this).html() >= 11.00 ||  $(this).html() <= -11.00 && $(this).html() != '' ) {
              			$(this).css("background-color", "red");$(this).css("color", "white");
        			}else if($(this).html() >= 5.01 || $(this).html() < 11.0 && $(this).html() != ''){
        				$(this).css("background-color", "orange");$(this).css("color", "white");
        			}
        		});
        		$('#tabla_prodia tbody tr').map(function(){
		    		if($(this).data('clipd')=='Total' || $(this).data('proad')=='Total') {	
	    				$(this).css('background','lightyellow');
	    				$(this).css('font-weight','bold');
	    				$(this).css('text-align','center');
	    			}
		    	})
			},       
		});
		$("#tabla_depdia").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tabladepd',  
			filters:['cmbGra','dia'],
        	sort:false,  
        	onSuccess:function(){ 
        		$('#tabla_depdia tbody tr td:nth-child(1)').map(function(){$(this).css("font-weight", "bold");});
        		$('#tabla_depdia tbody tr td:nth-child(2)').map(function(){$(this).css("text-align", "left");});
        		$('#tabla_depdia tbody tr td:nth-child(3)').map(function(){$(this).css("text-align", "right");$(this).css("color", "blue");});	
        		$('#tabla_depdia tbody tr td:nth-child(4)').map(function(){$(this).css("text-align", "right");$(this).css("color", "blue");});
        		$('#tabla_depdia tbody tr').map(function(){
		    		if($(this).data('razon')=='Total' || $(this).data('destd')=='Total') {	
	    				$(this).css('background','lightyellow');
	    				$(this).css('font-weight','bold');
	    				$(this).css('text-align','center');
	    			}
		    	})
			},       
		});
		
		$("#tabla_vtatalg").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablavtatal',  
			filters:['cicloG','cmbGra','dia','g1'],
        	sort:false,  
        	onSuccess:function(){ 
        		//$('#tabla_vtatalg tbody tr td:nth-child(1)').map(function(){$(this).css("font-weight", "bold");});
        		$('#tabla_vtatalg tbody tr td:nth-child(8)').map(function(){$(this).css("color", "blue");$(this).css("font-weight", "bold");$(this).css('text-align','center');});
        		$('#tabla_vtatalg tbody tr td:nth-child(9)').map(function(){$(this).css("font-weight", "bold");});
        		$('#tabla_vtatalg tbody tr td:nth-child(10)').map(function(){$(this).css("font-weight", "bold");});
        		$('#tabla_vtatalg tbody tr').map(function(){
		    		if($(this).data('nomt')=='Total' || $(this).data('destd')=='Total') {	
	    				$(this).css('background','lightyellow');
	    				$(this).css('font-weight','bold');
	    				$(this).css('text-align','center');
	    			}
		    	});
        		$('#tabla_vtatalg tbody tr').map(function(){
        			$("#d1").val($(this).data('dt1'));$("#d2").val($(this).data('dt2'));$("#d3").val($(this).data('dt3'));
        			$("#d4").val($(this).data('dt4'));$("#d5").val($(this).data('dt5'));$("#d6").val($(this).data('dt6'));
        			$("#d7").val($(this).data('dt7'));
        		});
        		$('#tabla_vtatalg tr').map(function(){	    		
	    			if($(this).data('d7')!='' && $(this).data('nomt')!='Total' && $(this).data('nomt')!='Talla'){
	    				//$(this).css('background','lightgray');
	    				$(this).css('font-weight','bold');
	    			}
	    		});
			},       
		});
		$("#tabla_vtatalgra").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablavtatal',   
			filters:['cicloG','cmbGra','dia','g2'],
        	sort:false,  
        	onSuccess:function(){ 
        		$('#tabla_vtatalgra tbody tr').map(function(){
        			inidia=$(this).data('dt1');
        			findia=$(this).data('dt7');
        		});
        		var chart; 
				var pieColors = (function () {
				    var colors = [],
				        base = Highcharts.getOptions().colors[0],
				        i;
				    for (i = 0; i < 10; i += 1) {
				        // Start out with a darkened base color (negative brighten), and end
				        // up with a much brighter color
				        colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
				    }
				    return colors;
				}());

				// Build the chart
				Highcharts.chart('gravtasem', {
				     data: {
				        table: 'mytablaVtatalgra'
				    },
				    chart: {
				        plotBackgroundColor: null,
				        plotBorderWidth: null,
				        plotShadow: false,
				        type: 'pie'
				    },
				    title: {
				        text: 'Ventas Semanales por Talla del día '+inidia+' al '+findia,
				        style: {fontSize: '12px',fontWeight: 'bold',color: 'navy',}
				    },
				    tooltip: {
				        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				    },
				    accessibility: {
				        point: {
				            valueSuffix: '%'
				        }
				    },
				    plotOptions: {
				        pie: {
				            allowPointSelect: true,
				            cursor: 'pointer',
				            colors: pieColors,
				            dataLabels: {
				                enabled: true,
				                format: '<b>{point.name}</b><br>{point.percentage:.2f} %',
				                distance: -50,
				                filter: {
				                    property: 'percentage',
				                    operator: '>',
				                    value: 4
				                }
				            }
				        }
				    },
				});	
			},       
		});
		$("#tabla_vtatalgram").ajaxSorter({
			url:'<?php echo base_url()?>index.php/general/tablavtatalm',   
			filters:['cicloG','cmbGra','cmbMes','g2'],
        	sort:false,  
        	onSuccess:function(){ 
        		dia=$("#cmbMes").val();
        		if(dia==0) dia='Ciclo';	if(dia==1) dia='Enero';	if(dia==2) dia='Febrero'; if(dia==3) dia='Marzo';
        		if(dia==4) dia='Abril'; if(dia==5) dia='Mayo';	if(dia==6) dia='Junio'; if(dia==7) dia='Julio';
        		if(dia==8) dia='Agosto'; if(dia==9) dia='Septiemnbre';	if(dia==10) dia='Octubre';
        		if(dia==11) dia='Noviembre'; if(dia==12) dia='Diciembre';
				Highcharts.chart('gravtames', {
				    data: {table: 'mytablaVtatalgram'},
				    chart: {type: 'line'},
				    title: {text: 'Ventas Producto Maquilado - '+dia, style: {fontWeight: 'bold',color: 'navy',}
					},
				    /*subtitle: {text: 'Actuales en ciclo'},*/
				    xAxis: {
				        labels: {style: {fontSize: '8px',fontWeight: 'bold',color: 'navy',}}
				    },
				    yAxis: {
				        title: {text: 'kgs'}
				    },
				    plotOptions: {
				        line: {
				            dataLabels: {enabled: true},
				            //format: '<b>{point.name}</b>',
				            //tooltip: {
        						//headerFormat: '<b>{point.x}</b><br/>',
        						//pointFormat: '{point.name}: {point.y}'
    						//}
				            enableMouseTracking: false
				        }
				    },				    
				});
			},       
		});

		$("#tabla_gralz").ajaxSorter({
		url:'<?php echo base_url()?>index.php/provta/tablagral', 
		filters:['cicact','cmbGra','seccgz','tc'],
		sort:false,	
		onSuccess:function(){ 
			$('#tabla_gralz tbody tr').map(function(){
		    	$("#prepl").val($(this).data('prepls'));
		    	$("#preal").val($(this).data('preali'));
		    	$("#preco").val($(this).data('precom'));
		    })	
		    $('#tabla_gralz tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','center');$(this).css('font-weight','bold'); $(this).css('background','lightblue');})
		    $('#tabla_gralz tbody tr td:nth-child(12)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); $(this).css('background','lightblue');})	
		    $('#tabla_gralz tbody tr td:nth-child(13)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); $(this).css('background','lightgreen');})	
		    //$('#tabla_gralz tbody tr td:nth-child(12)').map(function(){ $(this).css('font-size','8px');})
		    $('#tabla_gralz tbody tr td:nth-child(15)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); $(this).css('background','lightblue');})
		    $('#tabla_gralz tbody tr').map(function(){
		    	if($(this).data('pisg')=='Total') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    });
		    $('#tabla_gralz tbody tr td:nth-child(16)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 35 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 20 && $(this).html() <= 34.99 && $(this).html() != '' ) {
                		$(this).css("background-color", "yellow"); $(this).css("color", "black");
            		}
            		if($(this).html() >= 0 && $(this).html() <= 19.99 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "white");
        			}
        			
	    	});  
	    	$('#tabla_gralz tbody tr td:nth-child(17)').map(function(){ 
				$(this).css('font-weight','bold');	    		
	    	});
	    	$('#tabla_gralz tbody tr td:nth-child(19)').map(function(){ 
				$(this).css('font-weight','bold');	     $(this).css('text-align','right');		
	    	});
		},	
	});

	$("#tabla_estsema").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjac2/tablaestsem',  
		filters:['cmbGra','cmbCicloGra','idFec','secc'],
        sort:false,
      //  multipleFilter:true,
        onSuccess:function(){
    		importe = 0;
    		c1='rgb(169,209,141)';
    	   	/*$('#tabla_estsema tbody tr').map(function(){	    		
	    		$(this).css('text-align','right');
	    	});*/	
	    	$('#tabla_estsema tbody tr').map(function(){
		    	if($(this).data('pisg')=='Tot') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			
	    		}
		    }); 
		    $('#tabla_estsema tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');});
		    $('#tabla_estsema tbody tr td:nth-child(2)').map(function(){$(this).css('font-size','9px');});
		    $('#tabla_estsema tbody tr td:nth-child(3)').map(function(){$(this).css('font-size','9px');});
		    $('#tabla_estsema tbody tr td:nth-child(4)').map(function(){$(this).css('font-size','9px');});
		    $('#tabla_estsema tbody tr td:nth-child(5)').map(function(){$(this).css('font-size','9px');});
		    $('#tabla_estsema tbody tr td:nth-child(6)').map(function(){$(this).css('font-weight','bold');});
		    $('#tabla_estsema tbody tr td:nth-child(9)').map(function(){$(this).css('font-weight','bold');});
		    $('#tabla_estsema tbody tr td:nth-child(10)').map(function(){$(this).css('font-size','10px');});
		    $('#tabla_estsema tbody tr td:nth-child(11)').map(function(){$(this).css('font-size','9px');});
		    $('#tabla_estsema tbody tr td:nth-child(12)').map(function(){$(this).css('font-weight','bold'); $(this).css('background',c1); })
		    $('#tabla_estsema tbody tr td:nth-child(13)').map(function(){$(this).css('font-size','10px');});
		    $('#tabla_estsema tbody tr td:nth-child(14)').map(function(){$(this).css('font-weight','bold'); $(this).css('background','rgb(132,151,176)'); })
		    $('#tabla_estsema tbody tr td:nth-child(15)').map(function(){$(this).css('font-size','10px');});
		    $('#tabla_estsema tbody tr td:nth-child(16)').map(function(){ $(this).css('background','rgb(132,151,176)'); })
		    $('#tabla_estsema tbody tr td:nth-child(18)').map(function(){$(this).css('font-size','10px');});
		    $('#tabla_estsema tbody tr td:nth-child(17)').map(function(){$(this).css('font-weight','bold'); $(this).css('background','yellow'); })   	
		    $('#tabla_estsema tbody tr td:nth-child(19)').map(function(){ $(this).css('background','yellow'); })
		    $('#tabla_estsema tbody tr td:nth-child(20)').map(function(){$(this).css('font-weight','bold'); $(this).css('background','rgb(0,176,80)'); })
		    $('#tabla_estsema tbody tr td:nth-child(21)').map(function(){$(this).css('font-weight','bold');});
		    $('#tabla_estsema tbody tr td:nth-child(22)').map(function(){$(this).css('font-weight','bold');});
    	},
    });	

	}); 
</script>
