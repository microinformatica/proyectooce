<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/sagarpa4.jpg" width="25" height="25" border="0"> Asignación </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px" >
		    <div class="ajaxSorterDiv" id="tabla_cerasi" name="tabla_cerasi" align="left"  >             	
            	<span class="ajaxTable" style=" height: 295px; width: 935px; background-color: white; margin-top: -5px ">
                    <table id="mytablaCasi" name="mytablaCasi" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <th data-order = "FechaR" style="width: 70px" >Fecha</th>                            
                            <th data-order = "RemisionR" style="width: 50px">Remisión</th>
                            <th data-order = "Razon" >Razón Social</th>
                            <th data-order = "Cantidad" style="width: 80px">Postlarvas</th>
                            <th data-order = "NumCer" style="width: 70px">Certificado</th>                                                       
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                 </span>
                 <div class="ajaxpager" >        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/certificadosasi/pdfrep" >
                    	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                    </form>   
 				</div>
 			</div>
 					<table border="2px" style="width: 650px; margin-top: -30px; ">
 						<thead style="background-color: #DBE5F1">
 							<tr >
 								<th colspan="2" style="text-align: center">1.- Registro de Certificado a Remisión No. 
 								<input size="2%" readonly="true" type="text" name="rem" id="rem" style="text-align: left;  border: 0; background: none">
 								<input readonly="true" type="hidden" name="id" id="id" style="text-align: left;  border: 0; background: none">
 								<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
 								</th>
 								<th colspan="3" style="text-align: center">2.- Asignación Grupal &nbsp;&nbsp;
 								<input style="font-size: 14px" type="submit" id="aceptarg" name="aceptarg" value="Procesar" /></th>
 							</tr>
 						</thead>
 						<tbody style="background-color: #F7F7F7">
 							<tr>
 								<th style="text-align: center">Razón Social</th>
 								<th style="text-align: center">Certificado</th> 								
 								<th style="text-align: center">Iniciar</th>
 								<th style="text-align: center">Finalizar</th>
 								<th style="text-align: center">Certificado</th>
 								
 							</tr>
 							<tr style="background-color: white;">
	 							<th style="text-align: center"><input readonly="true" size="60%" type="text" name="raz" id="raz" style="text-align: center;  border: 0; background: none"></th>
	 							<th>
									<select name="certi" id="certi" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          								<?php	          							
          									$data['result']=$this->certificadosasi_model->verCertificado();
											foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->NumReg;?>" ><?php echo $row->NumCer;?></option>
           									<?php endforeach;?>
   									</select>
   								</th>
	 							<th style="text-align: center">
	 								<select name="inicio" id="inicio"  style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
           								<?php	
           									//$this->load->model('cargos_model');
											$data['result']=$this->certificadosasi_model->verRemision();
											foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->NumRegR;?>" ><?php if($row->NumRegR==0) { echo "SN";} else { echo $row->RemisionR;}?></option>
           									<?php endforeach;?>
    								</select>
	 							</th>								
								<th style="text-align: center">
									<select name="fin" id="fin"  style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
           								<?php	
           									//$this->load->model('cargos_model');
											$data['result']=$this->certificadosasi_model->verRemision();
											foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->NumRegR;?>" ><?php if($row->NumRegR==0) { echo "SN";} else { echo $row->RemisionR;}?></option>
           									<?php endforeach;?>
    								</select>
								</th>
								<th>
									<select name="certi1" id="certi1" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          								<?php	          							
          									$data['result']=$this->certificadosasi_model->verCertificado();
											foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->NumReg;?>" ><?php echo $row->NumCer;?></option>
           									<?php endforeach;?>
   									</select>
   								</th>
 							</tr>
 						</tbody>
 					</table>  
 					<table  style="width: 510px; height: 90px; margin-top: -25px">
 							<tr>
 								<th colspan="5">
 									<div class="ajaxSorterDiv" id="tabla_cer" name="tabla_cer" align="left">             	
            							<span class="ajaxTable" style=" height: 95px; width: 500px; background-color: white ">
                    						<table id="mytablaC" name="mytablaC" class="ajaxSorter" style="font-size: 10px" >
                        						<thead >                         	
                            						<th data-order = "NumCer" >Certificado</th>                            
                            						<th data-order = "IniVig" >Inicia</th>
                            						<th data-order = "FinVig" >Finaliza</th>
                            						<th data-order = "pos" >Autorizados</th>                                                       
                            						<th data-order = "ent" >Asignados</th>
                            						<th data-order = "dif" >Diferencia</th>
                        						</thead>
                        						<tbody style="font-size: 10px">
                        						</tbody>
                    						</table>
                 						</span>                 
 									</div>
 								</th>
 							</tr>
 					</table> 
            </div>             
    
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#aceptar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/certificadosasi/actualizar", 
				data: "id="+$("#id").val()+"&cer="+$("#certi").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaCasi").trigger("update");$("#mytablaC").trigger("update");
								alert("Datos actualizados correctamente");										
								
								$("#id").val('');$("#raz").val('');$("#rem").val('');$("#certi").val('');																					
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder actualizar datos");				
		return false;
	}	 
});
$("#aceptarg").click( function(){	
	ini=$("#inicio").val();
	fin=$("#fin").val();
	
	//if(ini>fin){
	//	alert(ini+fin);		
	//	$("#inicio").focus();		
	//	return false;
	//}else{
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/certificadosasi/actualizarg", 
				data: "cer="+$("#certi1").val()+"&ini="+$("#inicio").val()+"&fin="+$("#fin").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaCasi").trigger("update");$("#mytablaC").trigger("update");
								//alert("Datos actualizados correctamente");										
																			
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
		
//	}	 
});


$(document).ready(function(){ 
	$("#tabla_cerasi").ajaxSorter({
		url:'<?php echo base_url()?>index.php/certificadosasi/tabla',  		
		//filters:['cer'],
    	sort:false,
    	onRowClick:function(){
    		$("#id").val($(this).data('numregr'));
			$("#raz").val($(this).data('razon'));
			$("#rem").val($(this).data('remisionr'));
			$("#certi").val($(this).data('numcerr'));
    	},   
    	onSuccess:function(){
    		$('#tabla_cerasi tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','center');  })
    		$('#tabla_cerasi tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center');  })
    	   	$('#tabla_cerasi tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');  })
    	   	$('#tabla_cerasi tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','center');  })
    	},              
	});
	$("#tabla_cer").ajaxSorter({
		url:'<?php echo base_url()?>index.php/certificadosasi/tablac',  				
    	sort:false,
   	});
});  	
</script>