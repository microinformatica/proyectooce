<?php $this->load->view('header_cv'); ?>

<style>
	.menu{
		display:inline-block;
	}
	.menu li{
		margin-right:15px;
		margin-top:15px;
		padding:0;
		float:left;
	}
	.menu li a img{
		width: 42px;
		display:inline-block;
	}
	.ui-dialog-titlebar-close{
		visibility: hidden;
	}
	.menu li a{
		display:block;
		text-align:center;
	}
	.menu li a:hover{
		opacity:1;
		text-decoration:none;
	}
	.menu li a div{
		display:inline-block;
	}
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#opciones"> Opciones de Trabajo </a></li>
		<li><a href="#directorio"> @ contactos</a></li>		
		</strong>
	</ul>	
	<div class="tab_container" id="tabPrincipal">
			<div id="opciones" class="tab_content" style="overflow-x:hidden;" >
				<ul class="menu" >
					<?php foreach ($result as $row):
					if($row->modulo!="Inicio"){
					?>
					<li>
						<?php if($row->controlador!=""){ ?>
						<a href="<?= base_url()?>index.php/<?php echo $row->controlador; ?>" >
						<img src="<?= base_url()?>assets/images/menu/<?php echo $row->imagen; ?>" />
						<br>						
						<div style="margin-top: 1px" > <?php echo $row->modulo; ?></div>
						</a>
						<?php } else { ?>
							<a href="#" >
								<img src="<?= base_url()?>assets/images/menu/<?php echo $row->imagen; ?>" />
								<br>						
								<!--<div style="margin-top: 1px" > <?php echo $row->modulo; ?></div>-->
								<div style="margin-top: 1px" > En Construcción</div>
							</a>	
						<?php } ?>
					</li>
					<?php }	endforeach;?>
				</ul>
			</div>
			<div id="directorio" class="tab_content" style="overflow-x:hidden;" >
				<ul class="menu">
					<table cellspacing="1" class="ajaxSorter" width="790px" border="0" >
						<thead>
							<tr>
								<th><img width="60px"  src="<?= base_url()?>assets/images/correo.png" /></th>
								<th colspan="9" style="width: 470px; font-size: 50px">Directorio Corporativo</th>
							</tr>			
							<tr>
								<th colspan="5" style="width:360px">Oficina Mzt</th>							
								<th colspan="5" style="width:370px">Granja</th>							
							</tr>
						</thead>
						<tbody>
							<tr height="25px">
								<th style="width:70px">Dirección</th>
								<th style="width:5px" ></th>
								<th >Joel Lizárraga Aguirre</th>
								<th style="width:5px"></th>
								<th style="width:50px">jla</th>								
								<th style="width:70px">Administración</th>
								<th style="width:5px"></th>
								<th >Antonio Hernandez</th>
								<th style="width:5px"></th>
								<th style="width:50px">antonio.h</th>								
							</tr>				
							<tr height="25px">
								<th style="width:70px">Administración</th>
								<th style="width:5px"></th>
								<th >Lety Lizárraga</th>
								<th style="width:5px"></th>
								<th style="width:50px">lety</th>								
								<th style="width:70px"></th>
								<th style="width:5px"></th>
								<th >Ezequiel Rodriguez</th>
								<th style="width:5px"></th>
								<th style="width:50px">ezequiel</th>								
							</tr>
							<tr height="25px">
								<th style="width:70px"></th>
								<th style="width:5px"></th>
								<th >Trudy Lizárraga</th>
								<th style="width:5px"></th>
								<th style="width:50px">facturacion</th>								
								<th style="width:70px">Producción</th>
								<th style="width:5px"></th>
								<th >Antonio Valdez</th>
								<th style="width:5px"></th>
								<th style="width:50px">antonio.v</th>								
							</tr>
							<tr height="25px">
								<th style="width:70px"></th>
								<th style="width:5px"></th>
								<th >Salvador Partida</th>
								<th style="width:5px"></th>
								<th style="width:50px">contador</th>								
								<th style="width:70px">Alimento</th>
								<th style="width:5px"></th>
								<th >Julio Lizárraga</th>
								<th style="width:5px"></th>
								<th style="width:50px">julio</th>								
							</tr>
							<tr height="25px">
								<th style="width:70px"></th>
								<th style="width:5px"></th>
								<th >Esmeralda</th>
								<th style="width:5px"></th>
								<th style="width:50px">contabilidad</th>								
								<th style="width:70px"></th>
								<th style="width:5px"></th>
								<th ></th>
								<th style="width:5px"></th>
								<th style="width:50px"></th>								
							</tr>
							<tr height="25px">
								<th style="width:70px">Ventas</th>
								<th style="width:5px"></th>
								<th >Efraín Lizárraga</th>
								<th style="width:5px"></th>
								<th style="width:50px">efrain</th>								
								<th style="width:70px"></th>
								<th style="width:5px"></th>
								<th ></th>
								<th style="width:5px"></th>
								<th style="width:50px"></th>								
							</tr>
							<tr height="25px">
								<th style="width:70px"></th>
								<th style="width:5px"></th>
								<th >Zuleima Benitez</th>
								<th style="width:5px"></th>
								<th style="width:50px">clientes</th>								
								<th style="width:70px"></th>
								<th style="width:5px"></th>
								<th ></th>
								<th style="width:5px"></th>
								<th style="width:50px"></th>								
							</tr>
							<tr height="25px">
								<th style="width:70px">Compras</th>
								<th style="width:5px"></th>
								<th >Javier Morán</th>
								<th style="width:5px"></th>
								<th style="width:50px">compras</th>								
								<th style="width:70px">Almacen</th>
								<th style="width:5px"></th>
								<th >Miguel Peñuelas</th>
								<th style="width:5px"></th>
								<th style="width:50px">almacen</th>								
							</tr>
							<tr height="25px">
								<th style="width:70px">Téc. Información</th>
								<th style="width:5px"></th>
								<th >Jesús Benítez</th>
								<th style="width:5px"></th>
								<th style="width:50px">sistemas</th>								
								<th style="width:120px"></th>
								<th style="width:5px"></th>
								<th ></th>
								<th style="width:5px"></th>
								<th style="width:50px"></th>								
							</tr>
						</tbody>
						<tfoot>
							<tr style="">
								<th colspan="2" style="width:90px;font-size: 9px">Departamento</th>								
								<th colspan="2" style="width:120px;font-size: 9px">Responsable</th>								
								<th style="width:150px;font-size: 9px"><center>@acuicolaoceanozul.mx</center></th>									
								<th colspan="2" style="width:90px;font-size: 9px">Departamento</th>								
								<th colspan="2" style="width:120px;font-size: 9px">Responsable</th>								
								<th style="width:150px;font-size: 9px"><center>@acuicolaoceanozul.mx</center></th>							
							</tr>
						</tfoot>	
					</table>
				</ul>
			</div>
		</div>
</div>
</div>
<?php $this->load->view('footer_cv'); ?>


<script>
	//$("#accord").accordion({
	//	event: "click",
	//	collapsible: true
	//});
</script>