<?
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=VentasTotales.xls");

?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <style>
            th, td{
                font-size: 12pt;                
            }            
        </style>
    </head>
    <body>
    	<?php $hoy=date("d-m-Y"); ?>
    	<table style="width: 770px;">
			<tr>
				<td width="200px">
					<p style="font-size: 10px;"  >
						<img src="<?php echo base_url();?>assets/images/oceanoazul.jpg" width="200" height="60" border="0">
						
					</p>
				</td>	
				<td colspan="8" align="center" style="font-size: 14px; color: navy">
					<strong>Acuícola Oceano Azul S.A. de C.V. </strong><br />
        			<strong> <?= $texto?> <br /></strong>
        			<strong> CICLO <?= $ciclo?> <br /></strong>
        		</td>			
			</tr>
		</table>
	    <table border="1" class="">
        	<?= $tabla?>
        </table>
        <table style="width: 770px;">
        	<tr>
        		<td colspan="9">SIA - <?= $usuario?> - <?= $perfil?> <br /></td>
        	</tr>
        	<tr >
				<td colspan="8" width="590" style="font-size: 10px;">
				  <br /> www.acuicolaoceanoazul.mx	Av. Reforma No.2007-B Int. 803, Col. Flamingos, Mazatlán, Sin. México, RFC:AOA-180206-SI2, CP:82149 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
    </body>
    
</html>


