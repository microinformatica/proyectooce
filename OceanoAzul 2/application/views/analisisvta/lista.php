<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts-more.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/export-data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/accessibility.js"></script>


<!--
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

-->

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 13px;		
}   
</style>
<div style="height:500px; text-align: right">
	
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li ><a href="#totales"><img src="<?php echo base_url();?>assets/images/menu/kgs.jpg" width="25" height="25" border="0"> </a></li>
		<li ><a href="#mesk"><img src="<?php echo base_url();?>assets/images/menu/graf2.png" width="25" height="25" border="0"> Mes</a></li>
		<li ><a href="#tallas"><img src="<?php echo base_url();?>assets/images/menu/graf3.png" width="25" height="25" border="0"> Tallas</a></li>
		<li ><a href="#mest"><img src="<?php echo base_url();?>assets/images/menu/graf4.png" width="25" height="25" border="0"> Mes</a></li>	
		<li ><a href="#comparativa"><img src="<?php echo base_url();?>assets/images/menu/compara.jpg" width="25" height="25" border="0"> Comparativa</a></li>
		<li ><a href="#meskr"><img src="<?php echo base_url();?>assets/images/menu/saldos.png" width="25" height="25" border="0"> Saldos</a></li>
		<li ><a href="#diastal"><img src="<?php echo base_url();?>assets/images/menu/dia.png" width="25" height="25" border="0"> Dias Tallas </a></li>
		<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez"){ ?>	
		<li ><a href="#final"><img src="<?php echo base_url();?>assets/images/menu/resul1.jpg" width="25" height="25" border="0"> Resultados </a></li>
		<?php }?>	
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="totales" class="tab_content"  >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >
				 		<div id='ver_vtasgral' class='hide' >				 		
                 		<div class="ajaxSorterDiv" id="tabla_vtag" name="tabla_vtag" style="width: 950px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaVtaG" name="mytablaVtaG" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>  
                        				<tr>                                                      			
                            			<th rowspan="2" data-order = "noma" >Almacen</th>
                            			<th rowspan="2" data-order = "kgs" >kgs</th>
                            			<th rowspan="2" data-order = "por" >%</th>
                            			<th colspan="3">M.N.</th>
                            			<th colspan="3">U.S.D.</th>
                            			</tr>
                            			<tr>
                            			<th data-order = "kmn" >kgs</th>
                            			<th data-order = "imn" >Importe</th>
                            			<th data-order = "pvmn" >P.P.</th>
                            			<th data-order = "kus" >kgs</th>
                            			<th data-order = "ius" >Importe</th>
                            			<th data-order = "pvus" >P.P.</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 12px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablesg" action="<?= base_url()?>index.php/analisisvta/reporte1" method="POST" >							
    	    	            		<ul class="order_list" style="width:380px; margin-top: 1px">
    	    	            			Ventas Totales: Ciclo- <input size="3%" type="text" name="cic" id="cic" readonly="true" style="border: none"  >    	    	            			
    	                				<button type="button" id="vtagra" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Grafica</button>
    	                			</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablavtag" value ="" class="htmlTable"/> 
        	            	    	<input type="hidden" name="texto" value ="Ventas Totales de Camarón - Distribución en Almacenes" />
        	            	    	
								</form>   
                			</div>       
	                	</div>
	                	</div>
	                	<div id='ver_gravtasgral' class='hide' >
	                		<div name="vtagral" id="vtagral" style="width: 950px; height: 440px; margin: 0 auto"></div>
	                		<button type="button" id="vtatab" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Información</button>
	                	</div>	
	        		</th>
            		</tr>
     		</table>	
		</div>
		<div id="mesk" class="tab_content"  >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >
				 		<div id='ver_vtasgralmes' class='hide' >
				 		<div class="ajaxSorterDiv" id="tabla_vtam" name="tabla_vtam" style="width: 950px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaVtaM" name="mytablaVtaM" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>  
                        				<tr>                                                      			
                            			<th rowspan="2" data-order = "ano" >Año</th>
                            			<th rowspan="2" data-order = "mes" >Mes</th>
                            			<th rowspan="2" data-order = "kgs" >kgs</th>
                            			<th rowspan="2" data-order = "por" >%</th>
                            			<th colspan="3">M.N.</th>
                            			<th colspan="3">U.S.D.</th>
                            			</tr>
                            			<tr>
                            			<th data-order = "kmn" >kgs</th>
                            			<th data-order = "imn" >Importe</th>
                            			<th data-order = "pvmn" >P.P.</th>
                            			<th data-order = "kus" >kgs</th>
                            			<th data-order = "ius" >Importe</th>
                            			<th data-order = "pvus" >P.P.</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 12px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablesm" action="<?= base_url()?>index.php/analisisvta/reporte2" method="POST" >							
    	    	            		<ul class="order_list" style="width:380px; margin-top: 1px">
    	    	            			Ventas Mensuales: Ciclo- <input size="3%" type="text" name="cicm" id="cicm" readonly="true" style="border: none"  >
    	    	            			<button type="button" id="vtagrames" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Grafica</button>    	    	            			
    	                			</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px; margin-left: -10px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablavtag" value ="" class="htmlTable"/> 
        	            	    	<input type="hidden" name="texto" value ="Ventas Totales de Camarón - Kgs Mensuales" />
        	            	    	 
								</form>   
                			</div>       
	                	</div>
	                	</div>
	                	<div id='ver_gravtasgralmes' class='hide' >
	                		<div name="vtamesgra" id="vtamesgra" style="width: 950px; height: 440px; margin: 0 auto"></div>
	                		<button type="button" id="vtatabmes" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Información</button>
	                	</div>	
	               </th>
	        		
            	</tr>
     		</table>	
		</div>
		<div id="tallas" class="tab_content"  >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >
				 		<div id='ver_vtasgraltal' class='hide' >
				 		<div class="ajaxSorterDiv" id="tabla_vtat" name="tabla_vtat" style="width: 950px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaVtaT" name="mytablaVtaT" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>  
                        				<tr>                                                      			
                            			<th rowspan="2" data-order = "grupo" >Talla</th>
                            			<th rowspan="2" data-order = "kgs" >kgs</th>
                            			<th rowspan="2" data-order = "por" >%</th>
                            			<th colspan="3">M.N.</th>
                            			<th colspan="3">U.S.D.</th>
                            			</tr>
                            			<tr>
                            			<th data-order = "kmn" >kgs</th>
                            			<th data-order = "imn" >Importe</th>
                            			<th data-order = "pvmn" >P.P.</th>
                            			<th data-order = "kus" >kgs</th>
                            			<th data-order = "ius" >Importe</th>
                            			<th data-order = "pvus" >P.P.</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 12px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablest" action="<?= base_url()?>index.php/analisisvta/reporte3" method="POST" >						
    	    	            		<ul class="order_list" style="width:380px; margin-top: 1px">
    	    	            			Ventas Tallas: Ciclo- <input size="3%" type="text" name="cict" id="cict" readonly="true" style="border: none"  >
    	    	            			<button type="button" id="vtagratal" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Grafica</button>    	    	            			
    	                			</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px; margin-left: -10px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablavtag" value ="" class="htmlTable"/> 
        	            	    	<input type="hidden" name="texto" value ="Ventas Totales Por Talla de Camarón - Kgs Mensuales" />
        	            	    	
								</form>   
                			</div>       
	                	</div>
	                	</div>
	                	<div id='ver_gravtasgraltal' class='hide' >
	                		<div name="vtatalgra" id="vtatalgra" style="width: 950px; height: 440px; margin: 0 auto"></div>
	                		<button type="button" id="vtatabtal" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Información</button>
	                	</div>	
	               </th>
	        		
            	</tr>
     		</table>	
		</div>
		<div id="mest" class="tab_content"  >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >
				 		<div id='ver_vtasgralmest' class='hide' >
				 		<div class="ajaxSorterDiv" id="tabla_vtamt" name="tabla_vtamt" style="width: 950px; height: 460px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 420px;"  >
                    			<table id="mytablaVtaMT" name="mytablaVtaMT" class="ajaxSorter" border="1" style="width: 933px;" >
                        			<thead>  
                        				<tr>                                                      			
                            			<th data-order = "grupo" >Talla</th>
                            			<th data-order = "kgs" >Rep.</th>
                            			<th data-order = "por" >%</th>
                            			 <?php	
                        				    for ($mes = 1; $mes <= 15; $mes++){ ?>
           									<th data-order ="<?php echo 'm'.$mes;?>" style="font-size: 10px;"><?php echo $mes;?></option>
           								<?php };?> 
                            			</tr>
                            			
                            		</thead>
                        			<tbody style="font-size: 12px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px "> 
								<form id="data_tablesm" action="<?= base_url()?>index.php/analisisvta/reporte4" method="POST" >							
    	    	            		<ul class="order_list" style="width:480px; margin-top: 1px">
    	    	            			Ventas Mensuales: Ciclo- <input size="3%" type="text" name="cicmt" id="cicmt" readonly="true" style="border: none"  >
    	    	            			 
    	    	            			<button type="button" id="vtagramest" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Grafica</button>    	    	            			
    	                			</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px; margin-left: -10px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablavtag" value ="" class="htmlTable"/> 
        	            	    	<input type="hidden" name="texto" value ="Ventas Totales Por Talla de Camarón - Mensuales" />
        	            	    	
								</form>   
                			</div>       
	                	</div>
	                	</div>
	                	<div id='ver_gravtasgralmest' class='hide' >
	                		<div name="vtamesgrat" id="vtamesgrat" style="width: 950px; height: 420px; margin: 0 auto"></div>
	                		<button type="button" id="vtatabmest" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Información</button>
	                	</div>	
	               </th>
	        		
            	</tr>
            	<tr>
            		<th style="text-align: right; margin-top: -10px">
            			Reporte en:
    	    	             <select id="rep" style="font-size: 12px;height: 25px;margin-top: -5px;"  >
    							<option value="0">Kgs</option>
    							<option value="1">M.N.</option>
    							<option value="2">U.S.D.</option>
     						</select>
            		</th>
            	</tr>
     		</table>	
		</div>
		<div id="comparativa" class="tab_content"  >	
			<table border="0" style="margin-left: -5px; margin-top: -5px; ">
				<tr>
            		<th style="text-align: right; ">
            			vs 
    	    	            <select placeholder="Clientes"  id="cli2" style="font-size: 12px;height: 25px;margin-top: -25px;"  >
    							<option value="0">Sel. Cliente 2</option>
						     		<?php	
						        		$data['result']=$this->analisisvta_model->getClientes();
										foreach ($data['result'] as $row):?>
						        				<option value="<?php echo $row->Numero;?>"><?php echo $row->Razon;?></option>
						        	<?php endforeach;?>
						   	</select>   
						   	<input type="hidden" id="nombre2" name="nombre2" />	
            		</th>
            	</tr>
				<tr>
				 	<th >
				 		<div id='ver_vtasgralcomt' class='hide' >
				 		<div class="ajaxSorterDiv" id="tabla_vtact" name="tabla_vtact" style="width: 950px;  margin-top: -10px"  >                 			
							<span class="ajaxTable" style="height: 425px;"  >
                    			<table id="mytablaVtaCT" name="mytablaVtaCT" class="ajaxSorter" border="1" style="width: 933px;" >
                        			<thead>  
                        				<tr>                                                      			
                            				<th rowspan="3" data-order = "grupo" >Talla</th>
                            			<!--
                            				<th rowspan="3" data-order = "kgs" >kgs</th>
                            				<th rowspan="3" data-order = "por" >%</th>
                            			-->	
                            				<th colspan="9">Moneda Nacional: M.N. </th>
                            				<th colspan="9">Moneda Extranjera: U.S.D.</th>
                            				
                            			</tr>
                            			<tr>
                            				<th colspan="3">Cliente</th>
                            				<th colspan="3">Cliente2</th>
                            				<th colspan="3">Diferencia</th>
                            				<th colspan="3">Cliente</th>
                            				<th colspan="3">Cliente2</th>
                            				<th colspan="3">Diferencia</th>
                            			</tr>
                            			<tr>
                            				<th data-order = "kmn1" >kgs</th>
                            				<th data-order = "imn1" >Importe</th>
                            				<th data-order = "pvmn1" >P.P.</th>
                            				<th data-order = "kmn2" >kgs</th>
                            				<th data-order = "imn2" >Importe</th>
                            				<th data-order = "pvmn2" >P.P.</th>
                            				<th data-order = "kmnd" >kgs</th>
                            				<th data-order = "imnd" >Importe</th>
                            				<th data-order = "pvmnd" >$</th>
                            				
                            				<th data-order = "kus1" >kgs</th>
                            				<th data-order = "ius1" >Importe</th>
                            				<th data-order = "pvus1" >P.P.</th>
                            				<th data-order = "kus2" >kgs</th>
                            				<th data-order = "ius2" >Importe</th>
                            				<th data-order = "pvus2" >P.P.</th>
                            				<th data-order = "kusd" >kgs</th>
                            				<th data-order = "iusd" >Importe</th>
                            				<th data-order = "pvusd" >$</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 12px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px "> 
								<form id="data_tablesC" action="<?= base_url()?>index.php/analisisvta/reporte5" method="POST" >							
    	    	            		<ul class="order_list" style="width:480px; margin-top: 1px">
    	    	            			Ventas Mensuales: Ciclo- <input size="3%" type="text" name="cicct" id="cicct" readonly="true" style="border: none"  >
    	    	            			 
    	    	            			<button type="button" id="vtagracomt" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Grafica</button>    	    	            			
    	                			</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px; margin-left: -10px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablavtag" value ="" class="htmlTable"/> 
        	            	    	<input type="hidden" name="texto" value ="Comparativo de Ventas Totales Por Talla de Camarón " />
        	            	    	
								</form>   
                			</div>       
	                	</div>
	                	</div>
	                	<div id='ver_gravtasgralcomt' class='hide' >
	                		<div name="vtacomgrat" id="vtacomgrat" style="width: 950px; height: 425px; margin: 0 auto"></div>
	                		<button type="button" id="vtatabcomt" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Información</button>
	                	</div>	
	               </th>
	        		
            	</tr>
            	
     		</table>	
		</div>
		
		<div id="meskr" class="tab_content"  >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >
				 		<div id='ver_vtasgralmesr' class='hide' >
				 		<div class="ajaxSorterDiv" id="tabla_vtamr" name="tabla_vtamr" style="width: 950px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaVtaMR" name="mytablaVtaMR" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>  
                        				<tr>                                                      			
                            			<th rowspan="2" data-order = "ano" >Año</th>
                            			<th colspan="3" style="border-right: none">Ventas</th>
                            			<th colspan="2" style="border-left: none">Acumuladas</th>
                            			<th colspan="2">Pagos Acumulados</th>
                            			<th colspan="4">Saldo</th>
                            			
                            			<!--<th rowspan="2" data-order = "por" >%</th>
                            			<th colspan="3">M.N.</th>
                            			<th colspan="3">U.S.D.</th>
                            			<th colspan="3">Acumulado Ventas</th>
                            			<th colspan="2">Acumulado Pagos</th>
                            			</tr>
                            			<tr>
                            			<th data-order = "kmn" >kgs</th>
                            			<th data-order = "imn" >Importe</th>
                            			<th data-order = "pvmn" >P.P.</th>
                            			<th data-order = "kus" >kgs</th>
                            			<th data-order = "ius" >Importe</th>
                            			<th data-order = "pvus" >P.P.</th>-->
                            			</tr>
                            			<tr>
                            			<th data-order = "mes" >Mes</th>
                            			<th data-order = "kgs" >Kgs</th>
                            			<th data-order = "imvm" >Importe</th>
                            			<th data-order = "kgsa" >Kgs</th>
                            			<th data-order = "imna" >Importe</th>
                            			<th data-order = "depmn" >Mes</th>
                            			<th data-order = "depmna" >Importe</th>
                            			<th data-order = "salmn" style="border-right: none" >Importe</th>
                            			<th data-order = "salmni" style="width: 1px; border-left: none" ></th>
                            			<th data-order = "pormn" >%</th>
                            			<th data-order = "porinc" >[+-] %</th>
                            			<!--<th data-order = "iusa" >U.S.D.</th>
                            			<th data-order = "imnp" >M.N</th>
                            			<th data-order = "iusp" >U.S.D.</th>-->
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 12px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablesmr" action="<?= base_url()?>index.php/analisisvta/reporte6" method="POST" >							
    	    	            		<ul class="order_list" style="width:480px; margin-top: 1px">
    	    	            			Comportamiento General de Saldos hasta el actual Ciclo- <input size="3%" type="text" name="cicmr" id="cicmr" readonly="true" style="border: none"  >
    	    	            			<button type="button" id="vtagramesr" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Grafica</button>    	    	            			
    	                			</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px; margin-left: -10px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablavtag" value ="" class="htmlTable"/> 
        	            	    	<input type="hidden" name="texto" value ="Ventas Totales de Camarón - Kgs Mensuales Totales" />
        	            	    	
								</form>   
                			</div>       
	                	</div>
	                	<div id='ver_vtasgralmesrg' class='hide' >
				 		<div class="ajaxSorterDiv" id="tabla_vtamrg" name="tabla_vtamrg" style="width: 950px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaVtaMRG" name="mytablaVtaMRG" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>  
                        				<tr>                                                      			
                            			<th data-order = "mes" ></th>
                            			<th data-order = "imna" >Ventas M.N.</th>
                            			<th data-order = "depmn" >Pagos M.N.</th>
                            			<th data-order = "salmn" >Saldo M.N.</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 12px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
	                	</div>
	                	</div>
	                	<div id='ver_gravtasgralmesr' class='hide' >
	                		<div name="vtamesrgra" id="vtamesrgra" style="width: 950px; height: 440px; margin: 0 auto"></div>
	                		<button type="button" id="vtatabmesr" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Información</button>
	                	</div>	
	               </th>
	        		
            	</tr>
     		</table>	
		</div>
		
		<div id="diastal" class="tab_content"  >
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >
				 		<div id='ver_diastal' class='hide' >
				 		<div class="ajaxSorterDiv" id="tabla_diastal" name="tabla_diastal" style="width: 950px; margin-top: -20px;"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaDiasTal" name="mytablaDiasTal" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>  
                        				<tr>                                                      			
                            			<th rowspan="2" data-order = "noma1" >Almacen</th>
                            			<th rowspan="2" data-order = "sigg" >Granja</th>
                            			<th rowspan="2" data-order = "nomt" >Talla</th>
                            			<th colspan="3" >Kgs Totales</th>
                            			<th colspan="2" >Procesados</th>
                            			<th colspan="3" >Ventas</th>
                            			</tr>
                            			<tr>
                            				<th data-order = "kgsent" >Oferta</th>
                            				<th data-order = "kgssal" >Vendidos</th>
                            				<th data-order = "kgsexi" >Existencia</th>
                            				<th data-order = "pe" >Primera</th>                            				
                            				<!--<th data-order = "dpe" >Días</th>-->
                            				<th data-order = "ue" >Última</th>                            				
                            				<!--<th data-order = "due" >Días</th>-->
                            				<th data-order = "ps" >Primera</th>                            				
                            				<!--<th data-order = "dps" >Días</th>-->
                            				<th data-order = "us" >Última</th>                            				
                            				<th data-order = "dus" >Días</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 11px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablesmr" action="<?= base_url()?>index.php/analisisvta/reporte7" method="POST" >							
    	    	            		<ul class="order_list" style="width:480px; margin-top: 1px">
    	    	            			Días Transcurridos al día de hoy Ciclo- <input size="3%" type="text" name="cicdt" id="cicdt" readonly="true" style="border: none"  >
    	    	            			
    	    	            			<button type="button" id="bottabdt" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Grafica</button>    	    	            			
    	                			</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px; margin-left: -10px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablavtag" value ="" class="htmlTable"/> 
        	            	    	<input type="hidden" name="texto" value ="Existencia en Almacenes con sus Días Transcurridos a la última venta" />
        	            	    	 
								</form>   
                			</div>       
	                	</div>
	                	<div id='ver_datosgra' class='hide' >
	                	<div class="ajaxSorterDiv" id="tabla_diastalg" name="tabla_diastalg" style="width: 950px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaDiasTalg" name="mytablaDiasTalg" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>  
                        				<tr>                                                      			
                            			<!--<th data-order = "noma1" >Almacen</th>
                            			<th data-order = "sigg" >Granja</th>-->
                            			<th data-order = "nomt" ></th>
                            			<th data-order = "kgsexi" >kgs</th>
                            			<th data-order = "dus" >Días</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 11px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
	                	</div>
	                	</div>
	                	<div id='ver_gradiastal' class='hide' >
	                		<div name="diastalgra" id="diastalgra" style="width: 950px; height: 440px; margin: 0 auto"></div>
	                		<button type="button" id="botgradt" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Información</button>
	                	</div>		
	               </th>
	        		
            	</tr>
            	
     		</table>	
		</div>	
		<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez"){ ?>
		<div id="final" class="tab_content"  >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >
				 		<div id='ver_final' class='hide' >
				 		<div class="ajaxSorterDiv" id="tabla_final" name="tabla_final" style="width: 950px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaFinal" name="mytablaFinal" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>  
                        				<tr>                                                      			
                            				<th rowspan="2" data-order = "nomt" >Kgs Tot <br>Talla</th>
                            				<th >Bordo</th>
                            				<th colspan="2" >Maquila</th>
                            				<th colspan="4" >Ventas</th>
                            				<th colspan="3" >Saldos</th>
                            				<th colspan="5">Costos</th>
                            				<th colspan="2" >Utilidad</th>
                            			</tr>
                            			<tr>
                            				<th data-order = "bordo" >Kgs</th>
                            				                            			
                            				<th data-order = "procesado" >Kgs</th>
                            				<th data-order = "porpro" >%</th>
                            				<!--<th data-order = "pe" >1ra.</th>-->
                            				<!--	<th data-order = "mqkmn" >Maquila</th>-->
                            				
                            			<!--	<th data-order = "vkmn" >Venta</th>-->
                            				
                            			
                            			<!--	<th data-order = "mqkus" >Maquila</th>-->
                            			<!--	<th data-order = "mqius" >U.S.D.</th>--->
                            			<!--	<th data-order = "vkus" >Venta</th>-->
                            			<!--	<th data-order = "vpus" >P.P.</th>-->
                            			
                            				<th data-order = "ventas" >Kgs</th>
                            				<th data-order = "vimn" >Total</th>
                            				<th data-order = "vpmn" >P.P.</th>
                            			<!--	<th data-order = "vius" >U.S.D.</th>-->
                            				<th data-order = "us" >Última</th>
                            			
                            				<th data-order = "existencia" >Kgs</th>
                            				<th data-order = "por" >%</th>
                            			
                            				<th data-order = "da" >Días</th>
                            				
                            				<th data-order = "bormn" >Bordo</th>
                            				<th data-order = "mqimn" >Maquila</th>
                            				<th data-order = "fst" >FleSegTra</th>
                            				<th data-order = "cosalm" >Almacenaje</th>
                            				<th data-order = "costot" >Total</th>
                            				
                            				<th data-order = "porutil" ></th>
                            				<th data-order = "utilidad" >Importe</th>
                            				
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 10px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_final" action="<?= base_url()?>index.php/analisisvta/reporte8" method="POST" >							
    	    	            		<ul class="order_list" style="width:480px; margin-top: 1px">
    	    	            			Resultados Finales Ciclo- <input size="3%" type="text" name="cicf" id="cicf" readonly="true" style="border: none"  >
    	    	            			<button type="button" id="finalt" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Grafica</button>    	    	            			
    	                			</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px; margin-left: -10px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablavtag" value ="" class="htmlTable"/> 
        	            	    	<input type="hidden" name="texto" value ="Resultados Finales" /> 
        	            	    	
								</form>   
                			</div>       
	                	</div>
	                	<div id='ver_finalgra' class='hide' >
				 		<div class="ajaxSorterDiv" id="tabla_finalg" name="tabla_finalg" style="width: 950px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaFinalG" name="mytablaFinalG" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>  
                        				<tr>                                                      			
                            			<th data-order = "mes" ></th>
                            			<th data-order = "imna" >Ventas M.N.</th>
                            			<th data-order = "depmn" >Pagos M.N.</th>
                            			<th data-order = "salmn" >Saldo M.N.</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 12px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
	                	</div>
	                	</div>
	                	<div id='ver_finalg' class='hide' >
	                		<div name="finalgra" id="finalgra" style="width: 950px; height: 440px; margin: 0 auto"></div>
	                		<button type="button" id="finalg" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Información</button>
	                	</div>	
	               </th>
	        		
            	</tr>
     		</table>	
		</div>
			<?php }?>
		</div>
	</div> 	
	<select placeholder="Ciclo" name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px;margin-top: 1px; margin-left:4px;" >
           	<?php $ciclof=19; $actual=date("y"); //$actual+=1;
				while($actual >= $ciclof){?>
				<option value="<?php echo $actual;?>" > <?php echo '20'.$actual;?> </option>
    		<?php  $actual-=1; } ?>
    </select>
    <select id="almacenes" style="font-size: 12px;height: 25px;margin-top: 1px;"  ></select>
   <!-- <select id="clientes" style="font-size: 12px;height: 25px;margin-top: 1px;"  ></select>-->
    <select placeholder="Clientes"  id="clientes" style="font-size: 11px;height: 25px;margin-top: 1px;"  >
    	<option value="0">Clientes</option>
     		<?php	
        		$data['result']=$this->analisisvta_model->getClientes();
				foreach ($data['result'] as $row):?>
        				<option value="<?php echo $row->Numero;?>"><?php echo $row->Razon;?></option>
        	<?php endforeach;?>
   	</select>
   	<select id="Tallas" style="font-size: 12px;height: 25px;margin-top: 1px;"  >   	</select>	
   	<input type="hidden" id="nombre" name="nombre" /><input type="hidden" id="congeladora" name="congeladora" />
   	<input type="hidden" id="Tallas1" name="Tallas1" />
   	<select id="granjas" style="font-size: 12px;height: 25px;margin-top: -5px;"  >
    	<option value="0">Granjas</option>
    	<option value="4">Ahome</option>
    	<option value="10" >Topolobampo</option>
    	<option value="2">Kino</option>
    	<option value="6">Huatabampo</option>
    	
    </select>
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#clientes").change( function(){
	$("#nombre").val(0);	
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/analisisvta/buscarcli", 
			data: "cli="+$("#clientes").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#nombre").val(obj.razon);
   			} 
	});	
	
});
$("#almacenes").change( function(){
	$("#congeladora").val(0);	
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/analisisvta/buscarconge", 
			data: "con="+$("#almacenes").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#congeladora").val(obj.noma);
   			} 
	});	
	
});

$("#cli2").change( function(){
	$("#nombre2").val(0);	
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/analisisvta/buscarcli", 
			data: "cli="+$("#cli2").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#nombre2").val(obj.razon);
   			} 
	});	
	
});

$('#almacenes').boxLoader({
   url:'<?php echo base_url()?>index.php/analisisvta/comboa',
   equal:{id:'ciclo',value:'#cmbCiclo'},
   select:{id:'ida',val:'val'},
   all:"Almacenes",            
});

$('#Tallas').boxLoader({
   url:'<?php echo base_url()?>index.php/analisisvta/combob',
   equal:{id:'ciclo',value:'#cmbCiclo'},
   select:{id:'idt',val:'val'},
   all:"Tallas",            
});

$('#clientescompara').boxLoader({
   url:'<?php echo base_url()?>index.php/analisisvta/comboc',
   equal:{id:'ida',value:'#almacenes'},
   select:{id:'Numero',val:'val'},
   all:"Sel.",            
});

$("#Tallas").change( function(){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/analisisvta/buscartalgpo", 
			data: "tal="+$("#Tallas").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
  					if(obj.gpoid!=0)	
					$("#Tallas1").val(obj.gpoid);
					else
					$("#Tallas1").val(0);
					$("#mytablaVtaG").trigger("update");
					$("#mytablaVtaM").trigger("update");
					$("#mytablaVtaT").trigger("update");
					$("#mytablaVtaMT").trigger("update");
   			} 
	});		
});	

$('#vtagra').click(function(){$(this).removeClass('used');$('#ver_vtasgral').hide();$('#ver_gravtasgral').show();});
$('#vtatab').click(function(){$(this).removeClass('used');$('#ver_gravtasgral').hide();$('#ver_vtasgral').show();});
$('#vtagrames').click(function(){$(this).removeClass('used');$('#ver_vtasgralmes').hide();$('#ver_gravtasgralmes').show();});
$('#vtatabmes').click(function(){$(this).removeClass('used');$('#ver_gravtasgralmes').hide();$('#ver_vtasgralmes').show();});
$('#vtagratal').click(function(){$(this).removeClass('used');$('#ver_vtasgraltal').hide();$('#ver_gravtasgraltal').show();});
$('#vtatabtal').click(function(){$(this).removeClass('used');$('#ver_gravtasgraltal').hide();$('#ver_vtasgraltal').show();});
$('#vtagramest').click(function(){$(this).removeClass('used');$('#ver_vtasgralmest').hide();$('#ver_gravtasgralmest').show();});
$('#vtatabmest').click(function(){$(this).removeClass('used');$('#ver_gravtasgralmest').hide();$('#ver_vtasgralmest').show();});
$('#vtagracomt').click(function(){$(this).removeClass('used');$('#ver_vtasgralcomt').hide();$('#ver_gravtasgralcomt').show();});
$('#vtatabcomt').click(function(){$(this).removeClass('used');$('#ver_gravtasgralcomt').hide();$('#ver_vtasgralcomt').show();});
$('#vtagramesr').click(function(){$(this).removeClass('used');$('#ver_vtasgralmesr').hide();$('#ver_gravtasgralmesr').show();});
$('#vtatabmesr').click(function(){$(this).removeClass('used');$('#ver_gravtasgralmesr').hide();$('#ver_vtasgralmesr').show();});
$('#botgradt').click(function(){$(this).removeClass('used');$('#ver_gradiastal').hide();$('#ver_diastal').show();});
$('#bottabdt').click(function(){$(this).removeClass('used');$('#ver_diastal').hide();$('#ver_gradiastal').show();});
$('#finalt').click(function(){$(this).removeClass('used');$('#ver_final').hide();$('#ver_finalg').show();});
$('#finalg').click(function(){$(this).removeClass('used');$('#ver_finalg').hide();$('#ver_final').show();});
$("#cmbCiclo").change( function(){
	$("#cic").val('20'+$("#cmbCiclo").val());
	$("#mytablaVtaG").trigger("update");
	$("#cicm").val('20'+$("#cmbCiclo").val());
	$("#mytablaVtaM").trigger("update");
	$("#cict").val('20'+$("#cmbCiclo").val());
	$("#mytablaVtaT").trigger("update");
	$("#cicmt").val('20'+$("#cmbCiclo").val());
	$("#mytablaVtaMT").trigger("update");
	$("#cicct").val('20'+$("#cmbCiclo").val());
	$("#mytablaVtaCT").trigger("update");
	$("#cicmr").val('20'+$("#cmbCiclo").val());
	$("#mytablaVtaMR").trigger("update");
	$("#mytablaVtaMRG").trigger("update");
	$("#cicdt").val('20'+$("#cmbCiclo").val());
	$("#mytablaDiasTal").trigger("update");
	$("#mytablaDiasTalg").trigger("update");
	$("#cicf").val('20'+$("#cmbCiclo").val());
	$("#mytablaFinal").trigger("update");
	$("#mytablaFinalG").trigger("update");
	return true;
});

$(document).ready(function(){
	$('#ver_vtasgral').hide();$('#ver_gravtasgral').show();
	$('#ver_vtasgralmes').hide();$('#ver_gravtasgralmes').show();
	$('#ver_vtasgraltal').hide();$('#ver_gravtasgraltal').show();
	$('#ver_vtasgralmest').hide();$('#ver_gravtasgralmest').show();
	$('#ver_vtasgralcomt').hide();$('#ver_gravtasgralcomt').show();
	$('#ver_vtasgralmesr').hide();$('#ver_gravtasgralmesr').show();$('#ver_vtasgralmesrg').hide();
	$('#ver_diastal').hide();$('#ver_gradiastal').show();$('#ver_datosgra').hide();
	$('#ver_fial').show();$('#ver_finalgra').hide();$('#ver_finalg').hide();
	var f = new Date();
	//$("#cmbMesM").val((f.getMonth() +1));
	//$("#txtFI").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());	
	$("#cic").val(f.getFullYear());	$("#cicm").val(f.getFullYear()); $("#cict").val(f.getFullYear());$("#cicmt").val(f.getFullYear());$("#cicct").val(f.getFullYear());$("#cicmr").val(f.getFullYear());
	$("#cicdt").val(f.getFullYear());$("#cicf").val(f.getFullYear());
	$("#nombre").val(0);$("#congeladora").val(0);
	$("#tabla_vtag").ajaxSorter({
		url:'<?php echo base_url()?>index.php/analisisvta/tablavtag',  
		filters:['cic','almacenes','clientes','Tallas1'],
		//active:['color',1],
        sort:false,
        onSuccess:function(){
       		$('#tabla_vtag tbody tr').map(function(){
				if($(this).data('noma')=='Total') {	    				    			
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('font-size','12px');
	    		}
	    		
			});
			$('#tabla_vtag tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
			$('#tabla_vtag tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold');});
			$('#tabla_vtag tbody tr td:nth-child(3)').map(function(){ $(this).css('font-weight','bold');});
	    	nombre=''; 	if($("#nombre").val()!=0) nombre='- Cliente: '+$("#nombre").val();
	    	$('#tabla_vtag tbody tr').map(function(){
	    		if($(this).data('kgs')!=''){kgstot=$(this).data('kgs');} else {kgstot='';} 
	    		if($(this).data('a1')!=''){a1=$(this).data('a1');v1=$(this).data('v1');p1=$(this).data('p1');
	    									if($(this).data('mn1')!=''){mn1=$(this).data('mn1');pmn1=$(this).data('pmn1');} else {mn1='-';pmn1='';}
	    									if($(this).data('us1')!=''){us1=$(this).data('us1');pus1=$(this).data('pus1');} else {us1='-';pus1='';}
	    									} else{a1='';p1='';mn1='';us1='';v1='';pmn1='';pus1='';}	
	    		if($(this).data('a2')!=''){a2=$(this).data('a2');v2=$(this).data('v2');p2=$(this).data('p2');
	    									if($(this).data('mn2')!=''){mn2=$(this).data('mn2');pmn2=$(this).data('pmn2');} else {mn2='-';pmn2='';}
	    									if($(this).data('us2')!=''){us2=$(this).data('us2');pus2=$(this).data('pus2');} else {us2='-';pus2='';}
	    									} else{a2='';p2='';mn2='';us2='';v2='';pmn2='';pus2='';}
	    		if($(this).data('a3')!=''){a3=$(this).data('a3');v3=$(this).data('v3');p3=$(this).data('p3');
	    									if($(this).data('mn3')!=''){mn3=$(this).data('mn3');pmn3=$(this).data('pmn3');} else {mn3='-';pmn3='';}
	    									if($(this).data('us3')!=''){us3=$(this).data('us3');pus3=$(this).data('pus3');} else {us3='-';pus3='';}
	    									} else{a3='';p3='';mn3='';us3='';v3='';pmn3='';pus3='';}
	    		if($(this).data('a4')!=''){a4=$(this).data('a4');v4=$(this).data('v4');p4=$(this).data('p4');
	    									if($(this).data('mn4')!=''){mn4=$(this).data('mn4');pmn4=$(this).data('pmn4');} else {mn4='-';pmn4='';}
	    									if($(this).data('us4')!=''){us4=$(this).data('us4');pus4=$(this).data('pus4');} else {us4='-';pus4='';}
	    									} else{a4='';p4='';mn4='';us4='';v4='';pmn4='';pus4='';}
	    		if($(this).data('a5')!=''){a5=$(this).data('a5');v5=$(this).data('v5');p5=$(this).data('p5');
	    									if($(this).data('mn5')!=''){mn5=$(this).data('mn5');pmn5=$(this).data('pmn5');} else {mn5='-';pmn5='';}
	    									if($(this).data('us5')!=''){us5=$(this).data('us5');pus5=$(this).data('pus5');} else {us5='-';pus5='';}
	    									} else{a5='';p5='';mn5='';us5='';v5='';pmn5='';pus5='';}
	    		if($(this).data('a6')!=''){a6=$(this).data('a6');v6=$(this).data('v6');p6=$(this).data('p6');
	    									if($(this).data('mn6')!=''){mn6=$(this).data('mn6');pmn6=$(this).data('pmn6');} else {mn6='-';pmn6='';}
	    									if($(this).data('us6')!=''){us6=$(this).data('us6');pus6=$(this).data('pus6');} else {us6='-';pus6='';}
	    									}else{a6='';p6='';mn6='';us6='';v6='';pmn6='';pus6='';}
	    	});	
	    	/*
	    	//if(entro>0){
	    	Highcharts.chart('vtagral', {
   				 chart: { type: 'column' },
    			 title: { text: 'Ventas Totales de Camarón: '+kgstot+' kgs',  align: 'left'},
    			 subtitle: { text: 'Distribución en Almacenes '+nombre,  align: 'left'},
    			 xAxis: {
        					type: 'category',
					        labels: {	//rotation: -45,
            							style: {fontSize: '10px',fontFamily: 'Verdana, sans-serif'}
        							}
    			},
    			yAxis: {
        				min: 0,
        				title: {  text: 'Camarón'  },
        				labels: {format: '{value} kgs', style: {color: Highcharts.getOptions().colors[0]} }
    			},
    			legend: {   enabled: false  },
    			tooltip: {  pointFormat: 'Total Almacen: <b>{point.y:,.2f} kgs</b>' },
    			series: [{
					        name: 'Almacenes',
        					data: [ [a1+'<br>['+p1+'%]<br>MN<br>'+mn1+'<br>USD<br>'+us1+'<br>', v1],
        							[a2+'<br>['+p2+'%]<br>MN<br>'+mn2+'<br>USD<br>'+us2+'<br>', v2],
        							[a3+'<br>['+p3+'%]<br>MN<br>'+mn3+'<br>USD<br>'+us3+'<br>', v3],
        							[a4+'<br>['+p4+'%]<br>MN<br>'+mn4+'<br>USD<br>'+us4+'<br>', v4],
        							[a5+'<br>['+p5+'%]<br>MN<br>'+mn5+'<br>USD<br>'+us5+'<br>', v5],
        							[a6+'<br>['+p6+'%]<br>MN<br>'+mn6+'<br>USD<br>'+us6+'<br>', v6]],
        					dataLabels: {   enabled: true,
            								//rotation: -90,
            								color: '#FFFFFF',
            								align: 'center',
            								format: '{point.y:,.2f}', // one decimal
            								y: 10, // 10 pixels down from the top
            								style: {	fontSize: '10px', fontFamily: 'Verdana, sans-serif' }
            								 //tooltip: {
      								  //pointFormat: "Value: {point.y:.2f} mm"
    //},
        					}
    			}]
			});
			//}
			
			*/
			Highcharts.chart('vtagral', {
    			//chart: {zoomType: 'xy'},
    			 title: { text: 'Ventas Totales de Camarón: '+kgstot+' kgs',  align: 'left'},
    			 subtitle: { text: 'Distribución en Almacenes '+nombre,  align: 'left'},
    			xAxis: [{
    				 labels: {	rotation: 0,
            							style: {fontSize: '10px',fontFamily: 'Verdana, sans-serif'}
        						},
        				categories: [a1+'<br>['+p1+'%]<br>MN<br>'+mn1+'<br>USD<br>'+us1+'<br>', 
        							 a2+'<br>['+p2+'%]<br>MN<br>'+mn2+'<br>USD<br>'+us2+'<br>',
        							 a3+'<br>['+p3+'%]<br>MN<br>'+mn3+'<br>USD<br>'+us3+'<br>',
        							 a4+'<br>['+p4+'%]<br>MN<br>'+mn4+'<br>USD<br>'+us4+'<br>',
        							 a5+'<br>['+p5+'%]<br>MN<br>'+mn5+'<br>USD<br>'+us5+'<br>',
        							 a6+'<br>['+p6+'%]<br>MN<br>'+mn6+'<br>USD<br>'+us6+'<br>'
        							 ],
        				crosshair: true
    			}],
    			yAxis: [{ // Primary yAxis
        					labels: {format: '${value} usd', style: {color: Highcharts.getOptions().colors[2] }	},
        					title: {text: 'U.S.D.', style: {color: Highcharts.getOptions().colors[2]}  },
        					opposite: true
			    		}, { // Secondary yAxis
        					gridLineWidth: 0,
        					title: {text: 'Camarón', style: {color: Highcharts.getOptions().colors[0]} },
        					labels: {format: '{value} kgs', style: {color: Highcharts.getOptions().colors[0]} }
    					}, { // Tertiary yAxis
        					gridLineWidth: 0,
        					title: {text: 'M.N.', style: {color: Highcharts.getOptions().colors[1]} },
        					labels: {format: '${value} mn', style: {color: Highcharts.getOptions().colors[1]} },
        					opposite: true
    			}],
    			//tooltip: {shared: true},
    			tooltip: {
        					headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            							'<td style="padding:0"><b>{point.y:.2f} kgs</b></td></tr>',
        					footerFormat: '</table>',
        					shared: true,
        					useHTML: true
    			},
    			legend: { layout: 'vertical', align: 'right', x: -10, verticalAlign: 'top', y: -5, floating: true, 
        				  backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || // theme
            			  'rgba(255,255,255,0.25)'
    					},
    			series: [{
        				name: 'kgs',
        				type: 'column',
        				yAxis: 1,
        				colorByPoint: true,
        				dataLabels: { enabled: true,
        					//rotation: -90,
        					color: 'blue',
            				align: 'center',
            				format: '{point.y:,.2f}', // one decimal
            				y: 5, // 10 pixels down from the top
        				},
        				data: [v1, v2, v3, v4, v5, v6],
        				tooltip: { valueSuffix: ' kgs' }
					 }, {
        				name: 'M.N. ',
        				type: 'spline',
        				yAxis: 2,
        				dataLabels: { enabled: true,
        					//rotation: -90,
        					//color: '#FFFFFF',
            				align: 'center',
            				format: '{point.y:.2f}', // one decimal
            				y: 5, // 10 pixels down from the top
        				},
        				data: [pmn1, pmn2, pmn3, pmn4, pmn5, pmn6],
        				marker: { enabled: false },
        				dashStyle: 'shortdot',
        				tooltip: { valueSuffix: ' mn'  }

    				}, {
        				name: 'U.S.D.',
        				type: 'spline',
        				dataLabels: { enabled: true,
        					//rotation: -90,
        					color: 'green',
            				align: 'center',
            				format: '{point.y:.2f}', // one decimal
            				y: 5, // 10 pixels down from the top
        				},
        				data: [pus1, pus2, pus3, pus4, pus5, pus6],
        				tooltip: { valueSuffix: ' usd' }
    			}],
    			responsive: {
        			rules: [{
            		condition: {  maxWidth: 500  },
            		chartOptions: {
                		legend: { floating: false, layout: 'horizontal', align: 'center', verticalAlign: 'bottom', x: 0, y: 0 },
                			  yAxis: [{
                    					labels: { align: 'right', 	x: 0,	y: -6	},
                    					showLastLabel: false
                					  }, {
                    					labels: { align: 'left', x: 0,  y: -6  },
                    					showLastLabel: false
                					  }, {
                    					visible: false
                				}]
            		}
        		}]
    		}
		});
		

			
	   },   
	});
	
			

	
	$("#tabla_vtam").ajaxSorter({
		url:'<?php echo base_url()?>index.php/analisisvta/tablavtam',  
		filters:['cicm','almacenes','clientes','Tallas1'],
		//active:['color',1],
        sort:false,
        onSuccess:function(){
       		$('#tabla_vtam tbody tr').map(function(){
				if($(this).data('ano')=='Total') {	    				    			
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('font-size','12px');
	    		}
	    		
			});
			$('#tabla_vtam tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
			$('#tabla_vtam tbody tr td:nth-child(3)').map(function(){ $(this).css('font-weight','bold');});
	    	$('#tabla_vtam tbody tr td:nth-child(4)').map(function(){ $(this).css('font-weight','bold');});
	    	nombre=''; 	if($("#nombre").val()!=0) nombre='- Cliente: '+$("#nombre").val();
	    	$('#tabla_vtam tbody tr').map(function(){
	    		if($(this).data('kgs')!=''){kgstot=$(this).data('kgs');} else {kgstot='';} 
	    		if($(this).data('m1')!=''){m1=$(this).data('m1');v1=$(this).data('v1');p1=$(this).data('p1');
	    									if($(this).data('a1')!='') a1=$(this).data('a1'); else a1=''; 
	    									if($(this).data('mn1')!=''){mn1=$(this).data('mn1');pmn1=$(this).data('pmn1');} else {mn1='-';pmn1='';}
	    									if($(this).data('us1')!=''){us1=$(this).data('us1');pus1=$(this).data('pus1');} else {us1='-';pus1='';}
	    									} else{a1='';p1='';mn1='';us1='';v1='';pmn1='';pus1='';m1='';}
	    		if($(this).data('m2')!=''){m2=$(this).data('m2');v2=$(this).data('v2');p2=$(this).data('p2');
	    									if($(this).data('a2')!='') a2=$(this).data('a2'); else a2=''; 
	    									if($(this).data('mn2')!=''){mn2=$(this).data('mn2');pmn2=$(this).data('pmn2');} else {mn2='-';pmn2='';}
	    									if($(this).data('us2')!=''){us2=$(this).data('us2');pus2=$(this).data('pus2');} else {us2='-';pus2='';}
	    									} else{a2='';p2='';mn2='';us2='';v2='';pmn2='';pus2='';m2='';}
	    		if($(this).data('m3')!=''){m3=$(this).data('m3');v3=$(this).data('v3');p3=$(this).data('p3');
	    									if($(this).data('a3')!='') a3=$(this).data('a3'); else a3=''; 
	    									if($(this).data('mn3')!=''){mn3=$(this).data('mn3');pmn3=$(this).data('pmn3');} else {mn3='-';pmn3='';}
	    									if($(this).data('us3')!=''){us3=$(this).data('us3');pus3=$(this).data('pus3');} else {us3='-';pus3='';}
	    									} else{a3='';p3='';mn3='';us3='';v3='';pmn3='';pus3='';m3='';}
	    		if($(this).data('m4')!=''){m4=$(this).data('m4');v4=$(this).data('v4');p4=$(this).data('p4');
	    									if($(this).data('a4')!='') a4=$(this).data('a4'); else a4=''; 
	    									if($(this).data('mn4')!=''){mn4=$(this).data('mn4');pmn4=$(this).data('pmn4');} else {mn4='-';pmn4='';}
	    									if($(this).data('us4')!=''){us4=$(this).data('us4');pus4=$(this).data('pus4');} else {us4='-';pus4='';}
	    									} else{a4='';p4='';mn4='';us4='';v4='';pmn4='';pus4='';m4='';}	
	    		if($(this).data('m5')!=''){m5=$(this).data('m5');v5=$(this).data('v5');p5=$(this).data('p5');
	    									if($(this).data('a5')!='') a5=$(this).data('a5'); else a5=''; 
	    									if($(this).data('mn5')!=''){mn5=$(this).data('mn5');pmn5=$(this).data('pmn5');} else {mn5='-';pmn5='';}
	    									if($(this).data('us5')!=''){us5=$(this).data('us5');pus5=$(this).data('pus5');} else {us5='-';pus5='';}
	    									} else{a5='';p5='';mn5='';us5='';v5='';pmn5='';pus5='';m5='';}
	    		if($(this).data('m6')!=''){m6=$(this).data('m6');v6=$(this).data('v6');p6=$(this).data('p6');
	    									if($(this).data('a6')!='') a6=$(this).data('a6'); else a6=''; 
	    									if($(this).data('mn6')!=''){mn6=$(this).data('mn6');pmn6=$(this).data('pmn6');} else {mn6='-';pmn6='';}
	    									if($(this).data('us6')!=''){us6=$(this).data('us6');pus6=$(this).data('pus6');} else {us6='-';pus6='';}
	    									} else{a6='';p6='';mn6='';us6='';v6='';pmn6='';pus6='';m6='';}
	    		if($(this).data('m7')!=''){m7=$(this).data('m7');v7=$(this).data('v7');p7=$(this).data('p7');
	    									if($(this).data('a7')!='') a7=$(this).data('a7'); else a7=''; 
	    									if($(this).data('mn7')!=''){mn7=$(this).data('mn7');pmn7=$(this).data('pmn7');} else {mn7='-';pmn7='';}
	    									if($(this).data('us7')!=''){us7=$(this).data('us7');pus7=$(this).data('pus7');} else {us7='-';pus7='';}
	    									} else{a7='';p7='';mn7='';us7='';v7='';pmn7='';pus7='';m7='';}	
	    		if($(this).data('m8')!=''){m8=$(this).data('m8');v8=$(this).data('v8');p8=$(this).data('p8');
	    									if($(this).data('a8')!='') a8=$(this).data('a8'); else a8=''; 
	    									if($(this).data('mn8')!=''){mn8=$(this).data('mn8');pmn8=$(this).data('pmn8');} else {mn8='-';pmn8='';}
	    									if($(this).data('us8')!=''){us8=$(this).data('us8');pus8=$(this).data('pus8');} else {us8='-';pus8='';}
	    									} else{a8='';p8='';mn8='';us8='';v8='';pmn8='';pus8='';m8='';}
	    		if($(this).data('m9')!=''){m9=$(this).data('m9');v9=$(this).data('v9');p9=$(this).data('p9');
	    									if($(this).data('a9')!='') a9=$(this).data('a9'); else a9=''; 
	    									if($(this).data('mn9')!=''){mn9=$(this).data('mn9');pmn9=$(this).data('pmn9');} else {mn9='-';pmn9='';}
	    									if($(this).data('us9')!=''){us9=$(this).data('us9');pus9=$(this).data('pus9');} else {us9='-';pus9='';}
	    									} else{a9='';p9='';mn9='';us9='';v9='';pmn9='';pus9='';m9='';}	
	    		if($(this).data('m10')!=''){m10=$(this).data('m10');v10=$(this).data('v10');p10=$(this).data('p10');
	    									if($(this).data('a10')!='') a10=$(this).data('a10'); else a10=''; 
	    									if($(this).data('mn10')!=''){mn10=$(this).data('mn10');pmn10=$(this).data('pmn10');} else {mn10='-';pmn10='';}
	    									if($(this).data('us10')!=''){us10=$(this).data('us10');pus10=$(this).data('pus10');} else {us10='-';pus10='';}
	    									} else{a10='';p10='';mn10='';us10='';v10='';pmn10='';pus10='';m10='';}	
	    		if($(this).data('m11')!=''){m11=$(this).data('m11');v11=$(this).data('v11');p11=$(this).data('p11');
	    									if($(this).data('a11')!='') a11=$(this).data('a11'); else a11=''; 
	    									if($(this).data('mn11')!=''){mn11=$(this).data('mn11');pmn11=$(this).data('pmn11');} else {mn11='-';pmn11='';}
	    									if($(this).data('us11')!=''){us11=$(this).data('us11');pus11=$(this).data('pus11');} else {us11='-';pus11='';}
	    									} else{a11='';p11='';mn11='';us11='';v11='';pmn11='';pus11='';m11='';}	
	    		if($(this).data('m12')!=''){m12=$(this).data('m12');v12=$(this).data('v12');p12=$(this).data('p12');
	    									if($(this).data('a12')!='') a12=$(this).data('a12'); else a12=''; 
	    									if($(this).data('mn12')!=''){mn12=$(this).data('mn12');pmn12=$(this).data('pmn12');} else {mn12='-';pmn12='';}
	    									if($(this).data('us12')!=''){us12=$(this).data('us12');pus12=$(this).data('pus12');} else {us12='-';pus12='';}
	    									} else{a12='';p12='';mn12='';us12='';v12='';pmn12='';pus12='';m12='';}	
	    		if($(this).data('m13')!=''){m13=$(this).data('m13');v13=$(this).data('v13');p13=$(this).data('p13');
	    									if($(this).data('a13')!='') a13=$(this).data('a13'); else a13=''; 
	    									if($(this).data('mn13')!=''){mn13=$(this).data('mn13');pmn13=$(this).data('pmn13');} else {mn13='-';pmn13='';}
	    									if($(this).data('us13')!=''){us13=$(this).data('us13');pus13=$(this).data('pus13');} else {us13='-';pus13='';}
	    									} else{a13='';p13='';mn13='';us13='';v13='';pmn13='';pus13='';m13='';}							
	    		if($(this).data('m14')!=''){m14=$(this).data('m14');v14=$(this).data('v14');p14=$(this).data('p14');
	    									if($(this).data('a14')!='') a14=$(this).data('a14'); else a14=''; 
	    									if($(this).data('mn14')!=''){mn14=$(this).data('mn14');pmn14=$(this).data('pmn14');} else {mn14='-';pmn14='';}
	    									if($(this).data('us14')!=''){us14=$(this).data('us14');pus14=$(this).data('pus14');} else {us14='-';pus14='';}
	    									} else{a14='';p14='';mn14='';us14='';v14='';pmn14='';pus14='';m14='';}							
	    		if($(this).data('m15')!=''){m15=$(this).data('m15');v15=$(this).data('v15');p15=$(this).data('p15');
	    									if($(this).data('a15')!='') a15=$(this).data('a15'); else a15=''; 
	    									if($(this).data('mn15')!=''){mn15=$(this).data('mn15');pmn15=$(this).data('pmn15');} else {mn15='-';pmn15='';}
	    									if($(this).data('us15')!=''){us15=$(this).data('us15');pus15=$(this).data('pus15');} else {us15='-';pus15='';}
	    									} else{a15='';p15='';mn15='';us15='';v15='';pmn15='';pus15='';m15='';}						
	    	});	
	    	
	    	Highcharts.chart('vtamesgra', {
    			chart: {zoomType: 'xy'},
    			title: { text: 'Ventas Totales de Camarón: '+kgstot+' kgs' ,  align: 'left'},
    			subtitle: { text: 'Mensuales '+nombre,   align: 'left'},
    			xAxis: [{
    				 labels: {	rotation: 0,
            					style: {fontSize: '10px',fontFamily: 'Verdana, sans-serif'}
        						},
        				categories: [m1+'<br>['+p1+'%]<br>MN<br>'+mn1+'<br>USD<br>'+us1+'<br>'+a1, 
        							 m2+'<br>['+p2+'%]<br>MN<br>'+mn2+'<br>USD<br>'+us2+'<br>'+a2,
        							 m3+'<br>['+p3+'%]<br>MN<br>'+mn3+'<br>USD<br>'+us3+'<br>'+a3,
        							 m4+'<br>['+p4+'%]<br>MN<br>'+mn4+'<br>USD<br>'+us4+'<br>'+a4,
        							 m5+'<br>['+p5+'%]<br>MN<br>'+mn5+'<br>USD<br>'+us5+'<br>'+a5,
        							 m6+'<br>['+p6+'%]<br>MN<br>'+mn6+'<br>USD<br>'+us6+'<br>'+a6,
        							 m7+'<br>['+p7+'%]<br>MN<br>'+mn7+'<br>USD<br>'+us7+'<br>'+a7,
        							 m8+'<br>['+p8+'%]<br>MN<br>'+mn8+'<br>USD<br>'+us8+'<br>'+a8,
        							 m9+'<br>['+p9+'%]<br>MN<br>'+mn9+'<br>USD<br>'+us9+'<br>'+a9,
        							 m10+'<br>['+p10+'%]<br>MN<br>'+mn10+'<br>USD<br>'+us10+'<br>'+a10,
        							 m11+'<br>['+p11+'%]<br>MN<br>'+mn11+'<br>USD<br>'+us11+'<br>'+a11,
        							 m12+'<br>['+p12+'%]<br>MN<br>'+mn12+'<br>USD<br>'+us12+'<br>'+a12,
        							 m13+'<br>['+p13+'%]<br>MN<br>'+mn13+'<br>USD<br>'+us13+'<br>'+a13,
        							 m14+'<br>['+p14+'%]<br>MN<br>'+mn14+'<br>USD<br>'+us14+'<br>'+a14,
        							 m15+'<br>['+p15+'%]<br>MN<br>'+mn15+'<br>USD<br>'+us15+'<br>'+a15
        							 ],
        				crosshair: true
    			}],
    			yAxis: [{ // Primary yAxis
        					labels: {format: '${value} usd', style: {color: Highcharts.getOptions().colors[2] }	},
        					title: {text: 'U.S.D.', style: {color: Highcharts.getOptions().colors[2]}  },
        					opposite: true
			    		}, { // Secondary yAxis
        					gridLineWidth: 0,
        					title: {text: 'Camarón', style: {color: Highcharts.getOptions().colors[0]} },
        					labels: {format: '{value} kgs', style: {color: Highcharts.getOptions().colors[0]} }
    					}, { // Tertiary yAxis
        					gridLineWidth: 0,
        					title: {text: 'M.N.', style: {color: Highcharts.getOptions().colors[1]} },
        					labels: {format: '${value} mn', style: {color: Highcharts.getOptions().colors[1]} },
        					opposite: true
    			}],
    			//tooltip: {shared: true},
    			tooltip: {
        					headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            							'<td style="padding:0"><b>{point.y:.2f} kgs</b></td></tr>',
        					footerFormat: '</table>',
        					shared: true,
        					useHTML: true
    			},
    			legend: { layout: 'vertical', align: 'right', x: -10, verticalAlign: 'top', y: -5, floating: true, 
        				  backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || // theme
            			  'rgba(255,255,255,0.25)'
    					},
    			series: [{
        				name: 'kgs',
        				type: 'column',
        				yAxis: 1,
        				colorByPoint: true,
        				dataLabels: { enabled: true,
        					//rotation: -90,
        					color: 'blue',
            				align: 'center',
            				format: '{point.y:,.2f}', // one decimal
            				y: 5, // 10 pixels down from the top
        				},
        				data: [v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15],
        				tooltip: { valueSuffix: ' kgs' }
					 }, {
        				name: 'M.N. ',
        				type: 'spline',
        				yAxis: 2,
        				dataLabels: { enabled: true,
        					//rotation: -90,
        					//color: '#FFFFFF',
            				align: 'center',
            				format: '{point.y:.2f}', // one decimal
            				y: 10, // 10 pixels down from the top
        				},
        				data: [pmn1, pmn2, pmn3, pmn4, pmn5, pmn6, pmn7, pmn8, pmn9, pmn10, pmn11, pmn12, pmn13, pmn14, pmn15],
        				marker: { enabled: false },
        				dashStyle: 'shortdot',
        				tooltip: { valueSuffix: ' mn'  }

    				}, {
        				name: 'U.S.D.',
        				type: 'spline',
        				dataLabels: { enabled: true,
        					//rotation: -90,
        					color: 'green',
            				align: 'center',
            				format: '{point.y:.2f}', // one decimal
            				y: 10, // 10 pixels down from the top
        				},
        				data: [pus1, pus2, pus3, pus4, pus5, pus6, pus7, pus8, pus9, pus10, pus11, pus12, pus13, pus14],
        				tooltip: { valueSuffix: ' usd' }
    			}],
    			responsive: {
        			rules: [{
            		condition: {  maxWidth: 500  },
            		chartOptions: {
                		legend: { floating: false, layout: 'horizontal', align: 'center', verticalAlign: 'bottom', x: 0, y: 0 },
                			  yAxis: [{
                    					labels: { align: 'right', 	x: 0,	y: -6	},
                    					showLastLabel: false
                					  }, {
                    					labels: { align: 'left', x: 0,  y: -6  },
                    					showLastLabel: false
                					  }, {
                    					visible: false
                				}]
            		}
        		}]
    		}
		}); 
	    	/*
	    	Highcharts.chart('vtamesgra', {
   				 chart: { type: 'column' },
    			 title: { text: 'Ventas Totales de Camarón: '+kgstot+' kgs' ,  align: 'left'},
    			 subtitle: { text: 'Mensuales '+nombre,   align: 'left'},
    			 xAxis: {
        					type: 'category',
					        labels: {	rotation: 0,
            							style: {fontSize: '10px',fontFamily: 'Verdana, sans-serif'}
        							}
    			},
    			yAxis: {
        				min: 0,
        				title: {  text: 'Camarón'  },
        				labels: {format: '{value} kgs', style: {color: Highcharts.getOptions().colors[0]} }
    			},
    			legend: {   enabled: false  },
    			tooltip: {  pointFormat: 'Total Mes: <b>{point.y:,.2f} kgs</b>' },
    			series: [{
					        name: 'Mes',
        					data: [ [m1+'<br>['+p1+'%]<br>MN<br>'+mn1+'<br>USD<br>'+us1+'<br>'+a1, v1],
        							[m2+'<br>['+p2+'%]<br>MN<br>'+mn2+'<br>USD<br>'+us2+'<br>'+a2, v2],
        							[m3+'<br>['+p3+'%]<br>MN<br>'+mn3+'<br>USD<br>'+us3+'<br>'+a3, v3],
        							[m4+'<br>['+p4+'%]<br>MN<br>'+mn4+'<br>USD<br>'+us4+'<br>'+a4, v4],
        							[m5+'<br>['+p5+'%]<br>MN<br>'+mn5+'<br>USD<br>'+us5+'<br>'+a5, v5],
        							[m6+'<br>['+p6+'%]<br>MN<br>'+mn6+'<br>USD<br>'+us6+'<br>'+a6, v6],
        							[m7+'<br>['+p7+'%]<br>MN<br>'+mn7+'<br>USD<br>'+us7+'<br>'+a7, v7],
        							[m8+'<br>['+p8+'%]<br>MN<br>'+mn8+'<br>USD<br>'+us8+'<br>'+a8, v8],
        							[m9+'<br>['+p9+'%]<br>MN<br>'+mn9+'<br>USD<br>'+us9+'<br>'+a9, v9],
        							[m10+'<br>['+p10+'%]<br>MN<br>'+mn10+'<br>USD<br>'+us10+'<br>'+a10, v10],
        							[m11+'<br>['+p11+'%]<br>MN<br>'+mn11+'<br>USD<br>'+us11+'<br>'+a11, v11],
        							[m12+'<br>['+p12+'%]<br>MN<br>'+mn12+'<br>USD<br>'+us12+'<br>'+a12, v12],
        							 ],
        					dataLabels: {   enabled: true,
            								//rotation: -90,
            								color: '#FFFFFF',
            								align: 'center',
            								format: '{point.y:,.2f}', // one decimal
            								y: 10, // 10 pixels down from the top
            								style: {	fontSize: '10px', fontFamily: 'Verdana, sans-serif' }
        					}
    			}]
			});
			*/
			
			
			
	   },   
	});
	
	
	$("#tabla_vtat").ajaxSorter({
		url:'<?php echo base_url()?>index.php/analisisvta/tablavtat',  
		filters:['cict','almacenes','clientes','Tallas1'],
		//active:['color',1],
        sort:false,
        onSuccess:function(){
       		$('#tabla_vtat tbody tr').map(function(){
				if($(this).data('grupo')=='Total') {	    				    			
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('font-size','12px');
	    		}
	    		
			});
			$('#tabla_vtat tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
			$('#tabla_vtat tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold');});
			$('#tabla_vtat tbody tr td:nth-child(3)').map(function(){ $(this).css('font-weight','bold');});
			nombre=''; 	if($("#nombre").val()!=0) nombre='- Cliente: '+$("#nombre").val();
			$('#tabla_vtat tbody tr').map(function(){
	    		if($(this).data('kgs')!=''){kgstot=$(this).data('kgs');} else {kgstot='';} 
	    		if($(this).data('a1')!=''){a1=$(this).data('a1');v1=$(this).data('v1');p1=$(this).data('p1');
	    									if($(this).data('mn1')!=''){mn1=$(this).data('mn1');pmn1=$(this).data('pmn1');} else {mn1='-';pmn1='';}
	    									if($(this).data('us1')!=''){us1=$(this).data('us1');pus1=$(this).data('pus1');} else {us1='-';pus1='';}
	    									} else{a1='';p1='';mn1='';us1='';v1='';pmn1='';pus1='';}	
	    		if($(this).data('a2')!=''){a2=$(this).data('a2');v2=$(this).data('v2');p2=$(this).data('p2');
	    									if($(this).data('mn2')!=''){mn2=$(this).data('mn2');pmn2=$(this).data('pmn2');} else {mn2='-';pmn2='';}
	    									if($(this).data('us2')!=''){us2=$(this).data('us2');pus2=$(this).data('pus2');} else {us2='-';pus2='';}
	    									} else{a2='';p2='';mn2='';us2='';v2='';pmn2='';pus2='';}	
	    		if($(this).data('a3')!=''){a3=$(this).data('a3');v3=$(this).data('v3');p3=$(this).data('p3');
	    									if($(this).data('mn3')!=''){mn3=$(this).data('mn3');pmn3=$(this).data('pmn3');} else {mn3='-';pmn3='';}
	    									if($(this).data('us3')!=''){us3=$(this).data('us3');pus3=$(this).data('pus3');} else {us3='-';pus3='';}
	    									} else{a3='';p3='';mn3='';us3='';v3='';pmn3='';pus3='';}	
	    		if($(this).data('a4')!=''){a4=$(this).data('a4');v4=$(this).data('v4');p4=$(this).data('p4');
	    									if($(this).data('mn4')!=''){mn4=$(this).data('mn4');pmn4=$(this).data('pmn4');} else {mn4='-';pmn4='';}
	    									if($(this).data('us4')!=''){us4=$(this).data('us4');pus4=$(this).data('pus4');} else {us4='-';pus4='';}
	    									} else{a4='';p4='';mn4='';us4='';v4='';pmn4='';pus4='';}	
	    		if($(this).data('a5')!=''){a5=$(this).data('a5');v5=$(this).data('v5');p5=$(this).data('p5');
	    									if($(this).data('mn5')!=''){mn5=$(this).data('mn5');pmn5=$(this).data('pmn5');} else {mn5='-';pmn5='';}
	    									if($(this).data('us5')!=''){us5=$(this).data('us5');pus5=$(this).data('pus5');} else {us5='-';pus5='';}
	    									} else{a5='';p5='';mn5='';us5='';v5='';pmn5='';pus5='';}	
	    		if($(this).data('a6')!=''){a6=$(this).data('a6');v6=$(this).data('v6');p6=$(this).data('p6');
	    									if($(this).data('mn6')!=''){mn6=$(this).data('mn6');pmn6=$(this).data('pmn6');} else {mn6='-';pmn6='';}
	    									if($(this).data('us6')!=''){us6=$(this).data('us6');pus6=$(this).data('pus6');} else {us6='-';pus6='';}
	    									} else{a6='';p6='';mn6='';us6='';v6='';pmn6='';pus6='';}	
	    		if($(this).data('a7')!=''){a7=$(this).data('a7');v7=$(this).data('v7');p7=$(this).data('p7');
	    									if($(this).data('mn7')!=''){mn7=$(this).data('mn7');pmn7=$(this).data('pmn7');} else {mn7='-';pmn7='';}
	    									if($(this).data('us7')!=''){us7=$(this).data('us7');pus7=$(this).data('pus7');} else {us7='-';pus7='';}
	    									} else{a7='';p7='';mn7='';us7='';v7='';pmn7='';pus7='';}	
	    		if($(this).data('a8')!=''){a8=$(this).data('a8');v8=$(this).data('v8');p8=$(this).data('p8');
	    									if($(this).data('mn8')!=''){mn8=$(this).data('mn8');pmn8=$(this).data('pmn8');} else {mn8='-';pmn8='';}
	    									if($(this).data('us8')!=''){us8=$(this).data('us8');pus8=$(this).data('pus8');} else {us8='-';pus8='';}
	    									} else{a8='';p8='';mn8='';us8='';v8='';pmn8='';pus8='';}															
				if($(this).data('a9')!=''){a9=$(this).data('a9');v9=$(this).data('v9');p9=$(this).data('p9');
	    									if($(this).data('mn9')!=''){mn9=$(this).data('mn9');pmn9=$(this).data('pmn9');} else {mn9='-';pmn9='';}
	    									if($(this).data('us9')!=''){us9=$(this).data('us9');pus9=$(this).data('pus9');} else {us9='-';pus9='';}
	    									} else{a9='';p9='';mn9='';us9='';v9='';pmn9='';pus9='';}	
	    		if($(this).data('a10')!=''){a10=$(this).data('a10');v10=$(this).data('v10');p10=$(this).data('p10');
	    									if($(this).data('mn10')!=''){mn10=$(this).data('mn10');pmn10=$(this).data('pmn10');} else {mn10='-';pmn10='';}
	    									if($(this).data('us10')!=''){us10=$(this).data('us10');pus10=$(this).data('pus10');} else {us10='-';pus10='';}
	    									} else{a10='';p10='';mn10='';us10='';v10='';pmn10='';pus10='';}		    									
	    	});	
	    	/*
	    	//if(entro>0){
	    	Highcharts.chart('vtatalgra', {
   				 chart: { type: 'column' },
    			 title: { text: 'Ventas Totales por Talla de Camarón: '+kgstot+' kgs',  align: 'left'},
    			 subtitle: { text: 'Se muestran las 10 Tallas más vendidas en el mercado. '+nombre,  align: 'left'},
    			 xAxis: {
        					type: 'category',
					        labels: {	//rotation: -45,
            							style: {fontSize: '10px',fontFamily: 'Verdana, sans-serif'}
        							}
    			},
    			yAxis: {
        				min: 0,
        				title: {  text: 'Camarón'  },
        				labels: {format: '{value} kgs', style: {color: Highcharts.getOptions().colors[0]} }
    			},
    			legend: {   enabled: false  },
    			tooltip: {  pointFormat: 'Total Almacen: <b>{point.y:,.2f} kgs</b>' },
    			series: [{
					        name: 'Almacenes',
        					data: [ [a1+'<br>['+p1+'%]<br>MN<br>'+mn1+'<br>USD<br>'+us1+'<br>', v1],
        							[a2+'<br>['+p2+'%]<br>MN<br>'+mn2+'<br>USD<br>'+us2+'<br>', v2],
        							[a3+'<br>['+p3+'%]<br>MN<br>'+mn3+'<br>USD<br>'+us3+'<br>', v3],
        							[a4+'<br>['+p4+'%]<br>MN<br>'+mn4+'<br>USD<br>'+us4+'<br>', v4],
        							[a5+'<br>['+p5+'%]<br>MN<br>'+mn5+'<br>USD<br>'+us5+'<br>', v5],
        							[a6+'<br>['+p6+'%]<br>MN<br>'+mn6+'<br>USD<br>'+us6+'<br>', v6],
        							[a7+'<br>['+p7+'%]<br>MN<br>'+mn7+'<br>USD<br>'+us7+'<br>', v7],
        							[a8+'<br>['+p8+'%]<br>MN<br>'+mn8+'<br>USD<br>'+us8+'<br>', v8],
        							[a9+'<br>['+p9+'%]<br>MN<br>'+mn9+'<br>USD<br>'+us9+'<br>', v9],
        							[a10+'<br>['+p10+'%]<br>MN<br>'+mn10+'<br>USD<br>'+us10+'<br>', v10]],
        					dataLabels: {   enabled: true,
            								//rotation: -90,
            								color: '#FFFFFF',
            								align: 'center',
            								format: '{point.y:,.2f}', // one decimal
            								y: 10, // 10 pixels down from the top
            								style: {	fontSize: '10px', fontFamily: 'Verdana, sans-serif' }
        					}
    			}]
			});
			//}	  
			*/
			Highcharts.chart('vtatalgra', {
    			chart: {zoomType: 'xy'},
    			title: { text: 'Ventas Totales por Talla de Camarón: '+kgstot+' kgs',  align: 'left'},
    			subtitle: { text: 'Se muestran las 10 Tallas más vendidas en el mercado. '+nombre,  align: 'left'},
    			xAxis: [{
    				 labels: {	rotation: 0,
            					style: {fontSize: '10px',fontFamily: 'Verdana, sans-serif'}
        						},
        				categories: [a1+'<br>['+p1+'%]<br>MN<br>'+mn1+'<br>USD<br>'+us1+'<br>', 
        							 a2+'<br>['+p2+'%]<br>MN<br>'+mn2+'<br>USD<br>'+us2+'<br>',
        							 a3+'<br>['+p3+'%]<br>MN<br>'+mn3+'<br>USD<br>'+us3+'<br>',
        							 a4+'<br>['+p4+'%]<br>MN<br>'+mn4+'<br>USD<br>'+us4+'<br>',
        							 a5+'<br>['+p5+'%]<br>MN<br>'+mn5+'<br>USD<br>'+us5+'<br>',
        							 a6+'<br>['+p6+'%]<br>MN<br>'+mn6+'<br>USD<br>'+us6+'<br>',
        							 a7+'<br>['+p7+'%]<br>MN<br>'+mn7+'<br>USD<br>'+us7+'<br>',
        							 a8+'<br>['+p8+'%]<br>MN<br>'+mn8+'<br>USD<br>'+us8+'<br>',
        							 a9+'<br>['+p9+'%]<br>MN<br>'+mn9+'<br>USD<br>'+us9+'<br>',
        							 a10+'<br>['+p10+'%]<br>MN<br>'+mn10+'<br>USD<br>'+us10+'<br>'
        							 ],
        				crosshair: true
    			}],
    			yAxis: [{ // Primary yAxis
        					labels: {format: '${value} usd', style: {color: Highcharts.getOptions().colors[2] }	},
        					title: {text: 'U.S.D.', style: {color: Highcharts.getOptions().colors[2]}  },
        					opposite: true
			    		}, { // Secondary yAxis
        					gridLineWidth: 0,
        					title: {text: 'Camarón', style: {color: Highcharts.getOptions().colors[0]} },
        					labels: {format: '{value} kgs', style: {color: Highcharts.getOptions().colors[0]} }
    					}, { // Tertiary yAxis
        					gridLineWidth: 0,
        					title: {text: 'M.N.', style: {color: Highcharts.getOptions().colors[1]} },
        					labels: {format: '${value} mn', style: {color: Highcharts.getOptions().colors[1]} },
        					opposite: true
    			}],
    			//tooltip: {shared: true},
    			tooltip: {
        					headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            							'<td style="padding:0"><b>{point.y:.2f} kgs</b></td></tr>',
        					footerFormat: '</table>',
        					shared: true,
        					useHTML: true
    			},
    			legend: { layout: 'vertical', align: 'right', x: -10, verticalAlign: 'top', y: -5, floating: true, 
        				  backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || // theme
            			  'rgba(255,255,255,0.25)'
    					},
    			series: [{
        				name: 'kgs',
        				type: 'column',
        				yAxis: 1,
        				colorByPoint: true,
        				dataLabels: { enabled: true,
        					//rotation: -90,
        					color: 'blue',
            				align: 'center',
            				format: '{point.y:,.2f}', // one decimal
            				y: 5, // 10 pixels down from the top
        				},
        				data: [v1, v2, v3, v4, v5, v6, v7, v8, v9, v10],
        				tooltip: { valueSuffix: ' kgs' }
					 }, {
        				name: 'M.N. ',
        				type: 'spline',
        				yAxis: 2,
        				dataLabels: { enabled: true,
        					//rotation: -90,
        					//color: '#FFFFFF',
            				align: 'center',
            				format: '{point.y:.2f}', // one decimal
            				y: 10, // 10 pixels down from the top
        				},
        				data: [pmn1, pmn2, pmn3, pmn4, pmn5, pmn6, pmn7, pmn8, pmn9, pmn10],
        				marker: { enabled: false },
        				dashStyle: 'shortdot',
        				tooltip: { valueSuffix: ' mn'  }

    				}, {
        				name: 'U.S.D.',
        				type: 'spline',
        				dataLabels: { enabled: true,
        					//rotation: -90,
        					color: 'green',
            				align: 'center',
            				format: '{point.y:.2f}', // one decimal
            				y: 10, // 10 pixels down from the top
        				},
        				data: [pus1, pus2, pus3, pus4, pus5, pus6, pus7, pus8, pus9, pus10],
        				tooltip: { valueSuffix: ' usd' }
    			}],
    			responsive: {
        			rules: [{
            		condition: {  maxWidth: 500  },
            		chartOptions: {
                		legend: { floating: false, layout: 'horizontal', align: 'center', verticalAlign: 'bottom', x: 0, y: 0 },
                			  yAxis: [{
                    					labels: { align: 'right', 	x: 0,	y: -6	},
                    					showLastLabel: false
                					  }, {
                    					labels: { align: 'left', x: 0,  y: -6  },
                    					showLastLabel: false
                					  }, {
                    					visible: false
                				}]
            		}
        		}]
    		}
		});  			
	   },   
	});
	
	
	$("#tabla_vtamt").ajaxSorter({
		url:'<?php echo base_url()?>index.php/analisisvta/tablavtamt',  
		filters:['cicmt','almacenes','clientes','rep'],
		//filters:['cicmt','almacenes','clientes','Tallas1'],
		//active:['color',1],
        sort:false,
        onSuccess:function(){
       		$('#tabla_vtamt tbody tr').map(function(){
				if($(this).data('grupo')=='Total') {	    				    			
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('font-size','12px');
	    		}
	    		
			});
			$('#tabla_vtamt tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
			$('#tabla_vtamt tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold');});
	    	$('#tabla_vtamt tbody tr td:nth-child(3)').map(function(){ $(this).css('font-weight','bold');});
	    	nombre=''; 	if($("#nombre").val()!=0) nombre='- Cliente: '+$("#nombre").val();
	    	$('#tabla_vtamt tbody tr').map(function(){
	    		if($(this).data('kgs')!=''){kgstot=$(this).data('kgs');} else {kgstot='';} 
	    		if($(this).data('mm1')!=''){m1=$(this).data('mm1');v1=$(this).data('v1');p1=$(this).data('p1');
	    									t1=$(this).data('t1');
	    									t1m1=$(this).data('t1m1');t1m2=$(this).data('t1m2');t1m3=$(this).data('t1m3');t1m4=$(this).data('t1m4');t1m5=$(this).data('t1m5');
	    									t1m6=$(this).data('t1m6');t1m7=$(this).data('t1m7');t1m8=$(this).data('t1m8');t1m9=$(this).data('t1m9');t1m10=$(this).data('t1m10');
	    									t1m11=$(this).data('t1m11');t1m12=$(this).data('t1m12');t1m13=$(this).data('t1m13');t1m14=$(this).data('t1m14');
	    									if($(this).data('a1')!='') a1=$(this).data('a1'); else a1=''; 
	    									} else{m1='';p1='';a1='';v1='';t1='';t1m1='';t1m2='';t1m3='';t1m4='';t1m5='';t1m6='';t1m7='';t1m8='';t1m9='';t1m10='';
	    									t1m11='';t1m12='';t1m13='';t1m14='';
	    									}	
	    		if($(this).data('mm2')!=''){m2=$(this).data('mm2');v2=$(this).data('v2');p2=$(this).data('p2');
	    									t2=$(this).data('t2');
	    									t2m1=$(this).data('t2m1');t2m2=$(this).data('t2m2');t2m3=$(this).data('t2m3');t2m4=$(this).data('t2m4');t2m5=$(this).data('t2m5');
	    									t2m6=$(this).data('t2m6');t2m7=$(this).data('t2m7');t2m8=$(this).data('t2m8');t2m9=$(this).data('t2m9');t2m10=$(this).data('t2m10');
	    									t2m11=$(this).data('t2m11');t2m12=$(this).data('t2m12');t2m13=$(this).data('t2m13');t2m14=$(this).data('t2m14');
	    									if($(this).data('a2')!='') a2=$(this).data('a2'); else a2=''; 
	    									} else{m2='';p2='';a2='';v2='';t2='';t2m1='';t2m2='';t2m3='';t2m4='';t2m5='';t2m6='';t2m7='';t2m8='';t2m9='';t2m10='';
	    									t2m11='';t2m12='';t2m13='';t2m14='';
	    									}
	    		if($(this).data('mm3')!=''){m3=$(this).data('mm3');v3=$(this).data('v3');p3=$(this).data('p3');
	    									t3=$(this).data('t3');
	    									t3m1=$(this).data('t3m1');t3m2=$(this).data('t3m2');t3m3=$(this).data('t3m3');t3m4=$(this).data('t3m4');t3m5=$(this).data('t3m5');
	    									t3m6=$(this).data('t3m6');t3m7=$(this).data('t3m7');t3m8=$(this).data('t3m8');t3m9=$(this).data('t3m9');t3m10=$(this).data('t3m10');
	    									t3m11=$(this).data('t3m11');t3m12=$(this).data('t3m12');t3m13=$(this).data('t3m13');t3m14=$(this).data('t3m14');
	    									if($(this).data('a3')!='') a3=$(this).data('a3'); else a3=''; 
	    									} else{m3='';p3='';a3='';v3='';t3='';t3m1='';t3m2='';t3m3='';t3m4='';t3m5='';t3m6='';t3m7='';t3m8='';t3m9='';t3m10='';
	    									t3m11='';t3m12='';t3m13='';t3m14='';
	    									}
	    		if($(this).data('mm4')!=''){m4=$(this).data('mm4');v4=$(this).data('v4');p4=$(this).data('p4');
	    									t4=$(this).data('t4');
	    									t4m1=$(this).data('t4m1');t4m2=$(this).data('t4m2');t4m3=$(this).data('t4m3');t4m4=$(this).data('t4m4');t4m5=$(this).data('t4m5');
	    									t4m6=$(this).data('t4m6');t4m7=$(this).data('t4m7');t4m8=$(this).data('t4m8');t4m9=$(this).data('t4m9');t4m10=$(this).data('t4m10');
	    									t4m11=$(this).data('t4m11');t4m12=$(this).data('t4m12');t4m13=$(this).data('t4m13');t4m14=$(this).data('t4m14');
	    									if($(this).data('a4')!='') a4=$(this).data('a4'); else a4=''; 
	    									} else{m4='';p4='';a4='';v4='';t4='';t4m1='';t4m2='';t4m3='';t4m4='';t4m5='';t4m6='';t4m7='';t4m8='';t4m9='';t4m10='';
	    									t4m11='';t4m12='';t4m13='';t4m14='';
	    									}
	    		if($(this).data('mm5')!=''){m5=$(this).data('mm5');v5=$(this).data('v5');p5=$(this).data('p5');
	    									t5=$(this).data('t5');
	    									t5m1=$(this).data('t5m1');t5m2=$(this).data('t5m2');t5m3=$(this).data('t5m3');t5m4=$(this).data('t5m4');t5m5=$(this).data('t5m5');
	    									t5m6=$(this).data('t5m6');t5m7=$(this).data('t5m7');t5m8=$(this).data('t5m8');t5m9=$(this).data('t5m9');t5m10=$(this).data('t5m10');
	    									t5m11=$(this).data('t5m11');t5m12=$(this).data('t5m12');t5m13=$(this).data('t5m13');t5m14=$(this).data('t5m14');
	    									if($(this).data('a5')!='') a5=$(this).data('a5'); else a5=''; 
	    									} else{m5='';p5='';a5='';v5='';t5='';t5m1='';t5m2='';t5m3='';t5m4='';t5m5='';t5m6='';t5m7='';t5m8='';t5m9='';t5m10='';
	    									t5m11='';t5m12='';t5m13='';t5m14='';
	    									}
	    		if($(this).data('mm6')!=''){m6=$(this).data('mm6');v6=$(this).data('v6');p6=$(this).data('p6');
	    									t6=$(this).data('t6');
	    									t6m1=$(this).data('t6m1');t6m2=$(this).data('t6m2');t6m3=$(this).data('t6m3');t6m4=$(this).data('t6m4');t6m5=$(this).data('t6m5');
	    									t6m6=$(this).data('t6m6');t6m7=$(this).data('t6m7');t6m8=$(this).data('t6m8');t6m9=$(this).data('t6m9');t6m10=$(this).data('t6m10');
	    									t6m11=$(this).data('t6m11');t6m12=$(this).data('t6m12');t6m13=$(this).data('t6m13');t6m14=$(this).data('t6m14');
	    									if($(this).data('a6')!='') a6=$(this).data('a6'); else a6=''; 
	    									} else{m6='';p6='';a6='';v6='';t6='';t6m1='';t6m2='';t6m3='';t6m4='';t6m5='';t6m6='';t6m7='';t6m8='';t6m9='';t6m10='';
	    									t6m11='';t6m12='';t6m13='';t6m14='';
	    									}
	    		if($(this).data('mm7')!=''){m7=$(this).data('mm7');v7=$(this).data('v7');p7=$(this).data('p7');
	    									t7=$(this).data('t7');
	    									t7m1=$(this).data('t7m1');t7m2=$(this).data('t7m2');t7m3=$(this).data('t7m3');t7m4=$(this).data('t7m4');t7m5=$(this).data('t7m5');
	    									t7m6=$(this).data('t7m6');t7m7=$(this).data('t7m7');t7m8=$(this).data('t7m8');t7m9=$(this).data('t7m9');t7m10=$(this).data('t7m10');
	    									t7m11=$(this).data('t7m11');t7m12=$(this).data('t7m12');t7m13=$(this).data('t7m13');t7m14=$(this).data('t7m14');
	    									if($(this).data('a7')!='') a7=$(this).data('a7'); else a7=''; 
	    									} else{m7='';p7='';a7='';v7='';t7='';t7m1='';t7m2='';t7m3='';t7m4='';t7m5='';t7m6='';t7m7='';t7m8='';t7m9='';t7m10='';
	    									t7m11='';t7m12='';t7m13='';t7m14='';
	    									}
	    		if($(this).data('mm8')!=''){m8=$(this).data('mm8');v8=$(this).data('v8');p8=$(this).data('p8');
	    									t8=$(this).data('t8');
	    									t8m1=$(this).data('t8m1');t8m2=$(this).data('t8m2');t8m3=$(this).data('t8m3');t8m4=$(this).data('t8m4');t8m5=$(this).data('t8m5');
	    									t8m6=$(this).data('t8m6');t8m7=$(this).data('t8m7');t8m8=$(this).data('t8m8');t8m9=$(this).data('t8m9');t8m10=$(this).data('t8m10');
	    									t8m11=$(this).data('t8m11');t8m12=$(this).data('t8m12');t8m13=$(this).data('t8m13');t8m14=$(this).data('t8m14');
	    									if($(this).data('a8')!='') a8=$(this).data('a8'); else a8=''; 
	    									} else{m8='';p8='';a8='';v8='';t8='';t8m1='';t8m2='';t8m3='';t8m4='';t8m5='';t8m6='';t8m7='';t8m8='';t8m9='';t8m10='';
	    									t8m11='';t8m12='';t8m13='';t8m14='';
	    									}
	    		if($(this).data('mm9')!=''){m9=$(this).data('mm9');v9=$(this).data('v9');p9=$(this).data('p9');
	    									t9=$(this).data('t9');
	    									t9m1=$(this).data('t9m1');t9m2=$(this).data('t9m2');t9m3=$(this).data('t9m3');t9m4=$(this).data('t9m4');t9m5=$(this).data('t9m5');
	    									t9m6=$(this).data('t9m6');t9m7=$(this).data('t9m7');t9m8=$(this).data('t9m8');t9m9=$(this).data('t9m9');t9m10=$(this).data('t9m10');
	    									t9m11=$(this).data('t9m11');t9m12=$(this).data('t9m12');t9m13=$(this).data('t9m13');t9m14=$(this).data('t9m14');
	    									if($(this).data('a9')!='') a9=$(this).data('a9'); else a9=''; 
	    									} else{m9='';p9='';a9='';v9='';t9='';t9m1='';t9m2='';t9m3='';t9m4='';t9m5='';t9m6='';t9m7='';t9m8='';t9m9='';t9m10='';
	    									t9m11='';t9m12='';t9m13='';t9m14='';
	    									}
	    		if($(this).data('mm10')!=''){m10=$(this).data('mm10');v10=$(this).data('v10');p10=$(this).data('p10');
	    									t10=$(this).data('t10');
	    									t10m1=$(this).data('t10m1');t10m2=$(this).data('t10m2');t10m3=$(this).data('t10m3');t10m4=$(this).data('t10m4');t10m5=$(this).data('t10m5');
	    									t10m6=$(this).data('t10m6');t10m7=$(this).data('t10m7');t10m8=$(this).data('t10m8');t10m9=$(this).data('t10m9');t10m10=$(this).data('t10m10');
	    									t10m11=$(this).data('t10m11');t10m12=$(this).data('t10m12');t10m13=$(this).data('t10m13');t10m14=$(this).data('t10m14');
	    									if($(this).data('a10')!='') a10=$(this).data('a10'); else a10=''; 
	    									} else{a10='';m10='';p10='';v10='';t10='';t10m1='';t10m2='';t10m3='';t10m4='';t10m5='';t10m6='';t10m7='';t10m8='';t10m9='';t10m10='';
	    									t10m11='';t10m12='';t10m13='';t10m14='';
	    									}	
	    });	
	    	
			Highcharts.chart('vtamesgrat', {
   				 chart: { type: 'column' },
    			 title: { text: 'Ventas Totales de Camarón: '+kgstot+' kgs' ,  align: 'left'},
    			 subtitle: { text: 'Las 10 Tallas más vendidas en el Mercado - Mensuales por Talla '+nombre,   align: 'left'},
    			 legend: {
               				itemStyle: { fontWeight: 'bold', fontSize: '9px'}
                   			//color: 'white',
                 },
    			 xAxis: {
    			 		  // style: {fontSize: '8px',fontFamily: 'Verdana, sans-serif'},	
    			 	       categories: [m1+'<br>'+a1,m2+'<br>'+a2,m3+'<br>'+a3,m4+'<br>'+a4,m5+'<br>'+a5,m6+'<br>'+a6,m7+'<br>'+a7,
        								m8+'<br>'+a8,m9+'<br>'+a9,m10+'<br>'+a10,m11+'<br>'+a11,m12+'<br>'+a12,m13+'<br>'+a13,m14+'<br>'+a14],
        		 		   crosshair: true   
        		 },
    			 yAxis: {
        					min: 0,
        					title: {
            					text: 'Camarón (mm)'
        					}
    			},
    			tooltip: {
        					headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            							'<td style="padding:0"><b>{point.y:.2f} kgs</b></td></tr>',
        					footerFormat: '</table>',
        					shared: true,
        					useHTML: true
    			},
    			/*tooltip: {
        					headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            							'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        					footerFormat: '</table>',
        					shared: true,
        					useHTML: true
    			},*/
    			plotOptions: {
        						column: {
            								pointPadding: 0.2,
            								borderWidth: 0
        						}
    			},
    			series: [{
        						name: t1+'<br>['+v1+']'+'<br>['+p1+'%]',
        						data: [t1m1, t1m2, t1m3, t1m4, t1m5, t1m6, t1m7, t1m8, t1m9, t1m10, t1m11, t1m12, t1m13, t1m14]
						  }, {
        						name: t2+'<br>['+v2+']'+'<br>['+p2+'%]',
        						data: [t2m1, t2m2, t2m3, t2m4, t2m5, t2m6, t2m7, t2m8, t2m9, t2m10, t2m11, t2m12, t2m13, t2m14]
						  }, {
        						name: t3+'<br>['+v3+']'+'<br>['+p3+'%]',
        						data: [t3m1, t3m2, t3m3, t3m4, t3m5, t3m6, t3m7, t3m8, t3m9, t3m10, t3m11, t3m12, t3m13, t3m14]
						  }, {
        						name: t4+'<br>['+v4+']'+'<br>['+p4+'%]',
        						data: [t4m1, t4m2, t4m3, t4m4, t4m5, t4m6, t4m7, t4m8, t4m9, t4m10, t4m11, t4m12, t4m13, t4m14]
    					  }, {
    							name: t5+'<br>['+v5+']'+'<br>['+p5+'%]',
        						data: [t5m1, t5m2, t5m3, t5m4, t5m5, t5m6, t5m7, t5m8, t5m9, t5m10, t5m11, t5m12, t5m13, t5m14]
    				      }, {
    				    		name: t6+'<br>['+v6+']'+'<br>['+p6+'%]',
        						data: [t6m1, t6m2, t6m3, t6m4, t6m5, t6m6, t6m7, t6m8, t6m9, t6m10, t6m11, t6m12, t6m13, t6m14]
    				      }, {
    				      		name: t7+'<br>['+v7+']'+'<br>['+p7+'%]',
        						data: [t7m1, t7m2, t7m3, t7m4, t7m5, t7m6, t7m7, t7m8, t7m9, t7m10, t7m11, t7m12, t7m13, t7m14]
    				      }, {
    				      		name: t8+'<br>['+v8+']'+'<br>['+p8+'%]',
        						data: [t8m1, t8m2, t8m3, t8m4, t8m5, t8m6, t8m7, t8m8, t8m9, t8m10, t8m11, t8m12, t8m13, t8m14]
    				      }, {
    				      		name: t9+'<br>['+v9+']'+'<br>['+p9+'%]',
        						data: [t9m1, t9m2, t9m3, t9m4, t9m5, t9m6, t9m7, t9m8, t9m9, t9m10, t9m11, t9m12, t9m13, t9m14]
    				      }, {
    				      		name: t10+'<br>['+v10+']'+'<br>['+p10+'%]',
        						data: [t10m1, t10m2, t10m3, t10m4, t10m5, t10m6, t10m7, t10m8, t10m9, t10m10, t10m11, t10m12, t10m13, t10m14]
    				      }
    				    ]
			});	
		
	   },   
	});
	
	$("#tabla_vtact").ajaxSorter({
		url:'<?php echo base_url()?>index.php/analisisvta/tablavtact',  
		filters:['cicct','almacenes','clientes','cli2'],
		//filters:['cicmt','almacenes','clientes','Tallas1'],
		//active:['color',1],
        sort:false,
        onSuccess:function(){
       		$('#tabla_vtact tbody tr').map(function(){
				if($(this).data('grupo')=='Total') {	    				    			
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('font-size','12px');
	    		}
	    		
			});
			$('#tabla_vtact tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
			$('#tabla_vtact tbody tr td:nth-child(4)').map(function(){ $(this).css('font-weight','bold');});
	    	$('#tabla_vtact tbody tr td:nth-child(7)').map(function(){ $(this).css('font-weight','bold');});
	    	col=10; pasar=1;
	    	while(pasar<=2){
	    	$('#tabla_vtact tbody tr td:nth-child('+col+')').map(function(){ 
	    		$(this).css('font-weight','bold');
	    		if($(this).html() > 0 && $(this).html() != '' ) {
                	$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            	}
            	if($(this).html() == 0 && $(this).html() != '' ) {
                	$(this).css("background-color", "yellow"); $(this).css("color", "black");
            	}
            	if($(this).html() < 0  && $(this).html() != '' ) {
              		$(this).css("background-color", "red"); $(this).css("color", "black");
        		}
	    	});
			pasar+=1;col+=9;
	    	}
	    	cliente1='C1';cliente2='C2';
	    	nombre=''; 	if($("#nombre").val()!=0){ nombre='- Cliente: '+$("#nombre").val();cliente1=$("#nombre").val();}
	    	if($("#nombre2").val()!=0){ nombre=nombre+' vs '+$("#nombre2").val(); cliente2=$("#nombre2").val();}
	    	$('#tabla_vtact tbody tr').map(function(){
	    		if($(this).data('kgs')!=''){kgstot=$(this).data('kgs');} else {kgstot='';} 
	    		if($(this).data('t1')!=''){ t1=$(this).data('t1');t1vt=$(this).data('t1vt');t1vi=$(this).data('t1vi');t1vp=$(this).data('t1vp');
	    									t1vt1=$(this).data('t1vt1');t1vi1=$(this).data('t1vi1');t1vp1=$(this).data('t1vp1');
	    									t1vt2=$(this).data('t1vt2');t1vi2=$(this).data('t1vi2');t1vp2=$(this).data('t1vp2');
	    									t1gp=$(this).data('t1gp');
	    								   }else{t1='';t1vt='';t1vi='';t1vp='';t1vt1='';t1vi1='';t1vp1='';t1vt2='';t1vi2='';t1vt2='';t1gp='';}	
	    		if($(this).data('t2')!=''){ t2=$(this).data('t2');t2vt=$(this).data('t2vt');t2vi=$(this).data('t2vi');t2vp=$(this).data('t2vp');
	    									t2vt1=$(this).data('t2vt1');t2vi1=$(this).data('t2vi1');t2vp1=$(this).data('t2vp1');
	    									t2vt2=$(this).data('t2vt2');t2vi2=$(this).data('t2vi2');t2vp2=$(this).data('t2vp2');
	    									t2gp=$(this).data('t2gp');
	    								   }else{t2='';t2vt='';t2vi='';t2vp='';t2vt1='';t2vi1='';t2vp1='';t2vt2='';t2vi2='';t2vt2='';t2gp='';}							   
	 			if($(this).data('t3')!=''){ t3=$(this).data('t3');t3vt=$(this).data('t3vt');t3vi=$(this).data('t3vi');t3vp=$(this).data('t3vp');
	    									t3vt1=$(this).data('t3vt1');t3vi1=$(this).data('t3vi1');t3vp1=$(this).data('t3vp1');
	    									t3vt2=$(this).data('t3vt2');t3vi2=$(this).data('t3vi2');t3vp2=$(this).data('t3vp2');
	    									t3gp=$(this).data('t3gp');
	    								   }else{t3='';t32vt='';t3vi='';t3vp='';t3vt1='';t3vi1='';t3vp1='';t3vt2='';t3vi2='';t3vt2='';t3gp='';}
	    		if($(this).data('t4')!=''){ t4=$(this).data('t4');t4vt=$(this).data('t4vt');t4vi=$(this).data('t4vi');t4vp=$(this).data('t4vp');
	    									t4vt1=$(this).data('t4vt1');t4vi1=$(this).data('t4vi1');t4vp1=$(this).data('t4vp1');
	    									t4vt2=$(this).data('t4vt2');t4vi2=$(this).data('t4vi2');t4vp2=$(this).data('t4vp2');
	    									t4gp=$(this).data('t4gp');
	    								   }else{t4='';t42vt='';t4vi='';t4vp='';t4vt1='';t4vi1='';t4vp1='';t4vt2='';t4vi2='';t4vt2='';t4gp='';}
	    		if($(this).data('t5')!=''){ t5=$(this).data('t5');t5vt=$(this).data('t5vt');t5vi=$(this).data('t5vi');t5vp=$(this).data('t5vp');
	    									t5vt1=$(this).data('t5vt1');t5vi1=$(this).data('t5vi1');t5vp1=$(this).data('t5vp1');
	    									t5vt2=$(this).data('t5vt2');t5vi2=$(this).data('t5vi2');t5vp2=$(this).data('t5vp2');
	    									t5gp=$(this).data('t5gp');
	    								   }else{t5='';t52vt='';t5vi='';t5vp='';t5vt1='';t5vi1='';t5vp1='';t5vt2='';t5vi2='';t5vt2='';t5gp='';}
	    		if($(this).data('t6')!=''){ t6=$(this).data('t6');t6vt=$(this).data('t6vt');t6vi=$(this).data('t6vi');t6vp=$(this).data('t6vp');
	    									t6vt1=$(this).data('t6vt1');t6vi1=$(this).data('t6vi1');t6vp1=$(this).data('t6vp1');
	    									t6vt2=$(this).data('t6vt2');t6vi2=$(this).data('t6vi2');t6vp2=$(this).data('t6vp2');
	    									t6gp=$(this).data('t6gp');
	    								   }else{t6='';t62vt='';t6vi='';t6vp='';t6vt1='';t6vi1='';t6vp1='';t6vt2='';t6vi2='';t6vt2='';t6gp='';}
	    	});	
	    	
			Highcharts.chart('vtacomgrat', {
   				 chart: { type: 'column' },
    			 title: { text: 'Comparativo de Ventas Totales de Camarón: '+kgstot+' kgs' ,  align: 'left'},
    			 subtitle: { text: 'Las 6 Tallas más vendidas en el Mercado '+nombre,   align: 'left'},
    			 legend: {
               				itemStyle: { fontWeight: 'bold', fontSize: '9px'}
                   			//color: 'white',
                 },
    			 xAxis: {
    			 		  // style: {fontSize: '8px',fontFamily: 'Verdana, sans-serif'},	
    			 	       categories: [t1+'<br>'+t1vt+'<br>'+t1vi+'<br>'+t1vp+'<br>'+t1gp,t2+'<br>'+t2vt+'<br>'+t2vi+'<br>'+t2vp+'<br>'+t2gp,
    			 	       				t3+'<br>'+t3vt+'<br>'+t3vi+'<br>'+t3vp+'<br>'+t3gp,t4+'<br>'+t4vt+'<br>'+t4vi+'<br>'+t4vp+'<br>'+t4gp,
    			 	       				t5+'<br>'+t5vt+'<br>'+t5vi+'<br>'+t5vp+'<br>'+t5gp,t6+'<br>'+t6vt+'<br>'+t6vi+'<br>'+t6vp+'<br>'+t6gp
    			 	       ],
        		 		   crosshair: true   
        		 },
    			 /*yAxis: {
        					min: 0,
        					title: {
            					text: 'Camarón (kgs)'
        					}
    			},*/
    			yAxis: [{ // Primary yAxis
        					gridLineWidth: 0,
        					title: {text: 'Camarón', style: {color: Highcharts.getOptions().colors[0]} },
        					labels: {format: '{value} kgs', style: {color: Highcharts.getOptions().colors[0]} }
        					
			    		}, { // Secondary yAxis
        					gridLineWidth: 0,
        					title: {text: 'M.N.', style: {color: Highcharts.getOptions().colors[1]} },
        					labels: {format: '${value} mn', style: {color: Highcharts.getOptions().colors[1]} },
        					opposite: true
    				//	}, { // Tertiary yAxis
    				//		labels: {format: '${value} usd', style: {color: Highcharts.getOptions().colors[2] }	},
        			//		title: {text: 'U.S.D.', style: {color: Highcharts.getOptions().colors[2]}  },
        			//		opposite: true
        					
    			}],
    			
    			tooltip: {
        					headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            							'<td style="padding:0"><b>{point.y:.2f} kgs</b></td></tr>',
        					footerFormat: '</table>',
        					shared: true,
        					useHTML: true
    			},
    			plotOptions: {
        						column: {
            								pointPadding: 0.2,
            								borderWidth: 0
        						}
    			},
    			series: [{
        						name: cliente1,
        						dataLabels: { enabled: true,
        							//rotation: -90,
        						color: 'blue',
            						align: 'center',
            						format: '{point.y:.2f}', // one decimal
            						y: 10, // 10 pixels down from the top
        						},
        						data: [t1vt1, t2vt1, t3vt1, t4vt1, t5vt1, t6vt1]
						  },{
        						name: 'M.N. ',
        						type: 'spline',
        						yAxis: 1,
        						color:  'blue',
        						dataLabels: { enabled: true,
        									//rotation: -90,
        									//color: '#FFFFFF',
            								align: 'center',
            								format: '{point.y:.2f}', // one decimal
            								y: 5, // 10 pixels down from the top
        						},
        						data: [t1vp1, t2vp1, t3vp1, t4vp1, t5vp1, t6vp1],
        						marker: { enabled: false },
        						dashStyle: 'shortdot',
        						tooltip: { valueSuffix: ' mn'  }
    					},{
        						name: cliente2,
        						dataLabels: { enabled: true,
        							//rotation: -90,
        							align: 'center',
            						format: '{point.y:.2f}', // one decimal
            						y: 10, // 10 pixels down from the top
        						},
        						data: [t1vt2, t2vt2, t3vt2, t4vt2, t5vt2, t6vt2]
						  },{
        						name: 'M.N. ',
        						type: 'spline',
        						yAxis: 1,
        						color: 'black',
        						dataLabels: { enabled: true,
        									//rotation: -90,
        									//color: '#FFFFFF',
            								align: 'center',
            								format: '{point.y:.2f}', // one decimal
            								y: 5, // 10 pixels down from the top
        						},
        						data: [t1vp2, t2vp2, t3vp2, t4vp2, t5vp2, t6vp2],
        						marker: { enabled: false },
        						dashStyle: 'shortdot',
        						tooltip: { valueSuffix: ' mn'  }
    					}
    					
    				    ]
			});	
			
	   },   
	});
	
	$("#tabla_vtamr").ajaxSorter({
		url:'<?php echo base_url()?>index.php/analisisvta/tablavtamr',  
		filters:['cicmr','almacenes','clientes','Tallas1'],
		//active:['color',1],
        sort:false,
        onSuccess:function(){
        	$('#tabla_vtamr tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');});
        	$('#tabla_vtamr tbody tr td:nth-child(2)').map(function(){$(this).css('font-weight','bold');});
        	$('#tabla_vtamr tbody tr td:nth-child(6)').map(function(){$(this).css('font-weight','bold');});
        	$('#tabla_vtamr tbody tr td:nth-child(8)').map(function(){$(this).css('font-weight','bold');});
        	$('#tabla_vtamr tbody tr td:nth-child(9)').map(function(){$(this).css('font-weight','bold');});
        	$('#tabla_vtamr tbody tr td:nth-child(10)').map(function(){ 
	    		$(this).css("font-size", "1px");
	    		if($(this).html() >= 3000000 && $(this).html() != '' ) {
                	$(this).css("background-color", "red"); $(this).css("color", "red");
            	}
            	if($(this).html() >= 1000000 && $(this).html() < 3000000 && $(this).html() != '' ) {
                	$(this).css("background-color", "orange"); $(this).css("color", "orange");
            	}
            	if($(this).html() < 1000000  && $(this).html() != '' ) {
              		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
        		}
	    	});
	    	$('#tabla_vtamr tbody tr td:nth-child(11)').map(function(){$(this).css('font-weight','bold');});
	    	$('#tabla_vtamr tbody tr td:nth-child(12)').map(function(){$(this).css('font-weight','bold');
	    		if($(this).html() <= -10.00  && $(this).html() != '' ) {
             				$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        					}else if($(this).html() <= 9.00 && $(this).html() >= -9.99 && $(this).html() != '' ) {
              					$(this).css("background-color", "orange");$(this).css("color", "white");
        						}else if($(this).html() != '' ){
        							$(this).css("background-color", "red");$(this).css("color", "white");
        						}
        					/*
        		if($(this).html() >= -10.00 && $(this).html() <= 10.00 && $(this).html() != '' ) {
             				$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        					}else if($(this).html() >= 10.01 ||  $(this).html() <= -10.01 && $(this).html() != '' ) {
              					$(this).css("background-color", "red");$(this).css("color", "white");
        						}else if($(this).html() != '' ){
        							$(this).css("background-color", "orange");$(this).css("color", "white");
        						}	*/			
	    	});
	   },   
	});
	$("#tabla_vtamrg").ajaxSorter({
		url:'<?php echo base_url()?>index.php/analisisvta/tablavtamrg',  
		filters:['cicmr','almacenes','clientes','Tallas1'],
		//active:['color',1],
        sort:false,
        onSuccess:function(){
       		nombre=''; 	if($("#nombre").val()!=0) nombre='Cliente: '+$("#nombre").val();
	    	
	    	Highcharts.chart('vtamesrgra', {
   				 data: {table: 'mytablaVtaMRG'},
    			 chart: {type: 'spline'},
    			 title: {text: 'Comportamiento General Acumulado de Ventas, Pagos y Saldos',align: 'left'},
    			 subtitle: { text: nombre,   align: 'left'},
    			 xAxis: {
    			 		labels: {style: {fontSize: '11px',fontWeight: 'bold',color: 'red',}}
    			 },
    			 yAxis: {title: {text: 'Importe '},min: 0,labels: {format: '$'  }
    			 },
    			tooltip: {
        				formatter: function () {
            									return '<b>' + this.series.name + '</b><br/>' +
                								this.point.y + ' ' + this.point.name.toLowerCase();
        										}
    				},
    			series: [{		//type: 'areaspline',
        						color:  'blue',
        						dataLabels: { enabled: false,
        									align: 'center',
            								format: '{point.y:.2f}', // one decimal
            								y: 5, // 10 pixels down from the top
        						},
        				  },{
        						//type: 'areaspline',
        						color:  '#0F0',
        						dataLabels: { enabled: false,
        									align: 'center',
            								format: '{point.y:.2f}', // one decimal
            								y: 5, // 10 pixels down from the top
        						},
        				 },{
        						type: 'column',
        						color:  'red',
        						dataLabels: { enabled: true,
        									align: 'center',
            								format: '{point.y:.2f}', // one decimal
            								y: 5, // 10 pixels down from the top
        						},
        						zones: [{value: 1000000, color: '#0F0'}, {value: 3000000,color: 'orange'}, {color: 'red'}],
						  }
    				    ],		
			});
	    
	   },   
	});
	
	$("#tabla_diastal").ajaxSorter({
		url:'<?php echo base_url()?>index.php/analisisvta/tablainv',  		
		filters:['cicdt','almacenes','granjas','Tallas'],
	
		/*filters:['cicdt','almacenes','clientes','Tallas1'],*/
		sort:false,
		onRowClick:function(){
				
    	},
		onSuccess:function(){  
			$('#tabla_diastal tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
			$('#tabla_diastal tbody tr td:nth-child(3)').map(function(){ $(this).css('font-weight','bold');});
			$('#tabla_diastal tbody tr td:nth-child(6)').map(function(){ $(this).css('font-weight','bold');});
			$('#tabla_diastal tbody tr td:nth-child(10)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','center');});
			$('#tabla_diastal tbody tr').map(function(){
				if($(this).data('noma1')=='Total:') {	    				    			
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('font-size','12px');
	    		}
	    		
			});
			$('#tabla_diastal tbody tr td:nth-child(11)').map(function(){ 
	    		$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		if($(this).html() >= 90 && $(this).html() != '' ) {
                	$(this).css("background-color", "red"); $(this).css("color", "black");
            	}
            	if($(this).html() >= 61 && $(this).html() <= 89 && $(this).html() != '' ) {
                	$(this).css("background-color", "orange"); $(this).css("color", "black");
            	}
            	if($(this).html() <= 60  && $(this).html() != '' ) {
              		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        		}
	    	});
    	}, 
    });
    
    $("#tabla_diastalg").ajaxSorter({
		url:'<?php echo base_url()?>index.php/analisisvta/tablainvg',  		
		filters:['cicdt','almacenes','granjas','Tallas'],
		sort:false,
		onRowClick:function(){
				
    	},
		onSuccess:function(){  
			if($("#granjas").val()==2) nomg='Kino'; else nomg='Ahome';
			if($("#granjas").val()!=0) granja=' - GRANJA: '+nomg; else  granja=' - GRANJA: Ambas';
			if($("#congeladora").val()!=0) conge='ALMACEN: '+$("#congeladora").val(); else  conge='ALMACEN: Todos';
			Highcharts.chart('diastalgra', {
   				 data: {table: 'mytablaDiasTalg'},
    			 chart: {type: 'spline'},
    			 title: {text: 'Existencia en Almacenes con sus Días Transcurridos a la última venta',align: 'left'},
    			 subtitle: { text: conge+granja,   align: 'left'},
    			 xAxis: {
    			 		labels: {style: {fontSize: '10px',color: 'red',}}
    			 },
    			 yAxis: { title: {text: 'Dias '}, min: 0,
        				//labels: {format: 'D'  }		
    					},
    			tooltip: {
        				formatter: function () {
            									return '<b>' + this.series.name + '</b><br/>' +
                								this.point.y + ' ' + this.point.name.toLowerCase();
        										}
    				},
    			series: [{		//type: 'areaspline',
        						color:  'blue',
        						visible: false,
        						dataLabels: { enabled: true,
        									//rotation: -90,
        									//color: '#FFFFFF',
            								align: 'center',
            								format: '{point.y:.2f}', // one decimal
            								y: 5, // 10 pixels down from the top
        						},
        				  },{
        						type: 'column',
        						color:  'red',
        						dataLabels: { enabled: true,
        									align: 'center',
            								y: 5, // 10 pixels down from the top
        						},
        						zones: [{ value: 60, color: '#0F0'}, { value: 90, color: 'orange'}, {color: 'red'}],
						  }],	
			});
    	}, 
    });
    
    $("#tabla_final").ajaxSorter({
		url:'<?php echo base_url()?>index.php/analisisvta/tablafin',  		
		filters:['cicdt','almacenes','granjas','Tallas'],
	
		/*filters:['cicdt','almacenes','clientes','Tallas1'],*/
		sort:false,
		onRowClick:function(){
				
    	},
		onSuccess:function(){  
			$('#tabla_final tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
			$('#tabla_final tbody tr td:nth-child(3)').map(function(){ $(this).css('font-weight','bold');$(this).css("color", "navy");$(this).css('font-size','13px');});
			$('#tabla_final tbody tr td:nth-child(4)').map(function(){ $(this).css('font-weight','bold');});
			$('#tabla_final tbody tr td:nth-child(5)').map(function(){ $(this).css('font-weight','bold');});
			$('#tabla_final tbody tr td:nth-child(6)').map(function(){ $(this).css('font-weight','bold');$(this).css("color", "blue");$(this).css('font-size','13px');});
			$('#tabla_final tbody tr td:nth-child(9)').map(function(){ $(this).css('font-weight','bold');});
			$('#tabla_final tbody tr td:nth-child(12)').map(function(){ $(this).css('font-weight','bold');$(this).css("color", "red");});
			$('#tabla_final tbody tr td:nth-child(13)').map(function(){ $(this).css('font-weight','bold');$(this).css("color", "red");});
			$('#tabla_final tbody tr td:nth-child(14)').map(function(){ $(this).css('font-weight','bold');$(this).css("color", "red");});
			$('#tabla_final tbody tr td:nth-child(15)').map(function(){ $(this).css('font-weight','bold');$(this).css("color", "red");});
			$('#tabla_final tbody tr td:nth-child(16)').map(function(){ $(this).css('font-weight','bold');$(this).css("color", "red");$(this).css('font-size','13px');});
			$('#tabla_final tbody tr td:nth-child(18)').map(function(){ $(this).css('font-weight','bold');$(this).css('font-size','13px');});
			//$('#tabla_final tbody tr td:nth-child(18)').map(function(){ $(this).css('font-weight','bold');});
			$('#tabla_final tbody tr').map(function(){
				if($(this).data('nomt')=='Total:') {	    				    			
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('font-size','11px');
	    			$(this).css("background-color", "lightblue");
	    		}
	    		
			});
			
			$('#tabla_final tbody tr').map(function(){
				if($(this).data('nomt')=='') {	    				    			
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('font-size','12px');
	    		}
	    		
			});
			$('#tabla_final tbody tr').map(function(){
				if($(this).data('nomt')=='Costo/Kg') {	    				    			
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('font-size','12px');
	    		}
	    		
			});
			$('#tabla_final tbody tr td:nth-child(10)').map(function(){ 
	    		$(this).css('font-weight','bold');
	    		if($(this).html() >= 50 && $(this).html() != '' ) {
                	$(this).css("background-color", "red"); $(this).css("color", "black");
            	}
            	if($(this).html() > 15 && $(this).html() <= 49.99 && $(this).html() != '' ) {
                	$(this).css("background-color", "orange"); $(this).css("color", "black");
            	}
            	if($(this).html() <= 15  && $(this).html() != '' ) {
              		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        		}
	    	});
	    	$('#tabla_final tbody tr td:nth-child(17)').map(function(){ 
	    		if($(this).html() == 1 && $(this).html() != '' ) {
                	$(this).css("background-color", "red"); $(this).css("color", "red");
            	}
            	if($(this).html() == 2 && $(this).html() != '' ) {
                	$(this).css("background-color", "orange"); $(this).css("color", "orange");
            	}
            	if($(this).html() == 3  && $(this).html() != '' ) {
              		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
        		}
	    	});
    	}, 
    });
    
});
</script>