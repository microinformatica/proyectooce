/*/////////////////////////////AJAXSORTER 1.0////////////////////////////////// 
Esta libreria permite la creacion de listados dinamicos con:

    -   Opcion de ordenar los datos 
    -   Mostrar una cantidad seleccionada desde un select
    -   Paginacion ordenada y desordenada 
    -   Actualizar la paginacion
    -   Filtrar informacion por campo 


    NOTAS IMPORTANTES:


    -   Si se desea trabajar con los datos de la tabla mediante los eventos como
    click, es necesario colocar dicho trabajo dentro de una funcion llamada 
    AjaxSorterRefresh la cual debera de ser declarada manualmente.

    -   En caso de necesitar otros parametros, estos deben de enviarse por el 
    método GET en texto plano o encriptado con MD5 (Para aquellos que necesiten 
    enviar id's para una busqueda especifica u otra condicionante).

    -   AJAXSORTER 1.0 depende de jquery

    POR: ING.ROBERTO ANTONIO RAMOS JALOMA                                   */

function initAjaxSorter(div_ajax_sorter,url_action,extra_filters){ 
	 //CREACION DE IDS
	var url = url_action;
    var div_table  = div_ajax_sorter + " .ajaxSorter";
    var id_filter = div_ajax_sorter + " .filter";
    var id_pager = div_ajax_sorter + " .ajaxpager"	
    //SI EXISTEN FILTROS EXTRAS, (SELECT OPTION)
	if(extra_filters.length){
		$.each(extra_filters, function() {
			url+="/"+$('#'+this).val();		
		});
		$.each(extra_filters, function(){                        
			//SE POSICIONAN LOS VALORES DENTRO DEL TR PARA SU TRABAJO			
			$('#'+this).change(function (){
				url = url_action;
				//SE CARGAN LOS VALORES CORRESPONDIENTES
				$.each(extra_filters, function() {
					url+="/"+$('#'+this).val();		
				});
				paginate(0,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
			});
		});
	}
	
   
    //CARGA AUTOMATICA DE LA PRIMERA PAGINA    
    paginate(0,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
    
    
    //PAGINADO UTILIZANDO PRIMER,ANTERIOR,SIGUIENTE, ULTIMO
    $(id_pager + ' form .first').click(function(){//Se pagina la primera                           
        paginate(0,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
    });     
    $(id_pager + ' form .prev').click(function(){//Se pagina la anterior
        paginate(1,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
    }); 
    $(id_pager + ' form .next').click(function(){//Se pagina la siguiente                      
        paginate(2,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
    }); 
     $(id_pager + ' form .last').click(function(){//Se pagina la ultima                     
        paginate(3,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
    });
    
    
    //CANTIDAD DE REGISTROS POR PAGINA (CHANGE)
    $(id_pager + ' form select.pagesize').change(function(){        
         paginate(0,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
    });       
    
    
    //ORDENAMIENTO POR COLUMNA (CLICK)
    $(div_table + ' thead th').click(function (){        
        if($(this).hasClass('asc')){//Ordenacion DESC
            setAllNone(div_table); 
            $(this).removeClass('none');
            $(this).addClass('desc'); 
            //Se pagina
            paginate(0,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
        }else{//Ordenacion ASC
            setAllNone(div_table);  
            $(this).removeClass('none');
            $(this).addClass('asc');
            //Se pagina
            paginate(0,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
        }               
    }); 
    
    
    //COMBO DE CAMPOS (AUTO)
    var content ="";
    $(id_filter+' select.filter_type').html('');        
    $(div_table + ' thead th').each(function (){        
        content+="<option find = '' value="+$(this).data('order')+">"+$(this).text()+"</option>"
    });              
    $(id_filter+' select.filter_type').append(content);
    
    
    //CARGA LO QUE SE HABIA BUSCADO ANTERIORMENTE DEL CAMPO SELECCIONADO
    $(id_filter+' select.filter_type').change(function(){
        var value_option = $(id_filter+" select.filter_type option:selected").attr('find');
        $(id_filter+" input.filter_text").val(value_option);
    });

        
    //BUSQUEDA (ENTER=BUSCAR)
    $(id_filter+" input.filter_text").keyup(function (e) {
        if (e.keyCode == 13) {
          $(this).trigger("enter");
        }
    }).bind("enter", function(){ 
        //SI EL HISTORIAL NO ESTA ACTIVADO
        if($(id_filter+" input.keep_filter").is(':checked')==false){            
            $(id_filter + ' select.filter_type option').each(function (){        
                $(this).attr('find', '');                
            });
        }        
        $(id_filter+ " select.filter_type option:selected").attr('find', $(id_filter+" input.filter_text").val());
        
        
        //AGREGA A LA TABLA DE FILTROS LOS FILTROS PREVIOS ACTIVOS 
        var fil_pre='';
        var head='';
        $(id_filter+' ul.order_list').html('');
        head+="<li class='list_header' relation ='all-items' title='DOBLE CLICK PARA ELIMINAR LOS FILTROS'>Filtros activos</li>";            
        $(id_filter + ' select.filter_type option').each(function (){                 
            if($(this).attr('find')!='')
                fil_pre+="<li class='show_li' relation='"+$(this).val()+"' title='DOBLE CLICK PARA ELIMINAR FILTRO'>"+$(this).text()+":"+$(this).attr('find')+"</li>";            
        }); 
        if(fil_pre!='')
            fil_pre = head + fil_pre;
        $(id_filter+' ul.order_list').append(fil_pre);
        
        
        //OPCIONES PARA QUE SE PUEDAN BORRAR CON DOBLE CLICK
        $(id_filter+' ul.order_list li').dblclick(function(){
            
            
            //ENCUENTRA EL SELECCIONADO Y LO CAMBIA A NULO
            var relation = this;
            $(id_filter+ ' select.filter_type option').each(function(){                
                if($(relation).attr('relation')=='all-items')//ELIMINA TODOS LOS FILTROS
                     $(this).attr('find','');
                else if($(this).val()==$(relation).attr('relation'))//ELIMINA UNO
                    $(this).attr('find','');
            });
            
                        
            if($(relation).attr('relation')=='all-items'){////REMUEVE TODOS LOS FILTROS
                $(id_filter+' ul.order_list').html('');
            }else{//REMUEVE EL FILTRO EN LA TABLA DE FILTROS
                $(relation).remove();                 
                if($(id_filter+' ul.order_list li').length==1)
                    $(id_filter+' ul.order_list').html('');
            }
            $(id_filter+" input.filter_text").val('');
            
            
            //PAGINA NUEVAMENTE
            paginate(0,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
        });
        paginate(0,url,id_pager,div_table,id_filter,verifyOrdered(div_table));        
    });
        
    $(id_filter+" input.keep_filter").change(function(){
        if($(this).is(':checked')==false){
            
            //BORRA TODAS LAS BUSQUEDAS GUARDADAS
            $(id_filter + ' select.filter_type option').each(function (){        
                $(this).attr('find', '');                
            });             
            
            
            //BORRA LA TABLA DE FILTROS PREVIOS            
            $(id_filter+' ul.order_list').html(''); 
            paginate(0,url,id_pager,div_table,id_filter,verifyOrdered(div_table)); 
        }
    });       
    
    
    //ELIMINA FILTROS
    $(id_filter+" input.keep_filter").change(function(){
        if($(this).is(':checked')==false){
            $(id_filter + ' select.filter_type option').each(function (){        
                $(this).attr('find', '');
            }); 
        }        
    });
    
    
    //CAMBIO DE HOJA MANUAL (ENTER=FIN)
    $(id_pager+" input.pagedisplay").keyup(function (e) {
        if (e.keyCode == 13){
          $(this).trigger("enter");
        }
    }).bind("enter", function(){        
        paginate(4,url,id_pager,div_table,id_filter,verifyOrdered(div_table));
    });
       
    setAllNone(div_table);     
}      
function verifyOrdered(div_table){

    //OBTIENE EL ORDENAMIENTO ACTUAL
    var isIsOrdered ="";
    $(div_table + ' thead th').each(function(){
        if($(this).hasClass('asc')){
            isIsOrdered =$(this).data('order');
        }
        if($(this).hasClass('desc')){
            isIsOrdered =$(this).data('order')+' desc';
        }
    });
    return isIsOrdered;
}
function paginate(side,url,id_pager,ajxdiv,id_filter,order){    
    
    var max_pp =$(id_pager+' form select').val();    
    var id_display = id_pager+' form input.pagedisplay';
    var id_displayMax =id_pager+' form input.pagedisplayMax';
    var pagina = parseInt($(id_display).val(), 10);                          
                   
    
    //SE PAGINA EN BASE AL LINK SELECCIONADO
    var n_rows = parseInt($(id_displayMax).val(), 10);
    if(!isNaN(pagina)){//Si es una cantidad valida lo pagina        
        pagina--;//Ajuste                   
        if(side == 0){//Pagina 0
            pagina = 0;
        }else if(side == 1){//Anterior
            if(pagina >=1)
            pagina = pagina - 1;
        }else if(side == 2){//Siguiente
            if(pagina < (n_rows-1))
            pagina = pagina + 1;                                   
        }else if(side==3){//Ultima
            pagina = n_rows - 1;
        }else{
            if(pagina>n_rows){
                pagina = 0;
            }
        }            
    }else{
        pagina = 0;       
    }   
    
    
    //CARGA DE CAMPOS VISIBLES
    var fields = [];
    $(ajxdiv + " thead th").each(function(){        
        fields.push($(this).data('order'));        
    });  
                    
    
    //CARGA DE CONDICIONES------FALTAAAA    
    var condition_values =[];        
    
    $(id_filter + " select.filter_type option").each(function(){          
        condition_values.push((($(this).attr('find'))?$(this).attr('find'):''));
    }); 
    if(condition_values.length==0){
        for(i_t=0;i_t<fields.length;i_t++)
            condition_values.push("");
    }
    
    //REVISA LOS FILTROS QUE ESTAS MANDANDO!    
    //console.debug(condition_values);
    //console.debug(fields);    
    
    //SE CARGA TODO CON AJAX       
    $.ajax({
        type: "POST",
        url: url,
        data: "pa="+ pagina+"&max_pp="+max_pp+"&order="+order+"&field="+fields+"&value="+condition_values,
        success: function(msg)
        {
            var obj = eval(msg);
            var tr="";                   
            var rows = obj['rows'];
            //OBTENEMOS LA CANTIDAD DE ROWS      
            var nr = obj['num_rows'];           
            if((nr%max_pp)==0){        
                nr = (nr-(nr%max_pp))/max_pp;        
            }else{
                nr = ((nr-(nr%max_pp))/max_pp)+1;
            }
            $(id_displayMax).val(nr); 
            
            $(ajxdiv + " tbody").html(tr)   
                       
            
            //FALTA LA CARGA AUTOMATICA DE LOS VALORES EN GENERAL, QUE SEA ADAPTABLE A LA CONSULTA
            
            if(rows.length>0){
                for(i = 0; i < rows.length;i++){                
                    tr+="<tr";
                    $.each(rows[i], function(id,val) {                        
                        //SE POSICIONAN LOS VALORES DENTRO DEL TR PARA SU TRABAJO
                        tr+= ' '+id+'="'+val+'"';                        
                    });
                    tr+=">";
                     $.each(fields,function(){ 
						var field = this;
                        $.each(rows[i], function(id,val) {                             
                            //SE VALIDA PARA MOSTRAR LAS COLUMNAS SOLICITADAS                                                
                             if(id==field){
                                 tr+="<td id='"+id+"'>"+val +"</td>"; 
                             }
                         });   
                         
                     });                                                           
                    tr+="</tr>";                                         
                }
            }else{
                tr='<tr><td colspan="'+fields.length+'" align="center">Su busqueda no obtubo ningun resultado</td></tr>';
            }
            $(ajxdiv + " tbody").append(tr)   
            
            
            //LLAMAR FUNCION PARA QUE PUEDAN SER CLICKEADOS
            //AjaxSorterRefresh(ajxdiv);           
        }
    }); 
    
    pagina++;//AJUSTE       
    $(id_display).val(pagina);    
}    

function setAllNone(div_table){//CANCELA ORDENAMIENTOS
     $(div_table + ' thead th').removeClass('asc');
     $(div_table + ' thead th').removeClass('desc');
     $(div_table + ' thead th').addClass('none');
}   