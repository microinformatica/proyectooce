<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Parametros extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('parametros_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('parametros_model');
			//$data['result']=$this->parametros_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('parametros/lista',$data);
        }
		
		public function tablap($ano=0,$cic=0,$gra=0,$tip=0,$est=0,$mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
        	if($gra!=0){$filter['where']['numgrap =']=$gra;}
        	if($cic!=0){$cic='20'.$ano.'-'.$cic;$filter['where']['cicfq =']=$cic;}
        	if($est!=0){$filter['where']['idpisfq =']=$est;}
        	if($mes!=0){$filter['where']['month(fecfq) =']=$mes;}
			$data['rows'] = $this->parametros_model->parametros($filter,$ano,$tip);
        	echo '('.json_encode($data).')';                
    	}    	
    	public function tablap2($ano=0,$cic=0,$gra=0,$tip=0,$est=0,$mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
        	$ano-=1;
        	if($gra!=0){$filter['where']['numgrap =']=$gra;}
        	if($cic!=0){$cic='20'.$ano.'-'.$cic;$filter['where']['cicfq =']=$cic;}
        	if($est!=0){$filter['where']['idpisfq =']=$est;}
        	if($mes!=0){$filter['where']['month(fecfq) =']=$mes;}
			$data['rows'] = $this->parametros_model->parametros($filter,$ano,$tip);
        	echo '('.json_encode($data).')';                
    	}
    }
?>