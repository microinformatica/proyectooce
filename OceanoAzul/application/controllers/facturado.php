<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Facturado extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('facturado_model'); 
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
		
        function index() {
            $this->load->model('facturado_model');
			//$data['result']=$this->facturado_model->Entregado();
			$data['result']=$this->facturado_model->zonas();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('facturado/lista',$data);
        }
		function pdfrep() {
            $this->load->model('facturado_model');
			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['zona']=$this->input->post('zonasel');
			$data['ciclo']=$this->input->post('ciclosel2');
			$this->load->view('facturado/lista',$data);
			//$data['zona']=$this->input->post('zonasel');			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('facturado/listapdf', $data, true);  
			pdf ($html,'facturado/listapdf', true);
        	set_paper('letter');
        }
		function pdfrepdetalle() {
            $this->load->model('facturado_model');
			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['cliente']=$this->input->post('cliente');
			$data['ciclo']=$this->input->post('ciclosel1');
			$this->load->view('facturado/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('facturado/listapdfdetalle', $data, true);  
			pdf ($html,'facturado/listapdfdetalle', true);
        	set_paper('letter');
        }		
		public function tabla($ciclo='',$zona=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//$filter['where']['Zona']=0;
			if($ciclo>0) {
			$z=0;
			if($zona!='null' && $zona!='0'){
        		if($zona=='El%20Dorado') $zona='El Dorado';
				if($zona=='Sur%20Sinaloa') $zona='Sur Sinaloa';
				if($zona=='Obreg%C3%B3n') $zona='Obregón';
				if($zona=='Yucat%C3%A1n') $zona='Yucatán';
        		$filter['where']['Zona']=$zona;$z=1;
			}			
			$filter['num'] = $ciclo; 												
        	$data['rows'] = $this->facturado_model->getFacturado($filter,$z);
        	$data['num_rows'] = $this->facturado_model->getNumRows($filter);
			}else{
				$data['rows'] ="";$data['num_rows'] ="";
			}
        	echo '('.json_encode($data).')'; 
    	}
		public function tablazon($ciclo=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($ciclo>0) {  
			$filter['num'] = $ciclo; 												
        	$data['rows'] = $this->facturado_model->gralzona($filter);
        	$data['num_rows'] = $this->facturado_model->getNumRowsZ($filter);
			}else{
				$data['rows'] ="";$data['num_rows'] ="";
			}
        	echo '('.json_encode($data).')'; 
    	}
		public function tablaentregado($extra=0,$ciclo=''){
        	$filter = $this->ajaxsorter->filter($this->input);
			if($ciclo!='') $filter['limit']=$ciclo;
			$filter['where']['numero']=$extra;
			$filter['num'] = $ciclo; 
        	$data['rows'] = $this->facturado_model->getEntregado($filter);
        	$data['num_rows'] = $this->facturado_model->getNumRowsE($filter);
        	echo '('.json_encode($data).')';                
    	}	
		public function tablagranja($cliente=0,$seccion=''){
        	$filter = $this->ajaxsorter->filter($this->input);
			if($seccion!='null' && $seccion!='0'){ $filter['where']['seccion']=$seccion;}
			$filter['where']['cliente']=$cliente;
			$data['rows'] = $this->facturado_model->getSiembras($filter);
        	$data['num_rows'] = $this->facturado_model->getNumRowsSie($filter);
        	echo '('.json_encode($data).')';                
    	}	
		public function combo(){
			$act=$this->input->post('actual');
        	if($act>0) {	        
        	$filter['actual']=$this->input->post('actual');           
        	$data = $this->facturado_model->getElements($filter);        
        	echo '('.json_encode($data).')'; 
			}
    	}
		public function comboSecc(){
			$filter['actual']=$this->input->post('actual');           
        	$data = $this->facturado_model->verSeccion($filter);
			echo '('.json_encode($data).')';
		}
		
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	}
		
		/*function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('facturado_model');
		$id_post=$this->input->post('id'); 
		$obs=$this->input->post('obs');
		$sn=$this->input->post('sn');
		if($id_post!=''){
			$return=$this->facturado_model->actualizar($id_post,$obs,$sn); 			
			redirect('facturado');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->facturado_model->getCliente($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('facturado/actualizar',$datos);
		}*/
		function agregarg(){
		$this->load->helper('url');
		$this->load->model('facturado_model');		
		$cli=$this->input->post('cli');
		$sec=$this->input->post('sec');
		$fs=$this->input->post('fs');
		$ori=$this->input->post('ori');
		$cic=$this->input->post('cic');
		$est=$this->input->post('est');
		$sup=$this->input->post('sup');
		$pls=$this->input->post('pls');
		if($est!=''){	
			$this->facturado_model->agregarg($cli,$sec,$fs,$ori,$cic,$est,$sup,$pls);			
			redirect('facturado');
			
		}
		}
		function actualizarg($id=0){
			$this->load->helper('url');
			$this->load->model('facturado_model');
			$id_post=$this->input->post('id'); $cli=$this->input->post('cli'); $sec=$this->input->post('sec'); $fs=$this->input->post('fs');
			$ori=$this->input->post('ori'); $cic=$this->input->post('cic'); $est=$this->input->post('est'); $sup=$this->input->post('sup');
			$pls=$this->input->post('pls'); $fc=$this->input->post('fc'); $pp=$this->input->post('pp'); $at=$this->input->post('at');
			$camf=$this->input->post('camf'); $sob=$this->input->post('sob'); $biop=$this->input->post('biop'); $camp=$this->input->post('camp');
			$bioc=$this->input->post('bioc'); $camc=$this->input->post('camc');
			if($id_post!=''){
				$return=$this->facturado_model->actualizarg($id_post,$cli,$sec,$fs,$ori,$cic,$est,$sup,$pls,$fc,$pp,$at,$camf,$sob,$biop,$camp,$bioc,$camc); 			
				//redirect('facturado');
				echo json_encode($return);
				
			}
		}
		function borrarg($id=0){
		$this->load->helper('url');
		$this->load->model('facturado_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->facturado_model->borrarg($id_post); 			
			redirect('facturado');
		}
		}		
    }
    
?>