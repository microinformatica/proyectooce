<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     class Gralest extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
       	$this->load->model('gralest_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('gralest_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('gralest/lista',$data);
        }
		
		function pdfrepcosg( ) {
            $this->load->model('gralest_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('gralest/lista',$data);
			$data['tabla'] = $this->input->post('tablag');
			$html = $this->load->view('gralest/listapdfgral', $data, true);  
			//$dia=date("Y-m-d");
			pdf ($html,'Rep_Gral_'.date("d-m-Y"), true);
			         	
        }
		
		public function tablagral($gra=0,$ciclo=0,$secc=0){
			$filter = $this->ajaxsorter->filter($this->input);	
			
			$filter['where']['numgrab =']=$gra;
			//$filter['where']['ciccha =']=$ciclo;
			$filter['where']['cicb =']=$ciclo;
			if($secc>0)$filter['where']['secc =']=$secc; 
        	 $base=base_url(); 
			$data['rows'] = $this->gralest_model->gral($filter,$ciclo,$secc,$base);
        	echo '('.json_encode($data).')';                
    	}
		
		public function graldetest1($est=0){
			$filter = $this->ajaxsorter->filter($this->input);	
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('gralest/lista',$data);
			$cic=substr($est,0,6);
			$dia=substr($est,7,10);
			$tq=substr($est,18,2);
			$data['cic'] = $cic;$data['est'] = $tq;$data['dia'] = $dia;
			$html = $this->load->view('gralest/detalle', $data, true);  
			//$dia=date("Y-m-d");
			pdf ($html,'Est_'.$tq.'_Cic_'.$cic.'_'.$dia, true);             
    	}	
		public function graldetest($est=0){
			$filter = $this->ajaxsorter->filter($this->input);	
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('gralest/lista',$data);
			$cic=substr($est,0,6);
			$dia=substr($est,7,10);
			$sec=substr($est,18,2);
			$tq=substr($est,21,2);
			$data['cic'] = $cic;$data['est'] = $tq;$data['dia'] = $dia;$data['sec'] = $sec;
			$this->load->view('gralest/listadet',$data);         
    	}
		public function tablafca($cic=0,$est=0,$dia=0,$sec=0){
			$filter = $this->ajaxsorter->filter($this->input);
			//$data['usuario']=$this->usuario;
			//$data['perfil']=$this->perfil; 	
			//$this->load->view('gralest/listadet',$data);
			$filter['where']['cicg =']=$cic;
			$filter['where']['pisg =']=$est;
			//$filter['where']['secc =']=$dia;
			//$data['cic'] = $cic;$data['est'] = $est;$data['dia'] = $dia;
        	$data['rows'] = $this->gralest_model->fca($filter,$cic,$est,$dia,$sec);
			echo '('.json_encode($data).')';                
    	}
		public function tablatal($cic=0,$est=0,$dia=0,$sec=0){
			$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['cicg =']=$cic;
			$filter['where']['pisg =']=$est;
			$data['rows'] = $this->gralest_model->tal($filter,$cic,$est,$dia,$sec);
			echo '('.json_encode($data).')';                
    	}	
		public function tablainc($cic=0,$est=0,$dia=0,$sec=0){
			$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['cicg =']=$cic;
			$filter['where']['pisg =']=$est;
			$data['rows'] = $this->gralest_model->inc($filter,$cic,$est,$dia,$sec);
			echo '('.json_encode($data).')';                
    	}
		public function tablaali($cic=0,$est=0,$dia=0,$sec=0){
			$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['cicg =']=$cic;
			$filter['where']['pisg =']=$est;
			$data['rows'] = $this->gralest_model->ali($filter,$cic,$est,$dia,$sec);
			echo '('.json_encode($data).')';                
    	}
    }
    
?>