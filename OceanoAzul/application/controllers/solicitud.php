<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Solicitud extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('solicitud_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('solicitud_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('solicitud/lista',$data);
        }
		function pdfrep( ) {
            $this->load->model('solicitud_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('solicitud/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$data['ini'] = $this->input->post('ini');
			$data['fin'] = $this->input->post('fin');
			$html = $this->load->view('solicitud/listapdf', $data, true);  
			pdf ($html,'solicitud/listapdf', true);
        	set_paper('letter');
        }
		function pdfrepsoli() {
            $this->load->model('solicitud_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['fecha'] = $this->input->post('fecsoli');
			$data['nomsa'] = $this->input->post('nom1');
			$data['nomap'] = $this->input->post('nom2');
			/*$data['firma'] = $this->input->post('firmarb');
			$data['puesto'] = $this->input->post('puestorb');
			$data['empleado'] = $this->input->post('empleadob');
			$data['fecha'] = $this->input->post('fecexpb');
			
			$data['ciu'] = $this->input->post('ciub');
			$data['edo'] = $this->input->post('edob');
			$data['rfc'] = $this->input->post('rfcb');
			$data['ben'] = $this->input->post('bene');
			$data['par'] = $this->input->post('paren');*/
			$data['relacion'] = $this->input->post('relacion');
			$html = $this->load->view('solicitud/solicitud', $data, true);  
			pdf ($html,'solicitud/SolicitudMovilizacion', true);
        }
		public function tablasoli(){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->solicitud_model->getSolicitudes($filter);
        	$data['num_rows'] = $this->solicitud_model->getNumRowsSM($filter);
        	echo '('.json_encode($data).')'; 
    	}
		public function tablasolcsm($extra=''){
			//if($extra!='') $filter['where']['numsolmov =']=$extra;
			$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->solicitud_model->solicitudescsm($filter,$extra);
        	$data['num_rows'] = $this->solicitud_model->getNumRowsCSM($filter);
        	echo '('.json_encode($data).')'; 
    	}
		 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('solicitud_model');		
		$can=$this->input->post('can');
		$fso=$this->input->post('fso');
		$fpa=$this->input->post('fpa');
		$nsa=$this->input->post('nsa');
		$nap=$this->input->post('nap');
		$ins=$this->input->post('ins');
		$fol=$this->input->post('fol');
		$ref=$this->input->post('ref');
		if($fso!=''){	
			$this->solicitud_model->agregar($fso,$nsa,$nap,$can,$ins,$fol,$ref,$fpa);			
			redirect('solicitud');
		}
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('solicitud_model');
		$id_post=$this->input->post('id'); 
		$can=$this->input->post('can');
		$fso=$this->input->post('fso');
		$fpa=$this->input->post('fpa');
		$nsa=$this->input->post('nsa');
		$nap=$this->input->post('nap');
		$ins=$this->input->post('ins');
		$fol=$this->input->post('fol');
		$ref=$this->input->post('ref');
		if($id_post!=''){
			$return=$this->solicitud_model->actualizar($id_post,$fso,$nsa,$nap,$can,$ins,$fol,$ref,$fpa); 			
			redirect('solicitud');
		}
		}
		
		function quitar($id=0){
		$this->load->helper('url');
		$this->load->model('solicitud_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->solicitud_model->quitar($id_post); 			
			redirect('solicitud');
		}
		}
		function agrega(){
		$this->load->helper('url');
		$this->load->model('solicitud_model');		
		$nc=$this->input->post('id');
		$sol=$this->input->post('sol');
		if($nc!=''){	
			$this->solicitud_model->agrega($nc,$sol);			
			redirect('solicitud');
		}
		}		
    }
    
?>