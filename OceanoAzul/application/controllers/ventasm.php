<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Ventasm extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('ventasm_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('ventasm_model');
			//$data['result']=$this->ventasm_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ventasm/lista',$data);
        }
		
		public function tabla($cic=0,$extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['ciclo =']= '20'.$cic;
			if($extra>0){$filter['where']['estsal =']=$extra;} 
			
        	$data['rows'] = $this->ventasm_model->getVentas($filter);
        	
        	echo '('.json_encode($data).')';                
    	}
		public function tablag($cic=0,$gra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->ventasm_model->getVentasg($cic,$gra);
        	echo '('.json_encode($data).')';                
    	}
		public function tablasema($cic=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$cic='20'.$cic;
			$data['rows'] = $this->ventasm_model->getEstatus($cic);
        	echo '('.json_encode($data).')';                
    	}
		function pdfrepvta() {
            $this->load->model('ventasm_model');
			$dia=date('d-m-Y');
			$data['dia'] = date('d-m-Y');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ventasm/lista',$data);
			$data['tablavtas'] = $this->input->post('tablav');
			$html = $this->load->view('ventasm/listapdf', $data, true);  
			pdf ($html,'gral_ventas_'.$dia, true);   
        }
		function pdfrepvtasg() {
            $this->load->model('ventasm_model');
			$dia=date('d-m-Y');
			$data['dia'] = date('d-m-Y');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ventasm/lista',$data);
			$data['tablag'] = $this->input->post('tabla');
			$html = $this->load->view('ventasm/listapdfvtag', $data, true);  
			pdf ($html,'ventas_'.$dia, true);        	
        }
		function pdfrepsema() {
            $this->load->model('ventasm_model');
			$dia=date('d-m-Y');
			$data['dia'] = date('d-m-Y');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ventasm/lista',$data);
			$data['tablag'] = $this->input->post('tablasema');
			$html = $this->load->view('ventasm/listapdfsema', $data, true);  
			pdf ($html,'estatus_saldos_'.$dia, true);        	
        }
		public function combo(){        
        	$filter['id_unidad']=$this->input->post('id_unidad');           
        	$data = $this->base_model->getElements($filter);        
        	echo '('.json_encode($data).')';              
    	}
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	} 
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('ventasm_model');
		$id_post=$this->input->post('id'); //id
		$pre=$this->input->post('pre');
		if($pre=='') $pre=0;
		$usd=$this->input->post('usd');
		if($usd=='') $usd=0;
		$fac=$this->input->post('fac');
		$avi=$this->input->post('avi');
		$obs=$this->input->post('obs');
		if($id_post!=''){
			$return=$this->ventasm_model->actualizar($id_post,$pre,$fac,$avi,$usd,$obs); 
			redirect('ventasm');
		}		
		}
		function buscar(){
		$busqueda=$this->input->post('busqueda');
		$zon=$this->input->post('zon');
		$this->load->helper('url');
		$this->load->model('ventasm_model');
		$datos['result']=$this->ventasm_model->buscar_activos($busqueda,$zon);		
		$datos['busqueda']=$busqueda;
		$datos['zon']=$zon;		
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('ventasm/buscar',$datos);
}
		function borrara($id=0){
		$this->load->helper('url');
		$this->load->model('ventasm_model');
		$id_post=$this->input->post('id');
		if($id_post!=''){
			$return=$this->ventasm_model->borrara($id_post); 			
			redirect('programa');
		}
		}
		
    }
    
?>