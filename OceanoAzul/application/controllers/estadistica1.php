<?php 
    class Estadistica extends CI_Controller {
        public function __construct() {
        parent::__construct();	
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		//$this->load->library('libreria');
		$this->load->library('session'); 
		$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');   
	   }
        function index() {
            $this->load->model('estadistica_model');
			$data['result']=$this->estadistica_model->zonas();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('estadistica_v',$data);
        }
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('estadistica_model');
		$id_post=$this->input->post('id'); //id
		$rblugar=$this->input->post('rbLugar');
		$obs=$this->input->post('txtobs');
		if($id_post!=''){
			$return=$this->estadistica_model->actualizar($id_post,$rblugar,$obs); 			
			redirect('estadistica');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->estadistica_model->getClienteEst($id);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('estadisticas/actualizar',$datos);
		}
	}
?>