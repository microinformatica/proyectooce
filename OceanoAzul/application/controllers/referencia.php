<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Referencia extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('referencia_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('referencia_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;		
			$this->load->view('referencia/lista',$data);
        }
		//referencia diario
		public function tablareferencia($ciclo=0,$dia='',$ori='Todos'){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			$filter['where']['fecpd =']=$dia;
			$ciclo='prog_'.$ciclo;
			if($ori!='Todos'){ $filter['where']['tipopl =']=$ori;}
			$data['rows'] = $this->referencia_model->getreferenciadia($filter,$ciclo,$ori);
        	$data['num_rows'] = $this->referencia_model->getNumRowsPd($filter,$ciclo);
        	echo '('.json_encode($data).')'; 
    	}
		public function tablacli(){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			$data['rows'] = $this->referencia_model->getClientes($filter);
        	$data['num_rows'] = $this->referencia_model->getNumRowsC($filter);
        	echo '('.json_encode($data).')'; 
    	}
		
		function borrara($id=0){
		$this->load->helper('url');
		$this->load->model('referencia_model');
		$id_post=$this->input->post('id');
		$cic='prog_'.$this->input->post('cic'); 
		if($id_post!=''){
			$return=$this->referencia_model->borrara($id_post,$cic); 			
			redirect('referencia');
		}
		}
		
		function agregara(){
		$this->load->helper('url');
		$this->load->model('referencia_model');		
		$fec=$this->input->post('fec');
		$idcte=$this->input->post('idcte');
		$can=$this->input->post('can');
		$por=$this->input->post('por');
		$obs=$this->input->post('obs');
		$tip=$this->input->post('tip');
		$cic='prog_'.$this->input->post('cic');
		if($fec!=''){	
			$this->referencia_model->agregara($fec,$idcte,$can,$por,$obs,$tip,$cic);			
			redirect('referencia');
		}
		}
		function actualizara($id=0){
		$this->load->helper('url');
		$this->load->model('referencia_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$idcte=$this->input->post('idcte');
		$can=$this->input->post('can');
		$por=$this->input->post('por');
		$obs=$this->input->post('obs');
		$tip=$this->input->post('tip');
		$cic='prog_'.$this->input->post('cic');
		if($id_post!=''){
			$return=$this->referencia_model->actualizara($id_post,$fec,$idcte,$can,$por,$obs,$tip,$cic); 			
			redirect('referencia');
		}
		}
		
		function tablaprogmes($ciclo=0,$mes=0,$tip=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($ciclo>0) $filter['where']['year(fecpd) =']=$ciclo;
			if($mes>0) $filter['where']['month(fecpd) =']=$mes;
			if($tip!='') $filter['where']['tipopl =']=$tip;
			//$ciclo='prog_'.$ciclo;
			$data['rows'] = $this->referencia_model->getMesProg($filter,$ciclo,$tip,$mes);
        	//$data['num_rows'] = $this->referencia_model->getNumRowsTGral($filter);
        	
        	echo '('.json_encode($data).')'; 
    	}
		
		//Estados
		public function tablaestados($edo=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			if($edo!='0' && $edo!='null'){ $filter['where']['nomedo =']=$edo;}
			//$filter['where']['nomedo =']=$edo;
			$data['rows'] = $this->referencia_model->getEstados($filter);
        	$data['num_rowss'] = $this->referencia_model->getNumRowsE($filter);
        	echo '('.json_encode($data).')'; 
    	}
		public function tablaestadosbco($edob=0,$zonb=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			/* $filter['where']['refedo =']=$edob;
			 $filter['where']['refzon =']=$zonb;*/
			if($edob!=0 && $edob!='null'){ $filter['where']['refedo =']=$edob;}
			if($zonb!='0' && $zonb!='null'){ $filter['where']['refzon =']=$zonb;}
			$data['rows'] = $this->referencia_model->getEstadosBco($filter,$edob,$zonb);
        	$data['num_rowss'] = $this->referencia_model->getNumRowsB($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function pdfrepref( ) {
			$this->load->model('referencia_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$idcli = $this->input->post('numcli');
			$data['cli'] = $this->input->post('cli');
			$data['dom'] = $this->input->post('dom');
			$data['lecp'] = $this->input->post('lecp');
			$data['ref'] = $this->input->post('ref');
			$html = $this->load->view('referencia/CartaRef', $data, true);
			pdf ($html,'Cte_'.$idcli.'_Ref_'.$data['ref'], true);
							
      	}
		function pdflistaref( ) {
			$this->load->model('referencia_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablaref');
			$html = $this->load->view('referencia/Referencias', $data, true);
			pdf ($html,'Referencias', true);
							
      	}
		function pdflistarefedozon( ) {
			$this->load->model('referencia_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablaedo');
			$html = $this->load->view('referencia/Catalago', $data, true);
			pdf ($html,'CatalagoGral', true);
							
      	}
		function buscaredo($id=0){
		$this->load->helper('url');
		$this->load->model('referencia_model');
		$id=$this->input->post('id');
		if($id!=''){
			$row=$this->referencia_model->buscaredo($id);
			$size=sizeof($row);
			if($size>0){
				$nomedo=$row->nomedo;$num=$row->num;
			}else{
				$nomedo='';$num='';
			}
			echo json_encode(array('nomedo'=>$nomedo,'num'=>$num));
		}
		}
		function borraras($id=0){
		$this->load->helper('url');
		$this->load->model('referencia_model');
		$id_post=$this->input->post('id');
		$cic='progs_'.$this->input->post('cic'); 
		if($id_post!=''){
			$return=$this->referencia_model->borraras($id_post,$cic); 			
			redirect('referencia');
		}
		}
		
		function agregaredo(){
		$this->load->helper('url');
		$this->load->model('referencia_model');		
		$num=$this->input->post('num');
		$nomedo=$this->input->post('nomedo');
		if($num!=''){	
			$this->referencia_model->agregaraedo($num,$nomedo);			
			redirect('referencia');
		}
		}
		function actualizaredo($id=0){
		$this->load->helper('url');
		$this->load->model('referencia_model');
		$id_post=$this->input->post('id'); 
		//$num=$this->input->post('num');
		$nomedo=$this->input->post('nomedo');
		if($id_post!=''){
			$return=$this->referencia_model->actualizaredo($id_post,$nomedo); 			
			redirect('referencia');
		}
		}
		function combo(){
			$this->load->model('referencia_model');
			//$act=$this->input->post('actual');
        	//if($act>0) {	        
        	$filter['actual']=1;           
        	$data = $this->referencia_model->getElementsC($filter);        
        	echo '('.json_encode($data).')'; 
			//}
    	}
		function comboez(){
			$this->load->model('referencia_model');
			//$act=$this->input->post('actual');
        	//if($act>0) {	        
        	$filter['actual']=1;           
        	$data = $this->referencia_model->getElementsCEZ($filter);        
        	echo '('.json_encode($data).')'; 
			//}
    	}
		function comboz($act='0'){
			$this->load->model('referencia_model');
			$act=$this->input->post('actual');
        	if($act!='0' && $act!='null'){ $filter['where']['idzonedo =']=$act;            
        	$data = $this->referencia_model->getElementsCZ($filter);        
        	echo '('.json_encode($data).')'; 
			}
    	}
		function agregarzon(){
		$this->load->helper('url');
		$this->load->model('referencia_model');		
		$num=$this->input->post('num');
		$nomzon=$this->input->post('nomzon');
		$ref1=$this->input->post('ref1');
		$ref2=$this->input->post('ref2');
		if($num!=''){	
			$this->referencia_model->agregarzon($num,$nomzon,$ref1,$ref2);			
			redirect('referencia');
		}
		}
		function actualizarzon($id=0){
		$this->load->helper('url');
		$this->load->model('referencia_model');
		$id_post=$this->input->post('id'); 
		$num=$this->input->post('num');
		$nomzon=$this->input->post('nomzon');
		$ref1=$this->input->post('ref1');
		$ref2=$this->input->post('ref2');
		if($id_post!=''){
			$return=$this->referencia_model->actualizarzon($id_post,$num,$nomzon,$ref1,$ref2); 			
			redirect('referencia');
		}
		}
		function agregarbco(){
		$this->load->helper('url');
		$this->load->model('referencia_model');		
		$idcli=$this->input->post('idcli');
		$edo=$this->input->post('edo');
		$zon=$this->input->post('zon');
		$cli=$this->input->post('cli');
		$bco=$this->input->post('bco');
		if($idcli!=''){	
			$this->referencia_model->agregarbco($idcli,$edo,$zon,$cli,$bco);			
			redirect('referencia');
		}
		}
		function actualizarbco($id=0){
		$this->load->helper('url');
		$this->load->model('referencia_model');
		$id_post=$this->input->post('id'); 
		$idcli=$this->input->post('idcli');
		$edo=$this->input->post('edo');
		$zon=$this->input->post('zon');
		$cli=$this->input->post('cli');
		$bco=$this->input->post('bco');
		if($id_post!=''){
			$return=$this->referencia_model->actualizarbco($id_post,$idcli,$edo,$zon,$cli,$bco); 			
			redirect('referencia');
		}
		}
		function buscarcli1($id=0){
		$this->load->helper('url');
		$this->load->model('referencia_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->referencia_model->buscarcli($id_post); 			
			redirect('referencia');
		}
		}
		function buscarcli($fec=0){
		$this->load->helper('url');
		$this->load->model('referencia_model');
		$id_post=$this->input->post('id');
		if($id_post!=''){ 
			$row=$this->referencia_model->buscarcli($id_post);
			$size=sizeof($row);
			if($size>0){
				if($row->can!=86) $can=''; else $can='1';//;
			}else{
				$can='1';	
			}
			echo json_encode(array('can'=>$can));
		}
		}
    }
    
?>