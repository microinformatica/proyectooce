<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     class Cosechas extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
       	$this->load->model('cosechas_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('cosechas_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('cosechas/lista',$data);
        }
		function pdfrepcos( ) {
            $this->load->model('cosechas_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('cosechas/lista',$data);
			$data['tabla'] = $this->input->post('tabla');
			$data['dia'] = $this->input->post('dia');
			$data['tip'] = $this->input->post('tip');
			$html = $this->load->view('cosechas/listapdfdia', $data, true);  
			if($data['tip']==0) pdf ($html,$data['dia'].'_Gral', true);
			if($data['tip']==1) pdf ($html,$data['dia'].'_Maq', true);         	
			if($data['tip']==2) pdf ($html,$data['dia'].'_Vta', true);
			if($data['tip']==3) pdf ($html,$data['dia'].'_Don', true);
        }
		function pdfrepcosg( ) {
            $this->load->model('cosechas_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('cosechas/lista',$data);
			$data['tabla'] = $this->input->post('tablag');
			$html = $this->load->view('cosechas/listapdfgral', $data, true);  
			//$dia=date("Y-m-d");
			pdf ($html,'Rep_Gral_'.date("d-m-Y"), true);
			         	
        }
		public function tablacosechas($ciclo=0,$dia=0,$tipo=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($ciclo!=0){$filter['where']['ciccos =']=$ciclo;}
			if($dia!=0){$filter['where']['feccos =']=$dia;}
			if($tipo>0) {$filter['where']['tipcos =']=$tipo;}
			
        	$data['rows'] = $this->cosechas_model->cosechas($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablagral($ciclo=0,$secc=0){
			$filter = $this->ajaxsorter->filter($this->input);	
			$ano=substr($ciclo, 2,2);
			$filter['where']['numgra =']=4;
			//$filter['where']['ciccha =']=$ciclo;
			if($ciclo!=0){$filter['where']['ciccos =']=$ciclo;}
			if($secc>0) $filter['where']['secc =']=$secc;        
        	  
			$data['rows'] = $this->cosechas_model->gral($filter,$ano);
        	echo '('.json_encode($data).')';                
    	}
		function agregaCos(){
		$this->load->helper('url');
		$this->load->model('cosechas_model');
		$fec=$this->input->post('fec'); 
		$tip=$this->input->post('tip');
		$num=$this->input->post('num');
		$cli=$this->input->post('cli');
		$est=$this->input->post('est');
		$grs=$this->input->post('grs');
		$kgs=$this->input->post('kgs');
		$pre=$this->input->post('pre');
		$cic=$this->input->post('cic');
		if($fec!=''){	
			$this->cosechas_model->agregacos($fec,$tip,$num,$cli,$est,$grs,$kgs,$pre,$cic);			
			redirect('cosechas');
		}
		}
		function actualizaCos($id=0){
		$this->load->helper('url');
		$this->load->model('cosechas_model');
		$id=$this->input->post('id');
		$fec=$this->input->post('fec'); 
		$tip=$this->input->post('tip');
		$num=$this->input->post('num');
		$cli=$this->input->post('cli');
		$est=$this->input->post('est');
		$grs=$this->input->post('grs');
		$kgs=$this->input->post('kgs');
		$pre=$this->input->post('pre');
		$cic=$this->input->post('cic');
		if($id!=''){
			$return=$this->cosechas_model->actualizacos($id,$fec,$tip,$num,$cli,$est,$grs,$kgs,$pre,$cic); 			
			redirect('cosechas');
		}		
		}
		
		function quitarCos($id=0){
		$this->load->helper('url');
		$this->load->model('cosechas_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->cosechas_model->borrarcos($id_post); 			
			redirect('cosechas');
		}
		}
		public function combob(){
			//$filter['secc']=$this->input->post('secc');
			$filter['cicg']=$this->input->post('ciccos');
			$ciclo=$this->input->post('ciccos');
			
			$data = $this->cosechas_model->getElementsb($filter,$ciclo); 
			echo '('.json_encode($data).')';              
    	}
		
    }
    
?>