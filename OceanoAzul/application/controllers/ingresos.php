<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Ingresos extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('ingresos_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('ingresos_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('ingresos/lista',$data);
        }
		function pdfrep() {
            $this->load->model('ingresos_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ingresos/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('ingresos/listapdf', $data, true);  
			pdf ($html,'ingresos/listapdf', true);
        	set_paper('letter');
        }
		public function tabla(){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$data['rows'] = $this->ingresos_model->getingresos($filter);
        	echo '('.json_encode($data).')';                
    	}
				
    }
    
?>