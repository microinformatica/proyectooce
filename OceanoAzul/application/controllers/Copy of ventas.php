<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Ventas extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('ventas_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('ventas_model');
			//$data['result']=$this->facturado_model->Entregado();			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('ventas/lista',$data);
        }
		public function tabla($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($extra=='1') $filter['where']['estatus <=']=$extra; else $filter['where']['estatus =']=$extra;
			$data['rows'] = $this->ventas_model->getVentas($filter);
        	$data['num_rows'] = $this->ventas_model->getNumRowsV($filter);
        	echo '('.json_encode($data).')'; 
    	}
		public function tablaZona(){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$data['rows'] = $this->ventas_model->getZonasvta($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaVC($ciclo='',$extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['num'] = $ciclo;  
			if($extra=='1') $filter['where']['estatus <=']=$extra; else $filter['where']['estatus =']=$extra;
			$data['rows'] = $this->ventas_model->getVentasC($filter);
        	$data['num_rows'] = $this->ventas_model->getNumRowsVC($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function pdfrep() {
            $this->load->model('ventas_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ventas/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('ventas/listapdf', $data, true);  
			pdf ($html,'ventas/listapdf', true);        	
        	set_paper('letter');
			
        }
		function reporte( ) {
            $data['tablac'] = $this->input->post('tabla');        
        	$this->load->view('facproveedores/reporte',$data); 
        }
		function pdfrepVC() {
            $this->load->model('ventas_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ventas/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('ventas/listapdf', $data, true);  
			pdf ($html,'ventas/listapdf', true);        	
        	set_paper('letter');
			
        }
	}
    
?>