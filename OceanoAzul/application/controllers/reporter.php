<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporter extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model('facproveedores_model'); 
        $this->load->helper(array('url','form','html'));
        $this->load->library(array('ajaxsorter'));	
    }
    public function index(){
        $data['tablac'] = $this->input->post('tabla');        
        $this->load->view('reporte',$data);        
    }
   
}
