<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Aqpgranja extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('aqpgranja_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('aqpgranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('aqpgranja/lista',$data);
        }
		function pdfrep() {
            $this->load->model('aqpgranja_model');
			//$data['result']=$this->aqpgranja_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabla');
			$data['ciclo']=$this->input->post('cmbCiclop');
			$ng=$this->input->post('cmbGranja');
			if($ng==1) $data['ng']='Santa Fe';
			if($ng==2) $data['ng']='Gran Kino'; 
			if($ng==3) $data['ng']='Jazmin';
			if($ng==4) $data['ng']='Ahome';
			$html = $this->load->view('aqpgranja/programasie', $data, true);  
			pdf ($html,$data['ng'].'_'.'Siembra_'.$data['ciclo'], true);        	
        }
		function reporte( ) {
            $data['tablac'] = $this->input->post('tabla'); 
			$ngra=$this->input->post('cmbGranjaprod');
			if($ngra==1) $data['ng']='Santa Fe';
			if($ngra==2) $data['ng']='Gran Kino'; 
			if($ngra==3) $data['ng']='Jazmin';
			if($ngra==4) $data['ng']='Ahome';
			$this->load->view('aqpgranja/reporte',$data); 
        }
		function reportesem( ) {
            $this->load->model('aqpgranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;			
			$data['tablasem'] = $this->input->post('tablasem');
			$data['fec'] = 'Sem '.$this->input->post('idFec');
			$ngra=$this->input->post('cmbGranjasem');
			if($ngra==1) $data['ng']='Santa Fe';
			if($ngra==2) $data['ng']='Gran Kino'; 
			if($ngra==3) $data['ng']='Jazmin';
			if($ngra==4) $data['ng']='Ahome';
			$this->load->view('aqpgranja/reportesem',$data); 
        }
		function reportesemi() {
			$this->load->model('aqpgranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;			
			$data['tablasem'] = $this->input->post('tablasemi');
			$data['fec'] = $this->input->post('idFeci');
			$ngra=$this->input->post('cmbGranjasemi');
			if($ngra==1) $data['ng']='Santa Fe';
			if($ngra==2) $data['ng']='Gran Kino'; 
			if($ngra==3) $data['ng']='Jazmin';
			if($ngra==4) $data['ng']='Ahome';
			$this->load->view('aqpgranja/reportesem',$data);
	  	}
		function reportesempar( ) {
            $this->load->model('aqpgranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;			
			$data['tablasemp'] = $this->input->post('tablaparg');
			$ngra=$this->input->post('cmbGranjapar');
			if($ngra==1) $data['ng']='Santa Fe';
			if($ngra==2) $data['ng']='Gran Kino'; 
			if($ngra==3) $data['ng']='Jazmin';
			if($ngra==4) $data['ng']='Ahome';
			$this->load->view('aqpgranja/reportesempar',$data); 
        }
		function pdfrepestsem() {
			$this->load->model('aqpgranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;			
			$data['tablasem'] = $this->input->post('tablasem');
			$data['fec'] = $this->input->post('idFec');
			$ngra=$this->input->post('cmbGranjasem');
			if($ngra==1) $data['ng']='Santa Fe';
			if($ngra==2) $data['ng']='Gran Kino'; 
			if($ngra==3) $data['ng']='Jazmin';
			if($ngra==4) $data['ng']='Ahome';
			$html = $this->load->view('aqpgranja/semanal', $data, true);
			
			pdf ($html, $data['ng'].'-Sem:'.$data['fec'], true);	
								
      	}
		
		function pdfrepestdet() {
			$this->load->model('aqpgranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;			
			$data['tablaestdet'] = $this->input->post('tablaestdet');
			$data['tablaestdat'] = $this->input->post('tablaestdat');
			$data['ciclo'] = $this->input->post('cicloed');
			$est = $this->input->post('ested');
			//pedir el numero de estanque de acuerdo al id recibido
			$data['estnom'] =$this->aqpgranja_model->buscar($est);
			$html = $this->load->view('aqpgranja/infbioest', $data, true);
			pdf ($html,'aqpgranja/InfBioEst', true);						
      	}
		
		function pdfrepbio() {
			$this->load->model('aqpgranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;			
			$data['tablabio'] = $this->input->post('tablabio');
			$data['fec1'] = $this->input->post('txtFI');
			$data['fec2'] = $this->input->post('txtFF');
			$html = $this->load->view('aqpgranja/biometrias', $data, true);
			pdf ($html,'aqpgranja/Biometrias', true);						
      	}
		function pdfrepali() {
			$this->load->model('aqpgranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;			
			$data['tablaali'] = $this->input->post('tablaali');
			$data['fec1'] = $this->input->post('txtFIAli');
			$data['fec2'] = $this->input->post('txtFFAli');
			$html = $this->load->view('aqpgranja/alimentos', $data, true);
			
			pdf ($html,'aqpgranja/Alimentacion', true);						
      	}
		function pdfreppar() {
			$this->load->model('aqpgranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;			
			$data['tablabio'] = $this->input->post('tablapar');
			$data['fec1'] = $this->input->post('txtFIfq');
			$data['fec2'] = $this->input->post('txtFFfq');
			$html = $this->load->view('aqpgranja/parametros', $data, true);
			pdf ($html,'aqpgranja/Parametros', true);						
      	}
		function pdfrepprod() {
			$this->load->model('aqpgranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;			
			$data['tablabio'] = $this->input->post('tablaprod');
			$data['ciclo'] = $this->input->post('cicloprod');
			$html = $this->load->view('aqpgranja/produccion', $data, true);
			pdf ($html,'aqpgranja/Produccion', true);						
      	}
		public function tabla($numgra=0,$ciclo=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgra =']=$numgra;
			$filter['where']['cicg =']=$ciclo;
        	$data['rows'] = $this->aqpgranja_model->getSiembraPis($filter);
        	$data['num_rows'] = $this->aqpgranja_model->getNumRows($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaprod($numgra='',$ciclo=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgra =']=$numgra;
			$filter['where']['cicg =']=$ciclo;
        	$data['rows'] = $this->aqpgranja_model->getProduccion($filter);
        	$data['num_rows'] = $this->aqpgranja_model->getNumRowP($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaest($ciclo='',$est=0,$numgra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['cicg =']=$ciclo;
			$filter['where']['idpis =']=$est;
			$filter['where']['numgra =']=$numgra;
        	$data['rows'] = $this->aqpgranja_model->getDatosEst($filter);
        	$data['num_rows'] = $this->aqpgranja_model->getNumRowDE($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaestdat($ciclo='',$est=0,$numgra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['cicg =']=$ciclo;
			$filter['where']['idpis =']=$est;
			$filter['where']['numgra =']=$numgra;
        	$data['rows'] = $this->aqpgranja_model->getDatosEstDat($filter);
        	$data['num_rows'] = $this->aqpgranja_model->getNumRowDED($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaestsem($numgra=0,$ciclo=0,$fec=0,$sec=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['granja']=$numgra;
			$filter['where']['cicg =']=$ciclo;
			//$filter['where']['numgrab =']=$numgra;
			$fecq=explode(":",$fec);
			//$fec=substr($fec, 3, 10);
			$filter['where']['fecb =']=$fecq[1];
			if($sec!=0)$filter['where']['secc =']=$sec;
			
        	$data['rows'] = $this->aqpgranja_model->getDatosEstSem($filter,$fecq[1],$ciclo);
        	$data['num_rows'] = $this->aqpgranja_model->getNumRowDES($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablabio($desde='',$ciclo=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($desde!='') $filter['where']['fecb =']=$desde;
			//if($secc!='0') $filter['where']['secc =']=$secc; 
			if($ciclo!='0')$filter['where']['cicb =']=$ciclo;
			$data['rows'] = $this->aqpgranja_model->getBiometria($filter);
        	$data['num_rows'] = $this->aqpgranja_model->getNumRowsB($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablasob($desde='',$ciclo=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($desde!='') $filter['where']['fecs =']=$desde;
			//if($secc!='0') $filter['where']['secc =']=$secc; 
			if($ciclo!='0') $filter['where']['cics =']=$ciclo;
			$data['rows'] = $this->aqpgranja_model->getSobrevivencia($filter);
        	$data['num_rows'] = $this->aqpgranja_model->getNumRowsS($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaali($desde='',$hasta=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($desde!='') $filter['where']['fecali >=']=$desde;
			if($hasta!='') $filter['where']['fecali <=']=$hasta; 
			$data['rows'] = $this->aqpgranja_model->getAlimentos($filter);
        	$data['num_rows'] = $this->aqpgranja_model->getNumRowsA($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablapar($desde='',$ciclo=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($desde!='') $filter['where']['fecfq =']=$desde;
			//if($sec!='0') $filter['where']['secc =']=$sec; 
			if($ciclo!='0') $filter['where']['cicfq =']=$ciclo;
			$data['rows'] = $this->aqpgranja_model->getParametros($filter);
        	$data['num_rows'] = $this->aqpgranja_model->getNumRowsP($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablaparg($pil=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			
			//if($sec!='0') $filter['where']['secc =']=$sec; 
			if($pil!='') $filter['where']['idpischa =']=$pil;
			$data['rows'] = $this->aqpgranja_model->getParametrosg($filter);
        	echo '('.json_encode($data).')';                
    	}
    	function actualizarpis($id=0){
		$this->load->helper('url');
		$this->load->model('aqpgranja_model');
		$id_post=$this->input->post('id'); 
		$pis=$this->input->post('pis');
		$has=$this->input->post('has');
		$esp=$this->input->post('esp');
		$org=$this->input->post('org');
		$pro=$this->input->post('pro');
		$fec=$this->input->post('fec');
		$est=$this->input->post('est');
		if($est=='') $est=0;
		$cic=$this->input->post('cic');
		$fecc=$this->input->post('fecc');
		$bio=$this->input->post('bio');if($bio=='undefined') $bio=0;
		$pp=$this->input->post('pp');if($pp=='undefined')  $pp=0;
		$obs=$this->input->post('obs');if($obs=='undefined')  $obs='';
		$numgra=$this->input->post('numgra');
		$secc=$this->input->post('secc');
		$lote=$this->input->post('lote');if($lote=='') $lote=0;
		if($id_post!=''){
			$return=$this->aqpgranja_model->actualizar($id_post,$pis,$has,$esp,$org,$pro,$fec,$est,$cic,$fecc,$bio,$pp,$obs,$numgra,$secc,$lote); 			
			redirect('aqpgranja');
		}
		
		}
		
		function agregarpis(){
		$this->load->helper('url');
		$this->load->model('aqpgranja_model');		
		$pis=$this->input->post('pis');
		$has=$this->input->post('has');
		$esp=$this->input->post('esp');
		$org=$this->input->post('org');
		$pro=$this->input->post('pro');
		$fec=$this->input->post('fec');
		$est=$this->input->post('est');
		$cic=$this->input->post('cic');
		$numgra=$this->input->post('numgra');
		$secc=$this->input->post('secc');
		$lote=$this->input->post('lote');if($lote=='') $lote=0;
		if($pis!=''){	
			$this->aqpgranja_model->agregar($pis,$has,$esp,$org,$pro,$fec,$est,$cic,$numgra,$secc,$lote);			
			redirect('aqpgranja');
		}
		}
		function actualizarbio($id=0){
		$this->load->helper('url');
		$this->load->model('aqpgranja_model');
		$id_post=$this->input->post('id'); 
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$pes=$this->input->post('pes');
		$cic=$this->input->post('cic');
		$obs=$this->input->post('obs');
		$gra=$this->input->post('gra');
		$inc=$this->input->post('inc');
		if($inc=='') $inc=0;
		if($id_post!=''){
			$return=$this->aqpgranja_model->actualizarbio($id_post,$pis,$fec,$pes,$cic,$obs,$gra,$inc); 			
			redirect('aqpgranja');
		}
		
		}
		
		function agregarbio(){
		$this->load->helper('url');
		$this->load->model('aqpgranja_model');		
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$pes=$this->input->post('pes');
		$cic=$this->input->post('cic');
		$obs=$this->input->post('obs');
		$gra=$this->input->post('gra');
		$inc=$this->input->post('inc');
		if($inc=='') $inc=0;
		if($pis!=''){	
			$this->aqpgranja_model->agregarbio($pis,$fec,$pes,$cic,$obs,$gra,$inc);			
			redirect('aqpgranja');
		}
		}	
		function actualizarsob($id=0){
		$this->load->helper('url');
		$this->load->model('aqpgranja_model');
		$id_post=$this->input->post('id'); 
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$sob=$this->input->post('sob');
		$cic=$this->input->post('cic');
		$obs=$this->input->post('obs');
		$gra=$this->input->post('gra');
		$cam=$this->input->post('cam');
		$has=$this->input->post('has');
		$org=$this->input->post('org');
		if($id_post!=''){
			$return=$this->aqpgranja_model->actualizarsob($id_post,$pis,$fec,$sob,$cic,$obs,$gra,$cam,$has,$org); 			
			redirect('aqpgranja');
		}
		
		}
		
		function agregarsob(){
		$this->load->helper('url');
		$this->load->model('aqpgranja_model');		
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$sob=$this->input->post('sob');
		$cic=$this->input->post('cic');
		$obs=$this->input->post('obs');
		$gra=$this->input->post('gra');
		$cam=$this->input->post('cam');
		$has=$this->input->post('has');
		$org=$this->input->post('org');
		if($pis!=''){	
			$this->aqpgranja_model->agregarsob($pis,$fec,$sob,$cic,$obs,$gra,$cam,$has,$org);			
			redirect('aqpgranja');
		}
		}	
		function actualizarpar($id=0){
		$this->load->helper('url');
		$this->load->model('aqpgranja_model');
		$id_post=$this->input->post('id'); 
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$t1=$this->input->post('t1');
		$o1=$this->input->post('o1');
		$t2=$this->input->post('t2');
		$o2=$this->input->post('o2');
		$sal=$this->input->post('sal'); if($sal=='') $sal=0;
		$tur=$this->input->post('tur'); if($tur=='') $tur=0;
		$ph=$this->input->post('ph'); if($ph=='') $ph=0;
		$cic=$this->input->post('cic');
		$gra=$this->input->post('gra');		
		$salu=$this->input->post('salu');
		$reca=$this->input->post('reca');
		if($id_post!=''){
			$return=$this->aqpgranja_model->actualizarpar($id_post,$pis,$fec,$t1,$o1,$t2,$o2,$sal,$tur,$ph,$cic,$gra,$salu,$reca); 			
			redirect('aqpgranja');
		}
		
		}
		function agregarpar(){
		$this->load->helper('url');
		$this->load->model('aqpgranja_model');		
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$t1=$this->input->post('t1');
		$o1=$this->input->post('o1');
		$t2=$this->input->post('t2');
		$o2=$this->input->post('o2');
		$sal=$this->input->post('sal');  if($sal=='') $sal=0;
		$tur=$this->input->post('tur'); if($tur=='') $tur=0;
		$ph=$this->input->post('ph'); if($ph=='') $ph=0;
		$cic=$this->input->post('cic');
		$gra=$this->input->post('gra');
		$salu=$this->input->post('salu');
		$reca=$this->input->post('reca');
		if($pis!=''){	
			$this->aqpgranja_model->agregarpar($pis,$fec,$t1,$o1,$t2,$o2,$sal,$tur,$ph,$cic,$gra,$salu,$reca);			
			redirect('aqpgranja');
		}
		}	
		function actualizarali($id=0){
		$this->load->helper('url');
		$this->load->model('aqpgranja_model');
		$id_post=$this->input->post('id'); 
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$can=$this->input->post('can');
		$cic=$this->input->post('cic');
		$obs=$this->input->post('obs');
		$gra=$this->input->post('gra');
		if($id_post!=''){
			$return=$this->aqpgranja_model->actualizarali($id_post,$pis,$fec,$can,$cic,$obs,$gra); 			
			redirect('aqpgranja');
		}
		
		}
		
		function agregarali(){
		$this->load->helper('url');
		$this->load->model('aqpgranja_model');		
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$can=$this->input->post('can');
		$cic=$this->input->post('cic');
		$obs=$this->input->post('obs');
		$gra=$this->input->post('gra');
		if($pis!=''){	
			$this->aqpgranja_model->agregarali($pis,$fec,$can,$cic,$obs,$gra);			
			redirect('aqpgranja');
		}
		}
		public function comboba(){
			$filter['numgra']=$this->input->post('numgra');           
       		$data = $this->aqpgranja_model->getElementsb($filter); 
			echo '('.json_encode($data).')';              
    	}
		public function combob(){
			//$filter['secc']=$this->input->post('seccb');
			$filter['cicg']=$this->input->post('cmbCiclob');
			$ciclo=$this->input->post('cmbCiclob');
			//if($secc==4) $filter['where']['secc >=']='$secc';
			$data = $this->aqpgranja_model->getElementsb($filter,$ciclo); 
			echo '('.json_encode($data).')';              
    	}
		public function combobe(){
			//$filter['secc']=$this->input->post('seccb');
			$filter['cicg']=$this->input->post('cmbCicloEst');
			$ciclo=$this->input->post('cmbCicloEst');
			//if($secc==4) $filter['where']['secc >=']='$secc';
			$data = $this->aqpgranja_model->getElementsb($filter,$ciclo); 
			echo '('.json_encode($data).')';              
    	}
		public function combos(){
			$filter['cicg']=$this->input->post('cicg');
			//if($secc==4) $filter['where']['secc >=']='$secc';
			$data = $this->aqpgranja_model->getElementsbs($filter); 
			echo '('.json_encode($data).')';              
    	}
		public function comboo(){
			$filter['cicg']=$this->input->post('cmbCiclos');
			$ciclo=$this->input->post('cmbCiclos');
			//$filter['secc']=$this->input->post('secco');
			//if($secc==4) $filter['where']['secc >=']='$secc';
			$data = $this->aqpgranja_model->getElementsb($filter,$ciclo); 
			echo '('.json_encode($data).')';              
    	}
		public function combopSecc1(){
			//$filter['cicg']=$this->input->post('cmbCiclofq');
			//$ciclo=$this->input->post('cmbCiclofq');
			$secc=$this->input->post('seccp');
			if($secc>0) $filter['where']['secc =']=$secc;
			$data = $this->aqpgranja_model->getElementsbSecc($filter); 
			echo '('.json_encode($data).')';              
    	}
    	public function combopSecc($filest=0){
			$filest=$this->input->post('filest');
			$data = $this->aqpgranja_model->getElementsbSecc($filest); 
			echo '('.json_encode($data).')';              
    	}
		public function combopg(){
			$filter['secc']=$this->input->post('seccpg');
			//if($secc==4) $filter['where']['secc >=']='$secc';
			$data = $this->aqpgranja_model->getElementsb($filter); 
			echo '('.json_encode($data).')';              
    	}
		public function comboa(){
			$filter['secc']=$this->input->post('secca');
			//if($secc==4) $filter['where']['secc >=']='$secc';
			$data = $this->aqpgranja_model->getElementsb($filter); 
			echo '('.json_encode($data).')';              
    	}
		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('aqpgranja_model');
			$id_post=$this->input->post('id');
			$tab=$this->input->post('tabla'); 
			$cam=$this->input->post('campo'); 
			$ciclo=$this->input->post('cic');
			$ciclo=substr($ciclo,2,2);
			if($id_post!=''){
				$return=$this->aqpgranja_model->quitar($id_post,$tab,$cam,$ciclo); 			
				redirect('aqpgranja');
			}
		}
		function borrarfq($id=0){
			$this->load->helper('url');
			$this->load->model('aqpgranja_model');
			$id_post=$this->input->post('id');
			
			if($id_post!=''){
				$return=$this->aqpgranja_model->quitarfq($id_post); 			
				redirect('aqpgranja');
			}
		}
		function buscarest(){
			$pil = $this->input->post('pil');
			$data='';
			$data =$this->aqpgranja_model->history($pil);
			//$size=sizeof($data);
			if($data!=''){
				//si lo encuentra deja los datos
				echo json_encode(array('hasg'=>$data->hasg,'orgg'=>$data->orgg));
			}
						
		}
		function buscarestsie(){
			$pil = $this->input->post('pil');
			$data='';
			$data =$this->aqpgranja_model->historyestsie($pil);
			//$size=sizeof($data);
			if($data!=''){
				//si lo encuentra deja los datos
				echo json_encode(array('hasest'=>$data->hasest,'secest'=>$data->secest));
			}
						
		}		
    }
    
?>