<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Faccompras extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('faccompras_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('faccompras_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('faccompras/lista',$data);
        }
		function pdfrepf() {
            $this->load->model('faccompras_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			/*$data['ord']=$this->input->post('ordsel');
			$data['fec']=$this->input->post('fecsel');
			$data['pro']=$this->input->post('prosel');*/
			//$this->load->view('faccomprass/lista',$data);
			$data['des'] = '';
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('faccompras/listapdfF', $data, true);  
			pdf ($html,'faccompras/listapdf', true);
        	//set_paper('letter');
        }
		function pdfrep() {
            $this->load->model('faccompras_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabladps');
			$data['des'] = $this->input->post('dess');
			$html = $this->load->view('faccompras/listapdfF', $data, true);  
			pdf ($html,'faccompras/listapdf', true);
        }
		function pdfreps() {
            $this->load->model('faccompras_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabladms');
			$data['des'] = $this->input->post('desm');
			$html = $this->load->view('faccompras/listapdfF', $data, true);  
			pdf ($html,'faccompras/listapdf', true);
        }
		function pdfrepOrden() {
            $this->load->model('faccompras_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['ord']=$this->input->post('ordsel');
			$data['fec']=$this->input->post('fecsel');
			$data['pro']=$this->input->post('prosel');
			//$this->load->view('faccomprass/lista',$data);
			
			$data['tablac'] = $this->input->post('tablao');
			$html = $this->load->view('faccompras/listapdf', $data, true);  
			pdf ($html,'faccompras/listapdf', true);
        	//set_paper('letter');
        }
		public function tabla($ciclo='',$tip=0,$dia=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	if($tip!=0){ $filter['where']['tipo']=$tip; }
        	//if($dia!=0){ $filter['where']['fcap']=$dia; }
			if($dia!=0){ $filter['where']['fec']=$dia; }
			$data['rows'] = $this->faccompras_model->getFaccompras($filter,$ciclo);
        	$data['num_rows'] = $this->faccompras_model->getNumRows($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		public function tablacgral($ciclo='',$tip=0,$buscar=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	if($tip!=0){ $filter['where']['categoria']=$tip; }
			//if($buscar!='')	$filter['where']['des like']=$buscar;
        	$data['rows'] = $this->faccompras_model->getfaccomprasgral($filter,$buscar,$ciclo);
        	$data['num_rows'] = $this->faccompras_model->getNumRowsgral($filter,$buscar,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaclientes($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra; 
        	$data['rows'] = $this->faccompras_model->getClientes($filter);
        	$data['num_rows'] = $this->faccompras_model->getNumClientes($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaproves($ciclo=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	//$filter['num'] = $extra; 
        	$data['rows'] = $this->faccompras_model->getProves($filter,$ciclo);
        	$data['num_rows'] = $this->faccompras_model->getNumProves($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaps($ciclo='',$pro=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	//$filter['num'] = $extra; 
        	$data['rows'] = $this->faccompras_model->getPS($filter,$ciclo,$pro);
        	$data['num_rows'] = $this->faccompras_model->getNumPS($filter,$ciclo,$pro);
        	echo '('.json_encode($data).')';                
    	}
		public function tabladps($ciclo='',$pro=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	//$filter['num'] = $extra; 
        	$data['rows'] = $this->faccompras_model->getDPS($filter,$ciclo,$pro);
        	$data['num_rows'] = $this->faccompras_model->getNumDPS($filter,$ciclo,$pro);
        	echo '('.json_encode($data).')';                
    	}
		public function tabladms($ciclo='',$con=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	//$filter['num'] = $extra; 
        	$data['rows'] = $this->faccompras_model->getDMS($filter,$ciclo,$con);
        	$data['num_rows'] = $this->faccompras_model->getNumDMS($filter,$ciclo,$con);
        	echo '('.json_encode($data).')';                
    	}
    	public function tablaconceptos($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra; 
			$data['rows'] = $this->faccompras_model->getConceptos($filter);
        	$data['num_rows'] = $this->faccompras_model->getNumConceptos($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaconceps($ciclo=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	//$filter['num'] = $extra; 
			$data['rows'] = $this->faccompras_model->getConceptosS($filter,$ciclo);
        	$data['num_rows'] = $this->faccompras_model->getNumConceptosS($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaorden($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra; 
			$data['rows'] = $this->faccompras_model->getOrden($filter);
        	$data['num_rows'] = $this->faccompras_model->getNumOrden($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tabladetalle($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	if($extra!=0) $filter['num'] = $extra; else $filter['num'] = -1;  
			$data['rows'] = $this->faccompras_model->getDetalle($filter);
        	$data['num_rows'] = $this->faccompras_model->getNumDetalle($filter);
        	echo '('.json_encode($data).')';                
    	}
		function agregarR(){
			$con=$this->input->post('con');
			$tip=$this->input->post('tip');
			$ana=$this->input->post('ana');
			$cat=$this->input->post('cat');
			$pro=$this->input->post('pro');
			$ant=$this->input->post('ant');
			$impant=$this->input->post('impant');
			$dp=$this->input->post('dp');
			$eximzt=$this->input->post('eximzt');
			$exilab=$this->input->post('exilab');
			$consumo=$this->input->post('consumo');
			if($con!=''){	
				$this->faccompras_model->agregarR($con,$tip,$ana,$cat,$pro,$ant,$impant,$dp,$eximzt,$exilab,$consumo);			
				redirect('faccompras');
			}
		} 
		function actualizarR($id=0){
			$this->load->helper('url');
			$this->load->model('faccompras_model');
			$id_post=$this->input->post('id'); 
			$con=$this->input->post('con');
			$tip=$this->input->post('tip');
			$ana=$this->input->post('ana');
			$cat=$this->input->post('cat');
			$pro=$this->input->post('pro');
			$ant=$this->input->post('ant');
			$impant=$this->input->post('impant');
			$dp=$this->input->post('dp');
			$eximzt=$this->input->post('eximzt');
			$exilab=$this->input->post('exilab');
			$consumo=$this->input->post('consumo');
			if($id_post!=''){
				$return=$this->faccompras_model->actualizarR($id_post,$con,$tip,$ana,$cat,$pro,$ant,$impant,$dp,$eximzt,$exilab,$consumo); 			
				redirect('faccompras');
			}
		}
		function agregarF(){
			$con=$this->input->post('con');
			$pro=$this->input->post('pro');
			$tip=$this->input->post('tip');
			$fac=$this->input->post('fac');
			$can=$this->input->post('can');
			$pre=$this->input->post('pre');
			$med=$this->input->post('med');
			$fec=$this->input->post('fec');
			$cic=$this->input->post('cic');
			if($con!=''){	
				$this->faccompras_model->agregarF($con,$pro,$tip,$fac,$can,$pre,$med,$fec,$cic);			
				redirect('faccompras');
			}
		} 
		function actualizarF($id=0){
			$this->load->helper('url');
			$this->load->model('faccompras_model');
			$id_post=$this->input->post('id'); 
			$con=$this->input->post('con');
			$pro=$this->input->post('pro');
			$tip=$this->input->post('tip');
			$fac=$this->input->post('fac');
			$can=$this->input->post('can');
			$pre=$this->input->post('pre');
			$med=$this->input->post('med');
			$fec=$this->input->post('fec');
			$cic=$this->input->post('cic');
			if($id_post!=''){
				$return=$this->faccompras_model->actualizarF($id_post,$con,$pro,$tip,$fac,$can,$pre,$med,$fec,$cic); 			
				redirect('faccompras');
			}
		}
		function actualizarO($id=0){
			$this->load->helper('url');
			$this->load->model('faccompras_model');
			$id_post=$this->input->post('id'); 
			$pro=$this->input->post('pro');
			$fec=$this->input->post('fec');
			if($id_post!=''){
				$return=$this->faccompras_model->actualizarO($id_post,$pro,$fec); 			
				redirect('faccompras');
			}
		}
		function agregarO(){
			$pro=$this->input->post('pro');
			$fec=$this->input->post('fec');
			if($pro!=''){	
				$this->faccompras_model->agregarO($pro,$fec);			
				redirect('faccompras');
			}
		}
 		function actualizarD($id=0){
			$this->load->helper('url');
			$this->load->model('faccompras_model');
			$id_post=$this->input->post('id'); 
			$can=$this->input->post('can');
			$des=$this->input->post('des');
			$obs=$this->input->post('obs');
			if($id_post!=''){
				$return=$this->faccompras_model->actualizarD($id_post,$can,$des,$obs); 			
				redirect('faccompras');
			}
		}
		function agregarD(){
			$can=$this->input->post('can');
			$des=$this->input->post('des');
			$obs=$this->input->post('obs');
			$nro=$this->input->post('nro');
			if($nro!=''){	
				$this->faccompras_model->agregarD($can,$des,$obs,$nro);			
				redirect('faccompras');
			}
		}
		function borrarD($id=0){
		$this->load->helper('url');
		$this->load->model('faccompras_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->faccompras_model->borrarD($id_post); 			
			redirect('faccompras');
		}
		}
		function borrarF($id=0){
		$this->load->helper('url');
		$this->load->model('faccompras_model');
		$id_post=$this->input->post('id');
		$ciclo=$this->input->post('ciclo'); 
		if($id_post!=''){
			$return=$this->faccompras_model->borrarF($id_post,$ciclo); 			
			redirect('faccompras');
		}
		}
    }
    
?>