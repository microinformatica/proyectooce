<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class General extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('general_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('general_model');
			//$data['result']=$this->general_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('general/lista',$data);
        }
		
		public function tablab($cic=0,$gra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->general_model->generalb($gra,$cic);
        	echo '('.json_encode($data).')';                
    	}
    	public function tablav($cic=0,$gra=0,$tipo=0,$dia=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->general_model->generalv($gra,$cic,$tipo,$dia);
        	echo '('.json_encode($data).')';                
    	}
    	public function tablag($cic=0,$gra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->general_model->generalg($gra,$cic);
        	echo '('.json_encode($data).')';                
    	}
    	public function tablabord($cic=0,$gra=0,$dia=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->general_model->generalbord($cic,$gra,$dia);
        	echo '('.json_encode($data).')';                
    	}
    	public function tablaborvd($cic=0,$gra=0,$dia=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->general_model->generalborvd($cic,$gra,$dia);
        	echo '('.json_encode($data).')';                
    	}
    	public function tablamaqd($cic=0,$gra=0,$dia=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->general_model->generalmaqd($cic,$gra,$dia);
        	echo '('.json_encode($data).')';                
    	}
    	public function tablaprod($cic=0,$gra=0,$dia=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->general_model->generalprod($cic,$gra,$dia);
        	echo '('.json_encode($data).')';                
    	}
    	public function tabladepd($gra=0,$dia=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->general_model->generaldepd($gra,$dia);
        	echo '('.json_encode($data).')';                
    	}
    	public function tablavtatal($cic=0,$gra=0,$dia='',$g=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->general_model->generalvtat($cic,$gra,$dia,$g);
        	echo '('.json_encode($data).')';                
    	}
    	public function tablavtatalm($cic=0,$gra=0,$dia='',$g=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
        	$data['rows'] = $this->general_model->generalvtatm($cic,$gra,$dia,$g);
        	echo '('.json_encode($data).')';                
    	}
    	
		function pdfrepgral() {
            $this->load->model('general_model');
			$dia=date('d-m-Y');
			$data['dia'] = date('d-m-Y');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('general/lista',$data);
			$data['granja'] = $this->input->post('gra1');
			if($data['granja'] == 4) $data['granja']='OA-Ahome';
			if($data['granja'] == 10) $data['granja']='OA-Topolobampo';
			$data['tablab'] = $this->input->post('tablab');$data['tablav'] = $this->input->post('tablav');
			$data['tablam'] = $this->input->post('tablam');$data['tablag'] = $this->input->post('tablag');
			$data['ing'] = $this->input->post('ing');$data['gto'] = $this->input->post('gto');
			$data['sal'] = $this->input->post('sal');$data['dia'] = $this->input->post('dia1');
			//$data['grafica'] = $this->input->post('grapdf');
			$html = $this->load->view('general/listapdf', $data, true);  
			pdf ($html,'General_'.$data['granja'].'_'.$data['dia'], true);   
        }
        function pdfrepgrald() {
            $this->load->model('general_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('general/lista',$data);
			$data['granja'] = $this->input->post('gra1d');
			$data['dia'] = $this->input->post('dia1d');
			if($data['granja'] == 4) $data['granja']='OA-Ahome';
			if($data['granja'] == 10) $data['granja']='OA-Topolobampo';
			$data['tablabd'] = $this->input->post('tablabd');$data['tablabvd'] = $this->input->post('tablabvd');
			$data['tablavd'] = $this->input->post('tablavd');$data['tablamd'] = $this->input->post('tablamd');
			$data['tabladd'] = $this->input->post('tabladd');
			//$data['grafica'] = $this->input->post('grapdf');
			$html = $this->load->view('general/listapdfd', $data, true);  
			pdf ($html,'General_'.$data['granja'].'_Corte_'.$data['dia'], true);   
        }
    }
?>