<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Tecnicos extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('tecnicos_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('tecnicos_model');
			//$data['result']=$this->facturado_model->Entregado();			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('tecnicos/lista',$data);
        }
		function tabla($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//if($extra=='1') $filter['where']['estatus <=']=$extra; else $filter['where']['estatus =']=$extra;
			$filter['where']['numbios =']=$extra;
			$data['rows'] = $this->tecnicos_model->getTecnicos($filter);
        	$data['num_rows'] = $this->tecnicos_model->getNumRowsT($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablatotet($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//if($extra=='1') $filter['where']['estatus <=']=$extra; else $filter['where']['estatus =']=$extra;
			//$filter['where']['numbios =']=$extra;
			$data['rows'] = $this->tecnicos_model->getTotet($filter);
        	$data['num_rows'] = $this->tecnicos_model->getNumRowsTotet($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablatotdeti($bio=0,$mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			//if($extra=='1') $filter['where']['estatus <=']=$extra; else $filter['where']['estatus =']=$extra;
			$filter['where']['numbios =']=$bio;
			if($mes>0) $filter['where']['month(FechaR) =']=$mes;
			$data['rows'] = $this->tecnicos_model->getTecnicosdet($filter);
        	$data['num_rows'] = $this->tecnicos_model->getNumRowsTdet($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function pdfrep() {
            $this->load->model('tecnicos_model');
			
			$data['tecnico'] = $this->input->post('tecnico');
			$data['total'] = $this->input->post('totel');
			$datax['result']=$this->tecnicos_model->verTecnico1($data['tecnico']);
			foreach ($datax['result'] as $row): 
				$tec=$row->NomBio;
			endforeach;
			$data['tecnico']=$tec;
			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('tecnicos/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('tecnicos/listapdf', $data, true);  
			pdf ($html,'tecnicos/listapdf', true);        	
        	set_paper('letter');
			
        }
		function pdfrepdet() {
            $this->load->model('tecnicos_model');
			
			$data['tecnico'] = $this->input->post('tecnico1');
			$data['total'] = $this->input->post('toteld');
			$datax['result']=$this->tecnicos_model->verTecnico1($data['tecnico']);
			foreach ($datax['result'] as $row): 
				$tec=$row->NomBio;
			endforeach;
			$data['tecnico']=$tec;
			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('tecnicos/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('tecnicos/listapdfdet', $data, true);  
			pdf ($html,'tecnicos/listapdfdet', true);        	
        	set_paper('letter');
			
        }
	}
    
?>