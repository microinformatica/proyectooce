<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Vigilancia extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('vigilancia_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('vigilancia_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('vigilancia/lista',$data);
        }
		
		public function tablabit($lab=0,$dia=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($dia!='') $filter['where']['dia =']=$dia;
			if($lab>0) $filter['where']['lab =']=$lab;			
			$data['rows'] = $this->vigilancia_model->getbitacora($filter);
        	echo '('.json_encode($data).')'; 
    	}
		
		
		
		
	
		function borrarc($id=0){
		$this->load->helper('url');
		$this->load->model('vigilancia_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->vigilancia_model->borrarc($id_post); 			
			redirect('vigilancia');
		}
		}
		
		//asignacion
		function pdfrepa( ) {
            $this->load->model('vigilancia_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('vigilancia/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('vigilancia/listapdfa', $data, true);  
			pdf ($html,'vigilancia/listapdfa', true);
        	set_paper('letter');
        }
		
		
		function actualizar($id=0){
			$this->load->model('vigilancia_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$hr=$this->input->post('hr');
			$min=$this->input->post('min');
			$des=$this->input->post('des');
			$vig=$this->input->post('vig');
			$lab=$this->input->post('lab');
			$ap=$this->input->post('ap');
			if($id_post!=''){
				$return=$this->vigilancia_model->actualizar($id_post,$fec,$hr,$min,$des,$vig,$lab,$ap); 			
				redirect('vigilancia');
			}
		}
		
		function agregar(){
			$this->load->model('vigilancia_model');		
			$fec=$this->input->post('fec');
			$hr=$this->input->post('hr');
			$min=$this->input->post('min');
			$des=$this->input->post('des');
			$vig=$this->input->post('vig');
			$lab=$this->input->post('lab');
			$ap=$this->input->post('ap');
		if($fec!=''){	
			$this->vigilancia_model->agregar($fec,$hr,$min,$des,$vig,$lab,$ap);			
			redirect('vigilancia');
		}
		}

		function buscar(){
			$rem = $this->input->post('rem');
			//busca el actual si lo encuentra deja los datos
			$data =$this->vigilancia_model->history($rem);
			/*if($data->cp1=='-1') $data->cp1=0;if($data->cp2=='-1') $data->cp2=0;if($data->cp3=='-1') $data->cp3=0;if($data->cp4=='-1') $data->cp4=0;
			if($data->cp5=='-1') $data->cp5=0;if($data->cp6=='-1') $data->cp6=0;*/
			if($data->CantidadRR>'0') $data->CantidadRR=number_format($data->CantidadRR,3); else $data->CantidadRR='';
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos RemisionR,CantidadRR,NomCho,NomBio,NomUni,Loc,Edo
				echo json_encode(array('rem'=>$data->RemisionR,'can'=>$data->CantidadRR,'cho'=>$data->NomCho,'bio'=>$data->NomBio,'uni'=>$data->NomUni,'loc'=>$data->Loc,'edo'=>$data->Edo));
			}
						
		}
				
    }
    
?>