<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Viaticos extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('viaticos_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }        
        function index() {
            $this->load->model('viaticos_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('viaticos/lista',$data);
        }
		function pdfrep() {
            $this->load->model('viaticos_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['zona']=$this->input->post('zonasel');
			$data['ciclo']=$this->input->post('ciclosel2');
			$this->load->view('viaticos/lista',$data);
			//$data['zona']=$this->input->post('zonasel');			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('viaticos/listapdf', $data, true);  
			pdf ($html,'viaticos/listapdf', true);
        	set_paper('letter');
        }		
		public function tabla($destino='',$unidad=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($destino!='Todos'){
        		if($destino=='El%20Dorado') $destino='El Dorado';
				if($destino=='El%20Carrizo') $destino='El Carrizo';
				if($destino=='La%20Cruz') $destino='La Cruz';
				if($destino=='Los%20Mochis') $destino='Los Mochis';
				if($destino=='Cd.%20Obregon') $destino='Cd. Obregon';
				if($destino=='San%20Blas') $destino='San Blas';
				if($destino=='Reynosa%20Tam.') $destino='Reynosa Tam.';
				if($destino=='Nayarit%20Pimientillo') $destino='Nayarit Pimientillo';
				//if($destino=='Sant%Blas') $destino='San Blas';
				//if($zona=='Yucat%C3%A1n') $destino='Yucatán';
        		$filter['where']['nomdes']=$destino;
			} 
			//if($destino!='Todos') $filter['where']['nomdes =']=$destino;
			if($unidad!='Todos') $filter['where']['numunid =']=$unidad; 
			$data['rows'] = $this->viaticos_model->getCargosVia($filter);
        	$data['num_rows'] = $this->viaticos_model->getNumRowsVia($filter);
        	echo '('.json_encode($data).')';
    	}
		public function combo(){        
        	$filter['NomDes']=$this->input->post('NomDes');           
        	$data = $this->viaticos_model->getElements($filter);        
        	echo '('.json_encode($data).')';  
    	}
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	}
		function agregar(){
		$this->load->helper('url');
		$this->load->model('cargos_model');		
		$des=$this->input->post('des');
		$uni=$this->input->post('uni');
		$ali=$this->input->post('ali');
		$com=$this->input->post('com');
		$cas=$this->input->post('cas');
		$hos=$this->input->post('hos');
		$fit=$this->input->post('fit');
		$obs=$this->input->post('obs');
		if($des!=''){	
			$this->viaticos_model->agregar($des,$uni,$ali,$com,$cas,$hos,$fit,$obs);			
			redirect('viaticos');
		}
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('viaticos/lista',$datos);
		} 
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('viaticos_model');
		$id_post=$this->input->post('id'); 
		$des=$this->input->post('des');
		$uni=$this->input->post('uni');
		$ali=$this->input->post('ali');
		$com=$this->input->post('com');
		$cas=$this->input->post('cas');
		$hos=$this->input->post('hos');
		$fit=$this->input->post('fit');
		$obs=$this->input->post('obs');
		if($id_post!=''){
			$return=$this->viaticos_model->actualizar($id_post,$des,$uni,$ali,$com,$cas,$hos,$fit,$obs); 			
			redirect('viaticos');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->viaticos_model->getViatico($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('viaticos/lista',$datos);
		}
		function borrar($id=0){
		$this->load->helper('url');
		$this->load->model('viaticos_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->viaticos_model->borrar($id_post); 			
			redirect('viaticos');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->viaticos_model->getViatico($id_post);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('viaticos/lista',$datos);
		}		
    }
    
?>