<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Saldos extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('saldos_model');         
        $this->load->library(array('ajaxsorter','session','enletras','libreria','user_agent'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('saldos_model');
			//$data['result']=$this->facturado_model->Entregado();
			//$data['result']=$this->saldos_model->zonas();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('saldos/lista',$data);
        }
		function pdfrepBueno() {
            $this->load->model('saldos_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('saldos/lista',$data);
			$data['tablarem'] = $this->input->post('tablaremisiones');
			$data['tabladep'] = $this->input->post('tabladepositos');
			
			$data['cli'] = $this->input->post('cli');
			$data['dom'] = $this->input->post('dom');
			$data['lecp'] = $this->input->post('lecp');
			$data['saldo'] = $this->input->post('saldo');
			$data['letras'] = $this->input->post('letras');
			$data['atc'] = $this->input->post('atc');
			$data['ref'] = $this->input->post('ref');
			$html = $this->load->view('saldos/listapdf', $data, true);
			//$cia='aqp';
			//pdf ($html,'saldos/edocta/'.$cia, true);
			pdf ($html,'saldos/edocta', true);
        	set_paper('letter');
        }
		function pdfrep( ) {
			$this->load->model('saldos_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			
			$data['tablarem'] = $this->input->post('tablaremisiones');
			$data['tabladep'] = $this->input->post('tabladepositos');
			$data['cli'] = $this->input->post('cli');
			$data['dom'] = $this->input->post('dom');
			$data['lecp'] = $this->input->post('lecp');
			$data['saldo'] = $this->input->post('saldo');
			$data['letras'] = $this->input->post('letras');
			$data['atc'] = $this->input->post('atc');
			$data['ref'] = $this->input->post('ref');
			$html = $this->load->view('saldos/listapdf', $data, true);
			if ($this->agent->is_mobile()){
				if(file_exists('./assets/pdf/edocta.pdf')) unlink('./assets/pdf/edocta.pdf');
				file_put_contents('./assets/pdf/edocta.pdf', pdf ($html,'',false));	
			}else{
				pdf ($html,'EdoCta_'.$data['cli'], true);
			}					
      	}
		
		public function tablaremi($cliente=0,$ciclo='',$cancelacion=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $ciclo; 					
			$filter['where']['NumCliR']=$cliente;										
			if($cancelacion==-1){$filter['where']['Cancelacion']=0;}
			if($cancelacion==1){$filter['where']['imp']=0;}
			if($cancelacion==2){$filter['where']['imp']=1;}
			if($cancelacion==3){$filter['where']['imp']=2;}
        	$data['rows'] = $this->saldos_model->remisiones($filter,$cancelacion);
        	$data['num_rows'] = $this->saldos_model->getNumRowsR($filter);
        	echo '('.json_encode($data).')';			
    	}
		public function tabladepo($cliente=0,$ciclo=0,$cancelacion=0){
			$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $ciclo; 					
			$filter['where']['NRC']=$cliente;	$imp=0;				
			if($cancelacion==-1){$filter['where']['Cancelacion']=0;$imp=100;}	
			if($cancelacion==1){$filter['where']['impd']=0;$imp=0;}							
			if($cancelacion==2){$filter['where']['impd']=1;$imp=1;}
			if($cancelacion==3){$filter['where']['impd']=2;$imp=2;}
        	$data['rows'] = $this->saldos_model->depositos($filter,$cliente,$cancelacion,$imp);
        	$data['num_rows'] = $this->saldos_model->getNumRowsD($filter);
			echo '('.json_encode($data).')'; 
    	}
		public function tablaclientes($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra; 
        	$data['rows'] = $this->saldos_model->getClientes($filter);
        	$data['num_rows'] = $this->saldos_model->getNumClientes($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function letrastras(){
			$saldo=$this->input->post('saldo1');
			if($saldo=="") $saldo=0;
			$V=new EnLetras();
			$data['canlet'] =$V->ValorEnLetras($saldo,"Dolares");			
			
			//echo json_encode($data);
        	echo '('.json_encode($data).')'; 
			//echo json_encode(array('status'=>$status,'error'=>$error_msg));			
    	}
		
		public function remidepo($cliente=0,$tabla='',$valor='',$ciclo=0,$solodep=0){
			$this->load->model('saldos_model');
			$cliente=$this->input->post('cliente');//$cliente=289;
			$tabla=$this->input->post('tabla');//$tabla='r13';
			$valor=$this->input->post('valor');//$rod='Remisiones';
			$ciclo=$this->input->post('ciclo');//$rod='Remisiones';						
			$solodep=$this->input->post('solodep');//$rod='Remisiones';
			$data= $this->saldos_model->rescandatos($cliente,$tabla,$valor,$ciclo,$solodep);
    	}
		
		function actualizarD($id=0){
			$this->load->helper('url');
			$this->load->model('saldos_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$usd=$this->input->post('usd'); if($usd=='') $usd=0;
			$tc=$this->input->post('tc'); if($tc=='') $tc=0;
			$cta=$this->input->post('cta');
			$obs=$this->input->post('obs');
			$mn=$this->input->post('mn');  if($mn=='') $mn=0;
			$numcli=$this->input->post('nrc');
			$ciclo=$this->input->post('ciclo');
			$est=$this->input->post('est');
			$descam=$this->input->post('descam');
			$cancelar=$this->input->post('cancelar');
			$impd=$this->input->post('impd');  if($impd=='') $impd=0;
			if($id_post!=''){
				$return=$this->saldos_model->actualizard($id_post,$fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam,$cancelar,$impd); 			
				redirect('saldos');
			}
		}
		function borrarD($id=0){
			$this->load->helper('url');
			$this->load->model('saldos_model');
			$id_post=$this->input->post('id'); 
			$usd=$this->input->post('usd');
			$numcli=$this->input->post('nrc');		
			if($id_post!=''){
				$return=$this->saldos_model->borrard($id_post,$usd,$numcli); 			
				redirect('saldos');
			}
		}
		function actsaldo($id=0){
			$this->load->helper('url');
			$this->load->model('saldos_model');
			$id_post=$this->input->post('numcli'); 			
			$saldo=$this->input->post('saldo1');
			if($id_post!=''){
				$return=$this->saldos_model->actsaldocli($id_post,$saldo); 			
				redirect('saldos');
			}
		}
		function actualizarSI($id=0){
			$this->load->helper('url');
			$this->load->model('saldos_model');
			$id_post=$this->input->post('id'); 			
			$cancelar=$this->input->post('cancelar');
			if($id_post!=''){
				$return=$this->saldos_model->actualizarsi($id_post,$cancelar); 			
				redirect('saldos');
			}
		}
		function actualizarR($id=0){
			$this->load->helper('url');
			$this->load->model('saldos_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			//$rem=$this->input->post('rem');
			$cli=$this->input->post('cli');
			$can=$this->input->post('can');
			$pre=$this->input->post('pre');
			$avi=$this->input->post('avi');
			$est=$this->input->post('est');
			$dco=$this->input->post('dco');
			$tipo=$this->input->post('tipo');
			$obs=$this->input->post('obs');
			$tabla=$this->input->post('tabla');
			//$fol=$this->input->post('fol');
			//$ur=$this->input->post('ur');
			$cancelar=$this->input->post('rec');
			$imp=$this->input->post('imp');
			if($id_post!=''){
				$return=$this->saldos_model->actualizarr($id_post,$fec,$cli,$can,$pre,$avi,$est,$dco,$tipo,$obs,$tabla,$cancelar,$imp); 			
				redirect('saldos');
			}		
		}
		function agregardcd(){
		$this->load->helper('url');
		$this->load->model('saldos_model');		
		$fec=$this->input->post('fec');
		$usd=$this->input->post('usd'); if($usd=='') $usd=0;
		$tc=$this->input->post('tc'); if($tc=='') $tc=0;
		$cta=$this->input->post('cta');
		$obs=$this->input->post('obs');
		$mn=$this->input->post('mn');  if($mn=='') $mn=0;
		$numcli=$this->input->post('nrc');
		$ciclo=$this->input->post('ciclo');
		$est=$this->input->post('est');
		$descam=$this->input->post('descam');
		if($fec!=''){	
			$this->saldos_model->agregarDCD($fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam);			
			redirect('saldos');
		}
		}
}
    
?>