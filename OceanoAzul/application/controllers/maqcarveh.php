<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Maqcarveh extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('maqcarveh_model');         
        $this->load->library(array('ajaxsorter','session','enletras','libreria','user_agent'));		
        $this->load->helper(array('url','form','html','pdf'));
		$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		
		if($id_usuario==false)redirect('login'); 
	   }
        
        function index() {
            $this->load->model('maqcarveh_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('maqcarveh/lista',$data);
        }
		
		
		public function tablaeva($ciclo='',$mes=0,$cat){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->maqcarveh_model->getmaqcarvehG($filter,$ciclo,$mes,$cat);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablamaq($ciclo='',$fec=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['fecmaq =']=$fec;
			$data['rows'] = $this->maqcarveh_model->getmaq($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablacar($ciclo='',$fec=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['feccar =']=$fec;
			$data['rows'] = $this->maqcarveh_model->getcar($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablalim($ciclo='',$fec=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['feclim =']=$fec;
			$data['rows'] = $this->maqcarveh_model->getlim($filter,$ciclo,$fec);
        	echo '('.json_encode($data).')';                
    	}

		public function tablaveh($ciclo='',$fec=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['fecveh =']=$fec;
			$data['rows'] = $this->maqcarveh_model->getveh($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		
		function reportedie( ) {
        	$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablacomg');
			$this->load->view('maqcarveh/reportedie',$data); 
        }
		function pdfmes() {
            $this->load->model('maqcarveh_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablaeva');
			$data['mes']=$this->input->post('cmbMeskpi');
			$data['cic']=$this->input->post('ciclorep');
			$data['mesdia']='Mes';
			switch($data['mes']){
				case 1 : $mes='Enero'; break;
				case 2 : $mes='Febrero'; break;
				case 3 : $mes='Marzo'; break;
				case 4 : $mes='Abril'; break;
				case 5 : $mes='Mayo'; break;
				case 6 : $mes='Junio'; break;
				case 7 : $mes='Julio'; break;
				case 8 : $mes='Agosto'; break;
				case 9 : $mes='Septiembre'; break;
				case 10 : $mes='Octubre'; break;
				case 11 : $mes='Noviembre'; break;
				case 12 : $mes='Diciembre'; break;
			}
			$data['mes']=$mes;
			$nc=$this->input->post('categoria');
			if($nc==1) $data['nc']='Maquinaria';
			if($nc==2) $data['nc']='Carcamo'; 
			if($nc==3) $data['nc']='Vehiculo';
			if($nc==4) $data['nc']='Limpieza';
			$html = $this->load->view('maqcarveh/kpimes', $data, true);  
			pdf ($html,$data['nc'].'_KPI_'.$mes.'_'.$data['cic'], true);        	
        }

        function pdfdia() {
            $this->load->model('maqcarveh_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablaevadia');
			$data['mes']=$this->input->post('dia');
			$data['cic']=$this->input->post('cicdia');
			$data['mesdia']='Día';
			$nc=$this->input->post('categorial');
			if($nc==1) $data['nc']='Maquinaria';
			if($nc==2) $data['nc']='Carcamo'; 
			if($nc==3) $data['nc']='Vehiculo';
			if($nc==4) $data['nc']='Limpieza';
			$html = $this->load->view('maqcarveh/kpimes', $data, true);  
			pdf ($html,$data['nc'].'_KPI_'.$data['mes'], true);        	
        }
		
		function agregarmcv(){
		$this->load->helper('url');
		$this->load->model('maqcarveh_model');
		$todos=$this->input->post('todos');
		$fecha=$this->input->post('fec');		
		$cic=$this->input->post('cic');
		$cat=$this->input->post('cat');
		if($todos==1){$mcv=1;$mot=1;$tra=1;$shi=1;$sus=1;$bom=1;}
		else{
			$mcv=$this->input->post('mcv');
			$mot=$this->input->post('mot');
			$tra=$this->input->post('tra');
			$shi=$this->input->post('shi');
			$sus=$this->input->post('sus');
			$cic=$this->input->post('cic');
			$cat=$this->input->post('cat');
			$bom=$this->input->post('bom');	
		}
		
		if($fecha!=''){	
			$this->maqcarveh_model->agregarmcv($fecha,$mcv,$mot,$tra,$shi,$sus,$cic,$cat,$bom,$todos);			
			redirect('maqcarveh');
		}
		}
		function actualizarmcv($id=0){
		$this->load->helper('url');
		$this->load->model('maqcarveh_model');
		$id=$this->input->post('id');
		$mot=$this->input->post('mot');
		$tra=$this->input->post('tra');
		$shi=$this->input->post('shi');
		$sus=$this->input->post('sus');
		$cic=$this->input->post('cic');
		$cat=$this->input->post('cat');
		$bom=$this->input->post('bom');
		if($id!=''){
			$return=$this->maqcarveh_model->actualizarmcv($id,$mot,$tra,$shi,$sus,$cic,$cat,$bom); 			
			redirect('maqcarveh');
		}		
		}
		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('maqcarveh_model');
			$id_post=$this->input->post('id');
			$tab=$this->input->post('tabla'); 
			$cam=$this->input->post('campo'); 
			$ciclo=$this->input->post('cic');
			if($id_post!=''){
				$return=$this->maqcarveh_model->quitar($id_post,$tab,$cam,$ciclo); 			
				redirect('aqpgranja');
			}
		}
		function buscardia(){
			$data=0;
			$fec = $this->input->post('fec');
			$cic = $this->input->post('cic');
			$cat = $this->input->post('cat');
			$data =$this->maqcarveh_model->history($fec,$cic,$cat);
			//$size=sizeof($data);
			if($data>0){
				//si lo encuentra deja los datos
				echo json_encode(array('dia'=>'1'));
			}else{
				echo json_encode(array('dia'=>'0'));
			}
						
		}	
   }
    
?>