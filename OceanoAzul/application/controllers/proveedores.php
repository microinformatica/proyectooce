<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Proveedores extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('proveedores_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('proveedores_model');
			//$data['result']=$this->proveedores_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('proveedores/lista',$data);
        }
		function pdfrep() {
            $this->load->model('proveedores_model');
			//$data['result']=$this->proveedores_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('proveedores/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('proveedores/listapdf', $data, true);  
			pdf ($html,'proveedores/listapdf', true);
        	set_paper('letter');
        }
		public function tabla($extra=0){
			        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['usuario']=$this->usuario; 
			//if($extra==0){$filter['num'] = $extra;}
			if($extra==1){$filter['where']['dom =']='';} 
			if($extra==2){$filter['where']['loc =']='';}
			if($extra==3){$filter['where']['edo =']='-';}
			if($extra==4){$filter['where']['rfc =']='';}
			if($extra==5){$filter['where']['cp =']='';}
			if($extra==6){$filter['where']['siglas =']='-';}
        	$data['rows'] = $this->proveedores_model->getUsuarios($filter);
        	$data['num_rows'] = $this->proveedores_model->getNumRows($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function combo(){        
        	$filter['id_unidad']=$this->input->post('id_unidad');           
        	$data = $this->base_model->getElements($filter);        
        	echo '('.json_encode($data).')';              
    	}
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	} 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('proveedores_model');
		$nombre=$this->input->post('nombre');
		$dom=$this->input->post('dom');
		$loc=$this->input->post('loc');
		$edo=$this->input->post('edo');
		$rfc=$this->input->post('rfc');
		$cp=$this->input->post('cp');
		$siglas=$this->input->post('siglas');
		$filter['usuario']=$this->usuario; 
		//if($nombre!='' && $dom!=''){
		if($nombre!=''){	
			$this->proveedores_model->agregar($nombre,$dom,$loc,$edo,$rfc,$cp,$siglas,$filter);
			redirect('proveedores');
		}
		
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('proveedores_model');
		$id_post=$this->input->post('id'); //id
		$nombre=$this->input->post('nombre');
		$dom=$this->input->post('dom');
		$loc=$this->input->post('loc');
		$edo=$this->input->post('edo');
		$rfc=$this->input->post('rfc');
		$cp=$this->input->post('cp');
		$siglas=$this->input->post('siglas');
		$filter['usuario']=$this->usuario; 
		//echo $id_post.$nombre.$dom;
		if($id_post!=''){
			$return=$this->proveedores_model->actualizar($id_post,$nombre,$dom,$loc,$edo,$rfc,$cp,$siglas,$filter); 
			redirect('proveedores');
		}
		if($id<=0)die("URL no valida favor de volver1");
		$datos['resultado']=$this->proveedores_model->getCliente($id);
		$tipo=gettype($datos['resultado']);
		if($tipo!='object')die('URL No valida favor de volver2');
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;			
		$this->load->view('proveedores/actualizar',$datos);
	}
		function buscar(){
		$busqueda=$this->input->post('busqueda');
		$zon=$this->input->post('zon');
		$this->load->helper('url');
		$this->load->model('proveedores_model');
		$datos['result']=$this->proveedores_model->buscar_activos($busqueda,$zon);		
		$datos['busqueda']=$busqueda;
		$datos['zon']=$zon;		
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('proveedores/buscar',$datos);
}
		
		
    }
    
?>