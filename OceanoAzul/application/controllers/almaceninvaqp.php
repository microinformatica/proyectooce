<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Almaceninv extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('almaceninv_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('almaceninv_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('almaceninv/lista',$data);
        }
		function pdfrep() {
            $this->load->model('almaceninv_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$data['tablac'] = $this->input->post('tabla');
			$data['tablapro'] = $this->input->post('tablapro');
			$data['tabladet'] = $this->input->post('tabladet');
			$data['prod'] = $this->input->post('prod');
			$cat=$this->input->post('catego');
			if($cat==0) $cat="Todos";
			if($cat==1) $cat="Alimento";
			if($cat==2) $cat="Herramientas";
			if($cat==3) $cat="Insumos Varios";
			$data['categoria'] = $cat;
			$html = $this->load->view('almaceninv/listapdf', $data, true);  
			pdf ($html,$data['prod'], true);
        }
		function reported( ) {
             
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablapro'] = $this->input->post('tablapro');
			$data['tabladet'] = $this->input->post('tabladet');
			
			$this->load->view('almaceninv/reported',$data); 
        }
		function reporte( ) {
             
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablaali');
			$data['ali'] = $this->input->post('alimento');
			$mes=$this->input->post('cmbMesA');
			if($mes=='11') $mes="Noviembre 2021";if($mes==12) $mes="Diciembre 2021";
			if($mes==1) $mes="Enero 2022";if($mes==2) $mes="Febrero 2022";if($mes==3) $mes="Marzo 2022";if($mes==4) $mes="Abril 2022";if($mes==5) $mes="Mayo 2022";
			if($mes==6) $mes="Junio 2022";if($mes==7) $mes="Julio 2022";if($mes==8) $mes="Agosto 2022";if($mes==9) $mes="Septiembre 2022";if($mes==10) $mes="Octubre 2022";
			$data['mes'] = $mes;
			$this->load->view('almaceninv/reporte',$data); 
        }
		function pdfrepmes() {
            $this->load->model('almaceninv_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablam'] = $this->input->post('tablames');
			$mes=$this->input->post('cmbMesS');
			if($mes=='11') $mes="Noviembre 2019";if($mes==12) $mes="Diciembre 2019";
			if($mes==1) $mes="Enero 2020";if($mes==2) $mes="Febrero 2020";if($mes==3) $mes="Marzo 2020";if($mes==4) $mes="Abril 2020";if($mes==5) $mes="Mayo 2020";
			if($mes==6) $mes="Junio 2020";if($mes==7) $mes="Julio 2020";if($mes==8) $mes="Agosto 2020";if($mes==9) $mes="Septiembre 2020";if($mes==10) $mes="Octubre 2019";
			
			$data['mes'] = $mes;
			$html = $this->load->view('almaceninv/listapdfmes', $data, true);  
			pdf ($html,'Mes_'.$data['mes'], true);			
        }
		function pdfrepinv() {
            $this->load->model('almaceninv_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablai'] = $this->input->post('tablainv');
			$mes=$this->input->post('cmbMesI');
			if($mes=='11') $mes="Noviembre 2021";if($mes==12) $mes="Diciembre 2021";
			if($mes==1) $mes="Enero 2022";if($mes==2) $mes="Febrero 2022";if($mes==3) $mes="Marzo 2022";if($mes==4) $mes="Abril 2022";if($mes==5) $mes="Mayo 2022";
			if($mes==6) $mes="Junio 2022";if($mes==7) $mes="Julio 2022";if($mes==8) $mes="Agosto 2022";if($mes==9) $mes="Septiembre 2022";if($mes==10) $mes="Octubre 2022";
			$data['mes'] = $mes;
			$html = $this->load->view('almaceninv/listapdfinv', $data, true);  
			pdf ($html,'Exis_Mes_'.$data['mes'], true);			
        }
        function reportedet( ) {
             
			$this->load->model('almaceninv_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablai'] = $this->input->post('tablainv');
			$mes=$this->input->post('cmbMesI');
			if($mes=='11') $mes="Noviembre 2021";if($mes==12) $mes="Diciembre 2021";
			if($mes==1) $mes="Enero 2022";if($mes==2) $mes="Febrero 2022";if($mes==3) $mes="Marzo 2022";if($mes==4) $mes="Abril 2022";if($mes==5) $mes="Mayo 2022";
			if($mes==6) $mes="Junio 2022";if($mes==7) $mes="Julio 2022";if($mes==8) $mes="Agosto 2022";if($mes==9) $mes="Septiembre 2022";if($mes==10) $mes="Octubre 2022";
			$data['mes'] = $mes;
			
			$this->load->view('almaceninv/reportedet',$data); 
        }
        function pdfrepali() {
            $this->load->model('almaceninv_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablaa'] = $this->input->post('tablaali');
			$data['ali'] = $this->input->post('alimento');
			$mes=$this->input->post('cmbMesA');
			if($mes=='11') $mes="Noviembre 2021";if($mes==12) $mes="Diciembre 2021";
			if($mes==1) $mes="Enero 2022";if($mes==2) $mes="Febrero 2022";if($mes==3) $mes="Marzo 2022";if($mes==4) $mes="Abril 2022";if($mes==5) $mes="Mayo 2022";
			if($mes==6) $mes="Junio 2022";if($mes==7) $mes="Julio 2022";if($mes==8) $mes="Agosto 2022";if($mes==9) $mes="Septiembre 2022";if($mes==10) $mes="Octubre 2022";
			$data['mes'] = $mes;
			$html = $this->load->view('almaceninv/listapdfali', $data, true); 
			pdf ($html,'Cons_Mes_'.$data['mes'], true);			
			set_paper('a4', 'landscape');
        }
		public function tabla(){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$data['rows'] = $this->almaceninv_model->getmaterialesgral($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablaent($fec=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($fec!='') $filter['where']['fece =']=$fec; 
        	$data['rows'] = $this->almaceninv_model->getentradas($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablacon($cbr=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($cbr!='') $filter['where']['CBE =']=$cbr; 
        	$data['rows'] = $this->almaceninv_model->getconsultas($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablamov($cbr=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($cbr!='') $filter['where']['CBE =']=$cbr; 
			$cod=$cbr;
        	$data['rows'] = $this->almaceninv_model->getconsultasmov($filter,$cod);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaexis($cb=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($cb!='') $filter['where']['cbe =']=$cb; 
        	$data['rows'] = $this->almaceninv_model->getexistencia($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablasal($fecs=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($fecs!='') $filter['where']['fecs =']=$fecs; 
        	$data['rows'] = $this->almaceninv_model->getsalidas($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablainv($cic=0,$mess='',$dt=0,$sa=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($mess!='') $mes='month(FecS) ='.$mess; //else $filter['where']['ndeps >']=0;
			$data['rows'] = $this->almaceninv_model->getinv($filter,$cic,$mess,$dt,$sa);
        	echo '('.json_encode($data).')';                
    	}
		public function tabladep($deptos='',$desde='',$hasta='',$insumo=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($deptos!='') $filter['where']['ndeps =']=$deptos; //else $filter['where']['ndeps >']=0;
			if($desde!='') $filter['where']['fecs >=']=$desde;
			if($hasta!='') $filter['where']['fecs <=']=$hasta;
			if($insumo!='0') $filter['where']['CBS =']=$insumo;
        	$data['rows'] = $this->almaceninv_model->getsalidasdep($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tabladepm($mess='',$dep='',$dt=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($mess!='') $filter['where']['month(FecS) =']=$mess; //else $filter['where']['ndeps >']=0;
			if($dep!='0') $filter['where']['ndeps =']=$dep;
			$data['rows'] = $this->almaceninv_model->getsalidasdepm($filter,$dt,$dep);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaali($mes='',$gra='',$sec=0,$ali=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$filter['where']['numgra =']=$gra;
			if($sec!='0') $filter['where']['secc =']=$sec;
			
			//if($mess!='') $filter['where']['month(FecS) =']=$mess; //else $filter['where']['ndeps >']=0;
			$data['rows'] = $this->almaceninv_model->getConsumos($filter,$ali,$mes);
        	echo '('.json_encode($data).')';                
    	}
		function agregarR(){
			$con=$this->input->post('con');
			$tip=$this->input->post('tip');
			$ana=$this->input->post('ana');
			if($con!=''){	
				$this->almaceninv_model->agregarR($con,$tip,$ana);			
				redirect('almaceninv');
			}
		} 
		function actualizarR($id=0){
			$this->load->helper('url');
			$this->load->model('almaceninv_model');
			$id_post=$this->input->post('id'); 
			$con=$this->input->post('con');
			$tip=$this->input->post('tip');
			$ana=$this->input->post('ana');
			if($id_post!=''){
				$return=$this->almaceninv_model->actualizarR($id_post,$con,$tip,$ana); 			
				redirect('almaceninv');
			}
		}
		
		function actualizarcb($id=0){
			$this->load->helper('url');
			$this->load->model('almaceninv_model');
			$id_post=$this->input->post('id'); 
			$nom=$this->input->post('nom');
			$ana=$this->input->post('ana');
			if($id_post!=''){
				$return=$this->almaceninv_model->actualizarcb($id_post,$nom,$ana); 			
				redirect('almaceninv');
			}
		}
		function agregarcb(){
			$cb=$this->input->post('cb'); 
			$nom=$this->input->post('nom');
			$ana=$this->input->post('ana');
			if($cb!=''){	
				$this->almaceninv_model->agregarcb($cb,$nom,$ana);			
				redirect('almaceninv');
			}
		} 

		function actualizare($id=0){
			$this->load->helper('url');
			$this->load->model('almaceninv_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$prov=$this->input->post('pro');
			$fac=$this->input->post('fac');
			$cane=$this->input->post('can');
			$pres=$this->input->post('pres');
			$pre=$this->input->post('pre');
			$dol=$this->input->post('dol');
			$tc=$this->input->post('tc');
			if($id_post!=''){
				$return=$this->almaceninv_model->actualizare($id_post,$fec,$prov,$fac,$cane,$pres,$pre,$dol,$tc); 			
				redirect('almaceninv');
			}
		}
		function agregare(){
			$cb=$this->input->post('cb'); 
			$fec=$this->input->post('fec');
			$prov=$this->input->post('pro');
			$fac=$this->input->post('fac');
			$can=$this->input->post('can');
			$pres=$this->input->post('pres');
			$pre=$this->input->post('pre');
			$dol=$this->input->post('dol');
			$tc=$this->input->post('tc');
			if($cb!=''){	
				$this->almaceninv_model->agregare($cb,$fec,$prov,$fac,$can,$pres,$pre,$dol,$tc);			
				redirect('almaceninv');
			}
		} 
		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('almaceninv_model');
			$id_post=$this->input->post('id');
			$mat=$this->input->post('mat');
			$sal=$this->input->post('can');
			//busca la existencia del material a registrar
			$rem['result'] = $this->almaceninv_model->existencia($mat);
			$exisa=0;
			foreach ($rem['result'] as $row): 
				$exisa=$row->Exis;										
			endforeach;
			$exisa+=$sal;
			if($id_post!=''){
				$return=$this->almaceninv_model->borrar($id_post,$exisa,$mat); 			
				redirect('almaceninv');
			}
		}
		function agregars(){
			$cb=$this->input->post('cb'); 
			$fec=$this->input->post('fec');
			$mat=$this->input->post('mat');
			$can=$this->input->post('can');
			$dep=$this->input->post('dep');
			//busca la existencia del material a registrar
			$rem['result'] = $this->almaceninv_model->existencia($mat);
			$exisa=0;
			foreach ($rem['result'] as $row): 
				$exisa=$row->Exis;										
			endforeach;
			if($can<=$exisa){
				if($cb!=''){	
					$this->almaceninv_model->agregars($cb,$fec,$mat,$can,$dep,$exisa);	
					echo json_encode(array('msg'=>1));		
					
				}
			}else{
				echo json_encode(array('msg'=>0));				
			}
		} 
		function actualizars(){
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$mat=$this->input->post('mat');
			$can=$this->input->post('can');$sal=$this->input->post('sal');
			$dep=$this->input->post('dep');
			//busca la existencia del material a registrar
			$rem['result'] = $this->almaceninv_model->existencia($mat);
			$exisa=0;
			foreach ($rem['result'] as $row): 
				$exisa=$row->Exis;										
			endforeach;
			//busca la cantidad que habia salido para que la existencia se actualice
			//if($can>$sal)
			 $exisa+=$sal;
			//if($can<$sal) $exisa-=$sal;
			if($can<=$exisa && $can>0){
				//if($cb!=''){	
					$this->almaceninv_model->actualizars($id_post,$fec,$mat,$can,$dep,$exisa);	
					echo json_encode(array('msg'=>1));		
					
				//}
			}else{
				if($can>=$exisa)
				echo json_encode(array('msg'=>0));
				else	echo json_encode(array('msg'=>2));			
			}
		} 
		function buscarcb(){
			$data=array();
			$cb = $this->input->post('cb');
			$data =$this->almaceninv_model->historycb($cb);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				//echo json_encode(array('numfn'=>($data->ultimo+1)));
				echo json_encode(array('des'=>$data->NomMat));
			}else{
				echo json_encode(array('des'=>0));
			}
						
		}

		public function combosecc(){
			$deptos=$this->input->post('deptos');
			$data = $this->almaceninv_model->getElementssecc($deptos); 
			echo '('.json_encode($data).')';              
    	}
    }
    
?>