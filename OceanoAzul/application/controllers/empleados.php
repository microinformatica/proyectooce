<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Empleados extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('empleados_model');         
        $this->load->library(array('ajaxsorter','session','enletras','libreria','user_agent'));		
        $this->load->helper(array('url','form','html','pdf'));
		$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		
		if($id_usuario==false)redirect('login'); 
	   }
        
        function index() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('empleados/lista',$data);
        }
		function pdfrep() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabla');
			$data['depa'] = $this->input->post('depa');
			$html = $this->load->view('empleados/listapdf', $data, true);  
			pdf ($html,'empleados/listapdf', true);
        }
		function pdfreprec() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['firma'] = $this->input->post('firmar');
			$data['puesto'] = $this->input->post('puestor');
			$data['empleado'] = $this->input->post('empleado');
			$data['per1'] = $this->input->post('per1');
			$data['per2'] = $this->input->post('per2');
			$data['depa'] = $this->input->post('depar');
			$data['fecha'] = $this->input->post('fecexp');
			if($data['depa']=='LarMod1' || $data['depa']=='LarMod2' || $data['depa']=='LarMod3' || $data['depa']=='LarMod4' || $data['depa']=='LarMod5' || $data['depa']=='LarModE1' || $data['depa']=='LarModE2')
			{$data['depa'] = 'Larvicultura'; $d='Larvicultura';}
			if($data['depa']=='Man1' || $data['depa']=='Man2'){$data['depa'] = 'Mantenimiento';$d='Mantenimiento';}
			if($data['depa']=='Mic1' || $data['depa']=='Mic2'){$data['depa'] = 'Microalgas';$d='Microalgas';}
			if($data['depa']=='Mad1' || $data['depa']=='Mad2'){$data['depa'] = 'Maduración';$d='Maduracion';}
			$data['pues'] = $this->input->post('puest');$data['sexo'] = $this->input->post('sexo');
			$html = $this->load->view('empleados/recomendacion', $data, true);  
			pdf ($html,'empleados/Recomendacion', true);
        }
		function pdfrepvac() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['firma'] = $this->input->post('firmarv');
			$data['puesto'] = $this->input->post('puestorv');
			$data['empleado'] = $this->input->post('empleadov');
			$data['dias'] = $this->input->post('per1v');
			$data['depa'] = $this->input->post('deparv');
			$data['fecha'] = $this->input->post('fecexpv');
			$data['fechaing'] = $this->input->post('fecingv');
			$data['imss'] = $this->input->post('imssv');
			if($data['depa']=='LarMod1' || $data['depa']=='LarMod2' || $data['depa']=='LarMod3' || $data['depa']=='LarMod4' || $data['depa']=='LarMod5' || $data['depa']=='LarModE1' || $data['depa']=='LarModE2')
			{$data['depa'] = 'Larvicultura'; $d='Larvicultura';}
			if($data['depa']=='Man1' || $data['depa']=='Man2'){$data['depa'] = 'Mantenimiento';$d='Mantenimiento';}
			if($data['depa']=='Mic1' || $data['depa']=='Mic2'){$data['depa'] = 'Microalgas';$d='Microalgas';}
			if($data['depa']=='Mad1' || $data['depa']=='Mad2'){$data['depa'] = 'Maduración';$d='Maduracion';}
			$data['pues'] = $this->input->post('puestv');$data['sexo'] = $this->input->post('sexo');
			$html = $this->load->view('empleados/vacaciones', $data, true);  
			pdf ($html,'empleados/Vacaciones', true);
        }
		function pdfrepbac() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['firma'] = $this->input->post('firmarb');
			$data['puesto'] = $this->input->post('puestorb');
			$data['empleado'] = $this->input->post('empleadob');
			$data['fecha'] = $this->input->post('fecexpb');
			$data['dom'] = $this->input->post('canub');
			$data['col'] = $this->input->post('colb');
			$data['ciu'] = $this->input->post('ciub');
			$data['edo'] = $this->input->post('edob');
			$data['rfc'] = $this->input->post('rfcb');
			$data['ben'] = $this->input->post('bene');
			$data['par'] = $this->input->post('paren');
			$data['ide'] = $this->input->post('identificacion');
			$html = $this->load->view('empleados/banco', $data, true);  
			pdf ($html,'empleados/TarjetaBanco', true);
        }
		function pdfrepbacp() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['firma'] = $this->input->post('firmarp');
			$data['puesto'] = $this->input->post('puestorp');
			$data['empleado'] = $this->input->post('empleadop');
			$data['fecha'] = $this->input->post('fecexpp');
			$data['imssp'] = $this->input->post('imssp');
			$data['depa'] = $this->input->post('deptop');
			if($data['depa']=='LarMod1' || $data['depa']=='LarMod2' || $data['depa']=='LarMod3' || $data['depa']=='LarMod4' || $data['depa']=='LarMod5' || $data['depa']=='LarModE1' || $data['depa']=='LarModE2')
			{$data['depa'] = 'Larvicultura'; $d='Larvicultura';}
			if($data['depa']=='Man1' || $data['depa']=='Man2'){$data['depa'] = 'Mantenimiento';$d='Mantenimiento';}
			if($data['depa']=='Mic1' || $data['depa']=='Mic2'){$data['depa'] = 'Microalgas';$d='Microalgas';}
			if($data['depa']=='Mad1' || $data['depa']=='Mad2'){$data['depa'] = 'Maduración';$d='Maduracion';}
			$data['puesp'] = $this->input->post('puesp');
			$data['ingp'] = $this->input->post('ingp');
			$data['nomp'] = $this->input->post('nomp');
			$data['viap'] = $this->input->post('viap');
			$html = $this->load->view('empleados/bancoplastico', $data, true);  
			pdf ($html,'empleados/TarjetaBancoRep', true);
        }
		function pdfrepdia() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabladps');
			//$data['dia'] = $this->input->post('pfecd');
			$data['ini'] = $this->input->post('txtFI');
			$data['fin'] = $this->input->post('txtFF');
			$html = $this->load->view('empleados/listapdfdia', $data, true);  
			pdf ($html,'empleados/MovsDia', true);
        }
		function pdfrepdet() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabladpsd');
			$data['trabajador'] = $this->input->post('nomt');
			$html = $this->load->view('empleados/listapdfece', $data, true);  
			pdf ($html,'empleados/EdoCtaEmp', true);
        }
		function pdfrepgen() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablagen');
			$html = $this->load->view('empleados/listapdfgen', $data, true);  
			pdf ($html,'empleados/SaldosEmp', true);
        }
		function pdfrepcon() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['nom'] = mb_strtoupper($this->input->post('nomb'));
			$data['dom'] = $this->input->post('domi');
			$data['imss'] = $this->input->post('imss1');
			$data['rfc'] = $this->input->post('rfc1');
			$data['curp'] = $this->input->post('curp1');
			$data['fec'] = $this->input->post('fec1');
			$data['suel'] = $this->input->post('sue1');
			$data['pues'] = $this->input->post('pue1');$p=$data['pues'];
			$data['depa'] = $this->input->post('dep1');$d=$data['depa'];
			$data['fnac'] = $this->input->post('fnac1');
			if($data['depa']=='LarMod1' || $data['depa']=='LarMod2' || $data['depa']=='LarMod3' || $data['depa']=='LarMod4' || $data['depa']=='LarMod5' || $data['depa']=='LarModE1' || $data['depa']=='LarModE2')
			{$data['depa'] = 'Larvicultura'; $d='Larvicultura';}
			if($data['depa']=='Man1' || $data['depa']=='Man2'){$data['depa'] = 'Mantenimiento';$d='Mantenimiento';}
			if($data['depa']=='Mic1' || $data['depa']=='Mic2'){$data['depa'] = 'Microalgas';$d='Microalgas';}
			if($data['depa']=='Mad1' || $data['depa']=='Mad2'){$data['depa'] = 'Maduración';$d='Maduracion';}
			
			switch ($d){
				case 'Administracion': {$tipo = '1'; $d ='Administración';break;}
				case 'Almacen': {$tipo = '2'; break;}
				case 'Cocina': {$tipo = '3'; break;}
				case 'Compras': {$tipo = '4'; break;}
				case 'Mantenimiento': {$tipo = '5'; break;}
				case 'Produccion': {$tipo = '6'; $d ='Producción';break;}
				case 'Vigilancia': {$tipo = '7'; break;}
				case 'Maternidades': {$tipo = '8'; break;}
			}
			$data['depa']=mb_strtoupper($d);
			switch ($p){
				case 'Auxiliar': {$tipo =$tipo.'1'; break;}
				case 'AuxSoldador': {$tipo = $tipo.'2'; $p ='Auxiliar Soldador';break;}
				case 'AuxMantenimiento': {$tipo = $tipo.'3'; $p ='Auxiliar Mantenimiento'; break;}
				case 'AuxMecanico': {$tipo = $tipo.'4'; $p ='Auxiliar Mecánico'; break;}
				case 'Bombero': {$tipo = $tipo.'5'; break;}
				case 'Chofer': {$tipo = $tipo.'6'; break;}
				case 'Encargado': {$tipo =$tipo.'7'; break;}
				case 'Gerente': {$tipo = $tipo.'8'; break;}
				case 'Operario': {$tipo = $tipo.'9'; break;}
				case 'Soldador': {$tipo = $tipo.'10'; break;}
				case 'Supervisor': {$tipo = $tipo.'11'; break;}
				case 'Tecnico': {$tipo = $tipo.'12'; $p ='Técnico';break;}
				case 'SupAlimento': {$tipo = $tipo.'13'; $p ='Supervisor en Alimento';break;}
				case 'SupCalidadAgua': {$tipo = $tipo.'14'; $p ='Supervisor en Calidad de Agua';break;}
				case 'TecAlimento': {$tipo = $tipo.'15'; $p ='Técnico en Alimento';break;}
				case 'TecCalidadAgua': {$tipo = $tipo.'16'; $p ='Técnico en Calidad de Agua';break;}
				case 'Vigilante': {$tipo = $tipo.'17'; break;}
				case 'Mecanico': {$tipo = $tipo.'18'; break;}
			}
			$data['pues']=mb_strtoupper($p);
			$data['tip'] =$tipo;
			$sexo = $this->input->post('sexo1');
			switch ($sexo){
				case '1': {$data['sexo'] ='hombre'; break;}
				case '2': {$data['sexo'] ='mujer'; break;}
			}
			$civil=$this->input->post('civil1');
			switch ($civil){
				case '1': {$data['civil'] ='Soltero'; break;}
				case '2': {$data['civil'] ='Casado'; break;}
			}
			
			$html = $this->load->view('empleados/listapdfcon', $data, true);  
			pdf ($html,'empleados/contrato', true);
        }
		function pdfrepcre() {
            $this->load->model('empleados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['empl'] = $this->input->post('nemp').'.jpg';
			$data['firemp'] = $this->input->post('nemp').'f.jpg';
			$data['nom'] = $this->input->post('nomc');
			$data['dom'] = $this->input->post('domc');
			$data['imss'] = $this->input->post('imss2');
			$data['rfc'] = $this->input->post('rfc2');
			$data['fot'] = $this->input->post('rbFoto');
			$data['curp'] = $this->input->post('curp2');
			$data['pues'] = $this->input->post('pue2');$p=$data['pues'];
			$data['depa'] = $this->input->post('dep2');$d=$data['depa'];
			if($data['depa']=='LarMod1' || $data['depa']=='LarMod2' || $data['depa']=='LarMod3' || $data['depa']=='LarMod4' || $data['depa']=='LarMod5' || $data['depa']=='LarModE1' || $data['depa']=='LarModE2')
			{$data['depa'] = 'Larvicultura'; $d='Larvicultura';}
			if($data['depa']=='Man1' || $data['depa']=='Man2'){$data['depa'] = 'Mantenimiento';$d='Mantenimiento';}
			if($data['depa']=='Mic1' || $data['depa']=='Mic2'){$data['depa'] = 'Microalgas';$d='Microalgas';}
			if($data['depa']=='Mad1' || $data['depa']=='Mad2'){$data['depa'] = 'Maduración';$d='Maduracion';}
			switch ($d){
				case 'Administracion': {$tipo = '1'; $d ='Administración';break;}
				case 'Almacen': {$tipo = '2'; break;}
				case 'Artemia': {$tipo = '3'; break;}
				case 'Cocina': {$tipo = '4'; break;}
				case 'ControlCalidad': {$tipo = '5'; break;}
				case 'Cosecha': {$tipo = '6'; break;}
				case 'Genetica': {$tipo = '7'; $d ='Genética';break;}
				case 'Larvicultura': {$tipo = '8'; break;}
				case 'Maduracion': {$tipo = '9';break;}
				case 'Mantenimiento': {$tipo = '10'; break;}
				case 'Microalgas': {$tipo = '11'; break;}
				case 'Transporte': {$tipo = '12'; break;}
				case 'Vigilancia': {$tipo = '13'; break;}
				case 'Produccion': {$tipo = '14'; $d ='Producción';break;}
				case 'Reservas': {$tipo = '15'; break;}
				case 'Sistemas': {$tipo = '1'; break;}
			}
			//$data['depa']=mb_strtoupper($d);
			switch ($p){
				case 'Ayudante': {$tipo =$tipo.'1'; break;}
				case 'Tecnico': {$tipo = $tipo.'2'; $p ='Técnico';break;}
				case 'Encargado': {$tipo =$tipo.'3'; break;}
				case 'Auxiliar': {$tipo = $tipo.'4'; break;}
				case 'Chofer': {$tipo = $tipo.'5'; break;}
				case 'Vigilante': {$tipo = $tipo.'6'; break;}
				case 'Biologo': {$tipo = $tipo.'2'; $p ='Biólogo';break;}
			}
			//$data['pues']=mb_strtoupper($p);
			$data['tip'] =$tipo;
			$html = $this->load->view('empleados/listapdfcre', $data, true);  
			pdf ($html,'Credencial_'.$this->input->post('nemp'), true);
        }
		public function tabla($activos=0,$deptos='Todos',$extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($extra==1){$filter['where']['ap =']='';} 
			if($extra==2){$filter['where']['am =']='';}
			if($extra==3){$filter['where']['curp =']='';}
			if($extra==4){$filter['where']['rfc =']='';}
			if($extra==5){$filter['where']['imss =']='';}
			if($extra==6){$filter['where']['mail =']='';}
			if($extra==7){$filter['where']['canu =']='';}
			if($deptos!='Todos'){$filter['where']['depto =']=$deptos;
								 $filter['order']='puesto,fecing';
			}
			if($activos!=2){$filter['where']['activo =']=$activos;}
			//else $filter['where']['activo =']=$activos;
        	$data['rows'] = $this->empleados_model->getUsuarios($filter);
        	$data['num_rows'] = $this->empleados_model->getNumRows($filter);
			echo '('.json_encode($data).')';                
    	}
		public function tablapre($desde='',$hasta=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($extra!=0){$filter['where']['pfec']=$extra;}
			if($desde!='') $filter['where']['pfec >=']=$desde;
			if($hasta!='') $filter['where']['pfec <=']=$hasta; 
			$data['rows'] = $this->empleados_model->getPrestamos($filter);
        	$data['num_rows'] = $this->empleados_model->getNumRowsP($filter);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaE($cancelacion=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($cancelacion==-1){$filter['where']['pcancel']=0;}
			$data['rows'] = $this->empleados_model->getEmpleados($filter);
        	$data['num_rows'] = $this->empleados_model->getNumRowsE($filter);
        	echo '('.json_encode($data).')';                
    	}		
		public function tablaSD($extra=0,$cancelacion=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($extra!=0){$filter['where']['pide']=$extra;}
			if($cancelacion==-1){$filter['where']['pcancel']=0;}
			$data['rows'] = $this->empleados_model->getEmpleadosEDO($filter);
        	$data['num_rows'] = $this->empleados_model->getNumRowsEDO($filter);
        	echo '('.json_encode($data).')';                
    	} 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('empleados_model');
		$ide=$this->input->post('ide');
		$nombre=$this->input->post('nombre');
		$ap=$this->input->post('ap');
		$am=$this->input->post('am');
		$canu=$this->input->post('canu');
		$col=$this->input->post('col');
		$ciu=$this->input->post('ciu');
		$edo=$this->input->post('edo');
		$cp=$this->input->post('cp');
		$tel=$this->input->post('tel');
		$curp=$this->input->post('curp');
		$rfc=$this->input->post('rfc');
		$imss=$this->input->post('imss');
		$mail=$this->input->post('mail');
		$depto=$this->input->post('depto');
		$pues=$this->input->post('pues');
		$fec=$this->input->post('fec');
		$suel=$this->input->post('suel');
		$nac=$this->input->post('fnac');
		$cta=$this->input->post('ctad');
		$ctav=$this->input->post('ctav');
		$sex=$this->input->post('sex');
		$civ=$this->input->post('civ');
		$this->db->where('ide',$ide);
		$query=$this->db->get('empleados');
		if($query->num_rows()>0){
			$msg='1';	
		}else{
			$msg='0';	
		}
		if($ide!=0 && $msg=='0'){	
			$this->empleados_model->agregar($ide,$nombre,$ap,$am,$canu,$col,$ciu,$edo,$cp,$tel,$curp,$rfc,$imss,$mail,$depto,$pues,$fec,$suel,$nac,$cta,$ctav,$sex,$civ);
			echo json_encode(array('msg'=>$msg));
		}else{
			echo json_encode(array('msg'=>$msg));
		}		
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('empleados_model');
		$id_post=$this->input->post('id'); 
		$nombre=$this->input->post('nombre');
		$ap=$this->input->post('ap');
		$am=$this->input->post('am');
		$veri=$this->input->post('veri');
		$canu=$this->input->post('canu');
		$col=$this->input->post('col');
		$ciu=$this->input->post('ciu');
		$edo=$this->input->post('edo');
		$cp=$this->input->post('cp');
		$tel=$this->input->post('tel');
		$curp=$this->input->post('curp');
		$rfc=$this->input->post('rfc');
		$imss=$this->input->post('imss');
		$mail=$this->input->post('mail');
		$depto=$this->input->post('depto');
		$pues=$this->input->post('pues');
		$fec=$this->input->post('fec');
		$suel=$this->input->post('suel');
		$nac=$this->input->post('fnac');
		$cta=$this->input->post('ctad');
		$ctav=$this->input->post('ctav');
		$sex= $this->input->post('sex');
		$civ=$this->input->post('civ');
		if($id_post!=''){
			$return=$this->empleados_model->actualizar($id_post,$nombre,$ap,$am,$veri,$canu,$col,$ciu,$edo,$cp,$tel,$curp,$rfc,$imss,$mail,$depto,$pues,$fec,$suel,$nac,$cta,$ctav,$sex,$civ); 
			redirect('empleados');
		}		
	}
	function pactualizar($id=0){
		$this->load->helper('url');
		$this->load->model('empleados_model');
		$id_post=$this->input->post('pidp'); 
		$pfec=$this->input->post('pfec');
		$pimp=$this->input->post('pimp');
		$pide=$this->input->post('pide');
		$pca=$this->input->post('pca');
		$pobs=$this->input->post('pobs');
		if($id_post!=''){
			$return=$this->empleados_model->pactualizar($id_post,$pfec,$pimp,$pide,$pca,$pobs); 
			redirect('empleados');
		}		
	}
	function pagregar(){
		$this->load->model('empleados_model');		
		$pfec=$this->input->post('pfec');
		$pimp=$this->input->post('pimp');
		$pide=$this->input->post('pide');
		$pca=$this->input->post('pca');
		$pobs=$this->input->post('pobs');
			if($pimp!=''){	
				$this->empleados_model->pagregar($pfec,$pimp,$pide,$pca,$pobs);			
				redirect('empleados');
			}
		} 
	function pborrar($id=0){
			$this->load->helper('url');
			$this->load->model('empleados_model');
			$id_post=$this->input->post('id');
			if($id_post!=''){
				$return=$this->empleados_model->pborrar($id_post); 			
				redirect('empleados');
			}
	}
	function cancelar($id=0){
			$this->load->helper('url');
			$this->load->model('empleados_model');
			$id_post=$this->input->post('id');
			$valor=$this->input->post('pcancel');
			if($id_post!=''){
				$return=$this->empleados_model->cancelar($id_post,$valor); 			
				redirect('empleados');
			}
	}
   }
    
?>