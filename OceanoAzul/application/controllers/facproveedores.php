<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Facproveedores extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('facproveedores_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('facproveedores_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;		
			$this->load->view('facproveedores/lista',$data);
        }
		function pdfrep( ) {
            $this->load->model('facproveedores_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('facproveedores/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('facproveedores/listapdf', $data, true);  
			pdf ($html,'facproveedores/listapdf', true);
        	set_paper('letter');
        }
		function reporte( ) {
            $data['tablac'] = $this->input->post('tabla');        
        	$this->load->view('facproveedores/reporte',$data); 
        }
		function tabla($ciclo=0,$mes=0,$est=0,$dia=0){        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['num'] = $ciclo; 		
			if($dia!=0){$filter['where']['fcap']=$dia;}
			if($mes!=0){$filter['where']['aplicar']=$mes;}	
			$data['rows'] = $this->facproveedores_model->getfacproveedores($filter,$est);
        	$data['num_rows'] = $this->facproveedores_model->getNumRowsreq($filter,$est);		
			echo '('.json_encode($data).')'; 
    	}
		public function tablaclientes($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
        	$filter['num'] = $extra; 
        	$data['rows'] = $this->facproveedores_model->getClientes($filter);
        	$data['num_rows'] = $this->facproveedores_model->getNumClientes($filter);
        	echo '('.json_encode($data).')';                
    	}
		function agregar(){
			$this->load->model('facproveedores_model');		
			$fec=$this->input->post('fec');
			$bas=$this->input->post('bas');
			$mov=$this->input->post('mov');
			$nrp=$this->input->post('nrp');
			$t16=$this->input->post('t16');
			$t11=$this->input->post('t11');
			$t0=$this->input->post('t0');
			$exc=$this->input->post('exc');
			$iva=$this->input->post('iva');
			$isr=$this->input->post('isr');
			$riva=$this->input->post('riva');
			$fac=$this->input->post('fac');
			$pol=$this->input->post('pol');
			$ciclo=$this->input->post('ciclo');
			$aplicar=$this->input->post('aplicar');
			if($fec!=''){	
				$this->facproveedores_model->agregar($fec,$bas,$mov,$nrp,$t16,$t11,$t0,$exc,$iva,$isr,$riva,$fac,$pol,$ciclo,$aplicar);			
				redirect('facproveedores');
			}
		} 
		function actualizar($id=0){
			$this->load->model('facproveedores_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$bas=$this->input->post('bas');
			$mov=$this->input->post('mov');
			$nrp=$this->input->post('nrp');
			$t16=$this->input->post('t16');
			$t11=$this->input->post('t11');
			$t0=$this->input->post('t0');
			$exc=$this->input->post('exc');
			$iva=$this->input->post('iva');
			$isr=$this->input->post('isr');
			$riva=$this->input->post('riva');
			$fac=$this->input->post('fac');
			$pol=$this->input->post('pol');
			$ciclo=$this->input->post('ciclo');
			$aplicar=$this->input->post('aplicar');
			if($id_post!=''){
				$return=$this->facproveedores_model->actualizar($id_post,$fec,$bas,$mov,$nrp,$t16,$t11,$t0,$exc,$iva,$isr,$riva,$fac,$pol,$ciclo,$aplicar); 			
				redirect('facproveedores');
			}
		}
		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('facproveedores_model');
			$id_post=$this->input->post('id');
			$ciclo=$this->input->post('ciclo');
			if($id_post!=''){
				$return=$this->facproveedores_model->borrar($id_post,$ciclo); 			
				redirect('facproveedores');
			}
		}
	}
 ?>