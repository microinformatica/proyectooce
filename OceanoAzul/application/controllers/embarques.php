<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Embarques extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('embarques_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('embarques_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;		
			$this->load->view('embarques/lista',$data);
        }
		public function tablamesC($ciclo=0,$idcte=0,$mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 			
			$ciclo='prog_'.$ciclo;
			$data['rows'] = $this->embarques_model->mesC($filter,$ciclo,$idcte,$mes);
			echo '('.json_encode($data).')'; 
    	}
		//programa diario
		public function tablaprograma($ciclo=0,$dia='',$ori='Todos'){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			$filter['where']['fecpd =']=$dia;
			$ciclo='prog_'.$ciclo;
			if($ori!='Todos'){ $filter['where']['tipopl =']=$ori;}
			$data['rows'] = $this->embarques_model->getProgramadia($filter,$ciclo,$ori);
        	$data['num_rows'] = $this->embarques_model->getNumRowsPd($filter,$ciclo);
        	echo '('.json_encode($data).')'; 
    	}
		public function tablaprogramad($ciclo=0,$dia=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			$filter['where']['fecpd =']=$dia;
			$ciclo='prog_'.$ciclo;
			$data['rows'] = $this->embarques_model->getProgramadiad($filter,$ciclo);
        	echo '('.json_encode($data).')'; 
    	}
		public function tablacli(){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			$data['rows'] = $this->embarques_model->getClientes($filter);
        	$data['num_rows'] = $this->embarques_model->getNumRowsC($filter);
        	echo '('.json_encode($data).')'; 
    	}
		
		function borrara($id=0){
		$this->load->helper('url');
		$this->load->model('embarques_model');
		$id_post=$this->input->post('id');
		$cic='prog_'.$this->input->post('cic'); 
		if($id_post!=''){
			$return=$this->embarques_model->borrara($id_post,$cic); 			
			redirect('embarques');
		}
		}
		
		function agregara(){
		$this->load->helper('url');
		$this->load->model('embarques_model');		
		$fec=$this->input->post('fec');
		$idcte=$this->input->post('idcte');
		$can=$this->input->post('can');
		$por=$this->input->post('por');
		$obs=''; //$obs=$this->input->post('obs');
		$tip=$this->input->post('tip');
		$cic='prog_'.$this->input->post('cic');
		if($fec!=''){	
			$this->embarques_model->agregara($fec,$idcte,$can,$por,$obs,$tip,$cic);			
			redirect('embarques');
		}
		}
		function actualizara($id=0){
		$this->load->helper('url');
		$this->load->model('embarques_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$idcte=$this->input->post('idcte');
		$can=$this->input->post('can');
		$por=$this->input->post('por');
		$obs=''; //$obs=$this->input->post('obs');
		$tip=$this->input->post('tip');
		$cic='prog_'.$this->input->post('cic');
		$hrs=$this->input->post('hrs');
		$ela=$this->input->post('ela');
		$uni=$this->input->post('uni');
		$cho=$this->input->post('cho');
		$tec=$this->input->post('tec');
		$pil=$this->input->post('pil');
		if($id_post!=''){
			$return=$this->embarques_model->actualizara($id_post,$fec,$idcte,$can,$por,$obs,$tip,$cic,$hrs,$ela,$uni,$cho,$tec,$pil); 			
			redirect('embarques');
		}
		}
		
		function tablaprogmes($ciclo=0,$mes=0,$tip=''){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($ciclo>0) $filter['where']['year(fecpd) =']=$ciclo;
			if($mes>0) $filter['where']['month(fecpd) =']=$mes;
			if($tip!='') $filter['where']['tipopl =']=$tip;
			//$ciclo='prog_'.$ciclo;
			$data['rows'] = $this->embarques_model->getMesProg($filter,$ciclo,$tip,$mes);
        	//$data['num_rows'] = $this->embarques_model->getNumRowsTGral($filter);
        	
        	echo '('.json_encode($data).')'; 
    	}
		
		//Salas
		//programa diario
		public function tablasalas($ciclo=0,$ori='Todos'){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$usuario=$this->usuario;
			$ciclo='progs_'.$ciclo;
			//$filter['where']['fect =']=$dia;
			if($ori!='Todos'){ $filter['where']['origens =']=$ori;}
			$data['rows'] = $this->embarques_model->getProgramadiaS($filter,$ciclo,$ori);
        	$data['num_rowss'] = $this->embarques_model->getNumRowsS($filter,$ciclo);
        	echo '('.json_encode($data).')'; 
    	}
		function borraras($id=0){
		$this->load->helper('url');
		$this->load->model('embarques_model');
		$id_post=$this->input->post('id');
		$cic='progs_'.$this->input->post('cic'); 
		if($id_post!=''){
			$return=$this->embarques_model->borraras($id_post,$cic); 			
			redirect('embarques');
		}
		}
		
		function agregaras(){
		$this->load->helper('url');
		$this->load->model('embarques_model');		
		$fecs=$this->input->post('fecs');
		$fect=$this->input->post('fect');
		$can=$this->input->post('can');
		$cor=$this->input->post('cor');
		$sala=$this->input->post('sala');
		$ori=$this->input->post('ori');
		$cic='progs_'.$this->input->post('cic');
		if($fecs!=''){	
			$this->embarques_model->agregaras($fecs,$fect,$can,$cor,$sala,$ori,$cic);			
			redirect('embarques');
		}
		}
		function actualizaras($id=0){
		$this->load->helper('url');
		$this->load->model('embarques_model');
		$id_post=$this->input->post('id'); 
		$fecs=$this->input->post('fecs');
		$fect=$this->input->post('fect');
		$can=$this->input->post('can');
		$cor=$this->input->post('cor');
		$sala=$this->input->post('sala');
		$ori=$this->input->post('ori');
		$cic='progs_'.$this->input->post('cic');
		if($id_post!=''){
			$return=$this->embarques_model->actualizaras($id_post,$fecs,$fect,$can,$cor,$sala,$ori,$cic); 			
			redirect('embarques');
		}
		}
		//imprimir programa dia
		function pdfrepdetdia( ) {
            $this->load->model('embarques_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['cic'] = '['.$this->input->post('Ciclo').']';
			$data['fecha'] =$this->input->post('fecpd'); 
			$data['tip'] ='['.$this->input->post('Origen').']';
			$data['tabla'] = $this->input->post('tabladia');
			$html = $this->load->view('embarques/programadia', $data, true);  
			pdf ($html,'embarques/ProgDia', true);
        }
		//imprimir programa mensual
		function pdfrepdetmes( ) {
            $this->load->model('embarques_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('tablero/lista',$data);
			$data['cic'] = '['.$this->input->post('cmbCiclo').']';
			$mes = $this->input->post('cmbMesT');
			switch ($mes){
					case '1': $mes='[Enero]';break;
					case '2': $mes='[Febrero]';break;
					case '3': $mes='[Marzo]';break;
					case '4': $mes='[Abril]';break;
					case '5': $mes='[Mayo]';break;
					case '6': $mes='[Junio]';break;
					case '7': $mes='[Julio]';break;
					case '8': $mes='[Agosto]';break;
					case '9': $mes='[Septiembre]';break;
					case '10': $mes='[Octubre]';break;
					case '11': $mes='[Noviembre]';break;
					case '12': $mes='[Diciembre]';break;
				}
			$data['mes'] = $mes;
			$data['tip'] ='['.$this->input->post('OrigenMes').']';
			$data['tabla'] = $this->input->post('tabla');
			$html = $this->load->view('embarques/programames', $data, true);  
			pdf ($html,'embarques/ProgMes', true);
        }
		//mandar a excel
		public function excelmes(){
			$mes = $this->input->post('cmbMesT');
			switch ($mes){
					case '1': $mes='[Enero]';break;
					case '2': $mes='[Febrero]';break;
					case '3': $mes='[Marzo]';break;
					case '4': $mes='[Abril]';break;
					case '5': $mes='[Mayo]';break;
					case '6': $mes='[Junio]';break;
					case '7': $mes='[Julio]';break;
					case '8': $mes='[Agosto]';break;
					case '9': $mes='[Septiembre]';break;
					case '10': $mes='[Octubre]';break;
					case '11': $mes='[Noviembre]';break;
					case '12': $mes='[Diciembre]';break;
				}
			$data['mes'] = $mes;
        	$data['tabla'] = $this->input->post('tablae');        
        	$this->load->view('embarques/progentmes',$data);        
    	}
		//imprimir PROYECCION DE SALAS
		function pdfrepdetsal( ) {
            $this->load->model('embarques_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('tablero/lista',$data);
			$data['cic'] = '['.$this->input->post('CicloS').']';
			$data['tip'] ='['.$this->input->post('origens').']';
			$data['tabla'] = $this->input->post('tablasala');
			$html = $this->load->view('embarques/proyeccionsalas', $data, true);  
			pdf ($html,'embarques/ProySala', true);
        }
		//corte
		function buscarcd($fec=0){
		$this->load->helper('url');
		$this->load->model('embarques_model');
		$fec=$this->input->post('fec');
		$ori=$this->input->post('tip');
		$cic='progcd_'.$this->input->post('cic');
		if($fec!=''){
			$row=$this->embarques_model->buscarcd($fec,$ori,$cic);
			$size=sizeof($row);
			if($size>0){
				$can=$row->cancd;$id=$row->idcd;
			}else{
				$can='';$id='';
			}
			echo json_encode(array('can'=>$can,'id'=>$id));
		}
		}
		function quitarcor($id=0){
		$this->load->helper('url');
		$this->load->model('embarques_model');
		$id_post=$this->input->post('id');
		$cic='progcd_'.$this->input->post('cic'); 
		if($id_post!=''){
			$return=$this->embarques_model->quitarcor($id_post,$cic); 			
			redirect('embarques');
		}
		}
		function actualizarcor($id=0){
		$this->load->helper('url');
		$this->load->model('embarques_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$can=$this->input->post('can');
		$cic='progcd_'.$this->input->post('cic');
		if($id_post!=''){
			$return=$this->embarques_model->actualizarcor($id_post,$fec,$can,$cic); 			
			redirect('embarques');
		}
		}
		function agregarcor(){
		$this->load->helper('url');
		$this->load->model('embarques_model');		
		$fec=$this->input->post('fec');
		$can=$this->input->post('can');
		$cic='progcd_'.$this->input->post('cic');
		$tip=$this->input->post('tip');
		if($fec!=''){	
			$this->embarques_model->agregarcor($fec,$can,$cic,$tip);			
			redirect('embarques');
		}
		}
		
    }
