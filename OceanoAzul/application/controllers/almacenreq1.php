<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Almacenreq extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('almacenreq_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('almacenreq_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;		
			$this->load->view('almacenreq/lista',$data);
        }
		function pdfrep( ) {
            $this->load->model('almacenreq_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('almacenreq/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$req = $this->input->post('requisicion');
			$data['req'] = $this->almacenreq_model->verNomReq($req);
			$html = $this->load->view('almacenreq/listapdf', $data, true);  
			pdf ($html,'almacenreq/listapdf', true);
        	set_paper('letter');
        }
		function tabla($req=''){        
        	$filter = $this->ajaxsorter->filter($this->input);		
			$filter['where']['nrs']=$req;	
			$data['rows'] = $this->almacenreq_model->getalmacenreq($filter);
        	$data['num_rows'] = $this->almacenreq_model->getNumRowsreq($filter);		
			echo '('.json_encode($data).')'; 
    	}
		function agregar(){
			$this->load->model('almacenreq_model');		
			$can=$this->input->post('can');
			$uni=$this->input->post('uni');
			$des=$this->input->post('des');
			$obs=$this->input->post('obs');
			$req=$this->input->post('req');
			if($can!=''){	
				$this->almacenreq_model->agregar($can,$uni,$des,$obs,$req);			
				redirect('almacenreq');
			}
		} 
		function actualizar($id=0){
			$this->load->model('almacenreq_model');
			$id_post=$this->input->post('id'); 
			$can=$this->input->post('can');
			$uni=$this->input->post('uni');
			$des=$this->input->post('des');
			$obs=$this->input->post('obs');
			$cans=$this->input->post('cans');
			if($id_post!=''){
				$return=$this->almacenreq_model->actualizar($id_post,$can,$uni,$des,$obs,$cans); 			
				redirect('almacenreq');
			}
		}
		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('almacenreq_model');
			$id_post=$this->input->post('id');
			if($id_post!=''){
				$return=$this->almacenreq_model->borrar($id_post); 			
				redirect('almacenreq');
			}
		}
		function agregarreq(){
			$this->load->helper('url');
			$this->load->model('almacenreq_model');
			$return=$this->almacenreq_model->nueRequisicion(); 			
			redirect('almacenreq');
		}
	}
 ?>