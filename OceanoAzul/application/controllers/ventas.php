<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Ventas extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('ventas_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('ventas_model');
			//$data['result']=$this->ventas_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ventas/lista',$data);
        }
		
		public function tablavtas($cic=0,$gra=0){
			$filter = $this->ajaxsorter->filter($this->input);	
			if($gra>0) $filter['where']['numgrab =']=$gra;
			$data['rows'] = $this->ventas_model->bordogral($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}
		
		function pdfrepcosb( ) {
            $this->load->model('ventas_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ventas/lista',$data);
			$data['tabla'] = $this->input->post('tabla');
			$data['gra'] = $this->input->post('gra');
			$dia=date("d-m-Y");
			$data['dia'] =date("Y-m-d");
			$html = $this->load->view('ventas/listapdfdiab', $data, true);  
			pdf ($html,'Ventas_'.$dia, true);
			         	
        }
		public function tablag($cic=0,$gra=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->ventas_model->getVentasg($gra,$cic);
        	echo '('.json_encode($data).')';                
    	}
		function pdfrepvta() {
            $this->load->model('ventas_model');
			$dia=date('d-m-Y');
			$data['dia'] = date('d-m-Y');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ventas/lista',$data);
			$data['tablavtas'] = $this->input->post('tablav');
			$html = $this->load->view('ventas/listapdf', $data, true);  
			pdf ($html,'gral_ventas_'.$dia, true);   
        }
		function pdfrepvtasg() {
            $this->load->model('ventas_model');
			$dia=date('d-m-Y');
			$data['dia'] = date('d-m-Y');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('ventas/lista',$data);
			$data['tablag'] = $this->input->post('tabla');
			$html = $this->load->view('ventas/listapdfvtag', $data, true);  
			pdf ($html,'Granja_ventas_'.$dia, true);        	
        }
		public function combo(){        
        	$filter['id_unidad']=$this->input->post('id_unidad');           
        	$data = $this->base_model->getElements($filter);        
        	echo '('.json_encode($data).')';              
    	}
    	public function reporter(){
        	$campos = $this->base_model->obtenerNombreCampo();        
        	$data = $this->reporterCreator($campos);
        	$this->load->view('lista',$data);
    	} 
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('ventas_model');
		$id_post=$this->input->post('id'); //id
		$fac=$this->input->post('fac');
		$avi=$this->input->post('avi');
		$cic=$this->input->post('cic');
		if($id_post!=''){
			$return=$this->ventas_model->actualizar($id_post,$fac,$avi,$cic); 
			redirect('ventas');
		}		
		}
		function buscar(){
		$busqueda=$this->input->post('busqueda');
		$zon=$this->input->post('zon');
		$this->load->helper('url');
		$this->load->model('ventas_model');
		$datos['result']=$this->ventas_model->buscar_activos($busqueda,$zon);		
		$datos['busqueda']=$busqueda;
		$datos['zon']=$zon;		
		$datos['usuario']=$this->usuario;
		$datos['perfil']=$this->perfil;
		$this->load->view('ventas/buscar',$datos);
}
		function borrara($id=0){
		$this->load->helper('url');
		$this->load->model('ventas_model');
		$id_post=$this->input->post('id');
		if($id_post!=''){
			$return=$this->ventas_model->borrara($id_post); 			
			redirect('programa');
		}
		}
		
    }
    
?>