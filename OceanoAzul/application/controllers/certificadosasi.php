<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Certificadosasi extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('certificadosasi_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('certificadosasi_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;		
			$this->load->view('certificadosasi/lista',$data);
        }
		function pdfrep( ) {
            $this->load->model('certificadosasi_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('certificadosasi/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('certificadosasi/listapdf', $data, true);  
			pdf ($html,'certificadosasi/listapdf', true);
        	set_paper('letter');
        }
		public function tabla($cer=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			//$filter['where']['numcerr =']=$cer;
			$data['rows'] = $this->certificadosasi_model->getcertificadosasi($filter);
        	$data['num_rows'] = $this->certificadosasi_model->getNumRowsasi($filter);		
			echo '('.json_encode($data).')'; 
    	}
		public function tablac(){        
        	$filter = $this->ajaxsorter->filter($this->input);
			$data['rows'] = $this->certificadosasi_model->getcertificadosasic($filter);
        	$data['num_rows'] = $this->certificadosasi_model->getNumRowsasic($filter);		
			echo '('.json_encode($data).')'; 
    	}
		function actualizar($id=0){
			$this->load->model('certificadosasi_model');
			$id_post=$this->input->post('id'); 
			$cer=$this->input->post('cer');
			if($id_post!=''){
				$return=$this->certificadosasi_model->actualizar($id_post,$cer); 			
				redirect('certificadosasi');
			}
		}
		function actualizarg(){
			$this->load->model('certificadosasi_model');
			$cer=$this->input->post('cer');
			$ini=$this->input->post('ini');
			$fin=$this->input->post('fin');
			if($cer!=''){
				$return=$this->certificadosasi_model->actualizarg($cer,$ini,$fin); 			
				redirect('certificadosasi');
			}
		}	
    }
    
?>