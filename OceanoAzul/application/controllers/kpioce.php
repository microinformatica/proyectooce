<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Kpioce extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('kpioce_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('kpioce_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('kpioce/lista',$data);
        }
		
		public function tablakpi($cic='',$dia='',$lot=0,$sec=''){
			$filter = $this->ajaxsorter->filter($this->input);
			/*
			if($sec!=0){
				if($sec==41) {$filter['where']['idpis >=']=73; $filter['where']['idpis <=']=91;}
				if($sec==42) {$filter['where']['idpis >=']=92; $filter['where']['idpis <=']=207;}
				//if($sec==43) {$filter['where']['idpis >=']=100; $filter['where']['idpis <=']=111;$filter['where'][' idpis >=']=129; $filter['where']['idpis <=']=149;}
				//if($sec==44) {$filter['where']['idpis >=']=129; $filter['where']['idpis <=']=149;}
			} 
			 * */
			 if($lot!=0){
			 	$filter['where']['lote']=$lot;
			 }
			 if($sec!=0){
			 	$filter['where']['secc']=$sec;
			 }	
			$data['rows'] = $this->kpioce_model->kpioce($filter,$cic,$dia,$sec);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tabladiasd($cic='',$dia='',$lot='',$sec=''){
			$filter = $this->ajaxsorter->filter($this->input);
			/*if($sec!=0){
				if($sec==41) {$filter['where']['idpis >=']=73; $filter['where']['idpis <=']=91;}
				if($sec==42) {$filter['where']['idpis >=']=92; $filter['where']['idpis <=']=207;}
				//if($sec==43) {$filter['where']['idpis >=']=100; $filter['where']['idpis <=']=111;$filter['where'][' idpis >=']=129; $filter['where']['idpis <=']=149;}
				//if($sec==44) {$filter['where']['idpis >=']=129; $filter['where']['idpis <=']=149;}
			} */ 
			if($lot!=0){
			 	$filter['where']['lote']=$lot;
			 }
			if($sec!=0){
			 	$filter['where']['secc']=$sec;
			 }
			$data['rows'] = $this->kpioce_model->diasd($filter,$dia,$sec,$cic);
        	echo '('.json_encode($data).')';                
    	}
		
		function pdfrepgral() {
            $this->load->model('kpioce_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablainddia');
			$data['dia'] = $this->input->post('dia');
			$html = $this->load->view('kpioce/indicadores', $data, true);  
			pdf ($html,'Indicadores_dia_'.$data['dia'], true);        	
        }
				
    }
    
?>