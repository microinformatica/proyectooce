<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Aligranja extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('aligranja_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('aligranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('aligranja/lista',$data);
        }
		function pdfsem() {
            $this->load->model('aligranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablasem'] = $this->input->post('tablasem');
			$data['dia']=$this->input->post('fecchat');
			$data['ng']='AOA S.A. de C.V. - Ahome';
			$html = $this->load->view('aligranja/alimentosem', $data, true);  
			pdf ($html,$data['ng'].'_AliSem_'.$data['dia'], true);        	
        }
		function pdfdiacha() {
            $this->load->model('aligranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablacha');
			$data['dia']=$this->input->post('diaali');
			$ng=$this->input->post('granjaa');
			if($ng==1) $data['ng']='Santa Fe';
			if($ng==2) $data['ng']='Gran Kino'; 
			if($ng==3) $data['ng']='Jazmin';
			if($ng==4) $data['ng']='AOA S.A. de C.V. - Ahome';
			$data['fec1'] = $this->input->post('txtFIAli');
			$html = $this->load->view('aligranja/alimentos', $data, true);  
			pdf ($html,$data['ng'].'_Alimento_'.$data['dia'], true);        	
        }
		function pdfalimentosoy() {
            $this->load->model('aligranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablas1'] = $this->input->post('html_soys1');
			$data['tablas2'] = $this->input->post('html_soys2');
			$data['tablas3'] = $this->input->post('html_soys3');
			$data['tablas4'] = $this->input->post('html_soys4');
			$data['dia']=$this->input->post('diaalipdfsoy');
			$data['kgst']=$this->input->post('kgstsoya');
			$data['kgsha']=$this->input->post('kgshafsoy');
			$data['fm']='Soya';
			$html = $this->load->view('aligranja/alidiahora', $data, true);  
			pdf ($html,'Ali_Dia_Soya_'.$data['dia'], true);
			set_paper('letter');
        }
		function pdfalimento() {
            $this->load->model('aligranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablas1'] = $this->input->post('html_alis1');
			$data['tablas2'] = $this->input->post('html_alis2');
			$data['tablas3'] = $this->input->post('html_alis3');
			$data['tablas4'] = $this->input->post('html_alis4');
			$data['dia']=$this->input->post('diaalipdf');
			$data['kgst']=$this->input->post('kgstbala');
			$data['kgsha']=$this->input->post('kgshaf');
			$data['fm']='Balanceado';
			$html = $this->load->view('aligranja/alidiahora', $data, true);  
			pdf ($html,'Ali_Dia_Feeders_'.$data['dia'], true);
			set_paper('letter');
        }
		function pdfalimento09() {
            $this->load->model('aligranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablas1'] = $this->input->post('html_alis109');
			$data['tablas2'] = $this->input->post('html_alis209');
			$data['tablas3'] = $this->input->post('html_alis309');
			$data['tablas4'] = $this->input->post('html_alis409');
			$data['dia']=$this->input->post('diaalipdf09');
			$data['fm']='09:30 a.m.';
			$html = $this->load->view('aligranja/alidiahora', $data, true);  
			pdf ($html,'Ali_Dia_9_30am_'.$data['dia'], true);
			set_paper('letter');
        }
		function pdfalimento03() {
            $this->load->model('aligranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablas1'] = $this->input->post('html_alis103');
			$data['tablas2'] = $this->input->post('html_alis203');
			$data['tablas3'] = $this->input->post('html_alis303');
			$data['tablas4'] = $this->input->post('html_alis403');
			$data['dia']=$this->input->post('diaalipdf03');
			$data['fm']='03:00 p.m.';
			$html = $this->load->view('aligranja/alidiahora', $data, true);  
			pdf ($html,'Ali_Dia_3pm_'.$data['dia'], true);
			set_paper('letter');
        }
		function pdfalimento07() {
            $this->load->model('aligranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablas1'] = $this->input->post('html_alis107');
			$data['tablas2'] = $this->input->post('html_alis207');
			$data['tablas3'] = $this->input->post('html_alis307');
			$data['tablas4'] = $this->input->post('html_alis407');
			$data['dia']=$this->input->post('diaalipdf07');
			$data['fm']='07:00 p.m.';
			$html = $this->load->view('aligranja/alidiahora', $data, true);  
			pdf ($html,'Ali_Dia_7pm_'.$data['dia'], true);
			set_paper('letter');
        }
		
		function pdfrepgral() {
            $this->load->model('aligranja_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablagral');
			$ng=$this->input->post('numgrachat');
			$mes=$this->input->post('cmbMesgral');
			switch($mes){
				case 1 : $mes='Enero'; break;
				case 2 : $mes='Febrero'; break;
				case 3 : $mes='Marzo'; break;
				case 4 : $mes='Abril'; break;
				case 5 : $mes='Mayo'; break;
				case 6 : $mes='Junio'; break;
				case 7 : $mes='Julio'; break;
				case 8 : $mes='Agosto'; break;
				case 9 : $mes='Septiembre'; break;
				case 10 : $mes='Octubre'; break;
				case 11 : $mes='Noviembre'; break;
				case 12 : $mes='Diciembre'; break;
			}
			if($ng==1) $data['ng']='Santa Fe';
			if($ng==2) $data['ng']='Gran Kino'; 
			if($ng==3) $data['ng']='Jazmin';
			if($ng==4) $data['ng']='Ahome';
			$data['mes'] = $mes;
			$html = $this->load->view('aligranja/general', $data, true);  
			pdf ($html,$data['ng'].'_Ali_Mes_'.$data['mes'], true);        	
        }
		public function tabladiacha($numgra=0,$ciclo='',$fec=0,$sec=0,$est=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgracha =']=$numgra;
			$filter['where']['ciccha =']=$ciclo;
			if($est==0){
				$filter['where']['feccha =']=$fec;	
			}else{
				$filter['where']['idpischa =']=$est;
			}
			$data['rows'] = $this->aligranja_model->getDatosdia($filter,$fec,$est,$numgra,$sec,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		public function tabladiachag($numgra=0,$ciclo='',$fec=0,$sec=0,$est=0,$fec2=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgracha =']=$numgra;
			$filter['where']['ciccha =']=$ciclo;
			if($est==0){
				$filter['where']['feccha =']=$fec;	
			}else{
				$filter['where']['idpischa =']=$est;
				if($fec>0) $filter['where']['feccha >=']=$fec;
				if($fec2>0) $filter['where']['feccha <=']=$fec2;
			}
			$data['rows'] = $this->aligranja_model->getDatosdiag($filter,$fec,$est,$numgra,$sec,$ciclo,$fec2);
        	echo '('.json_encode($data).')';                
    	}
		public function tabladiachaali($numgra=0,$ciclo='',$fec=0,$sec=0,$sfm=0,$hora=0,$est=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgracha =']=$numgra;
			$filter['where']['ciccha =']=$ciclo;
			$fec2=0;
			if($est==0){
				$filter['where']['feccha =']=$fec;	
			}else{
				$filter['where']['idpischa =']=$est;
				
			}
			$data['rows'] = $this->aligranja_model->getDatosdiagali($filter,$fec,$est,$numgra,$sec,$ciclo,$fec2,$sfm,$hora);
        	echo '('.json_encode($data).')';                
    	}
		public function tablaaliacu($numgra=0,$ciclo='',$fec=0,$sec=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgracha =']=$numgra;
			$filter['where']['ciccha =']=$ciclo;
			$filter['where']['feccha <=']=$fec;
			if($sec>0)$filter['where']['secc =']=$sec;	
			$data['rows'] = $this->aligranja_model->getDatosaliacu($filter,$fec,$numgra,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		public function tabladiacha1($numgra=0,$ciclo='',$fec=0,$sec=0,$est=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgracha =']=$numgra;
			$filter['where']['ciccha =']=$ciclo;
			
			if($est==0){
				$filter['where']['feccha =']=$fec;
				$filter['where']['secc =']=$sec;
				
			}else{
				$filter['where']['secc =']=$sec;	
				$filter['where']['idpischa =']=$est;
				
			}
        	$data['rows'] = $this->aligranja_model->getDatosdia($filter,$fec,$est,$numgra,$sec);
        	echo '('.json_encode($data).')';                
    	}
		public function tablafac($gra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$data['rows'] = $this->aligranja_model->getDatosfac($filter,$gra);
        	echo '('.json_encode($data).')';                
    	}
		public function tablamod(){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$data['rows'] = $this->aligranja_model->getDatosmod($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		public function tablagral($gra=0,$cic='',$mes=0,$ha=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			if($gra>0) $filter['where']['numgracha =']=$gra;			
			$filter['where']['ciccha =']=$cic;
			$filter['where']['month(feccha) =']=$mes;
			$filter['where']['feccha >']='2017-07-11';
			$data['rows'] = $this->aligranja_model->getDatosgral($filter,$ha);
        	echo '('.json_encode($data).')';                
    	}
		public function tablames($cic='',$mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['where']['numgracha =']=4;			
			$filter['where']['ciccha =']=$cic;
			$filter['where']['month(feccha) =']=$mes;
			//$filter['where']['feccha >']='2017-07-11';
			$data['rows'] = $this->aligranja_model->getDatosmes($filter,$mes,$cic);
        	echo '('.json_encode($data).')';                
    	}
		function actualizarfac($id=0){
		$this->load->helper('url');
		$this->load->model('aligranja_model');
		$id_post=$this->input->post('id'); 
		$por=$this->input->post('por');
		$gra=$this->input->post('gra');
		if($id_post!=''){
			$return=$this->aligranja_model->actualizarfac($id_post,$por,$gra); 			
			redirect('aligranja');
		}
		}
		function actualizarmod($id=0){
		$this->load->helper('url');
		$this->load->model('aligranja_model');
		$id_post=$this->input->post('id'); 
		$r0=$this->input->post('r0');if($r0=='') $r0=0;
		$r1=$this->input->post('r1');if($r1=='') $r1=0;
		$r2=$this->input->post('r2');if($r2=='') $r2=0;
		$r3=$this->input->post('r3');if($r3=='') $r3=0;
		//if($id_post!=''){
			$return=$this->aligranja_model->actualizarmod($id_post,$r0,$r1,$r2,$r3); 			
			redirect('aligranja');
		//}
		}
		function actualizardia($id=0){
		$this->load->helper('url');
		$this->load->model('aligranja_model');
		$id_post=$this->input->post('id'); 
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$cic=$this->input->post('cic');
		$numgra=$this->input->post('gra');
		$cod=$this->input->post('cod');if($cod=='') $cod=0;
		$kgt=$this->input->post('kgt');if($kgt=='') $kgt=0;
		$nfd=$this->input->post('nfd');if($nfd=='') $nfd=0;
		$nch=$this->input->post('nch');if($nch=='') $nch=0;
		$r0=$this->input->post('r0');if($r0=='') $r0=0;
		$r1=$this->input->post('r1');if($r1=='') $r1=0;
		$r2=$this->input->post('r2');if($r2=='') $r2=0;
		$r3=$this->input->post('r3');if($r3=='') $r3=0;$mod=$this->input->post('mod');
		$bal=$this->input->post('bal'); if($bal=='') $bal=0;
		$soy=$this->input->post('soy'); if($soy=='') $soy=0;
		$kgt=$soy+$bal;
		//$rac=$this->input->post('rac');
		if($id_post!=''){
			//$return=$this->aligranja_model->actualizardia($id_post,$pis,$fec,$cic,$numgra,$ch1,$ch2,$ch3,$ch4,$ch5,$ch6,$tur,$kgt); 			
			$return=$this->aligranja_model->actualizardia($id_post,$pis,$fec,$cic,$numgra,$cod,$kgt,$nfd,$nch,$r0,$r1,$r2,$r3,$mod,$bal,$soy);
			redirect('aligranja');
		}
		
		}
		
		function agregardia(){
		$this->load->helper('url');
		$this->load->model('aligranja_model');		
		$pis=$this->input->post('pis');
		$fec=$this->input->post('fec');
		$cic=$this->input->post('cic');
		$numgra=$this->input->post('gra');
		$cod=$this->input->post('cod');if($cod=='') $cod=0;
		$kgt=$this->input->post('kgt');if($kgt=='') $kgt=0;
		$nfd=$this->input->post('nfd');if($nfd=='') $nfd=0;
		$nch=$this->input->post('nch');if($nch=='') $nch=0;
		$r0=$this->input->post('r0');if($r0=='') $r0=0;
		$r1=$this->input->post('r1');if($r1=='') $r1=0;
		$r2=$this->input->post('r2');if($r2=='') $r2=0;
		$r3=$this->input->post('r3');if($r3=='') $r3=0;
		$mod=$this->input->post('mod');
		$bal=$this->input->post('bal'); if($bal=='') $bal=0;
		$soy=$this->input->post('soy'); if($soy=='') $soy=0;
		$kgt=$soy+$bal;
		//$rac=$this->input->post('rac');
		if($pis!=''){	
			$this->aligranja_model->agregardia($pis,$fec,$cic,$numgra,$cod,$kgt,$nfd,$nch,$r0,$r1,$r2,$r3,$mod,$bal,$soy);			
			redirect('aligranja');
		}
		}
		public function combob(){
			$filter['cicg']=$this->input->post('ciccha');
			$ciclo=$this->input->post('ciccha');
			//$filter['secc']=$this->input->post('secc');
			$data = $this->aligranja_model->getElementsb($filter,$ciclo); 
			echo '('.json_encode($data).')';              
    	}
		
		public function comboseccprimero(){
			$filter['secc']=$this->input->post('secc');
			$ciclo=$this->input->post('secc');
			//$filter['secc']=$this->input->post('secc');
			$data = $this->aligranja_model->getElementssecc($filter,$ciclo); 
			echo '('.json_encode($data).')';              
    	}
    	public function combosecc(){
			$filest=$this->input->post('filest');
			$data = $this->aligranja_model->getElementssecc($filest); 
			echo '('.json_encode($data).')';              
    	}
		function buscar(){
			$pil = $this->input->post('pil');
			//busca el actual si lo encuentra deja los datos
			$data =$this->aligranja_model->history($pil);
			if($data->cp1=='-1') $data->cp1=0;if($data->cp2=='-1') $data->cp2=0;if($data->cp3=='-1') $data->cp3=0;if($data->cp4=='-1') $data->cp4=0;
			if($data->cp5=='-1') $data->cp5=0;if($data->cp6=='-1') $data->cp6=0;
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('cp1'=>$data->cp1,'cp2'=>$data->cp2,'cp3'=>$data->cp3,'cp4'=>$data->cp4,'cp5'=>$data->cp5,'cp6'=>$data->cp6));
			}
						
		}
		function buscarhas(){
			$pil = $this->input->post('pil');
			//busca el actual si lo encuentra deja los datos
			$data =$this->aligranja_model->historyhas($pil);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('hasga'=>$data->hasg, 'nfd'=>$data->fedcha, 'ncha'=>$data->numcha, 'soy'=>$data->porsoy, 'bal'=>$data->porbal, 'kgta'=>$data->kgta));
			}
						
		}
		function buscarpor(){
			$pro = $this->input->post('pro');$kta = $this->input->post('kta');
			//busca el actual si lo encuentra deja los datos
			$data =$this->aligranja_model->historypro($pro,$kta);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('por'=>number_format(($kta+($kta*$data->porfac))),0));
			}
						
		}
		function buscarmod(){
			$mod = $this->input->post('mod');
			//busca el actual si lo encuentra deja los datos
			$data =$this->aligranja_model->historymod($mod);
			$size=sizeof($data);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('r0mod'=>$data->r0mod, 'r1mod'=>$data->r1mod, 'r2mod'=>$data->r2mod, 'r3mod'=>$data->r3mod));
			}else{
				echo json_encode(array('r0mod'=>'', 'r1mod'=>'', 'r2mod'=>'', 'r3mod'=>''));
			}
						
		}
		function borrarcha($id=0){
			$this->load->helper('url');
			$this->load->model('aligranja_model');
			$id_post=$this->input->post('id');
			if($id_post!=''){
				$return=$this->aligranja_model->quitarcha($id_post); 			
				redirect('aligranja');
			}
		}
				
    }
    
?>