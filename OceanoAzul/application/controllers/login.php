<?php
    class Login extends CI_Controller {
        public function __construct() {
        	parent::__construct();	
        	$this->load->helper('url');
			$this->load->helper('form');
			$this->load->library(array('email','session','user_agent'));
			$this->load->model('usuarios_model');
		}
		
		public function index($msg=0){
			switch ($msg) {
				case '1':
					$texto="usuario o contraseña incorrecto.";
					break;
				case '2':
					$texto="sesión cerrada correctamente.";
					break;
				case '3':
					break;
				case '4':
					break;
				default:
					$texto="";
					break;	
			}
			$datos['mensaje']=$texto;
			//echo "hola".$texto;
			if ($this->agent->is_mobile('iphone')){
				$this->load->view('login_v',$datos);	
				//$this->load->view('iphone/home');
			}else if ($this->agent->is_mobile()){
				$this->load->view('login_v',$datos);	
				//$this->load->view('mobile/home');
				}else{
					$this->load->view('login_v',$datos);	
					//$this->load->view('web/home');
			}
			
		}
		public function log_in(){
			$usuario=$this->input->post('usuario');
			$pass=$this->input->post('passwd');
			$row=$this->usuarios_model->log_in($usuario,$pass);
			$size=sizeof($row);
			$error_msg=" ";
			if($size>0){
				$this->session->set_userdata('id_usuario',$row->id);
				<?php echo "<h1> $row->id;?> <h1> ";
				$this->session->set_userdata('usuario',$row->correo);
				$this->session->set_userdata('nombre',$row->usuario);
				//$row=$this->usuarios_model->log_per($row->perfil);
				//$this->session->set_userdata('perfil',$row->descripcion);
				//$this->session->set_userdata('perfilsel',$row->clave);
				$this->session->set_userdata('perfil',$row->descripcion);
				$this->session->set_userdata('perfilsel',$row->perfil);
				//redirect('alumnos');
				$status=1;
			}else{
				//redirect('login/index/1');
				$status=-1;
				$error_msg="Usuario o Contraseña Incorrecta.";
			}
			echo json_encode(array('status'=>$status,'error'=>$error_msg));
		}
		public function log_out(){
			$array_items=array('id_usuario'=>'','usuario'=>'');
			$this->session->unset_userdata($array_items);
			redirect('login');
		}
		public function olvidoclave() {
        	$this->load->view('olvidoclave');
        }
		public function enviar() {
			$corre=$this->input->post('correo');
			//$config['protocol'] = 'smtp';
			//$config['smtp_host'] = 'ssl://smtp.aquapacific.com.mx';
			//$config['smtp_port']    = '465'; 
        	//$config['smtp_timeout'] = '7'; 
			//$config['smtp_user']    = 'sistemas@aquapacific.com.mx';
       		//$config['smtp_pass']    = 'j$Mt3@*;';
			//$config['charset']    = 'utf-8';
        	//$config['newline']    = "\r\n";
        	//$config['mailtype'] = 'text'; // or html
        	//$config['validation'] = TRUE; // bool whether to validate email or not      
			//$config['mailpth'] = '/usr/sbin/sendmail';
			//$config['charset'] = 'iso-8859-1';
			//$config['wordwrap'] = TRUE;
			//$this->email->initialize($config);
			$this->email->to($corre);
			$this->email->from('sistemas@aquapacific.com.mx','Jesus Benitez');
			$this->email->subject('prueba');
			$this->email->message('hola');
			$status=1;
			if( ! $this->email->send()){
				$status=-1;
				echo $this->email->print_debugger();
				//$this->email->clear(true);
			}else{
				//$this->email->send();	
			}
			//echo $this->email->print_debugger();
						
			echo json_encode(array('status'=>$status,'correo'=>$corre));
		}
	}
?>