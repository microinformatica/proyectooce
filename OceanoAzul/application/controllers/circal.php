<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Circal extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('circal_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            /*$this->load->model('circal_model');
			$data['result']=$this->circal_model->zonas();*/
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('circal/lista',$data);
        }
		function pdfrep() {
            $this->load->model('circal_model');
			//$data['result']=$this->circal_model->verActivos();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['depa']=$this->input->post('depa');
			
			//$rem['result'] = $this->remisiones_model->ultimaremision();
			$data['result']=$this->circal_model->verDiaj();
			/*$txtRemision=0;$urr=0;
			foreach ($rem['result'] as $row): 
				$txtRemision=$row->ultimo+1;										
				$urr=$row->ultimoR+1;
			endforeach;
			$data['txtRemision']=$txtRemision;
			$data['urr']=$urr;*/
			
			$this->load->view('circal/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('circal/listapdf', $data, true);  
			pdf ($html,'circal/listapdf', true);        	
        }
		
		public function tabla($depto='',$depa='',$exc='',$dia=0,$logros=0){        
        	$filter = $this->ajaxsorter->filter($this->input);          	
			if($depto!='Todos'){$filter['where']['eje']=$depto;}
			if($depa!='Todos'){$filter['where']['eje']=$depa;}
			if($exc!='Todos'){$filter['where']['estatus !=']=$exc;}
			if($dia!=0){$filter['where']['fecd']=$dia;}
			if($logros=='Todos'){
				$filter['where']['estatus !=']='Proceso';
				$fechas=date("d/m/Y");
				$fechas=str_replace("/","-",$fechas);
		    	list($dia,$mes,$anio)=explode("-",$fechas);
		    	$dias=(((mktime ( 0, 0, 0, $mes, $dia, $anio) - mktime ( 0, 0, 0, 7, 17, 2006))/(60*60*24))+700000) % 7;
				
				if ( $dias == 0 ) { $dias=-7;}
				if ( $dias == 1 ) { $dias=-8;}
				if ( $dias == 2 ) { $dias=-9;}
				if ( $dias == 3 ) { $dias=-10;}
				if ( $dias == 4 ) { $dias=-11;}
				if ( $dias == 5 ) { $dias=-12;}
				if ( $dias == 6 ) { $dias=-13;}
				//$filter['where']['fecf >=']='2015-03-16';
				$fecha = date('Y-m-d');
				$nuevafecha = strtotime ( '+'.$dias.' day' , strtotime ( $fecha ) ) ;
				$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
				$filter['where']['fecf >=']=$nuevafecha;
				//$filter['where']['fecf <=']='2015-03-22';
				$fecha = $nuevafecha;
				$nuevafecha = strtotime ( '+ 6 day' , strtotime ( $fecha ) ) ;
				$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
				$filter['where']['fecf <=']=$nuevafecha;
			}
        	$data['rows'] = $this->circal_model->getcircal($filter);
        	$data['num_rows'] = $this->circal_model->getNumRows($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		function agregar(){
		$this->load->helper('url');
		$this->load->model('circal_model');		
		$fecd=$this->input->post('fecd');
		$pro=$this->input->post('pro');
		$sol=$this->input->post('sol');
		$eje=$this->input->post('eje');
		$estatus=$this->input->post('estatus');
		$fecf=$this->input->post('fecf');
		$obs=$this->input->post('obs');
		$pri=$this->input->post('pri');
		if($fecd!=''){	
			$this->circal_model->agregar($fecd,$pro,$sol,$eje,$estatus,$fecf,$obs,$pri);			
			redirect('circal');
		}
		}
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('circal_model');
		$id_post=$this->input->post('id'); 
		$fecd=$this->input->post('fecd');
		$pro=$this->input->post('pro');
		$sol=$this->input->post('sol');
		$eje=$this->input->post('eje');
		$estatus=$this->input->post('estatus');
		$fecf=$this->input->post('fecf');
		$obs=$this->input->post('obs');
		$pri=$this->input->post('pri');
		if($id_post!=''){
			$return=$this->circal_model->actualizar($id_post,$fecd,$pro,$sol,$eje,$estatus,$fecf,$obs,$pri); 			
			redirect('circal');
		}
		}
		
				
    }
    
?>