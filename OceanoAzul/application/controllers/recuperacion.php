<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Recuperacion extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('certificadosasi_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('recuperacion_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;		
			$this->load->view('recuperacion/lista',$data);
        }
		function pdfrep( ) {
            $this->load->model('recuperacion_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('recuperacion/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$data['ciclo'] = $this->input->post('ciclo');
			$html = $this->load->view('recuperacion/listapdf', $data, true);  
			pdf ($html,'recuperacion/listapdf', true);
        	set_paper('letter');
        }
		public function tabla($cer=''){
			$this->load->model('recuperacion_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			//$filter['where']['numcerr =']=$cer;
			$data['rows'] = $this->recuperacion_model->getRecuperacion($filter);
        	$data['num_rows'] = $this->recuperacion_model->getNumRowsRecu($filter);	
			echo '('.json_encode($data).')'; 
    	}
		public function tablaa($cer=0,$zon=0){
			$this->load->model('recuperacion_model');        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($cer>0){
				$dec=$cer;
				$dec=explode("0",$dec,2); $dec=(int)$dec[1];
				if($dec<10){ $ciclo = 'r0'.$dec; }
		   		else { $ciclo = 'r'.$dec;} 
				//$ciclo="r".$dec; $tbl=$dec;
				if($zon=='El%20Dorado') $zon='El Dorado';
				if($zon=='Sur%20Sinaloa') $zon='Sur Sinaloa';
				if($zon=='Obreg%C3%B3n') $zon='Obregón';
				if($zon=='Yucat%C3%A1n') $zon='Yucatán';
				$data['rows'] = $this->recuperacion_model->getRecuperacionA($filter,$ciclo,$cer,$zon);
        		$data['num_rows'] = $this->recuperacion_model->getNumRowsRecuA($filter,$ciclo,$cer,$zon);
			}
				
			echo '('.json_encode($data).')'; 
    	}	
		public function combo(){
			$this->load->model('recuperacion_model');
			$act=$this->input->post('actual');
        	if($act>0) {	        
        	$filter['actual']=$this->input->post('actual');           
        	$data = $this->recuperacion_model->getElementsC($filter);        
        	echo '('.json_encode($data).')'; 
			}
    	}
		
    }
    
?>