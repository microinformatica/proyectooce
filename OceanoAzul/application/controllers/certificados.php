<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Certificados extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('certificados_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('certificados_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('certificados/lista',$data);
        }
		function pdfrep( ) {
            $this->load->model('certificados_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('certificados/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$data['ini'] = $this->input->post('ini');
			$data['fin'] = $this->input->post('fin');
			$html = $this->load->view('certificados/listapdf', $data, true);  
			pdf ($html,'certificados/listapdf', true);
        	set_paper('letter');
        }
		public function tabla(){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->certificados_model->getCertificados($filter);
        	$data['num_rows'] = $this->certificados_model->getNumRowsC($filter);
        	echo '('.json_encode($data).')'; 
    	}
		
		 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('certificados_model');		
		$cer=$this->input->post('cer');
		$fi=$this->input->post('fi');
		$fv=$this->input->post('fv');
		$pos=$this->input->post('pos');
		$nau=$this->input->post('nau');
		if($cer!=''){	
			$this->certificados_model->agregar($cer,$fi,$fv,$pos,$nau);			
			redirect('certificados');
		}
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('certificados_model');
		$id_post=$this->input->post('id'); 
		$cer=$this->input->post('cer');
		$fi=$this->input->post('fi');
		$fv=$this->input->post('fv');
		$pos=$this->input->post('pos');
		$nau=$this->input->post('nau');
		if($id_post!=''){
			$return=$this->certificados_model->actualizar($id_post,$cer,$fi,$fv,$pos,$nau); 			
			redirect('certificados');
		}
		}
		
		function borrar($id=0){
		$this->load->helper('url');
		$this->load->model('certificados_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->certificados_model->borrar($id_post); 			
			redirect('certificados');
		}
		}
				
    }
    
?>