<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Maquila extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
       	$this->load->model('maquila_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('maquila_model');
			//$data['result']=$this->produccion_model->Entregado();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('maquila/lista',$data);
        }
		function pdfrepmaq( ) {
            $this->load->model('maquila_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('maquila/lista',$data);
			$data['tablaf'] = $this->input->post('tablaf');
			$data['tablap'] = $this->input->post('tablap');
			$data['dia'] = $this->input->post('dia');
			$data['gra'] = $this->input->post('gra');
			$data['cic'] = $this->input->post('cic');
			$data['fre'] = $this->input->post('fre');
			$data['pro'] = $this->input->post('pro');
			$data['ren'] = $this->input->post('rend');
			$data['imp'] = $this->input->post('imp');
			$data['impu'] = $this->input->post('impu');
			$html = $this->load->view('maquila/listapdfdia', $data, true);  
			pdf ($html,$data['dia'].'_'.$data['gra'], true);
        	set_paper('letter');
        }
		function pdfreppagos( ) {
            $this->load->model('maquila_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('maquila/lista',$data);
			$data['tablapag'] = $this->input->post('tablapag');
			$data['tablapaga'] = $this->input->post('tablapaga');
			$data['gra'] = $this->input->post('gran');
			if($data['gra']=='0') $data['gra']='Oceano Azul';
			if($data['gra']=='2') $data['gra']='Oceano Azul-Kino';
			if($data['gra']=='4') $data['gra']='Oceano Azul-Ahome';
			if($data['gra']=='6') $data['gra']='Oceano Azul-Huatabampo';
			$html = $this->load->view('maquila/listapdfpagos', $data, true);  
			pdf ($html,$data['gra'], true);
        	set_paper('letter');
        }
		function pdfrepfst( ) {
            $this->load->model('maquila_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('maquila/lista',$data);
			$data['tablafst'] = $this->input->post('tablafst');
			$html = $this->load->view('maquila/listapdffst', $data, true);  
			pdf ($html,'FleSeg', true);
        	set_paper('letter');
        }
		function tablamaq($desde='',$cic=0){
			$this->load->model('maquila_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			if($desde!='') $filter['where']['fece =']=$desde;
			$data['rows'] = $this->maquila_model->getmaquiladia($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function tablapag($granja='0',$cic=0,$almacen=0,$graf=0){
			$this->load->model('maquila_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			if($granja!='0') $filter['where']['idge =']=$granja;
			if($almacen!='0') $filter['where']['ida =']=$almacen;
			$data['rows'] = $this->maquila_model->getpagos($filter,$cic,$graf);
        	echo '('.json_encode($data).')'; 
    	}
		function tablapaga($granja='0',$cic=0,$almacen=0){
			$this->load->model('maquila_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			if($granja!='0') $filter['where']['idgp =']=$granja;
			if($almacen!='0') $filter['where']['ida =']=$almacen;
			$filter['where']['ida !=']=2;
			$data['rows'] = $this->maquila_model->getpagosa($filter,$cic,$granja);
        	echo '('.json_encode($data).')'; 
    	}
		function agregarfst(){
			$this->load->model('maquila_model');		
			$cic=$this->input->post('cic');
			$fec=$this->input->post('fec');
			$fol=$this->input->post('fol');
			$pro=$this->input->post('pro');
			$mon=$this->input->post('mon');
			$tc=$this->input->post('tc');
			$imp=$this->input->post('imp');
			$tip=$this->input->post('tip');
			if($fec!=''){	
				$this->maquila_model->agregarfst($cic,$fec,$fol,$pro,$mon,$tc,$imp,$tip);			
				redirect('maquila');
			}
		} 
		function actualizarfst($id=0){
			$this->load->model('maquila_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$fol=$this->input->post('fol');
			$pro=$this->input->post('pro');
			$mon=$this->input->post('mon');
			$tc=$this->input->post('tc');
			$imp=$this->input->post('imp');
			$tip=$this->input->post('tip');
			if($id_post!=''){
				$return=$this->maquila_model->actualizarfst($id_post,$fec,$fol,$pro,$mon,$tc,$imp,$tip); 			
				redirect('maquila');
			}
		}
		public function tablafle($cic=0,$tip=0,$pro=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($cic!='0') $filter['where']['cicfle =']='20'.$cic;
			if($tip!='0') $filter['where']['tipfle =']=$tip;
			if($pro!='0') $filter['where']['profle =']=$pro;
			$data['rows'] = $this->maquila_model->getfle($filter,$cic);
        	echo '('.json_encode($data).')';                
    	}
		
		function actualizarg(){
			$this->load->model('maquila_model');
			$lot=$this->input->post('lot'); 
			$fac=$this->input->post('fac');
			$cic=$this->input->post('cic');
			$mon=$this->input->post('mon');
			if($lot!='') $filter['where']['lotp =']=$lot;
			if($lot!=''){
				$return=$this->maquila_model->actualizarg($filter,$fac,$cic,$mon); 			
				redirect('maquila');
			}
		}
		function agregarmaq(){
			$this->load->model('maquila_model');		
			$fec=$this->input->post('fec');
			$alm=$this->input->post('alm');
			$cos=$this->input->post('cos');
			$gra=$this->input->post('gra');
			$cic=$this->input->post('cic');
			$kgc=$this->input->post('kgc');
			if($fec!=''){	
				$this->maquila_model->agregarmaq($fec,$alm,$cos,$gra,$cic,$kgc);			
				redirect('maquila');
			}
		} 
		function actualizarmaq($id=0){
			$this->load->model('maquila_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$alm=$this->input->post('alm');
			$cos=$this->input->post('cos');
			$gra=$this->input->post('gra');
			$cic=$this->input->post('cic');
			$kgc=$this->input->post('kgc');
			if($id_post!=''){
				$return=$this->maquila_model->actualizarmaq($id_post,$fec,$alm,$cos,$gra,$cic,$kgc); 			
				redirect('maquila');
			}
		}
		function tablamaqf($id='',$cic=0){
			$this->load->model('maquila_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			if($id!='') $filter['where']['idfe =']=$id;
			$data['rows'] = $this->maquila_model->getmaquiladiaf($filter,$cic);
        	echo '('.json_encode($data).')'; 
    	}
		function agregarmaqf(){
			$this->load->model('maquila_model');		
			$est=$this->input->post('est');
			$grg=$this->input->post('grg');
			$grp=$this->input->post('grp');
			$ntp=$this->input->post('ntp');
			$kgt=$this->input->post('kgt');
			$idf=$this->input->post('idf');
			$cic=$this->input->post('cic');
			if($ntp!=''){	
				$this->maquila_model->agregarmaqf($est,$grg,$grp,$ntp,$kgt,$idf,$cic);			
				redirect('maquila');
			}
		} 
		function actualizarmaqf($id=0){
			$this->load->model('maquila_model');
			$id_post=$this->input->post('id'); 
			$est=$this->input->post('est');
			$grg=$this->input->post('grg');
			$grp=$this->input->post('grp');
			$ntp=$this->input->post('ntp');
			$kgt=$this->input->post('kgt');
			$idf=$this->input->post('idf');
			$cic=$this->input->post('cic');
			if($id_post!=''){
				$return=$this->maquila_model->actualizarmaqf($id_post,$est,$grg,$grp,$ntp,$kgt,$idf,$cic); 			
				redirect('maquila');
			}
		}
		function tablamaqp($id='',$cic=0){
			$this->load->model('maquila_model');        
        	$filter = $this->ajaxsorter->filter($this->input);	
			if($id!='') $filter['where']['idpe =']=$id;
			$data['rows'] = $this->maquila_model->getmaquiladiap($filter,$id,$cic);
        	echo '('.json_encode($data).')'; 
    	}			
		function agregarmaqp(){
			$this->load->model('maquila_model');		
			$lot=$this->input->post('lot');
			$idt=$this->input->post('idt');
			$mas=$this->input->post('mas');
			$kgm=$this->input->post('kgm');
			$idg=$this->input->post('idg');
			$ida=$this->input->post('ida');
			$idp=$this->input->post('idp');
			$pre=$this->input->post('pre');
			$fec=$this->input->post('fec');
			$cic=$this->input->post('cic');
			$mon=$this->input->post('mon');
			$tcm=$this->input->post('tcm');
			$gpop=$this->input->post('gpop');
			$fecr=$this->input->post('fecr');
			if($mas!=''){	
				$this->maquila_model->agregarmaqp($lot,$idt,$mas,$kgm,$idg,$ida,$idp,$pre,$fec,$cic,$mon,$tcm,$gpop,$fecr);			
				redirect('maquila');
			}
		} 
		function actualizarmaqp($id=0){
			$this->load->model('maquila_model');
			$id_post=$this->input->post('id'); 
			$lot=$this->input->post('lot');
			$idt=$this->input->post('idt');
			$mas=$this->input->post('mas');
			$kgm=$this->input->post('kgm');
			$idg=$this->input->post('idg');
			$ida=$this->input->post('ida');
			$idp=$this->input->post('idp');
			$pre=$this->input->post('pre');
			$fec=$this->input->post('fec');
			$cic=$this->input->post('cic');
			$mon=$this->input->post('mon');
			$tcm=$this->input->post('tcm');
			$gpop=$this->input->post('gpop');
			$fecr=$this->input->post('fecr');
			if($id_post!=''){
				$return=$this->maquila_model->actualizarmaqp($id_post,$lot,$idt,$mas,$kgm,$idg,$ida,$idp,$pre,$fec,$cic,$mon,$tcm,$gpop,$fecr); 			
				redirect('maquila');
			}
		}
		function borrar($id=0){
			$this->load->helper('url');
			$this->load->model('maquila_model');
			$id_post=$this->input->post('id');
			$tab=$this->input->post('tabla'); 
			$cam=$this->input->post('campo'); 
			$cic=$this->input->post('cic');
			if($id_post!=''){
				$return=$this->maquila_model->quitar($id_post,$tab,$cam,$cic); 			
				redirect('maquila');
			}
		}
		function buscargpo(){
			$idt = $this->input->post('idt');
			//busca el actual si lo encuentra deja los datos
			$data='';
			$data =$this->maquila_model->historygpo($idt);
			//$size=sizeof($data);
			if($data!=''){
				//si lo encuentra deja los datos
				echo json_encode(array('gpop'=>$data->gpoid));
			}
						
		}

		function buscaralm(){
			$id = $this->input->post('id');
			//busca el actual si lo encuentra deja los datos
			$data='';
			$data =$this->maquila_model->historyalm($id);
			//$size=sizeof($data);
			if($data!=''){
				//si lo encuentra deja los datos
				echo json_encode(array('almnom'=>$data->noma));
			}
						
		}
		
    }
    
?>