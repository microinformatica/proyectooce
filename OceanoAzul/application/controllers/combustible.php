<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Combustible extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('combustible_model');         
        $this->load->library(array('ajaxsorter','session','enletras','libreria','user_agent'));		
        $this->load->helper(array('url','form','html','pdf'));
		$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		
		if($id_usuario==false)redirect('login'); 
	   }
        
        function index() {
            $this->load->model('combustible_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('combustible/lista',$data);
        }
		function pdfrep() {
            $this->load->model('combustible_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabla');
			$data['depa'] = $this->input->post('depa');
			$html = $this->load->view('combustible/listapdf', $data, true);  
			pdf ($html,'combustible/listapdf', true);
        }
		function pdfrepCommes() {
            $this->load->model('combustible_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabla');
			//$data['depa'] = $this->input->post('depa');
			$html = $this->load->view('combustible/listapdfcommes', $data, true);  
			pdf ($html,'combustible/CombustibleMes', true);
        }
		function pdfrepComPA() {
            $this->load->model('combustible_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabla');
			//$data['depa'] = $this->input->post('depa');
			$html = $this->load->view('combustible/listapdfcompa', $data, true);  
			pdf ($html,'combustible/CombustibleCom', true);
        }
		function pdfrepcomd() {
            $this->load->model('combustible_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablacomd');
			$data['unidad'] = $this->input->post('unidad');
			$html = $this->load->view('combustible/listapdfcomd', $data, true);  
			pdf ($html,'combustible/ValesUnidad', true);
        }
		function pdfrepcomg() {
            $this->load->model('combustible_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tablacomg');
			$data['ciclo'] = $this->input->post('cic');
			$data['mg'] = $this->input->post('cmbMesT');
			$html = $this->load->view('combustible/listapdfcomg', $data, true);  
			pdf ($html,'combustible/CicloGral', true);
        }
		function pdfrepdia() {
            $this->load->model('combustible_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tablac'] = $this->input->post('tabladps');
			//$data['dia'] = $this->input->post('pfecd');
			$data['ini'] = $this->input->post('txtFI');
			$data['fin'] = $this->input->post('txtFF');
			$html = $this->load->view('combustible/listapdfdia', $data, true);  
			pdf ($html,'combustible/MovsDia', true);
        }
		public function tablacom($ciclo='',$desde='',$hasta=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($ciclo==0){$ciclo=date("y");}
			if($desde!='') $filter['where']['cfec >=']=$desde;
			if($hasta!='') $filter['where']['cfec <=']=$hasta; 
			$data['rows'] = $this->combustible_model->getCombustible($filter,$ciclo);
        	$data['num_rows'] = $this->combustible_model->getNumRowsC($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		public function tablacomg($ciclo='',$mes='0'){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($mes!='0') $filter['where']['month(cfec) =']=$mes;
			$data['rows'] = $this->combustible_model->getCombustibleG($filter,$ciclo,$mes);
        	
        	echo '('.json_encode($data).')';                
    	}
		public function tablacomgd($ciclo='',$uni=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($uni>0) $filter['where']['uide =']=$uni;
			$data['rows'] = $this->combustible_model->getCombustibleGD($filter,$ciclo);
        	echo '('.json_encode($data).')';                
    	}
		public function tabla($extra=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$data['rows'] = $this->combustible_model->getVentas($filter);
        	$data['num_rows'] = $this->combustible_model->getNumRowsV($filter);
        	echo '('.json_encode($data).')'; 
    	}
		
		public function tablaVC($ciclo=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['num'] = $ciclo;  
			$data['rows'] = $this->combustible_model->getVentasC($filter);
        	$data['num_rows'] = $this->combustible_model->getNumRowsVC($filter);
        	echo '('.json_encode($data).')'; 
    	}
		
	function cactualizar($id=0){
		$this->load->helper('url');
		$this->load->model('combustible_model');
		$id_post=$this->input->post('idc'); 
		$cfec=$this->input->post('cfec');
		$clts=$this->input->post('clts');
		$cvale=$this->input->post('cvale');
		$cdg=$this->input->post('cdg');
		$cobs=$this->input->post('cobs');
		$uide=$this->input->post('uide');
		$imp=$this->input->post('imp');
		if($imp=='') $imp=0;
		$ciclo=$this->input->post('ciclo');
		if($id_post!=''){
			$return=$this->combustible_model->cactualizar($id_post,$cfec,$clts,$cvale,$cdg,$cobs,$uide,$ciclo,$imp); 
			redirect('combustible');
		}		
	}
	function cagregar(){
		$this->load->model('combustible_model');		
		$cfec=$this->input->post('cfec');
		$clts=$this->input->post('clts');
		$cvale=$this->input->post('cvale');
		$cdg=$this->input->post('cdg');
		$cobs=$this->input->post('cobs');
		$uide=$this->input->post('uide');
		$imp=$this->input->post('imp');
		if($imp=='') $imp=0;
		$ciclo=$this->input->post('ciclo');
			if($clts!=''){	
				$this->combustible_model->cagregar($cfec,$clts,$cvale,$cdg,$cobs,$uide,$ciclo,$imp);			
				redirect('combustible');
			}
		} 
	function cborrar($id=0){
			$this->load->helper('url');
			$this->load->model('combustible_model');
			$id_post=$this->input->post('idc');
			$ciclo=$this->input->post('ciclo');
			if($id_post!=''){
				$return=$this->combustible_model->cborrar($id_post,$ciclo); 			
				redirect('combustible');
			}
	}
	function buscar(){
			$uni = $this->input->post('uni');
			$data1 =$this->combustible_model->history($uni);
			$size1=sizeof($data1);
			$size=sizeof($data1);
			if($size>0){
				//si lo encuentra deja los datos
				echo json_encode(array('dg'=>$data1->dg));
			}
		}
	
   }
    
?>