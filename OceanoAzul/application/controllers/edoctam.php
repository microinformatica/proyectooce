<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Edoctam extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('edoctam_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));
        $this->load->helper(array('url','form','html','pdf'));
		$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('edoctam_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;		
			$this->load->view('edoctam/lista',$data);
        }
		public function tabla($cli=0,$cic=0,$ao=0,$mes=0){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($mes>0){$filter['where']['estsal =']=$extra;} 
			$data['rows'] = $this->edoctam_model->getSaldos($filter,$cli,$cic,$mes,$ao);
        	echo '('.json_encode($data).')';                
    	}
		
		/*function reporte1( ) {
        	$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$data['tabla'] = $this->input->post('tablavtag');
			$data['texto'] = $this->input->post('texto');
			$this->load->view('analisisvta/reporte1',$data); 
        }*/
		function edoctaexp( ) {
        	$this->load->model('edoctam_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			//$this->load->view('edoctam/lista',$data);
			$data['tabla'] = $this->input->post('tabla');
			$data['cli'] = $this->input->post('cli');
			$data['mes'] = $this->input->post('mes');
			//buscar al cliente y sus datos
			$filter['where']['Numero =']=$data['cli']; 
			$rem['result'] = $this->edoctam_model->getClientes($filter);
			$nomc='';$dom='';$lecp='';$atc='';$ref='';
			foreach ($rem['result'] as $row): 
				$nomc=$row->Razon;										
				$dom=$row->Dom;
				if($row->CP!=0) $lecp=$row->Loc." ".$row->Edo." ".$row->CP;
				else $lecp=$row->Loc." ".$row->Edo;
			endforeach;
			//if($(this).data('cp')!=0){	$("#lecp").val($(this).data('loc')+" "+$(this).data('edo')+" "+$(this).data('cp'));	}
			//	else{ $("#lecp").val($(this).data('loc')+" "+$(this).data('edo'));}
			$data['cli'] = $nomc;
			$data['dom'] = $dom;
			$data['lecp'] = $lecp;
			$data['atc'] = $atc;
			$data['ref'] = $ref;
			if($data['usuario']=='Zuleima Benitez'){$data['tel']='(669)166-5456'; $data['correo']='zuleima@acuicolaoceanoazul.mx';}
			else {$data['tel']='(669)994-7702'; $data['correo']='efrain@bonatto.com.mx';}
			$this->load->view('edoctam/expedocta',$data); 
        }
		function pdfrepedc() {
            $this->load->model('edoctam_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('edoctam/lista',$data);
			$data['tabla'] = $this->input->post('tabla');
			$data['cli'] = $this->input->post('cli');
			$data['mes'] = $this->input->post('mes');
			//buscar al cliente y sus datos
			$filter['where']['Numero =']=$data['cli']; 
			$rem['result'] = $this->edoctam_model->getClientes($filter);
			$nomc='';$dom='';$lecp='';$atc='';$ref='';
			foreach ($rem['result'] as $row): 
				$nomc=$row->Razon;										
				$dom=$row->Dom;
				if($row->CP!=0) $lecp=$row->Loc." ".$row->Edo." ".$row->CP;
				else $lecp=$row->Loc." ".$row->Edo;
			endforeach;
			//if($(this).data('cp')!=0){	$("#lecp").val($(this).data('loc')+" "+$(this).data('edo')+" "+$(this).data('cp'));	}
			//	else{ $("#lecp").val($(this).data('loc')+" "+$(this).data('edo'));}
			$data['cli'] = $nomc;
			$data['dom'] = $dom;
			$data['lecp'] = $lecp;
			$data['atc'] = $atc;
			$data['ref'] = $ref;
			if($data['usuario']=='Zuleima Benitez'){$data['tel']='(669)166-5456'; $data['correo']='zuleima@aquapacific.com.mx';}
			else {$data['tel']='(669)994-7702'; $data['correo']='efrain@bonatto.com.mx';}
			$html = $this->load->view('edoctam/repedocta', $data, true);  
			pdf ($html,'edoctam/repedcta', true);
        	
        }
		function pdfrepfacs( ) {
			$this->load->model('edoctam_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$fec=new Libreria();
			$data['tablafacs'] = $this->input->post('tablafacs');
			$data['tec'] = $this->input->post('tec1');
			$data['gas'] = $this->input->post('gto1');
			$data['fec'] = $fec->fecha($this->input->post('fec1'));
			$html = $this->load->view('edoctam/listapdffacs', $data, true);
			pdf ($html,$data['tec'].'_Gasto_'.$data['gas'], true);
      	}
		
		public function tabladetafac($id=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($id!='') 
			$filter['where']['gasf =']=$id; 
			$data['rows'] = $this->edoctam_model->getdetafac($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		/*public function combo(){        
        	$filter['NomDes']=$this->input->post('NomDes');           
        	$data = $this->edoctam_model->getElements($filter);        
        	echo '('.json_encode($data).')'; 
    	}*/
		 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('entregas_model');		
		$fec=$this->input->post('fec');
		$rem=$this->input->post('rem');
		$remi['result'] = $this->edoctam_model->remision($rem);
		$cliente=0;
		foreach ($remi['result'] as $row): 
			$cliente=$row->NumCliR;										
		endforeach;
		$cli=$cliente;
		$enc=$this->input->post('enc');
		$des=$this->input->post('des');
		$cho=$this->input->post('cho');
		$uni=$this->input->post('uni');
		$ali=$this->input->post('ali');
		$com=$this->input->post('com');
		$cas=$this->input->post('cas');
		$hos=$this->input->post('hos');
		$fit=$this->input->post('fit');
		$tot=$this->input->post('tot');
		$rep=$this->input->post('rep');
		$obs=$this->input->post('obs');
		$repos=$this->input->post('repos');	
		$corte=$this->input->post('corte');
		if($fec!=''){	
			$this->edoctam_model->agregar($fec,$rem,$enc,$des,$cho,$uni,$ali,$com,$cas,$hos,$fit,$rep,$obs,$repos,$cli,$corte);			
			redirect('edoctam');
		}
		//$datos['usuario']=$this->usuario;
		//$datos['perfil']=$this->perfil;
		//$this->load->view('edoctam',$datos);
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('edoctam_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$rem=$this->input->post('rem');
		$remi['result'] = $this->edoctam_model->remision($rem);
		$cliente=0;
		foreach ($remi['result'] as $row): 
			$cliente=$row->NumCliR;										
		endforeach;
		$cli=$cliente;
		$enc=$this->input->post('enc');
		$des=$this->input->post('des');
		$cho=$this->input->post('cho');
		$uni=$this->input->post('uni');
		$ali=$this->input->post('ali');
		$com=$this->input->post('com');
		$cas=$this->input->post('cas');
		$hos=$this->input->post('hos');
		$fit=$this->input->post('fit');
		$tot=$this->input->post('tot');
		$rep=$this->input->post('rep');
		$obs=$this->input->post('obs');
		$repos=$this->input->post('repos');
		$corte=$this->input->post('corte');
		if($id_post!=''){
			$return=$this->edoctam_model->actualizar($id_post,$fec,$rem,$enc,$des,$cho,$uni,$ali,$com,$cas,$hos,$fit,$rep,$obs,$repos,$cli,$corte); 			
			redirect('edoctam');
		}
		}
		
		function borrar($id=0){
		$this->load->helper('url');
		$this->load->model('edoctam_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->edoctam_model->borrar($id_post); 			
			redirect('edoctam');
		}
		}
		function quitarDet($id=0){
		$this->load->helper('url');
		$this->load->model('edoctam_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->edoctam_model->quitarDet($id_post); 			
			redirect('edoctam');
		}
		}
		function agregafac(){
		$this->load->helper('url');
		$this->load->model('edoctam_model');		
		$fecf=$this->input->post('fecf');
		$facf=$this->input->post('facf');
		$prof=$this->input->post('prof');
		$tipf=$this->input->post('tipf');
		$impf=$this->input->post('impf');
		$gasf=$this->input->post('gasf');
		$dedf=$this->input->post('dedf');
		if($fecf!=''){	
			$this->edoctam_model->agregafac($fecf,$facf,$prof,$tipf,$impf,$gasf,$dedf);			
			redirect('edoctam');
		}
		}
		function actualizafac($id=0){
		$this->load->helper('url');
		$this->load->model('edoctam_model');
		$id_post=$this->input->post('id'); 
		$fecf=$this->input->post('fecf');
		$facf=$this->input->post('facf');
		$prof=$this->input->post('prof');
		$tipf=$this->input->post('tipf');
		$impf=$this->input->post('impf');		
		$dedf=$this->input->post('dedf');
		if($id_post!=''){
			$return=$this->edoctam_model->actualizafac($id_post,$fecf,$facf,$prof,$tipf,$impf,$dedf); 			
			redirect('edoctam');
		}
		}
		
		function agregarD($id=0){
			$this->load->helper('url');
			$this->load->model('edoctam_model');
			$fec=$this->input->post('fec');
			$usd=$this->input->post('usd');
			$tc=$this->input->post('tc');
			$cta=$this->input->post('cta');
			$obs=$this->input->post('obs');
			$mn=$this->input->post('mn');
			$numcli=$this->input->post('nrc');
			$ciclo=$this->input->post('ciclo');
			$est=$this->input->post('est');
			$descam=$this->input->post('descam');
			$cancelar=$this->input->post('cancelar');
			$impd=$this->input->post('impd');
			$gra=$this->input->post('gra');
			if($fec!=''){
				$return=$this->edoctam_model->agregard($fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam,$cancelar,$impd,$gra); 			
				redirect('edoctam');
			}
		}
		function actualizarD($id=0){
			$this->load->helper('url');
			$this->load->model('edoctam_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$usd=$this->input->post('usd');
			$tc=$this->input->post('tc');
			$cta=$this->input->post('cta');
			$obs=$this->input->post('obs');
			$mn=$this->input->post('mn');
			$numcli=$this->input->post('nrc');
			$ciclo=$this->input->post('ciclo');
			$est=$this->input->post('est');
			$descam=$this->input->post('descam');
			$cancelar=$this->input->post('cancelar');
			$gra=$this->input->post('gra');
			if($id_post!=''){
				$return=$this->edoctam_model->actualizard($id_post,$fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam,$cancelar,$gra); 			
				redirect('edoctam');
			}
		}
		function borrarD($id=0){
			$this->load->helper('url');
			$this->load->model('edoctam_model');
			$id_post=$this->input->post('id'); 
			if($id_post!=''){
				$return=$this->edoctam_model->borrard($id_post); 			
				redirect('edoctam');
			}
		}
		
				
    }
    
?>