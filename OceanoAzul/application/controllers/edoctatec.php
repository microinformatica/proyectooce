<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Edoctatec extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('edoctatec_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));
        $this->load->helper(array('url','form','html','pdf'));
		$this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('edoctatec_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;		
			$rem['result'] = $this->edoctatec_model->ultimafactura();
			$urr=0;
			foreach ($rem['result'] as $row): 
				$urr=$row->ultimo+1;
			endforeach;
			$data['urr']=$urr;											
			$this->load->view('edoctatec/lista',$data);
        }
		function pdfrep() {
            $this->load->model('edoctatec_model');
			$data['tecnico'] = $this->input->post('tecnico');
			$datax['result']=$this->edoctatec_model->verTecnico1($data['tecnico']);
			foreach ($datax['result'] as $row): 
				$tec=$row->NomBio;
			endforeach;
			$data['tecnico']=$tec;
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('edoctatec/lista',$data);
			
			$data['tablac'] = $this->input->post('tabla');
			$data['ini'] = $this->input->post('ini');
			$data['fin'] = $this->input->post('fin');
			//$data['tablac'] = $this->input->post('mytablaCD');
			$html = $this->load->view('edoctatec/listapdf', $data, true);  
			pdf ($html,'edoctatec/listapdf', true);
        	set_paper('letter');
        }
		function pdfrepfacs( ) {
			$this->load->model('edoctatec_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$fec=new Libreria();
			$data['tablafacs'] = $this->input->post('tablafacs');
			$data['tec'] = $this->input->post('tec1');
			$data['gas'] = $this->input->post('gto1');
			$data['fec'] = $fec->fecha($this->input->post('fec1'));
			$html = $this->load->view('edoctatec/listapdffacs', $data, true);
			pdf ($html,$data['tec'].'_Gasto_'.$data['gas'], true);
      	}
		function tabla($enc=0,$extra=0,$corte=0,$mes=0){
			//$this->load->model('edoctatec_model'); 
			if($enc>0){      
        	$filter = $this->ajaxsorter->filter($this->input); 
			if($enc>0){$filter['where']['numbios =']=$enc;}
			if($extra>=1 && $extra<=2){$filter['where']['carabo =']=$extra;}
			if($extra==3){$filter['where']['nomdess =']="Improvistos";}
			if($corte>0){$filter['where']['corte =']=$corte;}
			
			$data['rows'] = $this->edoctatec_model->getSaldo($filter,$mes);
        	$data['num_rows'] = $this->edoctatec_model->getNumRowsS($filter,$mes);
        	}else{
        		if($this->usuario=="Anita Espino" || $this->usuario=="Jesus Benítez" || $this->usuario=="Zuleima Benitez"){
        		$data['rows'] = $this->edoctatec_model->getSaldoG();
        		}else{
        			$data['rows'] ="";$data['num_rows'] ="";
        		}	
        		//$data['num_rows'] = 7;
				
			}
        	echo '('.json_encode($data).')'; 
    	}
		
		public function tabladetafac($id=''){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			//if($id!='') 
			$filter['where']['gasf =']=$id; 
			$data['rows'] = $this->edoctatec_model->getdetafac($filter);
        	echo '('.json_encode($data).')';                
    	}
		
		/*public function combo(){        
        	$filter['NomDes']=$this->input->post('NomDes');           
        	$data = $this->edoctatec_model->getElements($filter);        
        	echo '('.json_encode($data).')'; 
    	}*/
		 
		function agregar(){
		$this->load->helper('url');
		$this->load->model('entregas_model');		
		$fec=$this->input->post('fec');
		$rem=$this->input->post('rem');
		$remi['result'] = $this->edoctatec_model->remision($rem);
		$cliente=0;
		foreach ($remi['result'] as $row): 
			$cliente=$row->NumCliR;										
		endforeach;
		$cli=$cliente;
		$enc=$this->input->post('enc');
		$des=$this->input->post('des');
		$cho=$this->input->post('cho');
		$uni=$this->input->post('uni');
		$ali=$this->input->post('ali');
		$com=$this->input->post('com');
		$cas=$this->input->post('cas');
		$hos=$this->input->post('hos');
		$fit=$this->input->post('fit');
		$tot=$this->input->post('tot');
		$rep=$this->input->post('rep');
		$obs=$this->input->post('obs');
		$repos=$this->input->post('repos');	
		$corte=$this->input->post('corte');
		if($fec!=''){	
			$this->edoctatec_model->agregar($fec,$rem,$enc,$des,$cho,$uni,$ali,$com,$cas,$hos,$fit,$rep,$obs,$repos,$cli,$corte);			
			redirect('edoctatec');
		}
		//$datos['usuario']=$this->usuario;
		//$datos['perfil']=$this->perfil;
		//$this->load->view('edoctatec',$datos);
		}
		
		function actualizar($id=0){
		$this->load->helper('url');
		$this->load->model('edoctatec_model');
		$id_post=$this->input->post('id'); 
		$fec=$this->input->post('fec');
		$rem=$this->input->post('rem');
		$remi['result'] = $this->edoctatec_model->remision($rem);
		$cliente=0;
		foreach ($remi['result'] as $row): 
			$cliente=$row->NumCliR;										
		endforeach;
		$cli=$cliente;
		$enc=$this->input->post('enc');
		$des=$this->input->post('des');
		$cho=$this->input->post('cho');
		$uni=$this->input->post('uni');
		$ali=$this->input->post('ali');
		$com=$this->input->post('com');
		$cas=$this->input->post('cas');
		$hos=$this->input->post('hos');
		$fit=$this->input->post('fit');
		$tot=$this->input->post('tot');
		$rep=$this->input->post('rep');
		$obs=$this->input->post('obs');
		$repos=$this->input->post('repos');
		$corte=$this->input->post('corte');
		if($id_post!=''){
			$return=$this->edoctatec_model->actualizar($id_post,$fec,$rem,$enc,$des,$cho,$uni,$ali,$com,$cas,$hos,$fit,$rep,$obs,$repos,$cli,$corte); 			
			redirect('edoctatec');
		}
		}
		
		function borrar($id=0){
		$this->load->helper('url');
		$this->load->model('edoctatec_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->edoctatec_model->borrar($id_post); 			
			redirect('edoctatec');
		}
		}
		function quitarDet($id=0){
		$this->load->helper('url');
		$this->load->model('edoctatec_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->edoctatec_model->quitarDet($id_post); 			
			redirect('edoctatec');
		}
		}
		function agregafac(){
		$this->load->helper('url');
		$this->load->model('edoctatec_model');		
		$fecf=$this->input->post('fecf');
		$facf=$this->input->post('facf');
		$prof=$this->input->post('prof');
		$tipf=$this->input->post('tipf');
		$impf=$this->input->post('impf');
		$gasf=$this->input->post('gasf');
		$dedf=$this->input->post('dedf');
		if($fecf!=''){	
			$this->edoctatec_model->agregafac($fecf,$facf,$prof,$tipf,$impf,$gasf,$dedf);			
			redirect('edoctatec');
		}
		}
		function actualizafac($id=0){
		$this->load->helper('url');
		$this->load->model('edoctatec_model');
		$id_post=$this->input->post('id'); 
		$fecf=$this->input->post('fecf');
		$facf=$this->input->post('facf');
		$prof=$this->input->post('prof');
		$tipf=$this->input->post('tipf');
		$impf=$this->input->post('impf');		
		$dedf=$this->input->post('dedf');
		if($id_post!=''){
			$return=$this->edoctatec_model->actualizafac($id_post,$fecf,$facf,$prof,$tipf,$impf,$dedf); 			
			redirect('edoctatec');
		}
		}
				
    }
    
?>