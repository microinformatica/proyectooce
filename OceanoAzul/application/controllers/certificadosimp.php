<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Certificadosimp extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('certificadosimp_model');         
        $this->load->library(array('ajaxsorter','session'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('certificadosimp_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;		
			$this->load->view('certificadosimp/lista',$data);
        }
		function pdfrep( ) {
            $this->load->model('certificadosimp_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('certificadosimp/lista',$data);
			$data['fi'] = $this->input->post('fi1');
			$data['ff'] = $this->input->post('ff1');
			$data['aut'] = $this->input->post('aut1');
			$data['ent'] = $this->input->post('ent1');
			$data['cer'] = $this->input->post('cer1');
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('certificadosimp/listapdf', $data, true);  
			pdf ($html,'certificadosimp/listapdf', true);
        	set_paper('letter');
        }
		/*public function tabla($cer=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['numcerr =']=$cer;
			$data['rows'] = $this->certificadosimp_model->getCertificadosimp($filter);
        	$data['num_rows'] = $this->certificadosimp_model->getNumRowsCimp($filter);
			//$data['result'] = $this->certificadosimp_model->getCertificadosNum($cer);;
			echo '('.json_encode($data).')'; 
    	}*/
		public function tabla($cer=''){
			$this->load->model('certificadosimp_model');
			$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['numcerr =']=$cer;
			$data['rows'] = $this->certificadosimp_model->getCertificadosimp($filter);
        	$data['num_rows'] = $this->certificadosimp_model->getNumRowsCimp($filter);
			echo '('.json_encode($data).')'; 
    	} 
		/*public function tablad($cer=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['numreg =']=$cer;
			$data['rows'] = $this->certificadosimp_model->getCertificadosimpd($filter);
        	$data['num_rows'] = $this->certificadosimp_model->getNumRowsCimpd($filter);			
			echo '('.json_encode($data).')'; 
    	}*/
			
    }
    
?>