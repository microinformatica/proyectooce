<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Gobierno extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('gobierno_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('gobierno_model');			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;													
			$this->load->view('gobierno/lista',$data);
        }
		
		public function tablac(){        
        	$filter = $this->ajaxsorter->filter($this->input); 
			$data['rows'] = $this->gobierno_model->getCertificados($filter);
        	$data['num_rows'] = $this->gobierno_model->getNumRowsC($filter);
        	echo '('.json_encode($data).')'; 
    	}
		
		public function tablar($cer=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			$filter['where']['numcerr =']=$cer;
			$data['rows'] = $this->gobierno_model->getCertificadosimp($filter);
        	$data['num_rows'] = $this->gobierno_model->getNumRowsCimp($filter);
			echo '('.json_encode($data).')'; 
    	} 
		function pdfrepimp( ) {
            $this->load->model('gobierno_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('gobierno/lista',$data);
			$data['fi'] = $this->input->post('fi1');
			$data['ff'] = $this->input->post('ff1');
			$data['aut'] = $this->input->post('aut1');
			$data['ent'] = $this->input->post('ent1');
			$data['cer'] = $this->input->post('cer1');
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('gobierno/listapdfimp', $data, true);  
			pdf ($html,'gobierno/listapdfimp', true);
        	set_paper('letter');
        }
		function agregarc(){
		$this->load->helper('url');
		$this->load->model('gobierno_model');		
		$cer=$this->input->post('cer');
		$fi=$this->input->post('fi');
		$fv=$this->input->post('fv');
		$pos=$this->input->post('pos');
		$nau=$this->input->post('nau');
		if($cer!=''){	
			$this->gobierno_model->agregarc($cer,$fi,$fv,$pos,$nau);			
			redirect('gobierno');
		}
		}
		
		function actualizarc($id=0){
		$this->load->helper('url');
		$this->load->model('gobierno_model');
		$id_post=$this->input->post('id'); 
		$cer=$this->input->post('cer');
		$fi=$this->input->post('fi');
		$fv=$this->input->post('fv');
		$pos=$this->input->post('pos');
		$nau=$this->input->post('nau');
		if($id_post!=''){
			$return=$this->gobierno_model->actualizarc($id_post,$cer,$fi,$fv,$pos,$nau); 			
			redirect('gobierno');
		}
		}
		
		function borrarc($id=0){
		$this->load->helper('url');
		$this->load->model('gobierno_model');
		$id_post=$this->input->post('id'); 
		if($id_post!=''){
			$return=$this->gobierno_model->borrarc($id_post); 			
			redirect('gobierno');
		}
		}
		
		//asignacion
		function pdfrepa( ) {
            $this->load->model('gobierno_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('gobierno/lista',$data);
			$data['tablac'] = $this->input->post('tabla');
			$html = $this->load->view('gobierno/listapdfa', $data, true);  
			pdf ($html,'gobierno/listapdfa', true);
        	set_paper('letter');
        }
		public function tablaa($cer=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			//$filter['where']['numcerr =']=$cer;
			$data['rows'] = $this->gobierno_model->getcertificadosasi($filter);
        	$data['num_rows'] = $this->gobierno_model->getNumRowsasi($filter);		
			echo '('.json_encode($data).')'; 
    	}
		public function tablaca(){        
        	$filter = $this->ajaxsorter->filter($this->input);
			$data['rows'] = $this->gobierno_model->getcertificadosasic($filter);
        	$data['num_rows'] = $this->gobierno_model->getNumRowsasic($filter);		
			echo '('.json_encode($data).')'; 
    	}
		function actualizara($id=0){
			$this->load->model('gobierno_model');
			$id_post=$this->input->post('id'); 
			$cer=$this->input->post('cer');
			if($id_post!=''){
				$return=$this->gobierno_model->actualizara($id_post,$cer); 			
				redirect('gobierno');
			}
		}
		function actualizarga(){
			$this->load->model('gobierno_model');
			$cer=$this->input->post('cer');
			$ini=$this->input->post('ini');
			$fin=$this->input->post('fin');
			if($cer!=''){
				$return=$this->gobierno_model->actualizarga($cer,$ini,$fin); 			
				redirect('gobierno');
			}
		}
		
				
    }
    
?>