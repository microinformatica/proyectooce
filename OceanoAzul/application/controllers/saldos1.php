<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Saldos extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('saldos_model');         
        $this->load->library(array('ajaxsorter','session','enletras','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('saldos_model');
			//$data['result']=$this->facturado_model->Entregado();
			//$data['result']=$this->saldos_model->zonas();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$this->load->view('saldos/lista',$data);
        }
		function pdfrep() {
            $this->load->model('saldos_model');
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			$this->load->view('saldos/lista',$data);
			$data['tablarem'] = $this->input->post('tablaremisiones1');
			$data['tabladep'] = $this->input->post('tabladepositos');
			$data['cli'] = $this->input->post('cli');
			$data['dom'] = $this->input->post('dom');
			$data['lecp'] = $this->input->post('lecp');
			$data['saldo'] = $this->input->post('saldo');
			$data['letras'] = $this->input->post('letras');
			$data['atc'] = $this->input->post('atc');
			$html = $this->load->view('saldos/listapdf', $data, true);
			pdf ($html,'saldos/listapdf', true);
        	set_paper('letter');
        }
		
		public function tablaremi($cliente=0,$ciclo='',$cancelacion=0){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $ciclo; 					
			$filter['where']['NumCliR']=$cliente;										
			if($cancelacion==-1){$filter['where']['Cancelacion']=0;}
        	$data['rows'] = $this->saldos_model->remisiones($filter,$cancelacion);
        	$data['num_rows'] = $this->saldos_model->getNumRowsR($filter);
        	echo '('.json_encode($data).')';			
    	}
		public function tabladepo($cliente=0,$ciclo=0,$cancelacion=0){
			$filter = $this->ajaxsorter->filter($this->input);  
			$filter['num'] = $ciclo; 					
			$filter['where']['NRC']=$cliente;					
			if($cancelacion==-1){$filter['where']['Cancelacion']=0;}								
        	$data['rows'] = $this->saldos_model->depositos($filter,$cliente,$cancelacion);
        	$data['num_rows'] = $this->saldos_model->getNumRowsD($filter);
			echo '('.json_encode($data).')'; 
    	}
		public function letrastras(){
			$saldo=$this->input->post('saldo1');
			if($saldo=="") $saldo=0;
			$V=new EnLetras();
			$data['canlet'] =$V->ValorEnLetras($saldo,"Dolares");			
			
			//echo json_encode($data);
        	echo '('.json_encode($data).')'; 
			//echo json_encode(array('status'=>$status,'error'=>$error_msg));			
    	}
		
		public function remidepo($cliente=0,$tabla='',$valor='',$ciclo=0,$solodep=0){
			$this->load->model('saldos_model');
			$cliente=$this->input->post('cliente');//$cliente=289;
			$tabla=$this->input->post('tabla');//$tabla='r13';
			$valor=$this->input->post('valor');//$rod='Remisiones';
			$ciclo=$this->input->post('ciclo');//$rod='Remisiones';						
			$solodep=$this->input->post('solodep');//$rod='Remisiones';
			$data= $this->saldos_model->rescandatos($cliente,$tabla,$valor,$ciclo,$solodep);
    	}
		
		function actualizarD($id=0){
			$this->load->helper('url');
			$this->load->model('saldos_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$usd=$this->input->post('usd');
			$tc=$this->input->post('tc');
			$cta=$this->input->post('cta');
			$obs=$this->input->post('obs');
			$mn=$this->input->post('mn');
			$numcli=$this->input->post('nrc');
			$ciclo=$this->input->post('ciclo');
			$est=$this->input->post('est');
			$descam=$this->input->post('descam');
			$cancelar=$this->input->post('cancelar');
			if($id_post!=''){
				$return=$this->saldos_model->actualizard($id_post,$fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam,$cancelar); 			
				redirect('saldos');
			}
		}
		function borrarD($id=0){
			$this->load->helper('url');
			$this->load->model('saldos_model');
			$id_post=$this->input->post('id'); 
			$usd=$this->input->post('usd');
			$numcli=$this->input->post('nrc');		
			if($id_post!=''){
				$return=$this->saldos_model->borrard($id_post,$usd,$numcli); 			
				redirect('saldos');
			}
		}
		function actsaldo($id=0){
			$this->load->helper('url');
			$this->load->model('saldos_model');
			$id_post=$this->input->post('numcli'); 			
			$saldo=$this->input->post('saldo1');
			if($id_post!=''){
				$return=$this->saldos_model->actsaldocli($id_post,$saldo); 			
				redirect('saldos');
			}
		}
		function actualizarSI($id=0){
			$this->load->helper('url');
			$this->load->model('saldos_model');
			$id_post=$this->input->post('id'); 			
			$cancelar=$this->input->post('cancelar');
			if($id_post!=''){
				$return=$this->saldos_model->actualizarsi($id_post,$cancelar); 			
				redirect('saldos');
			}
		}
		function actualizarR($id=0){
			$this->load->helper('url');
			$this->load->model('saldos_model');
			$id_post=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			//$rem=$this->input->post('rem');
			$cli=$this->input->post('cli');
			$can=$this->input->post('can');
			$pre=$this->input->post('pre');
			$avi=$this->input->post('avi');
			$est=$this->input->post('est');
			$dco=$this->input->post('dco');
			$tipo=$this->input->post('tipo');
			$obs=$this->input->post('obs');
			$tabla=$this->input->post('tabla');
			//$fol=$this->input->post('fol');
			//$ur=$this->input->post('ur');
			$cancelar=$this->input->post('rec');
			if($id_post!=''){
				$return=$this->saldos_model->actualizarr($id_post,$fec,$cli,$can,$pre,$avi,$est,$dco,$tipo,$obs,$tabla,$cancelar); 			
				redirect('saldos');
			}		
		}
		function agregardcd(){
		$this->load->helper('url');
		$this->load->model('saldos_model');		
		$fec=$this->input->post('fec');
		$usd=$this->input->post('usd');
		$tc=$this->input->post('tc');
		$cta=$this->input->post('cta');
		$obs=$this->input->post('obs');
		$mn=$this->input->post('mn');
		$numcli=$this->input->post('nrc');
		$ciclo=$this->input->post('ciclo');
		$est=$this->input->post('est');
		$descam=$this->input->post('descam');
		if($fec!=''){	
			$this->saldos_model->agregarDCD($fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$est,$descam);			
			redirect('saldos');
		}
		}
}
    
?>