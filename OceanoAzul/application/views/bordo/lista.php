<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
	
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}

#export_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datag { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datab { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#containers {
    	max-width: 900px;
    	min-width: 380px;
    	height: 380px;
    	margin: 1em auto;
	}
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#bordo" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/cosecha.jpg" width="20" height="20" border="0"> Salidas en Bordo </a></li>
		<li><a href="#salidabordo" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/cosecha.jpg" width="20" height="20" border="0"> Salidas por Estanque </a></li>
		<li><a href="#concentrado" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/camarones.png" width="20" height="20" border="0"> General </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 630px" >
		<div id="bordo" class="tab_content" style="margin-top: -15px" >
			<div class="ajaxSorterDiv" id="tabla_bordo" name="tabla_bordo" style="margin-top: -5px; margin-left: -10px; width: 990px;">   
             	  <div class="ajaxpager" style="margin-top: 0px;width: 990px;" > 
            		<ul class="order_list" style="width: 950px; text-align: left; height: 50px; " >
            		<table style="margin-left: -5px"  border="0" m >
 						<tr style="font-size: 10px">
 							<th colspan="2" style="text-align: center">Ciclo</th>
 							<th style="text-align: center">Fecha</th>
 							<th style="text-align: center">Folio</th>
 							<th style="text-align: center">Granja</th>
 							<th style="text-align: center">Tipo Salida</th>
 							<th style="text-align: center">Cosecha</th>
 							<th style="text-align: center">Cliente
 								<select name="secc" id="secc" style="font-size:0px; height: 0px;margin-top: 1px;visibility: hidden " >								
    								<option value="0">Sel.</option>
    							</select>
 							</th>
 							<th style="text-align: center">Est <input type="hidden" id="filest" name="filest" style="width: 40px;">
 							</th>
 							<th style="text-align: center">Gramos</th>
 							<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez" || $usuario=="Joel Lizarraga A."){ ?>
 							<th style="text-align: center">$ Base</th>
 							<?php }?>
 							<th style="text-align: center">Kilogramos</th>
 							<th >
 								<?php if($usuario=="Zuleima Benitez"){ ?>
 								<input style="font-size: 14px" type="submit" id="nue" name="nue" value="Nuevo" title="NUEVO" />
 							<?php } ?>
 							</th>	
 						</tr>
 						<tr>
 							<td>
 								<select name="cicloG" id="cicloG" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-4px;">   						
   									<option value="22" >2022</option>
   									<option value="21" >2021</option>
   									<option value="20" >2020</option>
   									<option value="19" >2019</option>
   									<option value="18" >2018</option>
   								</select>
 							</td>
 							<td>
 								<select name="ncicos" id="ncicos" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-15px;">   						
   									<option value="1" >1</option>
   									<option value="2" >2</option>
   								</select>
 							</td>
 							<td><input size="7%" type="text" name="feccos" id="feccos" class="fecha redondo mUp" readonly="true" style="text-align: center;font-size: 10px; margin-left: -16px" ></td>
 							<!--<td><input  size="4%" type="text" name="folio" id="folio" style="text-align: center; border: none; color: red; margin-left: -7px" value="<?php echo set_value('folio',$folio); ?>" ></td>-->
 							<td><input  size="2%" type="text" name="folio" id="folio" style="text-align: center; border: none; color: red; margin-left: -7px" ></td>
 							<td>
 								<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez"){ ?>
 								<select name="numgrab" id="numgrab" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-8px; width: 62px">   						
   									<option value="0" >Sel.</option>
   									<option value="4" >Ahome</option>
   									<option value="10" >Topolobampo</option>
   									<option value="6" >Huatabampo</option>
   									<option value="5" >Oce. Azul</option>
   									<option value="2" >Kino</option>
   								</select>
   							<?php } else {?>
   								<select name="numgrab" id="numgrab" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-8px; width: 62px">   						
   									<option value="4" >Ahome</option>   									
   								</select>
   							<?php } ?>
 							</td>
 							<td>
 								<select name="tipcos" id="tipcos" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-10px;">   						
   									<option value="0" >Sel.</option>
   									<option value="1" >Maquila</option>
   									<option value="2" >Venta</option>
   									<option value="3" >Donación</option>
   									
   								</select>
 							</td>
 							<td>
 								<select name="numcos" id="numcos" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-10px; width: 48px;">   						
   									<option value="0" >Sel.</option>
   									<option value="1" >1ra</option><option value="2" >2da</option><option value="3" >3ra</option>
   									<option value="4" >4ta</option><option value="5" >5ta</option><option value="6" >6ta</option> 
   									<option value="7" >Fin</option>
   								</select>
 							</td>
 							<td>
 								
 								 <select name="clicos" id="clicos" style="font-size: 10px;height: 25px;  margin-top: 1px;margin-left:-10px; width: 250px">
                    				<option value="0">Sel.</option>
          							<?php	
           							$data['result']=$this->bordo_model->verClientes();
									foreach ($data['result'] as $row):?>
           							<option value="<?php echo $row->Numero;?>" ><?php echo $row->Razon;?></option>
           							<?php endforeach;?>
   								</select> 
   							</td>
 							<td>
 								<!--
 								<select name="cicbor" id="cicbor" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-10px;">
      							<?php $ciclof=19;$actual=date("y"); //$actual+=1;
									while($actual >= $ciclof){?>
										<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
										<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            						<?php  $actual-=1; } ?>      									
    						  </select>
    						 -->
    						 	<select name="estcos" id="estcos" style="font-size: 10px;height: 23px;margin-top: 1px;margin-left:-10px;"  ></select>
    						</td>
 							<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px; text-align: center; margin-left: -15px" align="middle" name="grscos" type="text" id="grscos"  size="4" /></td>
 							<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez" || $usuario=="Joel Lizarraga A."){ ?>
 							<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px; text-align: center; margin-left: -9px" align="middle" name="prebas" type="text" id="prebas"  size="4" /></td>
 							<?php }?>
 							<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px; text-align: center; margin-left: -9px" align="middle" name="kgscos" type="text" id="kgscos"  size="8" /></td>
 							<td>
 								<?php if($usuario=="Zuleima Benitez"){ ?>
			   					<input style="font-size: 14px" type="submit" id="agregas" name="agregas" value="+" title="AGREGAR y ACTUALIZAR DETALLE" />
								<input style="font-size: 14px" type="submit" id="quitas" name="quitas" value="-" title="QUITAR DETALLE" />
							<?php } ?>
								<input type="hidden"  size="3%" type="text" name="idcos" id="idcos" style="text-align: center;">
							</td>
						</tr>
 					</table>		
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 400px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablabordo" name="mytablabordo" class="ajaxSorter" border="1" style="text-align: center" >
                       	<thead >
                       		
                       		<tr style="font-size: 13px">
                       			<!--
                       			<th data-order = "feccos1">Fecha</th>
                       			-->
                       			<th data-order = "folio2">Folio</th>
                       			
                       			<th data-order = "nomg">Granja</th>
                       			<th data-order = "tipcos1">Estatus</th>
                       			<th data-order = "numcos1">Cosecha</th>	
                       			<!--
                       			<th data-order = "clicos1">Cliente</th>
                       			-->
                       			<th data-order = "pisg">Estanque</th>	
                       			<th data-order = "grscos">Gramos</th>
                       			<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez" || $usuario=="Joel Lizarraga A."){ ?>
                       			<th data-order = "prebas1">Precio Base</th>
                       			<th data-order = "pregra">P.B + gr.</th>
                       			<?php }?>
                       			<th data-order = "kgscos">Kilogramos</th>   
                       			<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez" || $usuario=="Joel Lizarraga A."){ ?>                    			
                       			<th data-order = "imp">Importe M.N.</th>
                       			<?php }?>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 12px; text-align: center">
                       	</tbody>                        	
                   	</table>
                </span> 
                <div style="margin-left: 850px" >
                <button id="export_data" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px;" >Imprimir</button>
				<form id="data_tables" action="<?= base_url()?>index.php/bordo/pdfrepcos" method="POST">
					<input type="hidden" name="tabla" id="html_bor"/>
					<input type="hidden" type="text" name="dia" id="dia" >
					<input type="hidden" type="text" name="tip" id="tip" >					
					<input type="hidden" type="text" name="fol" id="fol" >
					<input type="hidden" type="text" name="cli" id="cli" >
					<input type="hidden" type="text" name="gra" id="gra" >
				</form>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#export_data').click(function(){									
								$('#html_bor').val($('#mytablabordo').html());
								$('#data_tables').submit();
								$('#html_bor').val('');
								
						});
					});
				</script>
				</div>
 			</div>
 			
 			<div class="ajaxSorterDiv" id="tabla_gralb" name="tabla_gralb" style="margin-top: -25px">   
             	  <div class="ajaxpager" style="margin-top: -5px;" > 
            		<ul class="order_list" style="width: 700px; text-align: left; " >
            			Compras de Camaron Fresco efectuadas a empresa: Acuícola Oceano Azul -	Granja
            			<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez"){ ?>
            			<select name="numgrabgb" id="numgrabgb" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   							<option value="0" >Seleccione</option>
   							<option value="4" >OA-Ahome</option>
   							<option value="10" >OA-Topolobampo</option>
   							<option value="6" >OA-Huatabampo</option>
   							<option value="5" >Oce. Azul</option><option value="2" >OA-Kino</option>
   						</select>  
   						<?php }else{ ?>
   							<select name="numgrabgb" id="numgrabgb" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   							<option value="4" >OA-Ahome</option>
   						</select>  
   						<?php } ?>
   						<button id="export_datab" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
						<form id="data_tableg" action="<?= base_url()?>index.php/bordo/pdfrepcosb" method="POST">
							<input type="hidden" name="tabla" id="html_borg"/> <input type="hidden" name="gra" id="gra"/>
						</form>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#export_datab').click(function(){									
									$('#html_borg').val($('#mytablagralb').html());
									$('#data_tableg').submit();
									$('#html_borg').val('');
								});
							});
						</script>          			
            		</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 130px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablagralb" name="mytablagralb" class="ajaxSorter" border="1" style="text-align: center" >
                       	<thead >
                       		<tr style="font-size: 12px">
                       			<th data-order = "feccos1">Dia</th>
                       			<th data-order = "folio1">Folio</th>
                       			<th data-order = "Razon">Granja - Cliente</th>
                       			<th data-order = "grs">Peso Promedio</th>
                       			<th data-order = "kgs">Kilogramos</th>
                       			<th data-order = "imp">Importe</th>
                       			<th data-order = "totk">Acum. Kgs.</th>
                       			<th data-order = "toti">Acum. Imp.</th>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 12px; text-align: center">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>
 		</div>	  
 		<div id="salidabordo" class="tab_content" style="margin-top: -15px" >
 			<div class="ajaxSorterDiv" id="tabla_bordoc" name="tabla_bordoc" style="margin-top: -5px;width: 950px;">   
             	  <div class="ajaxpager" style="margin-top: 0px;width: 940px;" > 
            		<ul class="order_list" style="width: 930px; text-align: left; height: 50px; " >
            		<table  border="0" >
 						<tr>
 							<th style="text-align: center"> Granja </th>
 							<th style="text-align: center"> Tanque </th>
 							<th style="text-align: center"> Ciclo </th>
 						</tr>
 						<tr>
 							<td>
 								<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez"){ ?>
 								<select name="numgrabc" id="numgrabc" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-8px;">   						
   									<option value="0" >Seleccione</option>
   									<option value="4" >OA-Ahome</option><option value="10" >OA-Topolobampo</option>
   									<option value="6" >OA-Huatabampo</option>
   									<option value="5" >Oce. Azul</option><option value="2" >OA-Kino</option>
   								</select>
   							<?php }else{ ?>
   								<select name="numgrabc" id="numgrabc" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-8px;">   						
   									<option value="4" >OA-Ahome</option>   									
   								</select>
   							<?php } ?>
 							</td>
 							<td>
 								<select name="estcosc" id="estcosc" style="font-size: 10px;height: 23px;margin-top: 1px;margin-left:-8px;"  ></select>
    						</td>
    						<td>
 							<select name="ncicosg" id="ncicosg" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-15px;">   					
 									<option value="0" >Sel.</option>	
   									<option value="1" >1</option>
   									<option value="2" >2</option>
   								</select>
   							</td>
						</tr>
 					</table>		
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 550px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablabordoc" name="mytablabordoc" class="ajaxSorter" border="1" style="text-align: center" >
                       	<thead >
                       		
                       		<tr style="font-size: 13px">
                       			<!--
                       			<th data-order = "feccos1">Fecha</th>
                       			-->
                       			<th data-order = "nomg">Granja</th>
                       			<th data-order = "folio2">Folio</th>
                       			<th data-order = "tipcos1">Estatus</th>
                       			<th data-order = "numcos1">Cosecha</th>	
                       			<!--
                       			<th data-order = "clicos1">Cliente</th>
                       			-->
                       			<th data-order = "pisg">Estanque</th>	
                       			<th data-order = "grscos">Gramos</th>
                       			<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez"){ ?>
                       			<th data-order = "prebas1">Precio Base</th>
                       			<th data-order = "pregra">P.B + gr.</th>
                       			<?php }?>
                       			<th data-order = "kgscos">Kilogramos</th>   
                       			<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez"){ ?>                    			
                       			<th data-order = "imp">Importe M.N.</th>
                       			<?php }?>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 12px; text-align: center">
                       	</tbody>                        	
                   	</table>
                </span> 
                
 			</div>
 		</div>		  
 		<div id="concentrado" class="tab_content" style="margin-top: -15px" >
 			<div class="ajaxSorterDiv" id="tabla_gral" name="tabla_gral" style=" margin-left: -10px;  margin-top: 1px;width:955px;">   
             	  <div class="ajaxpager" style="margin-top: -5px;" > 
            		<form action="<?= base_url()?>index.php/bordo/reporte" method="POST" >
            		<ul class="order_list" style="width: 860px; text-align: left; height: 25px " >
            			Reporte General - 
            			Granja
            			<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez"){ ?>
            			<select name="numgrabg" id="numgrabg" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   							<option value="0" >Seleccione</option>
   							<option value="4" >OA-Ahome</option><option value="10" >OA-Topolobampo</option>
   							<option value="6" >OA-Huatabampo</option><option value="5" >Oce. Azul</option><option value="2" >OA-Kino</option>
   						</select>
   						<?php }else{ ?>
   							<select name="numgrabg" id="numgrabg" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   							<option value="4" >OA-Ahome</option>
   						</select>
   						<?php } ?>
            			Sección	
            			<select name="seccg" id="seccg" style="font-size: 12px; height: 25px;margin-top: 1px;  " >	</select>
            			<img style="cursor: pointer" style="margin-top: -5px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            <input type="hidden" name="tabla" value ="" class="htmlTable"/>
            		</ul>
            		</form> 
 				</div>  
             	<span class="ajaxTable" style="height: 570px; background-color: white; width:950px; margin-top: 1px" >
            	   	<table id="mytablagral" name="mytablagral" class="ajaxSorter" border="1" style="text-align: center" >
                       	<thead >
                       		
                       		<tr style="font-size: 10px">
                       			<th rowspan="2" data-order = "pisg">Est</th>
                       			<th rowspan="2" data-order = "orgg">Orgs</th>
                       			<th rowspan="2" data-order = "hasg">Has</th>
                       			<th rowspan="2" data-order = "dc">DC</th>
                       			<!--<th rowspan="2" data-order = "ali">Alimento</th>-->	
                       			<th colspan="4">Primera</th>
                       			<th colspan="4">Segunda</th>	
                       			<th colspan="4">Tercera</th>
                       			<th colspan="4">Cuarta</th>
                       			<th colspan="4">Quinta</th>
                       			<th colspan="4">Sexta</th>
                       			<th colspan="4">Final</th>                       			
                       			<th colspan="5">Total</th>
                       		</tr>
                       		<tr style="font-size: 10px">
                       			<th data-order = "pp1">PP</th>
                       			<th data-order = "kgs1">Kgs</th>
                       			<th data-order = "pre1">$</th>
                       			<th data-order = "vta1">Importe</th>
                       			<th data-order = "pp2">PP</th>
                       			<th data-order = "kgs2">Kgs</th>
                       			<th data-order = "pre2">$</th>
                       			<th data-order = "vta2">Importe</th>
                       			<th data-order = "pp3">PP</th>
                       			<th data-order = "kgs3">Kgs</th>
                       			<th data-order = "pre3">$</th>
                       			<th data-order = "vta3">Importe</th>
                       			<th data-order = "pp4">PP</th>
                       			<th data-order = "kgs4">Kgs</th>
                       			<th data-order = "pre4">$</th>
                       			<th data-order = "vta4">Importe</th>
                       			<th data-order = "pp5">PP</th>
                       			<th data-order = "kgs5">Kgs</th>
                       			<th data-order = "pre5">$</th>
                       			<th data-order = "vta5">Importe</th>
                       			<th data-order = "pp6">PP</th>
                       			<th data-order = "kgs6">Kgs</th>
                       			<th data-order = "pre6">$</th>
                       			<th data-order = "vta6">Importe</th>
                       			<th data-order = "ppf">PP</th>
                       			<th data-order = "kgsf">Kgs</th>
                       			<th data-order = "pref">$</th>
                       			<th data-order = "vtaf">Importe</th>
                       			<th data-order = "kgst">Kgs</th>
                       			<th data-order = "ppt">PP</th>
                       			<!--
                       			<th data-order = "kgsha">Kgs/Ha</th>
                       			<th data-order = "fcat">FCA</th>-->
                       			<th data-order = "orgst">Orgs</th>
                       			<th data-order = "sob">% Sob</th>
                       			<th data-order = "vta">Venta</th>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 10px; text-align: center">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<button id="export_datag" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
			
				<form id="data_tablesg" action="<?= base_url()?>index.php/bordo/pdfrepcosg" method="POST">
					<input type="hidden" name="tablag" id="html_gral"/> <input type="hidden" name="gra1" id="gra1"/>
				</form>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#export_datag').click(function(){									
								$('#html_gral').val($('#mytablagral').html());
								$('#data_tablesg').submit();
								$('#html_gral').val('');
								
						});
					});
				</script>
 			
 		</div>
 	</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#cicloG").change( function(){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/bordo/ultimofolio", 
			data: "cic="+$("#cicloG").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#folio").val(obj.folio);

   			} 
	});	
	$("#filest").val($("#cicloG").val()+$("#ncicos").val()+$("#numgrab").val());
	$("#estcos").trigger("update");
});		

$("#nue").click( function(){	
	$("#estcos").val('');$("#grscos").val('');$("#kgscos").val('');$("#prebas").val('');
	$("#numgrab").val('');
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/bordo/ultimofolio", 
			data: "cic="+$("#cicloG").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#folio").val(obj.folio);
   			} 
	});	
	
	//var f = new Date(); 
	//mes=f.getMonth() +1; dia=f.getDate();
   	//if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	//if((f.getDate() )<=9) dia="0" + (f.getDate());
   	//$("#feccos").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#mytablabordo").trigger("update");
   	$("#mytablagral").trigger("update");$("#mytablagralb").trigger("update");
   	$("#mytablabordo").trigger("update");
	 return true;
});

$("#quitas").click( function(){	
	numero=$("#idcos").val();;
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/bordo/quitarCos", 
						data: "id="+$("#idcos").val()+"&cic="+$("#cicloG").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablabordo").trigger("update");$("#mytablabordoc").trigger("update");
										$("#mytablagral").trigger("update");$("#mytablagralb").trigger("update");
										$("#nue").click();$("#tipcos").val('');$("#numcos").val('');$("#clicos").val('Selectos del Mar S.A. de C.V.');$("#prebas").val('');
										$("#idcos").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar día de Muestra para poder Quitarlo");
		return false;
	}
});


$("#agregas").click( function(){	
	numero=$("#idcos").val();fecha=$("#feccos").val();tip=$("#tipcos").val();num=$("#numcos").val();cli=$("#clicos").val();est=$("#estcos").val();
	grs=$("#grscos").val();kgs=$("#kgscos").val();gra=$("#numgrab").val();
	if(fecha!=''){
	if(gra!='0'){	
	if(tip!='0'){
	if(num!='0'){
	if(cli!='0'){
	if(est!='0'){
	if(grs!=''){
	if(kgs!=''){				
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/bordo/actualizaCos", 
						data: "id="+$("#idcos").val()+"&fec="+$("#feccos").val()+"&tip="+$("#tipcos").val()+"&num="+$("#numcos").val()+"&cli="+$("#clicos").val()+"&est="+$("#estcos").val()+"&grs="+$("#grscos").val()+"&kgs="+$("#kgscos").val()+"&pre="+$("#prebas").val()+"&gra="+$("#numgrab").val()+"&fol="+$("#folio").val()+"&cic="+$("#cicloG").val()+"&nci="+$("#ncicos").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablabordo").trigger("update");$("#mytablabordoc").trigger("update");
										$("#mytablagral").trigger("update");$("#mytablagralb").trigger("update");
										$("#nue").click();
										$("#estcos").val('');$("#grscos").val('');$("#kgscos").val('');$("#prebas").val('');
										$("#idcos").val('');$("#tipcos").val('');$("#numcos").val('');$("#clicos").val('2');$("#prebas").val('');										
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/bordo/agregaCos", 
						data: "&fec="+$("#feccos").val()+"&tip="+$("#tipcos").val()+"&num="+$("#numcos").val()+"&cli="+$("#clicos").val()+"&est="+$("#estcos").val()+"&grs="+$("#grscos").val()+"&kgs="+$("#kgscos").val()+"&pre="+$("#prebas").val()+"&gra="+$("#numgrab").val()+"&fol="+$("#folio").val()+"&cic="+$("#cicloG").val()+"&nci="+$("#ncicos").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablabordo").trigger("update");$("#mytablagral").trigger("update");$("#mytablagralb").trigger("update");
										$("#mytablabordoc").trigger("update");
										$("#nue").click();
										$("#estcos").val('');$("#grscos").val('');$("#kgscos").val('');$("#prebas").val('');
										$("#idcos").val('');$("#estcos").focus();										
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});		
		}
	}else{
		alert("Error: Necesita Registrar Kilogramos");
		$("#kgscos").focus();
		return false;
	}	
	}else{
		alert("Error: Necesita Registrar Gramos");
		$("#grscos").focus();
		return false;
	}	
	}else{
		alert("Error: Necesita seleccionar Estanque");
		$("#estcos").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Registrar Destio y/o Cliente");
		$("#clicos").focus();
		return false;
	}	
	}else{
		alert("Error: Necesita seleccionar Cosecha");
		$("#numcos").focus();
		return false;
	}
	}else{
		alert("Error: Necesita seleccionar Tipo de Salida");
		$("#tipcos").focus();
		return false;
	}
	}else{
		alert("Error: Necesita seleccionar Granja");
		$("#numgrab").focus();
		return false;
	}
	}else{
		alert("Error: Necesita seleccionar Fecha");
		$("#feccos").focus();
		return false;
	}		
});

$("#fecdia").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",		
});

$("#feccos").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",	
	onSelect: function( selectedDate ){
		$("#mytablabordo").trigger("update");
		$("#nue").click();
		$("#idcos").val('');$("#tipcos").val('');$("#numcos").val('');$("#clicos").val('');
		$("#dia").val($("#feccos").val());
	}
});


jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

$("#tipcos").change( function(){
	$("#tip").val($("#tipcos").val());
});

$("#numgrabgb").change( function(){
	$("#numgrab").val($("#numgrabgb").val());$("#gra").val($("#numgrabgb").val());
	 $('#estcos').boxLoader({
            url:'<?php echo base_url()?>index.php/bordo/combob',
            equal:{id:'numgra',value:'#numgrab'},
            select:{id:'idpis',val:'val'},
            all:"Sel.",
            
    });
    
});
		

$("#numgrabg").change( function(){
	$("#gra1").val($("#numgrabg").val());
});	
$("#numgrab").change( function(){
	$("#filest").val($("#cicloG").val()+$("#ncicos").val()+$("#numgrab").val());$("#estcos").trigger("update");
});	
$("#ncicos").change( function(){
	$("#filest").val($("#cicloG").val()+$("#ncicos").val()+$("#numgrab").val());$("#estcos").trigger("update");
});
$(document).ready(function(){ 
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	$("#feccos").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#dia").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#tip").val(0);
   	$("#cicloG").change();
   	$("#numgrabg").change();
   	$("#filest").val($("#cicloG").val()+$("#ncicos").val()+$("#numgrab").val());
   	//$("#clicos").val('2');
	$("#tabla_bordo").ajaxSorter({
		url:'<?php echo base_url()?>index.php/bordo/tablabordo', 
		filters:['cicloG','feccos','numgrab','tipcos','folio'],
		sort:false,	
		onRowClick:function(){
		if($(this).data('idcos')>0){
			$("#idcos").val($(this).data('idcos'));
			$("#feccos").val($(this).data('feccos'));
       		$("#clicos").val($(this).data('clicos'));
       		$("#estcos").val($(this).data('estcos'));
       		$("#grscos").val($(this).data('grscos'));
       		$("#kgscos").val($(this).data('kgscos'));       		
       		$("#tipcos").val($(this).data('tipcos'));
       		$("#prebas").val($(this).data('prebas'));
       		$("#numcos").val($(this).data('numcos'));
       		$("#numgrab").val($(this).data('numgrab'));
       		
       		$("#folio").val($(this).data('folio'));
       		
       	}
   		},   
		onSuccess:function(){ 
			$('#tabla_bordo tbody tr').map(function(){	    		
	    		if($(this).data('pisg')=='Total'){
	    			$(this).css('background','lightblue');	 $(this).css('font-weight','bold');   				    				    			    			
	    		}    
	    		if($(this).data('feccos1')=='Total Dia'){
	    			$(this).css('background','lightgray');	 $(this).css('font-weight','bold');   				    				    			    			
	    		}	
	    	}); 
		},	
	});
	$("#tabla_bordoc").ajaxSorter({
		url:'<?php echo base_url()?>index.php/bordo/tablabordoc', 
		filters:['cicloG','numgrabc','estcosc','ncicosg'],
		sort:false,	
		onRowClick:function(){
		if($(this).data('idcos')>0){
			//$("#tabs" ).tabs( "bordo", "active", 1 );
			//$("#tabs").tabs({ active: 2});
			$('a[href="#bordo"]').click();
			$("#idcos").val($(this).data('idcos'));
			$("#feccos").val($(this).data('feccos'));
       		$("#clicos").val($(this).data('clicos'));
       		$("#estcos").val($(this).data('estcos'));
       		$("#grscos").val($(this).data('grscos'));
       		$("#kgscos").val($(this).data('kgscos'));       		
       		$("#tipcos").val($(this).data('tipcos'));
       		$("#prebas").val($(this).data('prebas'));
       		$("#numcos").val($(this).data('numcos'));
       		$("#numgrab").val($(this).data('numgrab'));
       		
       		$("#folio").val($(this).data('folio'));
       		
       	}
   		},   
		onSuccess:function(){ 
			$('#tabla_bordo tbody tr').map(function(){	    		
	    		if($(this).data('pisg')=='Total'){
	    			$(this).css('background','lightblue');	 $(this).css('font-weight','bold');   				    				    			    			
	    		}    
	    		if($(this).data('feccos1')=='Total Dia'){
	    			$(this).css('background','lightgray');	 $(this).css('font-weight','bold');   				    				    			    			
	    		}	
	    	}); 
		},	
	});
	$("#tabla_gralb").ajaxSorter({
		url:'<?php echo base_url()?>index.php/bordo/tablagralb', 
		filters:['cicloG','numgrabgb'],
		sort:false,	
		onRowClick:function(){
			$("#feccos").val($(this).data('feccos'));
			$("#dia").val($(this).data('feccos'));
			$("#folio").val($(this).data('folio'));$("#fol").val($(this).data('folio'));$("#gra").val($(this).data('numgrab'));
			$("#clicos").val($(this).data('clicos'));$("#cli").val($(this).data('razon'));$("#numgrab").val($(this).data('numgrab'));
			$("#mytablabordo").trigger("update");
			$('#estcos').boxLoader({
           		 url:'<?php echo base_url()?>index.php/bordo/combob',
            	 equal:{id:'numgra',value:'#numgrab'},
            	 select:{id:'idpis',val:'val'},
            	 all:"Sel.",
            });
       	}, 
		onSuccess:function(){ 
			$('#tabla_gralb tbody tr').map(function(){
		    	if($(this).data('pisg')=='Total') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    })		
		},	
	});
	$("#tabla_gral").ajaxSorter({
		url:'<?php echo base_url()?>index.php/bordo/tablagral', 
		filters:['cicloG','numgrabg','seccg'],
		sort:false,	
		onSuccess:function(){ 
			$('#tabla_gral tbody tr').map(function(){
		    	if($(this).data('pisg')=='Total') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    })		
		},	
	});
	
	$('#estcosB').boxLoader({
            url:'<?php echo base_url()?>index.php/bordo/combob',
            equal:{id:'cicbor',value:'#cicbor'},
            //equal:{id:'secc',value:'#secc'},
            select:{id:'idpis',val:'val'},
            all:"Sel.",
            
    });
    
	 $('#estcos').boxLoader({
            url:'<?php echo base_url()?>index.php/bordo/combobp',
            equal:{id:'filest',value:'#filest'},
            //equal:{id:'secc',value:'#secc'},
            select:{id:'idpis',val:'val'},
           // onLoad:false,
            all:"Sel.",
            
    });
    $('#estcosc').boxLoader({
            url:'<?php echo base_url()?>index.php/bordo/combob',
            equal:{id:'numgra',value:'#numgrabc'},
            //equal:{id:'secc',value:'#secc'},
            select:{id:'idpis',val:'val'},
            all:"Sel.",
            
    });
    $('#seccg').boxLoader({
            url:'<?php echo base_url()?>index.php/bordo/secciones',
            equal:{id:'numgrabg',value:'#numgrabg'},
            select:{id:'secc',val:'val'},
            all:"Sel.",
            
    });
});
</script>