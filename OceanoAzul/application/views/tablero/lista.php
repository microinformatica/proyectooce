<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#registro" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/bomba.png" width="25" height="25" border="0"> Registro de Equipos </a></li>
		<?php if($usuario=="Jesus Benítez" || $usuario=="Joaquin Domínguez"){ ?>	
		<li><a href="#control" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/tab1.png" width="25" height="25" border="0"> Tablero de Control </a></li>
		<li><a href="#mes" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/tecnicos.png" width="20" height="20" border="0"> Mes </a></li>
		<?php } ?>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px;" >
		<div id="registro" class="tab_content" style="margin-top: -5px" >
			<table border="0" height="450px" width="900px">
				<tr>
					<th>
						<div class="ajaxSorterDiv" id="tabla_eqp" name="tabla_eqp" style="width: 459px;height: 475px; margin-top: -15px ">                
               				<span class="ajaxTable" style="margin-left: 0px; height: 450px; width: 457px">
                				<table id="mytablaE" name="mytablaE" class="ajaxSorter">
                   					<thead >                            
                   						<th style="font-size: 11px" data-order = "num" >No.</th>                                                                                    
                   						<th style="font-size: 11px" data-order = "fec1" >Fecha</th>
                   						<th style="font-size: 11px" data-order = "tipo1" >Tipo</th>
                   						<?php if($usuario!="Joaquin Domínguez" && $usuario!="Miguel Mendoza"){ ?>
                   						<th style="font-size: 11px" data-order = "pre1" >Precio</th>
                   						<?php } ?>
                   						<th style="font-size: 11px" data-order = "nom" >Ubicación</th>
                   						<?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?>
                   						<th style="font-size: 11px" data-order = "fec2" >Día</th>
                   						<?php } ?>
                   					</thead>
                   					<tbody style="font-size: 10px">                                                 	  
                   					</tbody>
                				</table>
                	 		</span> 
            			</div>
                	</th>
					<th>
						<table style="width:458px" border="2px"  >
							<thead style="background-color: #DBE5F1" >
								<th colspan="3" height="15px" style="font-size: 20px">Datos de Equipo
									<input type="hidden" readonly="true" size="4%" type="text" name="ideqp" id="ideqp" style="text-align: right;  border: 0">
									<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="nuevoe" name="nuevoe" value="Nuevo" />
									<input style="font-size: 12px" type="submit" id="aceptare" name="aceptare" value="Guardar" />
 									<!--<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="borrare" name="borrare" value="Borrar" />--> 								
 								</th>
							</thead>
							<tbody style="background-color: #F7F7F7">
								<tr >
									<th style="text-align: center; border-right: none">Tipo</th>									
									<th colspan="2" style="text-align: center; border-left: none">Modelo</th>
								</tr>
								
								<tr style="background-color: white;">
									<th style="text-align: center" align="center">
									<select <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="tip" id="tip" style="font-size: 14px;height: 25px; margin-top: 1px;">
      										<option value="0" > Seleccionar </option>
      										<option value="9" > Agua </option>
      										<option value="1" > Bomba </option>
      										<option value="6" > Blower </option>
      										<option value="7" > Calentador </option>
      										<option value="8" > Chiller </option>
      										<option value="2" > Filtro </option>
      										<option value="5" > Intercambiador </option>
      										<option value="3" > Ozono </option>
      										<option value="4" > U.V. </option>
      								 </select>
    								</th>
    								<th colspan="2" style="text-align: center" align="center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="45%" type="text" name="mod" id="mod"></th>
    						</tr>
    						<tr >
									<th style="text-align: center; border-right: none">Departamento</th>
									<th style="text-align: center; border-left: none; border-right: none">Ubicación</th>
									<th style="text-align: center; border-left: none">Día</th>
									
							</tr>
    						<tr >
									<th style="text-align: center" align="center">
    									<select <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="dep" id="dep" style="font-size: 14px;height: 25px; margin-top: 1px;">
      										<option value="1" > Mantenimiento I </option>
      										<option value="2" > Mantenimiento II </option>      										
									  </select>
    								</th>
									<th style="text-align: center" align="center">
    								  <select <?php if($usuario!="Joaquin Domínguez" && $usuario!="Miguel Mendoza"){ ?> disabled="true" <?php } ?> name="ubi" id="ubi" style="font-size: 12px;height: 25px; margin-top: 1px; width: 140px"></select>	
    								</th>
    								<th style="text-align: center"><input <?php if($usuario!="Joaquin Domínguez" && $usuario!="Miguel Mendoza"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="feca" id="feca" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>
							</tr>
    							<tr>
    								<th colspan="3" height="15px" style="font-size: 20px;background-color: #DBE5F1">Datos de Proveedor</th>
    							</tr>
    							<tr >
									<th style="text-align: center; border-right: none">Fecha</th>
									<th style="text-align: center;border-left: none; border-right: none">Factura</th>	
									<th style="text-align: center;border-left: none">Precio</th>
											
								</tr>
								<tr >
									<th style="text-align: center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="fec" id="fec" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>
									<th style="text-align: center"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="15%" type="text" name="fac" id="fac"></th>
									<th style="text-align: center">$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="15%" type="text" name="pre" id="pre" style="text-align: right" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" ></th>	
								</tr>
								
    							<tr >
									<th colspan="3" style="text-align: center">Nombre</th>
								</tr>
								<tr style="background-color: white;">
									<th colspan="3" style="text-align: Left"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="70%" type="text" name="pro" id="pro">
 										<input type="hidden" size="3%" name="idpro" id="idpro" />
 									</th>
    							</tr>
    							<tr>
    								<th colspan="3">
    								<div style="height: 240px"  >                			              
    									<?php if($usuario=="Jesus Benítez" || $usuario=="Eduardo Lizárraga"){ ?>
                						<div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" style="width: 442px;height: 240px ">                
               			     				<span class="ajaxTable" style="margin-left: 0px; height: 210px; width: 440px;">
                    							<table id="mytablaC" name="mytablaC" class="ajaxSorter" >
                        							<thead >                            
                        								<th style="font-size: 10px" data-order = "Razon" >Proveedor</th>                                                                                    
                        							</thead>
                        							<tbody style="font-size: 9px">                                                 	  
                        							</tbody>
                    							</table>
                			 				</span> 
            							</div>
            							<?php } ?>
                					</div>
                					</th> 		
    							</tr>
							</tbody>
						</table>
					</th>	
				</tr>
			</table>
		</div>
		<?php if($usuario=="Jesus Benítez" || $usuario=="Joaquin Domínguez"){ ?>
		<div id="control" class="tab_content" style="margin-top: -5px" >
			<table border="3" width="750px" height="450px">
				<tr >
					<th rowspan="3">
			<table border="1"  style="margin-top: -50px">
				<tr >
					<th style="background-color: black; color: white;">Área</th>
					<th colspan="24" style="background-color: yellow;text-align: center">Lar</th>
					<th colspan="18" style="background-color: orange;text-align: center">Mad</th>					
					<th colspan="8" style="background-color: lightcoral;text-align: center">Mic</th>
					<th colspan="4" style="background-color: lightgreen;text-align: center">Cos</th>
					<th colspan="8" style="background-color: lightgray;text-align: center">Ext</th>
				</tr>
				<tr >
					<th  style="background-color: black; color: white; text-align: right">Puntas</th>
					<th colspan="6" style="text-align: center; border-right: none"><input type="submit" onclick="seleccion(1)" value="1" /></th>
					<th colspan="6" style="text-align: center; border-left: none; border-right: none"><input type="submit" onclick="seleccion(2)" value="2" /></th>
					<th colspan="6" style="text-align: center;border-left: none; border-right: none"><input type="submit" onclick="seleccion(3)" value="3" /></th>
					<th colspan="6" style="text-align: center;border-left: none; border-right: none"><input type="submit" onclick="seleccion(4)" value="4" /></th>
					<th colspan="6" style="text-align: center;border-left: none; border-right: none"><input type="submit" onclick="seleccion(5)" value="5" /></th>
					<th colspan="6" style="text-align: center;border-left: none; border-right: none"><input type="submit" onclick="seleccion(6)" value="6" /></th>
					<th colspan="6" style="text-align: center;border-left: none; border-right: none"><input type="submit" onclick="seleccion(108)" value="7" /></th>
					<th colspan="4" style="text-align: center; border-right: none"><input type="submit" id="mce1" name="mce1" onclick="seleccion(7)" value="1" /></th>
					<th colspan="4" style="text-align: center;border-left: none; border-right: none"><input type="submit" onclick="seleccion(8)" value="2" /></th>
					<th colspan="4" style="text-align: center;border-left: none; border-right: none"><input type="submit" onclick="seleccion(9)" value="3" /></th>
					<th colspan="4" style="text-align: center;border-left: none"><input type="submit" onclick="seleccion(10)" value="4" /></th>					
					<th colspan="4" style="text-align: center;border-left: none"><input type="submit" onclick="seleccion(109)" value="5" /></th>
				</tr>
				<tr>
					<th style="background-color: black; color: white; text-align: left">Reservorio</th>
					<th colspan="62" style="background-color: lightblue; color: white"></th>
				</tr>	
				<tr>
					<th  style="background-color: black; color: white; text-align: right">Bombas</th>
					<th colspan="24" style="text-align: center;background-color: yellow"><input type="submit" onclick="seleccion(11)" value="1" /></th>
					<th colspan="9" style="text-align: center;background-color: orange;border-left: none; border-right: none"><input type="submit" onclick="seleccion(12)" value="1" /></th>
					<th colspan="9" style="text-align: center;background-color: orange;border-left: none; border-right: none"><input type="submit" onclick="seleccion(13)" value="2" /></th>
					<th colspan="10" style="text-align: center; border-right: none"><input type="submit" onclick="seleccion(14)" value="1" /></th>
					<th colspan="10" style="text-align: center;border-left: none; border-right: none"><input type="submit" onclick="seleccion(15)" value="2" /></th>
				</tr>
				<tr>
					<th style="background-color: black; color: white; text-align: right">Filtros</th>
					<th colspan="6" style="text-align: center;background-color: yellow; border-right: none"><input type="submit" onclick="seleccion(16)" value="1" /></th>
					<th colspan="6" style="text-align: center;background-color: yellow;border-left: none; border-right: none"><input type="submit" onclick="seleccion(17)" value="2" /></th>
					<th colspan="6" style="text-align: center;background-color: yellow;border-left: none; border-right: none"><input type="submit" onclick="seleccion(18)" value="3" /></th>
					<th colspan="6" style="text-align: center;background-color: yellow;border-left: none; "><input type="submit" onclick="seleccion(19)" value="4" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-right: none"><input type="submit" onclick="seleccion(20)" value="1" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-left: none; border-right: none"><input type="submit" onclick="seleccion(21)" value="2" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-left: none; border-right: none"><input type="submit" onclick="seleccion(22)" value="3" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-left: none; border-right: none"><input type="submit" onclick="seleccion(23)" value="4" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-left: none; border-right: none"><input type="submit" onclick="seleccion(24)" value="5" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-left: none; "><input type="submit" onclick="seleccion(25)" value="6" /></th>
					<th colspan="5" style="text-align: center;border-right: none"><input type="submit" onclick="seleccion(26)" value="1" /></th>
					<th colspan="5" style="text-align: center;border-left: none; border-right: none"><input type="submit" onclick="seleccion(27)" value="2" /></th>
					<th colspan="5" style="text-align: center;border-left: none; border-right: none"><input type="submit" onclick="seleccion(28)" value="3" /></th>
					<th colspan="5" style="text-align: center;border-left: none; "><input type="submit" onclick="seleccion(29)" value="4" /></th>	
				</tr>
				<tr>
					<th style="background-color: black; color: white; text-align: right">Ozono</th>
					<th colspan="24" style="text-align: center;background-color: yellow"><input type="submit" onclick="seleccion(30)" value="1" /></th>
					<th colspan="18" style="text-align: center;background-color: orange"><input type="submit" onclick="seleccion(31)" value="1" /></th>
					<th colspan="20" style="text-align: center"><input type="submit" onclick="seleccion(32)" value="1" /></th>
				</tr>
				<tr>
					<th style="background-color: black; color: white; text-align: left">Reservorio</th>
					<th colspan="62" style="background-color: lightblue; color: white"></th>
				</tr>	
				<tr>
					<th style="background-color: black; color: white; text-align: right">Bombas</th>
					<th colspan="24" style="text-align: center;background-color: yellow"><input type="submit" onclick="seleccion(33)" value="1" /></th>
					<th colspan="9" style="text-align: center;background-color: orange;border-right: none;"><input type="submit" onclick="seleccion(34)" value="1" /></th>
					<th colspan="9" style="text-align: center;background-color: orange;border-left: none;"><input type="submit" onclick="seleccion(35)" value="2" /></th>
					<th colspan="8" style="text-align: center;background-color: lightcoral"><input type="submit" onclick="seleccion(36)" value="1" /></th>
					<th colspan="4" style="text-align: center;background-color: lightgreen"><input type="submit" onclick="seleccion(37)" value="1" /></th>
					<th colspan="8" style="text-align: center;background-color: lightgray"><input type="submit" onclick="seleccion(38)" value="1" /></th>
				</tr>
				<tr>
					<th style="background-color: black; color: white; text-align: right">Filtros</th>
					<th colspan="3" style="text-align: center;background-color: yellow;border-right: none"><input type="submit" onclick="seleccion(39)" value="1" /></th>
					<th colspan="3" style="text-align: center;background-color: yellow;border-left: none; border-right: none"><input type="submit" onclick="seleccion(40)" value="2" /></th>
					<th colspan="3" style="text-align: center;background-color: yellow;border-left: none; border-right: none"><input type="submit" onclick="seleccion(41)" value="3" /></th>
					<th colspan="3" style="text-align: center;background-color: yellow;border-left: none; border-right: none"><input type="submit" onclick="seleccion(42)" value="4" /></th>
					<th colspan="3" style="text-align: center;background-color: yellow;border-left: none; border-right: none"><input type="submit" onclick="seleccion(43)" value="5" /></th>
					<th colspan="3" style="text-align: center;background-color: yellow;border-left: none; border-right: none"><input type="submit" onclick="seleccion(44)" value="6" /></th>
					<th colspan="3" style="text-align: center;background-color: yellow;border-left: none; border-right: none"><input type="submit" onclick="seleccion(45)" value="7" /></th>
					<th colspan="3" style="text-align: center;background-color: yellow; border-right: none"><input type="submit" onclick="seleccion(46)" value="8" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-left: none;"><input type="submit" onclick="seleccion(47)" value="1" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-left: none; border-right: none"><input type="submit" onclick="seleccion(48)" value="2" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-left: none; border-right: none"><input type="submit" onclick="seleccion(49)" value="3" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-left: none; border-right: none"><input type="submit" onclick="seleccion(50)" value="4" /></th>
					<th colspan="3" style="text-align: center;background-color: orange;border-left: none; border-right: none"><input type="submit" onclick="seleccion(51)" value="5" /></th>
					<th colspan="3" style="text-align: center;background-color: orange; border-right: none"><input type="submit" onclick="seleccion(52)" value="6" /></th>
					<th colspan="2" style="text-align: center;background-color: lightcoral;border-right: none"><input type="submit" onclick="seleccion(53)" value="1" /></th>
					<th colspan="2" style="text-align: center;background-color: lightcoral;border-left: none; border-right: none"><input type="submit" onclick="seleccion(54)" name="fmce6" value="2" /></th>
					<th colspan="2" style="text-align: center;background-color: lightcoral;border-left: none; border-right: none"><input type="submit" onclick="seleccion(55)" name="fmce7" value="3" /></th>
					<th colspan="2" style="text-align: center;background-color: lightcoral;border-left: none; "><input type="submit" onclick="seleccion(56)" value="4" /></th>
					<th colspan="2" style="text-align: center;background-color: lightgreen; border-right: none"><input type="submit" onclick="seleccion(57)" value="1" /></th>
					<th colspan="2" style="text-align: center;background-color: lightgreen;border-left: none; "><input type="submit" onclick="seleccion(58)" value="2" /></th>
					<th colspan="2" style="text-align: center;background-color: lightgray; border-right: none"><input type="submit" onclick="seleccion(59)" value="1" /></th>
					<th colspan="2" style="text-align: center;background-color: lightgray;border-left: none; border-right: none"><input type="submit" onclick="seleccion(60)" value="2" /></th>
					<th colspan="2" style="text-align: center;background-color: lightgray;border-left: none; border-right: none"><input type="submit" onclick="seleccion(61)" value="3" /></th>
					<th colspan="2" style="text-align: center;background-color: lightgray;border-left: none; "><input type="submit" onclick="seleccion(62)" value="4" /></th>
					
				</tr>
				<tr>
					<th style="background-color: black; color: white; text-align: right">Blower</th>
					<th colspan="24" style="text-align: center;background-color: yellow"><input type="submit" onclick="seleccion(74)" value="1" /></th>
					<th colspan="18" style="text-align: center;background-color: orange;border-right: none;"><input type="submit" onclick="seleccion(75)" value="1" /></th>
					<th colspan="8" style="text-align: center;background-color: lightcoral"></th>
					<th colspan="4" style="text-align: center;background-color: lightgreen"></th>
					<th colspan="8" style="text-align: center;background-color: lightgray"></th>
				</tr>
				<tr>
					<th style="background-color: black; color: white; text-align: right">U.V.</th>
					<th colspan="12" style="text-align: center;background-color: yellow;border-right: none"><input type="submit" onclick="seleccion(63)" value="1" /></th>
					<th colspan="12" style="text-align: center;background-color: yellow;border-left: none; "><input type="submit" onclick="seleccion(64)" value="2" /></th>
					<th colspan="9" style="text-align: center;background-color: orange; border-right: none"><input type="submit" onclick="seleccion(65)" value="1" /></th>
					<th colspan="9" style="text-align: center;background-color: orange;border-left: none; "><input type="submit" onclick="seleccion(66)" value="2" /></th>
					<th colspan="8" style="text-align: center;background-color: lightcoral"><input type="submit" onclick="seleccion(67)" value="1" /></th>
					<th colspan="4" style="text-align: center;background-color: lightgreen"><input type="submit" onclick="seleccion(68)" value="1" /></th>
					<th colspan="4" style="text-align: center;background-color: lightgray; border-right: none"><input type="submit" onclick="seleccion(69)" value="1" /></th>
					<th colspan="4" style="text-align: center;background-color: lightgray;border-left: none; "><input type="submit" onclick="seleccion(70)" value="2" /></th>
				</tr>	
				<tr>
					<th style="background-color: black; color: white; text-align: right">Intercambiador</th>
					<th colspan="24" style="text-align: center;background-color: yellow"><input type="submit" onclick="seleccion(71)" value="1" /></th>
					<th colspan="9" style="text-align: center;background-color: orange; border-right: none"><input type="submit" onclick="seleccion(72)" value="1" /></th>
					<th colspan="9" style="text-align: center;background-color: orange;border-left: none; "><input type="submit" onclick="seleccion(73)" value="2" /></th>
					<th colspan="8" style="text-align: center;background-color: lightcoral"></th>
					<th colspan="4" style="text-align: center;background-color: lightgreen"></th>
					<th colspan="8" style="text-align: center;background-color: lightgray"></th>
				</tr>
				<tr>
					<th style="background-color: black; color: white; text-align: right">Calderas</th>
					<th colspan="12" style="text-align: center;background-color: yellow;border-right: none"><input type="submit" onclick="seleccion(76)" value="1" /></th>
					<th colspan="12" style="text-align: center;background-color: yellow;border-left: none; "><input type="submit" onclick="seleccion(77)" value="2" /></th>
					<th colspan="9" style="text-align: center;background-color: orange; border-right: none"><input type="submit" onclick="seleccion(78)" value="1" /></th>
					<th colspan="9" style="text-align: center;background-color: orange;border-left: none; "><input type="submit" onclick="seleccion(79)" value="2" /></th>
					<th colspan="8" style="text-align: center;background-color: lightcoral"></th>
					<th colspan="4" style="text-align: center;background-color: lightgreen"></th>
					<th colspan="8" style="text-align: center;background-color: lightgray"></th>
				</tr>
				<tr>
					<th style="background-color: black; color: white; text-align: right">Chiller</th>
					<th colspan="24" style="text-align: center;background-color: yellow;"> </th>
					<th colspan="18" style="text-align: center;background-color: orange; border-right: none"><input type="submit" onclick="seleccion(80)" value="1" /></th>
					<th colspan="8" style="text-align: center;background-color: lightcoral"></th>
					<th colspan="4" style="text-align: center;background-color: lightgreen"></th>
					<th colspan="8" style="text-align: center;background-color: lightgray"></th>
				</tr>	
				
				<tr>
					<th rowspan="4" style="background-color: black; color: white; text-align: right">Blower</th>
					<th colspan="24" style="text-align: center;background-color: yellow;border-right: none"><input type="submit" onclick="seleccion(81)" value="100" />
					<input type="submit" onclick="seleccion(82)" value="200" />
					<input type="submit" onclick="seleccion(83)" value="300" />
					</th>
					<th rowspan="4" colspan="9" style="text-align: center;background-color: orange; border-right: none"><input type="submit" onclick="seleccion(91)" value="A" /></th>
					<th rowspan="4" colspan="9" style="text-align: center;background-color: orange;border-left: none; "><input type="submit" onclick="seleccion(92)" value="B-C" /></th>
					<th rowspan="4" colspan="8" style="text-align: center;background-color: lightcoral"><input type="submit" onclick="seleccion(93)" value="Mic" />
					 	<input type="submit" onclick="seleccion(101)" value="Art" />
					</th>
					<th rowspan="4" colspan="4" style="text-align: center;background-color: lightgreen"></th>
					<th rowspan="4" colspan="8" style="text-align: center;background-color: lightgray">
						<input type="submit" onclick="seleccion(94)" value="A1" /><input type="submit" onclick="seleccion(95)" value="A2" />
						<input type="submit" onclick="seleccion(96)" value="B1" /><input type="submit" onclick="seleccion(97)" value="B2" />
						<input type="submit" onclick="seleccion(98)" value="C1" /><input type="submit" onclick="seleccion(99)" value="C2" />
						<input type="submit" onclick="seleccion(100)" value="Res" />
					</th>
				</tr>
				<tr>
					
					<th colspan="24" style="text-align: center;background-color: yellow;border-right: none">
					<input type="submit" onclick="seleccion(84)" value="400" />
					<input type="submit" onclick="seleccion(85)" value="500" />
					<input type="submit" onclick="seleccion(86)" value="600" />
					</th>
				</tr>
				<tr>
					
					<th colspan="24" style="text-align: center;background-color: yellow;border-right: none">
					<input type="submit" onclick="seleccion(87)" value="700" />
					<input type="submit" onclick="seleccion(88)" value="800" />
					<input type="submit" onclick="seleccion(89)" value="900" />
					</th>
				</tr>
				<tr>
					
					<th colspan="24" style="text-align: center;background-color: yellow;border-right: none">
					<input type="submit" onclick="seleccion(90)" value="1000" /></th>
				</tr>
				<tr>
					<th style="background-color: black; color: white; text-align: right">Línea Agua</th>
					<th colspan="24" style="text-align: center;background-color: yellow"><input type="submit" onclick="seleccion(102)" value="M" /></th>
					<th colspan="18" style="text-align: center;background-color: orange;border-right: none;"><input type="submit" onclick="seleccion(103)" value="M" /></th>
					
					<th colspan="8" style="text-align: center;background-color: lightcoral"><input type="submit" onclick="seleccion(104)" value="P" /></th>
					<th colspan="4" style="text-align: center;background-color: lightgreen"><input type="submit" onclick="seleccion(105)" value="P" /></th>
					<th colspan="8" style="text-align: center;background-color: lightgray"><input type="submit" onclick="seleccion(106)" value="M" /><input type="submit" onclick="seleccion(107)" value="P" /></th>
				</tr>
			</table>
			</th>
			<th>
					Ubicación:<input id="ubica" name="ubica" size="45%" style="border: none" />
					<input type="hidden"  id="equisel" name="equisel" />
				</th>
			<tr>
				<th>
					
					<div class="ajaxSorterDiv" id="tabla_eqpsele" name="tabla_eqpsele" style="width: 459px; margin-top: -8px;height: 385px; ">                
               				<span class="ajaxTable" style="margin-left: 0px; height: 345px; width: 457px">
                				<table id="mytablaESel" name="mytablaESel" class="ajaxSorter">
                   					<thead >                            
                   						<th style="font-size: 11px" data-order = "fec3" >Día</th>
                   						<th style="font-size: 11px" data-order = "tipo2" >Detalles Equipo</th>
                   						<!---<th style="font-size: 11px" data-order = "fec2" >Fecha</th>-->
                   					</thead>
                   					<tbody style="font-size: 10px">                                                 	  
                   					</tbody>
                				</table>
                	 		</span> 
                	 		<div class="ajaxpager" style="margin-top: -10px" >        
	                   			<ul class="order_list" ></ul>  
	                   				<form id="data_tableqpdet" method="post" action="<?= base_url()?>index.php/tablero/pdfrepeqpdet" >
                						<input type="hidden" id="eqpubi" name="eqpubi" />
                						<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" id="export_dataeqp" src="<?= base_url()?>assets/img/sorter/pdf.png"  />                                                                      
                        				<input type="hidden" name="tabla"  id="html_eqpdet"/>   
                        			</form>   
 									<script type="text/javascript">
										$(document).ready(function(){
											$('#export_dataeqp').click(function(){									
												$('#html_eqpdet').val($('#mytablaESel').html());
												$('#eqpubi').val($('#ubica').val());
												$('#data_tableqpdet').submit();
												$('#html_eqpdet').val('');									
											});
										});
									</script>  
                			</div> 
                			
            			</div>
				</th>
			</tr>
			<tr>
				<th>
					<input style="font-size: 14px" type="submit" id="agrega" name="agrega" value="+" title="AGREGAR y ACTUALIZAR DETALLE" />
					<input style="font-size: 14px" type="submit" id="quita" name="quita" value="-" title="QUITAR DETALLE" />
					Dia: <input type="hidden"  size="3%" type="text" name="tiposel" id="tiposel" style="text-align: center;">
					<input <?php if($usuario!="Joaquin Domínguez" && $usuario!="Miguel Mendoza"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="fecserv" id="fecserv" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
					<br />
					Detalle:<input size="65%" type="text" name="nombre" id="nombre">
					<input type="hidden"  size="3%" type="text" name="idserv" id="idserv" style="text-align: center;">					
				</th>	
			</tr>
			</tr>
			</table>
		</div>	
		<div id="mes" class="tab_content" style="margin-top: -5px" >
			<div style="height: 350px;">				
				<div class="ajaxSorterDiv" id="tabla_detmes" name="tabla_detmes" style="height: 470px; ">                
               		<div class="ajaxpager" style="margin-top: 0px" >        
	                   	<ul class="order_list" ></ul>  
	                   	Ciclo:  
	                   	<select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px;margin-top: 1px; margin-left:4px;" >
                    	<?php $ciclof=2014; $actual=date("Y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
							<?php  $actual-=1; } ?>
          				</select> 
          				Mes:
	                   	<select name="cmbMesT" id="cmbMesT" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
   										
   										<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   										<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   										<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   										<option value="10" >Octubre</option><option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   						</select>
   						Tipo:
   						<select name="tip2" id="tip2" style="font-size: 11px;height: 25px; margin-top: 1px;">
      										<option value="0" > Todos </option>
      										<option value="9" > Agua </option>
      										<option value="1" > Bomba </option>
      										<option value="6" > Blower </option>
      										<option value="7" > Calentador </option>
      										<option value="8" > Chiller </option>
      										<option value="2" > Filtro </option>
      										<option value="5" > Intercambiador </option>
      										<option value="3" > Ozono </option>
      										<option value="4" > U.V. </option>
      					</select>
   						<form id="data_tablestdet" method="post" action="<?= base_url()?>index.php/tablero/pdfrepdetmes" >
                			<input type="hidden" id="mez" name="mez" />
                			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" id="export_data" src="<?= base_url()?>assets/img/sorter/pdf.png"  />                                                                      
                        	<input type="hidden" name="tabla"  id="html_estdet"/>   
                        	<input type="hidden" name="cicd" id="cicd" ><input type="hidden" name="mesd" id="mesd" ><input type="hidden" name="tipd" id="tipd" >
                        </form>   
 						<script type="text/javascript">
							$(document).ready(function(){
								$('#export_data').click(function(){									
									$('#html_estdet').val($('#mytablaTM').html());
									$('#cicd').val($('#cmbCiclo').val());
									$('#mesd').val($('#cmbMesT').val());								
									$('#tipd').val($('#tip2').val());
									$('#data_tablestdet').submit();
									$('#html_estdet').val('');									
								});
							});
						</script>  
                	</div>  
               		<span class="ajaxTable" style="height: 430px; background-color: white; width: 915px; margin-top: -23px" >
            	       	<table id="mytablaTM" name="mytablaTM" class="ajaxSorter" border="1" >
                        	<thead >
                        		<th style="font-size: 8px" data-order = "nom">Ubi</th>
                        		<th  data-order = "d1">1</th><th data-order = "d2">2</th><th data-order = "d3">3</th>
                        		<th data-order = "d4">4</th><th data-order = "d5">5</th><th data-order = "d6">6</th>
                        		<th data-order = "d7">7</th><th data-order = "d8">8</th><th data-order = "d9">9</th><th data-order = "d10">10</th>
                        		<th data-order = "d11">11</th><th data-order = "d12">12</th><th data-order = "d13">13</th>
                        		<th data-order = "d14">14</th><th data-order = "d15">15</th><th data-order = "d16">16</th>
                        		<th data-order = "d17">17</th><th data-order = "d18">18</th><th data-order = "d19">19</th><th data-order = "d20">20</th>
                        		<th data-order = "d21">21</th><th data-order = "d22">22</th><th data-order = "d23">23</th>
                        		<th data-order = "d24">24</th><th data-order = "d25">25</th><th data-order = "d26">26</th>
                        		<th data-order = "d27">27</th><th data-order = "d28">28</th><th data-order = "d29">29</th><th data-order = "d30">30</th>
                        		<th data-order = "d31">31</th>
                        	</thead>
                        	<tbody >
                        	</tbody>                        	
                    	</table>
                	</span> 
             	</div>  
            	          	
            </div>
 		</div>
		<?php } ?> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#quita").click( function(){	
	numero=$("#idserv").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/tablero/quitarDet", 
						data: "id="+$("#idserv").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaESel").trigger("update");$("#mytablaTM").trigger("update");
										$("#nombre").val('');
   										$("#fecserv").val('');
   										$("#idserv").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Detalle de Equipo para poder Quitarlo");
		return false;
	}
});
$("#agrega").click( function(){	
	lugar=$("#equisel").val();
	numero=$("#idserv").val();
	fec=$("#fecserv").val();
	mod=$("#nombre").val();
	if(lugar!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/tablero/actualizaDet", 
						data: "id="+$("#idserv").val()+"&mod="+$("#nombre").val()+"&fec="+$("#fecserv").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaESel").trigger("update");$("#mytablaTM").trigger("update");
										$("#nombre").val('');
   										$("#fecserv").val('');
   										$("#idserv").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			if(fec!=''){
				if(mod!=''){
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/tablero/agregaDet", 
						data: "&mod="+$("#nombre").val()+"&fec="+$("#fecserv").val()+"&ubi="+$("#equisel").val()+"&tip="+$("#tiposel").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaESel").trigger("update");$("#mytablaTM").trigger("update");
										$("#nombre").val('');
   										$("#fecserv").val('');
   										$("#idserv").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			alert("Error: Registrar Datos de Equipo");
			$("#nombre").focus();
			return false;
		}
		}else{
			alert("Error: Seleccione Fecha para Registrar Detalle");
			$("#fecserv").focus();
			return false;
		}
		}
	}else{
		alert("Error: Necesita seleccionar Ubicación para Registro de Detalle");
		return false;
	}
});


function seleccion(sele){
	$("#equisel").val(sele);
	$("#mytablaESel").trigger("update");
	$("#nombre").val('');
   	$("#fecserv").val('');
   	$("#tiposel").val('');
   	$("#idserv").val('');	
}

$("#nuevoe").click( function(){
	$("#ideqp").val('');$("#tip").val('');$("#mod").val('');$("#dep").val('');$("#ubi").val('');$("#fec").val('');$("#feca").val('');
	$("#fac").val('');$("#pre").val('');$("#idpro").val('');$("#pro").val('');
 return true;
})

$("#borrare").click( function(){	
	numero=$("#ideqp").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/tablero/borrare", 
						data: "id="+$("#ideqp").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Certificado Eliminado");
										$("#nuevoc").click();
										$("#mytablae").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Equipo para poder Eliminarlo");
		return false;
	}
});

$("#aceptare").click( function(){		
	fec=$("#fec").val();pre=$("#pre").val();pro=$("#pro").val();
	numero=$("#ideqp").val();
	if( fec!=''){
	  if( pre!=''){
	  	if( pro!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/tablero/actualizare", 
						data: "id="+$("#ideqp").val()+"&tip="+$("#tip").val()+"&mod="+$("#mod").val()+"&dep="+$("#dep").val()+"&ubi="+$("#ubi").val()+"&fec="+$("#fec").val()+"&feca="+$("#feca").val()+"&fac="+$("#fac").val()+"&pre="+$("#pre").val()+"&pro="+$("#idpro").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevoe").click();
										$("#mytablaE").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/tablero/agregare", 
						data: "tip="+$("#tip").val()+"&mod="+$("#mod").val()+"&dep="+$("#dep").val()+"&ubi="+$("#ubi").val()+"&fec="+$("#fec").val()+"&feca="+$("#feca").val()+"&fac="+$("#fac").val()+"&pre="+$("#pre").val()+"&pro="+$("#idpro").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Equipo registrado correctamente");
										$("#nuevoe").click();
										$("#mytablaE").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		}else{
			alert("Error: Seleccione Proveedor");	
			$("#pro").focus();
			return false;
		}
		}else{
			alert("Error: Precio no valida");	
			$("#pre").focus();
			return false;
		}					
	}else{
		alert("Error: Fecha de Factura no valido");	
		$("#fec").focus();
		return false;
	}
});


$("#fec").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#feca").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#fecserv").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  



$(document).ready(function(){ 
	var f = new Date();
	$("#cmbMesT").val((f.getMonth() +1));
	$("#tabla_eqp").ajaxSorter({
		url:'<?php echo base_url()?>index.php/tablero/tablaequipos',  
		sort:false,	
   		onRowClick:function(){
   			$("#ideqp").val($(this).data('ideqp'));
   			$("#tip").val($(this).data('tipo'));
   			$("#mod").val($(this).data('mod'));
   			$("#dep").val($(this).data('depa'));
   			$("#ubi").val($(this).data('ubi'));
   			$("#fec").val($(this).data('fec'));
   			$("#feca").val($(this).data('feca'));
   			$("#fac").val($(this).data('fac'));
   			$("#pre").val($(this).data('pre'));
   			$("#pro").val($(this).data('pro'));
   			$("#idpro").val($(this).data('numero'));
   			$("#ubi").boxLoader({
        		url:'<?php echo base_url()?>index.php/tablero/combo',
        		equal:{id:'actual',value:'#tip'},
            	select:{id:'idcat',val:'val'},
            	all:"Seleccione Opcion",
    		});
   		},	
   		
   	});
   	
   	$("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facproveedores/tablaclientes',  
		multipleFilter:true, 
   		//onLoad:false,
   		sort:false,	
   		onRowClick:function(){
   			$("#pro").val($(this).data('razon'));
   			$("#idpro").val($(this).data('numero'));
   		},	
   		
   	});	
   	
   	$("#tabla_eqpsele").ajaxSorter({
		url:'<?php echo base_url()?>index.php/tablero/obtenerdatos',  
		filters:['equisel'],
		sort:false,
		onLoad:false,
		onRowClick:function(){
			if($(this).data('servicio')>'1'){
   				$("#nombre").val($(this).data('tipo2'));
   				$("#fecserv").val($(this).data('feca'));
   				$("#idserv").val($(this).data('ideqp'));
   			}else{
   				$("#nombre").val('');
   				$("#fecserv").val('');
   				$("#tiposel").val('');
   				$("#idserv").val('');
   			}
   		},
		onSuccess:function(){
			$('#tabla_eqpsele tbody tr').map(function(){
   				$("#ubica").val($(this).data('nom1'));
   				$("#tiposel").val($(this).data('cat'));
   		})
	   },	   	
   	});
   	
   	$("#tabla_detmes").ajaxSorter({
		url:'<?php echo base_url()?>index.php/tablero/tablatecmes',  		
		filters:['cmbCiclo','cmbMesT','tip2'],
		sort:false,
		onSuccess:function(){   						
			$('#tabla_detmes tbody tr').map(function(){ $(this).css('text-align','center'); $(this).css('font-size','18px');$(this).css('font-weight','bold');})
			$('#tabla_detmes tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','left');$(this).css('font-size','8px'); $(this).css('font-weight','normal');})
	    },
	});
   	
});  	
</script>
