<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 20px;		
}   
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li style="font-size: 20px"><a href="#general"><img src="<?php echo base_url();?>assets/images/menu/combustible.png" width="25" height="25" border="0"> Diesel</a></li>
		<li style="font-size: 20px"><a href="#compara"><img src="<?php echo base_url();?>assets/images/menu/combustible.png" width="25" height="25" border="0"> Comparativo</a></li>
			 <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px;margin-top: 1px; margin-left:4px;" >
                    	<?php $ciclof=19; $actual=date("y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo '20'.$actual;?> </option>
            				<?php  $actual-=1; } ?>
          			</select>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="general" class="tab_content"  >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_comg" name="tabla_comg" style="width: 950px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaComG" name="mytablaComG" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>                                                        			
                            			<th data-order = "NomDepA" >Unidad</th>
                            			<th data-order = "ene" >Ene</th>
                            			<th data-order = "feb" >Feb</th>
                            			<th data-order = "mar" >Mar</th>
                            			<th data-order = "abr" >Abr</th>
                            			<th data-order = "may" >May</th>
                            			<th data-order = "jun" >Jun</th>
                            			<th data-order = "jul" >Jul</th>
                            			<th data-order = "ago" >Ago</th>
                            			<th data-order = "sep" >Sep</th>
                            			<th data-order = "oct" >Oct</th>
                            			<th data-order = "nov" >Nov</th>
                            			<th data-order = "dic" >Dic</th>
                            			<th data-order = "tot" >Total</th>
                            			<th data-order = "por" >%</th>
                            			<!--<th data-order = "acum" >%</th>-->
                            		</thead>
                        			<tbody title="Seleccione para visualizar detalle" style="font-size: 12px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablesg" action="<?= base_url()?>index.php/diesel/reportedie" method="POST" >							
    	    	            		<ul class="order_list" style="width:380px; margin-top: 1px">
    	    	            			Consumos actuales: Ciclo- <input size="3%" type="text" name="cic" id="cic" readonly="true" style="border: none"  >    	    	            			
    	    	            			% Acum 
    	    	            			<select name="por" id="por" style="font-size: 11px;height: 25px;  margin-top: 1px;">
            								<option value="90">90%</option>
            								<option value="80">80%</option>
            								<option value="70">70%</option>
          								</select>
    	    	            		</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablacomg" value ="" class="htmlTable"/> 
								</form>   
                			</div>       
	                	</div>
	        		</th>
            		</tr>
     		</table>	
		</div>
		<div id="compara" class="tab_content"  >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_comgc" name="tabla_comgc" style="width: 950px; margin-top: -20px"  >                 			
							<span class="ajaxTable" style="height: 440px; " >
                    			<table id="mytablaComGc" name="mytablaComGc" class="ajaxSorter" border="1" style="width: 933px" >
                        			<thead>   
                        				<tr>                                                     			
                            				<th rowspan="2" data-order = "mes" >Mes</th>
                            				<th rowspan="2" data-order = "sem" >Semana</th>
                            				<th colspan="4">Lts</th>
                            				<th colspan="4" >Acumulado Semana</th>
                            				<th colspan="4" >Total</th>
                            			</tr>
                            			<tr>
                            				<th data-order = "c1lts" >2021</th>
                            				<th data-order = "c2lts" >2022</th>
                            				<th data-order = "diflts" >Lts</th>
                            				<th data-order = "porlts" >%</th>
                            				
                            				<th data-order = "c1acu" >2021</th>
                            				<th data-order = "c2acu" >2022</th>
                            				<th data-order = "difaslts" >Lts</th>
                            				<th data-order = "poraslts" >%</th>
                            				
                            				<th data-order = "c1gacu" >2021</th>
                            				<th data-order = "c2gacu" >2022</th>
                            				<th data-order = "difaglts" >Lts</th>
                            				<th data-order = "poraglts" >%</th>		
                            			</tr>		
                            		</thead>
                        			<tbody title="Seleccione para visualizar detalle" style="font-size: 12px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablesg" action="<?= base_url()?>index.php/diesel/reportedie" method="POST" >							
    	    	            		<ul class="order_list" style="width:480px; margin-top: 1px">
    	    	            			Comparativo de Consumos actuales acumulados al mes de    	    	            			
    	    	            			<select name="cmbMesM" id="cmbMesM" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
		   									<option value="0" >Todos</option>
		   									<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   											<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   											<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   											<option value="10" >Octubre</option><option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   										</select>	
    	    	            		</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablacomg" value ="" class="htmlTable"/> 
								</form>   
                			</div>       
	                	</div>
	        		</th>
            		</tr>
     		</table>	
		</div>
	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#cmbCiclo").change( function(){	
	$("#cic").val('20'+$("#cmbCiclo").val());
	return true;
});

$(document).ready(function(){
	var f = new Date();
	$("#cmbMesM").val((f.getMonth() +1));
	$("#txtFI").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());		
	$("#cic").val('20'+$("#cmbCiclo").val());
	$("#tabla_comg").ajaxSorter({
		url:'<?php echo base_url()?>index.php/diesel/tablacomg',  
		filters:['cmbCiclo','por'],
		//active:['color',1],
        sort:false,
        onSuccess:function(){
       		$('#tabla_comg tbody tr').map(function(){
				if($(this).data('nomdepa')=='Total:') {	    				    			
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('font-size','12px');
	    		}
	    		if($(this).data('color')=='1') {$(this).css("background-color", "lightyellow");	}
			});
			$('#tabla_comg tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
			$('#tabla_comg tbody tr td:nth-child(14)').map(function(){ $(this).css('font-weight','bold');});
	    	$('#tabla_comg tbody tr td:nth-child(15)').map(function(){ $(this).css('font-weight','bold');});
		},   
	});
	$("#tabla_comgc").ajaxSorter({
		url:'<?php echo base_url()?>index.php/diesel/tablacomgc',  
		filters:['cmbMesM'],
		//active:['color',1],
        sort:false,
        onSuccess:function(){
       		$('#tabla_comgc tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
       		$('#tabla_comgc tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','center');});
       		//$('#tabla_comgc tbody tr td:nth-child(5)').map(function(){ $(this).css('font-weight','bold');});
       		//$('#tabla_comgc tbody tr td:nth-child(8)').map(function(){ $(this).css('font-weight','bold');});
       		//$('#tabla_comgc tbody tr td:nth-child(13)').map(function(){ $(this).css('font-weight','bold');});
       		col=4;
       		while(col<=12){
       			$('#tabla_comgc tbody tr td:nth-child('+col+')').map(function(){$(this).css('font-weight','bold');});
	    		col+=4;
	    	}	
	    	col=6;
       		while(col<=14){
       		$('#tabla_comgc tbody tr td:nth-child('+col+')').map(function(){ 
					$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			if($(this).html() <= 5.00 && $(this).html() != '' ) {
              				$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        			}
        			if($(this).html() >= 5.01 && $(this).html() <= 10.00 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "black");
        			}
        			if($(this).html() >= 10.01 && $(this).html() != '' ) {
             				
             				$(this).css("background-color", "red");$(this).css("color", "black");
        			} 
	    	});
	    	col+=4;
	    	}
		},   
	});
	
});
</script>