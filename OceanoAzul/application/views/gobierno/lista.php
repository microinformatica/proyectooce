<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li ><a href="#certif" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/sagarpa3.png" width="25" height="25" border="0"> Certificados </a></li>
		<li><a href="#asig" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/sagarpa4.png" width="25" height="25" border="0"> Asignación </a></li>			
		<li><a href="#repor" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/printer.png" width="25" height="25" border="0"> Reportes </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px"  >
		<div id="certif" class="tab_content" style="margin-top: -5px">
			<div class="ajaxSorterDiv" id="tabla_cer" name="tabla_cer" align="left"  >             	
            	<div style="margin-top: -10px;">            	
				</div>	       
                <span class="ajaxTable" style=" height: 135px; width: 917px; background-color: white ">
                    <table id="mytablaC" name="mytablaC" class="ajaxSorter" ">
                        <thead style="font-size: 10px">                         	
                            <th data-order = "NumCer" >No</th>                                                        
                            <th data-order = "IniVig1" >Inicia Vigencia</th>                            
                            <th data-order = "FinVig1" >Vence el Día</th>
                            <th data-order = "CanPos" >Postlarvas</th>
                            <th data-order = "CanNau" >Nauplios</th>                                                       
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                 </span> 
             	<div class="ajaxpager" style="margin-top: -15px">        
                    <ul class="order_list"><input style="font-size: 14px" type="submit" id="nuevoc" name="nuevoc" value="Nuevo" /></ul>                          
                </div>  
            </div>             
        	<div id='ver_mas_cer' class='hide' style="height: 238px; margin-top: 4px" align="" >
						<!--<div style="color: blue">Certificado</div>-->
       					<table style="width: 680px;" border="2px">
							<thead style="background-color: #DBE5F1">
								<th colspan="4" height="15px" style="font-size: 20px">Número de Certificado DGSA-DSAP-CSAMO-<input size="8%" type="text" name="cerc" id="cerc" style="text-align: center; border: none; color: red;font-size: 20px" >
								<input size="8%" type="hidden"  name="idc" id="idc"/>																								
								</th>																	
							</thead>
							<tbody style="background-color: #F7F7F7">
							<tr>
								<th colspan="2" style="text-align: center">Vigencia</th><th colspan="2" style="text-align: center">Cantidad</th>
							</tr>
							<tr>
								<th style="font-size: 14px; text-align: center">Inicial</th>
								<th style="font-size: 14px; text-align: center">Vence el Día</th>
								<th style="font-size: 14px; text-align: center">Postlarvas</th>	
								<th style="font-size: 14px; text-align: center">Nauplios</th>
							</tr>
    						<tr style="font-size: 14px; background-color: white;">
								<th style="font-size: 14px; text-align: center"><input size="13%" type="text" name="txtFic" id="txtFic" class="fecha redondo mUp" style="text-align: center;"  readonly="true" ></th>									
								<th style="font-size: 14px; text-align: center"><input size="13%" type="text" name="txtFvc" id="txtFvc" class="fecha redondo mUp" style="text-align: center;"  readonly="true" ></th>
								<th style="font-size: 14px; text-align: center"><input size="13%" type="text" name="posc" id="posc" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" style="text-align: right" ></th>									
								<th style="font-size: 14px; text-align: center"><input size="13%" type="text" name="nauc" id="nauc" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" style="text-align: right"></th>
							</tr>
							</tbody>
    						<tfoot style="background-color: #DBE5F1">
    							<th colspan="4" style="text-align: right">
									<input style="font-size: 14px" type="submit" id="aceptarc" name="aceptarc" value="Guardar" />
									<input style="font-size: 14px" type="submit" id="borrarc" name="borrarc" value="Borrar" />
									<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(1)" />
								</th>	
							</tfoot>
						</table>
			</div>	
		</div>
		<div id="asig" class="tab_content" style="margin-top: -5px" >
			<div class="ajaxSorterDiv" id="tabla_cerasi" name="tabla_cerasi" align="left"  > 
				<div style="margin-top: -10px;">            	
				</div>	            	
            	<span class="ajaxTable" style=" height: 285px; width: 917px; background-color: white;  ">
                    <table id="mytablaCasi" name="mytablaCasi" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <th data-order = "FechaR" style="width: 70px" >Fecha</th>                            
                            <th data-order = "RemisionR" style="width: 50px">Remisión</th>
                            <th data-order = "Razon" >Razón Social</th>
                            <th data-order = "Cantidad" style="width: 80px">Postlarvas</th>
                            <th data-order = "NumCer" style="width: 70px">Certificado</th>                                                       
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                 </span>
                 <div class="ajaxpager" >        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/gobierno/pdfrepa" >
                    	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                    </form>   
 				</div>
 			</div>
 					<table border="2px" style="width: 650px; margin-top: -55px; ">
 						<thead style="background-color: #DBE5F1">
 							<tr >
 								<th colspan="2" style="text-align: center">1.- Registro de Certificado a Remisión No. 
 								<input size="2%" readonly="true" type="text" name="rem" id="rem" style="text-align: left;  border: 0; background: none">
 								<input readonly="true" type="hidden" name="id" id="id" style="text-align: left;  border: 0; background: none">
 								<input style="font-size: 14px" type="submit" id="aceptara" name="aceptara" value="Guardar" />
 								</th>
 								<th colspan="3" style="text-align: center">2.- Asignación Grupal &nbsp;&nbsp;
 								<input style="font-size: 14px" type="submit" id="aceptarga" name="aceptarga" value="Procesar" /></th>
 							</tr>
 						</thead>
 						<tbody style="background-color: #F7F7F7">
 							<tr>
 								<th style="text-align: center">Razón Social</th>
 								<th style="text-align: center">Certificado</th> 								
 								<th style="text-align: center">Iniciar</th>
 								<th style="text-align: center">Finalizar</th>
 								<th style="text-align: center">Certificado</th>
 								
 							</tr>
 							<tr style="background-color: white;">
	 							<th style="text-align: center"><input readonly="true" size="60%" type="text" name="raz" id="raz" style="text-align: center;  border: 0; background: none"></th>
	 							<th>
									<select name="certi" id="certi" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          								<?php	          							
          									$data['result']=$this->gobierno_model->verCertificado();
											foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->NumReg;?>" ><?php echo $row->NumCer;?></option>
           									<?php endforeach;?>
   									</select>
   								</th>
	 							<th style="text-align: center">
	 								<select name="inicio" id="inicio"  style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
           								<?php	
           									//$this->load->model('cargos_model');
											$data['result']=$this->gobierno_model->verRemision();
											foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->NumRegR;?>" ><?php if($row->NumRegR==0) { echo "SN";} else { echo $row->RemisionR;}?></option>
           									<?php endforeach;?>
    								</select>
	 							</th>								
								<th style="text-align: center">
									<select name="fin" id="fin"  style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
           								<?php	
           									//$this->load->model('cargos_model');
											$data['result']=$this->gobierno_model->verRemision();
											foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->NumRegR;?>" ><?php if($row->NumRegR==0) { echo "SN";} else { echo $row->RemisionR;}?></option>
           									<?php endforeach;?>
    								</select>
								</th>
								<th>
									<select name="certi1" id="certi1" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          								<?php	          							
          									$data['result']=$this->gobierno_model->verCertificado();
											foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->NumReg;?>" ><?php echo $row->NumCer;?></option>
           									<?php endforeach;?>
   									</select>
   								</th>
 							</tr>
 							
 						</tbody>
 						</table>
 						<table  style="width: 510px; height: 90px; margin-top: -25px">
 							<tr>
 								<th colspan="5">
 									<div class="ajaxSorterDiv" id="tabla_cera" name="tabla_cera" align="left"   >             	
            							<span class="ajaxTable" style=" height: 95px; width: 500px; background-color: white ">
                    						<table id="mytablaCc" name="mytablaCc" class="ajaxSorter" style="font-size: 10px" >
                        						<thead >                         	
                            						<th data-order = "NumCer" >Certificado</th>                            
                            						<th data-order = "IniVig" >Inicia</th>
                            						<th data-order = "FinVig" >Finaliza</th>
                            						<th data-order = "pos" >Autorizados</th>                                                       
                            						<th data-order = "ent" >Asignados</th>
                            						<th data-order = "dif" >Diferencia</th>
                        						</thead>
                        						<tbody style="font-size: 10px">
                        						</tbody>
                    						</table>
                 						</span>                 
 									</div>
 								</th>
 							</tr>
 						</table>  
 			
 		</div>
 		<div id="repor" class="tab_content" style="margin-top: -5px" >
			<div class="ajaxSorterDiv" id="tabla_cerimp" name="tabla_cerimp" align="left"   >    
				<div style="margin-top: -10px;">            	
				</div>	         	
            	<span class="ajaxTable" style=" height: 370px; width: 917px; background-color: white ">
                    <table id="mytablaCimp" name="mytablaCimp" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <th data-order = "Razon" >Razón Social</th>                                                        
                            <th data-order = "FechaR" >Fecha</th>                            
                            <th data-order = "RemisionR" >Remisión</th>
                            <th data-order = "CantidadRR" >Cantidad</th>                                                       
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                 </span>
                 <div class="ajaxpager" style="margin-top: -15px">        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/gobierno/pdfrepimp" >
                    	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/><input type="hidden" id="cer1" name="cer1" />  
                        <input type="hidden" id="fi1" name="fi1" /> <input type="hidden" id="ff1" name="ff1" /> 
                        <input type="hidden" id="aut1" name="aut1" /> <input type="hidden" id="ent1" name="ent1" />
                    </form>   
 				</div>
 			</div>
 			
 				<!--<table border="2px" style="width: 360px; left: 700px; top: 566px; position: absolute">--></table>
 					<table border="2px" style="width: 360px;margin-top: -40px ">
 					<thead style="background-color: #DBE5F1">
 						<th colspan="5">
 							Certificado de Movilización: DGSA-DSAP-CSAMO-<select name="cer" id="cer" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<?php	
          						$this->load->model('gobierno_model');
          						$data['result']=$this->gobierno_model->verCertificado();
								foreach ($data['result'] as $row):?>
           						<option value="<?php echo $row->NumReg;?>" ><?php echo $row->NumCer;?></option>
           						<?php endforeach; ?>
   								</select>  
 						</th>
 					</thead>
 					<tbody style="background-color: #F7F7F7">
 						<tr >
 							<th colspan="2" style="text-align: center">Vigencia</th>
 							<th colspan="3" style="text-align: center">Producto: Postlarvas</th>
 						</tr>
	 					<tr>
 							<th style="text-align: center">Inicial</th>
 							<th style="text-align: center">Final</th>
 							<th style="text-align: center">Autorizados</th>
 							<th style="text-align: center">Entregados</th>
 							<th style="text-align: center">Diferencia</th>
						</tr>
 						<tr style="background-color: white;">
	 						<th style="text-align: center"><input readonly="true" size="11%" type="text" name="fi" id="fi" style="text-align: center;  border: 0"></th>								
							<th style="text-align: center"><input readonly="true" size="11%" type="text" name="ff" id="ff" style="text-align: center;  border: 0"></th>
							<th style="text-align: center"><input readonly="true" size="13%" type="text" name="aut" id="aut" style="text-align: center;  border: 0"></th>
 							<th style="text-align: center"><input readonly="true" size="13%" type="text" name="ent" id="ent" style="text-align: center;  border: 0"></th>
	 						<th style="text-align: center"><input readonly="true" size="13%" type="text" name="dif" id="dif" style="text-align: center;  border: 0"></th>
 						</tr>
 					</tbody>
 				</table>  
 			
 		</div>
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
function cerrar(sel){
	$(this).removeClass('used');
	$('#ver_mas_cer').hide();
}

$("#nuevoc").click( function(){
	$("#cerc").val('');
	$("#txtFic").val('');
	$("#txtFvc").val('');
	$("#posc").val('');
	$("#nauc").val('');
	$("#idc").val('');
	$(this).removeClass('used');
	$('#ver_mas_cer').show();	
 return true;
})

$("#borrarc").click( function(){	
	numero=$("#idc").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/gobierno/borrarc", 
						data: "id="+$("#idc").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Certificado Eliminado");
										$("#nuevoc").click();
										$(this).removeClass('used');
										$('#ver_mas_cer').hide();
										$("#mytablaC").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Certificado para poder Eliminarlo");
		return false;
	}
});

$("#aceptarc").click( function(){		
	cer=$("#cerc").val();fi=$("#txtFic").val();fv=$("#txtFvc").val();
	numero=$("#idc").val();
	if( cer!=''){
	  if( fi!=''){
	  	if( fv!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/gobierno/actualizarc", 
						data: "id="+$("#idc").val()+"&cer="+$("#cerc").val()+"&fi="+$("#txtFic").val()+"&fv="+$("#txtFvc").val()+"&pos="+$("#posc").val()+"&nau="+$("#nauc").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevoc").click();
										$(this).removeClass('used');
										$('#ver_mas_cer').hide();
										$("#mytablaC").trigger("update");
										$("#mytablaCc").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/gobierno/agregarc", 
						data: "cer="+$("#cerc").val()+"&fi="+$("#txtFic").val()+"&fv="+$("#txtFvc").val()+"&pos="+$("#posc").val()+"&nau="+$("#nauc").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Certificado registrado correctamente");
										$("#nuevoc").click();
										$(this).removeClass('used');
										$('#ver_mas_cer').hide();
										$("#mytablaC").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		}else{
			alert("Error: Vencimiento no valido");	
			$("#txtFvc").focus();
			return false;
		}
		}else{
			alert("Error: Vigencia Inicial no valida");	
			$("#txtFic").focus();
			return false;
		}					
	}else{
		alert("Error: Número de Certificado no valido");	
		$("#cerc").focus();
		return false;
	}
});

$("#txtFic").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFvc").datepicker( "option", "minDate", selectedDate );
	}	
});
$("#txtFvc").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFic").datepicker( "option", "maxDate", selectedDate );
	}	
});	

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

$("#aceptara").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/gobierno/actualizara", 
				data: "id="+$("#id").val()+"&cer="+$("#certi").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaCasi").trigger("update");$("#mytablaCc").trigger("update");
								alert("Datos actualizados correctamente");										
								
								$("#id").val('');$("#raz").val('');$("#rem").val('');$("#certi").val('');																					
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder actualizar datos");				
		return false;
	}	 
});
$("#aceptarga").click( function(){	
	ini=$("#inicio").val();fin=$("#fin").val();
	if(ini > fin){
		alert("Error: Iniciar No debe ser Mayor a Finalizar");		
		$("#inicio").focus();		
		return false;
	}else{
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/gobierno/actualizarga", 
				data: "cer="+$("#certi1").val()+"&ini="+$("#inicio").val()+"&fin="+$("#fin").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaCasi").trigger("update");$("#mytablaCc").trigger("update");
								alert("Datos actualizados correctamente");										
																			
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
		});
	}	 
});


$(document).ready(function(){ 
	$('#ver_mas_cer').hide();
	$("#tabla_cer").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gobierno/tablac',  		
    	sort:false,              
    	onRowClick:function(){
    		if($(this).data('numreg')>'0'){
    			$(this).removeClass('used');
   				$('#ver_mas_cer').show();
   				$("#idc").val($(this).data('numreg'));
   				$("#cerc").val($(this).data('numcer')); 
   				$("#txtFic").val($(this).data('inivig'));
   				$("#txtFvc").val($(this).data('finvig'));
   				$("#posc").val($(this).data('canpos'));
   				$("#nauc").val($(this).data('cannau'));
    		}
    	}, 
	});
	$("#tabla_cerimp").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gobierno/tablar',  		
		filters:['cer'],
    	sort:false,
    	onSuccess:function(){
    	   	$('#tabla_cerimp tbody tr').map(function(){
    	   		if($(this).data('remisionr')=='Total:'){
    	   			$(this).css('font-weight','bold'); 
    	   		}
    	   		$("#cersel").val($(this).data('numcer'));$("#cer1").val($(this).data('numcer'));
	    		if($(this).data('numcer')!='Sin Registro'){
	    			$("#fi").val($(this).data('inivig')); $("#fi1").val($(this).data('inivig'));
	    			$("#ff").val($(this).data('finvig')); $("#ff1").val($(this).data('finvig'));
	    		}
	    			$("#aut").val($(this).data('canpos'));$("#aut1").val($(this).data('canpos'));
	    			$("#ent").val($(this).data('entregado'));$("#ent1").val($(this).data('entregado'));
	    			$("#dif").val($(this).data('dif'));
	    	});
	    	$('#tabla_cerimp tbody tr td:nth-child(1)').map(function(){ if($(this).data('razon')!=''){
    	   			$(this).css('font-weight','bold'); 
    	   		} })
    	   		
	    	$('#tabla_cerimp tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center');  })
	    	$('#tabla_cerimp tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','center');  })
	    	$('#tabla_cerimp tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');  })
    	},                 
	});
	$("#tabla_cerasi").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gobierno/tablaa',  		
		sort:false,
    	onRowClick:function(){
    		$("#id").val($(this).data('numregr'));
			$("#raz").val($(this).data('razon'));
			$("#rem").val($(this).data('remisionr'));
			$("#certi").val($(this).data('numcerr'));
    	},
    	onSuccess:function(){
    		$('#tabla_cerasi tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','center');  })
    		$('#tabla_cerasi tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center');  })
    	   	$('#tabla_cerasi tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');  })
    	   	$('#tabla_cerasi tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','center');  })
    	},    
   });
	$("#tabla_cera").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gobierno/tablaca',  				
    	sort:false,
   	});
});  	
</script>
