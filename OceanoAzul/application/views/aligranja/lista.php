<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts-more.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/export-data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/accessibility.js"></script>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 25px;
	/*background: none;*/		
}   
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>
<div style="height:600px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li style="font-size: 14px"><a href="#fac"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">FCA</a></li>
		<li style="font-size: 14px"><a href="#dia"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">Día</a></li>
		<li style="font-size: 14px"><a href="#acu"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">Acumulado</a></li>		
		<li style="font-size: 14px"><a href="#mes"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">Mes</a></li>
		<li style="font-size: 14px"><a href="#gral"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">General</a></li>
		</strong>
		<input type="hidden"  readonly="true" size="2%"  type="text" name="idcha" id="idcha">
		Granja
				<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
             		<select name="numgracha" id="numgracha" style="font-size: 12px; height: 25px; margin-top: 1px   " >		<option value="4">Ahome</option>
    					<option value="6">Huatabampo</option>		
    				</select>
    			<?php } elseif($usuario=="Zuleima Benitez"){ ?>
    				<select name="numgracha" id="numgracha" style="font-size: 12px; height: 25px;  margin-top: 1px  " >								
    					<option value="4">Ahome</option>
    					<option value="6">Huatabampo</option>		    									
    				</select>
    			<?php }elseif($usuario=="Julio Lizarraga" || $usuario=="Joel Lizarraga A." || $usuario=="Antonio Valdez" || $usuario=="Antonio Hernandez"  || $perfil=="Sup. Alimento"){ ?>	 
    				<select name="numgracha" id="numgracha" style="font-size: 12px; height: 25px; margin-top: 1px   " >	<option value="4">Ahome</option>
    				</select>
    			<?php }else{ ?>
    				<select name="numgracha" id="numgracha" style="font-size: 12px; height: 25px; margin-top: 1px   " >								
    					<option value="6">Huatabampo</option>
    				</select>
    			<?php } ?>	
    	Ciclo						
    			<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
    				<select name="ciccha" id="ciccha" style="font-size: 12px;height: 25px;margin-top: 1px; ">
      					<?php $ciclof=21;
      						$actual=date("y"); 
							while($actual >= $ciclof){?>
								<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
								<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            			<?php  $actual-=1; } ?>      									
    				</select>
    			<?php } else{ ?>
    				<select name="ciccha" id="ciccha" style="font-size: 12px;height: 25px;margin-top: 1px; ">
      					<?php $ciclof=21; $actual=date("y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
								<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            			<?php  $actual-=1; } ?>      									
    				</select>	
    			<?php } ?>	
    	Mes
   				<select name="cmbMes" id="cmbMes" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   					<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   					<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   					<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   					<option value="10" >Octubre</option><option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   				</select>				
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		
		<div id="fac" class="tab_content" style="height: 555px" >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
					<td style="text-align: center">			
						Tabla de Resultados Promedio de Lecturas/Charolas	 		
                 		<div class="ajaxSorterDiv" id="tabla_fac" name="tabla_fac" style="width: 305px;  "  >                 			
							<span class="ajaxTable" style="margin-top: 1px;height: 480px; " >
                    			<table id="mytablaFac" name="mytablaFac" class="ajaxSorter" border="1" style="width: 285px;" >
                        			<thead>                            
                            			<th data-order = "resfac" >Resultado</th>
                            			<th data-order = "porfac" >Sistema</th>
                            			<th data-order = "porbio"  >Tecnico</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 20px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
             			Res: <input readonly="true" size="2%"  type="text" name="resfac" id="resfac">
						% Téc: <input onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right" size="4%"  type="text" name="porbio" id="porbio" />
    	    	        <?php if($usuario=="Jesus Benítez" || $usuario=="Julio Lizarraga" ){ ?>
							<input type="submit" id="aceptarfac" name="aceptarfac" value="Guardar" />
						<?php }?>	
	           		</td>
	           		<td style="text-align: center">
	           			Modelos de Distrubición para Aplicación de Alimento Manual
	           			<div class="ajaxSorterDiv" id="tabla_mod" name="tabla_mod" style="width: 305px;  "  >                 			
							<span class="ajaxTable" style="margin-top: 1px;height: 480px; " >
                    			<table id="mytablaMod" name="mytablaMod" class="ajaxSorter" border="1" style="width: 285px;" >
                        			<thead>                            
                            			<th data-order = "idmod" >Modelo</th>
                            			<th data-order = "r0mod" >R0</th>
                            			<th data-order = "r1mod" >R1</th>
                            			<th data-order = "r2mod" >R2</th>
                            			<th data-order = "r3mod" >R3</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 20px; text-align: center">
                        			</tbody>                        	
                    			</table>
                			</span> 
                		</div>
	              		Porcentajes Modelo:
	              		<input placeholder="Mod" readonly="true" size="2%"  type="text" name="idmod" id="idmod" style="border: none; text-align: center">
	              		<input placeholder="R0" size="2%"  type="text" name="r0mod" id="r0mod">
	              		<input placeholder="R1" size="2%"  type="text" name="r1mod" id="r1mod">
	              		<input placeholder="R2" size="2%"  type="text" name="r2mod" id="r2mod">
	              		<input placeholder="R3" size="2%"  type="text" name="r3mod" id="r3mod">
						<?php if($usuario=="Jesus Benítez" || $usuario=="Julio Lizarraga" ){ ?>
							<input type="submit" id="aceptarmod" name="aceptarmod" value="Guardar" />
							<input type="submit" id="nuevomod" name="nuevomod" value="Nuevo" />
						<?php }?>	
	           		</td>
            	</tr>
			</table>	
		</div>
		<div id="dia" class="tab_content" style="height: 555px"  >
			<table border="3" style="margin-top: -5px; margin-left: -10px;" >
				 			<tr style="background-color: lightgray; font-weight: bold">
				 				<!--
				 				<th rowspan="3" style="background-color: lightblue"> Registro y Actualización de Datos</th>
				 				<th rowspan="2" style="text-align: center">Granja</th>
				 				
				 				<th rowspan="2" style="text-align: center">Ciclo</th>-->
				 				<th rowspan="2" style="text-align: center">Día</th>
				 				<th colspan="3" style="text-align: center">Estanque</th>
				 				<th colspan="3"  style="text-align: center">Charolas</th>
				 				<th colspan="4"  style="text-align: center">Kgs</th>
				 				<th colspan="2" style="text-align: center">Dist. Alimento</th>
				 				<th rowspan="2" colspan="2" style="text-align: center">09:30 </br> a.m.</th>
				 				<th rowspan="2" style="text-align: center">03:00 p.m.</th>
				 				<th rowspan="2" style="text-align: center">07:00 p.m.</th>
				 				<th rowspan="3" style="text-align: center">
				 					 <?php if($usuario=="Jesus Benítez" || $usuario=="Julio Lizarraga" || $usuario=="Antonio Valdez" ){ ?>
				 					<input type="submit" id="borrarcha" style="font-size: 11px;" name="borrarcha" value="Borrar" /><br />
									<input type="submit" style="font-size: 11px;" id="nuevocha" name="nuevocha" value="Nuevo"  />
									<input type="submit" style="font-size: 11px;" id="aceptarcha" name="aceptarcha" value="Guardar" /><br />
									 <?php }?>	
								</th>
								<th colspan="2"  style="text-align: center">General</th>
				 			</tr>
				 			<tr style="background-color: lightgray; font-weight: bold">
				 				<th style="text-align: center">Secc.</th>
				 				<th style="text-align: center">No</th>
				 				<th style="text-align: center">Has</th>
				 				<th style="text-align: center">Cód</th>
				 				<th style="text-align: center">Cha</th>
				 				<th style="text-align: center">Pro</th>
				 				<th style="text-align: center">Ant</th>
				 				<th style="text-align: center">Sis</th>
				 				<!--<th style="text-align: center">Act</th>-->
				 				<th style="text-align: center">Soya</th>
				 				<th style="text-align: center">Bal</th>
				 				<th style="text-align: center">NFD</th>
				 				<th style="text-align: center">Modelo Manual</th>
				 				
				 				<th style="text-align: center">Est</th>
				 				<th style="text-align: center">Gráf</th>
				 			</tr>
				 			<tr style="background-color: lightgreen;" >
				 				
				 				<th>
				 					<input size="8%" type="text" name="feccha" id="feccha" class="fecha redondo mUp" readonly="true" style="text-align: center; font-size: 12px;" >
				 				</th>
				 				<th>
				 					<select name="secc" id="secc" style="font-size: 10px; height: 25px;margin-top: 1px; width: 45px;  " >								
    									<option value="00">Sel.</option>
    									<option value="41">I</option>		
    									<option value="42">II</option>
    									<option value="43">III</option>   
    									<option value="44">IV</option>
    								</select>
				 				</th>
				 				<th>
				 					<input type="hidden" id="filest" name="filest" style="width: 40px;">
				 					<select name='idpischa' id="idpischa" style="font-size: 11px;height: 25px;margin-top: 1px;"  ></select>
				 					<input onKeyPress="return valida1(event)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='blue'; style.color='white'" style="font-size:12px; width: 40px; text-align: center" type="hidden" name="kgt"  id="kgt"/>
				 				</th>
				 				<th><input id='hasga' name="hasga" style="width: 40px; border: 0; background: none; text-align: center"  /></th>
				 				<th style="text-align: center"><input onKeyPress="return valida(event)" onFocus="style.backgroundColor='black';style.color='white'" onBlur="style.backgroundColor='green';style.color='white'" style="font-size:12px; width:20px; background:blue; color:white; text-align: center" type="text" name="codcha"  id="codcha" value="0" /></th>
				 				<th style="text-align: center"><input onKeyPress="return valida(event)" onFocus="style.backgroundColor='black';style.color='white'" onBlur="style.backgroundColor='green';style.color='white'" style="font-size:11px; width:20px; text-align: center" type="text" name="numcha"  id="numcha"  /></th>
								<th><input id='prome' name="prome" style="width: 40px; border: 0; background: none; text-align: center;font-size:11px;"  /></th>
								<th><input id='kgta' name="kgta" style="width: 40px; border: 0; background: none; text-align: center;font-size:11px;"  /></th>
								<th><input id='kgssis' name="kgssis" style="width: 40px; border: 0; background: none; text-align: center;font-size:11px;"  /></th>				 				
				 				
				 				<!--<th></th>-->
				 				<th><input onKeyPress="return valida1(event)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='blue'; style.color='yellow'" style="font-size:12px; width: 30px; text-align: center" type="text" name="porsoy"  id="porsoy"/></th>
				 				<th><input onKeyPress="return valida1(event)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='blue'; style.color='yellow'" style="font-size:12px; width: 30px; text-align: center" type="text" name="porbal"  id="porbal"/></th>
				 				<th>
				 					<input onKeyPress="return valida1(event)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='blue'; style.color='white'" style="font-size:11px; width: 30px; text-align: center" type="text" name="fedcha"  id="fedcha"/>
				 				</th>
				 				<th>
				 					<select name="modcha" id="modcha" style="font-size: 10px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="1">1: 20-20-30-30</option>		
    									<option value="2">2: 0-20-40-40</option>
    									<option value="3">3: 40-60-0-0</option>   
    									<option value="4">4: 33-33-37-0</option>
    									<option value="5">5: 20-40-40-0</option>
    									<option value="6">6: 0-40-60-0</option>
    									<option value="7">7: 0-33-33-33</option>
    								</select>
				 				</th>
				 				<th><input placeholder="R0" onKeyPress="return valida1(event)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='blue'; style.color='white'" style="font-size:11px; width: 38px; text-align: center" type="text" name="r0"  id="r0"/></th>
				 				<th><input placeholder="R1" onKeyPress="return valida1(event)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='blue'; style.color='white'" style="font-size:11px; width: 38px; text-align: center" type="text" name="r1"  id="r1"/></th>
				 				<th><input placeholder="R2" onKeyPress="return valida1(event)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='blue'; style.color='white'" style="font-size:11px; width: 38px; text-align: center" type="text" name="r2"  id="r2"/></th>
				 				<th><input placeholder="R3" onKeyPress="return valida1(event)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='blue'; style.color='white'" style="font-size:11px; width: 38px; text-align: center" type="text" name="r3"  id="r3"/></th>
				 				<script>
									function valida(e){
    									tecla = (document.all) ? e.keyCode : e.which;
									    //Tecla de retroceso para borrar, siempre la permite
    									if (tecla==8){
        									return true;
    									}
        							    // Patron de entrada, en este caso solo acepta numeros
        							    if (tecla>=48 && tecla<=58){
    									patron =/[0-9]/;
    									tecla_final = String.fromCharCode(tecla);
    									return patron.test(tecla_final);
    									}else{
    										return false;
    									}
									}
									function valida1(e){
    									tecla = (document.all) ? e.keyCode : e.which;
									    //Tecla de retroceso para borrar, siempre la permite
    									if (tecla==8){
        									return true;
    									}
        							    // Patron de entrada, en este caso solo acepta numeros
        							    if((tecla>=48 && tecla<=57) || (tecla==46)){
    									patron =/[0-9,.]/;
    									tecla_final = String.fromCharCode(tecla);
    									return patron.test(tecla_final);
    									}else{
    										return false;
    									}
									}
								</script>
								<th><select name='idpischag' id="idpischag" style="font-size: 11px;height: 25px;margin-top: 1px;"  ></th>
								<th><button type="button" id="aligra" style="width: 55px; cursor: pointer; background-color: lightblue" class="continuar used" >Ali</button></th>
				 			</tr>
				 		</table>
				 		   		
             <div id='ver_informacion' class='hide' >			
			<table border="0" style="margin-left: -5px; margin-top: -20px; ">
				<tr>
				 	<th >	
				 		<input type="hidden" name="kgst" id="kgst"/>			 		
                 		<div class="ajaxSorterDiv" id="tabla_diacha" name="tabla_diacha" style="width: 940px"  >                 			
							<span class="ajaxTable" style="height: 475px; margin-top: 1px; " >
                    			<table id="mytablaDiaCha" name="mytablaDiaCha" class="ajaxSorter" border="1" style="width: 920px" >
                        			<thead>     
                        				<tr style="font-size: 11px">
                        					<th colspan="3" >Datos Estanque</th>
                        					<th colspan="5" >Día Anterior</th>
                        					<th >Día</th>
                        					<th colspan="3">Lectura</th>
                        					
                        					<!--
                        					<th colspan="2" >Alimento <br />  Sistema</th>
                        					<!--<th colspan="2" ><br /> Técnico</th>
                        					<th colspan="4" >Ajuste <br /> 
                        						Has<input id='hasga' name="hasga" style="width: 40px; border: 0; background: none"  />
                        					</th>    -->
                        					
                        					<th colspan="3" >Sistema</th>
                        					<th colspan="4" >Supervisor</th>
                        					<th >Soya</th>
                        					<th colspan="6" >Balanceado</th> 
                        					<th style="border-bottom: none"></th>
                        				</tr>                       
                            			<tr style="font-size: 11px">
                            				<th data-order = "pisg" >No.</th>
                            				<th data-order = "hasg" >Has</th>
                            				<th data-order = "pesb" >Grs</th>
                            				<th data-order = "fecant" >Fecha</th>
                            				<th data-order = "nfdant" >FD</th>
                            				<th data-order = "aliant" >Kg</th>
                            				<!--<th data-order = "kgshas" >Kg/Ha</th>-->
                            				<th data-order = "kgssoyant">Soya</th>
                            				<th data-order = "kgsbalant">Bal</th>
                            				<th  data-order = "feccha1">Actual</th>
                            				<th data-order = "codcha">Cod</th>
                            				<th data-order = "numcha">Cha</th>
                            				<th data-order = "procha">Prom</th>
                            				
                            				<th data-order = "facsis">%</th>
                            				<th data-order = "kgssis">Kg</th>
                            				<th data-order = "incksa">Inc</th>
                            				<!--
                            				<th style="width: 42px" data-order = "facbio">% [+/-]</th>
                            				<th data-order = "kgsbio">Kg</th>
                            				-->
                            				<th  data-order = "pr">%</th>
                            				<th data-order = "kgstec">Kg</th>
                            				<th data-order = "kgsha" >Kg/Ha</th>
                            				<th data-order = "inckta">Inc</th>
                            				
                            				<th data-order = "kgssoy">kg</th>
                            				<th data-order = "kgsbal">kg</th>
                            				<th data-order = "pesbfd">Peso</th>
                            				<!--<th data-order = "m/odcha">NM</th>!-->
                            				<th data-order = "r0">R0</th>
                            				<th data-order = "r1">R1</th>
                            				<th data-order = "r2">R2</th>
                            				<th data-order = "r3">R3</th>
                            				<th data-order = "kgd" style="border-top: none">Final</th>
                            				
                            				<!--
                            				<th data-order = "rac1">Raciones/kg</th>
                            				<th data-order = "kgsha">Kg/Ha</th>
                            				
                            				<?php if($usuario!="Julio Lizárraga"){ ?>
                            				<th data-order = "kgsam">30%</th>
                            				<th data-order = "kgspm">70%</th>
                            				<?php }else{ ?>
                            				<th data-order = "kgsam">20%</th>
                            				<th data-order = "kgspm">80%</th>
                            				<?php } ?>
                            				
                            				-->
                            				<!--<th data-order = "totkgd">Kg Día</th>-->
                            				
                            			</tr>
                            			
                            		</thead>
                        			<tbody style="font-size: 10px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style="font-size: 12px; margin-top: -8px;text-align: right">
             			 		 <ul class="order_list" style="margin-top: 1px; width: 580px; ">
								<form id='aligral' action="<?= base_url()?>index.php/aligranja/pdfdiacha" method="POST" >	
									Alimento por Sección: 
									I <input id='sec1' name="sec1" style="width: 80px; text-align: center; background-color: #475; color: white "/>
									II <input id='sec2' name="sec2" style="width: 80px; text-align: center; background-color: #695; color: white" /> 
									III <input id='sec3' name="sec3" style="width: 80px; text-align: center;background-color: #799; color: white" /> 
									IV <input id='sec4' name="sec4" style="width: 80px; text-align: center;background-color: #559; color: white" />
									<input type="hidden" id='granjaa' name="granjaa" />
        	            	    	<input type="hidden" id="diaali" name="diaali" class="fecha redondo mUp" readonly="true" />
									<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablacha" value ="" class="htmlTable"/>
                    	    	</form>
                    	    	</ul> 
                    	    	<button id="dataalisoy" type="button" style="margin-left: 10px; width: 50px; cursor: pointer; background-color: lightyellow; text-align: center;">Soya</button>
								<button id="dataaliaut" type="button" style="margin-left: 10px; width: 50px; cursor: pointer; background-color: lightyellow; text-align: center;">Feeders</button>	
								<button id="dataali09" type="button" style="margin-left: 10px; width: 55px; cursor: pointer; background-color: lightyellow; text-align: center;">09:30 am</button>
								<button id="dataali03" type="button" style="margin-left: 10px; width: 55px; cursor: pointer; background-color: lightyellow; text-align: center;">03:00 pm</button>
								<button id="dataali07" type="button" style="margin-left: 10px; width: 55px; cursor: pointer; background-color: lightyellow; text-align: center;">07:00 pm</button>
							</div>	
							<div style="margin-top: 1px">
                    			<form id="alisoy" action="<?= base_url()?>index.php/aligranja/pdfalimentosoy" method="POST" >
									<input type="hidden" name="html_soys1" id="html_soys1"/><input type="hidden" name="html_soys2" id="html_soys2"/>
									<input type="hidden" name="html_soys3" id="html_soys3"/><input type="hidden" name="html_soys4" id="html_soys4"/>
									<input type="hidden" name="diaalipdfsoy" id="diaalipdfsoy"/>
									<input type="hidden" name="kgstsoya" id="kgstsoya"/><input type="hidden" name="kgshafsoy" id="kgshafsoy"/>
				    	    	</form>
				        		<script type="text/javascript">
									$(document).ready(function(){
										$('#dataalisoy').click(function(){	
											$('#html_soys1').val($('#mytablaAliSoyS1').html());$('#html_soys2').val($('#mytablaAliSoyS2').html());
											$('#html_soys3').val($('#mytablaAliSoyS3').html());$('#html_soys4').val($('#mytablaAliSoyS4').html());
											$('#diaalipdfsoy').val($('#feccha').val()); $('#kgshafsoy').val($('#kgshaf').val());
											$('#alisoy').submit();
											$('#html_soys1').val('');$('#html_soys2').val('');$('#html_soys3').val('');$('#html_soys4').val('');
											$('#diaalipdfsoy').val(''); $('#kgstfsoy').val('');$('#kgshafsoy').val('');
										});
									});
								</script>
                    			<form id="aliaut" action="<?= base_url()?>index.php/aligranja/pdfalimento" method="POST" >
									<input type="hidden" name="diaalipdf" id="diaalipdf"/><input type="hidden" name="kgstbala" id="kgstbala"/><input type="hidden" name="kgshaf" id="kgshaf"/>
									<input type="hidden" name="html_alis1" id="html_alis1"/><input type="hidden" name="html_alis2" id="html_alis2"/>
									<input type="hidden" name="html_alis3" id="html_alis3"/><input type="hidden" name="html_alis4" id="html_alis4"/>
				    	    	</form>
				        		<script type="text/javascript">
									$(document).ready(function(){
										$('#dataaliaut').click(function(){	
											$('#html_alis1').val($('#mytablaAliAutS1').html());$('#html_alis2').val($('#mytablaAliAutS2').html());
											$('#html_alis3').val($('#mytablaAliAutS3').html());$('#html_alis4').val($('#mytablaAliAutS4').html());
											$('#diaalipdf').val($('#feccha').val());
											$('#aliaut').submit();
											$('#html_alis1').val('');$('#html_alis2').val('');$('#html_alis3').val('');$('#html_alis4').val('');
											$('#diaalipdf').val('');$('#kgstf').val('');$('#kgshaf').val('');
										});
									});
								</script>
							
                    			<form id="ali09" action="<?= base_url()?>index.php/aligranja/pdfalimento09" method="POST" >
									<input type="hidden" name="diaalipdf09" id="diaalipdf09"/>
									<input type="hidden" name="html_alis109" id="html_alis109"/><input type="hidden" name="html_alis209" id="html_alis209"/>
									<input type="hidden" name="html_alis309" id="html_alis309"/><input type="hidden" name="html_alis409" id="html_alis409"/>
				    	    	</form>
				        		<script type="text/javascript">
									$(document).ready(function(){
										$('#dataali09').click(function(){	
											$('#html_alis109').val($('#mytablaAli09S1').html());$('#html_alis209').val($('#mytablaAli09S2').html());
											$('#html_alis309').val($('#mytablaAli09S3').html());$('#html_alis409').val($('#mytablaAli09S4').html());
											$('#diaalipdf09').val($('#feccha').val());
											$('#ali09').submit();
											$('#html_alis109').val('');$('#html_alis209').val('');$('#html_alis309').val('');$('#html_alis409').val('');
											$('#diaalipdf09').val('');
										});
									});
								</script>
								
								<form id="ali03" action="<?= base_url()?>index.php/aligranja/pdfalimento03" method="POST" >
									<input type="hidden" name="diaalipdf03" id="diaalipdf03"/>
									<input type="hidden" name="html_alis103" id="html_alis103"/><input type="hidden" name="html_alis203" id="html_alis203"/>
									<input type="hidden" name="html_alis303" id="html_alis303"/><input type="hidden" name="html_alis403" id="html_alis403"/>
				    	    	</form>
				        		<script type="text/javascript">
									$(document).ready(function(){
										$('#dataali03').click(function(){	
											$('#html_alis103').val($('#mytablaAli03S1').html());$('#html_alis203').val($('#mytablaAli03S2').html());
											$('#html_alis303').val($('#mytablaAli03S3').html());$('#html_alis403').val($('#mytablaAli03S4').html());
											$('#diaalipdf03').val($('#feccha').val());
											$('#ali03').submit();
											$('#html_alis103').val('');$('#html_alis203').val('');$('#html_alis303').val('');$('#html_alis403').val('');
											$('#diaalipdf03').val('');
										});
									});
								</script>
								
								<form id="ali07" action="<?= base_url()?>index.php/aligranja/pdfalimento07" method="POST" >
									<input type="hidden" name="diaalipdf07" id="diaalipdf07"/>
									<input type="hidden" name="html_alis107" id="html_alis107"/><input type="hidden" name="html_alis207" id="html_alis207"/>
									<input type="hidden" name="html_alis307" id="html_alis307"/><input type="hidden" name="html_alis407" id="html_alis407"/>
				    	    	</form>
				        		<script type="text/javascript">
									$(document).ready(function(){
										$('#dataali07').click(function(){	
											$('#html_alis107').val($('#mytablaAli07S1').html());$('#html_alis207').val($('#mytablaAli07S2').html());
											$('#html_alis307').val($('#mytablaAli07S3').html());$('#html_alis407').val($('#mytablaAli07S4').html());
											$('#diaalipdf07').val($('#feccha').val());
											$('#ali07').submit();
											$('#html_alis107').val('');$('#html_alis207').val('');$('#html_alis307').val('');$('#html_alis407').val('');
											$('#diaalipdf07').val('');
										});
									});
								</script>
							</div>	
						</div>
						
	        		</th>
            	</tr>
			</table>
			</div>
			<div id='ver_mas_aliaut' class='hide' style="height: 1px" > 
			<input id="secc1" name="secc1" value="41"/>
			<input id="soya" name="soya" value="3"/><input id="feder" name="feder" value="1"/><input id="manual" name="manual" value="2"/>
			<input id="hora09" name="hora09" value="9"/><input id="hora03" name="hora03" value="3"/><input id="hora07" name="hora07" value="7"/>
			<div class="ajaxSorterDiv" id="tabla_alisoys1" name="tabla_alisoys1" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAliSoyS1" name="mytablaAliSoyS1" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">Kg.</th>
                       		
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
			<div class="ajaxSorterDiv" id="tabla_aliauts1" name="tabla_aliauts1" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAliAutS1" name="mytablaAliAutS1" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">Kg.</th><th data-order = "sacos">Desglose</th><th data-order = "kilos">+</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>	
 			<div class="ajaxSorterDiv" id="tabla_ali09s1" name="tabla_ali09s1" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli09S1" name="mytablaAli09S1" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R0</th><th data-order = "r1">R1</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali03s1" name="tabla_ali03s1" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli03S1" name="mytablaAli03S1" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R2</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali07s1" name="tabla_ali07s1" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli07S1" name="mytablaAli07S1" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R3</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<input id="secc2" name="secc2" value="42"/>
 			<div class="ajaxSorterDiv" id="tabla_alisoys2" name="tabla_alisoys2" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAliSoyS2" name="mytablaAliSoyS2" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">Kg.</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_aliauts2" name="tabla_aliauts2" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAliAutS2" name="mytablaAliAutS2" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">Kg.</th><th data-order = "sacos">Desglose</th><th data-order = "kilos">+</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali09s2" name="tabla_ali09s2" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli09S2" name="mytablaAli09S2" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R0</th><th data-order = "r1">R1</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali03s2" name="tabla_ali03s2" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli03S2" name="mytablaAli03S2" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R2</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali07s2" name="tabla_ali07s2" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli07S2" name="mytablaAli07S2" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R3</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<input id="secc3" name="secc3" value="43"/>
 			<div class="ajaxSorterDiv" id="tabla_alisoys3" name="tabla_alisoys3" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAliSoyS3" name="mytablaAliSoyS3" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">Kg.</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_aliauts3" name="tabla_aliauts3" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAliAutS3" name="mytablaAliAutS3" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">Kg.</th><th data-order = "sacos">Desglose</th><th data-order = "kilos">+</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali09s3" name="tabla_ali09s3" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli09S3" name="mytablaAli09S3" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R0</th><th data-order = "r1">R1</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali03s3" name="tabla_ali03s3" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli03S3" name="mytablaAli03S3" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R2</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali07s3" name="tabla_ali07s3" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli07S3" name="mytablaAli07S3" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R3</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<input id="secc4" name="secc4" value="44"/>
 			<div class="ajaxSorterDiv" id="tabla_alisoys4" name="tabla_alisoys4" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAliSoyS4" name="mytablaAliSoyS4" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">Kg.</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_aliauts4" name="tabla_aliauts4" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAliAutS4" name="mytablaAliAutS4" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">Kg.</th><th data-order = "sacos">Desglose</th><th data-order = "kilos">+</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali09s4" name="tabla_ali09s4" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli09S4" name="mytablaAli09S4" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R0</th><th data-order = "r1">R1</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali03s4" name="tabla_ali03s4" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli03S4" name="mytablaAli03S4" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R2</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<div class="ajaxSorterDiv" id="tabla_ali07s4" name="tabla_ali07s4" style=" height: 460px;">   
             	<span class="ajaxTable" style="height: 420px; background-color: white; width: 210px; margin-top: 1px" >
            	   	<table id="mytablaAli07S4" name="mytablaAli07S4" class="ajaxSorter" border="1" style="height: 420px;" >
                       	<thead >
                       		<th data-order = "estali" >Est</th><th data-order = "totali">R3</th>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center"></tbody>                        	
                   	</table>
                </span> 
 			</div>
 			</div>
			<!--
			<div id='ver_aligra' class='hide' style="height: 90px" >
            	<div name="aligraf" id="aligraf" style="min-width: 900px; height: 430px; margin: 0 auto"></div>
            	<button type="button" id="aligra1" style="cursor: pointer; background-color: gray" class="continuar used" >Cerrar</button>
            </div>
           -->
            <div id='ver_aligra' class='hide' style="height: 90px" >
				<div id='ver_aligrat' class='hide' style="height: 90px" >
				<div class="ajaxSorterDiv" id="tabla_diachag" name="tabla_diachag" style="width: 940px"  >                 			
							<span class="ajaxTable" style="height: 475px; margin-top: 1px; " >
                    			<table id="mytablaDiaChag" name="mytablaDiaChag" class="ajaxSorter" border="1" style="width: 920px" >
                        			<thead>     
                        				<tr style="font-size: 11px">
                            				<th  data-order = "feccha1">Actual</th>
                            				<th data-order = "kgsha">Kg/Ha</th>
                            				<th data-order = "kgshapro">Protocolo</th>
                            				<th data-order = "kgshasy">Soya</th>
                            				<th data-order = "kgshabl">Balanceado</th>
                            			</tr>
                            			
                            		</thead>
                        			<tbody style="font-size: 13px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             	</div>
             	</div>
            	<div name="aligrafn" id="aligrafn" style="min-width: 900px; height: 430px; margin: 0 auto"></div>
            	Del:<input size="10%" type="text" name="fecchai" id="fecchai" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
            	Al:<input size="10%" type="text" name="fecchaf" id="fecchaf" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
            	<button type="button" id="aligra1" style="cursor: pointer; background-color: gray" class="continuar used" >Cerrar</button>
            </div>
		</div>
		<div id="acu" class="tab_content" style="height: 555px" >
			<div class="ajaxSorterDiv" id="tabla_acu" name="tabla_acu" style=" margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<form id='alisem' action="<?= base_url()?>index.php/aligranja/pdfsem" method="POST" >
            			<ul class="order_list" style="width: 450px; text-align: left" >
            				Cierre de Semana: <input size="10%" type="text" name="fecchat" id="fecchat" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
            				<select name="seca" id="seca" style="font-size: 10px; height: 25px;margin-top: 1px; width: 45px;  " >								
    							<option value="0">Sel.</option>
    							<option value="41">I</option>		
    							<option value="42">II</option>
    							<option value="43">III</option>   
    							<option value="44">IV</option>
    						</select>
						</ul>
						<img style="cursor: pointer;" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	       		<input type="hidden" name="tablasem" value ="" class="htmlTable"/>
					</form>
				</div>  
             	<span class="ajaxTable" style="height: 500px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablaAcu" name="mytablaAcu" class="ajaxSorter" border="1" >
                       	<thead >
                       		<tr>
                       			<th rowspan="2" data-order = "pisg">Est</th>
                       			<th rowspan="2" data-order = "hasg">Has</th>
                       			<th colspan="3">Soya</th>
                       			<th colspan="3">Balanceado</th>
                       			<th colspan="4">Soya y Balanceado</th>
                       			<th colspan="4">Sistema</th>
                       		</tr>
                       		</tr>
                       			<th data-order = "semsoy">Sem</th>
                       			<th data-order = "acusoy">AcuAnt</th>
                       			<th data-order = "totsoy">Total</th>
                       			<th data-order = "sembal">Sem</th>
                       			<th data-order = "acubal">AcuAnt</th>
                       			<th data-order = "totbal">Total</th>
                       			<th data-order = "semsb">Sem</th>
                       			<th data-order = "acusb">AcuAnt</th>
                       			<th data-order = "soybal">Total</th>
                       			<th data-order = "kgshas">kg/Ha</th>
                       			<th data-order = "semsi">Sem</th>
                       			<th data-order = "acusi">AcuAnt</th>
                       			<th data-order = "sybsi">Total</th>
                       			<th data-order = "kghasi">kg/Ha</th>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 12px; text-align: right">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>
		</div>	
		<div id="mes" class="tab_content" style="height: 555px" >
			<div class="ajaxSorterDiv" id="tabla_mes" name="tabla_mes" style=" margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 450px; text-align: left" >
            		
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 500px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablaMes" name="mytablaMes" class="ajaxSorter" border="1" >
                       	<thead >
                       		<th data-order = "pisg">Est</th>
                       		<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
                       	</thead>
                       	<tbody style="font-size: 9px">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>
		</div>	
		<div id="gral" class="tab_content" style="height: 555px;" >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
					<th>			
						<div class="ajaxSorterDiv" id="tabla_gral" name="tabla_gral" style="width: 725px;  "  >    
							<span class="ajaxTable" style="margin-top: 1px;height: 530px; " >
                    			<table id="mytablaGral" name="mytablaGral" class="ajaxSorter" border="1" style="width: 703px; font-size: 18px" >
                        			<thead> 
                        				<tr> <th colspan="11" style="text-align: center">Consumo de Alimentos </th> </tr>                           
                        				<tr>
                            			<th rowspan="2" data-order = "diam" >Día</th>
                            			<th colspan="4" style="text-align: center">Sistema</th>
                            			<th colspan="4" style="text-align: center">Real</th>
                            			<th colspan="2" style="text-align: center">KG Incremento</th>
                            			</tr>
                            			<tr>
                            			<th data-order = "sistema" style="text-align: center">Kg</th>
                            			<th data-order = "incsis" style="text-align: center">Inc Día</th>
                            			<th data-order = "porsis" style="text-align: center">%</th>
                            			<th data-order = "kgsis" style="text-align: center">Kg/Ha</th>
                            			<th data-order = "diario" style="text-align: center">Kg</th>
                            			<th data-order = "inctec" style="text-align: center">Inc Día</th>
                            			<th data-order = "portec" style="text-align: center">%</th>
                            			<th data-order = "kgtec" style="text-align: center">Kg/Ha</th>
                            			<th data-order = "diferencia" style="text-align: center">Sis VS Real</th>
                            			<th data-order = "acumulado" style="text-align: center">Acumulado</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 14px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form action="<?= base_url()?>index.php/aligranja/pdfrepgral" method="POST" >							
    	    	            		<ul class="order_list" style="width: 450px; margin-top: 1px">
    	    	     					Has   <input disabled="true"  id='ha' name="ha" style="width: 50px;" />    
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablagral" value ="" class="htmlTable"/> 
								</form>   
                			</div>       	                	
	                	</div>
	           		</th>
            	</tr>
			</table>	
		</div>
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#codcha").change( function(){	
	promedio = 0;porsis = 0; akta=0; kta=0; ktas=0;
	numero1 = $("#codcha").val();
	numero2 =  $("#numcha").val();
	//double 
	promedio = numero1/numero2;
 	if($("#numcha").val()>0)
 	$("#prome").val(promedio);
 	//busca el porcentaje de aumento o disminucion de alimento
 	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/aligranja/buscarpor", 
			data: "pro="+$("#prome").val()+"&kta="+$("#kgta").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#kgssis").val(obj.por);
   			} 
		});			 	
});

function cerrar(){
	$("#accordion").accordion( "activate",0 );
}
$('#aligra').click(function(){$(this).removeClass('used');$('#ver_informacion').hide();$('#ver_aligra').show();});
$('#aligra1').click(function(){$(this).removeClass('used');$('#ver_aligra').hide();$('#ver_informacion').show();});
$("#aceptarfac").click( function(){		
	por=$("#porbio").val();	numero=$("#idfac").val();
	if(por!=''){
	  	$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/aligranja/actualizarfac", 
				data: "id="+$("#idfac").val()+"&por="+$("#porbio").val()+"&gra="+$("#cmbGranjaf").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#idfac").val('');$("#resfac").val('');$("#porbio").val('');
								$("#mytablaFac").trigger("update");	
								$("#mytablaGral").trigger("update");
								$("#mytablaDiaCha").trigger("update");
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}		
			});
	}else{
		alert("Error: Seleccione Resultado de Tabla de Ajuste");		
		return false;
	}
});
$("#nuevomod").click( function(){	
	$("#idmod").val('');
	$("#r0mod").val('');$("#r1mod").val('');$("#r2mod").val('');$("#r3mod").val('');
	$("#r0mod").focus();
	return true;
});
$("#aceptarmod").click( function(){		
	r0=$("#r0mod").val();r1=$("#r1mod").val();r2=$("#r2mod").val();r3=$("#r3mod").val();
	numero=$("#idmod").val();
	if(r0!='' || r1!=''|| r2!=''|| r3!=''){
	  	$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/aligranja/actualizarmod", 
				data: "id="+$("#idmod").val()+"&r0="+$("#r0mod").val()+"&r1="+$("#r1mod").val()+"&r2="+$("#r2mod").val()+"&r3="+$("#r3mod").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#nuevomod").click();
								$("#mytablaMod").trigger("update");	
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}		
			});
	}else{
		alert("Error: Registre Porcentaje");		
		return false;
	}
});

//busca datos del turno
$("#turno").change( function(){	
 	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/aligranja/buscar", 
			data: "pil="+$("#idcha").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#ch1").val(obj.cp1);$("#ch2").val(obj.cp2);
					$("#ch3").val(obj.cp3);$("#ch4").val(obj.cp4);
					$("#ch5").val(obj.cp5);$("#ch6").val(obj.cp6);
   			} 
	});			 
});

/*$("#porsoy").change( function(){	
 	if($("#porsoy").val()>0) $("#porbal").val($("#kgt").val()-$("#porsoy").val());
 	else $("#porbal").val($("#kgt").val());
});

$("#kgt").change( function(){	
 	if($("#kgt").val()>0)  $("#porbal").val($("#kgt").val()-$("#porsoy").val());
});
*/
$("#nuevocha").click( function(){	
	$("#idcha").val(''); $("#idpischa").val('');
	$("#fedcha").val('');
	$("#codcha").val('0'); //$("#ch2").val('0');$("#ch3").val('0');$("#ch4").val('0');$("#ch5").val('0');$("#ch6").val('0');
	$("#numcha").val('');
	$("#modcha").val('0');
	$("#kgta").val('');
	$("#hasga").val('');$("#porbal").val('');$("#porsoy").val('');
	$("#r0").val('');$("#r1").val('');$("#r2").val('');$("#r3").val('');
	$("#kgt").val('');$("#prome").val('');$("#kgssis").val('');
	$("#idsob").focus();
	return true;
});

$("#aceptarcha").click( function(){		
	$("#aceptarcha").attr("disabled","disabled");
	fec=$("#feccha").val();est=$("#idpischa").val();cha=$("#numcha").val();
	numero=$("#idcha").val();
	if( est!='0'){
	  if( fec!=''){
	  	if( cha!=''){
	  	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aligranja/actualizardia", 
						data: "id="+$("#idcha").val()+"&pis="+$("#idpischa").val()+"&fec="+$("#feccha").val()+"&cic="+$("#ciccha").val()+"&gra="+$("#numgracha").val()+"&cod="+$("#codcha").val()+"&kgt="+$("#kgt").val()+"&nfd="+$("#fedcha").val()+"&nch="+$("#numcha").val()+"&r0="+$("#r0").val()+"&r1="+$("#r1").val()+"&r2="+$("#r2").val()+"&r3="+$("#r3").val()+"&mod="+$("#modcha").val()+"&bal="+$("#porbal").val()+"&soy="+$("#porsoy").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevocha").click();
										$("#mytablaDiaCha").trigger("update");	$("#mytablaGral").trigger("update");
										$("#mytablaAliSoyS1").trigger("update");$("#mytablaAliSoyS2").trigger("update");
										$("#mytablaAliSoyS3").trigger("update");$("#mytablaAliSoyS4").trigger("update");
										$("#mytablaAliAutS1").trigger("update");$("#mytablaAliAutS2").trigger("update");
										$("#mytablaAliAutS3").trigger("update");$("#mytablaAliAutS4").trigger("update");
										$("#mytablaAli09S1").trigger("update");$("#mytablaAli09S2").trigger("update");
										$("#mytablaAli09S3").trigger("update");$("#mytablaAli09S4").trigger("update");
										$("#mytablaAli03S1").trigger("update");$("#mytablaAli03S2").trigger("update");
										$("#mytablaAli03S3").trigger("update");$("#mytablaAli03S4").trigger("update");
										$("#mytablaAli07S1").trigger("update");$("#mytablaAli07S2").trigger("update");
										$("#mytablaAli07S3").trigger("update");$("#mytablaAli07S4").trigger("update");
										$("#aceptarcha").removeAttr("disabled");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
										$("#aceptarcha").removeAttr("disabled");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aligranja/agregardia", 
						data: "pis="+$("#idpischa").val()+"&fec="+$("#feccha").val()+"&cic="+$("#ciccha").val()+"&gra="+$("#numgracha").val()+"&cod="+$("#codcha").val()+"&kgt="+$("#kgt").val()+"&nfd="+$("#fedcha").val()+"&nch="+$("#numcha").val()+"&r0="+$("#r0").val()+"&r1="+$("#r1").val()+"&r2="+$("#r2").val()+"&r3="+$("#r3").val()+"&mod="+$("#modcha").val()+"&bal="+$("#porbal").val()+"&soy="+$("#porsoy").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevocha").click();
										$("#mytablaDiaCha").trigger("update");	$("#mytablaGral").trigger("update");
										$("#mytablaAliSoyS1").trigger("update");$("#mytablaAliSoyS2").trigger("update");
										$("#mytablaAliSoyS3").trigger("update");$("#mytablaAliSoyS4").trigger("update");
										$("#mytablaAliAutS1").trigger("update");$("#mytablaAliAutS2").trigger("update");
										$("#mytablaAliAutS3").trigger("update");$("#mytablaAliAutS4").trigger("update");
										$("#mytablaAli09S1").trigger("update");$("#mytablaAli09S2").trigger("update");
										$("#mytablaAli09S3").trigger("update");$("#mytablaAli09S4").trigger("update");
										$("#mytablaAli03S1").trigger("update");$("#mytablaAli03S2").trigger("update");
										$("#mytablaAli03S3").trigger("update");$("#mytablaAli03S4").trigger("update");
										$("#mytablaAli07S1").trigger("update");$("#mytablaAli07S2").trigger("update");
										$("#mytablaAli07S3").trigger("update");$("#mytablaAli07S4").trigger("update");
										$("#aceptarcha").removeAttr("disabled");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
										$("#aceptarcha").removeAttr("disabled");
									}					
								}		
				});
			}
		}else{
			alert("Error: Registre Numero de Charolas");
			$("#numcha").focus();$("#aceptarcha").removeAttr("disabled");
			return false;
		}
		}else{
			alert("Error: Seleccione Fecha");
			$("#feccha").focus();$("#aceptarcha").removeAttr("disabled");
			return false;
		}					
	}else{
		alert("Error: Seleccione Estanque");		
		$("#idpischa").focus();$("#aceptarcha").removeAttr("disabled");
		return false;
	}
});

$("#borrarcha").click( function(){	
	numero=$("#idcha").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aligranja/borrarcha", 
						data: "id="+$("#idcha").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#nuevocha").click();
										$("#mytablaDiaCha").trigger("update");		$("#mytablaGral").trigger("update");																							
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Estanque para poder Eliminarlo");
		return false;
	}
});

$("#numgracha").change( function(){	
	$("#mytablaDiaCha").trigger("update");	
	$("#granjaa").val($("#numgracha").val());
});

$("#secc").change( function(){	
	$("#filest").val($("#ciccha").val()+$("#secc").val()+$("#numgracha").val());
	$("#idpischa").trigger("update");
	return true;
});

$("#ciccha").change( function(){	
	$("#cicchad").val($("#ciccha").val());
	$("#filest").val($("#ciccha").val()+$("#secc").val()+$("#numgracha").val());
	$("#idpischa").trigger("update");
	return true;
});


$("#modcha").change( function(){	
	$("#modcha").val($("#modcha").val());
	kgs=$("#kgsbal").val();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/aligranja/buscarmod", 
			data: "mod="+$("#modcha").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					if(obj.r0mod>0) $("#r0").val(kgs*(obj.r0mod/100)); else $("#r0").val(obj.r0mod); 
					if(obj.r1mod>0) $("#r1").val(kgs*(obj.r1mod/100)); else $("#r1").val(obj.r1mod);
					if(obj.r2mod>0) $("#r2").val(kgs*(obj.r2mod/100)); else $("#r2").val(obj.r2mod);
					if(obj.r3mod>0) $("#r3").val(kgs*(obj.r3mod/100)); else $("#r3").val(obj.r3mod);
   			} 
	});	
});

$("#kgt").change( function(){	
	//$("#modcha").val($("#modcha").val());
	kgs=$("#kgt").val();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/aligranja/buscarmod", 
			data: "mod="+$("#modcha").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					if(obj.r0mod>0) $("#r0").val(kgs*(obj.r0mod/100)); else $("#r0").val(obj.r0mod); 
					if(obj.r1mod>0) $("#r1").val(kgs*(obj.r1mod/100)); else $("#r1").val(obj.r1mod);
					if(obj.r2mod>0) $("#r2").val(kgs*(obj.r2mod/100)); else $("#r2").val(obj.r2mod);
					if(obj.r3mod>0) $("#r3").val(kgs*(obj.r3mod/100)); else $("#r3").val(obj.r3mod);
   			} 
	});	
});

$("#idpischa").change( function(){	
	//$("#ested").val($("#idEst").val());pil=$("#idEst").val();
	$("#hasga").val('');
	//$("#idpischag").val($("#idpischa").val());$("#idpischag").change();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/aligranja/buscarhas", 
			data: "pil="+$("#idpischa").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#hasga").val(obj.hasga);
					$("#fedcha").val(obj.nfd);
					$("#numcha").val(obj.ncha);
					//$("#porsoy").val(obj.soy);
					//$("#porbal").val(obj.bal);
					$("#kgta").val(obj.kgta);
   			} 
	});	
	
});


$("#idpischag").change( function(){	
	//$("#ested").val($("#idEst").val());pil=$("#idEst").val();
	$("#hasga").val('');
	$("#idpischa").val($("#idpischag").val());
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/aligranja/buscarhas", 
			data: "pil="+$("#idpischag").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#hasga").val(obj.hasga);
					$("#fedcha").val(obj.nfd);
					$("#numcha").val(obj.ncha);
					//$("#porsoy").val(obj.soy);
					//$("#porbal").val(obj.bal);
					$("#kgta").val(obj.kgta);
   			} 
	});	
	
});

sumaFecha = function(d, fecha){
 var Fecha = new Date();
 var sFecha = fecha || (Fecha.getDate() + "-" + (Fecha.getMonth() +1) + "-" + Fecha.getFullYear());
 var sep = sFecha.indexOf('-') != -1 ? '-' : '-'; 
 var aFecha = sFecha.split(sep);
 var fecha = aFecha[2]+'-'+aFecha[1]+'-'+aFecha[0];
 fecha= new Date(fecha);
 fecha.setDate(fecha.getDate()+parseInt(d));
 var anno=fecha.getFullYear();
 var mes= fecha.getMonth()+1;
 var dia= fecha.getDate();
 mes = (mes < 10) ? ("0" + mes) : mes;
 dia = (dia < 10) ? ("0" + dia) : dia;
 var fechaFinal = anno+sep+mes+sep+dia;
 return (fechaFinal);
}
$("#fecchai").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
});
$("#fecchat").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
});
$("#fecchaf").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
});
$("#feccha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
	onSelect: function( selectedDate ){
		$("#diaalipdf").val($("#feccha").val());$("#diaali").val($("#feccha").val());
		$("#mytablaDiaCha").trigger("update");	
		$("#mytablaAliSoyS1").trigger("update");$("#mytablaAliSoyS2").trigger("update");
		$("#mytablaAliSoyS3").trigger("update");$("#mytablaAliSoyS4").trigger("update");
		$("#mytablaAliAutS1").trigger("update");$("#mytablaAliAutS2").trigger("update");
		$("#mytablaAliAutS3").trigger("update");$("#mytablaAliAutS4").trigger("update");
		$("#mytablaAli09S1").trigger("update");$("#mytablaAli09S2").trigger("update");
		$("#mytablaAli09S3").trigger("update");$("#mytablaAli09S4").trigger("update");
		$("#mytablaAli03S1").trigger("update");$("#mytablaAli03S2").trigger("update");
		$("#mytablaAli03S3").trigger("update");$("#mytablaAli03S4").trigger("update");
		$("#mytablaAli07S1").trigger("update");$("#mytablaAli07S2").trigger("update");
		$("#mytablaAli07S3").trigger("update");$("#mytablaAli07S4").trigger("update");
		$("#idcha").val(''); //$("#idpischa").val('');
		$("#ch1").val('0'); //$("#ch2").val('0');$("#ch3").val('0');$("#ch4").val('0');$("#ch5").val('0');$("#ch6").val('0');
		$("#kgt").val('');
		$("#idpischa").val('0');	
	}
});	


jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  



$(document).ready(function(){
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	var fecha = sumaFecha(-7,f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
	$("#fecchai").val(fecha);
	$("#fecchaf").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$('#ver_informacion').show();$("#secc").val("00");$("#idpischa").trigger("update");
	$('#ver_aligra').hide();$('#ver_aligrat').hide();$('#ver_mas_aliaut').hide();
	if($("#numgracha").val()==2){$("#granjasel").val('Granja Gran kino');$("#ha").val('331.800');}else{$("#granjasel").val('Granja Ahome');$("#ha").val('332.902');}
	$("#granjaa").val($("#numgracha").val());
	$("#granja").val($("#cmbGranja").val());
	var f = new Date();
	$("#feccha").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#cmbMes").val((f.getMonth() +1));	
	$("#diaali").val($("#feccha").val());
	$("#ciccha").change();$("#secc").change();
	$("#tabla_pis").ajaxSorter({
	url:'<?php echo base_url()?>index.php/aqpgranja/tabla',  	
	filters:['cmbGranja','cmbCiclop'],		
    sort:false,
    onRowClick:function(){
    	if($(this).data('idpis')>'0'){
        	$("#accordion").accordion( "activate",1 );
        	$("#idpis").val($(this).data('idpis'));$("#pisg").val($(this).data('pisg'));$("#hasg").val($(this).data('hasg'));       	
			$("#espg").val($(this).data('espg'));$("#orgg").val($(this).data('orgg'));$("#prog").val($(this).data('prog'));
			$("#fecg").val($(this).data('fecg'));$("#plg").val($(this).data('plg'));$("#cica").val($(this).data('cicg'));
			$("#fecgc").val($(this).data('fecgc'));$("#biogc").val($(this).data('biogc'));$("#ppgc").val($(this).data('ppgc'));
			$("#obsc").val($(this).data('obsc'));$("#cmbGranjas").val($(this).data('numgra'));
		}		
    }, 
    onSuccess:function(){
    		$('#tabla_pis tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_pis tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_pis tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
	    	 $('#tabla_pis tr').map(function(){	
	    		$("#ha").val($(this).data('hasg'));
	    	});	
    },     
	});
	$("#tabla_diacha").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aligranja/tabladiacha',  
		//filters:['numgracha','ciccha','feccha','secc','idpischa'],
		filters:['numgracha','ciccha','feccha','secc','idpischag'],
        sort:false,
         onRowClick:function(){
	    	if($(this).data('idpischa')>'0'){
	        	$("#idcha").val($(this).data('idcha'));$("#numgracha").val($(this).data('numgracha'));
	        	//$("#ciccha").val($(this).data('ciccha'));
	        	$("#secc").val($(this).data('secc'));pis=$(this).data('pisg');
	        	$("#hasga").val($(this).data('hasg'));$("#kgta").val($(this).data('aliant'));$("#turno").val($(this).data('turno'));
				$("#fedcha").val($(this).data('fedcha'));$("#codcha").val($(this).data('codcha'));$("#numcha").val($(this).data('numcha'));
				$("#modcha").val($(this).data('modcha'));$("#porbal").val($(this).data('porbal'));$("#porsoy").val($(this).data('porsoy'));
				$("#r0").val($(this).data('r0'));$("#r1").val($(this).data('r1'));$("#r2").val($(this).data('r2'));$("#r3").val($(this).data('r3'));
				$("#kgsbal").val($(this).data('kgsbal'));$("#kgt").val($(this).data('kgt'));$("#rac").val($(this).data('rac'));
				$("#idpischa").val(pis);
				$("#codcha").change();
			}		
    	}, 
        onSuccess:function(){
        	
	    	$('#tabla_diacha tr').map(function(){	
	    		pis=$(this).data('idpischa');inc=0;est=$(this).data('pisg1');
	    		t1=$(this).data('kgd'); has=$(this).data('tothas'); tota=$(this).data('aliant');inc=$(this).data('inc');hasa=$(this).data('tothasa');
	    		$("#kgst").val($(this).data('kgd'));$("#kgshaf").val($(this).data('tothas'));
	    		
	    		$("#sec1").val($(this).data('totkgds1'));s1=$(this).data('totkgds1');s1s=$(this).data('totkgds1s');s1b=$(this).data('totkgds1b');
	    		$("#sec2").val($(this).data('totkgds2'));s2=$(this).data('totkgds2');s2s=$(this).data('totkgds2s');s2b=$(this).data('totkgds2b'); 
	    		$("#sec3").val($(this).data('totkgds3'));s3=$(this).data('totkgds3');s3s=$(this).data('totkgds3s');s3b=$(this).data('totkgds3b');
	    		$("#sec4").val($(this).data('totkgds4'));s4=$(this).data('totkgds4');s4s=$(this).data('totkgds4s');s4b=$(this).data('totkgds4b');
	    		$("#kgstsoya").val($(this).data('soya'));
	    		$("#kgstbala").val($(this).data('balanceado'));  
	    		Tex='Incremento';
	    		if(parseFloat(inc)<0)  Tex='Decremento';
	    	});	
        	if(pis>0 && $("#idpischag").val()==0){
        		tr='<tr style=font-weight:bold><td colspan=27 style=background-color:lightgray>ALIMENTO TOTAL</td> </tr>';
	    		$('#tabla_diacha tbody').append(tr); 
	    		tr='<tr style=font-weight:bold ><td colspan=4>Anterior</td><td colspan=3>'+Tex+'</td><td colspan=4 >Actual</td><td colspan=4 style=background-color:#475;color:white>Sec I '+s1+'</td><td colspan=4 style=background-color:#695;color:white;>Sec II '+s2+'</td><td colspan=4 style=background-color:#799;color:white>Sec III '+s3+'</td><td colspan=4 style=background-color:#559;color:white>Sec IV '+s4+'</td> </tr>';
	    		$('#tabla_diacha tbody').append(tr);
	    		tr='<tr style=font-weight:bold><td colspan=4>'+tota+' kg - '+hasa+' kg/ha</td><td colspan=3>'+inc+'</td><td colspan=4>'+t1+' kg - '+has+' kg/ha</td><td colspan=4 style=background-color:#475;color:white>Soya-'+s1s+' Bal-'+s1b+'</td><td colspan=4 style=background-color:#695;color:white;color:white>Soya-'+s2s+' Bal-'+s2b+'</td><td colspan=4 style=background-color:#799;color:white>Soya-'+s3s+' Bal-'+s3b+'</td><td colspan=4 style=background-color:#559;color:white>Soya-'+s4s+' Bal-'+s4b+'</td></tr>';
	    		$('#tabla_diacha tbody').append(tr);
	    	}
	    	$('#tabla_diacha tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=1 && $(this).html()<=19){$(this).css("background-color", "#475");$(this).css("color", "white");}
	    		if($(this).html()>=20 && $(this).html()<=39){$(this).css("background-color", "#695");$(this).css("color", "white");}
	    		if($(this).html()>=40 && $(this).html()<=59){$(this).css("background-color", "#799");$(this).css("color", "white");}
	    		if($(this).html()>=60 && $(this).html()<=80){$(this).css("background-color", "#559");$(this).css("color", "white");}
	    	});	
        	$('#tabla_diacha tr').map(function(){$(this).css('text-align','center');});	
        	$('#tabla_diacha tbody tr td:nth-child(6)').map(function(){$(this).css('font-weight','bold');})
        	$('#tabla_diacha tbody tr td:nth-child(9)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
        	$('#tabla_diacha tbody tr td:nth-child(12)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 0 && $(this).html() <= 0.7 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() > 0.7 && $(this).html() < 1.5 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
            		/*if($(this).html() == 5 &&$(this).html() != '' ) {
                		$(this).css("background-color", "yellow"); $(this).css("color", "black");
            		}*/
	    			if($(this).html() >= 1.5 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "white");
        			}
        			
	    	});
    	   	$('#tabla_diacha tbody tr td:nth-child(17)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
    	   	$('#tabla_diacha tbody tr td:nth-child(20)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightgreen');})
    	    $('#tabla_diacha tbody tr td:nth-child(21)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightgreen');})
    	   	$('#tabla_diacha tbody tr td:nth-child(27)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
    	  	
	    	g1=0;g2=0;g3=0;g4=0;g5=0;g6=0;g7=0;g8=0;g9=0;g10=0;gp1=0;gp2=0;gp3=0;gp4=0;gp5=0;gp6=0;gp7=0;gp8=0;gp9=0;gp10=0;
	    	d1='';d2='';d3='';d4='';d5='';d6='';d7='';d8='';d9='';d10='';
	    	$('#tabla_diacha tbody tr').map(function(){
	    		if($(this).data('gram1')>0){g1=$(this).data('gram1');}
	    		if($(this).data('gram2')>0){g2=$(this).data('gram2');}
	    		if($(this).data('gram3')>0){g3=$(this).data('gram3');}
	    		if($(this).data('gram4')>0){g4=$(this).data('gram4');}
	    		if($(this).data('gram5')>0){g5=$(this).data('gram5');}
	    		if($(this).data('gram6')>0){g6=$(this).data('gram6');}
	    		if($(this).data('gram7')>0){g7=$(this).data('gram7');}
	    		if($(this).data('gram8')>0){g8=$(this).data('gram8');}
	    		if($(this).data('gram9')>0){g9=$(this).data('gram9');}
	    		if($(this).data('gram10')>0){g10=$(this).data('gram10');}
	    		if($(this).data('grap1')>0){gp1=$(this).data('grap1');}
	    		if($(this).data('grap2')>0){gp2=$(this).data('grap2');}
	    		if($(this).data('grap3')>0){gp3=$(this).data('grap3');}
	    		if($(this).data('grap4')>0){gp4=$(this).data('grap4');}
	    		if($(this).data('grap5')>0){gp5=$(this).data('grap5');}
	    		if($(this).data('grap6')>0){gp6=$(this).data('grap6');}
	    		if($(this).data('grap7')>0){gp7=$(this).data('grap7');}
	    		if($(this).data('grap8')>0){gp8=$(this).data('grap8');}
	    		if($(this).data('grap9')>0){gp9=$(this).data('grap9');}
	    		if($(this).data('grap10')>0){gp10=$(this).data('grap10');}
	    		if($(this).data('fec1')!=''){d1=$(this).data('fec1');}
	    		if($(this).data('fec2')!=''){d2=$(this).data('fec2');}
	    		if($(this).data('fec3')!=''){d3=$(this).data('fec3');}
	    		if($(this).data('fec4')!=''){d4=$(this).data('fec4');}
	    		if($(this).data('fec5')!=''){d5=$(this).data('fec5');}
	    		if($(this).data('fec6')!=''){d6=$(this).data('fec6');}
	    		if($(this).data('fec7')!=''){d7=$(this).data('fec7');}
	    		if($(this).data('fec8')!=''){d8=$(this).data('fec8');}
	    		if($(this).data('fec9')!=''){d9=$(this).data('fec9');}
	    		if($(this).data('fec10')!=''){d10=$(this).data('fec10');}
	    	});		
			var chart
			g1=parseFloat(g1);g2=parseFloat(g2);g3=parseFloat(g3);g4=parseFloat(g4);g5=parseFloat(g5);g6=parseFloat(g6);g7=parseFloat(g7);
			g8=parseFloat(g8);g9=parseFloat(g9);g10=parseFloat(g10);
			gp1=parseFloat(gp1);gp2=parseFloat(gp2);gp3=parseFloat(gp3);gp4=parseFloat(gp4);gp5=parseFloat(gp5);gp6=parseFloat(gp6);gp7=parseFloat(gp7);
			gp8=parseFloat(gp8);gp9=parseFloat(gp9);gp10=parseFloat(gp10);
			
			Highcharts.chart('aligraf', {
    			chart: {
        				type: 'areaspline'
    			},
    			title: {
        			text: 'Estanque ['+est+']'
    			},
    			legend: {
        			layout: 'vertical',
        			align: 'right',
        			verticalAlign: 'top',
        			//x: 150,
        			//y: 100,
        			floating: true,
        			borderWidth: 1,
        			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    			},
    			xAxis: {
        			categories: [d1, d2, d3, d4, d5, d6, d7, d8, d9, d10],
       				/*
        			plotBands: [{ // visualize the weekend
            			from: 4.5,
            			to: 6.5,
            			color: 'rgba(68, 170, 213, .2)'
        			}]*/
    			},
    			yAxis: {
        			title: {
            			text: 'kilogramos Totales/Ha'
        			}
    			},
    			tooltip: {
        			shared: true,
        			valueSuffix: ' Kgs/Ha'
    			},
    			credits: {
       				 enabled: false
    			},
    			plotOptions: {
        			areaspline: {
            		fillOpacity: 0.5
        		}
    		},
    		series: [{
        		name: 'Real',
        		dataLabels: {
              		  enabled: true
            	},
        		data: [gp1, gp2, gp3, gp4, gp5, gp6, gp7, gp8, gp9, gp10]
    		}, {
        		name: 'Sistema',
        		visible: false,
        		data: [g1, g2, g3, g4, g5, g6, g7, g8, g9, g10]
    		}]
		});
			
	
   	},
    });
	
	 
    $('#idpischaPrimero').boxLoader({
            url:'<?php echo base_url()?>index.php/aligranja/combosecc',
            equal:{id:'secc',value:'#secc'},
           // equal:{id:'ciccha',value:'#ciccha'},
            select:{id:'idpis',val:'val'},
            all:"Sel.",
            
    });

    $('#idpischa').boxLoader({
            url:'<?php echo base_url()?>index.php/aligranja/combosecc',
            //equal:{id:'seccp',value:'#seccp'},
            equal:{id:'filest',value:'#filest'},
            select:{id:'pisg',val:'val'},      
            all:"Sel."
    });
    $('#idpischag').boxLoader({
            url:'<?php echo base_url()?>index.php/aligranja/combob',
            //equal:{id:'secc',value:'#secc'},
            equal:{id:'ciccha',value:'#ciccha'},
            select:{id:'pisg',val:'val'},
            all:"Sel.",
           	
    });
    $("#tabla_fac").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aligranja/tablafac',  
		filters:['numgracha'],	
        sort:false,
        onRowClick:function(){
    		$("#idfac").val($(this).data('idfac'));
        	$("#resfac").val($(this).data('resfac'));
        	$("#porbio").val($(this).data('porbio'));
		}, 
		 onSuccess:function(){
    		importe = 0;
    	   	$('#tabla_fac tr').map(function(){	    		
	    		$(this).css('text-align','center');
	    	});	
	    	 $('#tabla_fac tbody tr td:nth-child(1)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 0 && $(this).html() < 1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() == 1 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
            		
	    			if($(this).html() > 1 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "white");
        			}
        			
	    	});   	
    	},
    });
    $("#tabla_mod").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aligranja/tablamod',  
		sort:false,
        onRowClick:function(){
    		$("#idmod").val($(this).data('idmod'));
        	$("#r0mod").val($(this).data('r0mod'));$("#r1mod").val($(this).data('r1mod'));
        	$("#r2mod").val($(this).data('r2mod'));$("#r3mod").val($(this).data('r3mod'));
		}, 
		 onSuccess:function(){
    			    	
    	},
    });
    $("#tabla_gral").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aligranja/tablagral',  
		filters:['numgracha','ciccha','cmbMes','ha'],	
        sort:false,
        onSuccess:function(){
    		importe = 0;
    	   	$('#tabla_gral tr').map(function(){	    		
	    		$(this).css('text-align','right');
	    	});	
	    	$('#tabla_gral tbody tr').map(function(){
		    	if($(this).data('diam')=='Total:') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    });
		    $('#tabla_gral tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold'); $(this).css('background','lightblue');});
	    	$('#tabla_gral tbody tr td:nth-child(6)').map(function(){ $(this).css('font-weight','bold'); $(this).css('background','lightblue');});
	    	$('#tabla_gral tbody tr td:nth-child(10)').map(function(){ $(this).css('font-weight','bold'); }); 
	    	$('#tabla_gral tbody tr td:nth-child(11)').map(function(){ $(this).css('font-weight','bold'); });   	
    	},
    });
    
    $("#tabla_mes").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aligranja/tablames',  
		filters:['ciccha','cmbMes'],	
        sort:false,
        onSuccess:function(){
    		$('#tabla_mes tbody tr').map(function(){
		    	if($(this).data('pisg')=='Luna') {	    				    			
	    			//$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    });
		    con=2;
		 	while(con<=32){
       		$('#tabla_mes tbody tr td:nth-child('+con+')').map(function(){ 
				if($(this).html() == 'LN'  && $(this).html() != '' ) {$(this).css("background-color", "yellow"); $(this).css("color", "black");$(this).css('text-align','center');}
            	if($(this).html() == 'CC'  && $(this).html() != '' ) {$(this).css("background-color", "yellow"); $(this).css("color", "black");$(this).css('text-align','center');}
            	if($(this).html() == 'LL'  && $(this).html() != '' ) {$(this).css("background-color", "yellow"); $(this).css("color", "black");$(this).css('text-align','center');}
            	if($(this).html() == 'CM'  && $(this).html() != '' ) {$(this).css("background-color", "yellow"); $(this).css("color", "black");$(this).css('text-align','center');}
	    	});
	    	con=con+1;
	    	}   	
    	},
    });
    
   	$("#tabla_diachag").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachag',  
		filters:['numgracha','ciccha','fecchai','secc','idpischag','fecchaf'],
        sort:false,
        onSuccess:function(){
        	//est=$(this).data('pisg1');
	    	//est=1;
	    	est='';
			$('#tabla_diachag tbody tr').map(function(){
   				if($(this).data('piscina')!=''){est=$(this).data('piscina');}    				
   			});
			cic=$("#ciccha").val();
	    	var chart
			Highcharts.chart('aligrafn', {
    			data: {table: 'mytablaDiaChag'},
    			chart: {
        				type: 'areaspline'
    			},
    			
    			title: {text: 'Estanque-'+est+' Ciclo:'+cic+'', align: 'right', y: 3},
    			legend: {
        			layout: 'horizontal',
        			align: 'left',
        			verticalAlign: 'top',
        			x: 30,
        			y: 5,
        			floating: true,
        			borderWidth: 0,
        			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    			},
    			xAxis: {
    			 		labels: {style: {fontSize: '12px',fontWeight: 'bold',color: 'navy',}}
    			},
    			yAxis: {
        			title: {
            			text: 'kilogramos Totales/Ha'
        			}
    			},
    			tooltip: {
        			shared: true,
        			valueSuffix: ' Kgs/Ha'
    			},
    			credits: {
       				 enabled: false
    			},
    			plotOptions: {
        			areaspline: {
            		fillOpacity: 0.5, 
            		 
        		}
        	},
    		series: [{
        				name: 'Real',
        				dataLabels: {enabled: true},
    				}, {
        				name: 'Protocolo',
        				visible: false,
        				dataLabels: {enabled: true},
        			}
        			, {
        				name: 'Soya',
        				dataLabels: {enabled: true},
        			}, {
        				name: 'Balanceado',
        				dataLabels: {enabled: true,  y: 25},
        			}
    		]
		});
			
		       
   	},
    }); 
    
    $("#tabla_alisoys1").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc1','soya','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_alisoys1 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=1 && $(this).html()<=19){$(this).css("background-color", "#475");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
    $("#tabla_aliauts1").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc1','feder','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_aliauts1 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=1 && $(this).html()<=19){$(this).css("background-color", "#475");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali09s1").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc1','manual','hora09','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali09s1 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=1 && $(this).html()<=19){$(this).css("background-color", "#475");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali03s1").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc1','manual','hora03','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali03s1 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=1 && $(this).html()<=19){$(this).css("background-color", "#475");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali07s1").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc1','manual','hora07','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali07s1 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=1 && $(this).html()<=19){$(this).css("background-color", "#475");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	
	$("#tabla_alisoys2").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc2','soya','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_alisoys2 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=20 && $(this).html()<=39){$(this).css("background-color", "#695");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_aliauts2").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc2','feder','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_aliauts2 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=20 && $(this).html()<=39){$(this).css("background-color", "#695");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali09s2").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc2','manual','hora09','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali09s2 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=20 && $(this).html()<=39){$(this).css("background-color", "#695");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali03s2").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc2','manual','hora03','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali03s2 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=20 && $(this).html()<=39){$(this).css("background-color", "#695");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali07s2").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc2','manual','hora07','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali07s2 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=20 && $(this).html()<=39){$(this).css("background-color", "#695");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	
	$("#tabla_alisoys3").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc3','soya','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_alisoys3 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=40 && $(this).html()<=59){$(this).css("background-color", "#799");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_aliauts3").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc3','feder','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_aliauts3 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=40 && $(this).html()<=59){$(this).css("background-color", "#799");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali09s3").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc3','manual','hora09','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali09s3 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=40 && $(this).html()<=59){$(this).css("background-color", "#799");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali03s3").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc3','manual','hora03','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali03s3 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=40 && $(this).html()<=59){$(this).css("background-color", "#799");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali07s3").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc3','manual','hora07','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali07s3 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=40 && $(this).html()<=59){$(this).css("background-color", "#799");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	
	$("#tabla_alisoys4").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc4','soya','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_alisoys4 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=60 && $(this).html()<=80){$(this).css("background-color", "#559");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_aliauts4").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc4','feder','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_aliauts4 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=60 && $(this).html()<=80){$(this).css("background-color", "#559");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali09s4").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc4','manual','hora09','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali09s4 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=60 && $(this).html()<=80){$(this).css("background-color", "#559");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali03s4").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc4','manual','hora03','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali03s4 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=60 && $(this).html()<=80){$(this).css("background-color", "#559");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	$("#tabla_ali07s4").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tabladiachaali',  
		filters:['numgracha','ciccha','feccha','secc4','manual','hora07','idpischag'],
		sort:false, 
		onSuccess:function(){
    	  $('#tabla_ali07s4 tbody tr td:nth-child(1)').map(function(){
	    		{$(this).css('font-weight','bold');}
	    		if($(this).html()>=60 && $(this).html()<=80){$(this).css("background-color", "#559");$(this).css("color", "white");}
	    	}); 	
    	},   
	});
	
	$("#tabla_acu").ajaxSorter({	
		url:'<?php echo base_url()?>index.php/aligranja/tablaaliacu',  
		filters:['numgracha','ciccha','fecchat','seca'],
		sort:false, 
		onSuccess:function(){
			$('#tabla_acu tr').map(function(){	
	    		$("#sec1").val($(this).data('totkgds1')); s1=$(this).data('totkgds1'); s1s=$(this).data('totkgds1s');s1b=$(this).data('totkgds1b');
	    		$("#sec2").val($(this).data('totkgds2')); s2=$(this).data('totkgds2'); s2s=$(this).data('totkgds2s');s2b=$(this).data('totkgds2b'); 
	    		$("#sec3").val($(this).data('totkgds3')); s3=$(this).data('totkgds3'); s3s=$(this).data('totkgds3s');s3b=$(this).data('totkgds3b');
	    		$("#sec4").val($(this).data('totkgds4')); s4=$(this).data('totkgds4'); s4s=$(this).data('totkgds4s');s4b=$(this).data('totkgds4b');
	    		//$("#kgstsoya").val($(this).data('soya'));
	    		//$("#kgstbala").val($(this).data('balanceado'));  
	    	});		
		   $('#tabla_acu tbody tr td:nth-child(1)').map(function(){
	    		$(this).css('font-weight','bold');
	    		$(this).css('text-align','center');
	    		if($(this).html()>=1 && $(this).html()<=19){$(this).css("background-color", "#475");$(this).css("color", "white");}
	    		if($(this).html()>=20 && $(this).html()<=39){$(this).css("background-color", "#695");$(this).css("color", "white");}
	    		if($(this).html()>=40 && $(this).html()<=59){$(this).css("background-color", "#799");$(this).css("color", "white");}
	    		if($(this).html()>=60 && $(this).html()<=80){$(this).css("background-color", "#559");$(this).css("color", "white");}
	       });		
    	   $('#tabla_acu tbody tr td:nth-child(3)').map(function(){$(this).css('font-weight','bold');});	
    	   $('#tabla_acu tbody tr td:nth-child(6)').map(function(){$(this).css('font-weight','bold');});
    	   $('#tabla_acu tbody tr td:nth-child(9)').map(function(){$(this).css('font-weight','bold');});
    	   $('#tabla_acu tbody tr td:nth-child(10)').map(function(){$(this).css('font-weight','bold');});
    	   $('#tabla_acu tbody tr td:nth-child(11)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');});
    	   $('#tabla_acu tbody tr td:nth-child(13)').map(function(){$(this).css('font-weight','bold');});
    	   $('#tabla_acu tbody tr td:nth-child(15)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');});
    	   $('#tabla_acu tbody tr').map(function(){
		    	if($(this).data('pisg')=='Tot') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    });
		    
		    $('#tabla_acu tbody tr').map(function(){
		    if($(this).data('pisg')=='Tot' ){
        		tr='<tr style=font-weight:bold;text-align:center><td colspan=16 style=background-color:lightgray>ALIMENTO TOTAL POR SECCION</td> </tr>';
	    		$('#tabla_acu tbody').append(tr); 
	    		tr='<tr style=font-weight:bold;text-align:center><td colspan=4 style=background-color:#475;color:white>Sección I - '+s1+'</td><td colspan=4 style=background-color:#695;color:white;>Sección II - '+s2+'</td><td colspan=4 style=background-color:#799;color:white>Sección III - '+s3+'</td><td colspan=4 style=background-color:#559;color:white>Sección IV - '+s4+'</td> </tr>';
	    		$('#tabla_acu tbody').append(tr);
	    		tr='<tr style=font-weight:bold;text-align:center><td colspan=4 style=background-color:#475;color:white>Soya-'+s1s+' Bal-'+s1b+'</td><td colspan=4 style=background-color:#695;color:white;color:white>Soya-'+s2s+' Bal-'+s2b+'</td><td colspan=4 style=background-color:#799;color:white>Soya-'+s3s+' Bal-'+s3b+'</td><td colspan=4 style=background-color:#559;color:white>Soya-'+s4s+' Bal-'+s4b+'</td></tr>';
	    		$('#tabla_acu tbody').append(tr);
	    	}
	    	});
    	},   
	});
	
});


</script>

