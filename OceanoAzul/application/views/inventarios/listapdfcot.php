    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 80px 50px 20px; }
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: orange; }	
</style>
<title>Cotizacion</title>
</head>
<body>
	<div id="header"> 
		<table style="width: 700px;">
			<tr>
				<td width="200px">
					<p style="font-size: 8px;"  >
						<img src="<?php echo base_url();?>assets/images/bonattologotipo.jpg" width="160" height="60" border="0"><br />
						SIA - <?= $usuario?> - <?= $perfil?> <br />
					</p>
				</td>	
				<td align="center" style="font-size: 14px; color: navy"><strong>Acuícola Océano Azul S.A. de C.V. <br />
					 Cotización Folio: <label style="font-size: 14px; color: blue"><?= $fol?></label></strong>
				</td>	
					
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: navy;">
				<td width="600 px;" style="text-align: center">
					Av. Reforma No.2007-B Int. 803, Col. Flamingos, Mazatlán, Sin. México, RFC:AOA-180206-SI2, CP:82149 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
	</div>
	
  <?php 
	function weekday($fechas){
		    $fechas=str_replace("/","-",$fechas);
		    list($anio,$mes,$dia)=explode("-",$fechas);
		    return (((mktime ( 0, 0, 0, $mes, $dia, $anio) - mktime ( 0, 0, 0, 7, 17, 2006))/(60*60*24))+700000) % 7;
		}
		//$fechas=$dia;
		$mes='';
		$dias=strtotime($dia);$anio=strtotime($dia);
		$fechas=date("m", strtotime($dia));
		if($fechas == 1 ) { $mes = "Enero"; }
		if($fechas == 2 ) { $mes = "Febrero"; }
		if($fechas == 3 ) { $mes = "Marzo"; }
		if($fechas == 4 ) { $mes = "Abril"; }
		if($fechas == 5 ) { $mes = "Mayo"; }
		if($fechas == 6 ) { $mes = "Junio"; }
		if($fechas == 7 ) { $mes = "Julio"; }
		if($fechas == 8 ) { $mes = "Agosto"; }
		if($fechas == 9 ) { $mes = "Septiembre"; }
		if($fechas == 10 ) { $mes = "Octubre"; }
		if($fechas == 11 ) { $mes = "Noviembre"; }
		if($fechas == 12 ) { $mes = "Diciembre"; }
		$diaN=weekday($dia);
		if ( $diaN == 0 ) { $dia="Lunes";}
		if ( $diaN == 1 ) { $dia="Martes";}
		if ( $diaN == 2 ) { $dia="Miércoles";}
		if ( $diaN == 3 ) { $dia="Jueves";}
		if ( $diaN == 4 ) { $dia="Viernes";}
		if ( $diaN == 5 ) { $dia="Sábado";}
		if ( $diaN == 6 ) { $dia="Domingo";}
	?>
<div style="margin-left: 350px; margin-top: 50px">
	Mazatlán, Sinaloa, <?= $dia.", ".date("d", $dias)." de ".$mes. " de ".date("Y", $anio)."."?>	
</div>

<div style="margin-left:10px; margin-top: 30px">
	At'n:	<?= $cli?><br /><?= $dom?><br />Tel: <?= $tel?>			
</div>
<div style="margin-left:130px; margin-top: 30px; text-align: justify; width: 550px">
	Le comparto lista de precios de productos que tenemos en venta.	
</div>
<div style="margin-left:130px; margin-top: 20px; text-align: justify; width: 550px;font-size: 11px">
	<table border="1" style="text-align: center; font-size: 16px" width="550px" >
     	<?= $tablascot?>
	</table>
	* Los precios de mayoreo aplican a partir de la compra de 1 ton. de camarón. <br />
	* Los precios de menudeo son exclusivos de bolsas en presentación de 1 kg. <br />
	* Las presentaciones que manejamos son: cajas de 10, 20 y 22 kg, así como bolsas de 1 kg.
	<br />
</div>
<div style="margin-left:130px; margin-top: 30px; text-align: justify; width: 550px">
	Agradeciendo de antemano sus atenciones, quedo a sus órdenes para cualquier aclaración al respecto.	
</div>
<div style="margin-left:130px; margin-top: 30px; text-align: justify; width: 550px">
	Atentamente
	<br /><br />
	<div style=" margin-top: 20px; text-align: justify; width: 550px">
	<table>
		<tr>
		<td><?= $usuario?></td>
		<td></td>
		<td></td>
		</tr>
		<tr>
		<td><?= $perfil?> </td>	
		<td style="width: 130px;"><img src="<?php echo base_url();?>assets/images/wats.png" width="20" height="20" border="0">&nbsp; <?= $cel?></td>
		<td style="width: 220px;"><img src="<?php echo base_url();?>assets/images/mail.png" width="20" height="20" border="0">&nbsp; <?= $correo?></td>
		</tr>
	</table>
	</div>
</div>
<div style="margin-top: 20px; text-align: justify; width: 750px">
	<table style="width: 750px;">
		<tr>
			<td style="text-align: center"><img src="<?php echo base_url();?>assets/images/www.png" width="20" height="20" border="0">
				<br />www.bonatto.com.mx
			</td>
			<td style="text-align: center">
				<img src="<?php echo base_url();?>assets/images/face.png" width="20" height="20" border="0">
				&nbsp;&nbsp;&nbsp;
				<img src="<?php echo base_url();?>assets/images/insta.png" width="20" height="20" border="0">
				<br />	Mariscos Bonatto
			</td>			
		</tr>
	</table>
	</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>