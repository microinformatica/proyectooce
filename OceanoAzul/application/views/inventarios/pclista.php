<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 18px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
#export_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datai { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datag { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_dataa { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datacot { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datarc { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li ><a href="#entradas"><img src="<?php echo base_url();?>assets/images/menu/embarque.png" width="24" height="24" border="0"> Entradas </a></li>
		 <?php if($usuario=="Jesus Benítez" or $usuario=="Zuleima Benitez" ){ ?>
		<li ><a href="#existencia"><img src="<?php echo base_url();?>assets/images/menu/inventarios.png" width="24" height="24" border="0"> Salidas </a></li>
		<?php } else { ?>
		<li ><a href="#existencia"><img src="<?php echo base_url();?>assets/images/menu/inventarios.png" width="24" height="24" border="0"> Inventario </a></li>						
		<?php } ?>
		<li ><a href="#vtas"><img src="<?php echo base_url();?>assets/images/menu/inventarios.png" width="24" height="24" border="0"> Existencia Tallas </a></li>
		<li ><a href="#cotiza"><img src="<?php echo base_url();?>assets/images/menu/facturas2.png" width="24" height="24" border="0"> Cotizaciones </a></li>
		<li ><a href="#repcotiza"><img src="<?php echo base_url();?>assets/images/menu/facturas2.png" width="24" height="24" border="0"> Rep Cotizaciones </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 650px"  >
		<div id="entradas" class="tab_content" style="margin-top: -5px">
			<div class="ajaxSorterDiv" id="tabla_ent" name="tabla_ent" style="width: 920px"  >
							<div class="ajaxpager" style="margin-top: -5px;" > 
            					<ul class="order_list" style="width: 850px; text-align: left" >
            						Ciclo
            						<select id="cicloe" style="font-size:12px; margin-top: 1px;" >
                    					<option value="21">2021</option><option value="20">2020</option><option value="19">2019</option>
   									</select>
            						Almacen
            						<select id="almente" style="font-size:10px; margin-top: 1px" >
                    					<option value="0">Todos</option>
          									<?php	
          										$data['result']=$this->inventarios_model->getAlmacenes();
												foreach ($data['result'] as $row):?>
           											<option value="<?php echo $row->ida;?>"><?php echo $row->noma;?></option>
           										<?php endforeach;?>
   									</select>
   									Granja
   									<select id="graente" style="font-size:10px; margin-top: 1px" >
                    					<option value="0">Todos</option>
          									<?php	
          										$data['result']=$this->inventarios_model->getGranjas();
												foreach ($data['result'] as $row):?>
           											<option value="<?php echo $row->idg;?>"><?php echo $row->nomg;?></option>
           										<?php endforeach;?>
   									</select>
   									Talla	
   									<select id="talente" style="font-size:10px; margin-top: 1px" >
                    					<option value="0">Todos</option>
          									<?php	
          										$data['result']=$this->inventarios_model->getTallas();
												foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->idt;?>"><?php echo $row->nomt;?></option>
           										<?php endforeach;?>
   									</select>	
                   				</ul>
 							</div>                 			
							<span class="ajaxTable" style="height: 600px; margin-top: 1px;width:910px " >
                    			<table id="mytablaEnt" name="mytablaEnt" class="ajaxSorter" border="1" >
                        			<thead >     
                        				<th data-order = "dia" style="font-size: 10px;">Dia</th><th data-order = "tg" style="font-size: 10px;">Total</th>    
                        				
                        				 <?php	
                        				    $cic=19;
          									$data['result']=$this->inventarios_model->getTallas();
											foreach ($data['result'] as $row):?>
           									<th data-order ="<?php echo 't'.$row->idt;?>" style="font-size: 10px;"><?php echo $row->nomt;?></option>
           								<?php endforeach;?>                  
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 10px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
		</div>
		 <?php if($usuario=="Jesus Benítez" or $usuario=="Zuleima Benitez" ){ ?>
		<div id="existencia" class="tab_content">
			<table border="1" style="width: 950px; margin-left: -5px; margin-top: -10px">
				<tr>
					<td width="600px" rowspan="11">
						<div id='ver_mas_detalle' class='hide' >
						<div class="ajaxSorterDiv" id="tabla_inv" name="tabla_inv" style="width: 610px; margin-left: -10px; margin-top: -10px "  >
							<div class="ajaxpager" style="margin-top: 1px;" > 
            					<ul class="order_list" style="width: 640px; text-align: left; font-size: 11px" >
            						<select id="ciclo" style="font-size:10px; margin-top: 1px;" >
                    					<option value="21">2021</option><option value="20">2020</option><option value="19">2019</option>
   									</select>
            						
            						<select id="alment" style="font-size:10px; margin-top: 1px" >
                    					<option value="0">Almacen</option>
          									<?php	
          										$data['result']=$this->inventarios_model->getAlmacenes();
												foreach ($data['result'] as $row):?>
           											<option value="<?php echo $row->ida;?>"><?php echo $row->noma;?></option>
           										<?php endforeach;?>
   									</select>
   									
   									<select id="graent" style="font-size:10px; margin-top: 1px" >
                    					<option value="0">Granja</option>
          									<?php	
          										$data['result']=$this->inventarios_model->getGranjas();
												foreach ($data['result'] as $row):?>
           											<option value="<?php echo $row->idg;?>"><?php echo $row->nomg;?></option>
           										<?php endforeach;?>
   									</select>
   									
   									<select id="talent" style="font-size:10px; margin-top: 1px" >
                    					<option value="0">Talla</option>
          									<?php	
          										$data['result']=$this->inventarios_model->getTallas();
												foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->idt;?>"><?php echo $row->nomt;?></option>
           										<?php endforeach;?>
   									</select>
   									
                   				</ul>
 							</div>                 			
							<span class="ajaxTable" style="height: 580px; margin-top: 1px; " >
                    			<table id="mytablaInv" name="mytablaInv" class="ajaxSorter" border="1" style="width: 593px" >
                        			<thead >     
                        				<th data-order = "cic" >Ciclo</th>                       
                        				<th data-order = "noma1" >Almacen</th>
                        				<th data-order = "nomg" >Granja</th>
                        				<th data-order = "nomt" >Talla</th>
                        				<th data-order = "kgsent" >Entradas</th>
                        				<th data-order = "kgssal" >Salidas</th>
                        				<th data-order = "exient"  >Existencia</th>
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 12px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
                		</div>
             			</div>
             			 <div id='ver_mas_tallas' class='hide'  >
             			 	Existencia General por Tallas en Almacenes
             			<div class="ajaxSorterDiv" id="tabla_exa" name="tabla_exa" style=" width: 610px;  margin-top: -5px; margin-left: -5px;  "  >
							<div class="ajaxpager" style="margin-top: 0px;" > 
            					<ul class="order_list" style=" text-align: left" >  				</ul>
 							</div>                 			
							<span class="ajaxTable" style="height: 580px;"  >
                    			<table id="mytablaExiact" name="mytablaExiact" class="ajaxSorter" border="1" style="width: 590px" >
                        			<thead >     
                        				<tr>
                        				<th data-order = "grupo" rowspan="2" >Talla</th>	
                        				<th colspan="11" >Almacenes</th>
                        				</tr>
                        				<tr>
                        				<th data-order = "exisel" >Selectos</th>
                        				<th data-order = "exifri" >Frizajal</th>
                        				<th data-order = "exibar" >Barlovento</th>
                        				<th data-order = "exisea" >Season</th>	
                        				<th data-order = "exisol" >Solman</th>
                        				<th data-order = "exibaj" >Baja Frio</th>
                        				<th data-order = "exifre" >Frezz&Lgs</th>
                        				<th data-order = "exiame" >Ameriben</th>
                        				<th data-order = "exiali" >CPMALI</th>
                        				<th data-order = "exizel" >ZelmaK</th>
                        				<th data-order = "exiact" >Total</th>
                        				</tr>
                        			</thead>
                        			<tbody style="font-size: 11px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			
             			</div>
             			
             			</div>
             		
             			<div style="margin-top:-4px; margin-left: 15px">
                			<button id="export_datai" type="button" style="width:150px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir Inventario</button>
                			</div>
									<form id="data_tablei" action="<?= base_url()?>index.php/inventarios/pdfrepinv" method="POST">
										<input type="hidden" name="tablasi" id="html_inv"/>
										
									</form>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#export_datai').click(function(){									
												$('#html_inv').val($('#mytablaInv').html());
												$('#data_tablei').submit();
												$('#html_inv').val('');
											});
										});
									</script>
									<div style="margin-top:-20px; margin-left: 200px">
										<button type="button" id="aligra" style="width: 80px; cursor: pointer; background-color: lightblue" class="continuar used" >Tallas</button>
									</div>
									<div style="margin-top:-28px; margin-left: 300px">
									<button id="export_datag" type="button" style="width:150px; cursor: pointer; background-color: lightyellow; text-align: right;"  >Existencia en Tallas</button>
									</div>
									<form id="data_tableg" action="<?= base_url()?>index.php/inventarios/pdfrepinvg" method="POST">
										<input type="hidden" name="tablasg" id="html_invg"/>
										
									</form>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#export_datag').click(function(){									
												$('#html_invg').val($('#mytablaExiact').html());
												$('#data_tableg').submit();
												$('#html_invg').val('');
											});
										});
									</script>	
									
									
					</td>
					<td colspan="4">	
						<div class="ajaxSorterDiv" id="tabla_sal" name="tabla_sal" style="margin-top: -10px; margin-left: -5px; "  >
							               			
							<span class="ajaxTable" style="height: 200px;"  >
                    			<table id="mytablaSal" name="mytablaSal" class="ajaxSorter" border="1"  >
                        			<thead >     
                        				
                        				<th data-order = "fecsal1" style="width: 50px" >Fecha</th>                       
                        				<th data-order = "folsal" >Folio</th>
                        				<th data-order = "estatus" >Estatus</th>
                        				<th data-order = "noma" >Almacen</th>
                        				
                        				<!--<th data-order = "nomc" >Cliente</th>-->
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			<div class="ajaxpager" style="margin-top: -5px;" > 
            					<ul class="order_list" style=" text-align: center; width: 320px; font: bold" >SALIDA</ul>
 							</div>  
             			</div>
             		</td>
             		</tr>
             		<tr style="background-color: lightblue">
             			<td colspan="2">Estatus</td>
             			<!--<td>Aviso</td>-->
             			<td>Fecha</td>
             			<td>Folio</td>
						
						
					</tr
             		<tr>
						<td colspan="2">
							<select id="estsal" style="font-size:13px; margin-top: 1px" >
                    			<option value="1">Venta</option>
          						<option value="2">Traslado</option>	
   							</select>
						</td>
						<!--
						<td><input id="aviso" style="width: 70px" /></td>
						-->
						<td><input size="9%" type="text" name="fecsal" id="fecsal" class="fecha redondo mUp" readonly="true" style="text-align: center; font-size: 13px" ></td>
						<td>
							<!--<input size="10px" type="text" name="folsal" id="folsal" readonly="true" style="text-align: center; font-size: 13px; color: red; border: none" >-->
							<input type="hidden" name="idsal" id="idsal" ><input type="hidden" name="idents" id="idents" >
							<input type="hidden" name="grasal" id="grasal" ><input type="hidden" name="kgssal" id="kgssal" >
							<input size="3px" type="text" name="folsal" id="folsal" readonly="true" style="text-align: center; font-size: 13px; color: blue; border: none" >
							<input size="3px" type="hidden" name="folsalu" id="folsalu" readonly="true" style="text-align: center; font-size: 13px; color: blue; border: none" >
							<!--
							<input size="3px" type="text" name="folsal" id="folsal" readonly="true" value="<?php echo set_value('rem',$txtuf); ?>" style="text-align: center; font-size: 13px; color: blue; border: none" >
							<input size="3px" type="hidden" name="folsalu" id="folsalu" readonly="true" value="<?php echo set_value('rem',$txtuf); ?>" style="text-align: center; font-size: 13px; color: blue; border: none" >
							-->
						</td>
						
						
					</tr>
					<tr style="background-color: lightblue">
						<td colspan="4">Cliente:
							<!--<select id="clisal1" style="font-size:13px; margin-top: 1px; width: 268px" >
                    		<option value="0">Todos</option>
          						<?php	
          							$data['result']=$this->inventarios_model->getClientes();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->idc;?>"><?php echo $row->nomc;?></option>
           						<?php endforeach;?>
   							</select>	
   							<br />-->
   							<select  id="clisal" style="font-size: 12px;height: 25px;margin-top: 1px;"  ></select>
   							
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<textarea  name="conc" id="conc"  cols="58" style="resize: none" rows="2"></textarea>
						</td>
					</tr>
					<tr style="background-color: lightblue">
						<td colspan="4">Almacen:
							<input type="hidden" name="almsal" id="almsal" >
							<input type="text" name="noma" id="noma" style="width: 200px; font-size: 12px; text-align: left; border: none;background-color: lightblue">
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<textarea  name="cona" id="cona"  cols="58" style="resize: none" rows="1"></textarea>
						</td>
					</tr>
					<tr style="background-color: lightblue">
						<td>Talla</td><td>Cajas</td><td>Presentación</td>
						<td rowspan="2">
							<input style="font-size: 14px" type="submit" id="nuevosd" name="nuevosd" value="Nuevo" />
							<input placeholder="Grupo" type="hidden" name="gpos" id="gpos" style="width: 30px; font-size: 15px; text-align: center" >
							<br />
							<input style="font-size: 14px" type="submit" id="aceptars" name="aceptars" value="Guardar" />
							<input style="font-size: 14px" type="submit" id="borrars" name="borrars" value="Borrar" />
							
						</td>
						
					</tr>
					<tr>
						<td>
							<input type="hidden" name="talsal" id="talsal" >
							<input type="text" name="nomt" id="nomt" style="width: 50px; font-size: 13px; text-align: left; border: none">
							
						</td>
						<td><input type="text" name="cajsal" id="cajsal" style="width: 50px; font-size: 13px; text-align: center"></td>
						<td><input type="text" name="presal" id="presal" style="width: 50px; font-size: 13px; text-align: center" value="22"> kgs</td>
						
					</tr>
					<tr>
						<td colspan="4">
							<div class="ajaxSorterDiv" id="tabla_sald" name="tabla_sald" style="margin-top: -25px; margin-left: -5px;"  >
								<div class="ajaxpager" style="margin-top: 0px;" > 
            						<ul class="order_list" style=" text-align: left" >	</ul>
 								</div>                 			
								<span class="ajaxTable" style="height: 100px;  " >
                    				<table id="mytablaSald" name="mytablaSald" class="ajaxSorter" border="1"  >
                        				<thead >     
                        					<th data-order = "nomt" >Talla</th>                       
                        					<th data-order = "cajsal">Cajas</th>
                        					<th data-order = "presal" >Pres. Kgs</th>
                        					<th data-order = "kgssal" >Kilos</th>
                        					<th data-order = "nomg" >Inventario</th>
                        				</thead>
                        				<tbody title="Seleccione para realizar cambios" style="font-size: 12px; text-align: center">
                        				</tbody>                        	
                    				</table>
                				</span> 
             				</div>
						</td>
					</tr>	
					<tr style="background-color: lightblue">
						<td colspan="2" style="text-align: center;">
							<input style="font-size: 14px" type="submit" id="nuevas" name="nuevas" value="Nueva Salida" />
							
						</td>
						<td colspan="2" style="text-align: center;">
							<button id="export_data" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
									<form id="data_tables" action="<?= base_url()?>index.php/inventarios/pdfrepsal" method="POST">
										<input type="hidden" name="tablasd" id="html_sal"/>
										<input type="hidden" type="text" id="dia" name="dia" >
										<input type="hidden" name="concs" id="concs">
										<input type="hidden" name="alms" id="alms">
										<input type="hidden" name="conas" id="conas">
										<input type="hidden" name="fols" id="fols">
										<input type="hidden" name="esttv" id="esttv">
										<input type="hidden" name="numalm" id="numalm">
										<input type="hidden" name="almori" id="almori">
									</form>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#export_data').click(function(){									
												$('#html_sal').val($('#mytablaSald').html());$('#fols').val('C'+$('#ciclo').val()+'-'+$('#folsal').val());
												$('#dia').val($('#fecsal').val());$('#concs').val($('#conc').val());
												$('#alms').val($('#noma').val());$('#conas').val($('#cona').val());
												$('#data_tables').submit();
												$('#html_sal').val('');
											});
										});
									</script>
						</td>
					</tr>	
			</table>		
 		</div>
 		 <?php } else {?>
 		 <div id="existencia" class="tab_content">
			<table border="1" style="width: 950px; margin-left: -5px; margin-top: -10px">
				<tr>
					<td width="570px" rowspan="2">
						<div class="ajaxSorterDiv" id="tabla_inve" name="tabla_inve" style="width: 620px; margin-left: -10px; margin-top: -10px "  >
							<div class="ajaxpager" style="margin-top: 1px;" > 
            					<ul class="order_list" style="width: 660px; text-align: left; font-size: 11px" >
            						<select id="ciclo" style="font-size:12px; margin-top: 1px;" >
                    					<option value="21">2021</option><option value="20">2020</option><option value="19">2019</option>
   									</select>
            						Alm
            						<select id="alment" style="font-size:10px; margin-top: 1px" >
                    					<option value="0">Todos</option>
          									<?php	
          										$data['result']=$this->inventarios_model->getAlmacenes();
												foreach ($data['result'] as $row):?>
           											<option value="<?php echo $row->ida;?>"><?php echo $row->noma;?></option>
           										<?php endforeach;?>
   									</select>
   									Gja
   									<select id="graent" style="font-size:10px; margin-top: 1px" >
                    					<option value="0">Todos</option>
          									<?php	
          										$data['result']=$this->inventarios_model->getGranjas();
												foreach ($data['result'] as $row):?>
           											<option value="<?php echo $row->idg;?>"><?php echo $row->nomg;?></option>
           										<?php endforeach;?>
   									</select>
   									Tal	
   									<select id="talent" style="font-size:10px; margin-top: 1px" >
                    					<option value="0">Todos</option>
          									<?php	
          										$data['result']=$this->inventarios_model->getTallas();
												foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->idt;?>"><?php echo $row->nomt;?></option>
           										<?php endforeach;?>
   									</select>
   									
                   				</ul>
 							</div>                 			
							<span class="ajaxTable" style="height: 580px; margin-top: 1px; " >
                    			<table id="mytablaInve" name="mytablaInve" class="ajaxSorter" border="1" style="width: 600px" >
                        			<thead >     
                        				<th data-order = "cic" >Ciclo</th>                       
                        				<th data-order = "noma1" >Almacen</th>
                        				<th data-order = "nomg" >Granja</th>
                        				<th data-order = "nomt" >Talla</th>
                        				<th data-order = "kgsent" >Entradas</th>
                        				<th data-order = "kgssal" >Salidas</th>
                        				<th data-order = "exient"  >Existencia</th>
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 12px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			<button id="export_datai" type="button" style="width:150px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir Inventario</button>
									<form id="data_tablei" action="<?= base_url()?>index.php/inventarios/pdfrepinv" method="POST">
										<input type="hidden" name="tablasi" id="html_inve"/>
										
									</form>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#export_datai').click(function(){									
												$('#html_inve').val($('#mytablaInve').html());
												$('#data_tablei').submit();
												$('#html_inve').val('');
											});
										});
									</script>	
             			</div>
					</td>
					<td colspan="4" rowspan="1">	
						Existencia General por Tallas en Almacenes
						<div class="ajaxSorterDiv" id="tabla_exa" name="tabla_exa" style="margin-top: -19px; margin-left: -5px;width: 325px  "  >
							<div class="ajaxpager" style="margin-top: 0px;" > 
            					<ul class="order_list" style=" text-align: left" >  				</ul>
 							</div>                 			
							<span class="ajaxTable" style="height: 470px;width: 315px"  >
                    			<table id="mytablaExiact" name="mytablaExiact" class="ajaxSorter" border="1"  >
                        			<thead >     
                        				<tr>
                        				<th data-order = "grupo" rowspan="2" >Talla</th>	
                        				<th colspan="11" >Almacenes</th>
                        				</tr>
                        				<tr>
                        				<th data-order = "exisel" >Selectos</th>
                        				<th data-order = "exifri" >Frizajal</th>	
                        				<th data-order = "exibar" >Barlovento</th>
                        				<th data-order = "exisea" >Season</th>
                        				<th data-order = "exisol" >Solman</th>
                        				<th data-order = "exibaj" >Baja Frio</th>
                        				<th data-order = "exifre" >Frezz&Lgs</th>
                        				<th data-order = "exiame" >Ameriben</th>
                        				<th data-order = "exiali" >CPMALI</th>
                        				<th data-order = "exizel" >ZelmaK</th>
                        				<th data-order = "exiact" >Total</th>
                        				</tr>
                        			</thead>
                        			<tbody style="font-size: 12px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			<button id="export_datag" type="button" style="width:150px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir General</button>
									<form id="data_tableg" action="<?= base_url()?>index.php/inventarios/pdfrepinvg" method="POST">
										<input type="hidden" name="tablasg" id="html_invg"/>
										
									</form>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#export_datag').click(function(){									
												$('#html_invg').val($('#mytablaExiact').html());
												$('#data_tableg').submit();
												$('#html_invg').val('');
											});
										});
									</script>	
             			</div>
             		</td>
             		
             		</tr>
             		<tr>
             			<td>General de Kgs. Recibidos y Procesados.
             				<div class="ajaxSorterDiv" id="tabla_todo" name="tabla_todo" style="margin-top: -8px; margin-left: -5px;  "  >
							<span class="ajaxTable" style="height: 95px;width: 315px"  >
                    			<table id="mytablaTodo" name="mytablaTodo" class="ajaxSorter" border="1"  >
                        			<thead >     
                        				<th data-order = "granja" >Procedencia</th>
                        				<th data-order = "fresco" >Fresco</th>
                        				<th data-order = "maquilado" >Procesado</th>	
                        				<th data-order = "rendimiento" >% Rend.</th>
                        				</tr>
                        			</thead>
                        			<tbody style="font-size: 14px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</td>
             		</tr>
			</table>		
 		</div>	
 		 <?php } ?>
 		 <div id="vtas" class="tab_content" style=" margin-top: -5px">
 		 	<div class="ajaxSorterDiv" id="tabla_actual" name="tabla_actual" style="width: 920px"  >
							<div class="ajaxpager" style="margin-top: -5px;" > 
            					<ul class="order_list" style="width: 750px; text-align: left" >
            						Existencia General por Tallas de acuerdo a lo Procesado y su equivalencia en venta.
            						Ciclo Sel:
            						<input id="ciex" name="ciex" style="border: none; background-color: none; width: 40px;" />
            						<button id="export_dataa" type="button" style="width:150px; cursor: pointer; background-color: lightyellow; text-align: right; margin-left: -5px"  >Imprimir Existencia</button>
									<form id="data_tablea" action="<?= base_url()?>index.php/inventarios/pdfrepact" method="POST">
										<input type="hidden" name="tablasa" id="html_act"/>
									</form>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#export_dataa').click(function(){									
												$('#html_act').val($('#mytablaActual').html());
												$('#data_tablea').submit();
												$('#html_act').val('');
											});
										});
									</script>	
                   				</ul>
                   				
 							</div>                 			
							<span class="ajaxTable" style="height: 600px; margin-top: 1px; " >
                    			<table id="mytablaActual" name="mytablaActual" class="ajaxSorter" border="1" style="width:900px" >
                        			<thead >     
                        				<tr>
                        				<th rowspan="2" data-order = "nomt" >Talla</th>
                        				<th colspan="4"  >Ahome</th>
                        				<th colspan="4"  >Kino</th>
                        				<th colspan="4"  >Huatabampo</th>
                        				<th colspan="4"  >Total</th>
                        				</tr>
                        				<tr>
                        				<th data-order = "procesadoa" >Pro</th>
                        				<th data-order = "ventasa" >Vta</th>	
                        				<th data-order = "precioa" >$ </th>
                        				<th data-order = "existenciaa" >Exi</th>	
                        				<th data-order = "procesadok" >Pro</th>
                        				<th data-order = "ventask" >Vta</th>
                        				<th data-order = "preciok" >$ </th>	
                        				<th data-order = "existenciak" >Exi</th>
                        				<th data-order = "procesadoh" >Pro</th>
                        				<th data-order = "ventash" >Vta</th>
                        				<th data-order = "precioh" >$ </th>	
                        				<th data-order = "existenciah" >Exi</th>
                        				<th data-order = "procesado" >Procesados</th>
                        				<th data-order = "ventas" >Ventas</th>	
                        				<th data-order = "existencia" >Existencia</th>
                        				<th data-order = "por" >%</th>
                        				</tr>
                        			</thead>
                        			<tbody style="font-size: 10px; text-align: right">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
		</div>
 		<div id="cotiza" class="tab_content">
			<table border="1" style="width: 950px; margin-left: -5px; margin-top: -10px">
				<tr>
					<td width="380px" rowspan="12">
						<div class="ajaxSorterDiv" id="tabla_cot" name="tabla_cot" style="margin-top: -10px; margin-left: -5px; "  >
							<div class="ajaxpager" style="margin-top: -5px;" > 
            					<ul class="order_list" style=" text-align: center; width: 290px; font: bold" >COTIZACIÓN
            					<select name="cicb" id="cicb" style="font-size: 10px; margin-top: 1px; margin-left: 12px; height: 23px;" >
											<?php $ciclof=2019; 
											//$actual=date("Y");
											$actual=2021; 
											$mes=date("m");$actual-=1;
											//if($mes>=11) $actual+=1;
											$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            									<?php $actual-=1; } ?>
          								</select> 	
            					</ul>
 							</div>                 			
							<span class="ajaxTable" style="height: 550px;"  >
                    			<table id="mytablaCot" name="mytablaCot" class="ajaxSorter" border="1"  >
                        			<thead >     
                        				<th data-order = "feccot1" >Fecha</th>                       
                        				<th data-order = "folcot1" >Folio</th>
                        				<th data-order = "nomcb" >Cliente</th>
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			
             			</div>
         			</td>
						<td style="background-color: lightblue" colspan="2">Fecha</td>
             			<td style="background-color: lightblue" colspan="2">Folio</td>
					</tr>
             		<tr>
						<td colspan="2"><input size="9%" type="text" name="feccb" id="feccb" class="fecha redondo mUp" readonly="true" style="text-align: center; font-size: 13px" ></td>
						<td colspan="2">
							<input size="3px" type="hidden" name="idtalcb" id="idtalcb" readonly="true" style="text-align: center; font-size: 13px; color: blue; border: none" >
							<input size="3px" type="text" name="foltalcb" id="foltalcb" readonly="true" style="text-align: center; font-size: 13px; color: blue; border: none" >
							<input size="3px" type="hidden"  name="folcotu" id="folcotu" readonly="true" style="text-align: center; font-size: 13px; color: blue; border: none" >							
						</td>
						
						
					</tr>
					<tr style="background-color: lightblue">
						<td colspan="4">Cliente:
							<select id="idclicb" style="font-size:13px; margin-top: 1px; width: 370px" >
                    			<option value="0">Nuevo</option>
          						<?php	
          							$data['result']=$this->inventarios_model->getClientescot();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->idclicb;?>"><?php echo $row->nomcb;?></option>
           						<?php endforeach;?>
   							</select>	
   							<input style="font-size: 14px" type="submit" id="aceptarcbcli" name="aceptarcbcli" value="Agregar/Actualizar" />
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<textarea  name="nomcb" id="nomcb"  cols="78" style="resize: none" rows="1"></textarea>
						</td>
					</tr>
					<tr style="background-color: lightblue">
						<td colspan="2">Domicilio:</td>
						<td colspan="2">Tel:</td>
					</tr>
					<tr>
						<td colspan="2">
							<textarea  name="domcb" id="domcb"  cols="78" style="resize: none" rows="1"></textarea>
						</td>
						<td colspan="2">
							<input type="text" name="telcb" id="telcb" style="width: 80px; font-size: 12px; text-align: left; ">
						</td>
					</tr>
					<tr style="background-color: lightblue">
						<td colspan="4">
						REGISTRO DE TALLAS
						</td>
					</tr>
					
					<tr style="background-color: lightblue">
						<td>Zona de Retiro</td><td>Talla</td><td>Menudeo</td><td>Mayoreo</td>
					</tr>
					<tr style="text-align: center">
						<td>
							<textarea  name="zonacb" id="zonacb"  cols="38" style="resize: none" rows="2"></textarea>
						</td>
						<td>
							<select name="talcb" id="talcb" style="font-size: 12px;height: 25px;margin-top: 1px; width: 170px"  ></select>
						</td>
						<td style="text-align: center"><input type="text" name="pmtalcb" id="pmtalcb" style="width: 50px; font-size: 13px; text-align: center"></td>
						<td style="text-align: center"><input type="text" name="pytalcb" id="pytalcb" style="width: 50px; font-size: 13px; text-align: center"></td>
						
					</tr>
					<tr style="background-color: lightblue">
						<td colspan="4">
							<input style="font-size: 14px" type="submit" id="nuevoscb" name="nuevoscb" value="Nuevo" />
							<input style="font-size: 14px" type="submit" id="aceptarcb" name="aceptarcb" value="Agregar/Actualizar" />
							<input style="font-size: 14px" type="submit" id="borrarcb" name="borrarcb" value="Borrar" />
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<div class="ajaxSorterDiv" id="tabla_cotd" name="tabla_cotd" style="margin-top: -20px; margin-left: -5px;"  >
								<div class="ajaxpager" style="margin-top: 0px;" > 
            						<ul class="order_list" style=" text-align: left" >	</ul>
 								</div>                 			
								<span class="ajaxTable" style="height: 300px;  " >
                    				<table id="mytablaCotd" name="mytablaCotd" class="ajaxSorter" border="1"  >
                        				<thead >     
                        					<th data-order = "zonacb" >Zona de Retiro</th>
                        					<th data-order = "nomt" >Talla</th>                       
                        					<th data-order = "pmtalcb1">Menudeo</th>
                        					<th data-order = "pytalcb1" >Mayoreo</th>
                        				</thead>
                        				<tbody title="Seleccione para realizar cambios" style="font-size: 14px; text-align: center">
                        				</tbody>                        	
                    				</table>
                				</span> 
             				</div>
						</td>
					</tr>	
					<tr style="background-color: lightblue">
						<td colspan="3" style="text-align: center;">
							<input style="font-size: 14px" type="submit" id="nuevacot" name="nuevacot" value="Nueva Cotización" />
							
						</td>
						<td colspan="2" style="text-align: center;">
							<button id="export_datacot" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right;  margin-left: -5px"  >Imprimir</button>
									<form id="data_tablecot" action="<?= base_url()?>index.php/inventarios/pdfrepcot" method="POST">
										<input type="hidden" name="tablacot" id="html_cot"/>
										<input type="hidden" name="ctfol" id="ctfol">
										<input type="hidden" name="ctdia" id="ctdia"  >
										<input type="hidden" name="ctcli" id="ctcli">
										<input type="hidden" name="ctdom" id="ctdom">
										<input type="hidden" name="cttel" id="cttel">
										
									</form>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#export_datacot').click(function(){									
												$('#html_cot').val($('#mytablaCotd').html());
												$('#ctfol').val('C'+$('#cicb').val()+'-'+$('#foltalcb').val());
												$('#ctdia').val($('#feccb').val());
												$('#ctcli').val($('#nomcb').val());
												$('#ctdom').val($('#domcb').val());
												$('#cttel').val($('#telcb').val());
												$('#data_tablecot').submit();
												$('#html_cot').val('');
											});
										});
									</script>
						</td>
					</tr>	
			</table>		
 		</div>
         <div id="repcotiza" class="tab_content" style=" margin-top: -5px">
 		 	
 		 	<div class="ajaxSorterDiv" id="tabla_rc" name="tabla_rc" style="width: 920px"  >
							<div class="ajaxpager" style="margin-top: -5px;" > 
            					<ul class="order_list" style="width: 850px; text-align: left" >
            						Cliente:
            						<select id="clirc" style="font-size:13px; margin-top: 1px; width: 370px" >
                    					<option value="0">Todos</option>
          									<?php	
          									$data['result']=$this->inventarios_model->getClientescot();
											foreach ($data['result'] as $row):?>
           									<option value="<?php echo $row->idclicb;?>"><?php echo $row->nomcb;?></option>
           									<?php endforeach;?>
   									</select>
   									Talla:
   									<select  id="talrc" style="font-size: 12px;height: 25px;margin-top: 1px; width: 170px"  ></select>
            						<button id="export_datarc" type="button" style="width:80px; cursor: pointer; background-color: lightyellow; text-align: right; margin-left: -5px"  >Imprimir</button>
									<form id="data_tablerc" action="<?= base_url()?>index.php/inventarios/pdfrepcotrc" method="POST">
										<input type="hidden" name="tablarc" id="html_rc"/>
										
									</form>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#export_datarc').click(function(){									
												$('#html_rc').val($('#mytablaRC').html());
												$('#data_tablerc').submit();
												$('#html_rc').val('');
											});
										});
									</script>	
                   				</ul>
                   				
 							</div>                 			
							<span class="ajaxTable" style="height: 600px; margin-top: 1px; " >
                    			<table id="mytablaRC" name="mytablaRC" class="ajaxSorter" border="1" style="width:900px" >
                        			<thead >     
                        				<tr>
                        				<th data-order = "nomcb" >Cliente</th>
                        				<!--<th data-order = "zonacb" >Zona</th>-->
                        				<th data-order = "nomt" >Talla</th>
                        				<th data-order = "feccb" >Fecha</th>	
                        				<th data-order = "foltalcb" >Folio</th>
                        				<th data-order = "pmtalcb">Menudeo</th>
                        				<th data-order = "pytalcb" >Mayoreo</th>
                        				</tr>
                        				                 
                        			</thead>
                        			<tbody style="font-size: 11px; text-align: center">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
		</div>    
 		 </div>
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
  $("#nuevoscb").click( function(){	
	$("#idtalcb").val(0);$("#talcb").val(0);$("#pmtalcb").val('');$("#pytalcb").val('');$("#zonacb").val('');
	return true;
	});
$("#nuevacot").click( function(){	
	$("#idclicb").val(0);$("#nomcb").val('');$("#domcb").val('');$("#telcb").val('');
	$("#folcotu").val(0);
	$("#nuevacot").change();
	$("#nuevoscb").click();
	$("#mytablaCotd").trigger("update");
	return true;
});
  $('#talcb').boxLoader({
     	url:'<?php echo base_url()?>index.php/inventarios/combocb',
     	equal:{id:'cic',value:'#cicb'},
     	select:{id:'idt',val:'val'},
     	all:"Sel.",            
	});
$('#talrc').boxLoader({
     	url:'<?php echo base_url()?>index.php/inventarios/comborc',
     	equal:{id:'cic',value:'#cicb'},
     	select:{id:'idt',val:'val'},
     	all:"Sel.",            
	});	
$("#ciclo").change( function(){
	$("#ciex").val('['+$("#ciclo").val()+']');	
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/inventarios/ultimofolio", 
			data: "cic="+$("#ciclo").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#folsal").val(obj.folio);$("#folsalu").val(obj.folio);
					$("#mytablaSald").trigger("update");
   			} 
	});	
});

$("#nuevacot").change( function(){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/inventarios/ultimofoliocot", 
			//data: "cic="+$("#ciclo").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#foltalcb").val(obj.folio);$("#folcotu").val(obj.folio);
					$("#mytablaCot").trigger("update");
   			} 
	});	
});
$("#talcb").change( function(){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/inventarios/precios", 
			data: "tal="+$("#talcb").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#pmtalcb").val(obj.premen);$("#pytalcb").val(obj.premay);
   			} 
	});	
});

$("#idclicb").change( function(){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/inventarios/datoscli", 
			data: "cli="+$("#idclicb").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#nomcb").val(obj.nomcb);$("#domcb").val(obj.domcb);$("#telcb").val(obj.telcb);
   			} 
	});	
});

$("#aceptarcbcli").click( function(){		
	//cli=$("#clisal").val();alm=$("#almsal").val();caj=$("#cajsal").val();
	numero=$("#idclicb").val();
	  if(numero!=0){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/inventarios/actualizarcotcli", 
						data: "id="+$("#idclicb").val()+"&nom="+$("#nomcb").val()+"&dom="+$("#domcb").val()+"&tel="+$("#telcb").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
									location.reload();	
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/inventarios/agregarcotcli", 
						data: "nom="+$("#nomcb").val()+"&dom="+$("#domcb").val()+"&tel="+$("#telcb").val(),
						success: 
								function(msg){	
									if(msg!=0){														
									location.reload();	
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		
});
$("#borrarcb").click( function(){	
	numero=$("#idtalcb").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/inventarios/borrar", 
						data: "id="+$("#idtalcb").val()+"&tabla=cotbtalla"+"&campo=idtalcb",
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaCotd").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Talla para poder Eliminarla");
		return false;
	}
});

$("#aceptarcb").click( function(){		
	//cli=$("#clisal").val();alm=$("#almsal").val();caj=$("#cajsal").val();
	numero=$("#idtalcb").val();
	  if(numero!=0){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/inventarios/actualizarcot", 
						data: "id="+$("#idtalcb").val()+"&tal="+$("#talcb").val()+"&pmen="+$("#pmtalcb").val()+"&pmay="+$("#pytalcb").val()+"&zon="+$("#zonacb").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevoscb").click();
										$("#mytablaCot").trigger("update");
										$("#mytablaCotd").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/inventarios/agregarcot", 
						data: "fec="+$("#feccb").val()+"&fol="+$("#foltalcb").val()+"&cli="+$("#idclicb").val()+"&tal="+$("#talcb").val()+"&pmen="+$("#pmtalcb").val()+"&pmay="+$("#pytalcb").val()+"&zon="+$("#zonacb").val(),
						success: 
								function(msg){	
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevoscb").click();
										$("#mytablaCot").trigger("update");
										$("#mytablaCotd").trigger("update");
										
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		
});


$('#aligra').click(function(){
  // $('#ver_mas_ajaxsorters').show();
   if($(this).hasClass('used')){
    		$(this).removeClass('used');	
    		$('#ver_mas_detalle').hide();
			$('#ver_mas_tallas').show();
			$(this).text('Detalle');
    	}    			
		else{
			$(this).addClass('used');
			
			$('#ver_mas_detalle').show();
    		$('#ver_mas_tallas').hide();
    		$(this).text('Tallas');
		}
});

 // $('#aligra').click(function(){$(this).removeClass('used');$('#ver_mas_detalle').hide();$('#ver_mas_tallas').show();});
 
  
  $('#clisal').boxLoader({
     			url:'<?php echo base_url()?>index.php/inventarios/combob',
     			equal:{id:'est',value:'#estsal'},
     			select:{id:'Numero',val:'val'},
     			all:"Sel.",            
	});	

//busca datos del contacto
$("#clisal").change( function(){
	$("#numalm").val($("#clisal").val());	
 	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/inventarios/buscarc", 
			data: "cli="+$("#clisal").val()+"&est="+$("#estsal").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#conc").val(obj.conc);
					$("#clis").val(obj.nomc);
					$("#aviso").val(obj.avisoc);
   			} 
	});			 
});
$("#estsal").change( function(){
	$("#esttv").val($("#estsal").val());$("#conc").val("");
	$('#clisal').boxLoader({
     			url:'<?php echo base_url()?>index.php/inventarios/combob',
     			equal:{id:'est',value:'#estsal'},
     			select:{id:'idc',val:'val'},
     			all:"Sel.",            
			});	
			
});

$("#borrars").click( function(){	
	numero=$("#idef").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/inventarios/borrar", 
						data: "id="+$("#idsal").val()+"&tabla=salidas"+"&campo=idsal"+"&est="+$("#estsal").val()+"&fol="+$("#folsal").val()+"&tal="+$("#talsal").val()+"&cic="+$("#ciclo").val(),
						//data: "id="+$("#idsal").val()+"&tabla=salidas"+"&campo=idsal"+"&ident="+$("#idents").val()+"&kgs="+$("#kgssal").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#nuevosd").click();
										$("#mytablaInv").trigger("update");
										$("#mytablaSal").trigger("update");
										$("#mytablaSald").trigger("update");
										$("#mytablaActual").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Biometria para poder Eliminarla");
		return false;
	}
});
$("#nuevas").click( function(){	
	$("#nuevosd").click();
	$("#mytablaInv").trigger("update");
	$("#mytablaSal").trigger("update");
	$("#ciclo").change();
	//$("#folsal").val($("#fosal").val())+1);
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	$("#fecsal").val(f.getFullYear() + "-" + mes + "-" + dia);
	//$("#folsal").val(parseFloat($("#folsalu").val()));
	$("#clisal").val('');$("#numalm").val('');
	$("#nomc").val('');$("#clis").val('');
	$("#almsal").val('');$("#noma").val('');
    $("#conc").val('');
    $("#cona").val('');$("#cona").val('');
    $("#mytablaSald").trigger("update");
 	return true;
});

$("#nuevosd").click( function(){	
	$("#idsal").val('');$("#nomt").val('');$("#talsal").val('');$("#cajsal").val('');$("#presal").val('22');
	return true;
});
$("#aceptars").click( function(){		
	cli=$("#clisal").val();alm=$("#almsal").val();caj=$("#cajsal").val();
	numero=$("#idsal").val();
	if( cli>0){
	if( alm>0){
	if( caj>0){		
	  if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/inventarios/actualizarsal", 
						data: "id="+$("#idsal").val()+"&caj="+$("#cajsal").val()+"&pre="+$("#presal").val()+"&coa="+$("#cona").val()+"&coc="+$("#conc").val()+"&est="+$("#estsal").val()+"&idents="+$("#idents").val()+"&gpos="+$("#gpos").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevosd").click();
										$("#mytablaInv").trigger("update");
										$("#mytablaSald").trigger("update");
										$("#mytablaActual").trigger("update");
										$("#mytablaExiact").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/inventarios/agregarsal", 
						data: "fec="+$("#fecsal").val()+"&est="+$("#estsal").val()+"&alm="+$("#almsal").val()+"&cli="+$("#clisal").val()+"&caj="+$("#cajsal").val()+"&pre="+$("#presal").val()+"&tal="+$("#talsal").val()+"&ent="+$("#idents").val()+"&fol="+$("#folsal").val()+"&coa="+$("#cona").val()+"&coc="+$("#conc").val()+"&gra="+$("#grasal").val()+"&clo="+$("#ciclo").val()+"&gpos="+$("#gpos").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevosd").click();
										$("#mytablaInv").trigger("update");
										$("#mytablaSald").trigger("update");
										$("#mytablaActual").trigger("update");
										$("#mytablaExiact").trigger("update");
										
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		}else{
			alert("Error: Registre la cantidad de cajas");	
			$("#cajsal").focus();
			return false;
		}	
		}else{
		alert("Error: Seleccione Almacen de Salida");	
		$("#almsal").focus();
		return false;
	}				
	}else{
		alert("Error: Seleccione Cliente");	
		$("#clisal").focus();
		return false;
	}	
});
$("#fece").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#ide").val('');$("#idge").val('0');$("#idpe").val('0');
		$("#mytablaMaq").trigger("update");$("#mytablaMaqf").trigger("update");$("#mytablaMaqp").trigger("update");
	}
});
$("#fecsal").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2018:+0",
});
$("#feccb").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2020:+0",
});


jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});



$(document).ready(function(){ 
	$("#ciclo").change();$("#nuevacot").change();$("#cicb").change();
	$('#ver_mas_tallas').hide();
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	//$("#folsal").val($("#fosal").val())+1);
   	$("#fece").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#fecsal").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#estsal").val('1');
   	$("#esttv").val('1');
   	$("#almente").val('1');
   	$("#folsalu").val($("#folsal").val());
   	$("#tabla_actual").ajaxSorter({
		url:'<?php echo base_url()?>index.php/inventarios/tablaactual', 
		filters:['ciclo'], 		
		sort:false,
		onSuccess:function(){    
			$('#tabla_actual tbody tr').map(function(){	    		
	    		if($(this).data('nomt')=='Total:'){
	    			 $(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','right');   				    				    			    			
	    		}    	
	    	});
	    	$('#tabla_actual tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			$('#tabla_actual tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_actual tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_actual tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			$('#tabla_actual tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			$('#tabla_actual tbody tr td:nth-child(13)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			$('#tabla_actual tbody tr td:nth-child(16)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold');$(this).css('background','lightblue');})
    	},
    });
   	$("#tabla_todo").ajaxSorter({
		url:'<?php echo base_url()?>index.php/inventarios/tablatodo', 
		filters:['ciclo'], 		
		sort:false,
		onSuccess:function(){    
			$('#tabla_todo tbody tr').map(function(){	    		
	    		if($(this).data('granja')=='Total:'){
	    			 $(this).css('font-weight','bold');   				    				    			    			
	    		}    	
	    	});
			$('#tabla_todo tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_todo tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right');})
			
			$('#tabla_todo tbody tr td:nth-child(4)').map(function(){ 
				$(this).css('text-align','right');
				$(this).css('text-align','center');$(this).css('font-weight','bold');
	    		if($(this).html() >= 70.00 && $(this).html() != '' ) {
                	$(this).css("background-color", "#0F0"); 
            	}
	    		if($(this).html() >= 69.50 && $(this).html() <= 69.99 && $(this).html() != '' ) {
                	$(this).css("background-color", "orange"); $(this).css("color", "white");
            	}
	    		if($(this).html() < 69.50 && $(this).html() != '' ) {
              		$(this).css("background-color", "red"); $(this).css("color", "white");
        		}
			})
    	},
    });
	$("#tabla_ent").ajaxSorter({
		url:'<?php echo base_url()?>index.php/inventarios/tablaent',  		
		filters:['cicloe','almente','graente','talente'],
		sort:false,
		onSuccess:function(){    
    		$('#tabla_ent tbody tr td:nth-child(1)').map(function(){$(this).css('text-align','center'); $(this).css('font-weight','bold');$(this).css('background','lightblue'); })
	    	$('#tabla_ent tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold');$(this).css('background','lightblue');}) 
	    	$('#tabla_ent tbody tr').map(function(){
		    	if($(this).data('dia')=='Total:') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    })	  	
	    }, 
    });
    
	$("#tabla_inv").ajaxSorter({
		url:'<?php echo base_url()?>index.php/inventarios/tablainv',  		
		filters:['ciclo','alment','graent','talent'],
		sort:false,
		onRowClick:function(){
			if($(this).data('pre')=='1'){
			$("#idsal").val('');	$("#idents").val($(this).data('ident')); $("#grasal").val($(this).data('graent'));
    		$("#noma").val($(this).data('noma'));$("#almsal").val($(this).data('alment'));$("#almori").val($(this).data('alment'));
    		$("#nomt").val($(this).data('nomt'));$("#talsal").val($(this).data('talent'));
    		$("#cona").val($(this).data('cona'));$("#presal").val('22');$("#gpos").val($(this).data('gpoid'));
    		$("#cajsal").focus();  
    		}  		
    	},
		onSuccess:function(){  
			$('#tabla_inv tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold'); $(this).css('text-align','center'); })  
			$('#tabla_inv tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold'); })
			$('#tabla_inv tbody tr td:nth-child(3)').map(function(){ $(this).css('font-weight','bold'); })
    		//$('#tabla_inv tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })
	    	$('#tabla_inv tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })   	
	    	$('#tabla_inv tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_inv tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_inv tbody tr').map(function(){	    		
	    		if($(this).data('nomt')=='Total:'){
	    			$(this).css('background','lightblue');	 $(this).css('font-weight','bold');   				    				    			    			
	    		}    	
	    	});
	    	$('#tabla_inv tbody tr').map(function(){	    		
	    		if($(this).data('pre')=='1'){
	    			$(this).css('font-weight','bold');   				    				    			    			
	    		} else{
	    			$(this).css('font-size','10px');$(this).css('text-align','right');
	    		}   	
	    	});  
    	}, 
    });
   
   $("#tabla_inve").ajaxSorter({
		url:'<?php echo base_url()?>index.php/inventarios/tablainv',  	//se llamaba para efrain tablainve	
		filters:['ciclo','alment','graent','talent'],
		sort:false,
		onRowClick:function(){
			/*if($(this).data('pre')=='1'){
			$("#idsal").val('');	$("#idents").val($(this).data('ident')); $("#grasal").val($(this).data('graent'));
    		$("#noma").val($(this).data('noma'));$("#almsal").val($(this).data('alment'));$("#almori").val($(this).data('alment'));
    		$("#nomt").val($(this).data('nomt'));$("#talsal").val($(this).data('talent'));
    		$("#cona").val($(this).data('cona'));$("#presal").val('22');
    		$("#cajsal").focus();  
    		}*/  		
    	},
		onSuccess:function(){  
			$('#tabla_inve tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold'); $(this).css('text-align','center'); })  
			$('#tabla_inve tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold'); })
			$('#tabla_inve tbody tr td:nth-child(3)').map(function(){ $(this).css('font-weight','bold'); })
    		//$('#tabla_inv tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })
	    	$('#tabla_inve tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })   	
	    	$('#tabla_inve tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_inve tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_inve tbody tr').map(function(){	    		
	    		if($(this).data('nomt')=='Total:'){
	    			$(this).css('background','lightblue');	 $(this).css('font-weight','bold');   				    				    			    			
	    		}    	
	    	});
	    	$('#tabla_inve tbody tr').map(function(){	    		
	    		if($(this).data('pre')=='1'){
	    			$(this).css('font-weight','bold');   				    				    			    			
	    		} else{
	    			$(this).css('font-size','10px');$(this).css('text-align','right');
	    		}   	
	    	});  
    	}, 
    });
    $("#tabla_exa").ajaxSorter({
		url:'<?php echo base_url()?>index.php/inventarios/tablaexa',  		
		filters:['ciclo'],
		sort:false,
		onSuccess:function(){    
			$('#tabla_exa tbody tr').map(function(){	    		
	    		if($(this).data('grupo')=='Total:'){
	    			 $(this).css('font-weight','bold');   				    				    			    			
	    		}    	
	    	});
			$('#tabla_exa tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_exa tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_exa tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_exa tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_exa tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_exa tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_exa tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_exa tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_exa tbody tr td:nth-child(10)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_exa tbody tr td:nth-child(11)').map(function(){ $(this).css('text-align','right');})
			$('#tabla_exa tbody tr td:nth-child(12)').map(function(){ $(this).css('text-align','right');$(this).css("background-color", "lightblue");})
			
    	},
    });
   
    $("#tabla_sal").ajaxSorter({
		url:'<?php echo base_url()?>index.php/inventarios/tablasal',  		
		filters:['ciclo','estsal'],
		sort:false,
		onRowClick:function(){
			$("#estsal").val($(this).data('estsal'));$("#esttv").val($(this).data('estsal'));
			//$("#estsal").trigger("update");
			$("#folsal").val($(this).data('folsal'));
			clisal=$(this).data('clisal');
			$("#nomc").val($(this).data('nomc'));$("#clis").val($(this).data('nomc'));
			$("#fecsal").val($(this).data('fecsal'));		
    		$("#almsal").val($(this).data('almsal'));$("#noma").val($(this).data('noma'));
    		$("#conc").val($(this).data('consalc'));
    		$("#cona").val($(this).data('cona'));$("#cona").val($(this).data('consala'));
    		$("#cajsal").focus();
    		$("#clisal").val($(this).data('clisal'));$("#numalm").val($(this).data('clisal'));
    		$("#tabla_sald").trigger("update");
			
    		
    	},
		onSuccess:function(){    
			$('#tabla_sal tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center');$(this).css("background-color", "blue");$(this).css("color", "white");})
    	}, 
    });
   $("#tabla_sald").ajaxSorter({
		url:'<?php echo base_url()?>index.php/inventarios/tablasald',  		
		filters:['ciclo','folsal'],
		sort:false,
		onRowClick:function(){
			if($(this).data('idsal')>0){
				$("#idsal").val($(this).data('idsal'));			
    			$("#talsal").val($(this).data('talsal'));
    			$("#nomt").val($(this).data('nomt'));
    			$("#cajsal").val($(this).data('cajsal'));
    			$("#presal").val($(this).data('presal'));
    			$("#cona").val($(this).data('consala'));
    			$("#conc").val($(this).data('consalc'));
    			$("#kgssal").val($(this).data('kgssal'));
    			$("#idents").val($(this).data('idents'));
    			$("#gpos").val($(this).data('gpos'));
    		}
    	},
		onSuccess:function(){    
    		//$('#tabla_inv tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })
    	}, 
    });
    $("#tabla_cot").ajaxSorter({
		url:'<?php echo base_url()?>index.php/inventarios/tablacot',  		
		//filters:['ciclo','folsal'],
		sort:false,
		onRowClick:function(){
			if($(this).data('foltalcb')>0){
				$("#foltalcb").val($(this).data('foltalcb'));$("#folcotu").val($(this).data('foltalcb'));			
    			$("#idclicb").val($(this).data('clicb'));
    			$("#nomcb").val($(this).data('nomcb'));$("#telcb").val($(this).data('telcb'));$("#domcb").val($(this).data('domcb'));
    			$("#feccb").val($(this).data('feccb'));
    			$("#tabla_cotd").ajaxSorter({
					url:'<?php echo base_url()?>index.php/inventarios/tablacotd',  		
					filters:['folcotu'],
					sort:false,
					onRowClick:function(){
						if($(this).data('idtalcb')>0){
							$("#idtalcb").val($(this).data('idtalcb'));			
							$("#talcb").val($(this).data('talcb'));
    						$("#pmtalcb").val($(this).data('pmtalcb'));
    						$("#pytalcb").val($(this).data('pytalcb'));    			
    						$("#zonacb").val($(this).data('zonacb'));
    					}
    				},		
    			});
    		}
    	},
		onSuccess:function(){    
    		//$('#tabla_inv tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })
    	}, 
    });
    $("#tabla_rc").ajaxSorter({
		url:'<?php echo base_url()?>index.php/inventarios/tablarc', 
		filters:['clirc','talrc'], 		
		sort:false,
		onSuccess:function(){    
			$('#tabla_rc tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','left');})
    	},
    });
}); 
</script>
