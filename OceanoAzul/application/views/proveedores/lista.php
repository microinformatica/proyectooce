<?php $this->load->view('header_cv'); ?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/proveedores.png" width="25" height="25" border="0"> Proveedores </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion">
			<h3 align="left"><a href="">Registrados</a></h3>
        <div  style="height:348px; text-align: right; font-size: 12px;"> 
        	Filtrar aquellos que les falte la captura de: 
        	<select name="cmbSindatos" id="cmbSindatos" style="font-size: 10px; height: 25px; width:105px; margin-top: -15px;" >
          				<option value="0">Elija Opcion</option>
          				<option value="1">Domicilio</option><option value="2">Localidad</option><option value="3">Estado</option>
          				<option value="4">RFC</option><option value="5">CP</option><option value="6">Abreviatura</option>
          	</select>                                                                   
            <div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" style="margin-top: 1px; height: 367px; " >                
                <span class="ajaxTable" style="height: 301px; width: 882px">
                    <table id="mytabla" name="mytabla" class="ajaxSorter" width="880px">
                        <thead title="" >                            
                            <th data-order = "Siglas" >Abreviatura</th>
                            <th data-order = "Razon" >Razon Social</th>                                                        
                            <th data-order = "Dom" >Domicilio</th>
                            <th data-order = "Loc" >Localidad</th>
                            <th data-order = "Edo" >Estado</th>
                            <th data-order = "RFC" >RFC</th>                            
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">                                                 	  
                        </tbody>
                    </table>
                </span> 
             	<div class="ajaxpager" style="margin-top: -5px">        
                    <ul class="order_list"></ul>     
                    <form method="post" action="<?= base_url()?>index.php/proveedores/pdfrep" >                    	             	                                                
                        <img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                        <input type="text" class="pagedisplay" size="3" /> /
                        <input type="text" class="pagedisplayMax" size="3" readonly="1" disabled="disabled"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                        <select class="pagesize" style="font-size: 10px; height: 23px">
                                <option selected="selected" value="10">10</option>
                                <option value="20">20</option>                            
                                <option value='0'>Gral</option>                                          
                        </select>                                                
                        <img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <!--<button id="imprimirB" name="imprimirB" style="height: 28px; width: 25px;"></button>-->
                        <!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 					</form>   
 					
                </div>                                      
            </div>    
        </div>
        <h3 align="left"><a href="" >Agregar Nuevo</a></h3>
       	<div style="height:150px; ">  
        	<table style="width: 780px; " border="2px"     >
							<thead style="background-color: #DBE5F1" ><th colspan="4" height="18px" style="font-size: 25px"><center>Datos de Proveedor</center>
									<input type="hidden"  name="id" id="id"/></th>
							</thead>
							<tbody style="background-color: #F7F7F7">
								<tr>
									<th><label style="margin: 2px; font-size: 20px" for="nombre">Razón Social:</label></th>
									<th colspan="3" style="font-size: 18px; background-color: white"><input size="71%"  type="text" name="nombre" id="nombre" ></th>
								</tr>
								<tr>
									<th><label style="margin: 2px; font-size: 20px" for="rfc">RFC:</label></th>
									<th style="font-size: 18px; background-color: white"><input type="text" name="rfc" id="rfc" value=""></th>
									<th><label style="margin: 2px; font-size: 20px" for="siglas">Abreviatura:</label></th>
									<th style="font-size: 18px; background-color: white"><input type="text" name="siglas" id="siglas" value=""></th>
								</tr>
								<tr>
									<th><label style="margin: 2px; font-size: 20px" for="dom">Domicilio:</label></th>
									<th colspan="3" style="font-size: 18px; background-color: white"><input size="71%" type="text" name="dom" id="dom" value=""></th>
								</tr>
    							<tr>
									<th><label style="margin: 2px; font-size: 20px" for="loc">Localidad:</label></th>
									<th colspan="3" style="font-size: 18px; background-color: white"><input size="71%" type="text" name="loc" id="loc" value=""></th>
								</tr>
    							<tr>
									<th><label style="margin: 2px; font-size: 20px" for="edo">Estado:</label></th>
									<th style="font-size: 18px; background-color: white">
										<select name="edo" id="edo" style="margin-top: 1px">
    										<option value="-" >-</option> <option value="Aguascalientes">Aguascalientes</option>
    										<option value="Baja California">Baja California</option> <option value="Baja California Sur" >Baja California Sur</option>	
    										<option value="Campeche">Campeche</option> <option value="Chiapas"  >Chiapas</option>	
    										<option value="Chihuahua">Chihuahua</option> <option value="Coahulia" >Coahuila</option>
    										<option value="Colima" >Colima</option> <option value="Distrito Federal" >Distrito Federal</option>	
    										<option value="Durango" >Durango</option> <option value="Guanajuato" >Guanajuato</option>	
    										<option value="Guerrero" >Guerrero</option> <option value="Hidalgo" >Hidalgo</option>	
    										<option value="Jalisco" >Jalisco</option> <option value="Estado de Mexico" >Estado de M&eacute;xico</option>
    										<option value="Michoacan" >Michoac&aacute;n</option> <option value="Morelos" >Morelos</option>		
    										<option value="Nayarit" >Nayarit</option> <option value="Nuevo Leon" >Nuevo Le&oacute;n</option>			
    										<option value="Oaxaca" >Oaxaca</option> <option value="Puebla" >Puebla</option>
    										<option value="Queretaro" >Queretaro</option> <option value="Quintana Roo" >Quintana Roo</option>
    										<option value="San Luis Potosi" >San Luis Potos&iacute;</option> <option value="Sinaloa" >Sinaloa</option>
    										<option value="Sonora" >Sonora</option> <option value="Tamaulipas" >Tamaulipas</option>
    										<option value="Tabasco" >Tabasco</option> <option value="Tlaxcala" >Tlaxcala</option>
    										<option value="Veracruz" >Veracruz</option> <option value="Yucatan" >Yucat&aacute;n</option>
    										<option value="Zacatecas" >Zacatecas</option>
  										</select>
									</th>
									<th><label style="margin: 2px; font-size: 20px" for="cp">CP:</label></th>
									<th style="font-size: 18px; background-color: white"><input type="text" name="cp" id="cp" value=""></th>
								</tr>
    							
    							
								</tbody>
							<tfoot style="background-color:#DBE5F1">
								<th colspan="3"></th>
								<th ><center>
									<?php if($usuario=="Jesus Benítez" or $usuario=="Diana Hernández" or $usuario=="Anita Espino" or $usuario=="Eduardo Lizárraga"){ ?>
									<input style="font-size: 18px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
									<?php }?>
									<input style="font-size: 18px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
								</center></th>
							</tfoot>
    				</table>
        </div>	
	 </div> 	
 	</div> 	
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/proveedores/tabla',  
		filters:['cmbSindatos'],
        //active:['Edo','Sinaloa'],  
        multipleFilter:true,
        //sort:false,
        onRowClick:function(){
            $("#accordion").accordion( "activate",1 );
           	$("#id").val($(this).data('numero'));
			$("#nombre").val($(this).data('razon'));
			$("#dom").val($(this).data('dom'));
			$("#loc").val($(this).data('loc'));
			$("#edo").val($(this).data('edo'));
			$("#rfc").val($(this).data('rfc'));
			$("#cp").val($(this).data('cp'));
			$("#siglas").val($(this).data('siglas'));
			$("#nombre").focus();			
        }   
});
$("#nuevo").click( function(){	
	$("#id").val('');
	$("#nombre").val('');
	$("#dom").val('');
	$("#loc").val('');
	$("#edo").val('');
	$("#rfc").val('');
	$("#cp").val('');
	$("#siglas").val('');
	$("#nombre").focus();
 return true;
}
)
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#aceptar").click( function(){	
	nombre=$("#nombre").val();
	cp=$("#cp").val();
	if(cp==""){ cp=0; }
	numero=$("#id").val();
	if(nombre!=''){
		if( is_int(cp)){
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/proveedores/actualizar", 
						data: "id="+$("#id").val()+"&nombre="+$("#nombre").val()+"&dom="+$("#dom").val()+"&loc="+$("#loc").val()+"&edo="+$("#edo").val()+"&rfc="+$("#rfc").val()+"&cp="+$("#cp").val()+"&siglas="+$("#siglas").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytabla").trigger("update");
										$("#accordion").accordion( "activate",0 );	
										$("#nuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/proveedores/agregar", 
						data: "&nombre="+$("#nombre").val()+"&dom="+$("#dom").val()+"&loc="+$("#loc").val()+"&edo="+$("#edo").val()+"&rfc="+$("#rfc").val()+"&cp="+$("#cp").val()+"&siglas="+$("#siglas").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Proveedor registrado correctamente");
										$("#mytabla").trigger("update");
										$("#accordion").accordion( "activate",0 );		
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Codigo Postal no valido");	
			$("#cp").focus();
				return false;
		}
	}else{
		alert("Error: Nombre de Proveedor no valido");
		$("#nombre").focus();
			return false;
	}
	 // return false;
});

</script>