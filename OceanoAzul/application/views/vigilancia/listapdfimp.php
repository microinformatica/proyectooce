    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reporte de Movilizaciones</title>
<style>
	@page { margin: 70px 50px 20px; }	
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; left: 0px; bottom: -120px; right: 0px; height: 150px;  } 
	/*#footer .page:after { content: counter(page, upper-roman);*/ 
</style> 
</head>
<body>
	<div id="header"> 
		<table style="width: 770px; margin-left: -20px">
			<tr>
				<td width="500px">
				<p style="font-size: 12px" >Secretaría de Agricultura, Ganadería, Desarrollo Rural, Pesca y Alimentación <br />
				Comisión Nacional de Acuacultura y Pesca <br />Dirección General de Ordenamiento Pesquero y Acuícola<br />
				Informe de Movilizaciones - <strong>Certificado DGOPA/CSAM/<?= $cer;?></strong>
				</p>
				</td>	
				<td>
					<p style="font-size: 12px" >
					Empresa: <strong>Aquapacific S.A. de C.V.</strong>   <br />
					Producto: <strong> Postlarvas <br /></strong>
					Vigencia: <strong> <?= $fi;?></strong> a <strong> <?= $ff;?><br /></strong>
					Autorizados: <strong><?= $aut;?></strong>   Entregados: <strong><?= $ent;?></strong> 
					</p>
				</td>			
			</tr>
		</table>
		
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-left: -20px;">
			<tr>
				<td width="500px">
					<p style="font-size: 10px" >www.aquapacific.com.mx
						<!--<p class="page" style="font-size: 10px">Pág </p>-->
					</p>
				</td>	
				<td>
					<p style="font-size: 10px" >
						Av. Emilio Barragán No.63-103 <br />
						Colonia Lázaro Cárdenas, Mazatlán, Sinaloa, México <br />
						C.P. 82040 - Tel: (669)985-64-46 
					</p>
				</td>		
			</tr>
		</table> 
	
	</div> 
	 
<div style="margin-left: -25px">
	<table style="width: 770px" border="1" style="font-size: 9px" >
    	  <?= $tablac?>
	</table>	
</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(0,0,0);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pagina {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>