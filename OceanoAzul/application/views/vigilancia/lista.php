<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#asig" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/casetasvig.jpg" width="25" height="25" border="0"> Bitacora </a></li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px"  >

		<div id="asig" class="tab_content" style="margin-top: -5px" >
			<div class="ajaxSorterDiv" id="tabla_bit" name="tabla_bit" align="left" style="margin-top: -5px"  > 
				<span class="ajaxTable" style=" height: 375px; width: 917px; background-color: white;  ">
                    <table id="mytablaBit" name="mytablaBit" class="ajaxSorter" style=" height: 372px;" >
                        <thead style="font-size: 10px">                         	
                           <!-- <th data-order = "lab" >Lab</th>-->
                            <th data-order = "FechaD" style="width: 70px" >Fecha</th>                            
                            <th data-order = "vigtur" style="width: 70px" >Vigilante</th>
                            <th data-order = "Tiempo" style="width: 50px">Hora</th>
                            <th data-order = "desc" >Descripción</th>
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                 </span>
                 <div class="ajaxpager" >        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/vigilancia/pdfrepa" >
                    	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                    </form>   
 				</div>
 			</div>
 					<table border="2px" style="width: 870px; margin-left: -30px; margin-top: -60px; ">
 						<tbody style="background-color: #F7F7F7">
 							<tr>
 								<th colspan="2" style="text-align: left; width: 180px">
 									Granja
 									<select name="lab" id="lab" style="font-size: 10px;height: 25px;  margin-top: 1px; margin-left:-2px; ">
      									<option value="1">Ahome</option>
			      				 	</select>
 									Fecha
 									<input size="8%" type="text" name="dia" id="dia" class="fecha redondo mUp" style="text-align: center;margin-left:-5px;" >
 									<select id="vigtur" style="font-size: 9px;height: 25px;  margin-top: 1px; ">
          								<option disabled selected value=''>Vigilante</option>
          								<?php	
          								$this->load->model('vigilancia_model');
          								$data['result']=$this->vigilancia_model->getVigilantes();
										foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->nombre;?>" ><?php echo $row->nombre;?></option>
           								<?php endforeach;?>
   									</select>
 									<select name="hora" id="hora" style="font-size: 10px;height: 25px;  margin-top: 1px; ">
      									 <option disabled selected value=''>Hora</option>
      									<?php $hr=0; 
										while($hr <= 24){?>
											<option value="<?php echo $hr;?>" > <?php echo $hr;?> </option>
											<?php  $hr+=1; } ?>
      				 		 		</select>
      				 		 		:
      				 		 		<select  name="mins" id="mins" style="font-size: 10px; height: 25px;margin-top: 1px;" >
	 									<option disabled selected value=''>Min</option>
                    					<?php $min=0; 
										while($min <= 60){?>
											<option value="<?php echo $min;?>" > <?php echo $min;?> </option>
											<?php  $min+=1; } ?>
          							</select>
          							<select name="ap" id="ap" style="font-size: 10px;height: 25px;  margin-top: 1px; margin-left:-2px; ">
      									<option disabled selected value=''>am/pm</option>
      									<option value="a.m."  >a.m.</option>
      									<option value="p.m."  >p.m.</option>
			      				 	</select>
			      				 	
    								<select name="per" id="per" style="font-size: 10px;height: 25px;  margin-top: 1px; margin-left:-2px; ">
      									<option disabled selected value=''>Sel</option>
      									<option value="Prod" >Produccion</option>
      									<option value="Admon" >Administracion</option>
      									<option value="Compras" >Compras</option>
      									<!--<option value="Ventas" >Ventas</option>-->
      									<option value="Antonio" >Antonio H</option>
      									<option value="Personal" >Cam Personal</option>
			      				 	</select>
			      				 	<input style="font-size: 14px" type="submit" id="aceptarb" name="aceptarb" value="Guardar" />
			      				 	<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
			      				 	<input style="font-size: 14px;" type="submit" id="borrarb" name="borrarb" value="Borrar"  />
 									<input type="hidden" name="nrb" id="nrb" >
 								</th>
 							</tr>
 							<tr style="background-color: white;">
								<th style="text-align: center">
	 								Descripción
	 							</th>
	 												
								<th style="text-align: center">
									<textarea  name="desc" id="desc"  cols="150" style="resize: none; text-align: justify" rows="2" ></textarea>
								</th>
 							</tr>
 						</tbody>
 						</table>
 						</div>
 		
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#per").change( function(){	
	per=$("#per").val();
	if(per=='Prod') $("#desc").val($("#desc").val()+' Antonio Valdez en veh Toyota Tacoma 2012, Higueras.');
	if(per=='Admon') $("#desc").val($("#desc").val()+' Ezequiel Rodríguez en veh Ford Duty F-250 2009, Higueras.');
	if(per=='Compras') $("#desc").val($("#desc").val()+' Jesus Berrelleza en veh Nissan Frontier NP300 2019, Higueras. ');
	//if(per=='Ventas') $("#desc").val($("#desc").val()+' Leonardo Sainz en veh Ford #3, Aguaverde. ');
	if(per=='Antonio') $("#desc").val($("#desc").val()+' Antonio Hernández en veh Toyota Hilux, Mazatlán. ');
	if(per=='Personal') $("#desc").val($("#desc").val()+' Magdiel Torres en veh Blue Bird 2006, con personal de turno, Higueras. ');
	$("#per").val('');
});


$("#hora").change( function(){	
	hora=$("#hora").val();
	if(hora<12){
		$("#ap").val('a.m.');
	}else{
		$("#ap").val('p.m.');
	}
	$("#mins").focus();
});
$("#mins").change( function(){	
	$("#desc").focus();
});



$("#borrarb").click( function(){	
	numero=$("#nrb").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/vigilancia/borrarc", 
						data: "id="+$("#nrb").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Registro Eliminado");
										$("#mytablaBit").trigger("update");
										$("#nuevo").click();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Registro para poder Eliminarlo");
		return false;
	}
});


$("#nuevo").click( function(){	
	$("#nrb").val('');$("#desc").val('');$("#hora").focus();
 return true;
});

$("#aceptarb").click( function(){
	$("#aceptarb").attr("disabled","disabled");	
	lab=$("#lab").val();
	fec=$("#dia").val();
	numero=$("#nrb").val();
	if(lab!=''){
		if( fec!=''){
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/vigilancia/actualizar", 
						data: "id="+$("#nrb").val()+"&fec="+$("#dia").val()+"&hr="+$("#hora").val()+"&min="+$("#mins").val()+"&des="+$("#desc").val()+"&vig="+$("#vigtur").val()+"&lab="+$("#lab").val()+"&ap="+$("#ap").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaBit").trigger("update");
										$("#nuevo").click();
										
										$("#aceptarb").removeAttr("disabled");																
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/vigilancia/agregar", 
						data: "&fec="+$("#dia").val()+"&hr="+$("#hora").val()+"&min="+$("#mins").val()+"&des="+$("#desc").val()+"&vig="+$("#vigtur").val()+"&lab="+$("#lab").val()+"&ap="+$("#ap").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#mytablaBit").trigger("update");
										$("#nuevo").click();
										$("#aceptarb").removeAttr("disabled");								
																					
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Fecha no válido");	
			$("#dia").focus();
			$("#aceptarb").removeAttr("disabled");
				return false;
		}
	}else{
		alert("Error: Seleccione Laboratorio");
		$("#lab").focus();
		$("#aceptarb").removeAttr("disabled");
			return false;
	}
});

$("#dia").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  



$(document).ready(function(){ 
	$("#tabla_bit").ajaxSorter({
		url:'<?php echo base_url()?>index.php/vigilancia/tablabit',  
		filters:['lab','dia'],	
		multipleFilter:true,
		onLoad:false,	
    	sort:false,              
    	onRowClick:function(){
    		if($(this).data('nrb')>'0'){
    			$("#nrb").val($(this).data('nrb'));
   				$("#lab").val($(this).data('lab')); 
   				$("#dia").val($(this).data('dia'));
   				$("#hora").val($(this).data('hora'));
   				$("#mins").val($(this).data('mins'));
   				$("#ap").val($(this).data('ap'));
   				$("#desc").val($(this).data('desc'));
    		}
    	}, 
	});
	
});  	
</script>
