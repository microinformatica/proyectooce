<?php $this->load->view('header_cv'); ?>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/cliente.png" width="25" height="25" border="0"> Clientes - Busqueda</li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal">
			<div id="clientes" class="tab_content" style="overflow-x:hidden;" >
				<div style="height:420px; margin-top: -15px">
				<form method="post" action="<?= base_url()?>index.php/clientes/buscar"  >		
		<table id="myTablaE" class="tablesorter" border="2px" width="44%">
			<thead>
				<tr><th colspan="3">CLIENTES REGISTRADOS</th></tr>				
			</thead>
			<tbody>
				<tr>
					<th width="5%">Zona: <input type="text" name="zon" id="zon" value="<?= $zon?>" /></th>
					<th width="35%">Razon Social: <input type="text" name="busqueda" id="busqueda" size="50%" value="<?= $busqueda?>" /></th>
					<th width="4%" ><button id="btn_buscar">Buscar</button>
						<a style="float:left;" href="<?= base_url()?>index.php/clientes/buscar">
		    				<li style="float:left;margin-left:5px" class="ui-state-default ui-corner-all" title="Buscar"><span class="ui-icon ui-icon-search" style=""></span></li>
						</a>
					</th>								
				</tr>				
			</tbody>			
		</table>
	</form>	
	<?php
		if($result>0){ ?>		
		<table id="myTabla" class="tablesorter" width="40%" border="2px" style="margin-top: -15px">
			<thead>				
				<tr>
					<th width="5%">Zona</th>
					<th width="35%">Razon Social</th>					
				</tr>
			</thead>			
			<tbody>
				<?php 
				$zoni=""; 
				foreach ($result as $row):?>
				<tr>
					<?php
					 	if($zoni!=$row->Zona){ $zoni=$row->Zona;?>
					<td width="5%"><strong><?php echo $row->Zona; ?></strong></td>
					<?php }else{ ?>
					<td width="5%" style="color: grey" align="right"><?php echo $row->Zona; ?></td>	
					<?php } ?>			
					<td width="35%"><a href="<?=base_url()?>index.php/clientes/actualizar/<?=$row->Numero?>"><?php echo $row->Razon;?></a></td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
						
	<?php	}else{ ?>
		<table id="myTable" class="tablesorter" width="40%" border="5px" >
			<thead>
				<tr>
					<th width="40%" colspan="3" style="color: red">ERROR: LO SENTIMOS PERO NO HAY RESULTADOS PARA SU BUSQUEDA...</th>					
				</tr>
			</thead>			
		</table>			
	<?php }?>	
	
			</div>
		</div>
	</div>	
</div>
<div  id="pager" class="pager"  >
	<form>
		<img style="margin-top: 0px" src="<?php echo base_url()?>assets/addons/pager/icons/first.png" class="first"/>
		<img src="<?php echo base_url()?>assets/addons/pager/icons/prev.png" class="prev"/>
		<input type="text" class="pagedisplay" size="3px"/>
		<img src="<?php echo base_url()?>assets/addons/pager/icons/next.png" class="next"/>
		<img src="<?php echo base_url()?>assets/addons/pager/icons/last.png" class="last"/>
		<select style="margin-top: 1px" class="pagesize">
			<option selected="selected" value="10">10</option>
			<option value="17">17</option>													
		</select>
		<a style="color: white" href="<?=base_url()?>index.php/clientes/agregar">AGREGAR CLIENTE</a>
	</form>			
</div>		
</div>
<?php $this->load->view('footer_cv'); ?>
<script type="text/javascript">
		$(document).ready(function(){ 		
			$("#myTabla").tablesorter ({headers:{2: {sorter: false}}, widthFixed: true, widgets: ['zebra']}).tablesorterPager ({container: $("#pager")});			
		}); 
</script>