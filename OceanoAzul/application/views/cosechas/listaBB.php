<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts-more.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/export-data.js"></script>



<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
	
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}

#export_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datag { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datagz { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#containers {
    	max-width: 900px;
    	min-width: 380px;
    	height: 380px;
    	margin: 1em auto;
	}
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#cosechas" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/cosecha.jpg" width="20" height="20" border="0"> Cosechas </a></li>
		<li><a href="#concentrado" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/camarones.png" width="20" height="20" border="0"> General </a></li>
		<?php if($usuario=="Antonio Hernandez"){ ?>  
		<li><a href="#costos" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/camarones.png" width="20" height="20" border="0"> Produccion </a></li>
		<?php }?>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 630px" >
		<div id="cosechas" class="tab_content" style="margin-top: -15px" >
 			<div class="ajaxSorterDiv" id="tabla_cosechas" name="tabla_cosechas" style="margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 860px; text-align: left; height: 50px" >
            		<table  border="0" >
 						<tr>
 							<th style="text-align: center"> Ciclo </th>
 							<th style="text-align: center"> Fecha </th>
 							<th style="text-align: center"> Tipo de Salida </th>
 							<th style="text-align: center"> Cosecha </th>
 							<th style="text-align: center"> Destino - Cliente
 								<select name="secc" id="secc" style="font-size:0px; height: 0px;margin-top: 1px;visibility: hidden " >								
    								<option value="0">Sel.</option>
    							</select>
 								 </th>
 							<th style="text-align: center"> Estanque </th>
 							<th style="text-align: center"> Gramos
 								<input type="hidden" type="text" name="prebase" id="prebase" > </th>
 							<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez"){ ?>
 							<th style="text-align: center"> Precio Base 
 								
 								</th>
 							<?php }?>
 							<th style="text-align: center"> Kilogramos </th>
 							<th ><input style="font-size: 14px" type="submit" id="nue" name="nue" value="Nuevo" title="NUEVO" /></th>	
 						</tr>
 						<tr>
 							<td>
 								<select name="cicloG" id="cicloG" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   									<option value="19" >2019</option>
   									<?php if($usuario=="Antonio Hernandez" || $usuario=="Zuleima Benitez"){ ?>
   									<option value="18" >2018</option>
   									<?php }?>
   								</select>
 							</td>
 							<td><input size="10%" type="text" name="feccos" id="feccos" class="fecha redondo mUp" readonly="true" style="text-align: center;font-size: 12px;" ></td>
 							<td>
 								<select name="tipcos" id="tipcos" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   									<option value="0" >Seleccione</option>
   									<option value="1" >Maquila</option><option value="2" >Venta</option><option value="3" >Donación</option>
   								</select>
 							</td>
 							<td>
 								<select name="numcos" id="numcos" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   									<option value="0" >Seleccione</option>
   									<option value="1" >Primera</option><option value="2" >Segunda</option><option value="3" >Tercera</option>
   									<option value="4" >Cuarta</option>
   									<option value="5" >Final</option>
   								</select>
 							</td>
 							<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px" align="middle" name="clicos" type="text" id="clicos"  size="40" value="Selectos del Mar S.A. de C.V."></td>
 							<td>
 								<select name="estcos" id="estcos" style="font-size: 10px;height: 23px;margin-top: 1px;"  ></select>
    						</td>
 							<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px; text-align: center" align="middle" name="grscos" type="text" id="grscos"  size="6" /></td>
 							<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez" || $usuario=="Diana Peraza" ){ ?>
 							<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px; text-align: center" align="middle" name="prebas" type="text" id="prebas"  size="6" /></td>
 							<?php }?>
 							<td><input onKeyPress="return tabular(event,this)" onFocus="style.backgroundColor='black'; style.color='white'" onBlur="style.backgroundColor='white'; style.color='black'" style="font-size:12px; text-align: center" align="middle" name="kgscos" type="text" id="kgscos"  size="8" /></td>
 							<td>
 								<?php if($usuario!="Antonio Valdez" ){ ?>
			   					<input style="font-size: 14px" type="submit" id="agregas" name="agregas" value="+" title="AGREGAR y ACTUALIZAR DETALLE" />
								<input style="font-size: 14px" type="submit" id="quitas" name="quitas" value="-" title="QUITAR DETALLE" />
								<?php }?>
								<input type="hidden"  size="3%" type="text" name="idcos" id="idcos" style="text-align: center;">
							</td>
						</tr>
 					</table>		
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 520px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablacosechas" name="mytablacosechas" class="ajaxSorter" border="1" style="text-align: center" >
                       	<thead >
                       		
                       		<tr style="font-size: 14px">
                       			<th data-order = "feccos1">Fecha</th>
                       			<th data-order = "tipcos1">Estatus</th>
                       			<th data-order = "numcos1">Cosecha</th>	
                       			<th data-order = "clicos1">Destino - Cliente</th>
                       			<th data-order = "pisg">Estanque</th>	
                       			<th data-order = "grscos">Gramos</th>
                       			<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez" || $usuario=="Diana Peraza" ){ ?>
                       			<th data-order = "prebas1">Precio Base</th>
                       			<th data-order = "pregra">P.B + gr.</th>
                       			<?php }?>
                       			<th data-order = "kgscos">Kilogramos</th>   
                       			<?php if($usuario=="Efrain Lizárraga" || $usuario=="Zuleima Benitez" || $usuario=="Diana Peraza" ){ ?>                    			
                       			<th data-order = "imp">Importe M.N.</th>
                       			<?php }?>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 14px; text-align: center">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<button id="export_data" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
				<form id="data_tables" action="<?= base_url()?>index.php/cosechas/pdfrepcos" method="POST">
					<input type="hidden" name="tabla" id="html_bor"/>
					<input type="hidden" type="text" name="dia" id="dia" >
					<input type="hidden" type="text" name="tip" id="tip" >					
				</form>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#export_data').click(function(){									
								$('#html_bor').val($('#mytablacosechas').html());
								$('#data_tables').submit();
								$('#html_bor').val('');
								
						});
					});
				</script>
 			
 		</div>	 
 		<div id="concentrado" class="tab_content" style="margin-top: -15px" >
 			<div class="ajaxSorterDiv" id="tabla_gral" name="tabla_gral" style="margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: -5px;" > 
            		<ul class="order_list" style="width: 860px; text-align: left; " >
            			Reporte General - Sección	
            			<select name="seccg" id="seccg" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    						<option value="0">Todas</option>
    						<option value="41">20-30</option>		
    						<option value="42">21-25</option>
    						<option value="43">Doble Ciclo</option>
    					</select>
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 570px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablagral" name="mytablagral" class="ajaxSorter" border="1" style="text-align: center" >
                       	<thead >
                       		
                       		<tr style="font-size: 12px">
                       			<th rowspan="2" data-order = "pisg">Est</th>
                       			<th rowspan="2" data-order = "orgg">Orgs</th>
                       			<th rowspan="2" data-order = "hasg">Has</th>
                       			<th rowspan="2" data-order = "dc">DC</th>
                       			<th rowspan="2" data-order = "ali">Alimento</th>	
                       			<th colspan="2">Primera</th>
                       			<th colspan="2">Segunda</th>	
                       			<th colspan="2">Tercera</th>
                       			<th colspan="2">Cuarta</th>
                       			<th colspan="2">Final</th>                       			
                       			<th colspan="6">Total</th>
                       		</tr>
                       		<tr style="font-size: 12px">
                       			<th data-order = "kgs1">Kgs</th>
                       			<th data-order = "pp1">PP</th>
                       			<th data-order = "kgs2">Kgs</th>
                       			<th data-order = "pp2">PP</th>
                       			<th data-order = "kgs3">Kgs</th>
                       			<th data-order = "pp3">PP</th>
                       			<th data-order = "kgs4">Kgs</th>
                       			<th data-order = "pp4">PP</th>
                       			<th data-order = "kgsf">Kgs</th>
                       			<th data-order = "ppf">PP</th>
                       			<th data-order = "kgst">Kgs</th>
                       			<th data-order = "ppt">PP</th>
                       			<th data-order = "kgsha">Kgs/Ha</th>
                       			<th data-order = "fcat">FCA</th>
                       			<th data-order = "orgst">Orgs</th>
                       			<th data-order = "sob">% Sob</th>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 12px; text-align: center">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>
 			<button id="export_datag" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
				<form id="data_tablesg" action="<?= base_url()?>index.php/cosechas/pdfrepcosg" method="POST">
					<input type="hidden" name="tablag" id="html_gral"/>
				</form>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#export_datag').click(function(){									
								$('#html_gral').val($('#mytablagral').html());
								$('#data_tablesg').submit();
								$('#html_gral').val('');
								
						});
					});
				</script>
 			
 		</div>
 		
 		<?php if($usuario=="Antonio Hernandez"){ ?> 	  
 		<div id="costos" class="tab_content" style="margin-top: -15px" >
 			<!--<div class="ajaxSorterDiv" id="tabla_gralz" name="tabla_gralz" style="margin-top: 1px"></div>-->   
 			<div class="ajaxSorterDiv" id="tabla_gralz" name="tabla_gralz" style=" margin-left: -10px;  margin-top: 1px;width:955px;">
             	  <div class="ajaxpager" style="margin-top: -5px;" > 
             	  	<form action="<?= base_url()?>index.php/bordo/reportet" method="POST" >
            		<ul class="order_list" style="width: 860px; text-align: left; height: 25px" >
            			Reporte General - 
            			Granja
            			<select name="numgrabgz" id="numgrabgz" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   							<option value="0" >Seleccione</option>
   							<option value="2" >OA-Kino</option><option value="4" >OA-Ahome</option><option value="5" >Oce. Azul</option>
   						</select>
            			Sección	
            			<select name="seccgz" id="seccgz" style="font-size: 12px; height: 25px;margin-top: 1px;  " >	</select>
            			<img style="cursor: pointer" style="margin-top: -5px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            <input type="hidden" name="tabla" value ="" class="htmlTable"/>
            		</ul>
            	</form>	
 				</div>  
             	<span class="ajaxTable" style="height: 570px; background-color: white; width:950px; margin-top: 1px" >
            	   	<table id="mytablagralz" name="mytablagralz" class="ajaxSorter" border="1" style="text-align: center" >
                       <thead >
                       		
                       		<tr style="font-size: 10px">
                       			<th rowspan="2" data-order = "pisg">Est</th>
                       			<th rowspan="2" data-order = "orgg">Orgs</th>
                       			<th rowspan="2" data-order = "hasg">Has</th>
                       			<th rowspan="2" data-order = "dc">DC</th>
                       			<!--<th rowspan="2" data-order = "ali">Alimento</th>-->	
                       			<th colspan="4">Primera</th>
                       			<th colspan="4">Segunda</th>	
                       			<th colspan="4">Tercera</th>
                       			<th colspan="4">Cuarta</th>
                       			<th colspan="4">Final</th>                       			
                       			<th colspan="5">Total</th>
                       		</tr>
                       		<tr style="font-size: 10px">
                       			<th data-order = "pp1">PP</th>
                       			<th data-order = "kgs1">Kgs</th>
                       			<th data-order = "pre1">$</th>
                       			<th data-order = "vta1">Importe</th>
                       			<th data-order = "pp2">PP</th>
                       			<th data-order = "kgs2">Kgs</th>
                       			<th data-order = "pre2">$</th>
                       			<th data-order = "vta2">Importe</th>
                       			<th data-order = "pp3">PP</th>
                       			<th data-order = "kgs3">Kgs</th>
                       			<th data-order = "pre3">$</th>
                       			<th data-order = "vta3">Importe</th>
                       			<th data-order = "pp4">PP</th>
                       			<th data-order = "kgs4">Kgs</th>
                       			<th data-order = "pre4">$</th>
                       			<th data-order = "vta4">Importe</th>
                       			<th data-order = "ppf">PP</th>
                       			<th data-order = "kgsf">Kgs</th>
                       			<th data-order = "pref">$</th>
                       			<th data-order = "vtaf">Importe</th>
                       			<th data-order = "kgst">Kgs</th>
                       			<th data-order = "ppt">PP</th>
                       			<!--
                       			<th data-order = "kgsha">Kgs/Ha</th>
                       			<th data-order = "fcat">FCA</th>-->
                       			<th data-order = "orgst">Orgs</th>
                       			<th data-order = "sob">% Sob</th>
                       			<th data-order = "vta">Venta</th>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 10px; text-align: center">
                       	</tbody>                        	     	
                   	</table>
                </span> 
 			</div>
 			<button id="export_datagz" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
				<form id="data_tablesgz" action="<?= base_url()?>index.php/bordo/pdfrepcosg" method="POST">
					<input type="hidden" name="tablag" id="html_gralz"/> <input type="hidden" name="gra1" id="gra1"/>
				</form>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#export_datagz').click(function(){									
								$('#html_gralz').val($('#mytablagralz').html());
								$('#data_tablesgz').submit();
								$('#html_gralz').val('');
								
						});
					});
				</script>

 		</div>
 		<?php } ?>
 	</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#nue").click( function(){	
	$("#estcos").val('');$("#grscos").val('');$("#kgscos").val('');$("#prebas").val('');
	$("#estcos").val('');
 return true;
});

$("#quitas").click( function(){	
	numero=$("#idcos").val();;
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/cosechas/quitarCos", 
						data: "id="+$("#idcos").val()+"&cic="+$("#cicloG").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablacosechas").trigger("update");$("#mytablagral").trigger("update");
										$("#nue").click();$("#tipcos").val('');$("#numcos").val('');$("#clicos").val('Selectos del Mar S.A. de C.V.');$("#prebas").val('');
										$("#idcos").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar día de Muestra para poder Quitarlo");
		return false;
	}
});


$("#agregas").click( function(){	
	numero=$("#idcos").val();fecha=$("#feccos").val();tip=$("#tipcos").val();num=$("#numcos").val();cli=$("#clicos").val();est=$("#estcos").val();
	grs=$("#grscos").val();kgs=$("#kgscos").val();
	if(fecha!=''){
	if(tip!='0'){
	if(num!='0'){
	if(cli!=''){
	if(est!='0'){
	if(grs!=''){
	if(kgs!=''){				
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/cosechas/actualizaCos", 
						data: "id="+$("#idcos").val()+"&fec="+$("#feccos").val()+"&tip="+$("#tipcos").val()+"&num="+$("#numcos").val()+"&cli="+$("#clicos").val()+"&est="+$("#estcos").val()+"&grs="+$("#grscos").val()+"&kgs="+$("#kgscos").val()+"&pre="+$("#prebase").val()+"&cic="+$("#cicloG").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablacosechas").trigger("update");$("#mytablagral").trigger("update");
										$("#nue").click();$("#idcos").val('');$("#tipcos").val('');$("#numcos").val('');$("#clicos").val('Selectos del Mar S.A. de C.V.');$("#prebas").val('');										
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/cosechas/agregaCos", 
						data: "&fec="+$("#feccos").val()+"&tip="+$("#tipcos").val()+"&num="+$("#numcos").val()+"&cli="+$("#clicos").val()+"&est="+$("#estcos").val()+"&grs="+$("#grscos").val()+"&kgs="+$("#kgscos").val()+"&pre="+$("#prebase").val()+"&cic="+$("#cicloG").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablacosechas").trigger("update");$("#mytablagral").trigger("update");
										$("#nue").click();
										$("#idcos").val('');$("#estcos").focus();										
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});		
		}
	}else{
		alert("Error: Necesita Registrar Kilogramos");
		$("#kgscos").focus();
		return false;
	}	
	}else{
		alert("Error: Necesita Registrar Gramos");
		$("#grscos").focus();
		return false;
	}	
	}else{
		alert("Error: Necesita seleccionar Estanque");
		$("#estcos").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Registrar Destio y/o Cliente");
		$("#clicos").focus();
		return false;
	}	
	}else{
		alert("Error: Necesita seleccionar Cosecha");
		$("#numcos").focus();
		return false;
	}
	}else{
		alert("Error: Necesita seleccionar Tipo de Salida");
		$("#tipcos").focus();
		return false;
	}
	}else{
		alert("Error: Necesita seleccionar Fecha");
		$("#feccos").focus();
		return false;
	}		
});

$("#fecdia").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",		
});

$("#feccos").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",	
	onSelect: function( selectedDate ){
		$("#mytablacosechas").trigger("update");
		$("#nue").click();$("#idcos").val('');$("#tipcos").val('');$("#numcos").val('');$("#clicos").val('Selectos del Mar S.A. de C.V.');
		$("#dia").val($("#feccos").val());
	}
});


jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

$("#tipcos").change( function(){
	$("#tip").val($("#tipcos").val());
});	

$("#numgrabgz").change( function(){
	$("#gra1").val($("#numgrabgz").val());
});	

$(document).ready(function(){ 
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	$("#feccos").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#dia").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#tip").val(0);$("#prebase").val(0);
	$("#tabla_cosechas").ajaxSorter({
		url:'<?php echo base_url()?>index.php/cosechas/tablacosechas', 
		filters:['cicloG','feccos','tipcos'],
		sort:false,	
		onRowClick:function(){
		if($(this).data('idcos')>0){
			$("#idcos").val($(this).data('idcos'));
			$("#feccos").val($(this).data('feccos'));
       		$("#clicos").val($(this).data('clicos'));
       		$("#estcos").val($(this).data('estcos'));
       		$("#grscos").val($(this).data('grscos'));
       		$("#kgscos").val($(this).data('kgscos'));       		
       		$("#tipcos").val($(this).data('tipcos'));
       		$("#prebas").val($(this).data('prebas'));
       		$("#numcos").val($(this).data('numcos'));
       	}
   		},   
		onSuccess:function(){ 
			$('#tabla_cosechas tbody tr').map(function(){	    		
	    		if($(this).data('feccos1')=='Total'){
	    			$(this).css('background','lightblue');	 $(this).css('font-weight','bold');   				    				    			    			
	    		}    	
	    	}); 
		},	
	});
	$("#tabla_gral").ajaxSorter({
		url:'<?php echo base_url()?>index.php/cosechas/tablagral', 
		filters:['cicloG','seccg'],
		sort:false,	
		onSuccess:function(){ 
			$('#tabla_gral tbody tr').map(function(){
		    	if($(this).data('pisg')=='Total') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    })		
		},	
	});
	 $('#estcos').boxLoader({
            url:'<?php echo base_url()?>index.php/cosechas/combob/'+$("#cicloG").val(),
            equal:{id:'secc',value:'#secc'},
            select:{id:'idpis',val:'val'},
            all:"Sel.",
            
    });
    
    $("#tabla_gralz").ajaxSorter({
		url:'<?php echo base_url()?>index.php/bordo/tablagral', 
		filters:['cicloG','numgrabgz','seccgz'],
		sort:false,	
		onSuccess:function(){ 
			$('#tabla_gral tbody tr').map(function(){
		    	if($(this).data('pisg')=='Total') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    })		
		},	
	});
	
	
	
	 $('#estcosz').boxLoader({
            url:'<?php echo base_url()?>index.php/bordo/combob/'+$("#cicloG").val(),
            equal:{id:'numgra',value:'#numgrab'},
            //equal:{id:'secc',value:'#secc'},
            select:{id:'idpis',val:'val'},
            all:"Sel.",
            
    });
    $('#seccgz').boxLoader({
            url:'<?php echo base_url()?>index.php/bordo/secciones/',
            equal:{id:'numgrabg',value:'#numgrabgz'},
            select:{id:'secc',val:'val'},
            all:"Sel.",
            
    });
});
</script>