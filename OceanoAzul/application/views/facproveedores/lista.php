<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/facturas.png" width="25" height="25" border="0"> Facturas Proveedores </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px" >
		    <div class="ajaxSorterDiv" id="tabla_facpro" name="tabla_facpro" align="left"  >  
		    	<div class="ajaxpager" style="margin-top: -7px">        
                    <ul class="order_list"><input size="2%"  type="text" name="pfac" id="pfac" style="text-align: left;  border: 0; background: none">
                    	<input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="nuevo" name="nuevo" value="Agregar" />
                     </ul>Año Actual: 
                    <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; " >
                    	<?php $ciclof=2013; $actual=date("Y"); $actual1 = substr($actual, 2, 2);//$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo 'fp'.$actual1;?>" > <?php echo $actual;?> </option>
            				<?php  $actual-=1;$actual1-=1; } ?>
          			</select>
                     Mes:
    				<select name="mes" id="mes" style="font-size: 10px;margin-left:10px; margin-top: 1px;">
      								<option value="0"> Todos </option>
      								<option value="01"> Enero </option>
      								<option value="02"> Febrero </option>      									
      								<option value="03"> Marzo </option>
      								<option value="04"> Abril </option>
      								<option value="05"> Mayo </option>
      								<option value="06"> Junio </option>
      								<option value="07"> Julio </option>
      								<option value="08"> Agosto </option>
      								<option value="09"> Septiembre </option>
      								<option value="10"> Octubre </option>
      								<option value="11"> Noviembre </option>
      								<option value="12"> Diciembre </option>
    				</select>
    			Estatus:
    				<select name="est" id="est" style="font-size: 10px;margin-left:10px; ">
      								<option value="0"> Todos </option>
      								<option value="1"> Sin Movimientos </option>
    				</select> 	
    				Dia: <input size="9%" type="text" name="txtFecDia" id="txtFecDia" class="fecha redondo mUp" style="text-align: center;" >                        
                    <form method="post" style="margin-top: 8px" action="<?= base_url()?>index.php/facproveedores/reporte" >
                    	<input size="3%" type="hidden" name="requisicion" id="requisicion" /> 
                    	<img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
              
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                    </form>   
 				</div>
		    	<span class="ajaxTable" style=" height: 335px; width: 935px; background-color: white;  ">
                    <table id="mytablaFPro" name="mytablaFPro" class="ajaxSorter" border="1" >
                        <thead title="Presione las columnas para ordenarlas como requiera" >                         	
                            <th style="font-size: 10px" data-order = "bas1" >Cla</th>                            
                            <th style="font-size: 10px" data-order = "mov" >Mov</th>
                            <th style="font-size: 10px" data-order = "RFC" >RFC</th>
                            <th style="font-size: 10px" data-order = "Razon" >Razón Social</th>                                                       
                            <th style="font-size: 10px; text-align: center" data-order = "t161" >T16%</th>
                            <th style="font-size: 10px; text-align: center" data-order = "t111">T11%</th>
                            <th style="font-size: 10px; text-align: center" data-order = "t01" >T0%</th>
                            <th style="font-size: 10px" data-order = "exc1" >Excento</th>
                            <th style="font-size: 10px" data-order = "imp" >Importe</th>
                            <th style="font-size: 10px" data-order = "iva1" >I.V.A.</th>
                            <th style="font-size: 10px" data-order = "sub" >Subtotal</th>
                            <th style="font-size: 10px" data-order = "isr1" >I.S.R.</th>
                            <th style="font-size: 10px" data-order = "riva1" >I.V.A.</th>
                            <th style="font-size: 10px" data-order = "tot" >Total</th>
                            <th style="font-size: 10px" data-order = "fac" >Factura</th>
                            <th style="font-size: 10px" data-order = "pol" >Poliza</th>
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 10px">
                        </tbody>
                    </table>
                 </span>
            </div>
 			<div id='ver_mas_reqn' class='hide' >
 			<table style="width: 935px; margin-top: -4px">
 				<th>
 					<table style="width: 265px; margin-top: -5px">
 							<tbody style="background-color: #F7F7F7">
 								<tr>
 									<th  style="text-align: center">
									<div style="height: 90px" >                			              
                						<div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" style="width: 259px;height: 90px ">                
               			     				<span class="ajaxTable" style="margin-left: 0px; height: 80px; width: 257px">
                    							<table id="mytablaC" name="mytablaC" class="ajaxSorter">
                        							<thead >                            
                        								<th style="font-size: 10px" data-order = "Razon" >Proveedor</th>                                                                                    
                        							</thead>
                        							<tbody style="font-size: 9px">                                                 	  
                        							</tbody>
                    							</table>
                			 				</span> 
            							</div>
                					</div> 							
 									</th>
	 							</tr>
 							</tbody>
 						</table>
 					</th>
 				<th>
 					<table>
 						<thead style="background-color: #DBE5F1;">
 							<th colspan="4" >FACTURA
 								<input type="hidden" size="3%" name="id" id="id" />
 								<input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
 								<input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="borrar" name="borrar" value="Borrar" /> 								
 								<input style="font-size: 12px" type="submit" onclick="cerrar(2)" id="x" name="x" value="Cerrar" />
 								
 							</th>
 							<th colspan="6">	
 								Fecha: <input size="9%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
 								Clasificacion:<select name="bas" id="bas" style="font-size: 10px;margin-left:10px; margin-top: 1px;">
      									<option value="1"> (1) Bienes </option>
      									<option value="2"> (2) Arrendamiento </option>
      									<option value="3"> (3) Servicios </option>
    								</select>  
    							Mov: <input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="4%" type="text" name="mov" id="mov">	
 							</th>
 							
 						</thead>
 						<tbody style="background-color: #F7F7F7">
 							<tr>
 								<th style="text-align: center">Aplicar</th>
 								<th colspan="9" style="text-align: Left">Proveedor: <input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="pro" id="pro">
 									<input type="hidden" size="3%" name="idp" id="idp" />
 								</th>
 								
 							</tr>
 							<tr>
 								<th rowspan="2">
									<select name="mes1" id="mes1" style="font-size: 10px;margin-left:10px; margin-top: 1px;">
      									<option value="1"> Ene </option>
      									<option value="2"> Feb </option>      									
      									<option value="3"> Mar </option>
      									<option value="4"> Abr </option>
      									<option value="5"> May </option>
      									<option value="6"> Jun </option>
      									<option value="7"> Jul </option>
      									<option value="8"> Ago </option>
      									<option value="9"> Sep </option>
      									<option value="10"> Oct </option>
      									<option value="11"> Nov </option>
      									<option value="12"> Dic </option>
    								</select>
								</th>
 								<th style="text-align: center">Tasa-16%</th>
 								<th style="text-align: center">Tasa-11%</th>
 								<th style="text-align: center">Tasa-0%</th>
 								<th style="text-align: center">Excento</th>
 								<th style="text-align: center">I.V.A.</th>
 								<th style="text-align: center">I.S.R.</th>
 								<th style="text-align: center">Ret. IVA</th>
 								<th style="text-align: center">Factura</th>
 								<th style="text-align: center">Poliza</th>
 									
 							</tr>
 							<tr>
 								<th style="font-size: 10px;">$<input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="t16" id="t16" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th> 									
								<th style="font-size: 10px;">$<input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="t11" id="t11" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
								<th style="font-size: 10px;">$<input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="t0" id="t0" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
								<th style="font-size: 10px;">$<input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="exc" id="exc" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>									
								<th style="font-size: 10px;">$<input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="iva" id="iva" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
								<th style="font-size: 10px;">$<input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="isr" id="isr" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
								<th style="font-size: 10px;">$<input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="riva" id="riva" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;"></th>
								<th style="font-size: 10px;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="fac" id="fac" style="text-align: center;"></th>
								<th style="font-size: 10px;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Diana Hernández"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="pol" id="pol" style="text-align: center;"></th>
							</tr> 	
 						</tbody>
 						</table>
 					</th>
 				</table>
 			</div>
 	</div>                
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#txtFecDia").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#txtFecDia").change( function(){	
	$("#mes").val('');
	return true;
});
$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});

$("#mes").change( function(){	
	$("#txtFecDia").val('');
	return true;
});
$("#est").change( function(){	
	$(this).removeClass('used');$('#ver_mas_reqn').hide(); 
	return true;
});
function radios(radio,dato){
	if(radio==1) { $("#cans").val(dato);}
}
function cerrar($sel){
	$(this).removeClass('used');
	if($sel==1){ $('#ver_mas_req').hide(); }
	else{  $('#ver_mas_reqn').hide(); 
		$("#nuevo").click();
		$('#ver_mas_reqn').hide();
	}
}
$("#nuevo").click(function(){	
	$(this).removeClass('used');
	$('#ver_mas_reqn').show(); 
	var f = new Date();
	$("#mes1").val((f.getMonth() +1));
	$("#id").val('');$("#txtFecha").val('');$("#bas").val('');$("#mov").val('');$("#pro").val('');$("#idp").val('');$("#t16").val('');
    $("#t11").val('');$("#t0").val('');$("#exc").val('');$("#iva").val('');$("#isr").val('');$("#riva").val('');$("#fac").val('');
    $("#pol").val('');
    return true;
});


$("#aceptar").click( function(){
	id=$("#id").val();	
	fec=$("#txtFecha").val();
	pro=$("#idp").val();
	if(pro!=''){
	 if(fec!=''){
	 	if(id==''){
			$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/facproveedores/agregar", 
				data: "fec="+$("#txtFecha").val()+"&bas="+$("#bas").val()+"&mov="+$("#mov").val()+"&nrp="+$("#idp").val()+"&t16="+$("#t16").val()+"&t11="+$("#t11").val()+"&t0="+$("#t0").val()+"&exc="+$("#exc").val()+"&iva="+$("#iva").val()+"&isr="+$("#isr").val()+"&riva="+$("#riva").val()+"&fac="+$("#fac").val()+"&pol="+$("#pol").val()+"&ciclo="+$("#cmbCiclo").val()+"&aplicar="+$("#mes1").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaFPro").trigger("update");
								$("#nuevo").click();
								$(this).removeClass('used');
								$('#ver_mas_reqn').hide();
								alert("Datos Agregados correctamente");										
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
			}else{
			$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/facproveedores/actualizar", 
				data: "id="+$("#id").val()+"&fec="+$("#txtFecha").val()+"&bas="+$("#bas").val()+"&mov="+$("#mov").val()+"&nrp="+$("#idp").val()+"&t16="+$("#t16").val()+"&t11="+$("#t11").val()+"&t0="+$("#t0").val()+"&exc="+$("#exc").val()+"&iva="+$("#iva").val()+"&isr="+$("#isr").val()+"&riva="+$("#riva").val()+"&fac="+$("#fac").val()+"&pol="+$("#pol").val()+"&ciclo="+$("#cmbCiclo").val()+"&aplicar="+$("#mes1").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaFPro").trigger("update");
								$("#nuevo").click();
								$(this).removeClass('used');
								$('#ver_mas_reqn').hide();
								alert("Datos Actualizados correctamente");										
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
			}
	}else{
		alert("Error: Necesita Seleccionar Fecha");	
		$("#txtFecha").focus();			
		return false;
	}
	}else{
		alert("Error: Necesita Seleccionar Proveedor");
		$("#pro").focus();				
		return false;
	}	 
});

$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/facproveedores/borrar", 
				data: "id="+$("#id").val()+"&ciclo="+$("#cmbCiclo").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaFPro").trigger("update");
								$("#nuevo").click();
								$(this).removeClass('used');
								$('#ver_mas_reqn').hide();
								alert("Datos eliminados correctamente");										
																								
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder eliminar datos");				
		return false;
	}	 
});

$(document).ready(function(){ 
	var f = new Date();
	$("#txtFecDia").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#mes1").val((f.getMonth() +1));
	$("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facproveedores/tablaclientes',  
		multipleFilter:true, 
   		//onLoad:false,
   		sort:false,	
   		onRowClick:function(){
   			$("#pro").val($(this).data('razon'));
   			$("#idp").val($(this).data('numero'));
   		},	
   	});	 
	$(this).removeClass('used');
   	$('#ver_mas_reqn').hide();
	$("#tabla_facpro").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facproveedores/tabla',  		
		filters:['cmbCiclo','mes','est','txtFecDia'],
		paginatorOptions:{exportXls:true},
		multipleFilter:true, 
    	sort:false,
    	onRowClick:function(){
    		val=$("#est").val();
    		if(val==0 && ($(this).data('bas1')!='Total')){
    			$(this).removeClass('used');
   				$('#ver_mas_reqn').show();
   				$("#id").val($(this).data('nrf'));$("#idp").val($(this).data('nrp'));$("#txtFecha").val($(this).data('fec'));
    			$("#bas").val($(this).data('bas'));$("#mov").val($(this).data('mov'));$("#pro").val($(this).data('razon'));
    			$("#t16").val($(this).data('t16'));$("#t11").val($(this).data('t11'));$("#t0").val($(this).data('t0'));
    			$("#exc").val($(this).data('exc'));$("#iva").val($(this).data('iva'));$("#isr").val($(this).data('isr'));
    			$("#riva").val($(this).data('riva'));$("#fac").val($(this).data('fac'));$("#pol").val($(this).data('pol'));
    			$("#mes1").val($(this).data('aplicar'));
    		}else{$(this).removeClass('used'); $('#ver_mas_reqn').hide();}
       	},   
    	onSuccess:function(){
    		$('#tabla_facpro tbody tr').map(function(){
		    	if($(this).data('bas1')=='Total'){
		    		$(this).css('background','lightgreen');	$(this).css('font-size','10px');$(this).css('font-weight','bold');
		    	}
		    	$("#pfac").val($(this).data('totp'));
	    		$(this).css('text-align','right');
	    	})
	    	$('#tabla_facpro tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','left');  })	    				
	    	$('#tabla_facpro tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center');  })
	    	$('#tabla_facpro tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','left');  })
	    	$('#tabla_facpro tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','left');  })
	    	$('#tabla_facpro tbody tr td:nth-child(15)').map(function(){ $(this).css('text-align','left');  })
	    	$('#tabla_facpro tbody tr td:nth-child(16)').map(function(){ $(this).css('text-align','left');  })
	    },          
	});
	
});  	
</script>