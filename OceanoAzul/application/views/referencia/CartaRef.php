    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 70px 50px 20px; }
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: orange; }	
	/*#header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }*/ 
	/*#footer .page:after { content: counter(page, upper-roman); }*/ 
</style> 
<!--
<body> 
	<div id="header"> <h1>Widgets Express</h1> </div> 
	<div id="footer"> <p class="page">Page </p> </div> 
	<div id="content"> <p>the first page</p> 
	<p style="page-break-before: always;">the second page</p> 
	</div> 
</body> 
</html> 
--> 
<title>Referencia Bancaria</title>
</head>
<body>
	<div id="header"> 
		<table style="width: 770px; margin-left: -20px">
			<tr>
				<td width="610px">
					<p><img src="<?php echo base_url();?>assets/images/aquapacificlogotipo.jpg" width="200" height="60" border="0"></p>
				</td>	
					
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: navy;">
				<td width="160px">www.aquapacific.com.mx</td>	
				<td width="490">
					Av. Emilio Barragán No.63-103, Col. Lázaro Cárdenas, Mazatlán, Sin. México, RFC:AQU-031003-CC9, CP:82040 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
	</div>
	<?php 
	function weekday($fechas){
		    $fechas=str_replace("/","-",$fechas);
		    list($dia,$mes,$anio)=explode("-",$fechas);
		    return (((mktime ( 0, 0, 0, $mes, $dia, $anio) - mktime ( 0, 0, 0, 7, 17, 2006))/(60*60*24))+700000) % 7;
		}
		$fechas=date("m");
		if($fechas == 1 ) { $mes = "Enero"; }
		if($fechas == 2 ) { $mes = "Febrero"; }
		if($fechas == 3 ) { $mes = "Marzo"; }
		if($fechas == 4 ) { $mes = "Abril"; }
		if($fechas == 5 ) { $mes = "Mayo"; }
		if($fechas == 6 ) { $mes = "Junio"; }
		if($fechas == 7 ) { $mes = "Julio"; }
		if($fechas == 8 ) { $mes = "Agosto"; }
		if($fechas == 9 ) { $mes = "Septiembre"; }
		if($fechas == 10 ) { $mes = "Octubre"; }
		if($fechas == 11 ) { $mes = "Noviembre"; }
		if($fechas == 12 ) { $mes = "Diciembre"; }
		$diaN=weekday(date("d/m/Y"));
		if ( $diaN == 0 ) { $dia="Lunes";}
		if ( $diaN == 1 ) { $dia="Martes";}
		if ( $diaN == 2 ) { $dia="Miércoles";}
		if ( $diaN == 3 ) { $dia="Jueves";}
		if ( $diaN == 4 ) { $dia="Viernes";}
		if ( $diaN == 5 ) { $dia="Sábado";}
		if ( $diaN == 6 ) { $dia="Domingo";}
	?>
<div style="margin-left: -25px">
	<table style="font-family: Verdana; font-size: 12">
		<tr>
			<th colspan="2" style="text-align: right">Mazatlán, Sinaloa  <?= $dia.", ".date("d")." de ".$mes. " de ".date("Y")."."?><br /><br /><br /> </th>	
		</tr>
		<tr>
			<th colspan="2" style="text-align: right">Asunto: <u>Referencia Bancaria.</u> <br /><br /><br />
		</tr>
		<tr>
			<th colspan="2" style="text-align: left"><?= $cli?><br /><?= $dom?><br /><?= $lecp?></th>
		</tr>
		<tr>
			<th></th>
			<th style="width: 650px">
				<p style="text-align: justify;font-family: Verdana; font-size: 12">  	
				<br /><br />
				Con el compromiso de seguirles ofreciendo un mejor servicio, nos hemos dado a la tarea de gestionar referencias bancarias para cada uno de nuestros clientes,
				la referencia correspondiente a su empresa se la estamos notificando en este documento, para que a partir de hoy usted	
				pueda realizar su pago a través de BBVA Bancomer.
				Su depósito podrá realizarlo en las siguientes cuentas a nombre de Aquapacific S.A. de C.V.
				<table style="font-family: Verdana; font-size: 12;" >
					
					<tr><td colspan="2"></td><td style="text-align: center"></td><td style="text-align: center">REFERENCIA</td></tr>
					<tr><td align="left" rowspan="2" style="width: 150px;text-align: center">CIE</td><td style="width: 150px">Dólares Americanos</td><td style="width: 90px;text-align: center">893617</td><td align="center" rowspan="2"><?= $ref?></td></tr>
					<tr><td style="width: 150px">Moneda Nacional</td><td style="width: 90px;text-align: center">893226</td></tr>	
					<!-- Efrain
					<tr><td colspan="2"  style="width: 150px;">Depósito en Ventanilla:</td><td style="width: 150px"></td><td style="text-align: center"></td><td style="text-align: center">REFERENCIA</td></tr>
					<tr><td rowspan="2"></td><td rowspan="2" style="width: 120px; text-align: right">CUENTA</td><td style="width: 150px">Dólares Americanos</td><td style="width: 90px;text-align: center">0141693816</td><td align="center" rowspan="2"><?= $ref?></td></tr>
					<tr><td style="width: 50px">Moneda Nacional</td><td style="width: 90px;text-align: center">0141693174</td></tr>
					<tr><td colspan="2" style="width: 150px">Depósito Vía Internet: </td><td style="width: 150px"></td><td style="text-align: center"></td><td style="text-align: center">.</td></tr>	
					<tr><td rowspan="5"></td><td align="left" rowspan="2" style="width: 120px; text-align: right">CLABE</td><td style="width: 150px">Dólares Americanos</td><td style="width: 90px">012744001416938161</td><td align="center" rowspan="2"><?= $ref?></td></tr>
					<tr><td style="width: 50px">Moneda Nacional</td><td style="width: 90px">012744001416931744</td></tr>
					<tr><td colspan="2" style="width: 150px"></td><td style="text-align: center"></td><td style="text-align: center">.</td></tr>
					<tr><td align="left" rowspan="2" style="text-align: center">CIE</td><td style="width: 150px">Dólares Americanos</td><td style="width: 90px;text-align: center">893617</td><td align="center" rowspan="2"><?= $ref?></td></tr>
					<tr><td style="width: 150px">Moneda Nacional</td><td style="width: 90px;text-align: center">893226</td></tr>	-->
				</table>		
				</p>
				<p style="text-align: justify;font-family: Verdana; font-size: 12">
					<br /><br />
				La finalidad es identificar los pagos realizados a la empresa, por lo que es indispensable no dejar de registrar la referencia en el depósito efectuado. Sin otro particular por el momento y para mayor información quedo a sus órdenes. 
 				</p>
				<br /><br />
			</th>	
		</tr>
		<tr>
			<th></th>
			<th style="width: 480px">
				<p style="text-align: center">  	
				Atentamente <br />
				Efraín Lizárraga Valdez
				 <div style="font-size: 10px;"> <!--Departamento de Crédito-->
				  <br />Atención Telefónica: (669)994-7702 <br />	e-mail: efrain@aquapacific.com.mx
				</div>
				</p>
				<br />
			</th>	
		</tr>	
	</table>
</div>


<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(0,0,0);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>