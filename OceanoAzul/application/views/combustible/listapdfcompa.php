    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 80px 50px 20px; }
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: navy; }	
</style> 
<title>Reporte de Descuentos</title>
</head>
<body>
	<?php 
	function weekday($fechas){
		    $fechas=str_replace("/","-",$fechas);
		    list($dia,$mes,$anio)=explode("-",$fechas);
		    return (((mktime ( 0, 0, 0, $mes, $dia, $anio) - mktime ( 0, 0, 0, 7, 17, 2006))/(60*60*24))+700000) % 7;
		}
		$fechas=date("m");
		if($fechas == 1 ) { $mes = "Enero"; }
		if($fechas == 2 ) { $mes = "Febrero"; }
		if($fechas == 3 ) { $mes = "Marzo"; }
		if($fechas == 4 ) { $mes = "Abril"; }
		if($fechas == 5 ) { $mes = "Mayo"; }
		if($fechas == 6 ) { $mes = "Junio"; }
		if($fechas == 7 ) { $mes = "Julio"; }
		if($fechas == 8 ) { $mes = "Agosto"; }
		if($fechas == 9 ) { $mes = "Septiembre"; }
		if($fechas == 10 ) { $mes = "Octubre"; }
		if($fechas == 11 ) { $mes = "Noviembre"; }
		if($fechas == 12 ) { $mes = "Diciembre"; }
		$diaN=weekday(date("d/m/Y"));
		if ( $diaN == 0 ) { $dia="Lunes";}
		if ( $diaN == 1 ) { $dia="Martes";}
		if ( $diaN == 2 ) { $dia="Miércoles";}
		if ( $diaN == 3 ) { $dia="Jueves";}
		if ( $diaN == 4 ) { $dia="Viernes";}
		if ( $diaN == 5 ) { $dia="Sábado";}
		if ( $diaN == 6 ) { $dia="Domingo";}
	?>
	<div id="header"> 
		<table style="width: 770px;">
			<tr>
				<td width="200px">
					<p style="font-size: 8px;"  >
						<img src="<?php echo base_url();?>assets/images/aquapacificlogotipo.jpg" width="200" height="60" border="0">
						SIA - <?= $usuario?> - <?= $perfil?> <br />
					</p>
				</td>	
				<td align="center" style="font-size: 14px; color: navy"><strong>COMPARATIVO ANUAL <br /> AL DíA: <?= $dia.", ".date("d")." de ".$mes. " de ".date("Y")."."?></strong></td>			
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: white;">
				<td width="160px">www.aquapacific.com.mx</td>	
				<td width="490">
					Av. Emilio Barragán No.63-103, Col. Lázaro Cárdenas, Mazatlán, Sin. México, RFC:AQU-031003-CC9, CP:82040 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
	</div>
<div style="margin-left: -25px">
	<table border="1"  style="font-size: 8px">
    	  <?= $tablac?>
	</table>	
</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>