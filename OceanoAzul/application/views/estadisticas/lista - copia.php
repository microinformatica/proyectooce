<?php $this->load->view('header_cv');?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/Estilo.css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Funcion.js"></script>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
.menu{
		display:inline-block;
	}
	.menu li{
		margin-right:15px;
		margin-top:15px;
		padding:0;
		float:left;
	}
	.menu li a img{
		width: 72px;
		display:inline-block;
	}
	.ui-dialog-titlebar-close{
		visibility: hidden;
	}
	.menu li a{
		display:block;
		text-align:center;
	}
	.menu li a:hover{
		opacity:1;
		text-decoration:none;
	}
	.menu li a div{
		display:inline-block;
	}
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/estadisti.png" width="25" height="25" border="0"> 	Estadistica </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion">
			<h3 align="left"><a href="">Saldos Actuales</a></h3>
        <div  style="height:350px;">                                                          
            <!--Tabla-->
            <div class="ajaxSorterDiv" id="tabla_est" name="tabla_est" style="margin-top: -21px">                
                <div style=" text-align:left" class='filter' >
                	Seleccione Zona:
                    <select id="cmbZona" style="font-size: 10px; height: 23px">
						<option value="0">Todos</option>
						<option value="1">Angostura</option>
						<option value="2">Colima</option>
						<option value="3">El Dorado</option>
						<option value="4">Elota</option>
						<option value="5">Guasave</option>
						<option value="6">Hermosillo</option>
						<option value="7">Mochis</option>
						<option value="8">Navojoa</option>
						<option value="9">Navolato</option>
						<option value="10">Nayarit</option>
						<option value="11">Obregon</option>
						<option value="12">Sur Sinaloa</option>
						<option value="13">Tamaulipas</option>
						<option value="14">Yucatán</option>
					</select>  
					         
                </div>
                <span class="ajaxTable" style="height: 335px; margin-left: -20px; background-color: white">
                    <table id="mytablaE" name="mytablaE" class="ajaxSorter">
                        <thead title="Presione las columnas para ordenarlas como requiera">                            
                            <th data-order = "Razon" style="width: 300px;">Razon Social</th>                                                        							                                                                                                              
                            <th data-order = "Saldo" style="width: 70px;">Saldo</th>
                            <th data-order = "FecUD" style="width: 70px;">Fecha</th> 
                            <th data-order = "ImpUD" style="width: 70px;">Importe</th>  
                            <th data-order = "ObsUD" style="width: 300px;">Observaciones</th>                            
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                </span> 
             	<div class="ajaxpager" style="">        
                    <ul class="order_list"></ul>                                            
                    <form method="post" action="<?= base_url()?>index.php/estadistica/pdfrep/" >
                    	<label style="font-size: 10px;">Nota: lo marcado en amarillo es Saldo Incobrable</label>                    	
                    	<input type="hidden" size="8%" name="zonasel" id="zonasel" />                      	             	                                                                                           
                        <img title="Enviar datos de tabla en archivo PDF" class="exporterpdf" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 					</form>   
                </div>                                      
            </div>    
        </div>
        
        <h3 align="left"><a href="" >Actualizar</a></h3>
        	
       	<div style="height:150px; ">  
        	<table style="width: 700px" border="2px"  class="tablesorter">
							<thead >
								<th colspan="4" height="15px" style="font-size: 20px">Información de Cliente
								<input size="8%" type="hidden" name="id" id="id"/>																
								<th >
									<center>
									<?php if($usuario=="Jesus Benítez" or $usuario=="Efrain Lizárraga"){ ?>
									<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
									<?php } else {?>
									<input disabled="true" style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />	
									<?php }?>	
									</center>	
								</th>
									</thead>
								<tr>
									<th><label style="margin: 2px; font-size: 17px" for="nombre">Razón Social</label></th>
									<th colspan="5" style="font-size: 17px; background-color: white"><input style="border: 1" readonly="true" size="85%"  type="text" name="nombre" id="nombre" ></th>
								</tr>
								<tr>
									<th><label style="margin: 2px; font-size: 17px;" for="zon">Zona</label></th>
									<th align="right" colspan="3" style="text-align: right; font-size: 17px">Saldo Neto:</th>	
									<th style="font-size: 17px; background-color: white; text-align: center">
										<input disabled="true" readonly="true" size="18%" type="text" name="impsal" id="impsal" style="text-align: center; margin-left: 10px; border: 0"></th>								
								</tr>
								<tr>
									<th rowspan="4" style="font-size: 17px;" ><input readonly="true" size="10%" type="text" name="zon" id="zon" style="text-align: center; border: none; color: red;" ></th>									
									<th colspan="4"><label style="margin: 2px; font-size: 17px" for="nombre">Registro del Último Depósito</label></th>									
								</tr>
								<tr >
									<th style="font-size: 17px; text-align: center">Fecha</th>
									<th style="font-size: 17px; text-align: center">Dias</th>
									<th style="font-size: 17px; text-align: center">Importe</th>
									<th style="font-size: 17px; text-align: center">Saldo Incobrable</th>																			
      							</tr>															
								</tr>
    							<tr style="font-size: 17px; background-color: white;">
									<th style="font-size: 17px; text-align: center"><input disabled="true"readonly="true" size="11%" type="text" name="fec" id="fec" class="fecha redondo mUp" style="text-align: center;  border: 0" ></th>								
									<th style="font-size: 17px; text-align: center"><input disabled="true"readonly="true" size="4%" type="text" name="dias" id="dias" value="" style="text-align: right;  border: 0"></th>
									<th style="font-size: 17px; text-align: center"><input disabled="true"readonly="true" size="18%" type="text" name="imp" id="imp" style="text-align: center; border: 0"></th>
									<th style="text-align: center" align="center">
									<?php if($usuario=="Jesus Benítez" or $usuario=="Efrain Lizárraga"){ ?>	
									<select name="sn" id="sn" style=" background-color: yellow;  font-size: 14px;height: 25px; margin-top: 1px;">
      										<option style="color: blue" value="0" > No </option>
      										<option style="color: blue" value="-1"  > Si </option>      									
    								  </select>
    								<?php } else {?>
    								<select disabled="true" name="sn" id="sn" style=" background-color: yellow;  font-size: 14px;height: 25px; margin-top: 1px;">
      										<option style="color: blue" value="0" > No </option>
      										<option style="color: blue" value="-1"  > Si </option>      									
    								  </select>	
    								<?php } ?>	  
    								</th>																												
								</tr>
    							<tr>
									
								</tr>
								
    							<tr>
    								<th ><label style="margin: 2px; font-size: 17px" for="obs">Observaciones</label></th>
    								<th colspan="5" style="font-size: 14px;">
    								<?php if($usuario=="Jesus Benítez" or $usuario=="Efrain Lizárraga"){ ?>
    								<input size="103%" type="text" name="obs" id="obs" value="" style="text-align: left;">
									<?php } else {?>
									<input readonly="true" size="103%" type="text" name="obs" id="obs" value="" style="text-align: left;">	
									<?php } ?>	
									</th>
								</tr>
								<tr>
									
									
								</tr>
								
							<tfoot>
														</tfoot>
    				</table>    				
        </div>	
	 </div> 	
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#tabla_est").ajaxSorter({
	url:'<?php echo base_url()?>index.php/estadistica/tabla',  	
	filters:['cmbZona'],		
    active:['SI','-1'], 
    onRowClick:function(){
        $("#accordion").accordion( "activate",1 );
       	$("#id").val($(this).data('numero'));
       	$("#zon").val($(this).data('zona'));
		$("#nombre").val($(this).data('razon'));
       	$("#fec").val($(this).data('fecud'));       	
		$("#dias").val($(this).data('diferencia'));
		$("#imp").val($(this).data('impud'));
		$("#impsal").val($(this).data('saldo'));
		$("#sn").val($(this).data('si'));
		$("#obs").val($(this).data('obsud'));		
    }   
});
$("#cmbZona").change( function(){		
	$("#zonasel").val($("#cmbZona").val());	
 return true;
});


$("#aceptar").click( function(){	
	numero=$("#id").val();
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/estadistica/actualizar", 
						data: "id="+$("#id").val()+"&obs="+$("#obs").val()+"&sn="+$("#sn").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaE").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
				}else{
		alert("Error: Necesita seleccionar Cliente para poder actualizarlo");
		$("#accordion").accordion( "activate",0 );
		return false;
	}	
});


</script>