<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts/js/modules/exporting.js"></script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;		
}   
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#zona"><img src="<?php echo base_url();?>assets/images/mexicopais.png" width="30" height="20" border="0"> Zonas</a></li>
		<li><a href="#todos"><img src="<?php echo base_url();?>assets/images/menu/estadisti.png" width="25" height="25" border="0"> Estadisticas</a> </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="zona" class="tab_content" >
			<table border="0" style="margin-top: -10px" width="900px">
				<tr>
					<th>
						<div id="circular" style="min-width: 600px; height: 470px;  "></div>			
					</th>
					<th>
				<table id="myTablaZons" name="myTablaZons" class="ajaxSorter" border="3px" >
					<thead>	
						<tr>
							<th style="font-size: 11px" width="100px" colspan="2">Zona</th>	<th style="font-size: 11px" width="150px" colspan="2">Importe</th>					
						</tr>
					</thead>
					<tbody>
						<?php $b = array();$n = array(); $cont=0; $saldot1=0; foreach ($result as $row): $saldot1+=$row->saldo;	
						 endforeach;
						$cont=0; $saldot=0; foreach ($result as $row): $saldot+=$row->saldo;  ?>
						<tr>							
							<td style="font-size: 11px" width="20px"><strong><?php echo $cont+=1; ?></strong></td>
							<td style="font-size: 11px" width="80px"><strong><?php echo $row->Zona; ?></strong></td>
							<td style="font-size: 11px" width="100px" align="right"><?php echo '$ '.number_format($row->saldo, 2, '.', ',');?></td>
							<td style="font-size: 11px" width="50px" align="right"><?php echo number_format(($row->saldo/$saldot1)*100, 2, '.', ',').'%';?></td>
						</tr>
						<?php 
							$b [$cont]=	number_format(($row->saldo/$saldot1)*100, 2, '.', ','); 
							if($cont==1){ ?> <input type="hidden" id="a1" value="<?php echo set_value('a1',$b [$cont]); ?>"> <input type="hidden" id="n1" value="<?php echo set_value('n1',$row->Zona); ?>"> <?php } 		
							if($cont==2){ ?> <input type="hidden" id="a2" value="<?php echo set_value('a2',$b [$cont]); ?>"> <input type="hidden" id="n2" value="<?php echo set_value('n2',$row->Zona); ?>"> <?php } 
							if($cont==3){ ?> <input type="hidden" id="a3" value="<?php echo set_value('a3',$b [$cont]); ?>"> <input type="hidden" id="n3" value="<?php echo set_value('n3',$row->Zona); ?>"> <?php } 
							if($cont==4){ ?> <input type="hidden" id="a4" value="<?php echo set_value('a4',$b [$cont]); ?>"> <input type="hidden" id="n4" value="<?php echo set_value('n4',$row->Zona); ?>"> <?php } 
							if($cont==5){ ?> <input type="hidden" id="a5" value="<?php echo set_value('a5',$b [$cont]); ?>"> <input type="hidden" id="n5" value="<?php echo set_value('n5',$row->Zona); ?>"> <?php } 
							if($cont==6){ ?> <input type="hidden" id="a6" value="<?php echo set_value('a6',$b [$cont]); ?>"> <input type="hidden" id="n6" value="<?php echo set_value('n6',$row->Zona); ?>"> <?php } 
							if($cont==7){ ?> <input type="hidden" id="a7" value="<?php echo set_value('a7',$b [$cont]); ?>"> <input type="hidden" id="n7" value="<?php echo set_value('n7',$row->Zona); ?>"> <?php } 
							if($cont==8){ ?> <input type="hidden" id="a8" value="<?php echo set_value('a8',$b [$cont]); ?>"> <input type="hidden" id="n8" value="<?php echo set_value('n8',$row->Zona); ?>"> <?php } 
							if($cont==9){ ?> <input type="hidden" id="a9" value="<?php echo set_value('a9',$b [$cont]); ?>"> <input type="hidden" id="n9" value="<?php echo set_value('n8',$row->Zona); ?>"> <?php } 
							if($cont==10){ ?> <input type="hidden" id="a10" value="<?php echo set_value('a10',$b [$cont]); ?>"> <input type="hidden" id="n10" value="<?php echo set_value('n10',$row->Zona); ?>"> <?php } 
							if($cont==11){ ?> <input type="hidden" id="a11" value="<?php echo set_value('a11',$b [$cont]); ?>"> <input type="hidden" id="n11" value="<?php echo set_value('n11',$row->Zona); ?>"> <?php } 		
							if($cont==12){ ?> <input type="hidden" id="a12" value="<?php echo set_value('a12',$b [$cont]); ?>"> <input type="hidden" id="n12" value="<?php echo set_value('n12',$row->Zona); ?>"> <?php } 
							if($cont==13){ ?> <input type="hidden" id="a13" value="<?php echo set_value('a13',$b [$cont]); ?>"> <input type="hidden" id="n13" value="<?php echo set_value('n13',$row->Zona); ?>"> <?php } 
							if($cont==14){ ?> <input type="hidden" id="a14" value="<?php echo set_value('a14',$b [$cont]); ?>"> <input type="hidden" id="n14" value="<?php echo set_value('n14',$row->Zona); ?>"> <?php } 
							if($cont==15){ ?> <input type="hidden" id="a15" value="<?php echo set_value('a15',$b [$cont]); ?>"> <input type="hidden" id="n15" value="<?php echo set_value('n15',$row->Zona); ?>"> <?php } 
							if($cont==16){ ?> <input type="hidden" id="a16" value="<?php echo set_value('a16',$b [$cont]); ?>"> <input type="hidden" id="n16" value="<?php echo set_value('n16',$row->Zona); ?>"> <?php } 
							if($cont==17){ ?> <input type="hidden" id="a17" value="<?php echo set_value('a17',$b [$cont]); ?>"> <input type="hidden" id="n17" value="<?php echo set_value('n17',$row->Zona); ?>"> <?php }?>
						<?php endforeach;?>
					</tbody>
					<tfoot>
						<tr>
							<th style="font-size: 12px; color: red" width="100px" colspan="2">Total</th>
							<th style="font-size: 12px; color: red" width="150px" colspan="2"><?php echo '$ '.number_format($saldot, 2, '.', ',');?></th>					
						</tr>
					</tfoot>
				</table>
					</th>
				</tr>
			</table>
				
	 	</div>
		<div id="todos" class="tab_content"  >	
			<table border="0">
				<tr>
					<th>
						<div class="ajaxSorterDiv" id="tabla_est" name="tabla_est" style="height: 455px;width: 640px; ">                
                		<div style=" text-align:left; font-size: 12px;" class='filter'  >
                		<div align="left">Zona:                    		
							<select id="cmbZona" style="font-size: 10px; height: 25px; " >								
    							<?php foreach ($result as $row):  ?>	
    							<option value="<?php echo $row->Zona; ?>"><?php echo $row->Zona;?></option>		
						        <?php endforeach ;?>
							</select> 
							<select name="cmbCancelacion" id="cmbCancelacion" style="font-size: 10px; height: 25px; width:150px;" >
          						<option value="-1">Sin Saldos Incobrables</option>
          						<option value="0">Todos</option>
          					</select>
          					</div>
                		</div>
                		<span class="ajaxTable" style="height: 390px; width: 630px; margin-top: 1px" >
                    	<table id="mytablaE" name="mytablaE" class="ajaxSorter" >
                        	<thead title="Presione las columnas para ordenarlas como requiera">                            
                            	<th data-order = "Razon" style="width: 250px;" >Razon Social</th>                                                        							                                                                                                              
                            	<th data-order = "Saldo" style="width: 70px;">Saldo</th>
                            	<th data-order = "FecUD" style="width: 70px;">Fecha</th> 
                            	<th data-order = "ImpUD" style="width: 70px;">Importe</th>  
                            	<th data-order = "ObsUD" >Observaciones</th>                            
                        	</thead>
                        	<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="margin-top: -10px">        
	                    	<ul class="order_list" style="width: 150px; color: red">SALDOS ACTUALES</ul>                                            
    	                	<form method="post" action="<?= base_url()?>index.php/estadistica/pdfrep/" >
        	            		<input type="hidden" size="8%" name="zonasel" id="zonasel" />                      	             	                                                                                           
                	        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        		                        		                                                      
 							</form>   
                		</div>                                      
            			</div>             	
					</th>
					<th>
						<table border="0" class="ajaxSorter">
							<thead>
							<tr>
								<th colspan="2" >Información de Cliente</th>								
							</tr>
							</thead>
							<tbody>
							<tr>
								<th >Razón Social<input type="hidden" size="3%" name="id" id="id"></th>
								<th style="text-align: right" ><textarea  disabled="true"  name="zon" id="zon"  cols="20" style="resize: none; text-align: center" rows="1"></textarea></th>
							</tr>
							<tr>
								<th colspan="2"><textarea disabled="true"  name="nombre" id="nombre"  cols="50" style="resize: none"></textarea></th>
							</tr>
							<tr>
								<th >Saldo Neto</th>
								<th style="text-align: right"><textarea  disabled="true"  name="impsal" id="impsal"  cols="20" style="resize: none; text-align: center" rows="1"></textarea></th>
							</tr>
							<tr>
								<th colspan="2">Registro del Último Depósito</th>
							</tr>
							<tr>
								<th style="text-align: right">Fecha</th>
								<th><textarea disabled="true" name="fec" id="fec"  cols="13" style="resize: none; text-align: center" rows="1"></textarea></th>
							</tr>
							<tr>
								<th style="text-align: right">Días</th>
								<th><textarea disabled="true" name="dias" id="dias"  cols="13" style="resize: none; text-align: center" rows="1"></textarea></th>								
							</tr>
							<tr>
								<th style="text-align: right">Importe</th>
								<th><textarea disabled="true" name="imp" id="imp"  cols="13" style="resize: none; text-align: center" rows="1"></textarea></th>								
							</tr>
							<tr>
								<th >Saldo Incobrable</th>
								<th style="text-align: right">
									<?php if($usuario=="Jesus Benítez" or $usuario=="Efrain Lizárraga"){ ?>	
									<select name="sn" id="sn" style=" background-color: red;  font-size: 14px;height: 25px; margin-top: 1px;">
      										<option style="color: blue" value="0" > No </option>
      										<option style="color: blue" value="-1"  > Si </option>      									
    								  </select>
    								<?php } else {?>
    								<select disabled="true" name="sn" id="sn" style=" background-color: red;  font-size: 14px;height: 25px; margin-top: 1px;">
      										<option style="color: blue" value="0" > No </option>
      										<option style="color: blue" value="-1"  > Si </option>      									
    								  </select>	
    								<?php } ?>	
									
								</th>
							</tr>
							<tr>
								<th colspan="2">Observaciones</th>
							</tr>
							<tr>
								<th colspan="2"><textarea  name="obs" id="obs"  cols="50" style="resize: none" rows="5"></textarea></th>
							</tr>
							</tbody>
							<tfoot>
							<tr>
								<th style="text-align: right" colspan="2"> Actualizar Datos
										<?php if($usuario=="Jesus Benítez" || $usuario=="Efrain Lizárraga"){ ?>
											<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
										<?php } else {?>
											<input disabled="true" style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />	
										<?php }?>	
										<!--<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar()" />-->
								</th>
							</tr>
							</tfoot>
							
						</table>						
					</th>
				</tr>
			</table>
					 
        			
	 								
		</div>			
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#cmbZona").change( function(){		
	$("#zonasel").val($("#cmbZona").val());	
	$("#zon").val('');$("#id").val('');$("#nombre").val('');$("#fec").val('');$("#dias").val('');
	$("#imp").val('');$("#impsal").val('');$("#sn").val('');$("#obs").val('');		
 return true;
});

$("#aceptar").click( function(){	
	numero=$("#id").val();
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/estadistica/actualizar", 
						data: "id="+$("#id").val()+"&obs="+$("#obs").val()+"&sn="+$("#sn").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");										
										$("#mytablaE").trigger("update");
										$("#zon").val('');$("#id").val('');       	
										$("#nombre").val('');$("#fec").val('');$("#dias").val('');
										$("#imp").val('');$("#impsal").val('');$("#sn").val('');
										$("#obs").val('');																										
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
				}else{
		alert("Error: Necesita seleccionar Cliente para poder actualizarlo");
		return false;
	}	
});

$(document).ready(function(){
	$("#zonasel").val($("#cmbZona").val());	
	$("#tabla_est").ajaxSorter({
	url:'<?php echo base_url()?>index.php/estadistica/tabla',  	
	filters:['cmbZona','cmbCancelacion'],		
    //active:['SI','-1'], 
    sort:false,
    onRowClick:function(){
    	if($(this).data('numero')>'0'){
        	$("#zon").val($(this).data('zona'));$("#id").val($(this).data('numero'));       	
			$("#nombre").val($(this).data('razon'));$("#fec").val($(this).data('fecud1'));$("#dias").val($(this).data('diferencia'));
			$("#imp").val($(this).data('impud'));$("#impsal").val($(this).data('saldo'));$("#sn").val($(this).data('si'));
			$("#obs").val($(this).data('obsud'));
			$("#obs").focus();
		}		
    }, 
    onSuccess:function(){
    		importe = 0;
    	   	$('#tabla_est tbody tr').map(function(){	    		
	    		if($(this).data('diferencia')>='60'){
	    			$(this).css('background','lightyellow');	    				    			
	    		}
	    		if($(this).data('si')=='-1'){
	    			$(this).css('background','red');	    				    			
	    		}	
	    	});
	    	$('#tabla_est tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_est tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
   },     
});
});


$(function () {
    var chart; 
    a1=$("#a1").val(); a1=parseFloat(a1);a2=$("#a2").val(); a2=parseFloat(a2);a3=$("#a3").val(); a3=parseFloat(a3);
    a4=$("#a4").val(); a4=parseFloat(a4);a5=$("#a5").val(); a5=parseFloat(a5);a6=$("#a6").val(); a6=parseFloat(a6);
    a7=$("#a7").val(); a7=parseFloat(a7);a8=$("#a8").val(); a8=parseFloat(a8);a9=$("#a9").val(); a9=parseFloat(a9);
    a10=$("#a10").val(); a10=parseFloat(a10);a11=$("#a11").val(); a11=parseFloat(a11);a12=$("#a12").val(); a12=parseFloat(a12);
    a13=$("#a13").val(); a13=parseFloat(a13);a14=$("#a14").val(); a14=parseFloat(a14);a15=$("#a15").val(); a15=parseFloat(a15);
    a16=$("#a16").val(); a16=parseFloat(a16);a17=$("#a17").val(); a17=parseFloat(a17);
    n1=$("#n1").val();n2=$("#n2").val();n3=$("#n3").val();n4=$("#n4").val();n5=$("#n5").val();n6=$("#n6").val();n7=$("#n7").val();
    n8=$("#n8").val();n9=$("#n9").val();n10=$("#n10").val();
    n11=$("#n11").val();n12=$("#n12").val();n13=$("#n13").val();n14=$("#n14").val();n15=$("#n15").val();n16=$("#n16").val();n17=$("#n17").val();
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'circular',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Saldos Generales'
            },
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            	percentageDecimals: 2
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ (parseFloat(this.percentage)).toFixed(2)  +' %';
                        
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: '% Deudor',
                data: [
                    {
                        name: n1,
                        y: a1,
                        sliced: true,
                        selected: true
                    },
                    [n2,   a2],
                    [n3,   a3],                    
                    [n4,   a4],
                    [n5,   a5],
                    [n6,   a6],
                    [n7,   a7],
                    [n8,   a8],
                    [n9,   a9],
                    [n10,   a10],
                    [n11,   a11],
                    [n12,   a12],
                    [n13,   a13]
                   // [n14,   a14],
                   // [n15,   a15]
                   // [n16,   a16]
                    //[n17,   a17]
                ]
            }]
        });
    });
    
});

</script>