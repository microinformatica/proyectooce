<?php $this->load->view('header_cv'); ?>
<style>        
.tab_content {
	padding: 10px; font-size: 1.2em; height: 452px;
}
ul.tabs li {
	border-top: none; border-left: none; border-right: none; border-bottom: 1px ; font-size: 25px; background: none;		
}
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/cliente.png" width="25" height="25" border="0"> Estadistica - Actualizar</li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal">
			<div id="clientes" class="tab_content" style="overflow-x:hidden;" >
				<div style="height:420px; margin-top: -15px">
					<form method="post" action="<?=base_url()?>index.php/estadistica/actualizar" id="form" >								
						<table border="2px"  class="tablesorter">
							<thead><th colspan="4" height="25px" style="font-size: 34px;"><center>Información de Cliente</center><input type="hidden" value="<?= $resultado->Numero?>" name="id" id="id"/></th></thead>
								<tr>
									<th colspan="4"><label style="margin: 2px; font-size: 25px" for="nombre" >Razón Social</label></th>									
								</tr>
								<tr  >									
									<th colspan="4" bgcolor="white"><label style="margin: 2px; font-size: 20px; background-color: white" ><?=$resultado->Razon?></label></th>
								</tr>
							<tbody>
								<tr style="font-size: 25px" >
									<th colspan="3"  ><div align="right">Saldo Neto:</div></th>
									<th bgcolor="white"><div align="right"><?='$ '.number_format($resultado->Saldo, 2, '.', ',');?></div></th>
								</tr>
								<tr style="font-size: 25px">
									<th colspan="4" >Registro del Último Depósito</th>																		
								</tr>
								<tr style="font-size: 25px">
        							<th ><div align="center">Fecha</div></th>
        							<th ><div align="center">Dias</div></th>
        							<th ><div align="center">Saldo Incobrable</div></th>
        							<th ><div align="center">Importe</div></th>
      							</tr>
      							<tr style="font-size: 20px" bgcolor="white">
        							<th ><div align="center"><?php if($resultado->ImpUD>0) echo date("d-m-Y",strtotime($resultado->FecUD))?></div></th>
        							<th ><div align="center"><?=$resultado->diferencia?></div></th>
        							<th ><div align="center">
        								<input id="rbLugar" name="rbLugar" type="radio"  value="0" <?php if($resultado->SI==0) {?> checked="checked" <?php }?>>NO
        								<input id="rbLugar" name="rbLugar" type="radio" value="-1" <?php if($resultado->SI==-1) {?> checked="checked"}<?php }?>>SI
          							</th>
        							<th ><div align="center"><?='$ '.number_format($resultado->ImpUD, 2, '.', ',');?></div></th>
      							</tr>
      							<tr style="font-size: 25px">
        							<th colspan="4">Observaciones</th>        							
      							</tr>
      							<tr style="font-size: 18px">
        							<th colspan="4" bgcolor="white"><div align="center"><textarea style="font-size: 25px" id ="txtobs" name="txtobs" cols="78" wrap="virtual"><?=$resultado->ObsUD?></textarea></div></th>        							
      							</tr>
							</tbody>								
							<tfoot>
								<th style="font-size: 25px" colspan="3">Zona: <?=$resultado->Zona?></th>
								<th ><center><button name="boton" value="1" style="font-size: 25px">Guardar</button></center></th>
							</tfoot>
    					</table>    					
				</form>
			</div>
		</div>
	</div>	
</div>
	
</div>
<?php $this->load->view('footer_cv'); ?>
