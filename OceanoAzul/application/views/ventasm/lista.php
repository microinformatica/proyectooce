<?php $this->load->view('header_cv'); ?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
/*ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
} */  
#export_datag { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datav { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_sema { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		Ciclo
            <select id="ciclo" style="font-size:12px; margin-top: 1px;" >
              <option value="22">2022</option>
              <option value="21">2021</option><option value="20">2020</option><option value="19">2019</option>
   			</select>
		<strong>
		<li><a href="#ventas" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/salida.jpg" width="20" height="20" border="0"> Ventas </a></li>
		<li><a href="#ventasg" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/salida.png" width="20" height="20" border="0"> General </a></li>
		<li><a href="#estatus" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/semaforo.jpg" width="20" height="20" border="0"> Estatus Saldos </a></li>
		</strong>		
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 650px"  >
		<div id="ventas" class="tab_content" style="margin-top: 1px">
			<div class="ajaxSorterDiv" id="tabla_vta" name="tabla_vta" style="width: 955px; margin-left: -10px; margin-top: -20px"  >
				<div class="ajaxpager" style="margin-top: 1px;" > 
            		<ul class="order_list" style="width: 650px; text-align: left" >
            			
            			Salidas según Estatus de 
        				<select id="estsal" style="font-size:13px; margin-top: 1px" >
                   			<option value="1">Venta</option>
          					<option value="2">Traslado</option>	
   						</select> 
   						<button id="export_datav" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
						<form id="data_tablev" action="<?= base_url()?>index.php/ventasm/pdfrepvta" method="POST">
							<input type="hidden" name="tablav" id="html_vtav"/> <input type="hidden" name="gra" id="gra"/>
						</form>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#export_datav').click(function(){									
									$('#html_vtav').val($('#mytablaVta').html());
									$('#data_tablev').submit();
									$('#html_vtav').val('');
								});
							});
						</script>       
                   	</ul>
 				</div>                 			
				<span class="ajaxTable" style="height: 480px;" >
                	<table id="mytablaVta" name="mytablaVta" class="ajaxSorter" border="1" style="width:945px" >
                		<thead >     
                   			<th data-order = "fecsal1" >Fecha</th>
                           	<th data-order = "folsal" >Folio</th>
                           	<th data-order = "noma" >Almacen</th> 
                           	<th data-order = "Razon" >Cliente</th> 
                           	<th data-order = "nomt" >Talla</th> 
                           	<!--
                           	<th data-order = "cajsal" >Cajas</th>	
                           	<th data-order = "presal" >Presentación</th>
                           	-->  
                           	<th data-order = "kgssal" >Kgs</th>  
                           	<th data-order = "prekgs1" style="width: 40px"> MN</th>
                           	<th data-order = "impvta" style="width: 80px" >Importe</th> 
                           	<th data-order = "prekgs2" style="width: 40px"> USD</th>  
                            <th data-order = "impvtau" style="width: 80px">Importe</th> 
                           	<th data-order = "facsal" >Factura</th>                          
                           	<th data-order = "aviso" >Aviso</th>
                           	<!--<th data-order = "obssal" >Observaciones</th>-->
                    	</thead>
                    	<tbody title="Seleccione para realizar cambios" style="font-size: 11px;">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
            <table width="780px" class="ajaxSorter"  border="1px">
					<tr>
						<td >Almacen:
						<input type="hidden" id="idsal" name="idsal" />
						</td>
						<td colspan="4" style="font-size: 14px; background-color: white"><input type="text" name="noma" id="noma" style="width: 280px; border: none"  readonly="true"></td>
						<td >Cliente:</td>
						<td colspan="4" style="font-size: 14px; background-color: white"><input type="text" name="Razon" id="Razon" style="width: 380px; border: none"  readonly="true"></td>
						<td rowspan="4">
							<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez" ){ ?>
						<input style="font-size: 18px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
						<?php }?>
						</center>
						</td>
					</tr>
					
    				<tr>
						<td>Fecha</td><td>Folio</td><td>Talla</td><td>Cajas</td><td>Presentación</td><td>Kgs</td>
						<td>$ MN</td><td>$ USD</td><td>Factura</td><td>Aviso</td>
					</tr>
					<tr>
						<td style="font-size: 14px; background-color: white"><input type="text" name="fecsal" id="fecsal" style="width: 80px; border: none;" readonly="true" ></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="folsal" id="folsal" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="nomt" id="nomt" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="cajsal" id="cajsal" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="presal" id="presal" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="kgssal" id="kgssal" style="width: 50px; border: none"  readonly="true"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="prekgs" id="prekgs" style="width: 50px;"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="usdkgs" id="usdkgs" style="width: 50px;"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="facsal" id="facsal" style="width: 140px;"></td>
						<td style="font-size: 14px; background-color: white"><input type="text" name="aviso" id="aviso" style="width: 70px" /></td>
					</tr>   
					<tr>
						<td>Observaciones:</td>
						<td colspan="9"><input type="text" name="obssal" id="obssal" style="width:700px" /></td>
					</tr> 			
    		</table> 			
		</div>
	 	<div id="ventasg" class="tab_content" style="margin-top: 1px">
			<div class="ajaxSorterDiv" id="tabla_vtag" name="tabla_vtag" style="width: 950px; margin-left: -10px"  >
				<div class="ajaxpager" style="margin-top: 1px;" > 
            		<ul class="order_list" style="width: 650px; text-align: left" >
            			Ventas a Clientes de acuerdo a su salida de inventarios en Granja 
            			<select id="cmbGra" style="font-size:13px; margin-top: 1px" >
                   			<option value="0">Ambas</option>
                   			<option value="4">OA- Ahome</option>
                   			<option value="6">OA- Huatabampo</option>
                   			<option value="10" >OA- Topolobampo</option>
                   			<option value="2">OA- Kino</option>
                   			<option value="120">Agrupadas</option>
          				</select> 
          				<button id="export_datag" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
						<form id="data_tableg" action="<?= base_url()?>index.php/ventasm/pdfrepvtasg" method="POST">
							<input type="hidden" name="tabla" id="html_vtag"/> <input type="hidden" name="gra" id="gra"/>
						</form>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#export_datag').click(function(){									
									$('#html_vtag').val($('#mytablaVtag').html());
									$('#data_tableg').submit();
									$('#html_vtag').val('');
								});
							});
						</script>       
                   	</ul>
 				</div>                 			
				<span class="ajaxTable" style="height:560px;width:945px" >
                	<table id="mytablaVtag" name="mytablaVtag" class="ajaxSorter" border="1"  >
                		<thead >     
                   			<tr>
                   			<th rowspan="2" data-order = "sigg" >Gja</th>
                           	<th rowspan="2" data-order = "Razon" >Cliente</th>
                           	<th rowspan="2" data-order = "kgsvta" >Kgs</th> 
                           	<th colspan="5" >M.N.</th> 
                           	<th colspan="4" >U.S.D.</th> 
                           	</tr>
                           	<tr>
                   			<th data-order = "impvta" >Venta</th> 
                           	<th data-order = "carvta" >Cargos</th>
                           	<th data-order = "depvta" >Depósitos</th> 
                           	<th data-order = "desvta" >Descts</th>
                           	<th data-order = "salvta" >Saldo</th>
                           	<th data-order = "impvtau" >Venta</th> 
                           	<th data-order = "depvtau" >Depósitos</th>
                           	<th data-order = "desvtau" >Descts</th>  
                           	<th data-order = "salvtau" >Saldo</th>
                           	</tr>
                    	</thead>
                    	<tbody style="font-size: 11px; text-align: right">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
          </div>   			
		<div id="estatus" class="tab_content" style="margin-top: 1px">
			<div class="ajaxSorterDiv" id="tabla_sema" name="tabla_sema" style="width: 950px; margin-left: -10px"  >
				<div class="ajaxpager" style="margin-top: 1px;" > 
            		<ul class="order_list" style="width: 650px; text-align: left" >
            			Saldos Actuales 
            			<button id="export_sema" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
						<form id="data_tablesema" action="<?= base_url()?>index.php/ventasm/pdfrepsema" method="POST">
							<input type="hidden" name="tablasema" id="html_sema"/> 
						</form>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#export_sema').click(function(){									
									$('#html_sema').val($('#mytablaSema').html());
									$('#data_tablesema').submit();
									$('#html_sema').val('');
								});
							});
						</script>       
                   	</ul>
 				</div>                 			
				<span class="ajaxTable" style="height:560px;width:945px" >
                	<table id="mytablaSema" name="mytablaSema" class="ajaxSorter" border="1"  >
                		<thead >     
                   			<tr>
                   			<th rowspan="2" data-order = "Razon" >Cliente</th>
                           	<th colspan="3" >M.N.</th> 
                           	<th colspan="3" >U.S.D.</th> 
                           	</tr>
                           	<tr>
                   			<th data-order = "salvta" >Saldo</th>
                   			<th data-order = "fecmn1" >Fecha</th> 
                           	<th data-order = "diasmn" >Dias</th>
                           	<th data-order = "salvtau" >Saldo</th>
                           	<th data-order = "fecus1" >Fecha</th> 
                           	<th data-order = "diasus" >Dias</th>
                           	</tr>
                    	</thead>
                    	<tbody style="font-size: 14px; text-align: right">
                    	</tbody>                        	
                    </table>
                </span> 
            </div>
          </div>  </div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#aceptar").click( function(){	
	numero=$("#idsal").val();
	pre=$("#prekgs").val();
	if(numero!=''){
		//if(pre!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/ventasm/actualizar", 
						data: "id="+$("#idsal").val()+"&pre="+$("#prekgs").val()+"&fac="+$("#facsal").val()+"&avi="+$("#aviso").val()+"&usd="+$("#usdkgs").val()+"&obs="+$("#obssal").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaVta").trigger("update");
										$("#idsal").val('');$("#noma").val('');$("#Razon").val('');$("#fecsal").val('');$("#folsal").val('');$("#nomt").val('');$("#cajsal").val('');
										$("#presal").val('');$("#kgssal").val('');$("#prekgs").val('');$("#facsal").val('');$("#aviso").val('');$("#usdkgs").val('');																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
		/*}else{
			alert("Error: Registre Precio de Venta");	
			$("#prekgs").focus();
			return false;
		}	*/		
		}else{
			alert("Error: Seleccione Venta para poder actualizar");	
			return false;
		}
});


$(document).ready(function(){
	$("#tabla_vta").ajaxSorter({
		url:'<?php echo base_url()?>index.php/ventasm/tabla',  
		filters:['ciclo','estsal'],
        //active:['Edo','Sinaloa'],  
        multipleFilter:true,
        sort:false,
        onRowClick:function(){
           	$("#idsal").val($(this).data('idsal'));
			$("#noma").val($(this).data('noma'));
			$("#Razon").val($(this).data('razon'));
			$("#fecsal").val($(this).data('fecsal1'));
			$("#folsal").val($(this).data('folsal'));
			$("#nomt").val($(this).data('nomt'));
			$("#cajsal").val($(this).data('cajsal'));
			$("#presal").val($(this).data('presal'));
			$("#kgssal").val($(this).data('kgssal'));
			$("#prekgs").val($(this).data('prekgs'));$("#usdkgs").val($(this).data('usdkgs'));
			$("#facsal").val($(this).data('facsal'));
			$("#aviso").val($(this).data('aviso'));
			$("#obssal").val($(this).data('obssal'));
			$("#prekgs").focus();			
        },
        onSuccess:function(){ 
			$('#tabla_vta tbody tr').map(function(){
		    	if($(this).data('fecsal1')=='') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		}
	        });
		    $('#tabla_vta tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right');});
	    	$('#tabla_vta tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right');});
	    	$('#tabla_vta tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right');});
	    	$('#tabla_vta tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','center');});  		
		},     
	});
	$("#tabla_vtag").ajaxSorter({
		url:'<?php echo base_url()?>index.php/ventasm/tablag',  
		filters:['ciclo','cmbGra'],
		//active:['salvta1','0','<='],  
        sort:false,  
        onSuccess:function(){ 
			$('#tabla_vtag tbody tr').map(function(){
		    	if($(this).data('sigg')=='Gran Total') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		}
	    		if($(this).data('razon')=='Total Granja') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		}
		    });
		    $('#tabla_vtag tbody tr td:nth-child(1)').map(function(){ 
	    		if( $(this).html() != '' ) {
              			$(this).css('background','lightblue');$(this).css('font-weight','bold');$(this).css('text-align','left');
        			}
	    	});
	    	$('#tabla_vtag tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','justify');});
	    		
		},       
	});
	$("#tabla_sema").ajaxSorter({
		url:'<?php echo base_url()?>index.php/ventasm/tablasema',  
		filters:['ciclo'],
		//active:['salvta1','0','<='],  
        sort:false,  
        onSuccess:function(){ 
        	$('#tabla_sema tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','left');});
			$('#tabla_sema tbody tr td:nth-child(4)').map(function(){ 
				$(this).css('font-weight','bold');$(this).css('text-align','center');
					if($(this).html() >= 30 && $(this).html() != '' ) {
              				$(this).css("background-color", "red");
        			}
        			if($(this).html() >= 25 && $(this).html() <= 29 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); 
        			}
        			if($(this).html() <= 25 && $(this).html() != '' ) {
             				$(this).css("background-color", "#0F0"); 
        			} 
	    	});
	    	$('#tabla_sema tbody tr').map(function(){
		    	if($(this).data('razon')=='Gran Total') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		}
	    		if($(this).data('fecmn1')!='') {	    				    			
	    			$(this).css('font-weight','bold');
	    		}	    		
		    });
		},       
	});
}); 



</script>
