<?php
header("Content-Type: application/vnd.ms-excel");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");

header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/vnd.ms-excel charset=iso-8859-1");
header("Content-Disposition: attachment; filename=EdoCta_".$cli.".xls");

?>

<html>
    <head>
       <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1 " />-->
        <title></title>
        <style>
            th, td{
                font-size: 12pt;                
            }            
        </style>
    </head>
    <body>
    		<div id="header"> 
		<table style="width: 890px; margin-left: -20px">
			<tr>
				<td colspan="2" width="710px">
					<p style="margin-left: 15px"><img src="<?php echo base_url();?>assets/images/oceanoazul.jpg" width="200" height="60" border="0"></p>
				</td>	
				<td colspan="10" style="font-size: 20px; color: navy; text-align: right"><strong>ESTADO DE CUENTA</strong></td>			
			</tr>
		</table>
	</div> 
	
	<?php 
	function weekday($fechas){
		    $fechas=str_replace("/","-",$fechas);
		    list($dia,$mes,$anio)=explode("-",$fechas);
		    return (((mktime ( 0, 0, 0, $mes, $dia, $anio) - mktime ( 0, 0, 0, 7, 17, 2006))/(60*60*24))+700000) % 7;
		}
		$fechas=date("m");
		if($fechas == 1 ) { $mes = "Enero"; }
		if($fechas == 2 ) { $mes = "Febrero"; }
		if($fechas == 3 ) { $mes = "Marzo"; }
		if($fechas == 4 ) { $mes = "Abril"; }
		if($fechas == 5 ) { $mes = "Mayo"; }
		if($fechas == 6 ) { $mes = "Junio"; }
		if($fechas == 7 ) { $mes = "Julio"; }
		if($fechas == 8 ) { $mes = "Agosto"; }
		if($fechas == 9 ) { $mes = "Septiembre"; }
		if($fechas == 10 ) { $mes = "Octubre"; }
		if($fechas == 11 ) { $mes = "Noviembre"; }
		if($fechas == 12 ) { $mes = "Diciembre"; }
		$diaN=weekday(date("d/m/Y"));
		//$diaN=weekday($mes);
		if ( $diaN == 0 ) { $dia="Lunes";}
		if ( $diaN == 1 ) { $dia="Martes";}
		if ( $diaN == 2 ) { $dia="Miércoles";}
		if ( $diaN == 3 ) { $dia="Jueves";}
		if ( $diaN == 4 ) { $dia="Viernes";}
		if ( $diaN == 5 ) { $dia="Sábado";}
		if ( $diaN == 6 ) { $dia="Domingo";}
	?>
<!--<label  style="font-size: 8px; margin-top: -15px">SIA - <?= $usuario?> - <?= $perfil?></label>-->
<table border="0" width="825px" style="margin-top: 5px">
	<tr> <td style="font-size: 16px; " colspan="6">	<?= $cli?></td> </tr>
	<tr> <td style="font-size: 12px; " colspan="6"> <?= $dom?></td>	</tr>
	<tr> <td style="font-size: 12px; " colspan="6"> <?= $lecp?></td> </tr>
	<tr> <td style="font-size: 14px; " colspan="6">	<?php if($atc!=''){ ?> At´n:<u> <?= $atc?></u> <?php } ?> </td>
		 <td colspan="6" align="right" style="font-size: 14px; ">Detalle de movimientos al día <?= $dia.", ".date("d")." de ".$mes. " de ".date("Y")."."?></td>
	</tr>
</table>



	<table border="1"  style="font-size: 11px" width="890px">
    	  <?= $tabla?>    	  
	</table>



	<p>
	<table style="font-size: 12px;" border="0" width="890px">
		<thead>			
			<tr> <th colspan="12" align="left" style="font-size: 14px">Sin más por el momento quedamos a sus ordenes para cualquier aclaración o duda al respecto.
				</th>
			</tr>
			<tr> <th colspan="12" align="right" style="font-size: 14px"><br /><?= $usuario?> <div style="font-size: 10px"></div>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr> <th colspan="12" align="left" style="font-size: 14px">ACUICOLA OCEANO AZUL S.A. DE C.V.</th></tr>	
			<tr ><th style="text-align: left" colspan="3">Banco</th><th colspan="2" style="width: 70px">Cuenta</th><th colspan="2" align="left">No. Clabe</th><th></th><th colspan="4" align="right" style="width: 295px"><?= $perfil?></th></tr>
			<tr ><th rowspan="2" align="left" style="width: 90px">BBVA Bancomer</th><th colspan="2" align="right" style="width: 105px">Moneda Nacional</th><th colspan="2" style="width: 70px;">0111540734</th><th colspan="3" align="left">&nbsp; 012744001115407346</th></th><th colspan="4" align="right" style="width: 295px">Atención Telefónica: <?= $tel?>  </th></tr>
			<tr><th colspan="2" align="right" style="width: 105px">Dólares Americanos</th><th colspan="2" style="width: 70px">0111843508</th><th colspan="3" align="left">&nbsp; 012744001118435085</th><th colspan="4" align="right" style="width: 295px">e-mail: <?= $correo?></th></tr>
			<tr style="font-size: 7px; color: navy;">
				<th colspan="12" style="font-size: 11px" >
					Av. Reforma No.2007-B Int. 803, Col. Flamingos, Mazatlán, Sin. México, RFC:AOA-180206-SI2, CP:82149 - Tel:(669)985-64-46 
				</th>		
			</tr>
		</tbody>
	</table>
	</p>	
    </body>    
</html>


