    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reporte de Entregas</title>
</head>
<body>
<label  style="font-size: 8px; margin-top: -15px">SIA - <?= $usuario?> - <?= $perfil?></label>
<?php
	if($zona==0){$zonas=" por Zona";}
	else{$zonas="";}
	/*if($zona==1){$zonas=" en Angostura";}
	if($zona==2){$zonas=" en Colima";}
	if($zona==3){$zonas=" en El Dorado";}			
	if($zona==4){$zonas=" en Elota";}
	if($zona==5){$zonas=" en Guasave";}
	if($zona==6){$zonas=" en Hermosillo";}
	if($zona==7){$zonas=" en Mochis";}
	if($zona==8){$zonas=" en Navojoa";}
	if($zona==9){$zonas=" en Navolato";}
	if($zona==10){$zonas=" en Nayarit";}
	if($zona==11){$zonas=" en Obregon";}
	if($zona==12){$zonas=" en Sur Sinaloa";}
	if($zona==13){$zonas=" en Tamaulipas";}
	if($zona==14){$zonas=" en Yucatán";}*/
	?>

<br>Entregas realizadas ciclo [<?= $ciclo ?>]  
<div style="margin-left: -25px">
	<table border="1" width="770px" style="font-size: 9px">
    	  <?= $tablac?>
	</table>
</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 14;
        $color = array(0,0,0);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height();
        $y = $h - 3 * $text_height - 24;
                
        $y += $text_height;
        $y1 = $y;
        $text = utf8_encode("Aquapacific S.A. de C.V.");
        $pdf->page_text($w-595, $y-728, $text, $font, $size, $color);  
        
        $size = 6;
        $y += $text_height;
        $text = utf8_encode("Sistema Integral Administrativo");
        $pdf->page_text($w-570, $y-726, $text, $font, $size, $color);
        
        $size = 7;
        $y = $y1;        
        $y += $text_height;
        $text = utf8_encode("Av. Emilio Barragán #63-103");
        $pdf->page_text($w - 229, $y+4, $text, $font, $size, $color);        
        
        $text = "Pagina {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(16, $y+4, $text, $font, $size, $color);
        
        $y += $text_height;
        $text = utf8_encode("Colonia Lázaro Cárdenas C.P 82400");
        $pdf->page_text($w - 229, $y-4, $text, $font, $size, $color);
        
        $y += $text_height;
        $text = utf8_encode("Mazatlán, Sinaloa; México. Teléfono: (669)984-6545, 984-6546");
        $pdf->page_text($w - 229, $y-12, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>