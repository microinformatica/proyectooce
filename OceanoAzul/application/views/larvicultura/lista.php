<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li ><a href="#larvicultura"><img src="<?php echo base_url();?>assets/images/menu/8.png" width="25" height="25" border="0"> Larvicultura </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="larvicultura" class="tab_content" style="height: 452px;">
			<?php $hoy=date("Y-m-d"); ?>
			<div align="left" style="margin-left: 25px; margin-top: -10px" >
			Estadíos Actuales en Salas de Producción al día:<input style="border: none" size="12%" name="hoy1" id="hoy1" value="<?php echo set_value('hoy1',date("d-m-Y",strtotime($hoy))); ?>">	
			</div>
				<input type="hidden" style="border: none" size="12%" name="hoy" id="hoy" value="<?php echo set_value('hoy',$hoy); ?>">
				<input type="hidden" name="100i" id="100i" value="1"><input type="hidden" size="12%" name="100f" id="100f" value="12">
				<input type="hidden" name="200i" id="200i" value="13"><input type="hidden" size="12%" name="200f" id="200f" value="24">
				<input type="hidden" name="300i" id="300i" value="25"><input type="hidden" size="12%" name="300f" id="300f" value="36">
				<input type="hidden" name="400i" id="400i" value="37"><input type="hidden" size="12%" name="400f" id="400f" value="48">
				<input type="hidden" name="500i" id="500i" value="49"><input type="hidden" size="12%" name="500f" id="500f" value="60">
				<input type="hidden" name="600i" id="600i" value="61"><input type="hidden" size="12%" name="600f" id="600f" value="72">
				<input type="hidden" name="700i" id="700i" value="73"><input type="hidden" size="12%" name="700f" id="700f" value="84">
				<input type="hidden" name="800i" id="800i" value="85"><input type="hidden" size="12%" name="800f" id="800f" value="96">
				<input type="hidden" name="900i" id="900i" value="97"><input type="hidden" size="12%" name="900f" id="900f" value="108">
				<input type="hidden" name="1000i" id="1000i" value="109"><input type="hidden" size="12%" name="1000f" id="1000f" value="120">
				<input type="hidden" name="1100i" id="1100i" value="121"><input type="hidden" size="12%" name="1100f" id="1100f" value="132">
				<input type="hidden" name="1200i" id="1200i" value="133"><input type="hidden" size="12%" name="1200f" id="1200f" value="144">
				<input type="hidden" name="1300i" id="1300i" value="145"><input type="hidden" size="12%" name="1300f" id="1300f" value="156">
				<input type="hidden" name="1400i" id="1400i" value="157"><input type="hidden" size="12%" name="1400f" id="1400f" value="168">
				<input type="hidden" name="1500i" id="1500i" value="169"><input type="hidden" size="12%" name="1500f" id="1500f" value="180">
				<input type="hidden" name="1600i" id="1600i" value="181"><input type="hidden" size="12%" name="1600f" id="1600f" value="192">
				<input type="hidden" size="2%" name="tablasell" id="tablasell">
				<div style="overflow-y: hidden;height:310px; width: 890px">
				<table >
					<tbody>
						<tr>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar1" name="tabla_lar1" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL1" name="mytablaL1" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >100</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar2" name="tabla_lar2" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL2" name="mytablaL2" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >200</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar3" name="tabla_lar3" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL3" name="mytablaL3" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >300</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar4" name="tabla_lar4" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL4" name="mytablaL4" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >400</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar5" name="tabla_lar5" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL5" name="mytablaL5" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >500</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
            				</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar6" name="tabla_lar6" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL6" name="mytablaL6" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >600</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar7" name="tabla_lar7" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL7" name="mytablaL7" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >700</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar8" name="tabla_lar8" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL8" name="mytablaL8" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >800</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar9" name="tabla_lar9" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px;width: 110px; background-color: white;" >
                   						<table id="mytablaL9" name="mytablaL9" class="ajaxSorter" style="" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >900</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar10" name="tabla_lar10" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;" >
                   						<table id="mytablaL10" name="mytablaL10" class="ajaxSorter" style="width: 110px;" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1000</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar11" name="tabla_lar11" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;" >
                   						<table id="mytablaL11" name="mytablaL11" class="ajaxSorter" style="width: 110px;" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1100</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar12" name="tabla_lar12" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;" >
                   						<table id="mytablaL12" name="mytablaL12" class="ajaxSorter" style="width: 110px;" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1200</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar13" name="tabla_lar13" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;" >
                   						<table id="mytablaL13" name="mytablaL13" class="ajaxSorter" style="width: 110px;" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1300</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar14" name="tabla_lar14" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;" >
                   						<table id="mytablaL14" name="mytablaL14" class="ajaxSorter" style="width: 110px;" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1400</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
            				</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar15" name="tabla_lar15" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;" >
                   						<table id="mytablaL15" name="mytablaL15" class="ajaxSorter" style="width: 110px;" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1500</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th>
								<div class="ajaxSorterDiv" id="tabla_lar16" name="tabla_lar16" style="margin-top: -10px" >                
            						<span class="ajaxTable" style="height: 285px; background-color: white;" >
                   						<table id="mytablaL16" name="mytablaL16" class="ajaxSorter" style="width: 110px;" >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "Nombre" >1600</th> <th data-order = "PL1">Est</th>	<th data-order = "CanSie" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
						</tr>
					</tbody>
				</table>
			</div>	
			<table style="width: 890px">
				<tr>
					<th style="width: 610px;">
						<div class="ajaxSorterDiv" id="tabla_larsie" name="tabla_larsie" align="right"  >                
            				<span class="ajaxTable" style=" width: 210px; height: 130px; background-color: white;" >
                   				<table id="mytablaLS" name="mytablaLS" class="ajaxSorter" >
                       				<thead style="font-size: 9px">
                       					<tr>
                       						<th colspan="4" style="text-align: center">Pilas en Procesos Actuales</th>
                       					</tr>
                       					<tr>                            
                           				<th data-order = "FecSie" >Fecha</th>
                           				<th data-order = "Sala" >Sala</th>
                           				<th data-order = "Nombre"  >Pila</th>
                           				<th data-order = "CanSie" >Cantidad</th>
                           				</tr>                                              	                            	
                       				</thead>
                       				<tbody style="font-size: 11px">
                       				</tbody>                        	
                   				</table>
            				</span>          
            			</div>
					</th>
					<th style="width: 280px;">
						<div id='ver_mas_larvi' class='hide' >
							<table class="ajaxSorter" border="1px" style="width: 280px; " >
           						<thead >
           							<th style="font-size: 14px; text-align: center" rowspan="2">Pila</th>
           							<th style="font-size: 14px; text-align: center" colspan="2">Datos de Siembra</th>
           							<tr>
           								<th style="font-size: 14px; text-align: center">Fecha</th>
           								<th style="font-size: 14px; text-align: center">Cantidad Actual</th>		
           							</tr>
           						</thead>
           						<tbody>
           							<th style="font-size: 14px; text-align: center"><input readonly="true" size="6%" type="text" name="pilal" id="pilal" style="text-align: center; border: none; color: red;" ></th>
           							<th style="font-size: 12px; text-align: center"><input size="13%" type="text" name="txtFechal" id="txtFechal" class="fecha redondo mUp" readonly="true" style="text-align: center; border: none;" ></th>
           							<th style="font-size: 12px; text-align: center"><input size="3%" type="text" name="canl" id="canl" style="text-align: center; border: none;" ></th>
           						</tbody>
           						<tfoot>
           							<th style="font-size: 14px" colspan="4">Pila Actualmente Vacía?
           								&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Enrique Sánchez"){ ?> disabled="true" <?php } ?> name="rblugarl" type="radio" value="-1" onclick="radios(2,-1)" />Si</td>
										&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Enrique Sánchez"){ ?> disabled="true" <?php } ?> name="rblugarl" type="radio" value="0" onclick="radios(2,0)" />No</td>
           							</th>
           							<tr>
           								<th colspan="3" style="text-align: right">
           									<?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Enrique Sánchez"){ ?>
												<input type="hidden" size="4%"  name="idl" id="idl"/>
												<input style="font-size: 14px" type="submit" id="aceptarl" name="aceptarl" value="Guardar" />
											<?php }?>
											<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(5)" />
											<input type="hidden" size="3%"  name="cancelal" id="cancelal" />
           								</th>
           							</tr>
           						</tfoot>
           					</table>
        				</div>
        			</th>
        		</tr>
		</table>	
		</div>
	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
function cerrar(sel){
	$(this).removeClass('used');
	if(sel==5)	$('#ver_mas_larvi').hide(); else $('#ver_mas_pila').hide(); 	
    
}
$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#txtFechal").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

function radios(radio,dato){
	if(radio==1) { $("#cancela").val(dato);}
	if(radio==2) { $("#cancelal").val(dato);}
}


$("#aceptarl").click( function(){	
	sel=$("#tablasell").val();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/larvicultura/actualizarl", 
			data: "id="+$("#idl").val()+"&fec="+$("#txtFechal").val()+"&can="+$("#canl").val()+"&vacio="+$("#cancelal").val(),
			success: 	
					function(msg){															
						if(msg!=0){			
							if(sel==1){$("#mytablaL1").trigger("update");}											
							if(sel==2){$("#mytablaL2").trigger("update");}
							if(sel==3){$("#mytablaL3").trigger("update");}
							if(sel==4){$("#mytablaL4").trigger("update");}
							if(sel==5){$("#mytablaL5").trigger("update");}
							if(sel==6){$("#mytablaL6").trigger("update");}
							if(sel==7){$("#mytablaL7").trigger("update");}
							if(sel==8){$("#mytablaL8").trigger("update");}
							if(sel==9){$("#mytablaL9").trigger("update");}
							if(sel==10){$("#mytablaL10").trigger("update");}
							if(sel==11){$("#mytablaL11").trigger("update");}
							if(sel==12){$("#mytablaL12").trigger("update");}
							if(sel==13){$("#mytablaL13").trigger("update");}
							if(sel==14){$("#mytablaL14").trigger("update");}
							if(sel==15){$("#mytablaL15").trigger("update");}
							if(sel==16){$("#mytablaL16").trigger("update");}
							$("#mytablaLS").trigger("update");
							$(this).removeClass('used');
							$('#ver_mas_larvi').hide();																
						}else{
							alert("Error con la base de datos o usted no ha actualizado nada");
						}					
					}	
	});	 
});
$(document).ready(function(){ 
	$('#ver_mas_pila').hide();
	$('#ver_mas_larvi').hide();
	$("#tabla_lar1").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','100i','100f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(1);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar2").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','200i','200f'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(2);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},	
	});
	$("#tabla_lar3").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','300i','300f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(3);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar4").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','400i','400f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(4);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar5").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','500i','500f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(5);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar6").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','600i','600f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(6);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar7").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','700i','700f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(7);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar8").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','800i','800f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(8);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar9").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','900i','900f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(9);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar10").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','1000i','1000f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(10);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar11").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','1100i','1100f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(11);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar12").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','1200i','1200f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(12);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar13").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','1300i','1300f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(13);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar14").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','1400i','1400f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(14);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar15").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','1500i','1500f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(15);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_lar16").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalar',
		filters:['hoy','1600i','1600f'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_larvi').show();
        	$("#idl").val($(this).data('numpila'));
        	$("#pilal").val($(this).data('nombre'));
       		$("#txtFechal").val($(this).data('fecsie'));
       		$("#canl").val($(this).data('cansie'));
       		$("#cancelal").val($(this).data('cancelacion'));
       		$("#tablasell").val(16);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugarl]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarl]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugarl]:nth(0)').attr('checked',false);$('input:radio[name=rblugarl]:nth(1)').attr('checked',false);
				 
			}
       	},
	});
	$("#tabla_larsie").ajaxSorter({
		url:'<?php echo base_url()?>index.php/larvicultura/tablalarsie',
		sort:false,	
	});
});
</script>
