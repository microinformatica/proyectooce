<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts-more.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/export-data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/accessibility.js"></script>



<style>        

#containers {
    	max-width: 900px;
    	min-width: 380px;
    	height: 380px;
    	margin: 1em auto;
	}
</style>
<?php 
	//$dia=date("Y-m-d");
	$f=explode("-",$dia);$nummes=(int)$f[1];$mes1="0-Enero-Febrero-Marzo-Abril-Mayo-Junio-Julio-Agosto-Septiembre-Octubre-Noviembre-Diciembre"; 
    $mes1=explode("-",$mes1); 
?>
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<table border="1" width="100%" style="table-layout: fixed" >
		<tr>
			<td >Estanque: <?= $est?> <input size="8%" type="hidden" name="est" id="est" value="<?php echo set_value('est',$est); ?>">
			Ciclo: <?= $cic?> <input size="8%" type="hidden" name="cic" id="cic" value="<?php echo set_value('cic',$cic); ?>">
			Dia: <?= $f[2]." de ".$mes1[$nummes]. " de ".$f[0]?> <input size="8%" type="hidden" name="dia" id="dia" value="<?php echo set_value('dia',$dia); ?>">
			<input size="8%" type="hidden" name="sec" id="sec" value="<?php echo set_value('sec',$sec); ?>"></td>
		</tr>
		<tr>
			<td>FCA</td>
		</tr>
		<tr>
			<td>
				<div id='ver_fca' class='hide' >
				<div class="ajaxSorterDiv" id="tabla_fca" name="tabla_fca">                 			
					<span class="ajaxTable" style="height: 200px; " >
                		<table id="mytablafca" name="mytablafca" class="ajaxSorter" border="1">
                   			<thead>  
                   				   <th data-order = "diasc"></th><th data-order = "fcap">Proy</th><th data-order = "fcaq">Est</th>
                       		</thead>
                   			<tbody style="font-size: 12px; text-align: right">
                   			</tbody>                        	
                		</table>
                	</span> 
             	</div>
             	</div>
             	<div name="grafca" id="grafca" style="width: 100%; height: 200px; margin: 0 auto"></div>
			</td>
		</tr>
		<tr>
			<td>Talla</td>
		</tr>
		<tr>
			<td>
				<div id='ver_tal' class='hide' >
				<div class="ajaxSorterDiv" id="tabla_tal" name="tabla_tal">                 			
					<span class="ajaxTable" style="height: 200px; " >
                		<table id="mytablatal" name="mytablatal" class="ajaxSorter" border="1">
                   			<thead>  
                   				   <th data-order = "diasc"></th><th data-order = "pesp">Proy</th><th data-order = "pesq">Est</th>
                       		</thead>
                   			<tbody style="font-size: 12px; text-align: right">
                   			</tbody>                        	
                		</table>
                	</span> 
             	</div>
             	</div>
				<div name="gratal" id="gratal" style="width: 100%x; height: 200px; margin: 0 auto"></div>
			</td>
		</tr>
		<tr>		
			<td>Inc</td>
		</tr>
		<tr>
			<td>
				<div id='ver_inc' class='hide' >
				<div class="ajaxSorterDiv" id="tabla_inc" name="tabla_inc">                 			
					<span class="ajaxTable" style="height: 200px; " >
                		<table id="mytablainc" name="mytablainc" class="ajaxSorter" border="1">
                   			<thead>  
                   				   <th data-order = "diasc"></th><th data-order = "incpp">Proy</th><th data-order = "incq">Est</th>
                       		</thead>
                   			<tbody style="font-size: 12px; text-align: right">
                   			</tbody>                        	
                		</table>
                	</span> 
             	</div>
             	</div>
				<div name="grainc" id="grainc" style="width: 100%x; height: 200px; margin: 0 auto"></div>
			</td>
		</tr>
		<tr>	
			<td>Alimento</td>
		</tr>
		<tr>
			<td>
				<div id='ver_ali' class='hide' >
				<div class="ajaxSorterDiv" id="tabla_ali" name="tabla_ali">                 			
					<span class="ajaxTable" style="height: 200px; " >
                		<table id="mytablaali" name="mytablaali" class="ajaxSorter" border="1">
                   			<thead>  
                   				   <th data-order = "diasc"></th><th data-order = "ahp">Proy</th><th data-order = "ahq">Est</th>
                       		</thead>
                   			<tbody style="font-size: 12px; text-align: right">
                   			</tbody>                        	
                		</table>
                	</span> 
             	</div>
             	</div>
				<div name="graali" id="graali" style="width: 100%x; height: 200px; margin: 0 auto"></div>
			</td>
		</tr>
		<tr>
			<td>Soya</td>
		</tr>
		<tr>
			<td>$/Kg</td>
		</tr>
		<tr>	
			<td>Biomasa
				<!--
				<input name='bio' id="bio" /><input name='bioha' id="bioha" />
				<input name='bio2' id="bio2" /><input name='bioha2' id="bioha2" /><input name='porbio' id="porbio" />-->
				</td>
		</tr>
		<tr>
			<td > 
			<div name="grabio" id="grabio" style="width: 100%x; height: 200px; margin: 0 auto"></div>
			</td>
		</tr>
	</table>
</div>



<script type="text/javascript">
$(document).ready(function(){
	$('#ver_fca').hide();$('#ver_tal').hide();$('#ver_inc').hide();$('#ver_ali').hide();
	$("#tabla_fca").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gralest/tablafca',  		
		filters:['cic','est','dia','sec'],
		sort:false,
		onSuccess:function(){  
			$('#tabla_fca tbody tr').map(function(){ 	
		    	$("#bio").val($(this).data('bio'));$("#bioha").val($(this).data('bioha'));
		    	$("#bio2").val($(this).data('bio2'));$("#bioha2").val($(this).data('bioha2'));$("#porbio").val($(this).data('porbio'));
		    	biox=$(this).data('porbio');bioha=$(this).data('biohag');
		    });
			Highcharts.chart('grafca', {
   				data: {table: 'mytablafca'},
    			chart: {type: 'spline'},
    			title: {text: '',align: 'left'},
    			 xAxis: { 
    			 			labels: {style: {fontSize: '10px',color: 'red'}
    			 			}
    			 },
    			 yAxis: { title: {text: ''}, min: 0,
        				//labels: {format: 'D'  }		
    					},
    			tooltip: {
        				formatter: function () {
            									return '<b>' + this.series.name + '</b><br/>' +
                								this.point.y + ' ' + this.point.name.toLowerCase();
        										}
    				},
    			series: [{		
        						color:  'blue',
        						dataLabels: { enabled: true,
            								align: 'center',
            								format: '{point.y:.2f}', // one decimal
            								y: 5, // 10 pixels down from the top
        						},
        				  },{
        						color:  'orange',
        						dataLabels: { enabled: true,
        									align: 'center',
            								y: 5, // 10 pixels down from the top
        						},
						  }],	
			});
			
			Highcharts.chart('grabio', {
    			chart: {
				        plotBackgroundColor: null,
				        plotBorderWidth: 0,
				        plotShadow: false
		    	},
			    title: {
			        text: 'Biomasa '+bioha+' kg/ha',
			        align: 'center',
			        verticalAlign: 'middle',
			        y: 60
			    },
			    tooltip: {
			        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			    },
			    accessibility: {
			        point: {
			            valueSuffix: '%'
			        }
			    },
			    plotOptions: {
			        pie: {
			            dataLabels: {
			                enabled: true,
			                distance: -50,
			                style: {
			                    fontWeight: 'bold',
			                    color: 'white'
			                }
			            },
			            startAngle: -90,
			            endAngle: 90,
			            center: ['50%', '75%'],
			            size: '110%'
			        }
			    },
			    series: [{
			        type: 'pie',
			        //name: 'Biomasa',
			        innerSize: '50%',
			        data: [
			            ['Bio', biox],
			            
			            {
			                name: 'Other',
			                y: (100-biox),
			                dataLabels: {
			                    enabled: false
			                }
			            }
			        ]
			    }]
		}); 
    	}, 
    });
    $("#tabla_tal").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gralest/tablatal',  		
		filters:['cic','est','dia','sec'],
		sort:false,
		onSuccess:function(){  
			Highcharts.chart('gratal', {
   				data: {table: 'mytablatal'},
    			chart: {type: 'spline'},
    			title: {text: '',align: 'left'},
    			xAxis: {	labels: {style: {fontSize: '10px',color: 'red'}	} },
    			yAxis: { title: {text: ''}, min: 0, },
    			tooltip: {
        					formatter: function () {
            									return '<b>' + this.series.name + '</b><br/>' +
                								this.point.y + ' ' + this.point.name.toLowerCase();
        										}
    					},
    			series: [{		
        						color:  'blue',
        						dataLabels: { enabled: true,
            								align: 'center',
            								format: '{point.y:.2f}', // one decimal
            								y: 5, // 10 pixels down from the top
        						},
        				  },{
 								type: 'column',
        						color:  'orange',
        						dataLabels: { enabled: true,
        									align: 'center',
            								y: 5, // 10 pixels down from the top
        						},
						  }],	
			});
    	}, 
    });
    $("#tabla_inc").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gralest/tablainc',  		
		filters:['cic','est','dia','sec'],
		sort:false,
		onSuccess:function(){  
			Highcharts.chart('grainc', {
   				data: {table: 'mytablainc'},
    			chart: {type: 'spline'},
    			title: {text: '',align: 'left'},
    			xAxis: {	labels: {style: {fontSize: '10px',color: 'red'}	} },
    			yAxis: { title: {text: ''}, min: 0, },
    			tooltip: {
        					formatter: function () {
            									return '<b>' + this.series.name + '</b><br/>' +
                								this.point.y + ' ' + this.point.name.toLowerCase();
        										}
    					},
    			series: [{		
        						color:  'blue',
        						dataLabels: { enabled: true,
            								align: 'center',
            								format: '{point.y:.2f}', // one decimal
            								y: 5, // 10 pixels down from the top
        						},
        				  },{
        						color:  'orange',
        						dataLabels: { enabled: true,
        									align: 'center',
            								y: 5, // 10 pixels down from the top
        						},
						  }],	
			});
    	}, 
    });
    $("#tabla_ali").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gralest/tablaali',  		
		filters:['cic','est','dia','sec'],
		sort:false,
		onSuccess:function(){  
			Highcharts.chart('graali', {
   				data: {table: 'mytablaali'},
    			chart: {type: 'spline'},
    			title: {text: '',align: 'left'},
    			xAxis: {	labels: {style: {fontSize: '10px',color: 'red'}	} },
    			yAxis: { title: {text: ''}, min: 0, },
    			tooltip: {
        					formatter: function () {
            									return '<b>' + this.series.name + '</b><br/>' +
                								this.point.y + ' ' + this.point.name.toLowerCase();
        										}
    					},
    			 plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },		
    			series: [{		type: 'area',
        						color:  'blue',
        						dataLabels: { enabled: true,
            								align: 'center',
            								format: '{point.y:.2f}', // one decimal
            								y: 5, // 10 pixels down from the top
        						},
        				 },{	
        						color:  'orange',
        						dataLabels: { enabled: true,
        									align: 'center',
            								y: 5, // 10 pixels down from the top
        						},
						  }],	
			});
    	}, 
    });
 	
});
</script>