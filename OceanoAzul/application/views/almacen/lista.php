<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li ><a href="#recibir"><img src="<?php echo base_url();?>assets/images/menu/ecommerce.png" width="24" height="24" border="0"> 
			<?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ 
				echo "Solicitudes Pendientes de Envío"; 
			 } else{ 
				echo "Solicitudes Pendientes de Recibir";
			}?></a></li>
		<li><a href="#requisiciones"><img src="<?php echo base_url();?>assets/images/menu/oc.png" width="24" height="24" border="0"> Requisiciones </a></li>			
		
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px"  >
		<div id="recibir" class="tab_content" style="margin-top: -5px">
				<div class="ajaxSorterDiv" id="tabla_almdia" name="tabla_almdia" align="left"  >             	
            	<span class="ajaxTable" style=" height: 345px;  background-color: white; ">
                    <table id="mytablaAdia" name="mytablaAdia" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <th data-order = "FechaS1" >Día</th>                            
                            <th data-order = "Requisicion">Requisición</th>
                            <th data-order = "hora" >Hora</th>
                            <th data-order = "CantidadS" >Cantidad</th>
                            <th data-order = "UnidadS" >Unidad</th>                                                       
                            <th data-order = "DescripcionS" >Descripción -( Observaciones )-</th>
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                 </span>
                 <div class="ajaxpager" style="margin-top: -15px">        
                    <ul class="order_list" style="width: 450px">En Total: <input size="2%"  type="text" name="solp" id="solp" style="text-align: left;  border: 0; background: none">
                    	Buscar del Día <input size="12%" type="text" name="txtFI" id="txtFI" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                &nbsp; al <input size="12%" type="text" name="txtFF" id="txtFF"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                    </ul>                          
                    <form method="post" action="<?= base_url()?>index.php/almacen/pdfrepd" >
                    	 
                    	<img style="cursor: pointer" style="margin-top: 6px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                    </form>   
 				</div>
 				
 			</div>
 			<div id='ver_mas_pedi' class='hide' >
 			<table border="2px" style="width: 500px; margin-top: -30px">
 						<thead style="background-color: #DBE5F1">
 							<th colspan="4">Procesar Datos en calidad de Recibido:
 								&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="-1" onclick="radios(1,-1)" />Si</td>
								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="0" onclick="radios(1,0)" />No</td>									
								&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="1" onclick="radios(1,1)" />Incompleto</td>
 								<input type="hidden" size="3%"  name="cans" id="cans" />
 								<input type="hidden" size="3%"  name="id" id="id" />
 								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="aceptard" name="aceptard" value="Guardar" />
 								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="borrard" name="borrard" value="Borrar" />
 								&nbsp;&nbsp;<input style="font-size: 12px" type="submit" onclick="cerrar(1)" id="x" name="x" value="Cerrar" />
 							</th>
 						</thead>
 						<tbody style="background-color: #F7F7F7">
 							<tr>
 								<th style="text-align: left">Cantidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="can" id="can" style="text-align: center;  border: 0"></th>
 								<th style="text-align: left">Unidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="uni" id="uni" style="text-align: center;  border: 0"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Descripcion</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="des" id="des" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Observaciones</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="obs" id="obs" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 						</tbody>
 						
 				</table>
 			</div>
		</div>
		<div id="requisiciones" class="tab_content" style="margin-top: -5px" >
			<div class="ajaxSorterDiv" id="tabla_almreq" name="tabla_almreq" align="left"  >  
		    	No:<select name="req" id="req" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
          							$data['result']=$this->almacen_model->verRequisicion();
									foreach ($data['result'] as $row):?>
           							<option value="<?php echo $row->Nr;?>" ><?php echo $row->Requisicion;?></option>
           							<?php endforeach;
           						?>    
           			</select> 
           			<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="reqn" name="reqn" value="Nueva Requisicion" />
            	<span class="ajaxTable" style=" height: 345px; width: 910px;background-color: white; margin-top: 1px">
                    <table id="mytablaAreq" name="mytablaAreq" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <th data-order = "FechaS1" >Día</th>                            
                            <th data-order = "hora" >Hora</th>
                            <th data-order = "CantidadS" >Cantidad</th>
                            <th data-order = "UnidadS" >Unidad</th>                                                       
                            <th data-order = "DescripcionS1" >Descripción</th>
                            <th data-order = "FechaR">Recibido</th>
                            <th data-order = "Obs" >Observaciones</th>
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                 </span>
                 <div class="ajaxpager" style="margin-top: -15px">        
                    <ul class="order_list"><input size="2%"  type="text" name="solpr" id="solpr" style="text-align: left;  border: 0; background: none">
                    	<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="nuereq" name="nuereq" value="Agregar" />
                    </ul>                          
                    <form method="post" action="<?= base_url()?>index.php/almacen/pdfrepr" >
                    	<input size="3%" type="hidden" name="requisicion" id="requisicion" /> 
                    	<img style="cursor: pointer" style="margin-top: 6px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                    </form>   
 				</div>
 				
 			</div>
 			<div id='ver_mas_req' class='hide' >
 			<table border="2px" style="width: 500px; margin-top: -30px">
 						<thead style="background-color: #DBE5F1">
 							<th colspan="4">Procesar Datos en calidad de Recibido:
 								&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="rblugarr" type="radio" value="1" onclick="radios(2,-1)" />Si</td>
								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="rblugarr" type="radio" value="0" onclick="radios(2,0)" />No</td>									
								&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="rblugarr" type="radio" value="-1" onclick="radios(2,1)" />Incompleto</td>
 								<input type="hidden" size="3%" name="cansr" id="cansr" />
 								<input type="hidden" size="3%" name="idr" id="idr" />
 								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="aceptarr" name="aceptarr" value="Guardar" />
 								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="borrarr" name="borrarr" value="Borrar" />
 								&nbsp;&nbsp;<input style="font-size: 12px" type="submit" onclick="cerrar(2)" id="x" name="x" value="Cerrar" />
 							</th>
 						</thead>
 						<tbody style="background-color: #F7F7F7">
 							<tr>
 								<th style="text-align: left">Cantidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="canr" id="canr" style="text-align: center;  border: 0"></th>
 								<th style="text-align: left">Unidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="unir" id="unir" style="text-align: center;  border: 0"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Descripcion</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="desr" id="desr" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Observaciones</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="obsr" id="obsr" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 						</tbody>
 						
 				</table>
 			</div>
 			<div id='ver_mas_reqn' class='hide' >
 			<table border="2px" style="width: 500px; margin-top: -30px">
 						<thead style="background-color: #DBE5F1">
 							<th colspan="4">Agregar a Requisición
 								<input type="hidden" size="3%" name="id1" id="id1" />
 								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="aceptar1" name="aceptar1" value="Guardar" /> 								
 								&nbsp;&nbsp;<input style="font-size: 12px" type="submit" onclick="cerrar(3)" id="x" name="x" value="Cerrar" />
 							</th>
 						</thead>
 						<tbody style="background-color: #F7F7F7">
 							<tr>
 								<th style="text-align: left">Cantidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="can1" id="can1" style="text-align: center;  border: 0"></th>
 								<th style="text-align: left">Unidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="uni1" id="uni1" style="text-align: center;  border: 0"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Descripcion</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="des1" id="des1" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Observaciones</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="obs1" id="obs1" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 						</tbody>
 						
 				</table>
 			</div>
			
 		</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#txtFI").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFF").datepicker( "option", "minDate", selectedDate );
		$('#tabla_almdia').trigger('update');
	}
});
$("#txtFF").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFI").datepicker( "option", "maxDate", selectedDate );
		$('#tabla_almdia').trigger('update');
	}
});	

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});

function radios(radio,dato){
	if(radio==1) { $("#cans").val(dato);}
	if(radio==2) { $("#cansr").val(dato);}
}
function cerrar($sel){
	$(this).removeClass('used');
	if($sel==1){ $('#ver_mas_pedi').hide();}
	if($sel==2){ $('#ver_mas_req').hide(); }else{ $('#ver_mas_reqn').hide();}
	if($sel==3){ $('#ver_mas_reqn').hide(); }
}
$("#req").change( function(){		
	$(this).removeClass('used');
	$('#ver_mas_req').hide();		
	$('#ver_mas_reqn').hide();
	$("#requisicion").val($("#req").val());
	return true;
});
$("#nuevod").click( function(){	
	$("#id").val('');			
    $("#can").val('');
    $("#uni").val('');
    $("#des").val('');
    $("#obs").val('');
    $("#cans").val('');
 return true;
})
$("#nuevor").click( function(){	
	$("#idr").val('');			
    $("#canr").val('');
    $("#unir").val('');
    $("#desr").val('');
    $("#obsr").val('');
    $("#cansr").val('');
 return true;
})
$("#nuereq").click( function(){
	$(this).removeClass('used');
	$('#ver_mas_reqn').show();	
	$("#id1").val('');			
    $("#can1").val('');
    $("#uni1").val('');
    $("#des1").val('');
    $("#obs1").val('');
    $("#can1").focus(); 
    $(this).removeClass('used');
   	$('#ver_mas_req').hide();   
 return true;
})
$("#reqn").click( function(){	
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/almacen/agregarreq", 
			//data: "can="+$("#can1").val()+"&uni="+$("#uni1").val()+"&des="+$("#des1").val()+"&obs="+$("#obs1").val()+"&req="+$("#req").val(),
			success: 	
					function(msg){															
						if(msg!=0){														
								$("#mytablaAreq").trigger("update");
								$("#mytablaAdia").trigger("update");
								//$("#req").trigger("update");
								//$(this).removeClass('used');
								//$('#ver_mas_reqn').hide();
								alert("Requisicion Nueva Agregada correctamente");										
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
	});
});
$("#aceptar1").click( function(){	
	can=$("#can1").val();
	des=$("#des1").val();
	if(can!=''){
	 if(des!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almacen/agregarr", 
				data: "can="+$("#can1").val()+"&uni="+$("#uni1").val()+"&des="+$("#des1").val()+"&obs="+$("#obs1").val()+"&req="+$("#req").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaAreq").trigger("update");
								$("#mytablaAdia").trigger("update");
								$("#nuevor").click();
								$(this).removeClass('used');
								$('#ver_mas_reqn').hide();
								alert("Datos Agregados correctamente");										
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita registrar Descripcion para poder actualizar datos");	
		$("#des1").focus();			
		return false;
	}
	}else{
		alert("Error: Necesita registrar Cantidad para poder actualizar datos");
		$("#can1").focus();				
		return false;
	}	 
});
$("#aceptarr").click( function(){	
	can=$("#canr").val();
	des=$("#desr").val();
	if(can!=''){
	 if(des!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almacen/actualizarr", 
				data: "id="+$("#idr").val()+"&can="+$("#canr").val()+"&uni="+$("#unir").val()+"&des="+$("#desr").val()+"&obs="+$("#obsr").val()+"&cans="+$("#cansr").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaAreq").trigger("update");
								$("#mytablaAdia").trigger("update");
								$("#nuevor").click();
								$(this).removeClass('used');
								$('#ver_mas_req').hide();
								alert("Datos Actualizados correctamente");										
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita registrar Descripcion para poder actualizar datos");	
		$("#desr").focus();			
		return false;
	}
	}else{
		alert("Error: Necesita registrar Cantidad para poder actualizar datos");
		$("#canr").focus();				
		return false;
	}	 
});
$("#borrarr").click( function(){	
	numero=$("#idr").val();
	if(numero!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almacen/borrarr", 
				data: "id="+$("#idr").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaAreq").trigger("update");
								$("#mytablaAdia").trigger("update");
								$("#nuevor").click();
								$(this).removeClass('used');
								$('#ver_mas_req').hide();
								alert("Datos eliminados correctamente");										
																								
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder actualizar datos");				
		return false;
	}	 
});
$("#aceptard").click( function(){	
	can=$("#can").val();
	des=$("#des").val();
	if(can!=''){
	 if(des!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almacen/actualizard", 
				data: "id="+$("#id").val()+"&can="+$("#can").val()+"&uni="+$("#uni").val()+"&des="+$("#des").val()+"&obs="+$("#obs").val()+"&cans="+$("#cans").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaAdia").trigger("update");
								$("#mytablaAreq").trigger("update");
								alert("Datos Actualizados correctamente");										
								$("#nuevod").click();
								$(this).removeClass('used');
								$('#ver_mas_pedi').hide();																
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita registrar Descripcion para poder actualizar datos");	
		$("#des").focus();			
		return false;
	}
	}else{
		alert("Error: Necesita registrar Cantidad para poder actualizar datos");
		$("#can").focus();				
		return false;
	}	 
});
$("#borrard").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almacen/borrard", 
				data: "id="+$("#id").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaAdia").trigger("update");
								$("#mytablaAreq").trigger("update");
								alert("Datos eliminados correctamente");										
								$("#nuevod").click();
								$(this).removeClass('used');
								$('#ver_mas_pedi').hide();																
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder actualizar datos");				
		return false;
	}	 
});
$(document).ready(function(){ 
	$(this).removeClass('used');
   	$('#ver_mas_pedi').hide();
	$("#tabla_almdia").ajaxSorter({
		url:'<?php echo base_url()?>index.php/almacen/tablad',  		
		filters:['txtFI','txtFF'],
		active:['CanS',1],
		multipleFilter:true, 
    	sort:false,
    	onRowClick:function(){
    		$("#id").val($(this).data('ns'));			
    		$("#can").val($(this).data('cantidads'));
    		$("#uni").val($(this).data('unidads'));
    		$("#des").val($(this).data('des1'));
    		$("#obs").val($(this).data('obs'));
    		$("#cans").val($(this).data('cans'));
    		ser=$(this).data('cans');
			if(ser==-1 || ser==0 || ser==1){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				if(ser==1){ $('input:radio[name=rblugar]:nth(2)').attr('checked',true); }
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				$('input:radio[name=rblugar]:nth(2)').attr('checked',false); 
			}
			$(this).removeClass('used');
   			$('#ver_mas_pedi').show();
    	},   
    	onSuccess:function(){
    		$('#tabla_almdia tbody tr').map(function(){
    	   		$("#solp").val($(this).data('totp'));	
    	   });
    	   $('#tabla_almdia tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })
    	},          
	});
	
	
	$("#requisicion").val($("#req").val());
	$(this).removeClass('used');
   	$('#ver_mas_req').hide();
   	$('#ver_mas_reqn').hide();
	$("#tabla_almreq").ajaxSorter({
		url:'<?php echo base_url()?>index.php/almacen/tablar',  		
		filters:['req'],
		active:['Recibido',1],
		//multipleFilter:true, 
    	sort:false,
    	onRowClick:function(){
    		$(this).removeClass('used');
   			$('#ver_mas_reqn').hide();
    		$("#idr").val($(this).data('ns'));			
    		$("#canr").val($(this).data('cantidads'));
    		$("#unir").val($(this).data('unidads'));
    		$("#desr").val($(this).data('descripcions'));
    		$("#obsr").val($(this).data('obs'));
    		$("#cansr").val($(this).data('cans'));
    		ser=$(this).data('cans');
			if(ser==-1 || ser==0 || ser==1){
				if(ser==-1){ $('input:radio[name=rblugarr]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugarr]:nth(1)').attr('checked',true); }
				if(ser==1){ $('input:radio[name=rblugarr]:nth(2)').attr('checked',true); }
			}else{
				$('input:radio[name=rblugarr]:nth(0)').attr('checked',false);$('input:radio[name=rblugarr]:nth(1)').attr('checked',false);
				$('input:radio[name=rblugarr]:nth(2)').attr('checked',false); 
			}
			$(this).removeClass('used');
   			$('#ver_mas_req').show();
    	},   
    	onSuccess:function(){
    		$('#tabla_almreq tbody tr').map(function(){
    	   		$("#solpr").val($(this).data('totp'));	
    	   });
    	   $('#tabla_almreq tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','center'); })
    	},          
	});
}); 
</script>
