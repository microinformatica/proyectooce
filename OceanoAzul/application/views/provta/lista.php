<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts-more.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/export-data.js"></script>



<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
	
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}

#export_datagz { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datavr { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }

#containers {
    	max-width: 900px;
    	min-width: 380px;
    	height: 380px;
    	margin: 1em auto;
	}
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<?php if($usuario=="Antonio Hernandez"){ ?>  
		<li><a href="#costos" style="font-size: 20px"><img src="<?php echo base_url();?>assets/images/menu/camarones.png" width="20" height="20" border="0"> Analísis de Rentabilidad </a></li>
		<?php }?>
		<?php if($usuario=="Salvador Partida" || $usuario=="Antonio Hernandez" || $usuario=="Joel Lizarraga A."){ ?>   
		<li><a href="#sueldos" style="font-size: 20px"><img src="<?php echo base_url();?>assets/images/menu/cargo.png" width="20" height="20" border="0"> Sueldos </a></li>
		<li><a href="#combustible" style="font-size: 20px"><img src="<?php echo base_url();?>assets/images/menu/combustible.png" width="20" height="20" border="0"> Combustible </a></li>
		<li><a href="#otros" style="font-size: 20px"><img src="<?php echo base_url();?>assets/images/menu/combustible.png" width="20" height="20" border="0"> Otros </a></li>
		<?php }?>
		<?php if($usuario=="Zuleima Benitez" || $usuario=="Efrain Lizárraga" || $usuario=="Antonio Hernandez"){ ?>
		<li><a href="#prevta" style="font-size: 20px"><img src="<?php echo base_url();?>assets/images/menu/coades.png" width="20" height="20" border="0"> Bordo </a></li>
		<?php }?>
		<?php if($usuario=="Zuleima Benitez" || $usuario=="Efrain Lizárraga"){ ?>
		<li><a href="#prevtar" style="font-size: 20px"><img src="<?php echo base_url();?>assets/images/menu/coades.png" width="20" height="20" border="0"> Clientes </a></li>
		<?php }?>
		</strong>
		<?php if($usuario=="Salvador Partida" || $usuario=="Antonio Hernandez" || $usuario=="Joel Lizarraga A."){ ?>  
		<select name="cicgra" id="cicgra" style="font-size: 10px; height: 25px; margin-top: 1px " >
			<option value="22">2022</option>
			<option value="21">2021</option><option value="20">2020</option><option value="19">2019</option>
        </select>
        <?php }?>   											
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 630px" >
		<?php if($usuario=="Antonio Hernandez"){ ?> 	  
 		<div id="costos" class="tab_content" style="margin-top: -15px" >
 			<!--<div class="ajaxSorterDiv" id="tabla_gralz" name="tabla_gralz" style="margin-top: 1px"></div>-->   
 			<div class="ajaxSorterDiv" id="tabla_gralz" name="tabla_gralz" style=" margin-left: -10px;  margin-top: 1px;width:955px;">
             	  <div class="ajaxpager" style="margin-top: -5px;" > 
             	  	<form action="<?= base_url()?>index.php/provta/reportet" method="POST" >
            		<ul class="order_list" style="width: 860px; text-align: left; height: 25px" >
            			<select name="cicact" id="cicact" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:5px;">
      							<?php $ciclof=19; $actual=date("y"); //$actual+=1;
									while($actual >= $ciclof){?>
										<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
										<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            						<?php  $actual-=1; } ?>      									
    						  </select>
            			Reporte General - 
            			Granja
            			<select name="numgrabgz" id="numgrabgz" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   							<option value="4" >OA-Ahome</option>
   							<!--
   							<option value="0" >Seleccione</option>
   							<option value="2" >OA-Kino</option>-->
   						</select>
            			Sección	
            			<select name="seccgz" id="seccgz" style="font-size: 12px; height: 25px;margin-top: 1px;  " >	</select>
 						<input type="hidden" id="filest" name="filest" style="width: 40px;">
						Pls:<input id="prepl" name="prepl" style="border: none; background-color: lightblue; width: 40px; font-size: 10px" readonly="true" />
            			Ali:<input id="preal" name="preal" style="border: none; background-color: lightblue; width: 40px; font-size: 10px" readonly="true" />
            			Com:<input id="preco" name="preco" style="border: none; background-color: lightblue; width: 55px; font-size: 10px" readonly="true" />
            				<input type="hidden" value="0" id="tc" name="tc" style="border: none; background-color: lightblue; width: 40px; font-size: 10px"/>
            			<img style="cursor: pointer" style="margin-top: -5px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            <input type="hidden" name="tabla" value ="" class="htmlTable"/>
        	           
            		</ul>
            	</form>	
 				</div>  
             	<span class="ajaxTable" style="height: 570px; background-color: white; width:950px; margin-top: 1px" >
            	   	<table id="mytablagralz" name="mytablagralz" class="ajaxSorter" border="1" style="text-align: center" >
                       <thead >
                       		
                       		<tr style="font-size: 10px">
                       			<th rowspan="2" data-order = "pisg">Est</th>
                       			<!--<th rowspan="2" data-order = "hasg">Has</th>-->
                       			<!--<th rowspan="2" data-order = "dc">DC</th>-->
                       			<!--<th rowspan="2" data-order = "ali">Alimento</th>-->	
                       			<th colspan="1">Postlarvas </br></th>
                       			<th colspan="2">Alimento </br></th>
                       			<th colspan="1">Sueldos</th>	
                       			
                       			<th colspan="1">Combustible </br>
                       				
                       			</th>
                       			<th style="text-align: center">Otros</th>
                       			<th style="text-align: center">Depreciación</th>
                       			<th style="text-align: center"></th>
                       			<th colspan="2">Kg. Actuales</th>                       			
                       			<!--<th colspan="4">Venta Sin Cabeza</th>-->
                       			<th colspan="3">Camarón Bordo</th>
                       			<th style="text-align: center"> </th>
                       			<th colspan="2" style="text-align: center">RENTABILIDAD</th>
                       			<th colspan="2" style="text-align: center">Cosecha Real Actual</th>
                       		</tr>
                       		<tr style="font-size: 10px">
                       			<!--<th data-order = "pp1">PP</th>
                       			<th data-order = "kgs1">Kgs</th>-->
                       			<!--<th data-order = "orgg">Orgs</th>-->
                       			<!--<th data-order = "prepls">Precio</th>-->
                       			<th data-order = "imppls">Importe</th>
                       			<th data-order = "kgsali">Kgs</th>
                       			<th data-order = "impali">Importe</th>
                       			<th data-order = "impsue">Importe</th>
                       			
                       			<!--<th data-order = "preali">Precio</th>-->
                       			
                       			<!--<th data-order = "ltscom">Lts</th>
                       			<th data-order = "precom">Precio</th>-->
                       			<th data-order = "impcom">Importe</th>
                       			<th data-order = "impotc">Importe</th>
                       			<th data-order = "impdep">Importe</th>
                       			<th data-order = "costo" style="text-align: center">COSTO</th>
                       			<th data-order = "ppkgs">grs</th>
                       			<th data-order = "kgskgs">Kgs</th><!--
                       			<th data-order = "tsc">Talla</th>
                       			<th data-order = "prekgssc">PB</th>
                       			<th data-order = "prevtasc">grs+PB</th>
                       			<th data-order = "impkgssc">Importe</th>-->
                       			<th data-order = "tcc">Talla</th>
                       			<th data-order = "prekgscc">PB</th>
                       			<th data-order = "prevtacc">grs+PB</th>
                       			<th data-order = "venta" style="text-align: center">VENTA</th>
                       			<th data-order = "renta">%</th>
                       			<th data-order = "imprenta">Importe</th>
                       			<th data-order = "kgsbor">Kgs</th>
                       			<th data-order = "vtabor">Importe</th>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 11px; text-align: center">
                       	</tbody>                        	     	
                   	</table>
                </span> 
 			</div>
 			<button id="export_datagz" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
				<form id="data_tablesgz" action="<?= base_url()?>index.php/bordo/pdfrepcosg" method="POST">
					<input type="hidden" name="tablag" id="html_gralz"/> <input type="hidden" name="gra1" id="gra1"/>
				</form>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#export_datagz').click(function(){									
								$('#html_gralz').val($('#mytablagralz').html());
								$('#data_tablesgz').submit();
								$('#html_gralz').val('');
								
						});
					});
				</script>

 		</div>
 		<?php } ?>
 		<?php if($usuario=="Salvador Partida" || $usuario=="Antonio Hernandez" || $usuario=="Joel Lizarraga A."){ ?> 
 		<div id="sueldos" class="tab_content"  >	
			<div style="width: 800px">
			<table style="margin-left: -60px" >
				<tr>
					<th>
						<table class="ajaxSorter" border="2" style="width: 280px;" >
							<thead>	<th>Semana</th><th>Fecha</th><th>Importe</th></thead>
							<tbody>
									<th>    
										<input type="hidden" readonly="true" size="2%"  type="text" name="idsem" id="idsem">	
										<select name="numsem" id="numsem" style="font-size: 10px; height: 25px; margin-top: 1px " >
											<option value="0">Sel</option>
											<?php	
          										$cont=0;
												while($cont<52){ $cont+=1;?>
           										<option style="font-size: 14px" value ="<?php echo $cont;?>" ><?php echo $cont;?></option>
           									<?php } ?>
           								</select>			             
                    				</th>	
                   					 <th style="text-align: center">
										<input size="10%" type="text" name="fecsem" id="fecsem" class="fecha redondo mUp" readonly="true" style="text-align: center;font-size: 14px" >
									</th>
									<th>
										<input size="12%" type="text" name="impsem" id="impsem" style="text-align: right; margin-left: 1px; font-size: 14px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" >
									</th>
									
									
							</tbody>
							<tr>
								<th colspan="3">
									<textarea name="obssem" id="obssem" rows="3" cols="57" style="resize: none" placeholder="Observaciones"></textarea>
								</th>
							</tr>
							
							<tfoot>	
								<th colspan="3">
									<?php if($usuario=="Salvador Partida"){ ?> 
									<input type="submit" id="nuevos" name="nuevos" value="Nuevo"  />
									<input type="submit" id="aceptars" name="aceptars" value="Guardar" />
									<input type="submit" id="borrars" name="borrars" value="Borrar" />
									<?php } ?>
								</th>
							</tfoot>	
						</table>
					</th>
					<th>
						<table border="0" style="margin-left: 15px; margin-top: 1px; ">
							<tr>
				 				<th >				 		
                 					<div class="ajaxSorterDiv" id="tabla_sue" name="tabla_sue" style="width: 530px"  >   <span class="ajaxTable" style="height: 570px; margin-top: 1px; " >
                    						<table id="mytablaSue" name="mytablaSue" class="ajaxSorter" border="1"  >
                        						<thead>                            
			                            			<th data-order = "numsem" >Semana</th>
            			                			<th data-order = "fecsem1" >Fecha</th>
                        			    			<th data-order = "impsem"  >Importe Bruto</th>
                        			    			<th data-order = "obssem"  >Observaciones</th>
			                            		</thead>
                        						<tbody title="Seleccione para realizar cambios" style="font-size: 12px">
                        						</tbody>                        	
                    						</table>
                						</span> 
             			 				<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
											<form action="<?= base_url()?>index.php/aqpgranja/pdfrepbio" method="POST" >							
    	    	            					<ul class="order_list" style="width: 150px; margin-top: 1px">
    	    	            						
    	    	            					</ul>
    	    	            					<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    				<input type="hidden" name="tablabio" value ="" class="htmlTable"/> 
											</form>   
                						</div>       	                	
	                				</div>	                	
            					</th>
            				</tr>
						</table>	
					</th>
				</tr>
			</table>
			</div>
		</div>	
		<div id="combustible" class="tab_content"  >	
			<div style="width: 800px">
			<table style="margin-left: -60px" >
				<tr>
					<th>
						<table class="ajaxSorter" border="2" style="width: 280px;" >
							<thead>	<th>Fecha</th><th>Tipo</th><th>Factura</th></thead>
							<tbody>
									<th>    
										<input type="hidden" readonly="true" size="2%"  type="text" name="iddie" id="iddie">	
										<input size="10%" type="text" name="fecdie" id="fecdie" class="fecha redondo mUp" readonly="true" style="text-align: center;font-size: 14px" >
									</th>	
                   					 <th style="text-align: center">
										<select name="tipdie" id="tipdie" style="font-size: 10px; height: 25px; margin-top: 1px " >
											<option value="1">Factura</option>
											<option value="2">Nota de Crédito</option>
           								</select>
									</th>
									<th>
										<input size="12%" type="text" name="facdie" id="facdie" style="margin-left: 1px; font-size: 14px" >
									</th>
									
									
							</tbody>
							<tr>
								<th colspan="3">
									Litros <input size="12%" type="text" name="candie" id="candie" style="text-align: right; margin-left: 1px; font-size: 14px" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" >
									Precio $ <input size="12%" type="text" name="predie" id="predie" style="text-align: right; margin-left: 1px; font-size: 14px" >
								</th>
							</tr>
							<tfoot>	
								<th colspan="3">
									<?php if($usuario=="Salvador Partida"){ ?>
									<input type="submit" id="nuevoc" name="nuevoc" value="Nuevo"  />
									<input type="submit" id="aceptarc" name="aceptarc" value="Guardar" />
									<input type="submit" id="borrarc" name="borrarc" value="Borrar" />
									<?php } ?>
								</th>
							</tfoot>	
						</table>
					</th>
					<th>
						<table border="0" style="margin-left: 15px; margin-top: 1px; ">
							<tr>
				 				<th >				 		
                 					<div class="ajaxSorterDiv" id="tabla_com" name="tabla_com" style="width: 570px"  >                 			
										<span class="ajaxTable" style="height: 570px; margin-top: 1px; " >
                    						<table id="mytablaCom" name="mytablaCom" class="ajaxSorter" border="1" style="width: 543px" >
                        						<thead>                            
			                            			<th data-order = "fecdie1" >Fecha</th>
			                            			<th data-order = "tipdie1" >Tipo</th>
			                            			<th data-order = "facdie" >Factura</th>
            			                			<th data-order = "candie"  >Litros</th>
                        			    			<th data-order = "predie1"  >Precio</th>
                        			    			<th data-order = "impdie"  >Importe</th>
			                            		</thead>
                        						<tbody title="Seleccione para realizar cambios" style="font-size: 14px">
                        						</tbody>                        	
                    						</table>
                						</span> 
             			 				<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
											<form action="<?= base_url()?>index.php/aqpgranja/pdfrepbio" method="POST" >							
    	    	            					<ul class="order_list" style="width: 150px; margin-top: 1px">
    	    	            					</ul>
    	    	            					<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    				<input type="hidden" name="tablabio" value ="" class="htmlTable"/> 
											</form>   
                						</div>       	                	
	                				</div>	                	
            					</th>
            				</tr>
						</table>	
					</th>
				</tr>
			</table>
			</div>
		</div>
		<div id="otros" class="tab_content"  >	
			<div style="width: 800px">
			<table style="margin-left: -60px" >
				<tr>
					<th>
						<table class="ajaxSorter" border="2" style="width: 280px;" >
							<thead>	<th>Fecha</th><th>Importe</th></thead>
							<tbody>
									 <th style="text-align: center">
                   					 	<input type="hidden" readonly="true" size="2%"  type="text" name="idotc" id="idotc">
										<input size="10%" type="text" name="fecotc" id="fecotc" class="fecha redondo mUp" readonly="true" style="text-align: center;font-size: 14px" >
									</th>
									<th>
										<input size="12%" type="text" name="impotc" id="impotc" style="text-align: right; margin-left: 1px; font-size: 14px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" >
									</th>
									
									
							</tbody>
							<tr>
								<th colspan="2">
									<textarea name="obsotc" id="obsotc" rows="3" cols="57" style="resize: none" placeholder="Observaciones"></textarea>
								</th>
							</tr>
							
							<tfoot>	
								<th colspan="2">
									<?php if($usuario=="Salvador Partida"){ ?> 
									<input type="submit" id="nuevoo" name="nuevoo" value="Nuevo"  />
									<input type="submit" id="aceptaro" name="aceptaro" value="Guardar" />
									<input type="submit" id="borraro" name="borraro" value="Borrar" />
										<?php } ?>
								</th>
							</tfoot>	
						</table>
					</th>
					<th>
						<table border="0" style="margin-left: 15px; margin-top: 1px; ">
							<tr>
				 				<th >				 		
                 					<div class="ajaxSorterDiv" id="tabla_otc" name="tabla_otc" style="width: 570px"  >	<span class="ajaxTable" style="height: 570px; margin-top: 1px; " >
                    						<table id="mytablaOtc" name="mytablaOtc" class="ajaxSorter" border="1">
                        						<thead>                            
			                            			<th data-order = "fecotc1" style="width: 80px" >Fecha</th>
                        			    			<th data-order = "impotc" style="width: 100px"   >Importe</th>
                        			    			<th data-order = "obsotc"  >Observaciones</th>
			                            		</thead>
                        						<tbody title="Seleccione para realizar cambios" style="font-size: 12px">
                        						</tbody>                        	
                    						</table>
                						</span> 
             			 				<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
											<form action="<?= base_url()?>index.php/aqpgranja/pdfrepbio" method="POST" >							
    	    	            					<ul class="order_list" style="width: 150px; margin-top: 1px">
    	    	            					</ul>
    	    	            					<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    				<input type="hidden" name="tablabio" value ="" class="htmlTable"/> 
											</form>   
                						</div>       	                	
	                				</div>	                	
            					</th>
            				</tr>
						</table>	
					</th>
				</tr>
			</table>
			</div>
		</div>
		<?php } ?>
		<?php if($usuario=="Zuleima Benitez" || $usuario=="Efrain Lizárraga" || $usuario=="Antonio Hernandez"){ ?>
		<div id="prevta" class="tab_content"  >	
			<div style="width: 800px">
			<div id='ver_precios' class='hide'  >
			<table border="1" style="margin-left: -60px" >
				<tr>
					<td>
								<div class="ajaxSorterDiv" id="tabla_prevta" name="tabla_prevta" style="width: 530px"  >                 			
										<div class="ajaxpager" style=" font-size: 12px;"> 
											<ul class="order_list" style="width: 460px; margin-top: 1px; text-align: ">
    	    	            						Fecha <input size="8%" type="text" name="fecpre" id="fecpre" class="fecha redondo mUp" readonly="true" style="text-align: center;font-size: 14px" >
													Gramos <input size="2%" type="text" name="idgrsp" id="idgrsp" style="margin-left: 1px; font-size: 14px" >
													Precio Base <input size="2%" type="text" name="pcc" id="pcc" style="margin-left: 1px; font-size: 14px" >
													<?php if($usuario=="Zuleima Benitez" || $usuario=="Efrain Lizárraga"){ ?>
													<input type="submit" id="nuevop" name="nuevop" value="Nuevo"  />
													<input type="submit" id="aceptarp" name="aceptarp" value="Guardar" />
													<input type="submit" id="borrarp" name="borrarp" value="Borrar" />
													<?php } ?>
													<input type="hidden" readonly="true" size="2%"  type="text" name="idpre" id="idpre">
    	    	            					</ul>
											<form action="<?= base_url()?>index.php/provta/pdfrepbio" method="POST" >							
    	    	            					<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    				<input type="hidden" name="tablabio" value ="" class="htmlTable"/> 
											</form>   
                						</div>       
										<span class="ajaxTable" style="height: 500px; margin-top: -10px; " >
                    						<table id="mytablaPrevta" name="mytablaPrevta" class="ajaxSorter" border="1" style="width: 498px" >
                        						<thead>                            
			                            			<th data-order = "fecpre1" >Fecha</th>
			                            			<th data-order = "idgrsp" >Gramos</th>
			                            			<th data-order = "pcc1" >Base</th>
            			                			<th data-order = "grspre"  >Neto</th>
			                            		</thead>
                        						<tbody title="Seleccione para realizar cambios" style="font-size: 14px">
                        						</tbody>                        	
                    						</table>
                						</span> 
             			 					                	
	                				</div>	                	
            					
					</td>
					<td>
							<div class="ajaxSorterDiv" id="tabla_prevtah" name="tabla_prevtah" style="width: 330px"  >                 			
										<div class="ajaxpager" style=" font-size: 12px;"> 
											<ul class="order_list" style="width: 160px; margin-top: 1px; text-align: ">
    	    	            						Historial por Gramo <input size="2%" type="text" name="idgrsph" id="idgrsph" style="margin-left: 1px; font-size: 14px" >
    	    	            					</ul>
											<form action="<?= base_url()?>index.php/provta/pdfrepbio" method="POST" >							
    	    	            					
    	    	            					<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    				<input type="hidden" name="tablabio" value ="" class="htmlTable"/> 
											</form>   
                						</div>       
										<span class="ajaxTable" style="height: 500px; margin-top: -10px; " >
                    						<table id="mytablaPrevtah" name="mytablaPrevtah" class="ajaxSorter" border="1" style="width: 298px" >
                        						<thead>                            
			                            			<th data-order = "fecpre1h" >Fecha</th>
			                            			<th data-order = "idgrsp" >Gramos</th>
			                            			<th data-order = "pcc1h" >Base</th>
            			                			<th data-order = "grspreh"  >Neto</th>
			                            		</thead>
                        						<tbody title="Seleccione para realizar cambios" style="font-size: 14px">
                        						</tbody>                        	
                    						</table>
                						</span> 
             			 					                	
	                				</div>	     
					</td>
				</tr>
			</table>
			<button type="button" id="precios" style="width: 180px; cursor: pointer; background-color: lightblue" class="continuar used" >Historia de Precios General</button>
			</div>
			<div id='ver_concentrado' class='hide'  >
				<table border="1" style="margin-left: -60px" >
					<tr>
					 	<th>		
				<div class="ajaxSorterDiv" id="tabla_grsmes" name="tabla_grsmes" style="width: 930px">  
               		<div class="ajaxpager" style="margin-top: 0px" >        
	                   	<ul class="order_list" style="width: 500px" >Reporte de Precio Base Registrados según su gramaje correspondiente</ul>     
	                   	<form method="post" action="<?= base_url()?>index.php/provta/pdfreppre" >
                			<input type="hidden" id="mez" name="mez" />
                			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png"  />                                                                      
                        	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                             
 						</form>   
                	</div>  
               		<span class="ajaxTable" style="height: 430px; background-color: white;  margin-top: -23px" >
            	       	<table id="mytablaGM" name="mytablaGM" class="ajaxSorter" border="1" >
                        	<thead title="Presione las columnas para ordenarlas como requiera">
                        		<th data-order = "dia">Dia/grs</th>
                        		<?php	
          						$data['result']=$this->provta_model->verGramos(); $cont=0;
								foreach ($data['result'] as $row):
										$cont+=1;
								?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $row->idgrsp;?></th>
           						<?php
									 
           						endforeach;?>
                        	</thead>
                        	<tbody style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                	</span> 
             	</div>  
             	</th>
             	</tr>
             	</table>
          		<button type="button" id="general" style="cursor: pointer; background-color: gray" class="continuar used" >Cerrar</button>
			</div>
			</div>	
			</div>
		<?php } ?>
		<?php if($usuario=="Zuleima Benitez" || $usuario=="Efrain Lizárraga"){ ?>
		<div id="prevtar" class="tab_content" style="margin-top: 0px" >
 			<div class="ajaxSorterDiv" id="tabla_pvtar" name="tabla_pvtar" style=" margin-left: -10px;  margin-top: 1px;width:955px;">
             	  <div class="ajaxpager" style="margin-top: -5px;" > 
             	  	<form action="<?= base_url()?>index.php/provta/reportet" method="POST" >
            		<ul class="order_list" style="width: 860px; text-align: left; height: 25px" >
            			Ciclo
            			<select  name="cicloPVR" id="cicloPVR" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:-4px;" >   						
   							<option value="22" >2022</option>
   							<option value="21" >2021</option>
   							<option value="20" >2020</option>
   							<option value="19" >2019</option>
   						</select>
   						<select id="Tallas" style="font-size: 12px;height: 25px;margin-top: 1px;"  >   	</select>
            		</ul>
            	</form>	
 				</div>  
             	<span class="ajaxTable" style="height: 550px; background-color: white; width:950px; margin-top: 1px" >
            	   	<table id="mytablaPVR" name="mytablaPVR" class="ajaxSorter" border="1" style="text-align: center" >
                       <thead >
                       		
                       		<tr style="font-size: 10px">
                       			<th rowspan="2" data-order = "nomt" >Talla</th>
                       			<th colspan="3">Selectos</th>
                       			<th colspan="3">Frizajal</th>
                       			<th colspan="3">Barlovento</th>
                       			<th colspan="3">Solman</th>
                       			<th colspan="3">Baja Frio</th>
                       			<th colspan="3">Frezz & Log</th>
                       		</tr>
                       		<tr style="font-size: 10px">
                       			<th data-order = "min1">Min</th>
                       			<th data-order = "pro1">Prom</th>
                       			<th data-order = "max1">Max</th>
                       			<th data-order = "min2">Min</th>
                       			<th data-order = "pro2">Prom</th>
                       			<th data-order = "max2">Max</th>
                       			<th data-order = "min3">Min</th>
                       			<th data-order = "pro3">Prom</th>
                       			<th data-order = "max3">Max</th>
                       			<th data-order = "min4">Min</th>
                       			<th data-order = "pro4">Prom</th>
                       			<th data-order = "max4">Max</th>
                       			<th data-order = "min5">Min</th>
                       			<th data-order = "pro5">Prom</th>
                       			<th data-order = "max5">Max</th>
                       			<th data-order = "min6">Min</th>
                       			<th data-order = "pro6">Prom</th>
                       			<th data-order = "max6">Max</th>
                       		</tr>
                       	</thead>
                       	<tbody style="font-size: 11px; text-align: center">
                       	</tbody>                        	     	
                   	</table>
                </span> 
 			</div>
 			<button id="export_datavr" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Imprimir</button>
			<form id="data_tablesvr" action="<?= base_url()?>index.php/provta/pdfreppvr" method="POST">
				<input type="hidden" name="tablapvr" id="html_gralvr"/> <input type="hidden" name="gravr" id="gravr"/>
			</form>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#export_datavr').click(function(){									
							$('#html_gralvr').val($('#mytablaPVR').html());
							$('#data_tablesvr').submit();
							$('#html_gralvr').val('');
					});
				});
			</script>
 		</div>
		<?php } ?>
		</div>
	</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#nuevos").click( function(){	
	$("#idsem").val('');$("#numsem").val('0');$("#fecsem").val('');$("#impsem").val('');$("#obssem").val('');
	$("#numsem").focus();
	return true;
});
$("#nuevoc").click( function(){	
	$("#iddie").val('');$("#tipdie").val('0');$("#fecdie").val('');$("#facdie").val('');$("#candie").val('');$("#predie").val('');
	//$("#fecdie").focus();
	return true;
});
$("#nuevop").click( function(){	
	$("#idpre").val('');$("#idgrsp").val('');$("#pcc").val('');
	return true;
});
$("#nuevoo").click( function(){	
	$("#idotc").val('');$("#fecotc").val('');$("#impotc").val('');$("#obsotc").val('');
	//$("#fecotc").focus();
	return true;
});
$("#borraro").click( function(){	
	numero=$("#idotc").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/borrar", 
						data: "id="+$("#idotc").val()+"&tabla=otroscostos"+"&campo=idotc",
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaOtc").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevoo").click();																																				
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar para poder Eliminarla");
		return false;
	}
});
$("#borrarp").click( function(){	
	numero=$("#idpre").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/borrar", 
						data: "id="+$("#idpre").val()+"&tabla=gramospre"+"&campo=idpre",
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaPrevta").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevop").click();																																				
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar para poder Eliminarla");
		return false;
	}
});
$("#borrarc").click( function(){	
	numero=$("#iddie").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/borrar", 
						data: "id="+$("#iddie").val()+"&tabla=fac_oad"+"&campo=iddie",
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaCom").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevoc").click();																																				
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar para poder Eliminarla");
		return false;
	}
});

$("#borrars").click( function(){	
	numero=$("#idsem").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/borrar", 
						data: "id="+$("#idsem").val()+"&tabla=sueldos"+"&campo=idsem",
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaSue").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevos").click();																																				
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar para poder Eliminarla");
		return false;
	}
});

$("#aceptaro").click( function(){		
	fec=$("#fecotc").val();
	numero=$("#idotc").val();
	if( fec!=''){
	  	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/actualizarotc", 
						data: "id="+$("#idotc").val()+"&fec="+$("#fecotc").val()+"&imp="+$("#impotc").val()+"&obs="+$("#obsotc").val()+"&cic="+$("#cicgra").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaOtc").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevoo").click();	
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/agregarotc", 
						data: "fec="+$("#fecotc").val()+"&imp="+$("#impotc").val()+"&obs="+$("#obsotc").val()+"&cic="+$("#ciclogra").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#mytablaOtc").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevoo").click();	
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
	}else{
		alert("Error: Seleccione Fecha");	
		$("#fecotc").focus();
		return false;
	}
});


$("#aceptars").click( function(){		
	fec=$("#fecsem").val();
	numero=$("#idsem").val();
	if( fec!=''){
	  	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/actualizarsue", 
						data: "id="+$("#idsem").val()+"&num="+$("#numsem").val()+"&fec="+$("#fecsem").val()+"&imp="+$("#impsem").val()+"&obs="+$("#obssem").val()+"&cic="+$("#cicgra").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaSue").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevos").click();	
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/agregarsue", 
						data: "num="+$("#numsem").val()+"&fec="+$("#fecsem").val()+"&imp="+$("#impsem").val()+"&obs="+$("#obssem").val()+"&cic="+$("#cicgra").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#mytablaSue").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevos").click();	
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
	}else{
		alert("Error: Seleccione Fecha");	
		$("#fecsem").focus();
		return false;
	}
});

$("#aceptarc").click( function(){		
	fec=$("#fecdie").val();
	numero=$("#iddie").val();
	if( fec!=''){
	  	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/actualizardie", 
						data: "id="+$("#iddie").val()+"&fec="+$("#fecdie").val()+"&fac="+$("#facdie").val()+"&lts="+$("#candie").val()+"&pre="+$("#predie").val()+"&tip="+$("#tipdie").val()+"&cic="+$("#cicgra").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaCom").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevoc").click();	
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/agregardie", 
						data: "fec="+$("#fecdie").val()+"&fac="+$("#facdie").val()+"&lts="+$("#candie").val()+"&pre="+$("#predie").val()+"&tip="+$("#tipdie").val()+"&cic="+$("#cicgra").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#mytablaCom").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevoc").click();	
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
	}else{
		alert("Error: Seleccione Fecha");	
		$("#fecdie").focus();
		return false;
	}
});

$("#aceptarp").click( function(){		
	fec=$("#fecpre").val();
	numero=$("#idpre").val();
	if( fec!=''){
	  	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/actualizarpre", 
						data: "id="+$("#idpre").val()+"&fec="+$("#fecpre").val()+"&grs="+$("#idgrsp").val()+"&pcc="+$("#pcc").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaPrevta").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevop").click();	
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/provta/agregarpre", 
						data: "fec="+$("#fecpre").val()+"&grs="+$("#idgrsp").val()+"&pcc="+$("#pcc").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#mytablaPrevta").trigger("update");
										$("#mytablagralz").trigger("update");
										$("#nuevop").click();	
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
	}else{
		alert("Error: Seleccione Fecha");	
		$("#fecpre").focus();
		return false;
	}
});



$("#fecsem").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",		
});
$("#fecdie").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",		
});
$("#fecpre").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2019:+0",		
});
$("#fecotc").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2019:+0",		
});
/*
$("#feccos").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2016:+0",	
	onSelect: function( selectedDate ){
		$("#mytablaprovta").trigger("update");
		$("#nue").click();$("#idcos").val('');$("#tipcos").val('');$("#numcos").val('');$("#clicos").val('Selectos del Mar S.A. de C.V.');
		$("#dia").val($("#feccos").val());
	}
});
*/

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

$("#cicact").change( function(){
	$("#filest").val($("#cicact").val()+$("#numgrabgz").val());$("#seccgz").trigger("update");
});	

$("#numgrabgz").change( function(){
	$("#gra1").val($("#numgrabgz").val());
	$("#filest").val($("#cicact").val()+$("#numgrabgz").val());$("#seccgz").trigger("update");
});
$("#precios").click( function(){
	$('#ver_precios').hide();
	$('#ver_concentrado').show();
});		
$("#general").click( function(){
	$('#ver_precios').show();
	$('#ver_concentrado').hide();
});	

$(document).ready(function(){ 
	$('#ver_precios').show();
	$('#ver_concentrado').hide();
	
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	$("#fecpre").val(f.getFullYear() + "-" + mes + "-" + dia);
   	/*$("#dia").val(f.getFullYear() + "-" + mes + "-" + dia);*/
   	$("#tip").val(0);$("#prebase").val(0);
	$("#filest").val($("#cicact").val()+$("#numgrabgz").val());$("#seccgz").trigger("update");
	
    $("#tabla_gralz").ajaxSorter({
		url:'<?php echo base_url()?>index.php/provta/tablagral', 
		filters:['cicact','numgrabgz','seccgz','tc'],
		sort:false,	
		onSuccess:function(){ 
			$('#tabla_gralz tbody tr').map(function(){
		    	$("#prepl").val($(this).data('prepls'));
		    	$("#preal").val($(this).data('preali'));
		    	$("#preco").val($(this).data('precom'));
		    })	
		    $('#tabla_gralz tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); $(this).css('background','lightblue');})
		    $('#tabla_gralz tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); $(this).css('background','lightblue');})	
		    $('#tabla_gralz tbody tr td:nth-child(12)').map(function(){ $(this).css('font-size','8px');})
		    $('#tabla_gralz tbody tr td:nth-child(15)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); $(this).css('background','lightblue');})
		    $('#tabla_gralz tbody tr').map(function(){
		    	if($(this).data('pisg')=='Total') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    });
		    $('#tabla_gralz tbody tr td:nth-child(16)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 35 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 20 && $(this).html() <= 34.99 && $(this).html() != '' ) {
                		$(this).css("background-color", "yellow"); $(this).css("color", "black");
            		}
            		if($(this).html() >= 0 && $(this).html() <= 19.99 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "white");
        			}
        			
	    	});  
	    	$('#tabla_gralz tbody tr td:nth-child(17)').map(function(){ 
				$(this).css('font-weight','bold');	    		
	    	});
	    	$('#tabla_gralz tbody tr td:nth-child(19)').map(function(){ 
				$(this).css('font-weight','bold');	     $(this).css('text-align','right');		
	    	});
		},	
	});
	
	
   
    
    $("#tabla_otc").ajaxSorter({
		url:'<?php echo base_url()?>index.php/provta/tablaotc',  
		filters:['cicgra'],
        sort:false,
        onRowClick:function(){
        	if($(this).data('idotc')>'0'){
           	$("#idotc").val($(this).data('idotc'));
           	$("#fecotc").val($(this).data('fecotc'));
           	$("#impotc").val($(this).data('impotc'));
           	$("#obsotc").val($(this).data('obsotc'));
           	}           	
		},
		onSuccess:function(){
    		$('#tabla_otc tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_otc tbody tr').map(function(){
		    	if($(this).data('fecotc1')=='Total') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			
	    		}
		    });   
    	},        
	});
    
    $("#tabla_sue").ajaxSorter({
		url:'<?php echo base_url()?>index.php/provta/tablasue',  
		filters:['cicgra'],
        sort:false,
        onRowClick:function(){
        	if($(this).data('idsem')>'0'){
           	$("#idsem").val($(this).data('idsem'));
           	$("#numsem").val($(this).data('numsem'));
           	$("#fecsem").val($(this).data('fecsem'));
           	$("#impsem").val($(this).data('impsem'));
           	$("#obssem").val($(this).data('obssem'));
           	$("#cicgra").val($(this).data('cicsem'));
           	}           	
		},
		onSuccess:function(){
    		$('#tabla_sue tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_sue tbody tr').map(function(){
		    	if($(this).data('fecsem1')=='Total') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			
	    		}
		    });   
    	},        
	});
	
	$("#tabla_com").ajaxSorter({
		url:'<?php echo base_url()?>index.php/provta/tablacom',  
		filters:['cicgra'],
		active:['tipdie',2],
        sort:false,
        onRowClick:function(){
        	if($(this).data('iddie')>'0'){
           	$("#iddie").val($(this).data('iddie'));
           	$("#fecdie").val($(this).data('fecdie'));
           	$("#facdie").val($(this).data('facdie'));
           	$("#candie").val($(this).data('candie'));
           	$("#predie").val($(this).data('predie'));
           	//$("#impdie").val($(this).data('impdie'));
           	$("#tipdie").val($(this).data('tipdie'));
           	$("#cicgra").val($(this).data('cicdie'));
           	}           	
		},
		onSuccess:function(){
    		$('#tabla_com tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_com tbody tr').map(function(){
		    	if($(this).data('fecdie1')=='Total') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			
	    		}
		    });   
    	},        
	});
	
	$("#tabla_prevta").ajaxSorter({
		url:'<?php echo base_url()?>index.php/provta/tablaprevtas',  
		filters:['fecpre'],
		//active:['tipdie',2],
		//multipleFilter:true,
		//onLoad:false,
        sort:false,
        onRowClick:function(){
        	if($(this).data('idpre')>'0'){
           	$("#idpre").val($(this).data('idpre'));
           	$("#fecpre").val($(this).data('fecpre'));
           	$("#idgrsp").val($(this).data('idgrsp'));
           	$("#pcc").val($(this).data('pcc'));
           	}           	
		},
		onSuccess:function(){
    		
    	},        
	});
	$("#tabla_pvtar").ajaxSorter({
		url:'<?php echo base_url()?>index.php/provta/tablaprevtasr',  
		filters:['cicloPVR','Tallas'],
		sort:false,
        onSuccess:function(){
    		colr=2;colb=4; pasar=1;
	    	while(pasar<=6){
	    		$('#tabla_pvtar tbody tr td:nth-child('+colr+')').map(function(){ 
	    			$(this).css('font-weight','bold');$(this).css("color", "red");
	    		});
	    		$('#tabla_pvtar tbody tr td:nth-child('+colb+')').map(function(){ 
	    			$(this).css('font-weight','bold');$(this).css("color", "blue");
	    		});
				pasar+=1;colr+=3;colb+=3;
	    	}   
    	},        
	});
	
	$("#tabla_prevtah").ajaxSorter({
		url:'<?php echo base_url()?>index.php/provta/tablaprevtash',  
		filters:['idgrsph'],
	    sort:false,
    });
    $("#tabla_grsmes").ajaxSorter({
		url:'<?php echo base_url()?>index.php/provta/tablagrsmes',  
		//filters:['idgrsph'],
	    sort:false,
    });
    
    $('#Tallas').boxLoader({
  		url:'<?php echo base_url()?>index.php/provta/combob',
   		equal:{id:'ciclo',value:'#cicloPVR'},
   		select:{id:'idt',val:'val'},
   		all:"Sel.",            
	});

    /*$('#seccgz').boxLoader({
            url:'<?php echo base_url()?>index.php/provta/secciones',
            equal:{id:'numgrabg',value:'#numgrabgz'},
            select:{id:'secc',val:'val'},
            all:"Sel.",
            
    });*/

	$('#seccgz').boxLoader({
            url:'<?php echo base_url()?>index.php/provta/secciones',
            equal:{id:'filest',value:'#filest'},
            //equal:{id:'secc',value:'#secc'},
            select:{id:'secc',val:'val'},
           // onLoad:false,
            all:"Sel.",
            
    });
});
</script>