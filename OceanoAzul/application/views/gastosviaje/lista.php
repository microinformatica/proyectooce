<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 20px;
}   
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#conanu"><img src="<?php echo base_url();?>assets/images/menu/gastosv.png" width="25" height="25" border="0">Gastos de Viaje</a></li>					
		<li><a href="#resumen"><img src="<?php echo base_url();?>assets/images/menu/resumen.png" width="25" height="25" border="0">Resumen</a></li>
		<li style="font-size: 20px"><a href="#zonas"><img src="<?php echo base_url();?>assets/images/mexicopais.png" width="25" height="25" border="0"> Zonas </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="conanu" class="tab_content" >
					<div align="right" style="margin-top: -20px">	
					Ciclo: <select name="cmbCic" id="cmbCic" style="font-size: 10px; height: 25px;" >
								<?php $ciclof=2010; $actual=date("Y");	$mes=date("m");
									 if($mes>=11) $actual+=1;
										while($actual >= $ciclof){?>
											<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            								<?php $actual-=1; } ?>
          					</select> 	
          			</div>		
					<div style="height: 415px;">				
					  <div class="ajaxSorterDiv" id="tabla_ggral" name="tabla_ggral" >                
                		<span class="ajaxTable" style="height: 390px; " >
                		<table id="mytablaGG" name="mytablaGG" class="ajaxSorter" >
                        	<thead>  
                        		<th data-order = "fec1">Fecha</th>
                            	<!--<th data-order = "RemisionR">Rem</th>-->
                            	<th data-order = "Razon">Cliente</th>  
                            	<th data-order = "Loc">Localidad</th>
                            	<th data-order = "Alias">Encarg.</th>
                            	<th data-order = "NomCho">Chofer</th>
                            	<th data-order = "NomUni">Unidad</th>
                            	<th data-order = "NomDesS">Gto</th>
                            	<th data-order = "tot">Total</th>
                            	<th data-order = "Com">Com</th>
                            	<th data-order = "Rep">Imp</th>
                            	<th data-order = "Obser">Observaciones</th>
                        	</thead>
                        	<tbody style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="margin-top: -10px">        
	                    	<ul class="order_list"></ul>                                            
    	                	<form method="post" action="<?= base_url()?>index.php/gastosviaje/pdfrep/" >     
    	                		<input type="hidden" size="8%" name="ciclo" id="ciclo" />     	            		               	             	                                                                                          
                	        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        		                        		                                                      
 							</form>   
                		</div>                                      
            			</div>             	
            		
            	 	</div> 
 	 	</div>
 	 	<div id="resumen" class="tab_content" >
					<div align="right" style="margin-top: -20px">	
					Ciclo: <select name="cmbCicR" id="cmbCicR" style="font-size: 10px; height: 25px;" >
								<?php $ciclof=2014; $actual=date("Y");	$mes=date("m");
									 if($mes>=11) $actual+=1;
										while($actual >= $ciclof){?>
											<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            								<?php $actual-=1; } ?>
          					</select> 
          					<!--
          			Porcentaje Improvistos: <select name="cmbPor" id="cmbPor" style="font-size: 10px; height: 25px;" >
								<option value="10.00" > 10 % </option>
            					<option value="20.00" > 20 % </option>
            					<option value="30.00" > 30 % </option>
          					</select> 	
          					<input size="8%" name="porc" id="porc" />-->		
          			</div>		
					<div style="height: 415px;">				
					  <div class="ajaxSorterDiv" id="tabla_res" name="tabla_res" >                
                		<span class="ajaxTable" style="height: 410px; margin-top: 1px " >
                		<table id="mytablaRes" name="mytablaRes" class="ajaxSorter" border="1" >
                        	<thead>  
                        		<tr>
                        			<th rowspan="2" data-order = "NomBio">Encargado</th>
                            		<th rowspan="2" data-order = "Cargo" style="text-align: center">Cargos</th>  
                            		<th colspan="5" style="text-align: center">Abonos</th>
                            		<th style="text-align: center">Saldo</th>
                            	</tr>
                        		<tr>
                        			<th data-order = "Gasto">Gasto</th>
                            		<th data-order = "Improvistos">Improvistos</th>
                            		<th data-order = "por">% Imp </th>
                            		<th data-order = "Otros">Otros</th>
                            		<th data-order = "totabo">Total</th>
                            		<th  data-order = "Saldo" style="text-align: center">Neto</th>
                            	</tr>
                            </thead>
                        	<tbody style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="margin-top: -10px">        
	                    	<ul style="width: 200px" class="order_list"><sup>(fila roja) % Imprevisto >= 10.00%.</sup></ul>                                         
    	                	<form method="post" action="<?= base_url()?>index.php/gastosviaje/pdfrep1/" >     
    	                		<input type="hidden" size="8%" name="cicloR" id="cicloR" />     	            		               	             	                                                                                          
                	        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        		                        		                                                      
 							</form>   
                		</div>                                      
            			</div>             	
            		
            	 	</div> 
 	 	</div>
 	 	<div id="zonas" class="tab_content" style="height: 485px;" >
			<!--<div align="left" style="margin-left: 25px">Concentrado de Depósitos</div>-->
			<table width="898px" style="margin-top: -10px">
			<tr>
				<th rowspan="2">
				<div class="ajaxSorterDiv" id="tabla_zon" name="tabla_zon" style="margin-top: -10px; width: 425px" >
					<span class="ajaxTable" style="height: 502px; width: 423px" " >
                  		<table id="mytablaZ" name="mytablaZ" class="ajaxSorter" >
                        		<thead style="font-size: 10px" >                            
                            		<th style="width: 30px" data-order = "zon"  >Zona</th>
                            		<th style="width: 20px" data-order = "por"  >% Pl´s</th>                                                        							                                                                                                              
                            		<th style="width: 30px" data-order = "a"  >2014 Pl´s</th>
                            		<th style="width: 35px" data-order = "ac"  >Combustible</th>
                            		<th style="width: 30px" data-order = "b"  >2013 Pl´s</th>        
                            		<th style="width: 35px" data-order = "bc"  >Combustible</th>                    	
                           		</thead>
                        		<tbody style="font-size: 11px">
                        		</tbody>                        	
                    	</table>
                	</span>
           		</div>
           		</th>
           		<th>
           			Zona:<input style="border: none" name="zona" id="zona" /> 
           		</th>
           </tr>
           <tr>
           		<th>
           			<div class="ajaxSorterDiv" id="tabla_cd" name="tabla_cd" style="margin-top: -10px;width: 455px" >
					<span class="ajaxTable" style="height: 238px; width: 453px" >
                  		<table id="mytablaCD" name="mytablaCD" class="ajaxSorter" >
                        		<thead style="font-size: 11px" >                            
                            		<th data-order = "Razon"  >Razon</th>
                            		<th data-order = "RemisionR"  >Rem</th>                   							                                                                                                              
                            		<th data-order = "NomUni"  >Veh</th>
                            		<th data-order = "Com"  >Importe</th>
                            	</thead>
                        		<tbody style="font-size: 11px">
                        		</tbody>                        	
                    	</table>
                	</span>
           		</div>
           		<div class="ajaxSorterDiv" id="tabla_cd1" name="tabla_cd1" style="margin-top: -10px;width: 455px" >
					<span class="ajaxTable" style="height: 238px; width: 453px" >
                  		<table id="mytablaCD1" name="mytablaCD1" class="ajaxSorter" >
                        		<thead style="font-size: 11px" >                            
                            		<th data-order = "Razon"  >Razon</th>
                            		<th data-order = "RemisionR"  >Rem</th>                   							                                                                                                              
                            		<th data-order = "NomUni"  >Veh</th>
                            		<th data-order = "Com"  >Importe</th>
                            	</thead>
                        		<tbody style="font-size: 11px">
                        		</tbody>                        	
                    	</table>
                	</span>
           		</div>
           		</th>
           </tr>
           
           </table>	
	 	</div>
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#cmbCic").change( function(){		
	$("#ciclo").val($("#cmbCic").val());	
 return true;
});
$("#cmbCicR").change( function(){		
	$("#cicloR").val($("#cmbCicR").val());	
 return true;
});
/*
$("#cmbPor").change( function(){		
	$("#mytablaRes").trigger("update");	
	$("#porc").val($("#cmbPor").val());
 return true;
});*/
$(document).ready(function(){
	$("#ciclo").val($("#cmbCic").val());$("#cicloR").val($("#cmbCicR").val());
	//$("#porc").val($("#cmbPor").val()); porci=$("#porc").val();
	$("#tabla_ggral").ajaxSorter({
	url:'<?php echo base_url()?>index.php/gastosviaje/tabla',  	
	filters:['cmbCic'],	
	multipleFilter: true,
    sort:false,
    onSuccess:function(){    		
    		$('#tabla_ggral tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })	    	
	    	$('#tabla_ggral tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_ggral tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_ggral tbody tr td:nth-child(10)').map(function(){ $(this).css('text-align','justify'); })
	},     
	});
	$("#tabla_res").ajaxSorter({
	url:'<?php echo base_url()?>index.php/gastosviaje/tablares',  	
	filters:['cmbCicR'],	
	active:['por1',10.00,'>='],
	//active:['por1','porc','>='],
	//multipleFilter: true,
    sort:false,
    onSuccess:function(){    
    		$('#tabla_res tbody tr').map(function(){
	    		if($(this).data('nombio')=='Total :')
	    			$(this).css('background','lightblue'); //$(this).css('font-weight','bold');   		
	    	});		
	    	$('#tabla_res tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); })	    	
	    	$('#tabla_res tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_res tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_res tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_res tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_res tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); })
	    	$('#tabla_res tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); })
	},     
	});

$("#tabla_zon").ajaxSorter({
		url:'<?php echo base_url()?>index.php/gastosviaje/tablaZona',  	
		sort:false,
		onRowClick:function(){
    		$("#zona").val($(this).data('zon'));
    		$("#tabla_cd").ajaxSorter({
				url:'<?php echo base_url()?>index.php/gastosviaje/tablacd',  	
				filters:['zona'],	
				sort:false,
    			onSuccess:function(){    		
    				$('#tabla_cd tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })	    	
	    			
				},     
			});
			$("#tabla_cd1").ajaxSorter({
				url:'<?php echo base_url()?>index.php/gastosviaje/tablacd1',  	
				filters:['zona'],	
				sort:false,
    			onSuccess:function(){    		
    				$('#tabla_cd1 tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })	    	
	    			
				},     
			});
    	},
    	onSuccess:function(){
    		$('#tabla_zon tbody tr').map(function(){
		    	if($(this).data('zon')=='Total:'){
		    		$(this).css('background','lightblue');$(this).css('font-weight','bold');		    		
		    	}		    	
	    	})
	    	$('#tabla_zon tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold'); })
    		$('#tabla_zon tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right');$(this).css('font-weight','bold'); })
	    	$('#tabla_zon tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right');$(this).css('background','lightgreen');$(this).css('font-weight','bold'); })	    	
	    	$('#tabla_zon tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_zon tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');$(this).css('background','lightgreen');$(this).css('font-weight','bold'); })
	    	$('#tabla_zon tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_zon tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_zon tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_zon tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','right'); })	    	
    	},     
	});


});

	
</script>