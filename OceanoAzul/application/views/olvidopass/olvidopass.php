<?php $this->load->view('headerInicio_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<div class="standard_error" style="height: 390px">
	<form id="loginForm" style="width:250px" name="loginForm" method="post" action="" onsubmit="return false;">		
		<h2 class="blockhead">RECUPERAR CONTRASEÑA</h2> 
		<fieldset id="body" name="body">
			<fieldset>
				<label for="usuario" id="txtUsu">Correo Electronico</label>
				<input type="text" name="usuario" id="usuario" title="Usuario" size="3px" />
				<div align="right">@aquapacific.com.mx</div>
		    </fieldset>
		    <input type="submit" id="aceptar" name="aceptar" value="Recuperar Cuenta" />		
		</fieldset>
	</form>	
</div>

<?php $this->load->view('footerInicio_cv'); ?>

<script type="text/javascript">
</script>
