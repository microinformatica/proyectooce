<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 80px 50px 20px; }
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: navy; }	
</style> 
<title>Reporte Estadistico</title>
</head>
<body>
	<div id="header"> 
		<table style="width: 770px;">
			<tr>
				<td rowspan="2" width="200px">
					<p style="font-size: 8px;"  >
						<img src="<?php echo base_url();?>assets/images/aquapacificlogotipo.jpg" width="200" height="60" border="0">
						SIA - <?= $usuario?> - <?= $perfil?> <br />
					</p>
				</td>	
				<td align="center" style="font-size: 14px; color: navy"><strong>Fecha: <?= $fec?></strong></td>
				<td align="center" style="font-size: 14px; color: navy"><strong>Orden: <?= $ord?></strong></td>			
			</tr>
			<tr>
					
				<td colspan="2" align="center" style="font-size: 14px; color: navy"><strong>Proveedor: <?= $pro?></strong></td>
							
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: white;">
				<td width="160px">www.aquapacific.com.mx</td>	
				<td width="490">
					Av. Emilio Barragán No.63-103, Col. Lázaro Cárdenas, Mazatlán, Sin. México, RFC:AQU-031003-CC9, CP:82040 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
	</div>

	<br/>
	<table  width="725px" border="1" style="font-size: 8px;">
    	  <?= $tablac?>
	</table>	
	<p>
	<table style="font-size: 12px;" border="0" width="725px">
		<thead>			
			<tr> <th align="justify" style="font-size: 14px">Sin más por el momento quedo a sus ordenes.
				
				</th>
			</tr>
			<tr> <th align="left" style="font-size: 14px"><br />Javier Morán <div style="font-size: 10px">Departamento de Compras</div>
				</th>
				<th align="right" style="width: 335px">Atención Telefónica: (669)918-0784</th>
			</tr>
			<tr> <th align="left" style="font-size: 14px">AQUAPACIFIC S.A. DE C.V.</th>
				<th align="right" style="width: 335px">e-mail: compras@aquapacific.com.mx</th>
			</tr>	
		</thead>
		
	</table>
	</p>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>
