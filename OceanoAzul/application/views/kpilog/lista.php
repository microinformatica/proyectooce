<?php $this->load->view('header_cv'); ?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
 <style type="text/css">
            .color{
                background-color: #ff00ff;
            }
        </style>
        <script type="text/javascript">
           /* $(document).ready(function(){
                $("table.mitabla td").click(function(){
                    $(this).toggleClass("color");
                });
            });*/
        </script>
<style>     
ul.tabs li {border-bottom: 1px ;}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs" >
		<strong>
		<li><a href="#kpi" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/kpi.png" width="25" height="25" border="0"> KPI </a></li>
		<li><a href="#entregados" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/truck.png" width="25" height="25" border="0"> Entregas </a></li>		
		<li><a href="#tecnicotra" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/caja.png" width="20" height="20" border="0"> Técnicos </a></li>
		<li><a href="#cv" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/tecnicos.png" width="20" height="20" border="0"> CV </a></li>
		<li><a href="#rescho" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/caja.png" width="20" height="20" border="0"> Chofer </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px" >
		<div id="kpi" class="tab_content" style="margin-top: -15px" >
		  <div class="ajaxSorterDiv" id="tabla_kpi" name="tabla_kpi" style=" height: 470px;margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 450px; text-align: left" >
            		Mes:
   					<select name="cmbMeskpi" id="cmbMeskpi" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   						<option value="12" >Diciembre</option>
   						<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   						<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   						<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   						<option value="10" >Octubre</option><option value="11" >Noviembre</option>
   						
   					</select>
   					Departamento:
   					<?php if($usuario=="Jesus Benítez"){ ?>
   					<select name="cmbDepto" id="cmbDepto" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   						<?php	
          					$this->load->model('kpilog_model');
							$data['result']=$this->kpilog_model->verDepto();
							foreach ($data['result'] as $row):?>
           						<option value="<?php echo $row->NDep;?>" ><?php echo $row->NomDep;?></option>
           				<?php endforeach;?>
   					</select>
   					<?php } else{?>
   					<?php if($usuario=="Daniel Lizárraga"  || $usuario=="Anita Espino" ){ ?>
   					<select name="cmbDepto" id="cmbDepto" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">
   						<option value="17">Logistica</option>
   					</select>		
   					<?php } } ?>
   					<?php if($usuario=="Jesus Benítez"){ ?>
                  	 	<select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 22px; visibility: hidden"  >
                    		<!--<option value="solicitud">2015</option>-->
                    		<?php $ciclof=2014; $actual=date("Y"); //$actual+=1;
								while($actual >= $ciclof){?>
									<option value="<?php echo ($actual-2000);?>" > <?php echo $actual;?> </option>
            					<?php  $actual-=1; } ?>
          				</select>      
                        
                   <?php } else {?>
                        <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 22px; visibility: hidden">
                    		<option value="17">2017</option>
                    	</select> 	
                    <?php } ?>		
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 430px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablakpi" name="mytablakpi" class="ajaxSorter" border="1" >
                       	<thead >
                       		<th data-order = "nom">Evaluación</th>
                       		<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
                       	</thead>
                       	<tbody style="font-size: 9px">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>		    
 		</div>
		<div id="entregados" class="tab_content" style="margin-top: -15px" >
			<div id='ver_mas_det' class='hide' style="height: 238px; margin-top: 4px" align="" >
			<div class="ajaxSorterDiv" id="tabla_entre" name="tabla_entre" style=" height: 470px;margin-top: 1px">   
             	 <?php if($usuario=="Jesus Benítez"){ ?>
                  	Ciclo:                 
                    	<select name="cmbCicloe" id="cmbCicloe" style="font-size: 10px; height: 22px;" >
                    		<!--<option value="solicitud">2015</option>-->
                    		<?php $ciclof=2014; $actual=date("Y"); //$actual+=1;
								while($actual >= $ciclof){?>
									<option value="<?php echo ($actual-2000);?>" > <?php echo $actual;?> </option>
            					<?php  $actual-=1; } ?>
          				</select>      
                        
                   <?php } else {?>
                        Ciclo:
                        <select name="cmbCicloe" id="cmbCicloe" style="font-size: 10px; height: 22px;">
                    		<option value="17">2017</option>
                    	</select> 	
                    <?php } ?>
             	<!--<div align="left"  style="font-size: 12px; margin-top: -10px">-->             
              	Entregas a Clientes Iniciando del Día <input size="12%" type="text" name="txtFIE" id="txtFIE" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                <span class="ajaxTable" style="height: 390px;" >
                    <table id="mytablaent" name="mytablaent" class="ajaxSorter" border="1" style="width: 900px;">
                			<thead >  
                				<th data-order = "zon" >Zona</th>
                				<th data-order = "cli" >Cliente</th>	
                				<?php	
          						$cont=0;
								while($cont<7){ $cont+=1;?>
           							<th data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           						<?php } ?>
                				<th data-order = "tc" align="right">Total</th>
                           	</thead>
                        	<tbody  style="font-size: 11px">                                                 	  
                        	</tbody>
                    </table>
                </span> 
             	<div class="ajaxpager" style="margin-top: -5px">        
                    <ul class="order_list"></ul>     
                    <form method="post" action="<?= base_url()?>index.php/kpilog/pdfrepent" >                    	             	                                                
                        <img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden"  name="tablaent" value ="" class="htmlTable"/>                                                      
 					</form>   
 				</div> 
 			</div>
 			</div>
 			<div id='ver_mas_ent' class='hide' style="margin-top: 4px">
                <div class="ajaxSorterDiv" id="tabla_deta" name="tabla_deta" style=" height: 470px;margin-top: 5px"> 
                 <div class="ajaxpager" style="margin-top: -5px">        
                    <ul class="order_list" style="width: 750px">
                    	Zona: <input style="width: 120px; border: none" name="zona" id="zona"/>
                					Cliente: <input style="width: 480px" name="clie" id="clie"/>
                					<input size="4%" type="hidden"  name="numcli" id="numcli"/>
                    	     	<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(1)" /></ul>     
                    <form method="post" action="<?= base_url()?>index.php/kpilog/pdfrepentdet" >                    	             	                                                
                        <img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden"  name="tablaentdet" value ="" class="htmlTable"/>    
                        <input type="hidden" style="width: 120px; border: none" name="zona1" id="zona1"/>
                		<input type="hidden" style="width: 480px" name="clie1" id="clie1"/>                                                  
 					</form>   
 				</div>
                 <span class="ajaxTable" style="height: 390px; margin-top: -5px" >
                    <table id="mytabladeta" name="mytabladeta" class="ajaxSorter" border="1" style="width: 900px;">
                			<thead >  
                				<th data-order = "Fecha1" >Fecha</th>
                				<th data-order = "cos" >Cosechó</th>
                				<th data-order = "CV" >CV</th>
                				<th data-order = "RemisionR" >Remision</th>	
                				<th data-order = "can" >Cantidad</th>
                				<th data-order = "tec" >Técnico</th>
                				<th data-order = "Folio" >Folio</th>
                				<th data-order = "Obs" >Observaciones</th>
                           	</thead>
                        	<tbody  style="font-size: 11px">                                                 	  
                        	</tbody>
                    </table>
                </span> 
             	 
 			</div>
            </div>
		</div>	
	<div id="tecnicotra" class="tab_content" style="margin-top: -15px" >
		 
		  <div class="ajaxSorterDiv" id="tabla_tectra" name="tabla_tectra" style=" height: 470px;margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 150px" >
            		Mes:
   					<select name="cmbMestra" id="cmbMestra" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   						<option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   						<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   						<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   						<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   						<option value="10" >Octubre</option>
   						
   					</select>	
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 430px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablaTectra" name="mytablaTectra" class="ajaxSorter" border="1" >
                       	<thead >
                       		<th data-order = "tec">Tecnico</th>
                       		<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
                       	</thead>
                       	<tbody style="font-size: 9px">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>		    
 		</div>	  		
 	<div id="cv" class="tab_content" style="margin-top: -15px" >
		  <div class="ajaxSorterDiv" id="tabla_cos" name="tabla_cos" style=" height: 470px;margin-top: 1px">   
             	  <div class="ajaxpager" style="margin-top: 0px;" > 
            		<ul class="order_list" style="width: 300px" >
            		Coeficientes de Variación - Mes:
   					<select name="cmbMesc" id="cmbMesc" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   						<option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   						<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   						<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   						<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   						<option value="10" >Octubre</option>
   					</select>
            		
					</ul>
 				</div>  
             	<span class="ajaxTable" style="height: 430px; background-color: white; width: 910px; margin-top: 1px" >
            	   	<table id="mytablaCos" name="mytablaCos" class="ajaxSorter" border="1" >
                       	<thead >
                       		<th data-order = "tecc">Tecnico</th>
                       		<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "dc".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
                       	</thead>
                       	<tbody style="font-size: 9px">
                       	</tbody>                        	
                   	</table>
                </span> 
 			</div>		    
 		</div>
 	  <div id="rescho" class="tab_content" style="margin-top: -15px" >
		     <table border="0" style="margin-top: 7px">
				<tr>
					<th>
						<div class="ajaxSorterDiv" id="tabla_rescho" name="tabla_rescho" style="height: 455px;width: 920px; ">   
             	  			<div class="ajaxpager" style="margin-top: 0px;" > 
             	  			<ul class="order_list" style="width: 300px" >
             	  				<select name="cmbMesch" id="cmbMesch" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
   									<option value="0" >Todos</option>
   									<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   									<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   									<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   									<option value="10" >Octubre</option>
   									<option value="11" >Noviembre</option><option value="12" >Diciembre</option>
   								</select>
            					Chofer: <select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Daniel Lizárraga"){ ?> disabled="true" <?php } ?> name="cho" id="cho" style="font-size: 10px;height: 25px;  margin-top: 1px; margin-left:1px; width: 130px">
          									<?php	
           										$data['result']=$this->kpilog_model->verChofer();
												foreach ($data['result'] as $row):?>
           											<option value="<?php echo $row->NumCho;?>" ><?php echo $row->NomCho;?></option>
           										<?php endforeach;?>
   										</select>
            				</ul>
 						</div>  
             			<span class="ajaxTable" style="height: 415px; background-color: white; width: 910px; margin-top: 1px" >
            	   		<table id="mytablaRescho" name="mytablaRescho" class="ajaxSorter" border="1" >
                       	<thead >
                       		<tr>
                       		<th rowspan="2" style="text-align: center" data-order = "fechaer" >Día</th>   
                       		<th rowspan="2" style="text-align: center" data-order = "RemisionR" >Rem</th>
                            	<th rowspan="2" style="text-align: center" data-order = "Razon" >Cliente</th>
                            	<th rowspan="2" style="text-align: center" data-order = "NomUni" >Unidad</th>
                            	<th rowspan="2" style="text-align: center" data-order = "NomBio" >Técnico</th>
                            	
                            	<th colspan="3" style="text-align: center" >Escaner</th>
                            	<th colspan="3" style="text-align: center" >Lts. Lab.</th>
                            	<th style="text-align: center" >Desv.</th>
                            	<th colspan="4" style="text-align: center">Descuento</th>
                           </tr>
                        	<tr>	
                        		<th rowspan="2" style="text-align: center" data-order = "kmr">Kms</th>
                            	<th rowspan="2" style="text-align: center" data-order = "cvn">Lts</th>
                        		<th style="text-align: center" data-order = "rene" >Ren</th>
                            	<!--<th style="text-align: center" data-order = "renv" >Vje</th>-->
                            	<th style="text-align: center" data-order = "rv" >Fac</th>
                            	<th style="text-align: center" data-order = "rl" >Rell</th>
                            	<th style="text-align: center" data-order = "lt" >Tot</th>
                            	<th style="text-align: center" data-order = "des" >%</th>
                            	<th style="text-align: center" data-order = "desc" >%</th>
                            	<th style="text-align: center" data-order = "ltsc" >Lts</th>
                            	<th style="text-align: center" data-order = "pre" >$</th>
                            	<th style="text-align: center" data-order = "tot" >Total</th>
                         </tr> 
                       	</thead>
                       	<tbody style="font-size: 9px">
                       	</tbody>                        	
                   		</table>
                		</span> 
 						</div>
					</th>
				</tr>
			</table>     
 		</div>	  
 	</div>	
</div>

<?php $this->load->view('footer_cv'); ?>



<script type="text/javascript">
function cerrar(sel){
	$(this).removeClass('used');
	if(sel==1){
		$('#ver_mas_ent').hide();
		$('#ver_mas_det').show();	
    	
	}	
}


$("#txtFIE").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFFE").datepicker( "option", "minDate", selectedDate );
		$('#tabla_entre').trigger('update')		
	}
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
		$.datepicker.setDefaults($.datepicker.regional['es']);
});  

// Función que suma o resta días a la fecha indicada
 
sumaFecha = function(d, fecha){
 var Fecha = new Date();
 var sFecha = fecha || (Fecha.getDate() + "-" + (Fecha.getMonth() +1) + "-" + Fecha.getFullYear());
 var sep = sFecha.indexOf('-') != -1 ? '-' : '-'; 
 var aFecha = sFecha.split(sep);
 var fecha = aFecha[2]+'-'+aFecha[1]+'-'+aFecha[0];
 fecha= new Date(fecha);
 fecha.setDate(fecha.getDate()+parseInt(d));
 var anno=fecha.getFullYear();
 var mes= fecha.getMonth()+1;
 var dia= fecha.getDate();
 mes = (mes < 10) ? ("0" + mes) : mes;
 dia = (dia < 10) ? ("0" + dia) : dia;
 var fechaFinal = anno+sep+mes+sep+dia;
 return (fechaFinal);
}
$(document).ready(function(){       
	var f = new Date(); 
    //Restar dia a la fecha actual
	var fecha = sumaFecha(-6,f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear());
	$("#txtFIE").val(fecha);	
	$("#txtFFE").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#cmbMes").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));$("#cmbMesch").val((f.getMonth() +1));
	$("#cmbMestra").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));
	$("#cmbMeskpi").val((f.getMonth() +1));$("#cmbMesc").val((f.getMonth() +1));
	$(this).removeClass('used');
   	$('#ver_mas_ent').hide();
	$("#tabla_entre").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpilog/tablaent', 
		filters:['cmbCicloe','txtFIE'],
		sort:false,	
		onRowClick:function(){
			if(($(this).data('ncli')>='0')){
				$(this).removeClass('used');$('#ver_mas_det').hide();$('#ver_mas_ent').show();
				$("#zona").val(($(this).data('zon1')));$("#clie").val(($(this).data('cli')));$("#numcli").val(($(this).data('ncli')));
				$("#zona1").val(($(this).data('zon1')));$("#clie1").val(($(this).data('cli')));
				$("#tabla_deta").ajaxSorter({
					url:'<?php echo base_url()?>index.php/kpilog/tabladeta', 
					filters:['cmbCicloe','txtFIE','numcli'],
					sort:false,	
					onSuccess:function(){  
						$('#tabla_deta tbody tr td:nth-child(4)').map(function(){	
               					if($(this).html() < 1 && $(this).html() != '' ) {$(this).css("background-color", "#0F0");}
            					if($(this).html() < 0 && $(this).html() != '' ) {$(this).css("background-color", "#F00");}
            					if($(this).html() > 1 && $(this).html() != '' ) {$(this).css("background-color", "orange");}
		    			});
    					$('#tabla_deta tbody tr td:nth-child(4)').map(function(){$(this).css('text-align','center'); $(this).css('font-weight','bold');})
    					$('#tabla_deta tbody tr td:nth-child(5)').map(function(){$(this).css('text-align','right'); $(this).css('font-weight','bold');})
					},	
				});
				
			}
        },
		onSuccess:function(){  
			 $('#tabla_entre tbody tr').map(function(){
			 	 						
			//if($(this).data('d1') > 0){	
	    		//$("tr").each(function(){
	    		$('#tabla_entre tbody tr').map(function(){	
        /*$(this).children("td").each(function(){
            switch ($(this).html()) {
                case 'Orlando':
                    $(this).css("background-color", "#F00");
                break;
                case '8887':
                    $(this).css("background-color", "#0F0");
                break;
                case '8888':
                    $(this).css("background-color", "#00F");
                break;
            }
        })*/
       
       		$(this).children("td").each(function(){
       				//if($(this).data('d1') < 1){	
            		if($(this).html() < 1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0");
            		}
            		if($(this).html() < 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#F00");
            		}
            		
            		if($(this).html() >1 && $(this).html() < 2 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange");
            		}
        		})
    		})
    		
	    //}	
	    });
	    $('#tabla_entre tbody tr td:nth-child(10)').map(function(){$(this).css('text-align','right'); $(this).css('font-weight','bold');})
	    $('#tabla_entre tbody tr').map(function(){
	    		if($(this).data('cli')=='Total Día' || $(this).data('cli')=='') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','right');
	    		}
	    		
	    	});	
		},	
			
	});
	
	$("#tabla_tectra").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpilog/tablatectra', 
		filters:['cmbCiclo','cmbMestra'],
		sort:false,	
		onSuccess:function(){   			
			//ilumina el dia actual del año en curso
			if($("#cmbMestra").val()==(f.getMonth() +1)){
				$('#tabla_tectra tbody tr td:nth-child('+((f.getDate())+1)+')').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			}
			$('#tabla_tectra tbody tr').map(function(){
		    	if($(this).data('tectra')=='KPI') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    })	
				
	    	$('#tabla_tectra tr').map(function(){	    		
	    		if($(this).data('d'+((f.getDate())))!='' && $("#cmbMestra").val()==(f.getMonth() +1)){
	    			$(this).css('background','lightblue');
	    		}
	    		
	    		$(this).children("td").each(function(){
       				//if($(this).data('d1') < 1){	
            		if($(this).html() > 0 && $(this).html() < 1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0");
            		}
            		if($(this).html() < 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "red");
            		}
            		if($(this).html() >1 && $(this).html() < 2 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange");
            		}
            		if($(this).html() >= 90.0 && $(this).html() <= 100.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 60.0 && $(this).html() <= 89.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
            		if($(this).html() >= 2 &&  $(this).html() <= 59.9 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "red");
            		}
        		})
        		
	    	});
	    	
	    	
		},		
	});
	$("#tabla_cos").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpilog/tablacos', 
		filters:['cmbCiclo','cmbMesc'],
		sort:false,	
		onSuccess:function(){   						
			//ilumina el dia actual del año en curso
			if($("#cmbMesc").val()==(f.getMonth() +1)){
				$('#tabla_cos tbody tr td:nth-child('+((f.getDate())+1)+')').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			}
			$('#tabla_cos tr').map(function(){	    		
	    		if($(this).data('tecc')=='KPI' || $(this).data('tecc')=='KPI Día') {	
	    		$(this).children("td").each(function(){
       				//if($(this).data('d1') < 1){	
            		
            		if($(this).html() >= 0.1 && $(this).html() <= 3.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0");//$(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 3.01 && $(this).html() <= 4.99 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); //$(this).css("color", "orange");
            		}
            		if($(this).html() >= 5 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); //$(this).css("color", "red");
            		}            		
        		})
        		}
	    	});
			
		},	
	});
	$("#tabla_kpi").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpilog/tablakpi', 
		filters:['cmbMeskpi','cmbDepto'],
		sort:false,	
		onSuccess:function(){   						
			//ilumina el dia actual del año en curso
			if($("#cmbMeskpi").val()==(f.getMonth() +1)){
				$('#tabla_kpi tbody tr td:nth-child('+((f.getDate())+1)+')').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			}
			$('#tabla_kpi tr').map(function(){	    		
	    		if($(this).data('nom')=='Entregas' || $(this).data('nom')=='Tecnico' || $(this).data('nom')=='Viaticos') {
	    		$(this).children("td").each(function(){
       				if($(this).html() >= 90.0 && $(this).html() <= 100.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 60.0 && $(this).html() <= 89.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
            		if($(this).html() >= 2 &&  $(this).html() <= 59.9 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "red");
            		}            		
        		})
        		}
        		
	    	});
	    	$('#tabla_kpi tr').map(function(){	    		
	    		if($(this).data('nom')=='C.V.') {
	    		$(this).children("td").each(function(){
       				if($(this).html() >= 0.1 && $(this).html() <= 3.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 3.01 && $(this).html() <= 4.99 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
            		if($(this).html() >= 5 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "red");
            		}  
        		})
        		}
        		
	    	});
	    	$('#tabla_kpi tr').map(function(){	  
	    	if($(this).data('nom')=='KPI') {
	    		$(this).children("td").each(function(){
       				if($(this).html() >= 90.0 && $(this).html() <= 100.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
            		if($(this).html() >= 60.0 && $(this).html() <= 89.9 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
            		if($(this).html() >= 2 &&  $(this).html() <= 59.9 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "red");
            		}         		 
        		})
        		}
        	});	
		},	
	});
	
	
	$("#tabla_rescho").ajaxSorter({
		url:'<?php echo base_url()?>index.php/kpilog/tablarescho', 
		filters:['cmbMesch','cho'],
		sort:false,	
		onSuccess:function(){
				$('#tabla_rescho tbody tr td:nth-child(12)').map(function(){ 
	    			$(this).css('text-align','right'); 
	    			if($(this).html() <= 5.00 && $(this).html() != '' ) {
              				//$(this).css("background-color", "#0F0");$(this).css("color", "#0F0");
              				$(this).css("background-color", "#0F0");$(this).css("color", "black");
        			}
        			if($(this).html() >= 5.01 && $(this).html() <= 20.0 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "white");
        			}
        			if($(this).html() >= 20.01 && $(this).html() != '' ) {
             				$(this).css("background-color", "red"); $(this).css("color", "white");
        			} 
	    		})
		}, 
	});
});
</script>

