<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/sagarpa3.jpg" width="25" height="25" border="0"> Certificados </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px" >
		    <div class="ajaxSorterDiv" id="tabla_cer" name="tabla_cer" align="left"  >             	
            	<div style="margin-top: -10px;">            	
				</div>	       
                <span class="ajaxTable" style=" height: 110px; width: 935px; background-color: white ">
                    <table id="mytablaC" name="mytablaC" class="ajaxSorter" ">
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <th data-order = "NumCer" >No</th>                                                        
                            <th data-order = "IniVig" >Inicia Vigencia</th>                            
                            <th data-order = "FinVig" >Vence el Día</th>
                            <th data-order = "CanPos" >Postlarvas</th>
                            <th data-order = "CanNau" >Nauplios</th>                                                       
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                 </span> 
             	<div class="ajaxpager" style="margin-top: -15px">        
                    <ul class="order_list"><input style="font-size: 14px" type="submit" id="nuevo" name="nuevo" value="Nuevo" /></ul>                          
                       
 				</div>  
            </div>             
        <div id='ver_mas_cer' class='hide' style="height: 238px; margin-top: 4px" align="" >
						<!--<div style="color: blue">Certificado</div>-->
       					<table style="width: 680px;" border="2px">
							<thead style="background-color: #DBE5F1">
								<th colspan="4" height="15px" style="font-size: 20px">Número de Certificado DGSA-DSAP-CSAMO-<input size="8%" type="text" name="cer" id="cer" style="text-align: center; border: none; color: red;font-size: 20px" >
								<input size="8%" type="hidden"  name="id" id="id"/>																								
								</th>																	
							</thead>
							<tbody style="background-color: #F7F7F7">
							<tr>
								<th colspan="2" style="text-align: center">Vigencia</th><th colspan="2" style="text-align: center">Cantidad</th>
							</tr>
							<tr>
								<th style="font-size: 14px; text-align: center">Inicial</th>
								<th style="font-size: 14px; text-align: center">Vence el Día</th>
								<th style="font-size: 14px; text-align: center">Postlarvas</th>	
								<th style="font-size: 14px; text-align: center">Nauplios</th>
							</tr>
    						<tr style="font-size: 14px; background-color: white;">
								<th style="font-size: 14px; text-align: center"><input size="13%" type="text" name="txtFi" id="txtFi" class="fecha redondo mUp" style="text-align: center;"  readonly="true" ></th>									
								<th style="font-size: 14px; text-align: center"><input size="13%" type="text" name="txtFv" id="txtFv" class="fecha redondo mUp" style="text-align: center;"  readonly="true" ></th>
								<th style="font-size: 14px; text-align: center"><input size="13%" type="text" name="pos" id="pos" style="text-align: right" ></th>									
								<th style="font-size: 14px; text-align: center"><input size="13%" type="text" name="nau" id="nau" style="text-align: right"></th>
							</tr>
							</tbody>
    						<tfoot style="background-color: #DBE5F1">
    							<th colspan="4" style="text-align: right">
									<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
									<input style="font-size: 14px" type="submit" id="borrar" name="borrar" value="Borrar" />
									<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar(1)" />
								</th>	
							</tfoot>
						</table>
					</div>
    
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
	
function cerrar(sel){
	$(this).removeClass('used');
	$('#ver_mas_cer').hide();
}

$("#nuevo").click( function(){
	$("#cer").val('');
	$("#txtFi").val('');
	$("#txtFv").val('');
	$("#pos").val('');
	$("#nau").val('');
	$("#id").val('');
	$(this).removeClass('used');
	$('#ver_mas_cer').show();	
 return true;
})

$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/certificados/borrar", 
						data: "id="+$("#id").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Certificado Eliminado");
										$("#nuevo").click();
										$(this).removeClass('used');
										$('#ver_mas_cer').hide();
										$("#mytablaC").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Certificado para poder Eliminarlo");
		return false;
	}
});


function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#aceptar").click( function(){		
	cer=$("#cer").val();fi=$("#txtFi").val();fv=$("#txtFv").val();
	numero=$("#id").val();
	if( cer!=''){
	  if( fi!=''){
	  	if( fv!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/certificados/actualizar", 
						data: "id="+$("#id").val()+"&cer="+$("#cer").val()+"&fi="+$("#txtFi").val()+"&fv="+$("#txtFv").val()+"&pos="+$("#pos").val()+"&nau="+$("#nau").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevo").click();
										$(this).removeClass('used');
										$('#ver_mas_cer').hide();
										$("#mytablaC").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/certificados/agregar", 
						data: "cer="+$("#cer").val()+"&fi="+$("#txtFi").val()+"&fv="+$("#txtFv").val()+"&pos="+$("#pos").val()+"&nau="+$("#nau").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Certificado registrado correctamente");
										$("#nuevo").click();
										$(this).removeClass('used');
										$('#ver_mas_cer').hide();
										$("#mytablaC").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		}else{
			alert("Error: Vencimiento no valido");	
			$("#txtFv").focus();
			return false;
		}
		}else{
			alert("Error: Vigencia Inicial no valida");	
			$("#txtFi").focus();
			return false;
		}					
	}else{
		alert("Error: Número de Certificado no valido");	
		$("#cer").focus();
		return false;
	}
});

/*$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});*/

$("#txtFi").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFv").datepicker( "option", "minDate", selectedDate );
		//$("#mytablaE").trigger("update");		
	}	
});
$("#txtFv").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFi").datepicker( "option", "maxDate", selectedDate );
		//$("#mytablaE").trigger("update");	
	}	
});	

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  


function noNumbers(e){
		var keynum;
		var keychar;
		var numcheck;
		if(window.event){
			jeynum = e.keyCode;
		}
		else if(e.which){
			heynum = eArray.which;
		}
		keychar = String.formCharccode(keynum);
		numcheck = /\d/;
		return !numcheck.test(keychar);
	}
		
$(document).ready(function(){ 
	//$("#nuevo").click();	
	$('#ver_mas_cer').hide();
	$("#tabla_cer").ajaxSorter({
		url:'<?php echo base_url()?>index.php/certificados/tabla',  		
		//filters:['cmbEstatus','txtFI','txtFF'],
		//active:['Verificacion',-1],
    	//multipleFilter:true, 
    	sort:false,              
    	onRowClick:function(){
    		if($(this).data('numreg')>'1'){
    			$(this).removeClass('used');
   				$('#ver_mas_cer').show();
   				$("#id").val($(this).data('numreg'));
   				$("#cer").val($(this).data('numcer')); 
   				$("#txtFi").val($(this).data('inivig'));
   				$("#txtFv").val($(this).data('finvig'));
   				$("#pos").val($(this).data('canpos'));
   				$("#nau").val($(this).data('cannau'));
    		}
    	}, 
    	onSuccess:function(){
    	},  
	});
	
	
		
});  	
</script>