<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts-more.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/export-data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/accessibility.js"></script>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 20px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
#export_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#export_datagme { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li ><a href="#recibir"><img src="<?php echo base_url();?>assets/images/menu/truck.png" width="24" height="24" border="0"> Almacen</a></li>
		<li ><a href="#pagos"><img src="<?php echo base_url();?>assets/images/menu/entradas.png" width="24" height="24" border="0"> General</a></li>
		<li ><a href="#grafrendi"><img src="<?php echo base_url();?>assets/images/menu/grafren.png" width="24" height="24" border="0"> Rendimiento</a></li>
		 <?php if($usuario=="Jesus Benítez" or $usuario=="Zuleima Benitez" ){ ?>
		<li ><a href="#fleseg"><img src="<?php echo base_url();?>assets/images/menu/truck.png" width="24" height="24" border="0"> FleSeg</a></li>
		 <?php } ?>
		</strong>
		<select id="ciclo" style="font-size:11px; margin-top: 1px;" >
			<option value="22">2022</option>
			<option value="21">2021</option><option value="20">2020</option><option value="19">2019</option>
   		</select>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 572px"  >
		<div id="recibir" class="tab_content" style="margin-top: -1px">
			
						<table border="1" style="font-size: 12px;margin-top: -5px"  >
							
							<tr style="background-color: lightgray; color: black">
								<td colspan="2" style="background-color: lightblue;">I.- Reg. 
									
									<select id="idae" style="font-size:10px; margin-top: 1px; margin-left: -5px" >
                    					<?php	
          										$data['result']=$this->maquila_model->getAlmacenes();
												foreach ($data['result'] as $row):?>
           											<option value="<?php echo $row->ida;?>"><?php echo $row->noma;?></option>
           										<?php endforeach;?>
   									</select>
   									<input type="hidden" id="ide" name="ide" />
   								</td>
								<td colspan="4" style="text-align: left">Día<input size="8%" type="text" name="fece" id="fece" class="fecha redondo mUp" style="text-align: center; font-size: 11px" >
								Granja
									<select id="idge" style="font-size:10px; margin-top: 1px; margin-left: -3px" >
                    					<option value="0">-</option>
          									<?php	
          										$data['result']=$this->maquila_model->getGranjas();
												foreach ($data['result'] as $row):?>
           											<option value="<?php echo $row->idg;?>"><?php echo $row->nomg;?></option>
           										<?php endforeach;?>
   									</select>
								Cosecha
									<select id="idce" style="font-size:11px; margin-top: 1px; margin-left: -5px">
										<option value="0">-</option>
										<option value="1">Primera</option>
										<option value="2">Segunda</option>
										<option value="3">Tercera</option>
										<option value="4">Cuarta</option>
										<option value="5">Quinta</option>
										<option value="6">Sexta</option>
										<option value="7">Final</option>
									</select>
									Kgs. 
									<input type="text" name="kgscos" id="kgscos" style="width: 65px; font-size: 13px; text-align: center">
								</td>
								<td style="background-color: lightblue;">
									 <?php if($usuario=="Jesus Benítez" or $usuario=="Zuleima Benitez" ){ ?>
									<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="+" />
									<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
									 <?php } ?>
								</td>
							</tr>
							<tr style="height: 1px; background-color: white">
								<td colspan="7" style="height: 1px">
									<div class="ajaxSorterDiv" id="tabla_maq" name="tabla_maq" style="width: 760px"  >                 			
										<span class="ajaxTable" style="height: 75px; margin-top: 1px; " >
                    						<table id="mytablaMaq" name="mytablaMaq" class="ajaxSorter" border="1" style="width: 743px" >
                        						<thead >                            
                            						<th data-order = "ide" >No. Proceso</th>
                            						<th data-order = "fec" >Fecha</th>
                            						<th data-order = "nomg" >Procedencia-Granja</th>
                            						<th data-order = "cic"  >Precosecha</th>
                            						<th data-order = "kgscos"  >Kilogramos</th>
                            					</thead>
                        						<tbody title="Seleccione para realizar cambios" style="font-size: 14px">
                        						</tbody>                        	
                    						</table>
                						</span> 
             			 			</div>
								</td>
							</tr>
							<tr style="background-color: lightgray; color: black">
								<td style="background-color: lightblue; color: black">II.- Recibido Fco
								<input type="hidden" id="idef" name="idef" /><input type="hidden" id="idfe" name="idfe" />
								</td>
								<td>Est<input type="text" name="estf" id="estf" style="width: 50px; font-size: 15px; text-align: center"></td>
								<td colspan="2">Gramaje: Granja<input type="text" name="grgf" id="grgf" style="width: 50px; font-size: 15px; text-align: center">
									Planta<input type="text" name="grpf" id="grpf" style="width: 50px; font-size: 15px; text-align: center"></td>
								<td colspan="2">TOB: Num<input type="text" name="ntpf" id="ntpf" style="width: 50px; font-size: 15px; text-align: center">
									Kgs<input type="text" name="kgtf" id="kgtf" style="width: 50px; font-size: 15px; text-align: center" value="40"></td>
								<td style="background-color: lightblue; color: black">
									 <?php if($usuario=="Jesus Benítez" or $usuario=="Zuleima Benitez" ){ ?>
									<input style="font-size: 14px" type="submit" id="aceptarf" name="aceptarf" value="+" />
									<input style="font-size: 14px;" type="submit" id="nuevof" name="nuevof" value="Nuevo"  />
									<input style="font-size: 14px" type="submit" id="borrarf" name="borrarf" value="-" />
									 <?php } ?>
								</td>
							</tr>
							<tr style="height: 1px; background-color: white">
								<td colspan="7" style="height: 1px">
									<div class="ajaxSorterDiv" id="tabla_maqf" name="tabla_maqf" style="width: 760px"  >                 			
										<span class="ajaxTable" style="height: 155px; margin-top: 1px; " >
                    						<table id="mytablaMaqf" name="mytablaMaqf" class="ajaxSorter" border="1" style="width: 743px" >
                        						<thead>   
                        							<tr>                         
                            						<th data-order = "estf1" rowspan="2">Est</th>
                            						<th colspan="2" >Gramaje</th>
                            						<th colspan="2"  >TOB</th>
                            						<th >Kilogramos</th>
                            					
                            					</tr> 
                            					<tr>                          
                            						<th data-order = "grgf1" >Granja</th>
                            						<th data-order = "grpf1">Planta</th>
                            						<th data-order = "ntpf">Num</th>
                            						<th data-order = "kgtf">Kgs</th>
                            						<th data-order = "subf">Total</th>
                            						<!--<th data-order = "totf">Total</th>-->
                            					</tr>
                            					</thead>
                        						<tbody title="Seleccione para realizar cambios" style="font-size: 13px; text-align: center">
                        						</tbody>                        	
                    						</table>
                						</span> 
             			 			</div>
								</td>
							</tr>
							<tr style="background-color: lightgray; color: black">
								<td style="background-color: lightblue; color: black">III.- Procesado
								<input type="hidden" id="idep" name="idep" /><input type="hidden" id="idpe" name="idpe" />
								<input type="hidden" id="idgp" name="idgp" /><input type="hidden" id="idap" name="idap" />
								</td>
								<td colspan="5">
									<input placeholder="Dia" size="8%" type="text" name="fecr" id="fecr" class="fecha redondo mUp" style="text-align: center; font-size: 11px; margin-left: -8px;" >	
									<input placeholder="LOTE" type="text" name="lotp" id="lotp" style="width: 90px; font-size: 15px; text-align: center">
									
									<select name='idtp' id="idtp" style="font-size:10px; margin-top: 1px" >
                    					<option value="0">Talla</option>
          									<?php	
          										$data['result']=$this->maquila_model->getTallas();
												foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->idt;?>"><?php echo $row->nomt;?></option>
           										<?php endforeach;?>
   									</select>
   									<input placeholder="Grupo" type="hidden" name="gpop" id="gpop" style="width: 10px; font-size: 15px; text-align: center"   >
								<input placeholder="Master" type="text" name="masp" id="masp" style="width: 50px; font-size: 15px; text-align: center">
								Marq. Kgs.<input type="text" name="kgmp" id="kgmp" style="width: 40px; font-size: 15px; text-align: center" value="22">
								$<input type="text" name="prep" id="prep" style="width: 50px; font-size: 15px; text-align: center" value="15.00">
								<select id="moneda" style="font-size:11px; margin-top: 1px; margin-left: -5px">
									<option value="1">MN</option>
									<option value="2">USD</option>
								</select>
								<input placeholder="T.C."  type="text" name="tcm" id="tcm" style="width: 55px; font-size: 15px; text-align: center"  value="1" >
								</td>
								<td style="background-color: lightblue; color: black">
									 <?php if($usuario=="Jesus Benítez" or $usuario=="Zuleima Benitez" ){ ?>
									<input style="font-size: 14px" type="submit" id="aceptarp" name="aceptarp" value="+" />
									<input style="font-size: 14px;" type="submit" id="nuevop" name="nuevop" value="Nuevo"  />
									<input style="font-size: 14px" type="submit" id="borrarp" name="borrarp" value="-" />
									 <?php } ?>
								</td>
							</tr>
							<tr style="height: 1px; background-color: white">
								<td colspan="7" style="height: 1px">
									<div class="ajaxSorterDiv" id="tabla_maqp" name="tabla_maqp" style="width: 760px"  >   
										<span class="ajaxTable" style="height: 155px; margin-top: 1px; " >
                    						<table id="mytablaMaqp" name="mytablaMaqp" class="ajaxSorter" border="1" style="width: 743px" >
                        						<thead>   
                        							<th data-order = "fecr1">Dia</th>
                        							<th data-order = "lotp1">Lote</th>
                            						<th data-order = "nomt">Talla</th>
                            						<th data-order = "masp">Master</th>
                            						<th data-order = "kgmp">Marqueta <br /> Kgs.</th>
                            						<th data-order = "kgsp">Kgs</th>
                            						<!--<th data-order = "prep">Precio</th>-->
                            						<th data-order = "totp">Totales</th>
                            						<!--
                            						<th data-order = "ren">Rend. %</th>-->
                            					</thead>
                        						<tbody title="Seleccione para realizar cambios" style="font-size: 13px; text-align: center">
                        						</tbody>                        	
                    						</table>
                						</span> 
             			 			</div>
								</td>
							</tr>
							<tr style="background-color: lightgray; color: black">
								<td style="background-color: lightblue; color: black">IV.- Resultados</td>
								<td colspan="5">
									Fresco<input type="text" name="resf" id="resf" style="width: 100px; font-size: 15px; text-align: center">
									Procesado<input type="text" name="resp" id="resp" style="width: 100px; font-size: 15px; text-align: center">
									Rendimiento %
									<input type="text" name="ren" id="ren" style="width: 80px; font-size: 15px; text-align: center;">
									
								</td>
								<td style="background-color: lightblue; color: black;">
									<button id="export_data" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right; margin-top: -5px; margin-left: -5px"  >Proceso</button>
									<form id="data_tables" action="<?= base_url()?>index.php/maquila/pdfrepmaq" method="POST">
										<input type="hidden" name="tablaf" id="html_fre"/>
										<input type="hidden" name="tablap" id="html_pro"/>
										<input type="hidden" type="text" name="dia" id="dia" >
										<input type="hidden" type="text" name="gra" id="gra" >
										<input type="hidden" type="text" name="cic" id="cic" ><input type="hidden" type="text" name="impu" id="impu" >
										<input type="hidden" type="text" name="fre" id="fre" ><input type="hidden" type="text" name="pro" id="pro" >
										<input type="hidden" type="text" name="rend" id="rend" ><input type="hidden" type="text" name="imp" id="imp" >
									</form>
									<script type="text/javascript">
										$(document).ready(function(){
											$('#export_data').click(function(){									
												$('#html_fre').val($('#mytablaMaqf').html());
												$('#html_pro').val($('#mytablaMaqp').html());									
												$('#data_tables').submit();
												$('#html_fre').val('');
												$('#html_pro').val('');
											});
										});
									</script>
								</td>
							</tr>
							
						</table>
					
 		</div>
 		<div id="grafrendi" class="tab_content" style="margin-top: -1px">
 			Almacen
   			<select placeholder="Almacen" name="almacenr" id="almacenr" style="font-size:10px; margin-top: 1px" >
            	<option value="0">Todos</option>
            		<?php	
          				$data['result']=$this->maquila_model->getAlmacenes();
						foreach ($data['result'] as $row):?>
           				<option value="<?php echo $row->ida;?>"><?php echo $row->noma;?></option>
           				<?php endforeach;?>	
   			</select>   
 			Granja
            <?php if($usuario=="Zuleima Benitez" || $usuario=="Antonio Hernandez"){ ?>
            <select placeholder="Granja"  id="granjar" style="font-size:10px; margin-top: 1px" >
            	<option value="0">-</option>
          			<?php	
          				$data['result']=$this->maquila_model->getGranjas();
						foreach ($data['result'] as $row):?>
           				<option value="<?php echo $row->idg;?>"><?php echo $row->nomg;?></option>
           			<?php endforeach;?>
   			</select>
   			<?php }else{ ?>
   			<select placeholder="Granja"  id="granjar" style="font-size:10px; margin-top: 1px" >
            	<option value="4">OA-Ahome</option>
            </select>	
   			<?php } ?>	
   			<input type="hidden" id="almnom" name="almnom">
   			<input type="hidden" id="graf1" name="graf1" value="1"><input type="hidden" id="graf2" name="graf2" value="2">			
   			<div name="rendigra" id="rendigra" style="width: 950px; height: 535px; margin-left: -10px;"></div>
   			<div id='ver_rendi' class='hide' >
 			<div class="ajaxSorterDiv" id="tabla_rendi" name="tabla_rendi" style="width: 960px; margin-left: -10px"  >
				 <span class="ajaxTable" style="height: 350px;width:950px;" >
                	<table id="mytablaRenGraf" name="mytablaRenGraf" class="ajaxSorter" border="1"  >
                		<thead >     
                   			<th data-order = "fec2" ></th><th data-order = "reni" >Ren</th>
                   			<th data-order = "ren1" >71.0%</th><th data-order = "ren2" >72.0%</th>
                    	</thead>
                    	<tbody>
                    	</tbody>                        	
                    </table>
                </span>                  
            </div>
        </div>
 		</div>	
 		<div id="pagos" class="tab_content" style="margin-top: -1px">
 			<div class="ajaxSorterDiv" id="tabla_pagos" name="tabla_pagos" style="width: 960px; margin-left: -10px"  >
				<div class="ajaxpager" style="margin-top: -10px; margin-left: 5px" > 
            		<ul class="order_list" style="width: 950px; text-align: left" >
            			Gja
            			<?php if($usuario=="Zuleima Benitez" || $usuario=="Antonio Hernandez"){ ?>
            			<select placeholder="Granja"  id="granja" style="font-size:10px; margin-top: 1px" >
                   			<option value="0">-</option>
          						<?php	
          							$data['result']=$this->maquila_model->getGranjas();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->idg;?>"><?php echo $row->nomg;?></option>
           							<?php endforeach;?>
   						</select>
   					<?php }else{ ?>
   						<select placeholder="Granja"  id="granja" style="font-size:10px; margin-top: 1px" >
                   			<option value="4">OA-Ahome</option>
                   		</select>	
   					<?php } ?>	
   						Alm
   						<select placeholder="Almacen" id="almacen" style="font-size:10px; margin-top: 1px" >
                   			<option value="0">Todos</option>
                   			<?php	
          							$data['result']=$this->maquila_model->getAlmacenes();
									foreach ($data['result'] as $row):?>
           							<option value="<?php echo $row->ida;?>"><?php echo $row->noma;?></option>
           					<?php endforeach;?>	
   						</select>
   						<?php if($usuario=="Zuleima Benitez" ){ ?>
            			Fecha
            			<input type="text" readonly="true" name="fecg" id="fecg" style="width: 80px; font-size: 15px; text-align: center; border: none;">
            			Lote
            			<input type="text" readonly="true" name="lotpg" id="lotpg" style="width: 120px; font-size: 15px; text-align: center; border: none; ">
            			Factura   
            			<input type="text" name="fac" id="fac" style="width: 100px; font-size: 15px; text-align: center; color: blue">   
            			<input type="hidden" name="mone" id="mone"/>
            			<input style="font-size: 14px" type="submit" id="aceptarg" name="aceptarg" value="+" />
            			<button id="export_datagme" type="button" style="width:25px; cursor: pointer; background-color: lightyellow; text-align: right; margin-left: 5px"  ></button>
						<form id="data_tablesgme" action="<?= base_url()?>index.php/maquila/pdfreppagos" method="POST">
							<input type="hidden" name="tablapag" id="html_gralpag"/>
							<input type="hidden" name="tablapaga" id="html_gralpaga"/>
							<input type="hidden" name="gran" id="gran"/>
						</form>
						
						<script type="text/javascript">
							$(document).ready(function(){
								$('#export_datagme').click(function(){									
									$('#html_gralpag').val($('#mytablaPagos').html());
									$('#html_gralpaga').val($('#mytablaPagosa').html());
									$('#data_tablesgme').submit();
									$('#html_gralpag').val('');
									$('#html_gralpaga').val('');
								});
							});
						</script>
            			<?php } ?>
                   	</ul>
                   	
 				</div>             			
				<span class="ajaxTable" style="height: 350px;width:950px;" >
                	<table id="mytablaPagos" name="mytablaPagos" class="ajaxSorter" border="1"  >
                		<thead >     
                   			<th data-order = "fec1" >Fecha</th>
                   			<th data-order = "nomg" >Granja</th>
                   			<th data-order = "kgscos" >Cosechados</th>
                   			<th data-order = "noma" >Almacen</th>
                   			<th data-order = "kgsf" >Recibidos</th>
                   			<th data-order = "dif" >Dif</th>
                   			<th data-order = "lotp1" >Lote</th> 
                           	<th data-order = "kgsp" >Procesados</th>
                           	<th data-order = "reni" >Ren</th>
                           	<th data-order = "impi" >Imp. Sin IVA</th>
                           	<th data-order = "imp" >Imp. Neto</th> 
                           	<th data-order = "fac" >Factura</th>                          
                    	</thead>
                    	<tbody title="Seleccione para realizar cambios" style="font-size: 11px; text-align: center">
                    	</tbody>                        	
                    </table>
                </span> 
                 
            </div>
            <div class="ajaxSorterDiv" id="tabla_pagosa" name="tabla_pagosa" style="width: 960px; margin-top: -5px;margin-left: -10px"  >
				<span class="ajaxTable" style="height: 155px;width:950px; margin-top: 1px" >
                	<table id="mytablaPagosa" name="mytablaPagosa" class="ajaxSorter" border="1"  >
                		<thead > 
                			<tr>
                			<th rowspan="2" data-order = "noma" >Concentrado por Almacen</th>
                			<th rowspan="2" data-order = "ren" >Ren</th>
                   			<th colspan="2"> Kilogramos</th>
                   			<th style="background-color: blue; color: white" > MN</th>
                   			<th style="background-color: blue; color: white"colspan="2"> Pagos</th>
                   			<th style="background-color: blue; color: white"colspan="2"  >Adeudos</th>
                   			<th style="background-color: green; color: white" > USD</th>
                   			<th style="background-color: green; color: white"colspan="2"> Pagos</th>
                   			<th style="background-color: green; color: white"colspan="2"  >Adeudos</th>
                			</tr>
                			<tr>
                			<th data-order = "fco" >Fcos</th>
                   			<th data-order = "kgsm" >Proc</th>
                   			<th style="background-color: blue; color: white" data-order = "cospmn" >Costo</th>    
                   			<th style="background-color: blue; color: white" data-order = "kgspmn" >Kgs </th>
                   			<th style="background-color: blue; color: white" data-order = "pagpmn" >Importe</th>
                   			<th style="background-color: blue; color: white" data-order = "deukmn" >Kgs</th> 
                           	<th style="background-color: blue; color: white" data-order = "deupmn" >Importe</th>  
                           	<th style="background-color: green; color: white" data-order = "cospusd" >Costo</th>    
                   			<th style="background-color: green; color: white" data-order = "kgspusd" >Kgs </th>
                   			<th style="background-color: green; color: white" data-order = "pagpusd" >Importe</th>
                   			<th style="background-color: green; color: white" data-order = "deukusd" >Kgs</th> 
                           	<th style="background-color: green; color: white" data-order = "deupusd" >Importe</th> 
                           	</tr>
                        </thead>
                    	<tbody title="Seleccione para realizar cambios" style="font-size: 11px; text-align: center">
                    	</tbody>                        	
                    </table>
                </span> 
                <div class="ajaxpager" style="margin-top: 1px;" > 
            		<ul class="order_list" style="width: 650px; text-align: left" >
            			
                   	</ul>
                   	
 				</div>  
            </div>
        </div>
        <div id="fleseg" class="tab_content" style="margin-top: -1px">
        	<div style="width: 880px">
			<table style="margin-left: -25px; margin-top: -5px" >
				<tr>
					<th>
						<table class="ajaxSorter" border="1" style="margin-left: -1px" >
							<thead>	<th>Tipo</th><th>Fecha</th><th>Factura</th><th>Proveedor</th>
								<th>Importe</th><th>Moneda</th><th>TC</th>
							</thead>
							<tbody>
									<th style="text-align: center">
										<input type="hidden" readonly="true" size="2%"  type="text" name="idfle" id="idfle">
										<select name="tipfle" id="tipfle" style="font-size: 10px; height: 25px; margin-top: 1px " >
											<option value="0">Sel</option>
											<option value="1">Flete</option>
											<option value="2">Seguro</option>
											
           								</select>
									</th>
									<th>    
										<input size="10%" type="text" name="fecfle" id="fecfle" class="fecha redondo mUp" style="text-align: center;font-size: 14px" >
									</th>	
                   					 
									<th>
										<input size="12%" type="text" name="folfle" id="folfle" style="margin-left: 1px; font-size: 14px" >
									</th>
									<th>
											<datalist id="prove" style="font-size:11px;width: 420px" >
                    							<option value="-"></option>
          										<?php	
          										//echo ucwords(strtolower($row->Razon));
           										$data['result']=$this->maquila_model->getProves();
												foreach ($data['result'] as $row):?>
           										<option value="<?php echo $row->Numero;?>"><?php echo $row->Razon;?></option>
           										<?php endforeach;?>
   											</datalist>
   											<input list="prove" id="profle" type="text" style="font-size:11px;width: 420px"> 
									</th>
									<th><input size="12%" type="text" name="impfle" id="impfle" style="text-align: right; margin-left: 1px; font-size: 14px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" ></th>
									<th style="text-align: center">
											<select name="monfle" id="monfle" style="font-size: 10px; height: 25px; margin-top: 1px " >
												<option value="1">M.N.</option>
												<option value="2">U.S.D.</option>
											</select>
									</th>
									<th><input size="2%" type="text" name="tcfle" id="tcfle" style="text-align: right; margin-left: 1px; font-size: 14px" value="1"  ></th>
									
									</tr>
							</tbody>
							
							<tfoot>	
								<th colspan="7" style="text-align: center">
									<?php if($usuario=="Zuleima Benitez" ){ ?>
									<input type="submit" id="nuevofst" name="nuevofst" value="Nuevo"  />
									<input type="submit" id="aceptarfst" name="aceptarfst" value="Guardar" />
									<input type="submit" id="borrarfst" name="borrarfst" value="Borrar" />
									<?php } ?>
								</th>
							</tfoot>	
						</table>
					</th>
				</tr>
				<tr>
					
					<th>
						<table border="0" style="margin-left: 15px; margin-top: 1px; ">
							<tr>
				 				<th >				 		
                 					<div class="ajaxSorterDiv" id="tabla_fle" name="tabla_fle" style="width: 900px"  >                 			
										<span class="ajaxTable" style="height: 470px; margin-top: 1px; " >
                    						<table id="mytablaFle" name="mytablaFle" class="ajaxSorter" border="1" style="width: 883px" >
                        						<thead>                            
			                            			<th data-order = "tipfle1" >Tipo</th>
			                            			<th data-order = "fecfle1" >Fecha</th>
			                            			<th data-order = "folfle" >Factura</th>
			                            			<th data-order = "Numero"  >No.</th>
            			                			<th data-order = "Razon"  >Proveedor</th>
                        			    			<th data-order = "monfle1"  >Moneda</th>
                        			    			<th data-order = "impfle1"  >Costo MXN</th>
			                            		</thead>
                        						<tbody title="Seleccione para realizar cambios" style="font-size: 14px">
                        						</tbody>                        	
                    						</table>
                						</span> 
             			 				<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
											<form action="<?= base_url()?>index.php/maquila/pdfrepfst" method="POST" >							
    	    	            					<ul class="order_list" style="width: 150px; margin-top: 1px">
    	    	            					</ul>
    	    	            					<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    				<input type="hidden" name="tablafst" value ="" class="htmlTable"/> 
											</form>   
                						</div>       	                	
	                				</div>	                	
            					</th>
            				</tr>
						</table>	
					</th>
				</tr>
			</table>
			</div>
        </div>	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#nuevofst").click( function(){	
	$("#idfle").val('');$("#tipfle").val('');$("#folfle").val('');$("#profle").val('');$("#monfle").val('1');$("#tcfle").val('1');
	$("#fecfle").val('');$("#impfle").val('');
	$("#tipfle").focus();
	return true;
});
$("#borrarfst").click( function(){	
	numero=$("#idfle").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/borrar", 
						data: "id="+$("#idfle").val()+"&tabla=maqflesegtra"+"&campo=idfle"+"&cic="+$("#ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#nuevofst").click();
										$("#mytablaFle").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Flete o Seguro para poder Eliminar");
		return false;
	}
});
$("#aceptarfst").click( function(){		
	numero=$("#idfle").val();
	tip=$("#tipfle").val();fec=$("#fecfle").val();
	if( tip>0){
	if( fec!=''){	
	  if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/actualizarfst", 
						data: "id="+$("#idfle").val()+"&fec="+$("#fecfle").val()+"&fol="+$("#folfle").val()+"&pro="+$("#profle").val()+"&mon="+$("#monfle").val()+"&tc="+$("#tcfle").val()+"&imp="+$("#impfle").val()+"&tip="+$("#tipfle").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevofst").click();
										$("#mytablaFle").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/agregarfst", 
						data: "cic="+$("#ciclo").val()+"&fec="+$("#fecfle").val()+"&fol="+$("#folfle").val()+"&pro="+$("#profle").val()+"&mon="+$("#monfle").val()+"&tc="+$("#tcfle").val()+"&imp="+$("#impfle").val()+"&tip="+$("#tipfle").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevofst").click();
										$("#mytablaFle").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		}else{
		alert("Error: Seleccione la Fecha");	
		$("#fecfle").focus();
		return false;
	}				
	}else{
		alert("Error: Seleccione el tipo");	
		$("#tipfle").focus();
		return false;
	}	
});

$("#moneda").change( function(){
	if($("#moneda").val()=='1') $("#tcm").val('1');
	else{$("#tcm").val('');}
	$("#tcm").focus();				
});
$("#idtp").change( function(){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/maquila/buscargpo", 
			data: "idt="+$("#idtp").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#gpop").val(obj.gpop);
   			} 
	});	
	$("#masp").focus();				
});
$("#almacenr").change( function(){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/maquila/buscaralm", 
			data: "id="+$("#almacenr").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#almnom").val(obj.almnom);
   			} 
	});	
	
});
$("#granja").change( function(){
	$("#gran").val($("#granja").val());	 				
});

$("#granja").change( function(){
	$("#gran").val();
	}
);
$("#aceptarg").click( function(){	
	//lote=$("#lotpg").val($("#granja").val());
	lote=$("#lotpg").val();
	if(lote!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/actualizarg", 
						data: "lot="+$("#lotpg").val()+"&fac="+$("#fac").val()+"&cic="+$("#ciclo").val()+"&mon="+$("#mone").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos Actualizados correctamente");
										$("#mytablaPagos").trigger("update");
										$("#fecg").val('');
										$("#lotpg").val('');
										$("#fac").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Datos En Filas de Tipo de Moneda para poder Agregar Factura");
		return false;
	}
});
$("#aceptarp").click( function(){		
	numero=$("#idep").val();
	num=$("#idtp").val();mas=$("#masp").val();
	if( num>0){
	if( mas>0){	
	  if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/actualizarmaqp", 
						data: "id="+$("#idep").val()+"&lot="+$("#lotp").val()+"&idt="+$("#idtp").val()+"&mas="+$("#masp").val()+"&kgm="+$("#kgmp").val()+"&idg="+$("#idgp").val()+"&ida="+$("#idap").val()+"&idp="+$("#idpe").val()+"&pre="+$("#prep").val()+"&fec="+$("#fece").val()+"&cic="+$("#ciclo").val()+"&mon="+$("#moneda").val()+"&tcm="+$("#tcm").val()+"&gpop="+$("#gpop").val()+"&fecr="+$("#fecr").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevop").click();
										$("#mytablaMaqf").trigger("update");
										$("#mytablaMaqp").trigger("update");
										$("#mytablaPagos").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/agregarmaqp", 
						data: "lot="+$("#lotp").val()+"&idt="+$("#idtp").val()+"&mas="+$("#masp").val()+"&kgm="+$("#kgmp").val()+"&idg="+$("#idgp").val()+"&ida="+$("#idap").val()+"&idp="+$("#idpe").val()+"&pre="+$("#prep").val()+"&fec="+$("#fece").val()+"&cic="+$("#ciclo").val()+"&mon="+$("#moneda").val()+"&tcm="+$("#tcm").val()+"&gpop="+$("#gpop").val()+"&fecr="+$("#fecr").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevop").click();
										$("#mytablaMaqf").trigger("update");
										$("#mytablaMaqp").trigger("update");
										$("#mytablaPagos").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		}else{
		alert("Error: Registre los Kgs Procesados");	
		$("#masp").focus();
		return false;
	}				
	}else{
		alert("Error: Seleccione La Talla");	
		$("#idtp").focus();
		return false;
	}	
});

$("#borrarp").click( function(){	
	numero=$("#idep").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/borrar", 
						data: "id="+$("#idep").val()+"&tabla=maqpro_"+"&campo=idep"+"&cic="+$("#ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#nuevop").click();
										$("#mytablaMaqp").trigger("update");																																				
										$("#mytablaMaqf").trigger("update");
										$("#mytablaPagos").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Biometria para poder Eliminarla");
		return false;
	}
});
$("#aceptarf").click( function(){		
	numero=$("#idef").val();
	num=$("#ntpf").val();
	if( num>0){
	  if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/actualizarmaqf", 
						data: "id="+$("#idef").val()+"&est="+$("#estf").val()+"&grg="+$("#grgf").val()+"&grp="+$("#grpf").val()+"&ntp="+$("#ntpf").val()+"&kgt="+$("#kgtf").val()+"&idf="+$("#idfe").val()+"&cic="+$("#ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevof").click();
										$("#mytablaMaqf").trigger("update");
										$("#mytablaMaqp").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/agregarmaqf", 
						data: "est="+$("#estf").val()+"&grg="+$("#grgf").val()+"&grp="+$("#grpf").val()+"&ntp="+$("#ntpf").val()+"&kgt="+$("#kgtf").val()+"&idf="+$("#idfe").val()+"&cic="+$("#ciclo").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevof").click();
										$("#mytablaMaqf").trigger("update");
										$("#mytablaMaqp").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
	}else{
		alert("Error: Regsitre el Numero de TOB");	
		$("#ntpf").focus();
		return false;
	}
});
$("#borrarf").click( function(){	
	numero=$("#idef").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/borrar", 
						data: "id="+$("#idef").val()+"&tabla=maqfco_"+"&campo=idef"+"&cic="+$("#ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#nuevof").click();
										$("#mytablaMaqf").trigger("update");
										$("#mytablaMaqp").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Biometria para poder Eliminarla");
		return false;
	}
});



$("#aceptar").click( function(){		
	numero=$("#ide").val();
	gra=$("#idge").val();
	//if( gra!='0'){
	  if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/actualizarmaq", 
						data: "id="+$("#ide").val()+"&fec="+$("#fece").val()+"&alm="+$("#idae").val()+"&cos="+$("#idce").val()+"&gra="+$("#idge").val()+"&cic="+$("#ciclo").val()+"&kgc="+$("#kgscos").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevo").click();
										$("#mytablaMaq").trigger("update");
										$("#mytablaMaqf").trigger("update");
										$("#mytablaMaqp").trigger("update");
										$("#mytablaPagos").trigger("update");
										
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maquila/agregarmaq", 
						data: "fec="+$("#fece").val()+"&alm="+$("#idae").val()+"&cos="+$("#idce").val()+"&gra="+$("#idge").val()+"&cic="+$("#ciclo").val()+"&kgc="+$("#kgscos").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#nuevo").click();
										$("#mytablaMaq").trigger("update");
										$("#mytablaMaqf").trigger("update");
										$("#mytablaMaqp").trigger("update");
										$("#mytablaPagos").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
	/*}else{
		alert("Error: Selecciones Procedencia");	
		$("#idge").focus();
		return false;
	}*/
});
$("#nuevo").click( function(){	
	$("#ide").val('');$("#fece").val('');$("#idge").val('0');$("#idce").val('0');$("#kgscos").val('');
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	$("#fece").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#fecr").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#nuevof").click();$("#nuevop").click()
   	$("#mytablaMaq").trigger("update");
   	$("#mytablaMaqf").trigger("update");
	$("#mytablaMaqp").trigger("update");
	$("#lotp").val('');
	return true;
});
$("#nuevop").click( function(){	
	$("#idep").val('');$("#idtp").val('0');$("#masp").val('');$("#tcm").val('1');
	$("#kgsp").val('');//$("#idgp").val('');$("#idap").val('');
	$("#kgmp").val('22');
	$("#lotp").focus();
	return true;
});
$("#nuevof").click( function(){	
	$("#idef").val(''); //$("#estf").val('');$("#grgf").val('');$("#grpf").val('');
	$("#ntpf").val('');$("#kgtf").val('');
	$("#ntpf").focus();
	return true;
});
$("#fecfle").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2018:+0",	
});
$("#fecr").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2018:+0",	
});
$("#fece").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2018:+0",
	onSelect: function( selectedDate ){
		$("#ide").val('');$("#idge").val('0');$("#idpe").val('0');
		$("#mytablaMaq").trigger("update");$("#mytablaMaqf").trigger("update");$("#mytablaMaqp").trigger("update");
	}
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});

$(document).ready(function(){ 
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	$("#fece").val(f.getFullYear() + "-" + mes + "-" + dia);$("#fecr").val(f.getFullYear() + "-" + mes + "-" + dia);
	$("#gran").val(0);	 $('#ver_rendi').hide();	
	$("#tabla_maq").ajaxSorter({
		url:'<?php echo base_url()?>index.php/maquila/tablamaq',  		
		filters:['fece','ciclo'],
		sort:false,
    	onRowClick:function(){
    		$("#ide").val($(this).data('ide'));$("#idfe").val($(this).data('ide'));$("#idpe").val($(this).data('ide'));			
    		$("#idge").val($(this).data('idg'));$("#idgp").val($(this).data('idg'));
    		$("#idae").val($(this).data('ida'));$("#idap").val($(this).data('ida'));
    		$("#idce").val($(this).data('idce'));
    		$("#dia").val($(this).data('fec'));$("#kgscos").val($(this).data('kgscos'));
    		$("#gra").val($(this).data('nomg'));
    		$("#cic").val($(this).data('cic'));
    		
    		$("#tabla_maqf").ajaxSorter({
				url:'<?php echo base_url()?>index.php/maquila/tablamaqf',  		
				filters:['ide','ciclo'],
				sort:false,
				onRowClick:function(){
					if($(this).data('estf1')!='Total:'){
    					$("#idef").val($(this).data('idef'));			
    					$("#estf").val($(this).data('estf'));
    					$("#grgf").val($(this).data('grgf'));
    					$("#grpf").val($(this).data('grpf'));
    					$("#ntpf").val($(this).data('ntpf'));
    					$("#kgtf").val($(this).data('kgtf'));
    					$("#idfe").val($(this).data('idfe'));
    				}
    			},
				onSuccess:function(){
    				$('#tabla_maqf tr').map(function(){	   
    					if ($(this).data('totf')!=''){ 		
	    					$("#resf").val($(this).data('subf')+' kgs');
	    					$("#fre").val($(this).data('subf')+' kgs');
	    				}
	    		   	});	  	
    			},
   			});
   			$("#tabla_maqp").ajaxSorter({
				url:'<?php echo base_url()?>index.php/maquila/tablamaqp',  		
				filters:['ide','ciclo'],
				sort:false,
				onRowClick:function(){
					if($(this).data('lotp1')!='Total:'){
	    				//$(this).css('background','lightblue');	    				    				    			    			
	    				$("#idep").val($(this).data('idep'));			
    					$("#lotp").val($(this).data('lotp'));
    					$("#idtp").val($(this).data('idtp')); 
    					$("#masp").val($(this).data('masp'));
    					$("#kgmp").val($(this).data('kgmp'));    				
    					$("#prep").val($(this).data('prep'));
    					$("#moneda").val($(this).data('moneda'));
    					$("#tcm").val($(this).data('tcm'));
    					$("#gpop").val($(this).data('gpop'));
    					$("#fecr").val($(this).data('fecr'));

    				} 
    			},
				onSuccess:function(){
    				$('#tabla_maqp tr').map(function(){	  
    					if ($(this).data('kgsp')!=''){ 		
	    					$("#resp").val($(this).data('kgsp1')+' kgs');
	    					$("#pro").val($(this).data('kgsp1')+' kgs');
	    				}
	    				$("#ren").val($(this).data('ren'));
	    				var inputVal = document.getElementById("ren");
    					if (inputVal.value >= 70) {
        					inputVal.style.backgroundColor = "#070";
        					inputVal.style.color = "white";
    					}
    					else if(inputVal.value >= 69.5 && inputVal.value <= 69.99){
        					inputVal.style.backgroundColor = "orange";
        					inputVal.style.color = "white";
    					}else if(inputVal.value < 69.5){
    						inputVal.style.backgroundColor = "red";
    						inputVal.style.color = "white";
    					}
	    				$("#ren").val($(this).data('ren')+' %');
	    				$("#rend").val($(this).data('ren')+' %');
	    				$("#imp").val($(this).data('totpm'));
	    				$("#impu").val($(this).data('totpu'));
	    		   	});	
	    		   	$('#tabla_maqp tbody tr').map(function(){	    		
	    				if($(this).data('moneda')=='2'){
	    					$(this).css('background','lightgreen');
	    					 $(this).css('font-weight','bold');   				    				    			    			
	    				}    
			    	});	
			    	$('#tabla_maqp tbody tr').map(function(){	    		
	    				if($(this).data('masp')=='MN' ){
	    					 $(this).css('font-weight','bold');  			    				    			    			
	    				}    
	    				if($(this).data('masp')=='USD'){
	    					$(this).css('background','lightgreen'); 	 $(this).css('font-weight','bold');    				    				    			    			
	    				}
	    			});   	
    			},
   			});
    	},   
   });
   $("#tabla_pagos").ajaxSorter({
		url:'<?php echo base_url()?>index.php/maquila/tablapag',  		
		filters:['granja','ciclo','almacen','graf1'],
		active:['fac',''],
		sort:false,            
    	//onLoad:false,  
    	onRowClick:function(){   
    		if($(this).data('moneda')>0){ 						
    			$("#fecg").val($(this).data('fec'));$("#lotpg").val($(this).data('lotp'));$("#fac").val($(this).data('fac'));$("#fac").focus();	
    			$("#mone").val($(this).data('moneda'));		
    		}else{
    			$("#fecg").val('');$("#lotpg").val('');$("#fac").val('');$("#mone").val('');	
    		};
    	}, 
    	onSuccess:function(){ 
    		$('#tabla_pagos tbody tr').map(function(){	   
    			if($(this).data('fec1')!='Totales' && $(this).data('fec1')!=''){
	    			$(this).css('font-weight','bold');  				    				    			    			
	    		} 		
	    		if($(this).data('fec1')=='Totales'){
	    			$(this).css("background-color","lightgray");$(this).css('font-weight','bold');  $(this).css('font-size','12px'); 				    				    			    			
	    		}    
	    		if($(this).data('lotp1')=='Pagos'){
	    			$(this).css("background-color","lightyellow");$(this).css('font-weight','bold'); $(this).css('font-size','12px');   				    				    			    			
	    		}
	    		if($(this).data('lotp1')=='Se Debe'){
	    			$(this).css("background-color","lightyellow");$(this).css('font-weight','bold'); $(this).css('font-size','12px');   				    				    			    			
	    		}
	    		if($(this).data('lotp1')=='PAGADOS'){
	    			$(this).css("background-color", "lightblue"); $(this).css("color", "white");$(this).css('font-weight','bold');$(this).css('font-size','12px');   				    				    			    			
	    		}
	    		if($(this).data('dif')=='MN' || $(this).data('dif')=='USD'){
	    			$(this).css('font-weight','bold');$(this).css('font-size','12px');   				    				    			    			
	    		}
	    	});	
	    	$('#tabla_pagos tbody tr td:nth-child(9)').map(function(){ 
	    		$(this).css('font-weight','bold');
	    			if($(this).html() >= 70.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 69.5 && $(this).html() <= 69.99 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() < 69.5 && $(this).html() != '' ) {
              			$(this).css("background-color", "red"); $(this).css("color", "black");
        			}
        			
	    	}); 
	    	$('#tabla_pagos tbody tr td:nth-child(12)').map(function(){ 
	    		if($(this).html() != '-' && $(this).html() != '' ) {
              				$(this).css("color", "blue");
        			}
	    	});   	  	
	    },	
	});
	$("#tabla_pagosa").ajaxSorter({
		url:'<?php echo base_url()?>index.php/maquila/tablapaga',  		
		filters:['granja','ciclo','almacen'],
		sort:false,            
    	
    	onSuccess:function(){ 
    		$('#tabla_pagosa tbody tr').map(function(){	    		
	    		if($(this).data('noma')=='Totales'){
	    			$(this).css('background','lightgray');	 $(this).css('font-weight','bold');  $(this).css('font-size','11px'); 				    				    			    			
	    		}    
	    		
	    	});
	    	$('#tabla_pagosa tbody tr td:nth-child(2)').map(function(){ 
	    		$(this).css('font-weight','bold');
	    			if($(this).html() >= 70.0 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() >= 69.5 && $(this).html() <= 69.99 &&$(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() <= 69.5 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "black");
        			}
        			
	    	});  	
	    		  	
	    },	
	});
	
	$("#tabla_fle").ajaxSorter({
		url:'<?php echo base_url()?>index.php/maquila/tablafle',  
		filters:['ciclo','tipfle','profle'],
        active:['monfle','2'],  
        //multipleFilter:true,
        sort:false,
        onRowClick:function(){
        	if($(this).data('idfle')>0){
            	$("#idfle").val($(this).data('idfle'));
            	$("#tipfle").val($(this).data('tipfle'));
            	$("#fecfle").val($(this).data('fecfle'));
            	$("#folfle").val($(this).data('folfle'));
            	$("#profle").val($(this).data('profle'));
            	$("#monfle").val($(this).data('monfle'));
            	$("#tcfle").val($(this).data('tcfle'));
            	$("#impfle").val($(this).data('impfle'));
           }
        },
       onSuccess:function(){
       		$('#tabla_fle tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
       		$('#tabla_fle tbody tr').map(function(){
		    	if($(this).data('tipfle1')=='Total') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');
	    		}
		    });
    	}
    });

    $("#tabla_rendi").ajaxSorter({
		url:'<?php echo base_url()?>index.php/maquila/tablapag',  		
		filters:['granjar','ciclo','almacenr','graf2'],
		active:['fac',''],
		sort:false,            
    	//onLoad:false,  
    	onRowClick:function(){   
    		
    	}, 
    	onSuccess:function(){ 
    		alm=$("#almnom").val();
    		texto='Rendimientos '+alm;
		        	//rojo=50; naranja=55;
					Highcharts.chart('rendigra', {
   				 		data: {table: 'mytablaRenGraf'},
    			 		chart: {type: 'spline'},
    			 		title: {text: texto,align: 'left'},
    			 		//subtitle: { text: conge+granja,   align: 'left'},
    			 		xAxis: {
		    			 		labels: {style: {fontSize: '10px',color: 'black',}}
    					 },
    					 yAxis: { title: {text: ' '}, min: 67,
        						//labels: {format: 'D'  }		
    					},
    					tooltip: {
        					formatter: function () {
            									return '<b>' + this.series.name + '</b><br/>' +
                								this.point.y + ' ' + this.point.name.toLowerCase();
        										}
    					},
    					
						  series: [{
						  		type: 'spline',
        						color:  'lightblue',
        						dataLabels: { enabled: true,
        									align: 'center',
            								y: 5, // 10 pixels down from the top
        						},
        						zones: [{ value: 71, color: 'red'}, { value: 72, color: 'orange'}, {color: 'lime'}],
						  },{
						  	type: 'line',color:  'orange',
						  },{
						  	type: 'line',color:  'lime',
						  }
						  ],		
					});
	    },	
	});
}); 
</script>
