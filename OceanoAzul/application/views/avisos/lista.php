<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
#export_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li ><a href="#consultas"><img src="<?php echo base_url();?>assets/images/menu/sagarpa3.png" width="24" height="24" border="0"> Busquedas</a></li>
		<li ><a href="#avisos"><img src="<?php echo base_url();?>assets/images/menu/sagarpa3.png" width="24" height="24" border="0"> Avisos de Cosecha</a></li>		
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 572px"  >
		<div id="consultas" class="tab_content" >
			Buscar del Día <input size="12%" type="text" name="txtFI" id="txtFI" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
             &nbsp; al <input size="12%" type="text" name="txtFF" id="txtFF"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
		<div class="ajaxSorterDiv" id="tabla_bus" name="tabla_bus" style="width: 860px"  >                 			
			<span class="ajaxTable" style="height: 500px; " >
        		<table id="mytablaBus" name="mytablaBus" class="ajaxSorter" border="1" style="width: 843px" >
        			<thead >                            
        				<th data-order = "fec" >Fecha</th>
                   		<th data-order = "folavi" >Folio</th>
                   		<th data-order = "Edo" >Estado</th>
                   		<th data-order = "Razon" >Razon Social</th>
                   		<th data-order = "canavi" >Cantidad</th>
                   		<th data-order = "canent" >Entregados</th>
                   		<th data-order = "dif" >Diferencia</th>
                   	</thead>
                	<tbody title="Seleccione para realizar cambios" style="font-size: 14px">
                	</tbody>                        	
                </table>
            </span> 
         </div>
	</div>
		<div id="avisos" class="tab_content" >
			<table border="2" style="font-size: 12px;" width="900px" >
				<tr style="background-color: lightgray; color: black">
					<td style="background-color: lightblue;">I.- Ciclo
						<select id="ciclo" style="font-size:13px; margin-top: 1px;" >
            				<option value="18">2018</option>
   						</select>
						<input type="hidden" id="idavi" name="idavi" />
   					</td>
					<td colspan="5">Día<input size="9%" type="text" name="fecavi" id="fecavi" class="fecha redondo mUp" style="text-align: center; font-size: 13px" >
						Folio<input type="text" name="folavi" id="folavi" style="width: 80px; font-size: 15px; text-align: center; background-color: blue; color: white">	
						$<input type="text" name="preavi" id="preavi" style="width: 60px; font-size: 15px; text-align: center">
						Pesca
						<input type="text" name="delavi" id="delavi" style="width: 320px; font-size: 15px; text-align: center" value="Victor Manuel Martinez, Enc. Oficina de Pesca.">	
					</td>
					<td style="background-color: lightblue;">
						<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="+" />
						<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
					</td>
				</tr>
				<tr style="height: 1px; background-color: white">
					<td colspan="7" style="height: 1px">
						<div class="ajaxSorterDiv" id="tabla_avi" name="tabla_avi" style="width: 860px"  >                 			
							<span class="ajaxTable" style="height: 150px; margin-top: 1px; " >
                				<table id="mytablaAvi" name="mytablaAvi" class="ajaxSorter" border="1" style="width: 843px" >
                					<thead >                            
                   						<th data-order = "fec" >Fecha</th>
                   						<th data-order = "folavi" >Folio</th>
                   						<th data-order = "can" >Can. Orgs.</th>
                   						<th data-order = "pre" >Precio Venta</th>
                   						<th data-order = "delavi" >Delegado Pesca</th>
                   					</thead>
                					<tbody title="Seleccione para realizar cambios" style="font-size: 14px">
                					</tbody>                        	
                				</table>
                			</span> 
             			</div>
					</td>
				</tr>
				<tr style="background-color: lightgray; color: black">
					<td style="background-color: lightblue; color: black">II.- Asignación de Clientes
						<input type="hidden" id="idavic" name="idavic" /><input type="hidden" id="cliavi" name="cliavi" /><input type="hidden" id="idavicli" name="idaviacli" />
					</td>
					<td colspan="5">
						<input type="text" name="cliente" id="cliente" style="width: 490px; font-size: 14px; text-align: center">
						Cant. Orgs.<input type="text" name="canavi" id="canavi" style="width: 80px; font-size: 14px; text-align: center">
					</td>
					<td style="background-color: lightblue; color: black">
						<input style="font-size: 14px" type="submit" id="aceptarf" name="aceptarf" value="+" />
						<input style="font-size: 14px;" type="submit" id="nuevof" name="nuevof" value="Nuevo"  />
						<input style="font-size: 14px" type="submit" id="borrarf" name="borrarf" value="-" />
					</td>
				</tr>
				<tr style="height: 1px; background-color: white">
					<td colspan="7" style="height: 1px">
						<div class="ajaxSorterDiv" id="tabla_avic" name="tabla_avic" style="width: 860px"  >                 			
							<span class="ajaxTable" style="height: 135px; margin-top: 1px; " >
                				<table id="mytablaAviC" name="mytablaAviC" class="ajaxSorter" border="1" style="width: 843px" >
                					<thead>   
                						<tr>  
                							<th data-order = "folio" >Aviso</th>                         
                       						<th data-order = "Razon" >Unidad Receptora</th>
                       						<th data-order = "Edo" >Estado</th>
                       						<th data-order = "canavi">Cantidad de Organismos Recibidos</th>                       						
                       					</tr>
                   					</thead>
                					<tbody title="Seleccione para realizar cambios" style="font-size: 13px; text-align: center">
                					</tbody>                        	
                				</table>
                			</span> 
                		</div>
             			Clientes - Unidad Receptora
                			<div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" style="width: 860px; margin-top: 1px">                
               				 		<span class="ajaxTable" style="margin-left: -5px; background-color: white; height: 100px">
                    					<table id="mytablaCli" name="mytablaCli" class="ajaxSorter">
                        					<thead title="Presione las columnas para ordenarlas como requiera">                            
                           						<th data-order = "Razon" >Razon Social</th>  
                           						<th data-order = "Edo" >Estado</th>
                           						<th data-order = "con" >Representante</th>                                                                                  
                        					</thead>
                        					<tbody title="Seleccione para asignar cliente a Remision" style="font-size: 11px">                                                 	  
                        					</tbody>
                    					</table>
                						</span> 
            				</div>
					</td>
				</tr>
		</table>
	</div>
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#aceptarf").click( function(){		
	num=$("#idavicli").val();can=$("#canavi").val();cli=$("#cliavi").val();numero=$("#idavic").val();
	if( num>0){
	if(cli!=''){
	if(can!=''){
	  if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/avisos/actualizaravif", 
						data: "id="+$("#idavic").val()+"&cli="+$("#cliavi").val()+"&can="+$("#canavi").val()+"&clia="+$("#idavicli").val()+"&cic="+$("#ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevof").click();
										$("#mytablaAvi").trigger("update");
										$("#mytablaAviC").trigger("update");
										$("#mytablaBus").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/avisos/agregaravif", 
						data: "cli="+$("#cliavi").val()+"&can="+$("#canavi").val()+"&clia="+$("#idavicli").val()+"&cic="+$("#ciclo").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevof").click();
										$("#mytablaAvi").trigger("update");
										$("#mytablaAvic").trigger("update");
										$("#mytablaBus").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
	}else{
		alert("Error: Registre Cantidad de Organismos");	
		$("#canavi").focus();
		return false;
	}
	}else{
		alert("Error: Seleccione Cliente - Unidad Receptora ");	
		
		return false;
	}					
	}else{
		alert("Error: Seleccione Aviso ");	
		
		return false;
	}
});
$("#borrarf").click( function(){	
	numero=$("#idavic").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/avisos/borrar", 
						data: "id="+$("#idavic").val()+"&tabla=avi_cli_"+"&campo=idavic"+"&cic="+$("#ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaAvi").trigger("update");
										$("#mytablaAvic").trigger("update");
										$("#mytablaBus").trigger("update");
										$("#nuevof").click();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Cliente en el apartado de asignación  para poder Eliminarlo");
		return false;
	}
});


$("#aceptar").click( function(){		
	numero=$("#idavi").val();fec=$("#fecavi").val();fol=$("#folavi").val();
	if( fec!=''){
	if( fol!=''){
	  if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/avisos/actualizaravi", 
						data: "id="+$("#idavi").val()+"&fec="+$("#fecavi").val()+"&fol="+$("#folavi").val()+"&pre="+$("#preavi").val()+"&del="+$("#delavi").val()+"&cic="+$("#ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevo").click();
										$("#mytablaAvi").trigger("update");
										$("#mytablaAviC").trigger("update");
										
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/avisos/agregaravi", 
						data: "fec="+$("#fecavi").val()+"&fol="+$("#folavi").val()+"&pre="+$("#preavi").val()+"&del="+$("#delavi").val()+"&cic="+$("#ciclo").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#nuevo").click();
										$("#mytablaAvi").trigger("update");
										$("#mytablaAviC").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		}else{
			alert("Error: Registre el Folio");	
			$("#folavi").focus();
			return false;
		}				
	}else{
		alert("Error: Selecciones Fecha");	
		$("#fecavi").focus();
		return false;
	}
});
$("#nuevo").click( function(){	
	$("#idavi").val('');$("#fecavi").val('');$("#folavi").val('');$("#preavi").val('');
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	$("#fecavi").val(f.getFullYear() + "-" + mes + "-" + dia);
   	$("#mytablaAvi").trigger("update");
   	$("#mytablaAviC").trigger("update");
   	$("#mytablaBus").trigger("update");
	return true;
});

$("#nuevof").click( function(){	
	$("#idavic").val('');$("#cliavi").val('');$("#canavi").val('');$("#cliente").val('');
	$("#tabla_avic").ajaxSorter({
				url:'<?php echo base_url()?>index.php/avisos/tablac',  		
				filters:['ciclo','idavicli'],
				sort:false,
    			onRowClick:function(){
    				$("#idavic").val($(this).data('idavic'));
        			$("#canavi").val($(this).data('canavi'));
        			$("#cliente").val($(this).data('razon'));
        			$("#cliavi").val($(this).data('numero'));
				},
		   });
	return true;
});
$("#fecavi").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#idavi").val('');$("#folavi").val('');$("#preavi").val('');
		$("#mytablaAvi").trigger("update");$("#mytablaAviC").trigger("update");
	}
});

$("#txtFI").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFF").datepicker( "option", "minDate", selectedDate );
		$('#mytablaBus').trigger('update');		
	}
});
$("#txtFF").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFI").datepicker( "option", "maxDate", selectedDate );
		$('#mytablaBus').trigger('update');		
	}
});	

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});

$(document).ready(function(){ 
	var f = new Date(); 
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
   	$("#fecavi").val(f.getFullYear() + "-" + mes + "-" + dia);
	
	$("#tabla_avi").ajaxSorter({
		url:'<?php echo base_url()?>index.php/avisos/tablaa',  		
		filters:['ciclo','fecavi'],
		
		sort:false,
    	onRowClick:function(){
    		$("#idavi").val($(this).data('idavi'));$("#idavicli").val($(this).data('folavi'));
        	$("#fecavi").val($(this).data('fecavi'));
       		$("#folavi").val($(this).data('folavi'));
       		$("#preavi").val($(this).data('preavi'));
       		$("#delavi").val($(this).data('delavi'));    
       		$("#tabla_avic").ajaxSorter({
				url:'<?php echo base_url()?>index.php/avisos/tablac',  		
				filters:['ciclo','idavicli'],
				sort:false,
    			onRowClick:function(){
    				$("#idavic").val($(this).data('idavic'));
        			$("#canavi").val($(this).data('canavi'));
        			$("#cliente").val($(this).data('razon'));
        			$("#cliavi").val($(this).data('numero'));
				},
				onSuccess:function(){ 
    				$('#tabla_avic tbody tr td:nth-child(1)').map(function(){ 
	    				if($(this).html() != '' ) {
              				$(this).css("color", "blue");$(this).css('font-weight','bold'); 
        				}
	    			});   	  	
	    		},	
		   });   		
		},
    	   
   });
   
  
   $("#tabla_bus").ajaxSorter({
		url:'<?php echo base_url()?>index.php/avisos/busquedas', 
		filters:['ciclo','txtFI','txtFF'],
		active:['Edo','Sonora'],
		multipleFilter:true, 	
	});
   
    $("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/avisos/tablaclientes', 
		multipleFilter:true, 	
		onRowClick:function(){
        	$("#cliavi").val($(this).data('numero'));
       		$("#cliente").val($(this).data('razon')); 
       		$("#canavi").focus();      		
		},		
	});
   
}); 
</script>
