<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 20px;
	/*background: none;*/		
}   
#exportc_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; } 

</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#facturas"><img src="<?php echo base_url();?>assets/images/menu/alimento.png" width="25" height="25" border="0"> Facturas de Alimento</a> </li>			
		<?php if($usuario=="Zuleima Benitez" || $usuario=="Antonio Hernandez"  || $usuario=="Joel Lizarraga A."){ ?>
		<li><a href="#fletes"><img src="<?php echo base_url();?>assets/images/menu/truck.png" width="25" height="25" border="0"> Fletes</a> </li>
		<?php } ?>
		<?php if($usuario=="Zuleima Benitez"){ ?>
		<li><a href="#mes"><img src="<?php echo base_url();?>assets/images/menu/mensual.png" width="25" height="25" border="0"> Calendario</a> </li>
		<?php } ?>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="facturas" class="tab_content" >
			<div id = "accordion" style="margin-top: -10px; margin-left: -10px; width: 958px;" >
					<h3 align="left" style="font-size: 12px" ><a href="#"> Registradas </a></h3>
        			<div  style=" text-align: right; font-size: 12px;margin-top: -5px;"> 
        				Ciclo
        				<select id="ciclo" style="font-size:13px; margin-top: 1px" >
        					<option value="22">2022</option>
        					<option value="21">2021</option>
        					<option value="20">2020</option>
                   			<option value="19">2019</option>
                   			<option value="18">2018</option>
                   		</select>   
        				Granja
        				<?php if($usuario=="Zuleima Benitez" || $usuario=="Antonio Hernandez"){ ?>
        				<select id="granja" style="font-size:13px; margin-top: 1px" >
                   			<option value="0">Sel</option>
                   			<option value="4">OA- Ahome</option>
                   			<option value="6">OA- Huatabampo</option>
                   			<option value="10">OA- Topolobampo</option>
                   			<option value="2">OA- Kino</option>	
   						</select>   
   					<?php } else{ ?>
   						<select id="granja" style="font-size:13px; margin-top: 1px" >
                   			<option value="4">OA- Ahome</option>
                   		</select>   
   					<?php }?>
   						Mes:
    	              	 		<select name="cmbMesF" id="cmbMesF" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px; width: 90px">   						
		   							<option value="0" >Todos</option>
   									<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   									<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   									<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   									<option value="10" >Octubre</option><option value="11" >Noviembre</option>
   									<option value="12" >Diciembre</option>
   								</select>	
          				<div class="ajaxSorterDiv" id="tabla_facoa" name="tabla_facoa" style="margin-top: -5px; height: 367px;margin-left: -25px; width: 942px " >  
                			<span class="ajaxTable" style="height: 357px" >
                    			<table id="mytablaFacoa" name="mytablaFacoa" class="ajaxSorter" border="1" >
                        			<thead > 
                        				<tr>
                        					<th colspan="8">Movimientos Efectuados al solictar y pagar Alimento</th>
                        					<th colspan="5">Factura</th>
                        					<th >Diferencia</th>
                        					<th >Costo</th>
                        				</tr> 
                        				<tr>                          
                        					<th data-order = "numpag" >No.</th> 
                            				<th data-order = "fecpag1" >Dia</th> 	
                            				<th data-order = "proag" >Proveedor</th>                                                        
                            				<th data-order = "des" >Descripción</th>
                            				<th data-order = "tonag" >Ton</th>
                            				<th data-order = "impmn" >M.N.</th>
                            				<th data-order = "tcpag1" >T.C.</th>
                            				<th data-order = "imppag1" >U.S.D.</th>
                            				<th data-order = "fecag1" >Fecha</th> 
                            				<th data-order = "facag" >Folio</th>  
                            				<th data-order = "estatuss" style="width: 1px;"></th>
                            				<th data-order = "impfac1" >Importe</th>
                            				<th data-order = "tcag1" >T.C.</th>
                            				<th data-order = "dif" >U.S.D.</th>
                            				<th data-order = "costo" >M.N.</th>
                            			</tr>                   
                        			</thead>
                        		<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        		</tbody>                        	
                    			</table>
                			</span> 
             				<div class="ajaxpager" style="margin-top: -5px">        
	                    		<ul class="order_list">Total: <input size="3%"  type="text" name="num" id="num" style="text-align: left;  border: 0; background: none"></ul>                                            
    	                		<form method="post" action="<?= base_url()?>index.php/empleados/pdfrep/" >
        	            			<input type="hidden"  name="depa" id="depa" readonly="true" >
        	            			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    		<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        		                        		                                                      
 								</form>   
                			</div>                                      
            			</div>                                                               
            	
       				</div>				
					<h3 align="left" style="font-size: 12px"><a href="#">Agregar o Actualizar </a></h3>
		       	 	<div  style=" text-align: left; font-size: 12px;margin-top: -10px;"> 
		       	 		Granja
        				<select id="idgag" style="font-size:13px; margin-top: 1px" >
                   			<option value="0">Sel</option>
                   			<option value="4">OA- Ahome</option><option value="6">OA- Huatabampo</option>
                   			<option value="10">OA- Topolobampo</option>
          					<option value="2">OA- Kino</option>	
   						</select> 
   						
   						<table border="2px" width="910px" style=" margin-left: -5px; margin-top: 10px">
						<tbody style="background-color: #F7F7F7">
							<tr>
								<th rowspan="2" style="background-color: #DBE5F1; font-size: 12px" >Proveedor</th>
								<th style="margin: 2px; font-size: 10px">Nombre</th>
								<th style="margin: 2px; font-size: 10px">No. de Pago</th>
							</tr>
							<tr>
								<th style="font-size: 14px; background-color: white">
									<input type="hidden" id="idag" name="idag" />
									<datalist id="prove" style="font-size:11px;width: 450px" >
                    					<option value="-"></option>
          						<?php	
          						//echo ucwords(strtolower($row->Razon));
           							$data['result']=$this->facoa_model->getProves();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->Siglas;?>"><?php echo $row->Razon;?></option>
           							<?php endforeach;?>
   									</datalist>
   									<input list="prove" id="proag" type="text" style="font-size:11px;width: 470px"> 
								</th>
								<th>
									<input name="numpag" id="numpag" type="text" style="font-size:14px;width: 70px; text-align: center">
								</th>
							</tr>
    					</tbody>
					</table>
					<table border="2px" width="910px;" style=" margin-left: -5px">
						<tbody style="background-color: #F7F7F7">
							<tr>
								<th rowspan="3" style="background-color: #DBE5F1; font-size: 12px" >Pago</th>
								<th style="margin: 2px; font-size: 10px">Fecha</th>
								<th style="margin: 2px; font-size: 10px">Importe</th>
								<th style="margin: 2px; font-size: 10px">T.C.</th>
								<th style="margin: 2px; font-size: 10px">Moneda</th>
							</tr>
							<tr>
								<th style="font-size: 14px; background-color: white">
								<input style="text-align: center; width: 90px" type="text" name="fecpag" id="fecpag" class="fecha redondo mUp" style="text-align: center;" >	
								</th>
								<th style="font-size: 14px; background-color: white;" >$ <input style="text-align: right; width: 120px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" type="text" name="imppag" id="imppag" ></th>
								<th style="font-size: 14px; background-color: white;" >$ <input style="text-align: right; width: 60px"  type="text" name="tcpag" id="tcpag" ></th>
								<th  style="font-size: 14px; background-color: white;" >
									<select id="tipag" style="font-size:13px; margin-top: 1px" >
                   						<option value="1">Pesos</option>
          								<option value="2">U.S.D.</option>	
   									</select>
   								</th> 
																
							</tr>
							<tr>
								<th style="margin: 2px; font-size: 10px">Observaciones</th>
								<th colspan="3" style="font-size: 14px; background-color: white;" >
								<input style="width: 775px" type="text" name="obspag" id="obspag"  >	
								</th>
							</tr>
    					</tbody>
					</table>
		       			<table border="2px" width="910px" style=" margin-left: -5px">
						<tbody style="background-color: #F7F7F7">
							<tr>
								<th rowspan="2" style="background-color: #DBE5F1; font-size: 12px" >Factura</th>
								<th style="margin: 2px; font-size: 10px">Fecha</th>
								<th style="margin: 2px; font-size: 10px">Folio</th>
								<th style="margin: 2px; font-size: 10px">Estatus</th>
								<th style="margin: 2px; font-size: 10px">Descripción</th>
								<th style="margin: 2px; font-size: 10px">Medida (mm)</th>
								<th style="margin: 2px; font-size: 10px">Cantidad (ton)</th>
								<th style="margin: 2px; font-size: 10px">Importe</th>
								<th style="margin: 2px; font-size: 10px">T.C.</th>
							</tr>
							<tr>
								<th style="font-size: 14px; background-color: white">
								<input style="text-align: center; width: 90px" type="text" name="fecag" id="fecag" class="fecha redondo mUp" style="text-align: center;" >	
								</th>
								<th  style="font-size: 14px; background-color: white;" >
								<input style="text-align: center; width: 90px" type="text" name="facag" id="facag" style="text-align: center;" >	
								</th>
								<th  style="background-color: white;" >
									<select name="estatuss" id="estatuss" style="font-size: 10px; height: 25px; margin-top: 1px" >								
    									<option value="0">Sel.</option>		
    									<option value="1">Pagada</option>
    									<option value="2">Abonada</option>
    									<option value="3">Pendiente</option>
    								</select>
								</th>
								<th style="font-size: 14px; background-color: white"><input type="text" name="aliag" id="aliag" style="text-align: center; width: 200px" ></th>
								<th style="font-size: 14px; background-color: white;" ><input style="text-align: center; width: 70px" " type="text" name="medag" id="medag" ></th>
								<th style="font-size: 14px; background-color: white;" ><input style="text-align: center; width: 60px"  onKeyPress="return(currencyFormat(this,',','.',event,'C'));" type="text" name="tonag" id="tonag" ></th>
								<th style="font-size: 14px; background-color: white;" >$ <input style="text-align: right; width: 120px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" type="text" name="impfac" id="impfac" ></th>
								<th style="font-size: 14px; background-color: white;" >$ <input style="text-align: right; width: 60px" onKeyPress="return(currencyFormat(this,'','.',event));" type="text" name="tcag" id="tcag" ></th>
							</tr>
    					</tbody>
					</table>
					<table border="2px" width="910px" style=" margin-left: -5px">
						<tbody style="background-color: #F7F7F7">
							<tr>
								<th rowspan="2" style="background-color: #DBE5F1; font-size: 12px" >Flete</th>
								<th style="margin: 2px; font-size: 10px">Fecha</th>
								<th style="margin: 2px; font-size: 10px">Folio</th>
								<th style="margin: 2px; font-size: 10px">Proveedor</th>
								<th style="margin: 2px; font-size: 10px">Subtotal</th>								
							</tr>
							<tr>
								<th style="font-size: 14px; background-color: white">
								<input style="text-align: center; width: 90px" type="text" name="fecfle" id="fecfle" class="fecha redondo mUp" style="text-align: center;" >	
								</th>
								<th  style="font-size: 14px; background-color: white;" >
								<input style="text-align: center; width: 90px" type="text" name="facfle" id="facfle" style="text-align: center;" >	
								</th>
								<th style="font-size: 14px; background-color: white">
									<datalist id="provef" style="font-size:11px;width: 450px" >
                    					<option value="-"></option>
          						<?php	
          						//echo ucwords(strtolower($row->Razon));
           							$data['result']=$this->facoa_model->getProves();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->Siglas;?>"><?php echo $row->Razon;?></option>
           							<?php endforeach;?>
   									</datalist>
   									<input list="provef" id="profle" type="text" style="font-size:11px;width: 470px"> 
								</th>
								<th style="font-size: 14px; background-color: white;" >$ <input style="text-align: right; width: 120px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" type="text" name="subfle" id="subfle" ></th>
								
							</tr>
    					</tbody>
					</table>
					<?php if($usuario=="Zuleima Benitez"){ ?>
					<table border="2px" width="910px;" style=" margin-left: -5px">
						<tbody style="background-color: #F7F7F7">
							<tr>
								<th rowspan="2" style="background-color: #DBE5F1; font-size: 12px; text-align: right" >
									<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  /> 
									<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
									<input style="font-size: 14px" type="submit" id="borrar" name="borrar" value="Eliminar" />
								</th>
							</tr>							
    					</tbody>
					</table>
					<?php } ?>
    				</div>
	 			</div>	
	 	</div>
	 	<?php if($usuario=="Zuleima Benitez"  || $usuario=="Antonio Hernandez"  || $usuario=="Joel Lizarraga A."){ ?>
	 	<div id="fletes" class="tab_content" >
	 		<div  style=" text-align: right; font-size: 12px;margin-top: -5px;"> 
        		Granja
        		<!--
        		<select id="granjaf" style="font-size:13px; margin-top: 1px" >
                	<option value="0">Sel</option>
                	<option value="4">OA- Ahome</option><option value="6">OA- Huatabampo</option><option value="10">OA- Topolobampo</option>
          			<option value="2">OA- Kino</option>	
   				</select>   
   			-->
   				<?php if($usuario=="Zuleima Benitez" || $usuario=="Antonio Hernandez"){ ?>
        				<select id="granjaf" style="font-size:13px; margin-top: 1px" >
                   			<option value="0">Sel</option>
                   			<option value="4">OA- Ahome</option>
                   			<option value="6">OA- Huatabampo</option>
                   			<option value="10">OA- Topolobampo</option>
                   			<option value="2">OA- Kino</option>	
   						</select>   
   					<?php } else{ ?>
   						<select id="granjaf" style="font-size:13px; margin-top: 1px" >
                   			<option value="4">OA- Ahome</option>
                   		</select>   
   					<?php }?>
          		<div class="ajaxSorterDiv" id="tabla_fle" name="tabla_fle" style="margin-top: -5px; height: 567px; width: 932px " >  
                	<span class="ajaxTable" style="height: 420px  " >
                		<table id="mytablaFlete" name="mytablaFlete" class="ajaxSorter" border="1" ">
                     		<thead > 
                     			<tr>
                        			<th colspan="3">Pago Alimento</th>
                        			<th colspan="7">Datos del Flete</th>
                        		
                        		</tr>
                        		<tr> 
                        			<th data-order = "numpag" >No.</th> 
                        			<th data-order = "fecpag2" >Dia</th>                         
                            		<th data-order = "tonag" >Ton</th>
                            		<th data-order = "fecfle1" >Dia</th> 
                            		<th data-order = "facfle" >Factura</th> 	                
                            		<th data-order = "profle" >Proveedor</th>
                            		<th data-order = "subfle" >Subtotal</th>
                            		<th data-order = "iva" >I.V.A.</th>
                            		<th data-order = "ret" >Retencion (4%) </th>
                            		<th data-order = "total" >Total</th>                            		
                            	</tr>                   
                        	</thead>
                        	<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        	</tbody>                        	
                    		</table>
                	</span> 
             	</div>                                                               
           </div>		
	 	</div>	
	 	<?php } ?>
	 	<?php if($usuario=="Zuleima Benitez"){ ?>
	 	<div id="mes" class="tab_content" style="height: 650px" >
	 		<div class="ajaxSorterDiv" id="tabla_ali" name="tabla_ali" style="width: 950px; margin-top: -20px; margin-left: -5px;">  
			 	<div style=" text-align:left; font-size: 12px;" class='filter' >
                	<div class="ajaxpager" style="margin-top: 1px; text-align: left">        
	                   	<ul style="width: 950px; text-align: left; height: 35px" class="order_list">
	                   		Adquisiciones de Alimento
	                   	<form method="post" action="<?= base_url()?>index.php/facoa/reporte">
    	               		Mes:
    	              	 		<select name="cmbMesA" id="cmbMesA" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px; width: 90px">   						
		   							<option value="0" >Todos</option>
   									<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   									<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   									<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   									<option value="10" >Octubre</option><option value="11" >Noviembre</option>
   									<option value="12" >Diciembre</option>
   								</select>	
   							Granja:
   							<select name="cmbGranja" id="cmbGranja" style="font-size: 10px; margin-top: 1px; height: 25px;  " >								
    							<option value="0">Sel</option>
                				<option value="4">OA- Ahome</option><option value="6">OA- Huatabampo</option><option value="10">OA- Topolobampo</option>
          						<option value="2">OA- Kino</option>	
    						</select>	
    						Alimento
    						<select  name="cmbAli" id="cmbAli" style="font-size: 10px; height: 25px; width: 130px; " >
    							<option value="0" >Sel.</option>
    								<?php
          			           		$cic=18;	
          							$data['result']=$this->facoa_model->alimentos($cic); ?>
									<?php foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->aliag;?>" ><?php echo $row->aliag;?></option>
           							<?php endforeach;?>
          			           		
                			</select>  
                			Medida
                			<select name='medida' id="medida" style="font-size: 10px;height: 25px;margin-top: 1px;"  ></select> 							
    						<!--<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />-->
                    	   <img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
                    	    <input type="hidden" name="tablaali" value ="" class="htmlTable"/>                        		                        		                                                      
 						</form>
 					</ul> 
 					</div>	
                </div> 
			   	<span class="ajaxTable" style="background-color: white; height: 280px;width: 945px; margin-top: -1px">
                	<table id="mytablaAli" name="mytablaAli" class="ajaxSorter" border="1">
                		<thead style="font-size: 9px">                            
                			<th style="font-size: 9px" data-order = "mes1" >Mes</th>
                			<th style="font-size: 9px" data-order = "ali" >Alimento</th>
                			<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
           					<th style="font-size: 9px" data-order = "tot" >Tot</th>
                		</thead>
                        <tbody style="font-size: 10px">                                                 	  
                        </tbody>
                    </table>
                    
                </span> 
  	 	</div>	
  	 	<div class="ajaxSorterDiv" id="tabla_alig" name="tabla_alig" style="width: 950px; margin-top: -20px; margin-left: -5px;">  
			 	<div style=" text-align:left; font-size: 12px;" class='filter' >
                	<div class="ajaxpager" style="margin-top: 1px; text-align: left">        
	                   	<ul style="width: 950px; text-align: left; height: 35px" class="order_list">
	                   		Recibido en Granja OA-Ahome
    	               	<form method="post" action="<?= base_url()?>index.php/facoa/reporteg">
    	               		Alimento
    						<select  name="cmbAlig" id="cmbAlig" style="font-size: 10px; height: 25px; width: 130px; " >
    							<option value="0" >Sel.</option>
          			           		<?php
          			           		$cic=18;
          			           		$data['result']=$this->facoa_model->alimentosg($cic); ?>
									<?php foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->CB;?>" ><?php echo $row->NomMat;?></option>
           							<?php endforeach;?>
                			</select>  
                			<!--
                			 <img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
                			-->
                    	    <input type="hidden" name="tablaalig" value ="" class="htmlTable"/>                        		                        		                                                      
 						</form>
 					</ul> 
 					</div>	
                </div> 
			   	<span class="ajaxTable" style="background-color: white; height: 280px;width: 945px; margin-top: -1px">
                	<table id="mytablaAlig" name="mytablaAlig" class="ajaxSorter" border="1">
                		<thead style="font-size: 9px">                            
                			<th style="font-size: 9px" data-order = "mes1" >Mes</th>
                			<th style="font-size: 9px" data-order = "ali" >Alimento</th>
                			<?php	
          						$cont=0;
								while($cont<31){ $cont+=1;?>
           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cont;?></th>
           					<?php } ?>
           					<th style="font-size: 9px" data-order = "tot" >Tot</th>
                		</thead>
                        <tbody style="font-size: 10px">                                                 	  
                        </tbody>
                    </table>
                    
                </span> 
  	 	</div>
 	</div> 	
 	<?php } ?>
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#fecag").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"1950:+0",
});
$("#fecpag").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"1950:+0",
});
$("#fecfle").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"1950:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});

$("#nuevo").click( function(){	
	$("#idag").val('');$("#fecag").val('');$("#facag").val('');$("#idgag").val('0');$("#aliag").val('');$("#medag").val('');
	$("#tonag").val('');$("#tcag").val('');$("#fecpag").val('');$("#obspag").val('');$("#proag").val('');
	$("#imppag").val('');$("#impfac").val('');$("#tcpag").val('');$("#tipag").val('1');
	$("#fecfle").val('');$("#facfle").val('');$("#profle").val('');$("#subfle").val('');$("#numpag").val('');$("#estatuss").val('');
 return true;
})

$("#borrar").click( function(){	
	id=$("#idag").val();
	if(id!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/facoa/borrar", 
				data: "id="+$("#idag").val()+"&cic="+$("#ciclo").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								alert("Datos Eliminados correctamente");
								$("#mytablaFacoa").trigger("update");$("#mytablaFlete").trigger("update");$("#mytablaAli").trigger("update");
								$("#accordion").accordion( "activate",0 );
								$("#nuevo").click();					
							}else{
								alert("Error con la base de datos o usted no ha eliminado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder eliminar datos");				
		return false;
	}	 
});

$("#aceptar").click( function(){	
	id=$("#idag").val();gra=$("#idgag").val();pro=$("#proag").val();imp=$("#imppag").val();
	fecpag=$("#fecpag").val();
	if(gra!=0){
	 if(pro!=''){
	  if(fecpag!=''){
	  	if(imp!=''){
	  	if(id!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/facoa/actualizar", 
						data: "id="+$("#idag").val()+"&feca="+$("#fecag").val()+"&gra="+$("#idgag").val()+"&fol="+$("#facag").val()+"&des="+$("#aliag").val()+"&med="+$("#medag").val()+"&ton="+$("#tonag").val()+"&tc="+$("#tcag").val()+"&fecp="+$("#fecpag").val()+"&obs="+$("#obspag").val()+"&pro="+$("#proag").val()+"&cic="+$("#ciclo").val()+"&ip="+$("#imppag").val()+"&if="+$("#impfac").val()+"&tcp="+$("#tcpag").val()+"&tip="+$("#tipag").val()+"&fecf="+$("#fecfle").val()+"&facf="+$("#facfle").val()+"&prof="+$("#profle").val()+"&subf="+$("#subfle").val()+"&npag="+$("#numpag").val()+"&ests="+$("#estatuss").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaFacoa").trigger("update");$("#mytablaFlete").trigger("update");$("#mytablaAli").trigger("update");
										$("#accordion").accordion( "activate",0 );
										$("#nuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/facoa/agregar", 
						data: "feca="+$("#fecag").val()+"&gra="+$("#idgag").val()+"&fol="+$("#facag").val()+"&des="+$("#aliag").val()+"&med="+$("#medag").val()+"&ton="+$("#tonag").val()+"&tc="+$("#tcag").val()+"&fecp="+$("#fecpag").val()+"&obs="+$("#obspag").val()+"&pro="+$("#proag").val()+"&cic="+$("#ciclo").val()+"&ip="+$("#imppag").val()+"&if="+$("#impfac").val()+"&tcp="+$("#tcpag").val()+"&tip="+$("#tipag").val()+"&fecf="+$("#fecfle").val()+"&facf="+$("#facfle").val()+"&prof="+$("#profle").val()+"&subf="+$("#subfle").val()+"&npag="+$("#numpag").val()+"&ests="+$("#estatuss").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos Registrados correctamente");$("#mytablaFlete").trigger("update");$("#mytablaAli").trigger("update");
										$("#mytablaFacoa").trigger("update");
										$("#accordion").accordion( "activate",0 );
										$("#nuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
			}
		}else{
			alert("Error: Registre el Importe de Pago");	
			$("#imppag").focus();
				return false;
		}				
		}else{
			alert("Error: Seleccione la Fecha de Pago");	
			$("#fecpag").focus();
				return false;
		}
	}else{
		alert("Error: Seleccione Proveedor");
		$("#proag").focus();
			return false;
	}
 }else{
		alert("Error: Seleccione Granja");
		$("#idgag").focus();
			return false;
	}
});

$(document).ready(function(){
	$("#tabla_facoa").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facoa/tabla',  
		filters:['ciclo','granja','cmbMesF'],
        active:['dif1','0','<='],  
        //multipleFilter:true,
        sort:false,
        onRowClick:function(){
            $("#accordion").accordion( "activate",1 );
           	$("#idag").val($(this).data('idag'));
           	$("#idgag").val($(this).data('idgag'));
           	$("#proag").val($(this).data('proag'));
			$("#fecpag").val($(this).data('fecpag'));
			$("#obspag").val($(this).data('obspag'));
			$("#fecag").val($(this).data('fecag'));
			$("#facag").val($(this).data('facag'));	
			$("#aliag").val($(this).data('aliag'));
			$("#medag").val($(this).data('medag'));
			$("#tonag").val($(this).data('tonag'));
			$("#tcag").val($(this).data('tcag'));
			$("#imppag").val($(this).data('imppag'));
			$("#impfac").val($(this).data('impfac'));
			$("#tcpag").val($(this).data('tcpag'));
			$("#tipag").val($(this).data('tipag'));
			$("#fecfle").val($(this).data('fecfle'));
			$("#facfle").val($(this).data('facfle'));
			$("#profle").val($(this).data('profle'));
			$("#subfle").val($(this).data('subfle'));
			$("#numpag").val($(this).data('numpag'));
			$("#estatuss").val($(this).data('estatuss'));
        },
       onSuccess:function(){
       		$('#tabla_facoa tbody tr').map(function(){
		    	if($(this).data('numpag')=='Total') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');
	    		}
		    });
       		$('#tabla_facoa tbody tr td:nth-child(8)').map(function(){ $(this).css('font-weight','bold'); $(this).css('background','lightblue');});
       		$('#tabla_facoa tbody tr td:nth-child(12)').map(function(){ $(this).css('font-weight','bold'); $(this).css('background','lightblue');});
       		$('#tabla_facoa tbody tr td:nth-child(13)').map(function(){ $(this).css('font-weight','bold'); $(this).css('background','lightblue');});
       		$('#tabla_facoa tbody tr td:nth-child(11)').map(function(){ 
					if($(this).html() == 3 && $(this).html() != '' ) {
              				$(this).css("background-color", "red");$(this).css("color", "red");
        			}
        			if($(this).html() == 2 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "orange");
        			}
        			if($(this).html() == 1 && $(this).html() != '' ) {
             				$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
        			} 
	    	});
    	}
    });
	
	$("#tabla_fle").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facoa/tablafle',  
		filters:['ciclo','granjaf'],
       // active:['dif1','0','<='],  
        //multipleFilter:true,
        sort:false,
        onRowClick:function(){
            
        },
       onSuccess:function(){
       		$('#tabla_fle tbody tr').map(function(){
		    	if($(this).data('numpag')=='Total') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');
	    		}
		    });
    	}
    });
    $("#tabla_ali").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facoa/tablaali',  
		filters:['ciclo','cmbMesA','cmbGranja','cmbAli','medida'],
       // active:['dif1','0','<='],  
        //multipleFilter:true,
        sort:false,
        onRowClick:function(){
            
        },
       onSuccess:function(){
       		$('#tabla_ali tbody tr').map(function(){
		    	if($(this).data('mes1')=='Tot') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');
	    		}
		    });
    	}
    });
    $("#tabla_alig").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facoa/tablaalig',  
		filters:['ciclo','cmbMesA','cmbAlig'],
       // active:['dif1','0','<='],  
        //multipleFilter:true,
        sort:false,
        onRowClick:function(){
            
        },
       onSuccess:function(){
       		$('#tabla_alig tbody tr').map(function(){
		    	if($(this).data('mes1')=='Tot') {	    				    			
	    			$(this).css('background','lightblue');$(this).css('font-weight','bold');
	    		}
		    });
    	}
    });
    $('#medida').boxLoader({
            url:'<?php echo base_url()?>index.php/facoa/combob',
            equal:{id:'aliag',value:'#cmbAli'},
            select:{id:'medag',val:'val'},
            all:"Sel.",   
   });
});
</script>