<?php $this->load->view('header_cv');?>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/base.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui/css/redmond/jquery-ui-1.8.4.custom.css" />
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding: 3px 3px; 
	color:black;
	height:15px;	
}
table.ajaxSorter thead tr th{padding: 3px 3px;font-size:8px;}
th{border-right: 1px solid black;}
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs"> 
		<strong>	
		<li><a href="#mes" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/mensual.png" width="20" height="20" border="0"> Mes </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 500px;" >
	<div class="ajaxpager" style="margin-top: 0px" >        
	                	<form  id="data_tables1" method="post" action="<?= base_url()?>index.php/programa/pdfrepdetmes" >
	                   	<ul class="order_list" style="width: 500px" >
	                   		Ciclo:  
	                   	<select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px;margin-top: 1px; margin-left:4px;" >
                    	<?php $ciclof=2014; $actual=date("Y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
							<?php  $actual-=1; } ?>
          				</select> 
          				Mes:
	                   	<select name="cmbMesT" id="cmbMesT" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
   							<option value="12" >Diciembre</option>
   							<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   							<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   							<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   							<option value="10" >Octubre</option><option value="11" >Noviembre</option>
   						</select>
   						Origen:<select name="OrigenMes" id="OrigenMes" style="font-size: 11px;height: 25px;  margin-top: 1px;">
   									<option value="Aquapacific" >Aquapacific</option>
   									<option value="Ecuatoriana" >Ecuatoriana</option>
   							   </select>
	                   		
	                   	</ul>  
	                   		<img style="cursor: pointer" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />
	                   		<input type="hidden"  name="tabla" class="htmlTable"/>
	                   		<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png"  />
                        </form >
                       	<script type="text/javascript">
							$(document).ready(function(){
									$('#exporterpdfi').click(function(){									
										$('#html_progmes').val($('#mytablaTM').html());
										$('#data_tablesi').submit();
											
										$('#html_progmes').val('');									
									});
							});
						</script> 		
 					</div> 
		<div id="mes" class="tab_content" style="" >
				<div class="ajaxSorterDiv" id="tabla_detmes" name="tabla_detmes" style="height: 470px;  margin-left: -10px ">                
               		 
               		<span class="ajaxTable" style="height: 410px; background-color: white; margin-top: -15px" >
            	       	<table id="mytablaTM" class="ajaxSorter" border="1 style="border-left: solid 1px;"">
            	       		<thead >
            	       			<tr>
                        		<th  data-order = "Razon">Dia</th>
                        		<th style="" data-order = "d1">1</th><th style="" data-order = "d2">2</th><th style="" data-order = "d3">3</th>
                        		<th style="" data-order = "d4">4</th><th style="" data-order = "d5">5</th><th style="" data-order = "d6">6</th>
                        		<th style="" data-order = "d7">7</th><th style="" data-order = "d8">8</th><th style="" data-order = "d9">9</th><th style="" data-order = "d10">10</th>
                        		<th style="" data-order = "d11">11</th><th style="" data-order = "d12">12</th><th style="" data-order = "d13">13</th>
                        		<th style="" data-order = "d14">14</th><th style="" data-order = "d15">15</th><th style="" data-order = "d16">16</th>
                        		<th style="" data-order = "d17">17</th><th style="" data-order = "d18">18</th><th style="" data-order = "d19">19</th><th style="" data-order = "d20">20</th>
                        		<th style="" data-order = "d21">21</th><th style="" data-order = "d22">22</th><th style="" data-order = "d23">23</th>
                        		<th style="" data-order = "d24">24</th><th style="" data-order = "d25">25</th><th style="" data-order = "d26">26</th>
                        		<th style="" data-order = "d27">27</th><th style="" data-order = "d28">28</th><th style="" data-order = "d29">29</th><th style="" data-order = "d30">30</th>
                        		<th style="" data-order = "d31">31</th>
                        		<th data-order = "tdc" style="">Mes</th>
                        		</tr>
                        	</thead>
                        	<tbody style="font-size: 8px;">                        		
                        	</tbody>
                        	                        	
                    	</table>
                	</span>
                	
             	</div>  
           
 		</div>
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$(document).ready(function(){ 
	var f = new Date();
	$("#cmbMesT").val((f.getMonth() +1));	
	
	$("#tabla_detmes").ajaxSorter({
		url:'<?php echo base_url()?>index.php/programa/tablaprogmes',
		filters:['cmbCiclo','cmbMesT','OrigenMes'],
		fixedHeader:true,
		sort:false,	
		onSuccess:function(){   						
			//ilumina el dia actual del año en curso
			if($("#cmbMesT").val()==(f.getMonth() +1)){
				$('#tabla_detmes tbody tr td:nth-child('+((f.getDate())+1)+')').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			}
			
			//ilumina los clientes de la fecha actual
	    	$('#tabla_detmes tr').map(function(){	    		
	    		if($(this).data('d'+((f.getDate())))!='' && $(this).data('razon')!='' && $("#cmbMesT").val()==(f.getMonth() +1)){
	    			$(this).css('background','lightblue');
	    		}
	    		
	    	});
			$('#tabla_detmes tr').map(function(){	    		
	    		/*if($(this).data('razon')=='Programado'){
	    			$(this).css('font-weight','bold');
	    			$(this).css('background','Orange'); #fad42e
	    		}*/
	    		if($(this).data('razon')=='Programado'|| $(this).data('razon')=='Remisionado' || $(this).data('razon')=='Deficit o Superavit'){
	    			$(this).css('font-weight','bold');
	    			$(this).css('background','#fad42e');$(this).css('opacity','.75');
	    		}
	    		if($(this).data('razon')=='Fecha de siembra'){
	    			$(this).css('background','lightblue');
	    			$(this).css('font-size','6px'); 
	    		}
	    		if($(this).data('razon')=='Sala' || $(this).data('razon')=='Corrida' || $(this).data('razon')=='Producción Estimada' || $(this).data('razon')=='Producción menos entrega'){
	    			$(this).css('background','lightgray');
	    			//if($(this).data('razon')=='Fecha de siembra'){ $(this).css('font-size','6px'); }
	    		}
	    		if($(this).data('razon')=='Cliente' || $(this).data('razon')=='Corte'){
	    			$(this).css('background','lightblue');
	    		}
	    	});	    		
			$('#tabla_detmes tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');$(this).css('font-size','8px');})
			$('#tabla_detmes tbody tr td:nth-child(33)').map(function(){$(this).css('text-align','right'); $(this).css('font-weight','bold');})
	    },
	});
	
         
	 
});  	
</script>
