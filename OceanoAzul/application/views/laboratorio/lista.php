<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}
</style>


<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<?php if($usuario=="Perla Peraza" || $usuario=="Jesus Benítez" ){ ?>	
		<li><a href="#valores" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/vanammei.png" width="25" height="25" border="0"> Tabla de Valores </a></li>
		<?php } ?>		
		<li><a href="#analisis" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/vanammei2.png" width="25" height="25" border="0"> Análisis en Fresco </a></li>
		<!--
		<li><a href="#mes" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/tecnicos.png" width="20" height="20" border="0"> Mes </a></li>
		-->
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px;" >
		<?php if($usuario=="Perla Peraza"  || $usuario=="Jesus Benítez" ){ ?>
		<div id="valores" class="tab_content" style="margin-top: -5px" >
			<table border="2">
				<tr>
					<th rowspan="4">
						<div class="ajaxSorterDiv" id="tabla_fis" name="tabla_fis" style="width: 160px"  >                 			
							<span class="ajaxTable" style="height: 370px; " >
                    			<table id="mytablaFis" name="mytablaFis" class="ajaxSorter" border="0" style="width: 143px" >
                        			<thead>                            
                            			<th data-order = "tipo" >Tipo</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
					</th>
					<th style="text-align: center">
						Tipo: <input size="18%"  type="text" name="tipo" id="tipo">	<input type="hidden" size="1%" name="idfis" id="idfis">
						<input type="submit" id="fnuevo" name="fnuevo" value="Nuevo"  />
						<input type="submit" id="faceptar" name="faceptar" value="Guardar" />
						<input <?php if($usuario!="Jesus Benítez"){ ?> disabled="true" <?php } ?>  type="submit" id="fborrar" name="fborrar" value="Borrar" />
					</th>
				</tr>
				<tr>
					
					<th>Valores</th>
				</tr>
				<tr>
					<th>
						<div class="ajaxSorterDiv" id="tabla_val" name="tabla_val"  style="margin-top: -5px"  >                 			
							<span class="ajaxTable" style="height: 335px; width: 335px"  >
                    			<table id="mytablaVal" name="mytablaVal" class="ajaxSorter" border="1"  >
                        			<thead>                            
                            			<th data-order = "val" >Valor</th>
                            			<th data-order = "nom" >Nomenclatura</th>
                            			<th data-order = "des" >Descripción</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			</div>
					</th>
				</tr>
				<tr>
					<th>
						<input style="font-size: 14px" type="submit" id="agrega" name="agrega" value="+" title="AGREGAR y ACTUALIZAR DATOS DE VALORES" />
						<input style="font-size: 14px" type="submit" id="quita" name="quita" value="-" title="QUITAR DATOS DE VALORES" />
						Val:<input style="text-align: center" size="3%"  type="text" name="val" id="val">
						Nom:<input style="text-align: center" size="4%"  type="text" name="nom" id="nom">
						Des:<input size="15%"  type="text" name="des" id="des">	
						<input type="hidden" size="1%" name="idval" id="idval">
						<input style="font-size: 14px" type="submit" id="vnuevo" name="vnuevo" value="N" title="PONER EN BLANCO PARA AGREGAR UN VALOR NUEVO" />
					</th>
				</tr>
			</table>
		</div>	
		<?php } ?>	
		<div id="analisis" class="tab_content" style="margin-top: -5px" >
			<!--<div style="width: 920px">-->
			<?php if($usuario=="Perla Peraza"  || $usuario=="Jesus Benítez" || $usuario=="Técnico"){ ?>	
			<table class="ajaxSorter" border="2" style="width: 940px" >
				<thead>
						<th>Fecha</th><th>PL</th>
						<th>Pila 
							<?php if($usuario=="Perla Peraza"  || $usuario=="Jesus Benítez"){ ?>
								<input style="font-size: 14px" type="submit" id="cancelarrp" name="cancelarrp" value="No" onclick="canresremi(2,0)"/>
								<input style="font-size: 14px; background-color: red; color: white" type="submit" id="restablecerrp" name="restablecerrp" value="Si" onclick="canresremi(1,0)"/></th>
							<?php } ?>
						<th>Org.</th><th style="width: 110px">Branquias</th><th>Intestino</th><th style="width: 110px">Exoesqueleto</th>
						<th >Hep1</th>
						<th >Hep2</th>
						<th >Hep3</th>
						<th >Observaciones
							<?php if($usuario=="Técnico"  || $usuario=="Jesus Benítez"){ ?>
							<input type="submit" id="pnuevo" name="pnuevo" value="Nuevo"  />
							<input type="submit" id="paceptar" name="paceptar" value="Guardar" />
							<input type="submit" id="pborrar" name="pborrar" value="Borrar" />
							<?php } ?>
							<input type="hidden" size="3%"  name="idaf" id="idaf" >
						</th>
						
				</thead>
				<tbody>
						<th style="text-align: center"><input type="text" name="fec" id="fec" class="fecha redondo mUp" readonly="true" style="text-align: center; width: 55px" ></th>
						<th style="text-align: center"><input type="text" name="pl" id="pl" value="12" style="text-align: center; width: 14px"></th>
						<th style="text-align: center">
							<select name="pila" id="pila" style="font-size: 9px;height: 25px;  margin-top: 1px; margin-left:4px; width: 60px">
          						<option value="0">Todas</option>
          						<?php	
           							$this->load->model('laboratorio_model');
									$data['result']=$this->laboratorio_model->verPila();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumPila;?>" ><?php echo $row->Nombre;?></option>
           							<?php endforeach;?>
   							</select>
   							
						</th>
						
						<th style="text-align: center"><input type="text" name="org" id="org" style="text-align: center;width: 30px"></th>
						<th style="text-align: center">
							<select name="bra" id="bra" style="font-size: 9px;height: 25px;  margin-top: 1px; width: 50px" >
          						<?php	
           							$this->load->model('laboratorio_model');
									$data['result']=$this->laboratorio_model->verBraval();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->val;?>" ><?php echo $row->nom;?></option>
           							<?php endforeach;?>
   							</select>
   							<input type="text" name="obsbra" id="obsbra" style="text-align: center;width: 45px">
						</th>
						<th style="text-align: center">
							<select name="inte" id="inte" style="font-size: 9px;height: 25px;  margin-top: 1px; margin-left:4px; width: 50px" >
          						<?php	
           							$this->load->model('laboratorio_model');
									$data['result']=$this->laboratorio_model->verIntval();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->val;?>" ><?php echo $row->nom;?></option>
           							<?php endforeach;?>
   							</select>
						</th>
						<th style="text-align: justify">
							<select name="exo" id="exo" style="font-size: 9px;height: 25px;  margin-top: 1px; margin-left:4px; width: 50px" >
          						<?php	
           							$this->load->model('laboratorio_model');
									$data['result']=$this->laboratorio_model->verExoval();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->val;?>" ><?php echo $row->nom;?></option>
           							<?php endforeach;?>
   							</select>
   							<input type="text" name="obsexo" id="obsexo" style="text-align: center;width: 45px">
						</th>
						<th style="text-align: center">
							<select name="hep1" id="hep1" style="font-size: 9px;height: 25px;  margin-top: 1px; margin-left:4px;" >
          						<?php	
           							$this->load->model('laboratorio_model');
									$data['result']=$this->laboratorio_model->verHepval();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->val;?>" ><?php echo $row->nom;?></option>
           							<?php endforeach;?>
   							</select>
   						</th>
   						<th style="text-align: center">	
   							<select name="hep2" id="hep2" style="font-size: 9px;height: 25px;  margin-top: 1px; margin-left:4px; " >
          						<?php	
           							$this->load->model('laboratorio_model');
									$data['result']=$this->laboratorio_model->verHepval();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->val;?>" ><?php echo $row->nom;?></option>
           							<?php endforeach;?>
   							</select>
   						</th>
   						<th style="text-align: center">
   							<select name="hep3" id="hep3" style="font-size: 9px;height: 25px;  margin-top: 1px; margin-left:4px; " >
          						<?php	
           							$this->load->model('laboratorio_model');
									$data['result']=$this->laboratorio_model->verHepval();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->val;?>" ><?php echo $row->nom;?></option>
           							<?php endforeach;?>
   							</select>
						</th>
						<th ><input style="width: 220px"  type="text" name="obs" id="obs"></th>
						
				</tbody>
			</table>
			<?php } ?>
			<!--</div>-->
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_fres" name="tabla_fres" style="width: 860px"  >                 			
							<span class="ajaxTable" <?php if($usuario!="Perla Peraza"  && $usuario!="Jesus Benítez" && $usuario!="Técnico"){ ?> style="height: 425px; margin-top: 1px; " <?php }else{ ?> style="height: 370px; margin-top: 1px; " <?php } ?> >
								
                    			<table id="mytablaFres" name="mytablaFres" class="ajaxSorter" border="1" style="width: 843px" >
                        			<thead style="text-align: center">
                        				
                        				<th data-order = "Nombre" rowspan="2"  >Pila</th>
                        				<th data-order = "plf1" rowspan="2" >Pl</th>
                        				<th data-order = "org" rowspan="2" >Org.</th>
                        				<th data-order = "bra1" rowspan="2" >Branquias</th>
                        				<th data-order = "inte1" rowspan="2" >Instestino</th>
                        				<th data-order = "exo1" rowspan="2"  >Exoesqueleto</th>                        				
                        				<th colspan="3" style="text-align: center" >Hepatopancreas</th>
                        				<th colspan="2" ></th>
                        				<tr>	
                            			<th data-order = "hep11" style="width: 40px" >M1</th>
                            			<th data-order = "hep21" style="width: 40px">M2</th>
                            			<th data-order = "hep31" style="width: 40px">M3</th>
                            			<th data-order = "kpi1">KPI</th>
                            			<th data-order = "obs" >Observaciones Generales</th>
                            			</tr>
                            		</thead>	
                            		<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px; text-align: left"> 
								<form id="data_tablef" action="<?= base_url()?>index.php/laboratorio/pdfrepdia" method="POST" >							
    	    	            		<ul class="order_list" style="width: 750px; margin-top: 1px;">
    	    	            			Día de Análisis:<!-- <input size="13%" type="text" name="pfecd" id="pfecd" class="fecha redondo mUp" readonly="true" style="text-align: center;" >-->
    	    	            			<input size="10%" type="text" name="feca" id="feca" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
    	    	            			<?php if($usuario!="Perla Peraza"  && $usuario!="Jesus Benítez" && $usuario!="Técnico"){ ?> 
    	    	            			Pila: <select  name="pila" id="pila" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          									<option value="0">Todas</option>
          										<?php	
           										$this->load->model('laboratorio_model');
												$data['result']=$this->laboratorio_model->verPila();
												foreach ($data['result'] as $row):?>
           											<option value="<?php echo $row->NumPila;?>" ><?php echo $row->Nombre;?></option>
           										<?php endforeach;?>
   											</select>
   										<?php } ?>
    	    	            			Estatus:
    	    	            			<select id="critica" name="critica" style="margin-top: 1px">
    	    	            				<option value="0">Todas</option>
    	    	            				<option value="1" style="background-color: red; color: white">Críticas</option>
    	    	            				<option value="2" style="background-color: blue; color: white">Buenas</option>
    	    	            			</select>
    	    	            			<?php if($usuario=="Jesus Benítez" || $usuario=="Técnico" ){ ?>
									<input name="nadr" id="nadr" value="1" type="hidden"><input name="estr" id="estr" type="hidden">
									<input style="font-size: 12px" type="button" id="his" name="his" value="Registrar Resultado" />
								<?php }?>
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" id="export_data" src="<?= base_url()?>assets/img/sorter/pdf.png" />
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de Pilas" id="exporta" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tabladps" id="html_dps"/>
        	            	    	<input type="hidden" size="10%"  type="text" name="tip" id="tip" >
								</form>  
								 
                			</div>       
	                	<script type="text/javascript">
							$(document).ready(function(){
								$('#export_data').click(function(){									
									$('#html_dps').val($('#mytablaFres').html());
									$('#tip').val('1');
									$('#data_tablef').submit();
									$('#html_dps').val('');
								});
							
								$('#exporta').click(function(){									
									$('#html_dps').val($('#mytablaFres').html());
									$('#tip').val('2');
									$('#data_tablef').submit();
									$('#html_dps').val('');
								});
							});
						</script>
	                	</div>
	                	
            		</th>
            	</tr>
			</table>
		</div>	
		
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#his").click( function(){	
 if(confirm('Esta Seguro de Realizar el proceso de historial para el día de Hoy?')){
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/laboratorio/historial", 
			data: "nadr="+$("#nadr").val()+"&estr="+$("#estr").val(),
			success: 	
					function(msg){															
						if(msg!=0){
							if(msg==1){alert("ya habias actualizado... hasta mañana podras de nuevo hacerlo");}		
							if(msg==2){alert("Historial Actualizado");
							}		
						}else{
							alert("Error con la base de datos o usted no ha actualizado nada");
						}					
					}			
	});	
 }		 
});
function canresremi(val,solodep){	
	$.ajax({
		type: "POST",//Envio
		url:'<?php echo base_url()?>index.php/laboratorio/criticas',
		data: "pila="+$("#pila").val()+"&feca="+$("#feca").val()+"&valor="+val+"&solodep="+solodep,
		success:function(msg){												
			//$("#cmbCancelacion").val(-1);$("#mytablaRem").trigger("update");$("#mytablaDep").trigger("update");
			$("#mytablaFres").trigger("update");												
		}
	});	
/*	$(this).removeClass('used');
   	if(solodep==0){
   		$('#ver_mas_remit').hide();
   		$('#ver_mas_remi').show();
   	}else{
   		$('#ver_mas_depot').hide();
   		$('#ver_mas_depo').show();
   }	
   $("#tabla").val('');$("#ciclo").val('');*/	   
}



$("#fnuevo").click( function(){
	$("#idfis").val('');$("#tipo").val('');$("#tipo").focus();
	$("#mytablaVal").trigger("update");
	$("#idval").val('');$("#val").val('');$("#des").val('');$("#nom").val('');
 return true;
})
$("#vnuevo").click( function(){
	$("#idval").val('');$("#val").val('');$("#des").val('');$("#val").focus();$("#nom").val('');
 return true;
})
$("#pnuevo").click( function(){
	$("#idaf").val('');$("#org").val('');$("#obsbra").val('');$("#obsexo").val('');$("#obs").val('');
	$("#bra").val('');$("#inte").val('');$("#exo").val('');$("#hep").val('');
 return true;
})
//valores
$("#quita").click( function(){	
	numero=$("#idval").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/laboratorio/quitarDet", 
						data: "id="+$("#idval").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaVal").trigger("update");
										$("#idval").val('');$("#val").val('');$("#des").val('');
										//setTimeout("window.location='laboratorio'",1); 
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Valor para poder Quitarlo");
		return false;
	}
});
/*function refrescar() 
{ 
var x=document.getElementById("bra"); 
t=x.options[x.selectedIndex].text; 
setTimeout("window.location='laboratorio'",t*1000); 
} */

$("#agrega").click( function(){	
	num=$("#idfis").val();id=$("#idval").val();
	val=$("#val").val();des=$("#des").val();nom=$("#nom").val();
	if(num!=''){
		if(val!=''){
		  if(nom!=''){
			if(des!=''){
				if(id!=''){
					$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/laboratorio/actualizaDet", 
						data: "id="+$("#idval").val()+"&val="+$("#val").val()+"&des="+$("#des").val()+"&nom="+$("#nom").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaVal").trigger("update");
										$("#idval").val('');$("#val").val('');$("#des").val('');$("#nom").val('');
										//setTimeout("window.location='laboratorio'",1); 
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
					});
				}else{
					$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/laboratorio/agregaDet", 
						data: "&val="+$("#val").val()+"&des="+$("#des").val()+"&fis="+$("#idfis").val()+"&nom="+$("#nom").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaVal").trigger("update");
										$("#idval").val('');$("#val").val('');$("#des").val('');$("#nom").val('');$("#val").focus();
										//setTimeout("window.location='laboratorio'",1);
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
					});
				}
			}else{
				alert("Error: Necesita Registrar Descripción");
				$("#des").focus();
				return false;
			}
		  }else{
			alert("Error: Necesita Registrar Nomenclatura");
			$("#nom").focus();
			return false;
		  }		
		}else{
			alert("Error: Necesita Registrar Valor");
			$("#val").focus();
			return false;
		}		
	}else{
		alert("Error: Necesita seleccionar Tipo de Análisis para Registro de Valores y Descripción");
		return false;
	}
});


function seleccion(sele){
	$("#equisel").val(sele);
	$("#mytablaESel").trigger("update");
	$("#nombre").val('');
   	$("#fecserv").val('');
   	$("#tiposel").val('');
   	$("#idserv").val('');	
}



//tipos
$("#fborrar").click( function(){	
	numero=$("#idfis").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/laboratorio/borrare", 
						data: "id="+$("#idfis").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Tipo de Análisis Eliminado");
										$("#fnuevo").click();
										$("#mytablaFis").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Tipo para poder Eliminarlo");
		return false;
	}
});

$("#faceptar").click( function(){		
	tip=$("#tipo").val();
	numero=$("#idfis").val();
	if( tip!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/laboratorio/actualizare", 
						data: "id="+$("#idfis").val()+"&tip="+$("#tipo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#fnuevo").click();
										$("#mytablaFis").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/laboratorio/agregare", 
						data: "tip="+$("#tipo").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Tipo registrado correctamente");
										$("#fnuevo").click();
										$("#mytablaFis").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
	}else{
		alert("Error: Registre Tipo de Análisis");	
		$("#tipo").focus();
		return false;
	}
});

//ANALISIS DIARIO
$("#pborrar").click( function(){	
	numero=$("#idaf").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/laboratorio/borrara", 
						data: "id="+$("#idaf").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Análisis Eliminado");
										$("#pnuevo").click();
										$("#mytablaFres").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Análisi para poder Eliminarlo");
		return false;
	}
});
$("#paceptar").click( function(){		
	fec=$("#fec").val();org=$("#org").val();
	numero=$("#idaf").val();
	if( fec!=''){
	  if( org!=''){	
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/laboratorio/actualizara", 
						data: "id="+$("#idaf").val()+"&fec="+$("#fec").val()+"&pl="+$("#pl").val()+"&pil="+$("#pila").val()+"&org="+$("#org").val()+"&bra="+$("#bra").val()+"&obsb="+$("#obsbra").val()+"&inte="+$("#inte").val()+"&exo="+$("#exo").val()+"&obse="+$("#obsexo").val()+"&hep1="+$("#hep1").val()+"&hep2="+$("#hep2").val()+"&hep3="+$("#hep3").val()+"&obs="+$("#obs").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#pnuevo").click();
										$("#mytablaFres").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/laboratorio/agregara", 
						data: "fec="+$("#fec").val()+"&pl="+$("#pl").val()+"&pil="+$("#pila").val()+"&org="+$("#org").val()+"&bra="+$("#bra").val()+"&obsb="+$("#obsbra").val()+"&inte="+$("#inte").val()+"&exo="+$("#exo").val()+"&obse="+$("#obsexo").val()+"&hep1="+$("#hep1").val()+"&hep2="+$("#hep2").val()+"&hep3="+$("#hep3").val()+"&obs="+$("#obs").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Tipo registrado correctamente");
										$("#pnuevo").click();
										$("#mytablaFres").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
	  }else{
		alert("Error: Registre Organismo a Analizar");	
		$("#org").focus();
		return false;
	  } 	
	}else{
		alert("Error: Registre Fecha de Análisis");	
		$("#fec").focus();
		return false;
	}
});



$("#fec").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#feca").val( selectedDate );
		$("#mytablaFres").trigger("update");		
	}
});
$("#feca").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
/*$("#fecserv").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});*/
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  



$(document).ready(function(){ 
	var f = new Date();
	$("#feca").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());	
	$("#tabla_fis").ajaxSorter({
		url:'<?php echo base_url()?>index.php/laboratorio/tablatipos',  
		multipleFilter:true, 
   		//onLoad:false,
   		sort:false,	
   		onRowClick:function(){
   			$("#tipo").val($(this).data('tipo'));
   			$("#idfis").val($(this).data('idfis'));
   			$("#idval").val('');$("#val").val('');$("#des").val('');$("#nom").val('');
   			$("#tabla_val").ajaxSorter({
				url:'<?php echo base_url()?>index.php/laboratorio/tablaval',  		
				filters:['idfis'],
				sort:false,
				onRowClick:function(){
					$("#idval").val($(this).data('idval'));$("#val").val($(this).data('val'));$("#des").val($(this).data('des'));$("#nom").val($(this).data('nom'));
				},		
			});
   		},	
   		
   	});
	
	$("#tabla_fres").ajaxSorter({
		url:'<?php echo base_url()?>index.php/laboratorio/tablafres',  		
		filters:['feca','pila','critica'],
		sort:false,
		active:['critica',1],
		onRowClick:function(){
			$("#idaf").val($(this).data('idaf'));$("#pila").val($(this).data('pila'));$("#fec").val($(this).data('fec'));
			$("#org").val($(this).data('org'));$("#pl").val($(this).data('plf'));
			$("#bra").val($(this).data('bra'));$("#obsbra").val($(this).data('obsbra'));$("#inte").val($(this).data('inte'));
			$("#exo").val($(this).data('exo'));$("#obsexo").val($(this).data('obsexo'));$("#hep1").val($(this).data('hep1'));
			$("#hep2").val($(this).data('hep2'));$("#hep3").val($(this).data('hep3'));$("#obs").val($(this).data('obs'));
		},
		onSuccess:function(){   						
			$('#tabla_fres tbody tr').map(function(){ $(this).css('text-align','center');})
			$('#tabla_fres tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');})
			//$('#tabla_fres tbody tr td:nth-child(10)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','center');$(this).css('background','lightyellow');})
			$('#tabla_fres tbody tr td:nth-child(11)').map(function(){ $(this).css('text-align','justify');})
			$('#tabla_fres tr').map(function(){	    		
	    		if($(this).data('nombre')=='KPI'){
	    			$(this).css('background','lightgray');$(this).css('font-weight','bold');	
	    			$("#estr").val($(this).data('totkpip'));    			
	    		}
	    		if($(this).data('nombre')=='Res'){
	    			$(this).css('background','orange');$(this).css('font-weight','bold');	    			
	    		}
	    	});
	    	
	    	$('#tabla_fres tbody tr').map(function(){	
              	$(this).children("td").each(function(){
       				//if($(this).data('d1') < 1){	
            		if(($(this).html() == 'G1-2' || $(this).html() == 'XX' || $(this).html() == '50%') && $(this).html() != '' ) {
                		$(this).css("background-color", "orange");
            		}
            		if(($(this).html() == 'G2' || $(this).html() == 'G3' || $(this).html() == 'G4' || $(this).html() == 'XXX' || $(this).html() == '0%') && $(this).html() != '' ) {
                		$(this).css("background-color", "red");
            		}
            		
        		})
    		});	    	
	      },		
	});
	
	   	
});  	
</script>
