<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/oc.png" width="25" height="25" border="0"> Requisiciones </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px" >
		    <div class="ajaxSorterDiv" id="tabla_almreq" name="tabla_almreq" align="left"  >  
		    	No:<select name="req" id="req" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
          							$data['result']=$this->almacenreq_model->verRequisicion();
									foreach ($data['result'] as $row):?>
           							<option value="<?php echo $row->Nr;?>" ><?php echo $row->Requisicion;?></option>
           							<?php endforeach;
           						?>    
           			</select>      
           			<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="reqn" name="reqn" value="Nueva Requisicion" />
            	<span class="ajaxTable" style=" height: 345px; width: 935px; background-color: white; ">
                    <table id="mytablaAreq" name="mytablaAreq" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <th data-order = "FechaS1" >Día</th>                            
                            <th data-order = "hora" >Hora</th>
                            <th data-order = "CantidadS" >Cantidad</th>
                            <th data-order = "UnidadS" >Unidad</th>                                                       
                            <th data-order = "DescripcionS1" >Descripción</th>
                            <th data-order = "FechaR">Recibido</th>
                            <th data-order = "Obs" >Observaciones</th>
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                 </span>
                 <div class="ajaxpager" style="margin-top: -10px">        
                    <ul class="order_list"><input size="2%"  type="text" name="solp" id="solp" style="text-align: left;  border: 0; background: none">
                    	<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="nuereq" name="nuereq" value="Agregar" />
                    </ul>                          
                    <form method="post" action="<?= base_url()?>index.php/almacenreq/pdfrep" >
                    	<input size="3%" type="hidden" name="requisicion" id="requisicion" /> 
                    	<img style="cursor: pointer" style="margin-top: 6px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                    </form>   
 				</div>
 				
 			</div>
 			<div id='ver_mas_req' class='hide' >
 			<table border="2px" style="width: 500px;  margin-top: -15px">
 						<thead style="background-color: #DBE5F1">
 							<th colspan="4">Procesar Datos en calidad de Recibido:
 								&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="1" onclick="radios(1,-1)" />Si</td>
								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="0" onclick="radios(1,0)" />No</td>									
								&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="-1" onclick="radios(1,1)" />Incompleto</td>
 								<input type="hidden" size="3%" name="cans" id="cans" />
 								<input type="hidden" size="3%" name="id" id="id" />
 								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
 								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="borrar" name="borrar" value="Borrar" />
 								&nbsp;&nbsp;<input style="font-size: 12px" type="submit" onclick="cerrar(1)" id="x" name="x" value="Cerrar" />
 							</th>
 						</thead>
 						<tbody style="background-color: #F7F7F7">
 							<tr>
 								<th style="text-align: left">Cantidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="can" id="can" style="text-align: center;  border: 0"></th>
 								<th style="text-align: left">Unidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="uni" id="uni" style="text-align: center;  border: 0"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Descripcion</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="des" id="des" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Observaciones</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="obs" id="obs" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 						</tbody>
 						
 				</table>
 			</div>
 			<div id='ver_mas_reqn' class='hide' >
 			<table border="2px" style="width: 500px; margin-top: -15px">
 						<thead style="background-color: #DBE5F1">
 							<th colspan="4">Agregar a Requisición
 								<input type="hidden" size="3%" name="id1" id="id1" />
 								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="aceptar1" name="aceptar1" value="Guardar" /> 								
 								&nbsp;&nbsp;<input style="font-size: 12px" type="submit" onclick="cerrar(2)" id="x" name="x" value="Cerrar" />
 							</th>
 						</thead>
 						<tbody style="background-color: #F7F7F7">
 							<tr>
 								<th style="text-align: left">Cantidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="can1" id="can1" style="text-align: center;  border: 0"></th>
 								<th style="text-align: left">Unidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="uni1" id="uni1" style="text-align: center;  border: 0"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Descripcion</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="des1" id="des1" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Observaciones</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Eduardo Lizárraga"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="obs1" id="obs1" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 						</tbody>
 						
 				</table>
 			</div>
 	</div>             
    
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#req").change( function(){		
	$(this).removeClass('used');
	$('#ver_mas_req').hide();		
	$('#ver_mas_reqn').hide();
	$("#requisicion").val($("#req").val());
 return true;
});
function radios(radio,dato){
	if(radio==1) { $("#cans").val(dato);}
}
function cerrar($sel){
	$(this).removeClass('used');
	if($sel==1){ $('#ver_mas_req').hide(); }else{ $('#ver_mas_reqn').hide();}
}
$("#nuevo").click( function(){	
	$("#id").val('');			
    $("#can").val('');
    $("#uni").val('');
    $("#des").val('');
    $("#obs").val('');
    $("#cans").val('');
 return true;
})
$("#nuereq").click( function(){
	$(this).removeClass('used');
	$('#ver_mas_req').hide();
	$('#ver_mas_reqn').show();	
	$("#id1").val('');			
    $("#can1").val('');
    $("#uni1").val('');
    $("#des1").val('');
    $("#obs1").val('');
    $("#can1").focus();    
 return true;
})
$("#reqn").click( function(){
	$(this).removeClass('used');
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/almacenreq/agregarreq", 
			//data: "can="+$("#can1").val()+"&uni="+$("#uni1").val()+"&des="+$("#des1").val()+"&obs="+$("#obs1").val()+"&req="+$("#req").val(),
			success: 	
					function(msg){															
						if(msg!=0){														
								$("#mytablaAreq").trigger("update");
								//$("#req").trigger("update");
								//$(this).removeClass('used');
								//$('#ver_mas_reqn').hide();
								alert("Requisicion Nueva Agregada correctamente");										
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
	});
});
$("#aceptar1").click( function(){	
	can=$("#can1").val();
	des=$("#des1").val();
	if(can!=''){
	 if(des!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almacenreq/agregar", 
				data: "can="+$("#can1").val()+"&uni="+$("#uni1").val()+"&des="+$("#des1").val()+"&obs="+$("#obs1").val()+"&req="+$("#req").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaAreq").trigger("update");
								$("#nuevo").click();
								$(this).removeClass('used');
								$('#ver_mas_reqn').hide();
								alert("Datos Agregados correctamente");										
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita registrar Descripcion para poder actualizar datos");	
		$("#des1").focus();			
		return false;
	}
	}else{
		alert("Error: Necesita registrar Cantidad para poder actualizar datos");
		$("#can1").focus();				
		return false;
	}	 
});
$("#aceptar").click( function(){	
	can=$("#can").val();
	des=$("#des").val();
	if(can!=''){
	 if(des!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almacenreq/actualizar", 
				data: "id="+$("#id").val()+"&can="+$("#can").val()+"&uni="+$("#uni").val()+"&des="+$("#des").val()+"&obs="+$("#obs").val()+"&cans="+$("#cans").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaAreq").trigger("update");
								$("#nuevo").click();
								$(this).removeClass('used');
								$('#ver_mas_req').hide();
								alert("Datos Actualizados correctamente");										
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita registrar Descripcion para poder actualizar datos");	
		$("#des").focus();			
		return false;
	}
	}else{
		alert("Error: Necesita registrar Cantidad para poder actualizar datos");
		$("#can").focus();				
		return false;
	}	 
});
$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almacenreq/borrar", 
				data: "id="+$("#id").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaAreq").trigger("update");
								$("#nuevo").click();
								$(this).removeClass('used');
								$('#ver_mas_req').hide();
								alert("Datos eliminados correctamente");										
																								
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder actualizar datos");				
		return false;
	}	 
});
$(document).ready(function(){ 
	$("#requisicion").val($("#req").val());
	$(this).removeClass('used');
   	$('#ver_mas_req').hide();
   	$('#ver_mas_reqn').hide();
	$("#tabla_almreq").ajaxSorter({
		url:'<?php echo base_url()?>index.php/almacenreq/tabla',  		
		filters:['req'],
		active:['Recibido',1],
		//multipleFilter:true, 
    	sort:false,
    	onRowClick:function(){
    		$(this).removeClass('used');
   			//$('#ver_mas_req').hide();
   			$('#ver_mas_reqn').hide();
    		$("#id").val($(this).data('ns'));			
    		$("#can").val($(this).data('cantidads'));
    		$("#uni").val($(this).data('unidads'));
    		$("#des").val($(this).data('descripcions'));
    		$("#obs").val($(this).data('obs'));
    		$("#cans").val($(this).data('cans'));
    		ser=$(this).data('cans');
			if(ser==-1 || ser==0 || ser==1){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				if(ser==1){ $('input:radio[name=rblugar]:nth(2)').attr('checked',true); }
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				$('input:radio[name=rblugar]:nth(2)').attr('checked',false); 
			}
			$(this).removeClass('used');
   			$('#ver_mas_req').show();
    	},   
    	onSuccess:function(){
    		$('#tabla_almreq tbody tr').map(function(){
    	   		$("#solp").val($(this).data('totp'));	
    	   });
    	},          
	});
	
});  	
</script>