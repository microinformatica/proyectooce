<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/ecommerce.png" width="25" height="25" border="0"> 
			
			<?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ 
				echo "Solicitudes Pendientes de Envío"; 
			 } else{ 
				echo "Solicitudes Pendientes de Recibir";
			}?> 
		</li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px" >
		    <div class="ajaxSorterDiv" id="tabla_almdia" name="tabla_almdia" align="left"  >             	
            	<span class="ajaxTable" style=" height: 350px; width: 935px; background-color: white; ">
                    <table id="mytablaAdia" name="mytablaAdia" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <th data-order = "FechaS1" >Fecha [DT]</th>
                            <th data-order = "Requisicion">Req</th>
                            <th data-order = "hora" >Hora</th>
                            <th data-order = "CantidadS" >Cantidad</th>
                            <th data-order = "UnidadS" >Unidad</th>                                                       
                            <th data-order = "DescripcionS" >Descripción -( Observaciones )-</th>
                        </thead>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        </tbody>
                    </table>
                 </span>
                 <div class="ajaxpager" style="margin-top: -10px">        
                    <ul class="order_list">En Total: <input size="2%"  type="text" name="solp" id="solp" style="text-align: left;  border: 0; background: none"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/almacendia/pdfrep" >
                    	 
                    	<img style="cursor: pointer" style="margin-top: 6px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                    </form>   
 				</div>
 				
 			</div>
 			<div id='ver_mas_pedi' class='hide' >
 			<table border="2px" style="width: 500px; margin-top: -18px">
 						<thead style="background-color: #DBE5F1">
 							<th colspan="4">Procesar Datos en calidad de Recibido:
 								&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="-1" onclick="radios(1,-1)" />Si</td>
								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="0" onclick="radios(1,0)" />No</td>									
								&nbsp;&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ ?> disabled="true" <?php } ?> name="rblugar" type="radio" value="1" onclick="radios(1,1)" />Incompleto</td>
 								<input type="hidden" size="3%"  name="cans" id="cans" />
 								<input type="hidden" size="3%"  name="id" id="id" /> 								
 								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
 								&nbsp;&nbsp;<input <?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ ?> disabled="true" <?php } ?> style="font-size: 12px" type="submit" id="borrar" name="borrar" value="Borrar" />
 								&nbsp;&nbsp;<input style="font-size: 12px" type="submit" onclick="cerrar()" id="x" name="x" value="Cerrar" />
 							</th>
 						</thead>
 						<tbody style="background-color: #F7F7F7">
 							<tr>
 								<th style="text-align: left">Cantidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="can" id="can" style="text-align: center;  border: 0"></th>
 								<th style="text-align: left">Unidad</th>
 								<th style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ ?> disabled="true" <?php } ?> size="21%" type="text" name="uni" id="uni" style="text-align: center;  border: 0"></th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Descripcion</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="des" id="des" style="text-align: left;  border: 0; margin-left: 2px">
 									
 								</th>
 							</tr>
 							<tr>
 								<th style="text-align: left">Observaciones</th>
 								<th colspan="3" style="text-align: left;background-color: white;"><input <?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ ?> disabled="true" <?php } ?> size="78%" type="text" name="obs" id="obs" style="text-align: left;  border: 0; margin-left: 2px"></th>
 							</tr>
 							<tr>
 								<th colspan="4" style="text-align: left">Apoyo de Calidad Total
 									<select name="cali" id="cali" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;" <?php if($usuario!="Jesus Benítez" && $usuario!="Miguel Penuelas"){ ?> disabled="true" <?php } ?>>
   										<option value="0" >No</option><option value="1" >Si</option>
   									</select>
								</th>
 							</tr>
 								
 						</tbody>
 						
 				</table>
 			</div>
 	</div>             
    
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
function radios(radio,dato){
	if(radio==1) { $("#cans").val(dato);}
}
function cerrar(){
	$(this).removeClass('used');
	$('#ver_mas_pedi').hide();
}
$("#nuevo").click( function(){	
	$("#id").val('');			
    $("#can").val('');
    $("#uni").val('');
    $("#des").val('');
    $("#obs").val('');
    $("#cans").val('');
    $("#cali").val(0);
 return true;
}
)
$("#aceptar").click( function(){	
	can=$("#can").val();
	des=$("#des").val();
	if(can!=''){
	 if(des!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almacendia/actualizar", 
				data: "id="+$("#id").val()+"&can="+$("#can").val()+"&uni="+$("#uni").val()+"&des="+$("#des").val()+"&obs="+$("#obs").val()+"&cans="+$("#cans").val()+"&cali="+$("#cali").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaAdia").trigger("update");$
								alert("Datos Actualizados correctamente");										
								$("#nuevo").click();
								$(this).removeClass('used');
								$('#ver_mas_pedi').hide();																
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita registrar Descripcion para poder actualizar datos");	
		$("#des").focus();			
		return false;
	}
	}else{
		alert("Error: Necesita registrar Cantidad para poder actualizar datos");
		$("#can").focus();				
		return false;
	}	 
});
$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/almacendia/borrar", 
				data: "id="+$("#id").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaAdia").trigger("update");$
								alert("Datos eliminados correctamente");										
								$("#nuevo").click();
								$(this).removeClass('used');
								$('#ver_mas_pedi').hide();																
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder actualizar datos");				
		return false;
	}	 
});
$(document).ready(function(){ 
	$(this).removeClass('used');
   	$('#ver_mas_pedi').hide();
	$("#tabla_almdia").ajaxSorter({
		url:'<?php echo base_url()?>index.php/almacendia/tabla',  		
		//filters:['cer'],
		active:['CanS',1],
		multipleFilter:true, 
    	sort:false,
    	onRowClick:function(){
    		$("#id").val($(this).data('ns'));			
    		$("#can").val($(this).data('cantidads'));
    		$("#uni").val($(this).data('unidads'));
    		$("#des").val($(this).data('des1'));
    		$("#obs").val($(this).data('obs'));
    		$("#cans").val($(this).data('cans'));$("#cali").val($(this).data('cali'));
    		ser=$(this).data('cans');
			if(ser==-1 || ser==0 || ser==1){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				if(ser==1){ $('input:radio[name=rblugar]:nth(2)').attr('checked',true); }
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				$('input:radio[name=rblugar]:nth(2)').attr('checked',false); 
			}
			$(this).removeClass('used');
   			$('#ver_mas_pedi').show();
    	},   
    	onSuccess:function(){
    		$('#tabla_almdia tbody tr').map(function(){
    	   		$("#solp").val($(this).data('totp'));	
    	   });
    	   $('#tabla_almdia tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })
    	},          
	});
	
});  	
</script>