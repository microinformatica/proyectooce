<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 25px;
	/*background: none;*/		
}   
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>
<div style="height:600px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li style="font-size: 14px"><a href="#facnot"><img src="<?php echo base_url();?>assets/images/menu/facturas2.png" width="20" height="20" border="0"> Elaboración</a></li>
		<li style="font-size: 14px"><a href="#dep"><img src="<?php echo base_url();?>assets/images/menu/banco.png" width="25" height="25" border="0"> Depósitos </a></li>
		<li style="font-size: 14px"><a href="#cli"><img src="<?php echo base_url();?>assets/images/menu/cliente.png" width="20" height="20" border="0"> Clientes</a></li>
		<li style="font-size: 14px"><a href="#saldo"><img src="<?php echo base_url();?>assets/images/menu/facturas.png" width="25" height="25" border="0"> Saldos </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="facnot" class="tab_content"  style="height: 555px"  >
		  <div id="accordion" style=" margin-top: -10px; width: 960px; margin-left: -10px; " >	
			<h3 align="left" style="font-size: 12px" ><a href="#">Facturas - Nota de Crédito</a></h3>
			<div style="height: 550px; " >
					<div class="ajaxSorterDiv" id="tabla_facnot" name="tabla_facnot" style="height: 475px;  margin-left: -20px; width: 910px;" > 
       		 			<div class="ajaxpager" style="font-size: 12px; margin-top: 1px;">
             				<form action="<?= base_url()?>index.php/facnot/pdffacnot" method="POST" >	
								<ul class="order_list" style="margin-top: 1px; width: 800px; text-align: left">
									Seleccione: 
									<select name="tipfne" id="tipfne" style="font-size: 10px; height: 25px; margin-top: 1px   " >
    									<option value="0">Fac / NC</option>
    									<option value="1">Factura</option>
    									<option value="7">Factura-C</option>
    									<option value="4">Nota de Crédito</option>
    									<option value="9">Nota de Crédito-C</option>
    									<option value="5">Pagos</option>
    									<option value="8">Pagos-C</option>
    									<option value="6">Almacenaje</option>
    								</select>                			
									Factura Sin Depósito:
				 					<select name="sd" id="sd" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="0">NO</option>
    									<option value="1">SI</option>
    								</select>	
									Dia: <input size="13%" type="text" name="fecini" id="fecini" class="fecha redondo mUp" style="text-align: center;" >
									Mostrar Facturas de Almacenaje :
				 					<select name="fal" id="fal" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="0">NO</option>
    									<option value="1">SI</option>
    								</select>
								</ul>						
    	    	            	<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    <input type="hidden" name="tablaFacNot" value ="" class="htmlTable"/>
                    	    </form>   
                		</div>  
						<span class="ajaxTable" style="margin-top: 10px;height: 390px;">
                    		<table id="mytablaFacNot" name="mytablaFacNot" class="ajaxSorter" border="1" >
                        		<thead>     
                        			<tr style="font-size: 12px">
                           				<th data-order = "fecfn1" style="width: 80px">Fecha</th>
                           				<th data-order = "numfn1" style="width: 40px">No.</th>
                           				<th data-order = "rasfn" style="width: 300px">Razón Social</th>
                           				<th data-order = "impp" style="width: 90px">Pesos</th>
                           				<th data-order = "impd" style="width: 90px">Dólares</th>
                           				<th data-order = "obsfn1">Observaciones</th>
                           			</tr>
                           		</thead>
                        		<tbody style="font-size: 12px">
                        		</tbody>                        	
                    		</table>
                		</span> 
             	</div>	
		</div>
       <h3 align="left" style="font-size: 12px">
        <?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez"){ ?>	<a href="#" >Agregar o Actualizar Datos</a><?php } ?>
        <?php if($usuario!="Jesus Benítez" && $usuario!="Zuleima Benitez"){ ?> <a href="#" >Analizar Datos</a><?php } ?>        
       </h3>		
       <div style="height:150px; margin-top: -5px; ">  
        	<table border="1" style="margin-top: -5px; margin-left: -10px;" >
				 			<tr>
				 				<th colspan="5" style="text-align: left">
				 					Ciclo:
				 					 <select name="cicfn" id="cicfn" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="2021">2021</option>
    									<option value="2020">2020</option>
    									<option value="2019">2019</option>
    								</select>
				 					<input type="hidden" readonly="true" size="2%"  type="text" name="idfn" id="idfn">
				 					<select name="tipfn" id="tipfn" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="0">Fac / NC</option>
    									<option value="1">Factura</option>
    									<option value="7">Factura-C</option>
    									<option value="4">Nota de Crédito</option>
    									<option value="9">Nota de Crédito-C</option>
    									<option value="5">Pagos</option>
    									<option value="8">Pagos-C</option>
    									<option value="6">Almacenaje</option>
    								</select>
				 					No.
				 					<input  size="8%" type="text" name="numfn" id="numfn" style="text-align: center; border: none; color: red;" >
				 					Factura Sin Depósito:
				 					<select name="facsd" id="facsd" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="0">NO</option>
    									<option value="1">SI</option>
    								</select>	 					
				 				</th>
				 			</tr>
				 			<tr>
				 				<th style="text-align: center">Fecha</th><th colspan="2" style="text-align: center">Cliente</th><th style="text-align: center">Importe</th>
				 				<th style="text-align: center">Moneda</th>
				 			</tr>
				 			<tr>
				 				<th>
				 					<input size="13%" type="text" name="fecfn" id="fecfn" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
				 				</th>
				 				<th colspan="2">
				 					<input type="hidden" name='idcfn' id="idcfn" >
				 					<input name='rasnom' id="rasnom" style="width: 510px" >
				 				</th>
				 				
				 				<th>
				 					<input size="14%" type="text" name="impfn" id="impfn" style="text-align: right;" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" >
				 				</th>
				 				<th style="text-align: center"> 
				 					<select name="tippd" id="tippd" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="">MN / DLLS</option>
    									<option value="MN">MN</option>
    									<option value="DLLS">DLLS</option>
    								</select>	
				 				</th>
				 				<tr>
				 				<th colspan="5">
				 					Producto:
				 					 <select name="tippc" id="tippc" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="2">Camarón</option>
    									<option value="1">Postlarva</option>
    									<option value="3">Cam. Maquilado</option>
    								</select>	
    								Estatus:
				 					 <select name="estfn" id="estfn" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="1">Vigente</option>
    									<option style="color:white; background-color: red" value="2">Cancelada</option>
    								</select>
    								Efecto Contable:
				 					 <select name="efefn" id="efefn" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    									<option value="1">Cargo</option>
    									<option value="2">Abono</option>
    								</select>	
    								Si es Abono Aplicarlo a Factura No.
    								<input size="8%" type="text" name="aplfac" id="aplfac" style="text-align: center; color: blue;" >
				 				</th>
				 				</tr>
				 				<tr>
				 				<th colspan="5">
				 					Observaciones:
				 				</th>
				 				</tr>
				 				<tr>
				 				<th colspan="5">
				 					<textarea  name="obsfn" id="obsfn"  cols="92" style="resize: none; text-align: justify" rows="5"></textarea>
				 				</th>
				 				</tr>
				 				<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez"){ ?>	
				 				<tr>
				 				<th colspan="5">
				 					<input type="submit" id="aceptarfn" name="aceptarfn" value="Guardar" />
									<input type="submit" id="nuevofn" name="nuevofn" value="Nuevo"  /> 	
				 				</th>
				 				</tr>
				 				<?php } ?>	
				 				<tr>
				 				<th colspan="5">
				 					<div id='ver_mas_ajaxsorter' class='hide' style="height: 160px" >
				 					<div class="ajaxSorterDiv" id="tabla_faccli" name="tabla_faccli" style=" margin-top: 1px;">                
               			        	  <span class="ajaxTable" style=" height: 130px; background-color: white">
                    					<table id="mytablaFaccli" name="mytablaFaccli" class="ajaxSorter">
                    						<thead title="Presione las columnas para ordenarlas como requiera">    
                    							<th data-order = "rfcfn" >R.F.C.</th>                        
                    							<th data-order = "rasfn" >Razon Social</th>                                                                                    
                    						</thead>
                    						<tbody title="Seleccione para asignar cliente a Depósito">                                                 	  
                    						</tbody>
                    					</table>
                				 	</span> 
            	 					</div> 
            	 					</div>  
				 				</th>
				 				</tr>
				 			</tr>
				 		</table>							
			</div>
        </div>
		</div>
		<div id="dep" class="tab_content" style="height: 580px" >	
			<table border="0" style="margin-left: -5px; ">
				<tr>
					<th>			
					<div class="ajaxSorterDiv" id="tabla_dep" name="tabla_dep" style="height: 565px; width: 875px ; " >
        			
                  <span class="ajaxTable" style="height: 515px;  width: 873px; ">
                    	<table id="mytablaDep" name="mytablaDep" class="ajaxSorter"  style="" >
                        	<thead>                            
                            	<th data-order = "Fecha1" style="width: 70px;">Fecha</th>
                           		<th data-order = "Razon" style="width: 450px;">Razon Social</th> 
                           		<th data-order = "factura" style="width: 40px;">Factura</th> 
                           		<th data-order = "estatuss" style="width: 4px;"> </th>                                                       							                                                                                  
                            	<th data-order = "Pesos2" style="width: 80px;">M.N.</th>
                            	<th data-order = "ImporteD2" style="width: 80px;">U.S.D.</th>                           
                        	</thead>
                        	<tbody style="font-size: 12px">  	</tbody>
                    	</table>
                	</span> 
             		<div class="ajaxpager" style="margin-top:-20px">        
                    	<form method="post" action="<?= base_url()?>index.php/facnot/pdfdep" >  
                    	<ul class="order_list" style="width:820px">
                    		Correspondientes del Día <input size="8%" type="text" name="dpDesde" id="dpDesde" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center; margin-top: -18px" >               	
						Al 	<input size="8%" type="text" name="dpHasta" id="dpHasta"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;margin-top: -18px" >            
                		Excluir depósitos en efectivo:
                		<select name="evo" id="evo" style="font-size: 10px; height: 25px; width: 55px;margin-top: -18px " >								
    						<option value="0">NO</option>		
    						<option value="1">SI</option>
    					</select>
                    	Depósitos sin Fac:
                    	<select name="sinf" id="sinf" style="font-size: 10px; height: 25px; width: 55px;margin-top: -18px " >								
    						<option value="0">Todos</option>		
    						<option value="1">SI</option>
    					</select>	
                    	</ul>     
                    	                  	             	                                                
                            <img style="cursor: pointer; margin-top: 5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        	<!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        	<input type="hidden" name="tabladep" value ="" class="htmlTable"/>                                                      
 						</form>    
 					</div>                                      
            		</div>
	           		</th>
            	</tr>
            	<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez"){ ?>	
            	<tr>
            		<th>
            			No. Factura:
            			<input style="border: 1" type="text" name="factura" id="factura" size="10%">
            			 Granja:
							<select name="ngra" id="ngra" style="font-size: 10px; height: 23px; margin-top: -25px " >								
    							<option value="0">Sel.</option>		
    							<option value="4">Ahome</option><option value="6">Huatabampo</option>
    							<option value="10">Topolobampo</option><option value="12">Astorga</option>
    							<option value="2">Kino</option>
    						</select>
            			 Estatus:
							<select name="estatuss" id="estatuss" style="font-size: 10px; height: 23px; margin-top: -25px " >								
    							<option value="0">Sel.</option>		
    							<option value="1">Pagada</option>
    							<option value="2">Abonada</option>
    							<option value="3">Pendiente</option>
    						</select>
    					
            			<input style="font-size: 14px" type="submit" id="aceptarD" name="aceptarD" value="Guardar" />
            			
            			<input name="gral" id="gral" style="border: none; width: 420px" >
            			<input type="hidden" name="nd" id="nd" >
            		</th>
            	</tr>
            	<?php } ?>
			</table>	
		</div>
		
		<div id="cli" class="tab_content" style="height: 555px" >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
					<th>			
						<input type="hidden" readonly="true" size="2%"  type="text" name="idctefn" id="idctefn">						
						R.F.C.: <input type="text" name="rfcfn" id="rfcfn" style="width: 120px" onKeyUp="this.value = this.value.toUpperCase();">
						Razón Social: <input type="text" name="rasfn" id="rasfn" style="width: 460px">
						<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez"){ ?>
							<input type="submit" id="aceptarcfn" name="aceptarcfn" value="Guardar" />
							<input type="submit" id="nuevocfn" name="nuevocfn" value="Nuevo" />
						<?php }?>		 		
                 		<div class="ajaxSorterDiv" id="tabla_fn" name="tabla_fn" style="width: 805px;  "  >                 			
							<span class="ajaxTable" style="margin-top: 10px;height: 480px; " >
                    			<table id="mytablaFN" name="mytablaFN" class="ajaxSorter" border="1" style="width: 803px;" >
                        			<thead>                            
                            			<th data-order = "rfcfn" >R.F.C.</th>
                            			<th data-order = "rasfn" >Razón Social</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 12px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; "> 
								<form action="<?= base_url()?>index.php/facnot/pdfrepfn" method="POST" >							
    	    	            		<ul class="order_list" style="width: 250px; margin-top: 1px">
    	    	            				
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablafn" value ="" class="htmlTable"/> 
								</form>   
                			</div>       	                	
	                	</div>
	           		</th>
            	</tr>
			</table>	
		</div>
		<div id="saldo" class="tab_content" style="height: 555px" >
			<div align="left" style="margin-top: 1px;"> 
				Cliente:<input style="border:0; background-color: lightgray; cursor: pointer" title="click para buscar cliente" readonly="true" size="78%"  type="text" name="nombre" id="nombre" >	
           		<input size="4%" type="hidden" name="aidcfn1" id="aidcfn1"/>
           		<input size="4%" type="hidden" name="aidcfn" id="aidcfn"/>
				<div id='ver_mas_ajaxsorters' class='hide' style="height: 100px" >                			              
                		<div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" style="width: 579px; margin-top: 1px; margin-left: 44px">                
               			     <span class="ajaxTable" style="margin-left: 0px; background-color: white; height: 70px; width: 577px">
                    			<table id="mytablaC" name="mytablaC" class="ajaxSorter">
                        			<thead title="Presione las columnas para ordenarlas como requiera">                            
                        				<th data-order = "rasfn" >Razon Social</th>                                                                                    
                        				<th data-order = "rfcfn" >RFC</th>
                        			</thead>
                        			<tbody title="Seleccione para consultar estado de cuenta" style="font-size: 11px">                                                 	  
                        			</tbody>
                    			</table>
                			 </span> 
            			</div>
                	</div>
               </div>	
                <div id='ver_mas_saldos' class='hide' style="height: 100px" >
			<div class="ajaxSorterDiv" id="tabla_ecc" name="tabla_ecc" align="left" style=" height: 498px; margin-top: -5px; " >             	
               <span class="ajaxTable" style=" height: 430px; width: 900px ">
                    <table id="mytablaECC" name="mytablaECC" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 12px"> 
                        	<th data-order = "numfn" style="width: 30px">Factura</th>
                        	<th data-order = "numfng" style="width: 30px">Movs</th>
                        	<th data-order = "fecfn1" style="width: 55px">Fecha</th>
                            <th data-order = "obsfn" style="width: 250px">Observaciones</th>                            
                            <th data-order = "Cargo1" style="width: 50px">Cargo</th>
                            <th data-order = "Abono1" style="width: 50px;">Abono</th>
                            <th data-order = "saldofila" style="width: 50px" >Saldo</th>                                                       
                        </thead>
                        <tbody title="Seleccione para analizar" style="font-size: 12px">	
                        </tbody>
                    </table>
                </span> 
             	<div class="ajaxpager" style="margin-top: -10px">        
                    <ul class="order_list" style="width: 400px">Registro y Actualización de Aplicaciones de Abonos a Facturas</ul>                          
                    <form method="post" action="<?= base_url()?>index.php/facnot/pdffacsd" >
                    	<input type="hidden"  id="razon" name="razon" />
                        <img style="cursor: pointer" style="margin-top: 6px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" name="tablasind" value ="" class="htmlTable"/> 
 					</form>   
 			    </div> 
 			    </div> 
            
            <table class="ajaxSorter" border="1" style="margin-top: -10px;">
            	<tr style="background-color: lightblue;">
            		<th style="text-align: center;">Factura</th>
            		<th style="text-align: center;">Fecha</th>
            		<th style="text-align: center;">Importe</th>
            		<th style="text-align: center;">Moneda</th>
            		<th style="text-align: center;">Estatus</th>
            		<th style="text-align: center;">Observaciones</th>
            		<?php if($usuario=="Jesus Benítez" || $usuario=="Zuleima Benitez"){ ?>
            		<th rowspan="2" style="text-align: center;">
            			
            		<input type="submit" id="aceptarfna" name="aceptarfna" value="Guardar" />
						<input type="submit" id="nuevofna" name="nuevofna" value="Nuevo"  /> 	
						<input type="submit" id="borrarabo" name="borrarabo" value="Borrar"  />
            		</th>
            		<?php } ?>
            	</tr>
            	<tr>
            		<th style="text-align: center;">
            			<input type="hidden" readonly="true" type="text" name="aidfn" id="aidfn">
            			<select name="aaplfac" id="aaplfac" style="font-size: 11px;height: 25px;  margin-top: 1px;">
            				<option value="0">Seleccione</option>
          				</select>
            		</th>
            		<th style="text-align: center;"><input type="text" name="afecfn" id="afecfn"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center; width: 80px" ></th>
            		<th style="text-align: center;"><input type="text" name="aimpfn" id="aimpfn" style="text-align: right;width: 120px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" ></th>
            		<th style="text-align: center"> 
				 		<select name="atippd" id="atippd" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    						<option value="">MN / DLLS</option>
    						<option value="MN">MN</option>
    						<option value="DLLS">DLLS</option>
    					</select>	
				 	</th>
            		<th style="text-align: center;">
            			<select name="aestfn" id="aestfn" style="font-size: 10px; height: 25px; margin-top: 1px   " >								
    						<option value="1">Vigente</option>
    						<option style="color:white; background-color: red" value="2">Cancelada</option>
    					</select>
            		</th>
            		<th ><textarea  name="aobsfn" id="aobsfn"  cols="75" style="resize: none; text-align: justify" rows="2"></textarea></th>
            	</tr>
            </table>
            </div>
		</div>			
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$('#nombre').click(function(){
  // $('#ver_mas_ajaxsorters').show();
   if($(this).hasClass('used')){
    		$(this).removeClass('used');	
    		$('#ver_mas_ajaxsorters').hide();
			$('#ver_mas_saldos').show();
    	}    			
		else{
			$(this).addClass('used');
			
			$('#ver_mas_ajaxsorters').show();
    		$('#ver_mas_saldos').hide();
		}
});

/*$('#aidcfn').click(function(){
    $("#razon").val($('#aidcfn').val());
});
*/
$("#aceptarD").click( function(){	
	numero=$("#nd").val();
	//factura=$("#factura").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/facnot/actualizarD", 
						data: "id="+$("#nd").val()+"&fac="+$("#factura").val()+"&est="+$("#estatuss").val()+"&gra="+$("#ngra").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Factura Actualizada correctamente");
										$("#mytablaDep").trigger("update");
										$("#nd").val('');$("#factura").val('');$("#estatuss").val('');$("#ngra").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Depósito");
		return false;
	}
});
$("#borrarabo").click( function(){	
	numero=$("#aidfn").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/facnot/borrarabo", 
						data: "id="+$("#aidfn").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaECC").trigger("update");
										$("#nuevofna").click();
																																
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Abono para poder Eliminarlo");
		return false;
	}
});
$("#nuevofna").click( function(){	
	//$("#numfn").val('');
						
	$("#aidfn").val('');
	$("#aimpfn").val('');
	$("#atippd").val('');
	$("#aobsfn").val('');$("#aaplfac").val('0');
	var f = new Date();
	$("#afecfn").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
 return true;
});
$("#aceptarfna").click( function(){		
numero=$("#aidfn").val();numfn=$("#aaplfac").val();ras=$("#aidcfn1").val();imp=$("#aimpfn").val();tip=$("#atippd").val();
 if( ras!='0'){
 if( numfn!='0'){
  	if( imp!=''){
  		if( tip!=''){
	if(numero!=''){
	  	$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/facnot/actualizarfacnot", 
				data: "id="+$("#aidfn").val()+"&idc="+$("#aidcfn1").val()+"&num=0"+"&fec="+$("#afecfn").val()+"&imp="+$("#aimpfn").val()+"&tippd="+$("#atippd").val()+"&tipc=1"+"&tipfn=3"+"&est="+$("#aestfn").val()+"&obs="+$("#aobsfn").val()+"&efe=3"+"&apl="+$("#aaplfac").val()+"&fsd=1"+"&cic="+$("#cicfn").val()+"&ban=2",
				success: 	
						function(msg){															
							if(msg!=0){														
								//$("#mytablaFacNot").trigger("update");	
								$("#mytablaECC").trigger("update");
								$("#nuevofna").click();
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}		
			});	
	}else{
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/facnot/agregarfacnot", 
				data: "idc="+$("#aidcfn1").val()+"&num=0"+"&fec="+$("#afecfn").val()+"&imp="+$("#aimpfn").val()+"&tippd="+$("#atippd").val()+"&tipc=1"+"&tipfn=3"+"&est="+$("#aestfn").val()+"&obs="+$("#aobsfn").val()+"&efe=3"+"&apl="+$("#aaplfac").val()+"&fsd=1"+"&cic="+$("#cicfn").val()+"&ban=2",
				success: 	
						function(msg){															
							if(msg!=0){														
								
								//$("#mytablaFacNot").trigger("update");
								$("#mytablaECC").trigger("update");
								//$("#aidcfn").val('');
								$("#nuevofna").click();
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}		
			});	
	}
	}else{
			alert("Error: Seleccione Tipo de Moneda");	
			$("#atipd").focus();
			return false;
	}
	}else{
			alert("Error: Registre Importe");	
			$("#aimpfn").focus();
			return false;
	}
						
	}else{
		alert("Error: Seleccione Factura");	
		$("#anumfn").focus();
		return false;
	}
	}else{
			alert("Error: Seleccione Cliente");	
			$("#aidcfn").focus();
			$("#aidcfn").click();
			return false;
	}
});



$("#aceptarfn").click( function(){		
numero=$("#idfn").val();numfn=$("#tipfn").val();ras=$("#rasnom").val();imp=$("#impfn").val();tip=$("#tippd").val();
 if( numfn!='0'){
  if( ras!=''){
  	if( imp!=''){
  		if( tip!=''){
	if(numero!=''){
	  	$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/facnot/actualizarfacnot", 
				data: "id="+$("#idfn").val()+"&idc="+$("#idcfn").val()+"&num="+$("#numfn").val()+"&fec="+$("#fecfn").val()+"&imp="+$("#impfn").val()+"&tippd="+$("#tippd").val()+"&tipc="+$("#tippc").val()+"&tipfn="+$("#tipfn").val()+"&est="+$("#estfn").val()+"&obs="+$("#obsfn").val()+"&efe="+$("#efefn").val()+"&apl="+$("#aplfac").val()+"&fsd="+$("#facsd").val()+"&cic="+$("#cicfn").val()+"&ban=1",
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#mytablaFacNot").trigger("update");	
								$("#mytablaECC").trigger("update");
								$("#accordion").accordion( "activate",0 );
								$("#nuevofn").click();
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}		
			});	
	}else{
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/facnot/agregarfacnot", 
				data: "idc="+$("#idcfn").val()+"&num="+$("#numfn").val()+"&fec="+$("#fecfn").val()+"&imp="+$("#impfn").val()+"&tippd="+$("#tippd").val()+"&tipc="+$("#tippc").val()+"&tipfn="+$("#tipfn").val()+"&est="+$("#estfn").val()+"&obs="+$("#obsfn").val()+"&efe="+$("#efefn").val()+"&apl="+$("#aplfac").val()+"&fsd="+$("#facsd").val()+"&cic="+$("#cicfn").val()+"&ban=1",
				success: 	
						function(msg){															
							if(msg!=0){														
								
								$("#mytablaFacNot").trigger("update");
								$("#mytablaECC").trigger("update");
								$("#accordion").accordion( "activate",0 );	
								$("#nuevofn").click();
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}		
			});	
	}
	}else{
			alert("Error: Seleccione Tipo de Moneda");	
			$("#tipd").focus();
			return false;
	}
	}else{
			alert("Error: Registre Importe");	
			$("#impfn").focus();
			return false;
	}
	}else{
			alert("Error: Registre Nombre de Cliente");	
			$("#rasnom").focus();
			$("#rasnom").click();
			return false;
	}					
	}else{
		alert("Error: Seleccione Si es Factura, Nota de Crédito o Pago");	
		$("#tipfn").focus();
		return false;
	}
});




$('#rasnom').click(function(){
    $('#ver_mas_ajaxsorter').show();
});

//busca la ultima factura o nota
$("#tipfn").change( function(){	
	con=$("#tipfn").val();
	$("#tipfne").val($("#tipfn").val());
	$("#mytablaFacNot").trigger("update");
 	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/facnot/buscarfn", 
			data: "fn="+$("#tipfn").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#numfn").val(obj.numfn);
					$("#rasnom").val('');					
					$("#idfn").val('');
					$("#impfn").val('');
					$("#tippd").val('');
					var f = new Date();
					$("#fecfn").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
   			} 
	});	
	if(con==5){$("#efefn").val('2');$("#facsd").val('1');}else{$("#efefn").val('');}		 
});

$("#aidcfn").change( function(){
	//$("#aidcfn").val('');
	$("#nuevofna").click();
});

$("#aidcfn1").change( function(){
	$("#aidcfn").val($("#aidcfn1").val());
});

$("#nuevocfn").click( function(){	
	$("#idctfn").val('');$("#rfcfn").val('');$("#rasfn").val('');
	$("#rfcfn").focus();
	return true;
});
$("#nuevofn").click( function(){	
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/facnot/buscarfn", 
			data: "fn="+$("#tipfn").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#numfn").val(obj.numfn);
					$("#rasnom").val('');					
					$("#idfn").val('');
					$("#impfn").val('');
					$("#tippd").val('');
					$("#obsfn").val('');$("#efefn").val('');$("#aplfac").val('');$("#facsd").val('');
					var f = new Date();
					$("#fecfn").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
   			} 
	});		
 return true;
});
$("#aceptarcfn").click( function(){		
numero=$("#idctefn").val();rfc=$("#rfcfn").val();ras=$("#rasfn").val();
 if( rfc!=''){
  if( ras!=''){
	if(numero!=''){
	  	$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/facnot/actualizarfn", 
				data: "id="+$("#idctefn").val()+"&rfc="+$("#rfcfn").val()+"&ras="+$("#rasfn").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								$("#idctefn").val('');$("#rfcfn").val('');$("#rasfn").val('');
								$("#mytablaFN").trigger("update");	
								$("#mytablaFaccli").trigger("update");
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}		
			});
	}else{
		//busca en rfc si existe manda que ya esta registrado
		$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/facnot/buscarrfc", 
			data: "rfc="+$("#rfcfn").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					if(obj.ras !=''){
						alert("R.F.C. ya esta Asignado a Cliente "+obj.ras);
						$("#rfcfn").focus();
					}else{
						$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/facnot/agregarfn", 
						data: "rfc="+$("#rfcfn").val()+"&ras="+$("#rasfn").val(),
						success: 	
							function(msg){															
								if(msg!=0){														
									$("#idctefn").val('');$("#rfcfn").val('');$("#rasfn").val('');
									$("#mytablaFN").trigger("update");
									$("#mytablaFaccli").trigger("update");	
								}else{
									alert("Error con la base de datos o usted no ha actualizado nada");
								}					
							}		
						});	
					}	
   			} 
		});			
	}
	}else{
			alert("Error: Registre Nombre de Cliente");	
			$("#rasfn").focus();
			return false;
		}					
	}else{
		alert("Error: Registre RFC de Cliente");	
		$("#rfcfn").focus();
		return false;
	}
});

$("#afecfn").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#fecfn").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#fecini").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

$("#dpDesde").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#dpHasta").datepicker( "option", "minDate", selectedDate );
		$('#tabla_dep').trigger('update')
	}
});
$("#dpHasta").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#dpDesde").datepicker( "option", "maxDate", selectedDate );
		$('#tabla_dep').trigger('update')
	}
});		
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});
$(document).ready(function(){
	$('#ver_mas_ajaxsorter').hide();
	$('#ver_mas_ajaxsorters').hide();
	$('#ver_mas_saldos').hide();
	var f = new Date();
	$("#fecfn").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#afecfn").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#fecini").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#dpDesde").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + (f.getDate() -7));
	$("#dpHasta").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
		
	$("#tabla_fn").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facnot/tablafn',  
		//filters:['cmbGranjaf'],	
        sort:false,
        multipleFilter:true,
        onRowClick:function(){
    		$("#idctefn").val($(this).data('idctefn'));
        	$("#rfcfn").val($(this).data('rfcfn'));
        	$("#rasfn").val($(this).data('rasfn'));
		}, 		 
    }); 
    
    $("#tabla_faccli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facnot/tablafn',  
		//filters:['cmbGranjaf'],	
        sort:false,
        multipleFilter:true,
        onRowClick:function(){
        	$('#ver_mas_ajaxsorter').hide();
    		$("#idcfn").val($(this).data('idctefn'));
        	//$("#rfcfn").val($(this).data('rfcfn'));
        	$("#rasnom").val($(this).data('rasfn'));
        	$("#impfn").focus();
		}, 		 
    });  
    
   
    $("#tabla_facnot").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facnot/tablafacnot',  
		filters:['tipfne','sd','fal','fecini'],
		active:['facsd','1'],
		sort:false,
        multipleFilter:true,
        onRowClick:function(){
        	$("#accordion").accordion( "activate",1 );
    		$("#idfn").val($(this).data('idfn'));
    		$("#idcfn").val($(this).data('idcfn'));
        	$("#numfn").val($(this).data('numfn'));
        	$("#fecfn").val($(this).data('fecfn'));
        	$("#rasnom").val($(this).data('rasfn'));
        	$("#impfn").val($(this).data('impfn'));
        	$("#tipfn").val($(this).data('tipfn'));
        	$("#tippd").val($(this).data('tippd'));
        	$("#tippc").val($(this).data('tippc'));
        	$("#estfn").val($(this).data('estfn'));
        	$("#obsfn").val($(this).data('obsfn'));
        	$("#efefn").val($(this).data('efefn'));
        	$("#aplfac").val($(this).data('aplfac'));
        	$("#facsd").val($(this).data('facsd'));
        	$("#cicfn").val($(this).data('cicfn'));
		}, 	
		onSuccess:function(){
    		$('#tabla_facnot tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })	    	
    		$('#tabla_facnot tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_facnot tbody tr').map(function(){
	    		if($(this).data('estfn')=='2'){	    			
	    			$(this).css('background','red');
	    		}
	    	});
	    	$('#tabla_facnot tbody tr').map(function(){
		    	if($(this).data('fecfn1')=='Total:') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    });
    	},    	
    });
    
    $("#tabla_dep").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facnot/tabdep',  
		filters:['dpDesde','dpHasta','evo','sinf'],
		active:['evo','1'],
		multipleFilter:true,
    	sort:false,
    	onRowClick:function(){
        	$("#nd").val($(this).data('nd'));
        	$("#factura").val($(this).data('factura'));$("#estatuss").val($(this).data('estatuss'));$("#ngra").val($(this).data('ngra'));
        	$("#gral").val($(this).data('fecha')+' '+$(this).data('nomc') +' '+$(this).data('impo'));
        	$("#factura").focus();
		}, 	
    	onSuccess:function(){
    		$('#tabla_dep tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_dep tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })	    	
    		$('#tabla_dep tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_dep tbody tr').map(function(){
		    	if($(this).data('fecha1')=='Total:') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    			$(this).css('text-align','center');
	    		}
		    });
		    $('#tabla_dep tbody tr td:nth-child(4)').map(function(){ 
					if($(this).html() == 3 && $(this).html() != '' ) {
              				$(this).css("background-color", "red");$(this).css("color", "red");
        			}
        			if($(this).html() == 2 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "orange");
        			}
        			if($(this).html() == 1 && $(this).html() != '' ) {
             				$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
        			} 
	    	});
    	}, 
	});
	
	
   
   $("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facnot/tablafn',  
		//filters:['cmbGranjaf'],	
        sort:false,
        multipleFilter:true,
        onRowClick:function(){
        	$('#ver_mas_ajaxsorters').hide();
			$('#ver_mas_saldos').show();
			$("#aidcfn1").val($(this).data('idctefn')); $("#nomc").val($(this).data('idctefn'));$("#razon").val($(this).data('idctefn'));
			$("#facturs").val($(this).data('idctefn'));
        	$("#nombre").val($(this).data('rasfn'));
        	$("#tabla_ecc").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facnot/tablasal',  
		filters:['aidcfn1'],
		sort:false,
    	onRowClick:function(){
    		if($(this).data('tipfn')==3){
        		$("#aidfn").val($(this).data('idfn'));
    			$("#aidcfn1").val($(this).data('idcfn'));
        		$("#afecfn").val($(this).data('fecfn'));
        		$("#aimpfn").val($(this).data('impfn'));
        		$("#atippd").val($(this).data('tippd'));
        		$("#atippc").val($(this).data('tippc'));
        		$("#aobsfn").val($(this).data('obsfn'));
        		$("#aaplfac").val($(this).data('aplfac'));
        }
		}, 
    	onSuccess:function(){
    		$('#tabla_ecc tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_ecc tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })	    	
    		$('#tabla_ecc tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_ecc tbody tr').map(function(){
	    		if($(this).data('numfn')!=''){	    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');
	    		}
	    	});
    	}, 
		});
		$('#aaplfac').boxLoader({
        url:'<?php echo base_url()?>index.php/facnot/combob',
        equal:{id:'idctefn',value:'#aidcfn1'},
        select:{id:'aplfac',val:'val'},
        all:"Sel.",   
   		});
    	}, 	 
    });
   
   
    
});


</script>

