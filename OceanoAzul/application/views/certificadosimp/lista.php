<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/printer.png" width="25" height="25" border="0"> Reportes </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px" >
		    <div class="ajaxSorterDiv" id="tabla_cerimp" name="tabla_cerimp" align="left"  >             	
            	<span class="ajaxTable" style=" height: 370px; width: 935px; background-color: white ">
                    <table id="mytablaCimp" name="mytablaCimp" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px">                         	
                            <th data-order = "Razon"  >Razón Social</th>                                                        
                            <th data-order = "FechaR" style="width: 60px">Fecha</th>                            
                            <th data-order = "RemisionR" style="width: 30px">Remisión</th>
                            <th data-order = "CantidadRR" style="width: 70px" >Cantidad</th>                                                       
                        </thead>
                        <tbody style="font-size: 11px">
                        </tbody>
                    </table>
                 </span>
                 <div class="ajaxpager">        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/certificadosimp/pdfrep" >
                    	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden" readonly="true" size="11%" type="text" name="cersel" id="cersel" style="text-align: left;  border: 0; background: none">  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> <input type="hidden" id="cer1" name="cer1" />  
                        <input type="hidden" id="fi1" name="fi1" /> <input type="hidden" id="ff1" name="ff1" /> 
                        <input type="hidden" id="aut1" name="aut1" /> <input type="hidden" id="ent1" name="ent1" />
                    </form>   
 				</div>
 			</div>
 		<table border="2px" style="width: 360px;margin-top: -20px ">
 						<thead style="background-color: #DBE5F1">
 							<th colspan="5">
 								Certificado de Movilización: DGSA-DSAP-CSAMO-<select name="cer" id="cer" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
          							$this->load->model('certificadosimp_model');
          							$data['result']=$this->certificadosimp_model->verCertificado();
									foreach ($data['result'] as $row):?>
           							<option value="<?php echo $row->NumReg;?>" ><?php echo $row->NumCer;?></option>
           							<?php endforeach;
           						?>
   								</select>  
 							</th>
 						</thead>
 						<tbody style="background-color: #F7F7F7">
 							<tr >
 								<th colspan="2" style="text-align: center">Vigencia</th>
 								<th colspan="3" style="text-align: center">Producto: Postlarvas</th>
 							</tr>
	 						<tr>
 								<th style="text-align: center">Inicial</th>
 								<th style="text-align: center">Final</th>
 								<th style="text-align: center">Autorizados</th>
 								<th style="text-align: center">Entregados</th>
 								<th style="text-align: center">Diferencia</th>
							</tr>
 							<tr style="background-color: white;">
	 							<th style="text-align: center"><input readonly="true" size="11%" type="text" name="fi" id="fi" style="text-align: center;  border: 0"></th>								
								<th style="text-align: center"><input readonly="true" size="11%" type="text" name="ff" id="ff" style="text-align: center;  border: 0"></th>
								<th style="text-align: center"><input readonly="true" size="13%" type="text" name="aut" id="aut" style="text-align: center;  border: 0"></th>
 								<th style="text-align: center"><input readonly="true" size="13%" type="text" name="ent" id="ent" style="text-align: center;  border: 0"></th>
	 							<th style="text-align: center"><input readonly="true" size="13%" type="text" name="dif" id="dif" style="text-align: center;  border: 0"></th>
 							</tr>
 						</tbody>
 					</table>	
 		
 	</div>
 	             
    
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$(document).ready(function(){ 
	$("#tabla_cerimp").ajaxSorter({
		url:'<?php echo base_url()?>index.php/certificadosimp/tabla',  		
		filters:['cer'],
    	sort:false,
    	onSuccess:function(){
    	   	$('#tabla_cerimp tbody tr').map(function(){
    	   		if($(this).data('remisionr')=='Total:'){
    	   			$(this).css('font-weight','bold'); 
    	   		}
    	   		$("#cersel").val($(this).data('numcer'));$("#cer1").val($(this).data('numcer'));
	    		if($(this).data('numcer')!='Sin Registro'){
	    			$("#fi").val($(this).data('inivig')); $("#fi1").val($(this).data('inivig'));
	    			$("#ff").val($(this).data('finvig')); $("#ff1").val($(this).data('finvig'));
	    		}
	    			$("#aut").val($(this).data('canpos'));$("#aut1").val($(this).data('canpos'));
	    			$("#ent").val($(this).data('entregado'));$("#ent1").val($(this).data('entregado'));
	    			$("#dif").val($(this).data('dif'));
	    	});
	    	$('#tabla_cerimp tbody tr td:nth-child(1)').map(function(){ if($(this).data('razon')!=''){
    	   			$(this).css('font-weight','bold'); 
    	   		} })
    	   		
	    	$('#tabla_cerimp tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center');  })
	    	$('#tabla_cerimp tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','center');  })
	    	$('#tabla_cerimp tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');  })
    	},                 
	});
});  	
</script>