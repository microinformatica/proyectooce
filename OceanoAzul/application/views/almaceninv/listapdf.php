<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 80px 50px 20px; }
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: orange; }	
</style> 
<title>Reporte General</title>
</head>
<body>
	<div id="header"> 
		<table style="width: 770px;">
			<tr>
				<td width="200px">
					<p style="font-size: 8px;"  >
						<img src="<?php echo base_url();?>assets/images/oceanoazul.jpg" width="200" height="60" border="0">
						SIA - <?= $usuario?> - <?= $perfil?> <br />
					</p>
				</td>	
				<td align="center" style="font-size: 14px; color: navy"><strong>Análisis General</strong></td>			
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: navy;">
				<td width="160px">www.aquapacific.com.mx</td>	
				<td width="490">
					Av. Reforma 2007-B 803, Col. Flamingos, Mazatlán, Sin. México, RFC:AQU-031003-CC9, CP:82149 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
	</div>

	<br/>
	<table  width="725px" border="1" style="font-size: 9px;">
    	  <?= $tablapro?>    	 
	</table>	
	<br />
     Movimientos de Entradas y Salidas
	<table  width="725px" border="1" style="font-size: 9px;">
    	  <?= $tabladet?>
	</table>	
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(0,0,0);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>
