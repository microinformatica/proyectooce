<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts/js/modules/exporting.js"></script>




<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
	</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 25px;
	/*background: none;*/		
}  
/*
table {
  border: 0;
  padding: 0;
  margin: 0 0 20px 0;
  border-collapse: collapse;
}

th {
  padding: 5px;
  
  text-align: right;
  font-weight: bold;
  line-height: 2em;
  color: #FFF;
  background-color: #555;
}

tbody td {
  padding: 10px;
  line-height: 18px;
  border-top: 1px solid #E0E0E0;
}

tbody tr:nth-child(2n) {
  background-color: #F7F7F7;
}

tbody tr:hover {
  background-color: #EEEEEE;
}

td {
  text-align: right;
}

td:first-child,
th:first-child {
  text-align: left;
} */
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		
		<li style="font-size: 14px"><a href="#todos"><img src="<?php echo base_url();?>assets/images/menu/camarones.png" width="20" height="20" border="0">Siembra</a> </li>			
		
		<?php if($usuario=="Jesus Benítez" || $usuario=="Antonio Valdez" || $usuario=="Julio Lizarraga" || $usuario=="Supervisor" || $perfil=="Sup. Parametrista" ){ ?>
		<li style="font-size: 14px"><a href="#bio"><img src="<?php echo base_url();?>assets/images/menu/balanza.png" width="20" height="20" border="0">Bio</a></li>
		<li style="font-size: 14px"><a href="#sob"><img src="<?php echo base_url();?>assets/images/menu/balanza.png" width="20" height="20" border="0">Sob</a></li>
		<li style="font-size: 14px"><a href="#par"><img src="<?php echo base_url();?>assets/images/menu/oximetro.png" width="20" height="20" border="0">Par</a></li>
		<!--
		<li style="font-size: 14px"><a href="#ali"><img src="<?php echo base_url();?>assets/images/menu/alimento.png" width="20" height="20" border="0"> Ali</a></li>
		-->
		<?php } ?>
		<?php if($usuario=="Antonio Hernandez"){ ?>
		<li style="font-size: 14px"><a href="#pg"><img src="<?php echo base_url();?>assets/images/menu/oximetro.png" width="20" height="20" border="0">Parametros</a></li>
		<li style="font-size: 14px"><a href="#sem"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">Semana</a></li>
		<?php } ?>
		<?php if($usuario=="Antonio Valdez" || $usuario=="Julio Lizarraga"){ ?>
			<li style="font-size: 14px"><a href="#sema"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">Semana</a></li>
		<?php } ?>	
		<?php if($perfil!="Sup. Parametrista"){ ?>
		<li style="font-size: 14px"><a href="#indisem"><img src="<?php echo base_url();?>assets/images/menu/semana.png" width="20" height="20" border="0">Indicadores</a></li>	
		<li style="font-size: 14px"><a href="#est"><img src="<?php echo base_url();?>assets/images/menu/granja.png" width="20" height="20" border="0">Estanque</a></li>
		<li style="font-size: 14px"><a href="#pro"><img src="<?php echo base_url();?>assets/images/menu/prod.png" width="20" height="20" border="0">Producción</a></li>
		<?php } ?>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		
		<div id="todos" class="tab_content" >
			<div id = "accordion" style="margin-top: -10px; margin-left: -10px; width: 958px;" >
					<h3 align="left" style="font-size: 12px" ><a href="#"> Estanques </a></h3>
					<div style="height: 350px;">
									
						<div class="ajaxSorterDiv" id="tabla_pis" name="tabla_pis" style="height: 403px;margin-top: -21px; ">                                		
                		<span class="ajaxTable" style="height: 363px; width: 860px" >
                			
                    	<table id="mytablaPis" name="mytablaPis" class="ajaxSorter" >
                        	<thead title="Presione las columnas para ordenarlas como requiera">                            
                            	<th data-order = "pisg" >Estanque</th>           
                            	<th data-order = "hasg" >Has</th>
                            	<th data-order = "espg">Especie</th> 
                            	<th data-order = "orgg">Organismos</th>  
                            	<th data-order = "den" >Densidad</th>                            
                            	<th data-order = "prog" >Procedencia</th>
                            	<th data-order = "fecg1" >Fecha</th>
                            	<th data-order = "plg" >Estadío</th>
                            	<th data-order = "lote" >Lote</th>
                        	</thead>
                        	<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                    	
                		</span> 
             			<div class="ajaxpager" style=" font-size: 10px; margin-top: -10px ">     
             				<form method="post" action="<?= base_url()?>index.php/aqpgranjac2/pdfrep/"  >
	                  	<ul class="order_list" style="width: 420px; margin-top: -10px ">
		             				Granja 
		             					<select name="cmbGranja" id="cmbGranja" style="font-size: 10px; height: 25px; " >								
		    										<option value="4">Ahome</option>
		    									</select>    							
		    								Ciclo
		    									<select name="cmbCiclop" id="cmbCiclop" style=" background-color: red;  font-size: 12px;height: 25px; margin-top: 1px;">
		      										<?php $ciclof=22; $actual=22; //date("y"); //$actual+=1;
													while($actual >= $ciclof){?>
														<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
																<?php  $actual-=1; } ?>      									
		    								  </select>
		    							</ul>  
    	                		<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tabla" value ="" class="htmlTable"/>
                    	    	<input type="hidden" id='granja' name="granja" />                         		                        		                                                      
 							</form>   
                		</div>                                      
            			</div>             	
            		
            	 	</div> 
            	 	<?php if($usuario=="Joel Lizárraga"){ ?>
        			<h3 align="left" style="font-size: 12px"><a href="#">Información Estanque</a></h3>
        			<?php }else{ ?>
        			<h3 align="left" style="font-size: 12px"><a href="#">Actualizar</a></h3>	
        			<?php } ?>
		       	 	<div style="height: 150px;">
		       	 		<table style="width: 758px" border="2px"  >
							<thead style="background-color: #DBE5F1" >
								<th colspan="10" height="15px" style="font-size: 14px">Datos de Siembra - Granja:
									<select name="cmbGranjas" id="cmbGranjas" style="font-size: 10px; height: 25px;margin-top: 1px  " >								
    									<option value="4">Ahome</option>
    								</select>
    								<input type="hidden" size="4%" type="text" name="idpis" id="idpis" value="" style="text-align: right;  border: 0"></th>
							</thead>
							<tbody style="background-color: #F7F7F7">
								<tr >
									<th style="text-align: center">Ciclo</th>
									<th style="text-align: center">Fecha</th>
									<th style="text-align: center">Est</th>
									<th style="text-align: center">Sec</th>
									<th style="text-align: center">Has</th>
									<th style="text-align: center">Especie</th>
									<th style="text-align: center">Organismos</th>
									<th style="text-align: center">Procedencia</th>
									<th style="text-align: center">Estadío</th>
									<th style="text-align: center">Lote</th>	
      					</tr>															
								<tr style="background-color: white; font-size: 12px">
									
									<th style="text-align: center" align="center">
										<select name="cica" id="cica" style=" background-color: red;  font-size: 12px;height: 25px; margin-top: 1px;">
      										<?php $ciclof=22; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
														<?php  $actual-=1; } ?>      									
    								  </select>
    								</th>
    							  <th style="text-align: center"><input size="12%" type="text" name="fecg" id="fecg" class="fecha redondo mUp" style="text-align: center;  border: 0" ></th>								
										<th style="text-align: center">
    									<!--<input size="3%" type="text" name="pisg" id="pisg" value="" style="text-align: right;  border: 0">-->
    									<select name="pisg" id="pisg" style=" font-size: 12px;height: 25px; margin-top: 1px;">
      										<option value="0" > Sel </option>
      										<?php $ciclof=80; $actual=1; //$actual+=1;
											while($actual <= $ciclof){?>
													<option value="<?php echo $actual;?>" > <?php echo $actual?> </option>
            								<?php  $actual+=1; } ?>      									
    								  </select>
    								</th>
    								<th style="text-align: center">
    									<select name="secc" id="secc" style=" font-size: 12px;height: 25px; margin-top: 1px;">
      										<option value="0" > Sel </option>
      										<option value="41" > I </option><option value="42" > II </option>
      										<option value="43" > III </option><option value="44" > IV </option>      									
    								  </select>
    								</th>
    								<th style="text-align: center"><input size="5%" type="text" name="hasg" id="hasg" value="" style="text-align: center;  border: 0"></th>
									<th style="text-align: center"><input size="18%" type="text" name="espg" id="espg" style="text-align: center; border: 0" value="Litopenaeus vannamei"></th>
									<th style="text-align: center"><input size="13%" type="text" name="orgg" id="orgg" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" value="" style="text-align: right;  border: 0"></th>
									<th style="text-align: center"><input size="20%" type="text" name="prog" id="prog" style="text-align: center; border: 0" value="Aquapacfic S.A. de C.V."></th>
									<th style="text-align: center"><input size="4%" type="text" name="plg" id="plg" style="text-align: center; border: 0"></th>
									<th><input size="3%" type="text" name="lote" id="lote" value="" style="text-align: center;border: 0"></th>
								</tr>
								<?php if($usuario=="Jesus Benítez"){ ?>
								<tr >
									<th colspan="10" style="text-align: left; background-color: lightblue; font-size: 14px">Datos de Cosecha Finales</th>
      							</tr>	
								<tr >
									<th colspan="1" style="text-align: center">Fecha</th>
									<th colspan="2" style="text-align: center">Biomasa (kg)</th>
									<th colspan="1" style="text-align: center">Peso Promedio</th>		
									<th colspan="4" style="text-align: center">Observaciones</th>
      							</tr>															
								<tr style="background-color: white;">									
									<th colspan="1" style="text-align: center"><input size="12%" type="text" name="fecgc" id="fecgc" class="fecha redondo mUp" style="text-align: center;  " ></th>
    								<th colspan="2" style="text-align: center"><input size="15%" type="text" name="biogc" id="biogc" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" style="text-align: right;  "></th>
    								<th colspan="1" style="text-align: center"><input size="6%" type="text" name="ppgc" id="ppgc" style="text-align: right;  "></th>									
    								<th colspan="4" style="text-align: center"><input size="80%" type="text" name="obsc" id="obsc" style="text-align: justify; font-size: 10px  "></th>
								</tr>	
								<?php }?>						
    							</tbody>
								<tfoot style="background-color: #DBE5F1">
									<th colspan="10" >
										<center>
										<?php if($usuario=="Jesus Benítez" || $usuario=="Antonio Valdez" || $usuario=="Antonio Hernandez"){ ?>
											<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
											<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
										<?php }?>	
										<input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar()" />
										</center>	
									</th>
								</tfoot>
								<?php if($usuario=="Jesus Benítez"){ ?>
								<table style="width: 458px" border="2px"  >
									<thead style="background-color: #DBE5F1" >
										<th colspan="5" height="15px" style="font-size: 14px">Datos de Precosecha<input type="hidden" size="4%" type="text" name="idcpis" id="idcpis" value="" style="text-align: right;  border: 0"></th>
									</thead>
									<tbody style="background-color: #F7F7F7">
										<tr>
											<th colspan="5">
												<div class="ajaxSorterDiv" id="tabla_pre" name="tabla_pre" style="height: 103px; margin-top: -10px; width: 790px ">                                		
                									<span class="ajaxTable" style="height: 90px; width: 780px" >
                    									<table id="mytablaPre" name="mytablaPre" class="ajaxSorter" >
                        									<thead>                            
                            									<th data-order = "pisg" >Fecha</th>                                 
                            									<th data-order = "hasg" >Biomasa</th>
                            									<th data-order = "espg">Peso Promedio</th> 
                            									<th data-order = "orgg">Observaciones</th>  
                            								</thead>
                        									<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        									</tbody>                        	
                    									</table>
                									</span> 
             									</div>
											</th>
										</tr>
										<tr >
											<th rowspan="2" style="text-align: center">
												<input style="font-size: 18px" type="submit" id="agrega" name="agrega" value="+" title="AGREGAR y ACTUALIZAR PRECOSECHA" />
												<input style="font-size: 18px" type="submit" id="quita" name="quita" value="-" title="QUITAR PRECOSECHA" />
											</th>
											<th style="text-align: center">Fecha</th>
											<th style="text-align: center">Biomasa (kg)</th>
											<th style="text-align: center">Peso Promedio</th>		
											<th style="text-align: center">Observaciones</th>
											
      									</tr>															
										<tr style="background-color: white;">									
											<th style="text-align: center"><input size="12%" type="text" name="fecgc" id="fecgc" class="fecha redondo mUp" style="text-align: center;  " ></th>
    										<th style="text-align: center"><input size="15%" type="text" name="biogc" id="biogc" onKeyPress="return(currencyFormat(this,',','.',event,'C'));" style="text-align: right;  "></th>
    										<th style="text-align: center"><input size="6%" type="text" name="ppgc" id="ppgc" style="text-align: right;  "></th>									
    										<th style="text-align: center"><input size="80%" type="text" name="obsc" id="obsc" style="text-align: justify; font-size: 11px  "></th>
										</tr>
									</tbody>	
								</table>
									<?php }?>
    					</table>  
        			</div>
	 		</div>	
	 	</div>
	 	
	 	<?php if($usuario=="Jesus Benítez" || $usuario=="Antonio Valdez" || $usuario=="Julio Lizarraga" || $usuario=="Supervisor" || $perfil=="Sup. Parametrista"){ ?>
		<div id="bio" class="tab_content"  >	
			<div style="width: 800px">
			<table class="ajaxSorter" border="2" style="width: 780px" >
				<thead>
						<th>Granja</th><th>Ciclo</th><th>Fecha</th> <!--<th>Sección</th>-->
						<th>Estanque</th><th>Peso</th><th>Inc Est</th><th >Observaciones</th>
						
				</thead>
				<tbody>
					<th>
						<select name="cmbGranjab" id="cmbGranjab" style="font-size: 10px; height: 25px; margin-top: 1px " >								
    						<option value="4">Ahome</option>
    					</select>
    							
    				</th>
					<th>    
						             
                    <?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
    								<select name="cmbCiclob" id="cmbCiclob" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      										<?php $ciclof=15; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
            								<?php  $actual-=1; } ?>      									
            				  </select>
    							<?php } else{ ?>
    								<select name="cmbCiclob" id="cmbCiclob" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      										<?php $ciclof=22; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
														<?php  $actual-=1; } ?>      									
    								  </select>	
    							<?php } ?>	
                   </th>
                   
						<th style="text-align: center">
							<input type="hidden" readonly="true" size="2%"  type="text" name="idbio" id="idbio">
							<input size="10%" type="text" name="fecb" id="fecb" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>
						<!--
						<th>
							<?php if($usuario=="Antonio Valdez" || $usuario=="Supervisor" || $usuario=="Antonio Hernandez" || $perfil=="Sup. Parametrista"){ ?>	 	
				 					<select name='seccb' id="seccb" style="font-size: 12px;height: 25px;margin-top: 1px;"  ></select>
				 					<!--
				 					<select name="seccb1" id="seccb1" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="41">20-30</option>		
    									<option value="42">21-25</option>
    									<option value="43">Doble Ciclo</option>
    								</select>
    						
    							<?php } ?>
						</th>
						-->
						<th >
							<!--<select name='idpisb2' id="idpisb2" style="margin-top: 1px;  "  ></select>-->
							<select name='idpisb2' id="idpisb2" style="font-size: 12px;height: 25px;margin-top: 1px;"  ></select>
						</th>	
						
						<th style="text-align: center">
							<input style="text-align: right" size="14%"  type="text" name="pesb" id="pesb" >
						</th>
						<th style="text-align: center">
							<input style="text-align: right" size="4%"  type="text" name="incest" id="incest" >
						</th>
						<th ><input size="35%"  type="text" name="obsb" id="obsb"></th>
						
				</tbody>
				<tfoot>
					<th colspan="8" style="text-align: center">
						<?php if($usuario=="Jesus Benítez" || $usuario=="Julio Lizarraga" || $usuario=="Antonio Valdez" || $perfil=="Sup. Parametrista"){ ?>
						<input type="submit" id="nuevob" name="nuevob" value="Nuevo"  />
							<input type="submit" id="aceptarb" name="aceptarb" value="Guardar" />
							<input type="submit" id="borrarb" name="borrarb" value="Borrar" />
						<?php }?>	
					</th>
				</tfoot>
			</table>
			</div>
			
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_bio" name="tabla_bio" style="width: 760px"  >                 			
							<span class="ajaxTable" style="height: 355px; margin-top: 1px; " >
                    			<table id="mytablaBio" name="mytablaBio" class="ajaxSorter" border="1" style="width: 743px" >
                        			<thead>                            
                            			<!--<th data-order = "cap" >Estatus</th>-->
                            			<th data-order = "fecb1" >Fecha</th>
                            			<th data-order = "pisg" >Estanque</th>
                            			<th data-order = "pesb"  >Peso</th>
                            			<th data-order = "incest"  >Inc. Est.</th>
                            			<th data-order = "obsb" >Observaciones</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form action="<?= base_url()?>index.php/aqpgranjac2/pdfrepbio" method="POST" >							
    	    	            		<ul class="order_list" style="width: 550px; margin-top: 1px">
    	    	            			<!--Fecha de Movimientos:
    	    	            			<input size="10%" type="text" name="txtFI" id="txtFI" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                								&nbsp; al <input size="10%" type="text" name="txtFF" id="txtFF"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                						-->		
    	    	            			</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablabio" value ="" class="htmlTable"/> 
								</form>   
                			</div>       	                	
	                	</div>
	                	
            		</th>
            	</tr>
			</table>	
		</div>		
		<div id="sob" class="tab_content"  >	
			<div style="width: 800px">
			<table class="ajaxSorter" border="2" style="width: 770px" >
				<thead>
						<th>Granja</th><th>Ciclo</th><th>Fecha</th> <!--<th>Sección</th>-->
						<th>Estanque</th><th>Sobrevivencia</th><th>Cam/m2</th><th >Observaciones</th>
						
				</thead>
				<tbody>
					<th>
									<select name="cmbGranjass" id="cmbGranjass" style="font-size: 10px; height: 25px; margin-top: 1px " >								
    									<option value="4">Ahome</option>
    								</select>
    										
					</th>
					<th>                 
                    <?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
    								<select name="cmbCiclos" id="cmbCiclos" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      										<?php $ciclof=15; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>
    							<?php } else{ ?>
    								<select name="cmbCiclos" id="cmbCiclos" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      										<?php $ciclof=21; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
														<?php  $actual-=1; } ?>      									
    								  </select>	
    							<?php } ?>	
                   </th>
						<th style="text-align: center">
							<input type="hidden" readonly="true" size="2%"  type="text" name="idsob" id="idsob">
							<input type="hidden" readonly="true" size="2%"  type="text" name="hasest" id="hasest">
							<input type="hidden" readonly="true" size="2%"  type="text" name="orgest" id="orgest">
							<input size="10%" type="text" name="fecs" id="fecs" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>
						<!--
						<th>
							<?php if($usuario=="Antonio Valdez" || $usuario=="Supervisor" || $usuario=="Antonio Hernandez"  || $perfil=="Sup. Parametrista"){ ?>	 	
				 					<select name="secco" id="secco" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="41">20-30</option>		
    									<option value="42">21-25</option>
    									<option value="43">Doble Ciclo</option>
    								</select>
    							<?php } ?>
						</th>
						-->
						<th >
							<select name='idpiss' id="idpiss" style="margin-top: 1px;  "  ></select>
						</th>	
						
						<th style="text-align: center">
							<input  style="text-align: right" size="4%"  type="text" name="sobp" id="sobp" >
						</th>
						<th style="text-align: center">
							<input  style="text-align: right" size="4%"  type="text" name="camm2" id="camm2" >
						</th>
						<th ><input size="35%"  type="text" name="obss" id="obss"></th>
						
				</tbody>
				<tfoot>
					<th colspan="8" style="text-align: center">
						<?php if($usuario=="Jesus Benítez" || $usuario=="Antonio Valdez"  || $perfil=="Sup. Parametrista"){ ?>
						<input type="submit" id="nuevos" name="nuevos" value="Nuevo"  />
							<input type="submit" id="aceptars" name="aceptars" value="Guardar" />
							<input type="submit" id="borrars" name="borrars" value="Borrar" />
						<?php }?>	
					</th>
				</tfoot>
			</table>
			</div>
			
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_sob" name="tabla_sob" style="width: 760px"  >                 			
							<span class="ajaxTable" style="height: 355px; margin-top: 1px; " >
                    			<table id="mytablaSob" name="mytablaSob" class="ajaxSorter" border="1" style="width: 743px" >
                        			<thead>                            
                            			<!--<th data-order = "cap" >Estatus</th>-->
                            			<th data-order = "fecs1" >Fecha</th>
                            			<th data-order = "pisg" >Estanque</th>
                            			<th data-order = "sobp"  >Sobrevivencia</th>
                            			<th data-order = "camm2"  >Cam/m2</th>
                            			<th data-order = "obss" >Observaciones</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form action="<?= base_url()?>index.php/aqpgranjac2/pdfrepbio" method="POST" >							
    	    	            		<ul class="order_list" style="width: 350px; margin-top: 1px">
    	    	            			<!--
    	    	            			Fecha de Movimientos:
    	    	            			<input size="10%" type="text" name="txtFIS" id="txtFIS" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                								&nbsp; al <input size="10%" type="text" name="txtFFS" id="txtFFS"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
    	    	            			
    	    	            		-->
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablasob" value ="" class="htmlTable"/> 
								</form>   
                			</div>       	                	
	                	</div>
	                	
            		</th>
            	</tr>
			</table>	
		</div>
		<div id="par" class="tab_content"  >
			<div style="width: 850px">
			<table class="ajaxSorter" border="2" style="width: 840px" >
				<thead>
						<th>Granja</th>					
						<th>Ciclo</th><th>Fecha</th> <th>Sección</th>
						<th>Estanque</th><th>Oxi mg/l max</th><th>Oxi mg/l min</th><th>Temp°C max</th><th>Temp°C min</th><th>% Recambio</th>
						<th colspan="4">Estado de Salud
							<input type="hidden" style="text-align: right" size="4%"  type="text" name="salfq" id="salfq" >
							<input type="hidden" style="text-align: right" size="4%"  type="text" name="turfq" id="turfq" >
							<input type="hidden" style="text-align: right" size="4%"  type="text" name="phfq" id="phfq" >
							<input type="hidden" style="text-align: right" size="4%"  type="text" name="salud1" id="salud1" >
						</th>
						<!--
						<th>Salinidad pm</th><th>Turbidez cm</th><th>pH</th>
						-->						
				</thead>
				<tbody>
				<th>
						<select name="cmbGranjafq" id="cmbGranjafq" style="font-size: 10px; height: 25px; margin-top: 1px " >								
    					<option value="4">Ahome</option>
    				</select>
    		</th>	
				<th>   
						 <?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
    								<select name="cmbCiclofq" id="cmbCiclofq" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      										<?php $ciclof=15; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>
    							<?php } else{ ?>
    								<select name="cmbCiclofq" id="cmbCiclofq" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      										<?php $ciclof=21; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
														<?php  $actual-=1; } ?>      									
    								  </select>	
    							<?php } ?>	          
						<th style="text-align: center">
							<input type="hidden" readonly="true" size="2%"  type="text" name="idpfq" id="idpfq">
							<input size="10%" type="text" name="fecfq" id="fecfq" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>
						
						<th>
							<?php if($usuario=="Antonio Valdez" || $usuario=="Supervisor" || $usuario=="Antonio Hernandez"  || $perfil=="Sup. Parametrista"){ ?>	 	
				 					<select name="seccp" id="seccp" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="1">I</option>		
    									<option value="2">II</option>
    									<option value="3">III</option>
    									<option value="4">IV</option>
    								</select>
    							<?php } ?>   							
						</th>
						
						<th >
							<input type="hidden" id="filest" name="filest" style="width: 40px;">
							<select name='idpisfq' id="idpisfq" style="margin-top: 1px;  "  ></select>
						</th>	
						<th style="text-align: center"><input style="text-align: right" size="4%"  type="text" name="o2fq" id="o2fq" ></th>
						<th style="text-align: center"><input style="text-align: right" size="4%"  type="text" name="o1fq" id="o1fq" ></th>
						<th style="text-align: center"><input style="text-align: right" size="4%"  type="text" name="t2fq" id="t2fq" ></th>
						<th style="text-align: center"><input style="text-align: right" size="4%"  type="text" name="t1fq" id="t1fq" ></th>
						<th style="text-align: center"><input style="text-align: right" size="4%"  type="text" name="reca" id="reca" ></th>
						<th style="background-color: #0F0;color:white;text-align: center"><input checked="true"  name="salud2" type="radio" value="1" onclick="radios(1,1)" />Bien</th>
						<th style="background-color: orange;color:white;text-align: center"><input  name="salud2" type="radio" value="0" onclick="radios(1,0)" />Regular</th>									
						<th style="background-color: red;color:white;text-align: center"><input  name="salud2" type="radio" value="-1" onclick="radios(1,-1)" />Malo</th>
						<th style="background-color: purple;color:white; text-align: center"><input  name="salud2" type="radio" value="-1" onclick="radios(1,2)" />Sano</th>
						<!--
						<th style="text-align: center"></th>
						<th style="text-align: center"></th>
						-->
				</tbody>
				<tfoot>
					<th colspan="14" style="text-align: center">
						<?php if($usuario=="Jesus Benítez" || $usuario=="Antonio Valdez"  || $perfil=="Sup. Parametrista"){ ?>
							<input type="submit" id="nuevofq" name="nuevofq" value="Nuevo"  />
							<input type="submit" id="aceptarfq" name="aceptarfq" value="Guardar" />
							<input type="submit" id="borrarfq" name="borrarfq" value="Borrar" />
						<?php  $actual-=1; } ?>	
					</th>
				</tfoot>
			</table>
			</div>
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_par" name="tabla_par" style="width: 760px"  >                 			
							<span class="ajaxTable" style="height: 345px; margin-top: 1px; " >
                    			<table id="mytablaPar" name="mytablaPar" class="ajaxSorter" border="1" style="width: 743px" >
                        			<thead>                            
                            			<th data-order = "fecfq1" >Fecha</th>
                            			<th data-order = "pisg" >Estanque</th>
                            			<th data-order = "o2fq"  >Oxi mg/l max</th>
                            			<th data-order = "o1fq"  >Oxi mg/l min</th>
                            			<th data-order = "t2fq"  >Temp°C max</th>
                            			<th data-order = "t1fq"  >Temp°C min</th>
                            			<th data-order = "reca"  >% Recambio</th>
                            			<th data-order = "salud"  >Salud</th>
                            			<!--
                            			<th data-order = "salfq"  >Salinidad pm</th>
                            			<th data-order = "turfq"  >Turbidez cm</th>
                            			<th data-order = "phfq"  >pH</th>
                            			-->
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form action="<?= base_url()?>index.php/aqpgranjac2/pdfreppar" method="POST" >							
    	    	            		<ul class="order_list" style="width: 350px; margin-top: 1px">
    	    	            			<!--
    	    	            			Fecha de Movimientos:
    	    	            			<input size="10%" type="text" name="txtFIfq" id="txtFIfq" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                								&nbsp; al <input size="10%" type="text" name="txtFFfq" id="txtFFfq"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
    	    	            		-->
    	    	            			</ul>
    	    	            		
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablapar" value ="" class="htmlTable"/> 
								</form>   
                			</div>       	                	
	                	</div>
	                	
            		</th>
            	</tr>
			</table>
		</div>	
		<!--	
		<div id="ali" class="tab_content"  >
			<div style="width: 800px">
			
			<table class="ajaxSorter" border="2" style="width: 780px" >
				<thead>
						<th>Granja</th><th>Ciclo</th><th>Fecha</th><th>Sección</th><th>Estanque</th><th>Cantidad</th><th >Observaciones</th>
						
				</thead>
				<tbody>
					<th>
						<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
             						<select name="cmbGranjaa" id="cmbGranjaa" style="font-size: 10px; height: 25px; margin-top: 1px " >								
    									<option value="4">Ahome</option>
    									<option value="2">Gran Kino</option>		
    								</select>
    							<?php } elseif($usuario=="Diana Peraza"){ ?>
    								<select name="cmbGranjaa" id="cmbGranjaa" style="font-size: 10px; height: 25px; margin-top: 1px  " >								
    									<option value="2">Gran Kino</option>		
    									<option value="4">Ahome</option>
    								</select>
    							<?php }elseif($usuario=="Antonio Valdez" || $usuario=="Supervisor" || $usuario=="Antonio Hernandez"){ ?>	 
    								<select name="cmbGranjaa" id="cmbGranjaa" style="font-size: 10px; height: 25px;  margin-top: 1px " >								
    									<option value="4">Ahome</option>
    								</select>
    							<?php }else{ ?>
    								<select name="cmbGranjaa" id="cmbGranjaa" style="font-size: 10px; height: 25px; margin-top: 1px  " >								
    									<option value="4">Ahome</option>
    								</select>
    							<?php } ?>	
					</th>
					<th>  
						 <?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
    								<select name="cmbCicloa" id="cmbCicloa" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      										<?php $ciclof=15; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>
    							<?php } else{ ?>
    								<select name="cmbCicloa" id="cmbCicloa" style="font-size: 10px;height: 25px;margin-top: 1px; ">
      										<?php $ciclof=17; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>	
    							<?php } ?>	               
                   </th>
						<th style="text-align: center">
							<input type="hidden" readonly="true" size="2%"  type="text" name="idali" id="idali">
							<input size="10%" type="text" name="feca" id="feca" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
							<button type="button" id="alimentod" style="width: 60px; cursor: pointer; background-color: lightblue"  >Alimento</button>	</th>					
						<th>
							<?php if($usuario=="Antonio Valdez" || $usuario=="Supervisor" || $usuario=="Antonio Hernandez"){ ?>	 	
				 					<select name="secca" id="secca" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="41">1</option>		
    									<option value="42">2</option>
    									<option value="43">3</option>
    									<option value="44">4</option>
    								</select>
    							<?php } ?>
    							<?php if($usuario=="Supervisor 1"){ ?>	
    								<select name="secca" id="secca" style="font-size: 12px; height: 25px;margin-top: 1px;  " >
    									<option value="41">1</option>	
    								</select>
    							<?php } ?> 	
    							<?php if($usuario=="Supervisor 2"){ ?>	
    								<select name="secca" id="secca" style="font-size: 12px; height: 25px;margin-top: 1px;  " >
    									<option value="42">2</option>	
    								</select>
    							<?php } ?>
    							<?php if($usuario=="Supervisor 3"){ ?>	
    								<select name="secca" id="secca" style="font-size: 12px; height: 25px;margin-top: 1px;  " >
    									<option value="43">3</option>	
    								</select>
    							<?php } ?>
    							<?php if($usuario=="Supervisor 4"){ ?>	
    								<select name="secca" id="secca" style="font-size: 12px; height: 25px;margin-top: 1px;  " >
    									<option value="44">4</option>	
    								</select>
    							<?php } ?>
						</th>
						<th >
							<select name='idpisali' id="idpisali" style="margin-top: 1px;  "  ></select>							
						</th>	
						
						<th style="text-align: center">
							<input onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right" size="14%"  type="text" name="canali" id="canali" >
						</th>
						<th ><input size="35%"  type="text" name="obsa" id="obsa"></th>
						
				</tbody>
				<tfoot>
					<th colspan="6" style="text-align: center">
						<?php if($usuario=="Jesus Benítez" || $usuario=="Diana Peraza"  || $usuario=="Antonio Valdez"){ ?>
							<input type="submit" id="nuevoa" name="nuevoa" value="Nuevo"  />
							<input type="submit" id="aceptara" name="aceptara" value="Guardar" />
							<input type="submit" id="borrara" name="borrara" value="Borrar" />
						<?php }?>
					</th>
				</tfoot>
			</table>
			</div>
			<div id='ver_ali' class='hide' style="height: 90px" >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_ali" name="tabla_ali" style="width: 760px"  >                 			
							<span class="ajaxTable" style="height: 355px; margin-top: 1px; " >
                    			<table id="mytablaAli" name="mytablaAli" class="ajaxSorter" border="1" style="width: 743px" >
                        			<thead>                            
                            			<th data-order = "feca1" >Fecha</th>
                            			<th data-order = "pisg" >Estanque</th>
                            			<th data-order = "canali"  >Cantidad</th>
                            			<th data-order = "obsali" >Observaciones</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form action="<?= base_url()?>index.php/aqpgranjac2/pdfrepali" method="POST" >							
    	    	            		<ul class="order_list" style="width: 650px; margin-top: 1px">
    	    	            			Sección
    	    	            			<?php if($usuario=="Antonio Valdez" || $usuario=="Supervisor" || $usuario=="Antonio Hernandez"){ ?>	 	
				 					<select name="secc" id="secc" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="41">1</option>		
    									<option value="42">2</option>
    									<option value="43">3</option>
    									<option value="44">4</option>
    								</select>
    							<?php } ?>
    							<?php if($usuario=="Supervisor 1"){ ?>	
    								<select name="secc" id="secc" style="font-size: 12px; height: 25px;margin-top: 1px;  " >
    									<option value="41">1</option>	
    								</select>
    							<?php } ?> 	
    							<?php if($usuario=="Supervisor 2"){ ?>	
    								<select name="secc" id="secc" style="font-size: 12px; height: 25px;margin-top: 1px;  " >
    									<option value="42">2</option>	
    								</select>
    							<?php } ?>
    							<?php if($usuario=="Supervisor 3"){ ?>	
    								<select name="secc" id="secc" style="font-size: 12px; height: 25px;margin-top: 1px;  " >
    									<option value="43">3</option>	
    								</select>
    							<?php } ?>
    							<?php if($usuario=="Supervisor 4"){ ?>	
    								<select name="secc" id="secc" style="font-size: 12px; height: 25px;margin-top: 1px;  " >
    									<option value="44">4</option>	
    								</select>
    							<?php } ?> 		
    									
    									Fecha de Movimientos:
    	    	            			<input size="10%" type="text" name="txtFIAli" id="txtFIAli" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                								&nbsp; al <input size="10%" type="text" name="txtFFAli" id="txtFFAli"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
    	    	            			</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablaali" value ="" class="htmlTable"/> 
								</form>   
                			</div>       	                	
	                	</div>
	                	
            		</th>
            	</tr>
			</table>
			</div>
			<div id='ver_aligra' class='hide' style="height: 90px" >
            	<button type="button" id="alid" style="cursor: pointer; background-color: red" class="continuar used" >Cerrar</button>
            	<div name="alidia" id="alidia" style="min-width: 900px; height: 430px; margin: 0 auto"></div>
            	
            </div>
		</div>
		-->
		<?php } ?>
		<?php if($usuario=="Antonio Hernandez"){ ?>
		<div id="pg" class="tab_content"  >
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_parg" name="tabla_parg" style="width: 760px"  >                 			
							<div class="ajaxpager" style=" font-size: 12px; margin-top: 1px"> 
								<form action="<?= base_url()?>index.php/aqpgranjac2/reportesempar" method="POST" >							
    	    	            		<ul class="order_list" style="width: 350px; margin-top: 1px">
    	    	            			<select name="cmbGranjapar" id="cmbGranjapar" style="font-size: 10px; height: 25px; margin-top: -5px; " >								
    										<option value="4">Ahome</option>
    									</select>
    	    	            			Sección: 
    	    	            			<select name="seccpg" id="seccpg" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    										<option value="0">Sel.</option>
    										<option value="41">I</option>		
    										<option value="42">II</option>
    										<option value="43">III</option>
    										<option value="44">IV</option>   										
    									</select>
    									Estanque:
    	    	            			<select name='idpisfqg' id="idpisfqg" style="margin-top: 1px;  "  ></select>
    	    	            			</ul>
    	    	            		<img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablaparg" value ="" class="htmlTable"/>
        	            	    	<!--
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablaparg" value ="" class="htmlTable"/> 
        	            	    	-->
								</form>   
                			</div>  
							<span class="ajaxTable" style="height: 345px; margin-top: 1px; " >
                    			<table id="mytablaParg" name="mytablaParg" class="ajaxSorter" border="1" style="width: 743px" >
                        			<thead>                            
                            			<th data-order = "pisg" >Estanque</th>
                            			<th data-order = "fecfq1" >Fecha</th>
                            			<th data-order = "kgd"  >Alimento</th>
                            			<th data-order = "o2fq"  >Oxi mg/l max</th>
                            			<th data-order = "o1fq"  >Oxi mg/l min</th>
                            			<th data-order = "t2fq"  >Temp°C max</th>
                            			<th data-order = "t1fq"  >Temp°C min</th>
                            			
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	     	                	
	                	</div>
	                	
            		</th>
            	</tr>
			</table>
		</div>	
		
		<div id="sem" class="tab_content"  >
			<table border="0" style="margin-left: -5px; margin-top: -5px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_estsem" name="tabla_estsem" style="width: 940px"  >                 			
							<span class="ajaxTable" style="height: 435px; margin-top: 1px; " >
								
                    			<table id="mytablaEstSem" name="mytablaEstSem" class="ajaxSorter" border="1" style="width: 920px" >
                        			<thead >     
                        				<tr style="font-size: 9px">
                        					<th data-order = "pisg" rowspan="3" >Est</th>
                        					<th data-order = "hasgg" rowspan="3" >Has</th>
                        					<th rowspan="2" >Siembra</th>
                        					<th rowspan="2" >Edad</th>
                        					<th rowspan="1" colspan="4" >Biometría</th>
                        					<th rowspan="2" colspan="2" >Sob</th>
                        					<th rowspan="2" colspan="3" >Biomasa</th>
                        					<th colspan="7" rowspan="2"  >Alimento Balanceado</th>
                        					<th rowspan="2" ></th>
                        					<!--<th colspan="2" rowspan="2"  >F.C.A.</th>-->
                        					<!--
                        					<th colspan="6" >Parámetros Físico-Químicos</th>
                        					-->
                        				</tr>                       
                            			<tr style="font-size: 9px">
                            				
                            				<th colspan="4" >Peso</th>
                            				<!--
                            				<th colspan="2" >Oxígeno</th>
                            				<th colspan="2" >Temperatura</th>
                            				<th >Sal</th>
                            				<th >Turb</th>
                            				-->
                            			</tr>
                            			<tr style="font-size: 9px">
                            				<!--
                            				<th data-order = "fecg1">Fecha</th>
                            				<th data-order = "orgs1">Pl´s</th>
                            				-->
                            				<th data-order = "orgsm1">org/m2</th>
                            				<th data-order = "dias">Dias</th>
                            				<th data-order = "pesb">Act</th>
                            				<th data-order = "pesa">Ant</th>
                            				<th data-order = "incp">Inc</th>
                            				<th data-order = "inc3">3 inc</th>
                            				<!--
                            				<th data-order = "prop">Prom</th>
                            				
                            				-->
                            				<!--<th data-order = "sem">Sem</th>-->
                            				<th data-order = "por">%</th>
                            				<!--<th data-order = "orgs">Orgs.</th>-->
                            				<th data-order = "orgsm">org/m2</th>
                            				<!--<th data-order = "pesa">Ant</th>-->
                            				
                            				<th data-order = "bio">Kg/Est</th>
                            				<th data-order = "bioh">Kh/Ha</th>
                            				<th data-order = "bioi">Inc</th>
                            				
                            				<!--<th data-order = "alia">Acu Ant</th>-->
                            				<th data-order = "ah">Kh/Ha</th>
                            				<th data-order = "adiario">Diario</th>
                            				<th data-order = "alis">Sem</th>
                            				<th data-order = "alicha">Acu/ha</th>
                            				<!--<th data-order = "ahd">Kh/Ha/Día</th>-->
                            				<th data-order = "alic">Acu Act</th>
                            				<th data-order = "bw">%BW</th>
                            				<!--<th data-order = "f1">Factor</th>-->
                            				<!--<th data-order = "ach">Acu/Ha</th>-->
                            				
                            				<!--<th data-order = "fcs">Sem</th>-->
                            				<th data-order = "fca">FCA</th>
                            				<th data-order = "isp">ISP</th>
                            				<!--
                            				<th data-order = "omin">Min</th>
                            				<th data-order = "omax">Max</th>
                            				<th data-order = "tmin">Min</th>
                            				<th data-order = "tmax">Max</th>
                            				<th data-order = "sal">Prom</th>
                            				<th data-order = "tur">Prom</th>
                            				-->
                            			</tr>
                            			
                            		</thead>
                        			<tbody style="font-size: 11px; ">
                        			</tbody>                        	
                    			</table>
                    			
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px">
             			 		<!--<form action="<?= base_url()?>index.php/aqpgranjac2/pdfrepestsem" method="POST" >  para el pdf-->
								<form action="<?= base_url()?>index.php/aqpgranjac2/reportesem" method="POST" >
									
									<!-- para excel -->
									<ul class="order_list" style="margin-top: 1px; width: 880px;"> 
										<select name="cmbCicloGra" id="cmbCicloGra" style="font-size: 10px; height: 25px;margin-top: -5px; margin-left:-3px;">
      										<?php $ciclof=21; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>
									<select name="cmbGranjasem" id="cmbGranjasem" style="font-size: 10px; height: 25px;;margin-top: 1px;; margin-left:-4px " >								
    									<option value="4">Ahome</option>
    								</select>
    									
									Sección:
    								<?php if($usuario=="Antonio Valdez" || $usuario=="Supervisor" || $usuario=="Antonio Hernandez"  || $perfil=="Sup. Parametrista"){ ?>	 	
				 					<select name="secc" id="secc" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="41">I</option>		
    									<option value="42">II</option>
    									<option value="43">III</option>
    									<option value="43">IV</option>
    								</select>
    							<?php } ?>
    							<?php
    							$sem=0;
								$this->load->model('aqpgranjac2_model'); 
								$sem=$this->aqpgranjac2_model->verFechat(); 
								?>
								Cierre:
             			 			<select name="idFec" id="idFec" style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<?php	
           						
           						$this->load->model('aqpgranjac2_model'); 
								$data['result']=$this->aqpgranjac2_model->verFecha(); $fec=new Libreria();
								foreach ($data['result'] as $row):    $row->fecb1 = $fec->fecha($row->fecb); ?> 
           							<option value="<?php echo $sem.':'.$row->fecb;?>" ><?php echo 'Sem '.$sem.': '.$row->fecb1;?></option>
           						<?php $sem-=1; endforeach;?>
   						</select>
             			 		Graficas:
             			 			<button type="button" id="tallag" style="width: 50px; cursor: pointer; background-color: lightblue" class="continuar used" >Talla</button>
             			 			<button type="button" id="biomasag" style="width: 60px; cursor: pointer; background-color: lightblue" >Biomasa</button>
             			 			<button type="button" id="oxigenog" style="width: 140px; cursor: pointer; background-color: lightblue"  >Oxígeno vs Biomasa</button>
             			 			<button type="button" id="alimentog" style="width: 60px; cursor: pointer; background-color: lightblue"  >Alimento</button>	
             			 		</ul>						
    	    	            	<!--	<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />-->
        	            	    	<img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablasem" value ="" class="htmlTable"/>          	            	    	        	            	    	
        	            	    </form>   
                			</div>  
                    	</div>
	        		</th>
            	</tr>
			</table>
		</div>
		<?php } ?>
		<?php if($usuario=="Antonio Valdez"   || $usuario=="Julio Lizarraga"){ ?>
		<div id="sema" class="tab_content"  >
			<table border="0" style="margin-left: -5px; margin-top: -5px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_estsema" name="tabla_estsema" style="width: 940px"  >                 			
							<span class="ajaxTable" style="height: 435px; margin-top: 1px; " >
								
                    			<table id="mytablaEstSema" name="mytablaEstSema" class="ajaxSorter" border="1" style="width: 920px" >
                        			<thead >     
                        				<tr style="font-size: 9px">
                        					<th data-order = "pisg" rowspan="3" >Est</th>
                        					<th data-order = "hasgg" rowspan="3" >Has</th>
                        					<th rowspan="2" colspan="2" >Siembra</th>
                        					<th rowspan="2" >Edad</th>
                        					<th rowspan="2" >Den</th>
                        					<th rowspan="2" >Pob</th>
                        					<th rowspan="2" >Sob</th>
                        					<th rowspan="1" colspan="3" >Biometría</th>
                        					
                        					<th rowspan="2" colspan="2" >Biomasa</th>
                        					<th colspan="8" >Alimento Kgs</th>
                        					<th rowspan="2" ></th>
                        					<!--<th colspan="2" rowspan="2"  >F.C.A.</th>-->
                        					<!--
                        					<th colspan="6" >Parámetros Físico-Químicos</th>
                        					-->
                        				</tr>                       
                            			<tr style="font-size: 9px">
                            				
                            				<th colspan="3" >Peso</th>
                            				<!--
                            				<th colspan="2" >Oxígeno</th>
                            				<th colspan="2" >Temperatura</th>
                            				<th >Sal</th>
                            				<th >Turb</th>
                            				-->
                            				<th colspan="3" >Soya</th>
                            				<th colspan="3" >Balanceado</th>
                            				<th >Ambos</th>
                            				<th >kgs/</th>
                            			</tr>
                            			<tr style="font-size: 9px">
                            				<!--
                            				
                            				<th data-order = "orgs1">Pl´s</th>
                            				-->
                            				<th data-order = "orgsm1">org/m2</th>
                            				<th data-order = "fecg1">Fecha</th>
                            				<th data-order = "dias">Dias</th>
                            				
                            				<!--<th data-order = "sem">Sem</th>-->
                            				<th data-order = "orgsm">org/m2</th>
                            				<th data-order = "orgs">Orgs.</th>
                            				<th data-order = "por">%</th>
                            				
                            				
                            				<!--<th data-order = "pesa">Ant</th>-->
                            				
                            				<th data-order = "pesb">Act</th>
                            				<th data-order = "pesa">Ant</th>
                            				<th data-order = "incp">Inc</th>
                            				<!--
                            				<th data-order = "inc3">3 inc</th>
                            				
                            				<th data-order = "prop">Prom</th>
                            				
                            				-->
                            				
                            				<th data-order = "bioh">Kh/Ha</th>
                            				<th data-order = "bio">Kg/Est</th>
                            				
                            				<!--<th data-order = "bioi">Inc</th>
                            				
                            				<th data-order = "alia">Acu Ant</th>
                            				<th data-order = "ah">Kh/Ha</th>
                            				<th data-order = "adiario">Diario</th>
                            				<th data-order = "alis">Sem</th>
                            				<th data-order = "alicha">Acu/ha</th>
                            				<th data-order = "ahd">Kh/Ha/Día</th>
                            				<th data-order = "alic">Acu Act</th>
                            				<th data-order = "bw">%BW</th>
                            				<!--<th data-order = "f1">Factor</th>-->
                            				<!--<th data-order = "ach">Acu/Ha</th>-->
                            				
                            				<!--<th data-order = "fcs">Sem</th>-->
                            				
                            				<th data-order = "semsoy">Sem</th>
                       						<th data-order = "acusoy">AcuAnt</th>
                       						<th data-order = "totsoy">Total</th>
                       						<th data-order = "sembal">Sem</th>
                       						<th data-order = "acubal">AcuAnt</th>
                       						<th data-order = "totbal">Total</th>
                            				<th data-order = "soybal">Acu</th>
                            				<th data-order = "kgshas">Ha</th>
                            				
                            				<th data-order = "fca">FCA</th>
                            				<!--
                            				<th data-order = "isp">ISP</th>
                            				
                            				<th data-order = "omin">Min</th>
                            				<th data-order = "omax">Max</th>
                            				<th data-order = "tmin">Min</th>
                            				<th data-order = "tmax">Max</th>
                            				<th data-order = "sal">Prom</th>
                            				<th data-order = "tur">Prom</th>
                            				-->
                            			</tr>
                            			
                            		</thead>
                        			<tbody style="font-size: 11px; text-align: center">
                        			</tbody>                        	
                    			</table>
                    			
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px">
             			 		<!--<form action="<?= base_url()?>index.php/aqpgranjac2/pdfrepestsem" method="POST" >  para el pdf-->
								<form action="<?= base_url()?>index.php/aqpgranjac2/reportesem" method="POST" >
									
									<!-- para excel -->
									<ul class="order_list" style="margin-top: 1px; width: 880px;"> 
										<select name="cmbCicloGra" id="cmbCicloGra" style="font-size: 10px; height: 25px;margin-top: -5px; margin-left:-3px;">
      										<?php $ciclof=21; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>
									<select name="cmbGranjasem" id="cmbGranjasem" style="font-size: 10px; height: 25px;;margin-top: 1px;; margin-left:-4px " >								
    									<option value="4">Ahome</option>
    								</select>
    									
									Sección:
    								<?php if($usuario=="Antonio Valdez" || $usuario=="Supervisor" || $usuario=="Antonio Hernandez"  || $perfil=="Sup. Parametrista"){ ?>	 	
				 					<select name="secc" id="secc" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="41">I</option>		
    									<option value="42">II</option>
    									<option value="43">III</option>
    									<option value="43">IV</option>
    								</select>
    							<?php } ?>
    							<?php
    							$sem=0;
								$this->load->model('aqpgranjac2_model'); 
								$sem=$this->aqpgranjac2_model->verFechat(); 
								?>
								Cierre:
             			 			<select name="idFec" id="idFec" style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<?php	
           						
           						$this->load->model('aqpgranjac2_model'); 
								$data['result']=$this->aqpgranjac2_model->verFecha(); $fec=new Libreria();
								foreach ($data['result'] as $row):    $row->fecb1 = $fec->fecha($row->fecb); ?> 
           							<option value="<?php echo $sem.':'.$row->fecb;?>" ><?php echo 'Sem '.$sem.': '.$row->fecb1;?></option>
           						<?php $sem-=1; endforeach;?>
   						</select>
             			 		Graficas:
             			 			<button type="button" id="tallag" style="width: 50px; cursor: pointer; background-color: lightblue" class="continuar used" >Talla</button>
             			 			<button type="button" id="biomasag" style="width: 60px; cursor: pointer; background-color: lightblue" >Biomasa</button>
             			 			<button type="button" id="oxigenog" style="width: 140px; cursor: pointer; background-color: lightblue"  >Oxígeno vs Biomasa</button>
             			 			<button type="button" id="alimentog" style="width: 60px; cursor: pointer; background-color: lightblue"  >Alimento</button>	
             			 		</ul>						
    	    	            	<!--	<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />-->
        	            	    	<img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tablasem" value ="" class="htmlTable"/>          	            	    	        	            	    	
        	            	    </form>   
                			</div>  
                    	</div>
	        		</th>
            	</tr>
			</table>
		</div>
		<?php } ?>
		<div id="indisem" class="tab_content"  >
			<table border="0" style="margin-left: -5px; margin-top: -5px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_estsemi" name="tabla_estsemi" style="width: 940px"  >                 			
							<span class="ajaxTable" style="height: 435px; margin-top: 1px; " >
								
                    			<table id="mytablaEstSemi" name="mytablaEstSemi" class="ajaxSorter" border="1" style="width: 920px" >
                        			<thead >     
                        				<tr style="font-size: 9px">
                        					<th data-order = "pisg" rowspan="3" >Est</th>
                        					<th data-order = "hasgg" rowspan="3" >Has</th>
                        					<th rowspan="2" >Siembra</th>
                        					<th rowspan="2" >Edad</th>
                        					<th rowspan="1" colspan="12" >Biometría</th>
                        					<th rowspan="1" colspan="8" >Sobrevivencia</th>
                        					<th rowspan="1" colspan="4" >Alimento</th>
                        				</tr>                       
                            			<tr style="font-size: 9px">
                            				<th colspan="4" >Peso</th>
                            				<th colspan="4" >Inc</th>
                            				<th colspan="4" >3 Inc</th>
                            				<th colspan="4" >%</th>
                            				<th colspan="4" >org/m2</th>
                            				<th colspan="4" >FCA</th>
                            			</tr>
                            			<tr style="font-size: 9px">
                            				<th data-order = "orgsm1">org/m2</th>
                            				<th data-order = "dias">Dias</th>
                            				<th data-order = "pesb">Act</th>
                            				<th data-order = "pesbp">Proy</th>
                            				<th data-order = "ipesb">Inc</th>
                            				<th data-order = "ppesb">%</th>
                            				<th data-order = "incp">Act</th>
                            				<th data-order = "incpp">Proy</th>
                            				<th data-order = "iincp">Inc</th>
                            				<th data-order = "pincp">%</th>
                            				<th data-order = "inc3">Act</th>
                            				<th data-order = "inc3p">Proy</th>
                            				<th data-order = "iinc3">Inc</th>
                            				<th data-order = "pinc3">%</th>
                            				<th data-order = "por">Act</th>
                            				<th data-order = "porp">Proy</th>
                            				<th data-order = "iporp">Inc</th>
                            				<th data-order = "pporp">%</th>                            		
                            				<th data-order = "orgsm">Act</th>
                            				<th data-order = "orgsmp">Proy</th>
                            				<th data-order = "iorgsm">Inc</th>
                            				<th data-order = "porgsm">%</th> 
                            				<th data-order = "fca">Act</th>
                            				<th data-order = "fcap">Proy</th>
                            				<th data-order = "ifcap">Inc</th>
                            				<th data-order = "pfcap">%</th>
                            			</tr>
                            		</thead>
                        			<tbody style="font-size: 11px; ">
                        			</tbody>                        	
                    			</table>                    			
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px">
             			 		<!--<form action="<?= base_url()?>index.php/aqpgranjac2/pdfrepestsem" method="POST" >  para el pdf-->
								<form action="<?= base_url()?>index.php/aqpgranjac2/reportesemi" method="POST" >
									<!-- para excel -->
									<ul class="order_list" style="margin-top: 1px; width: 880px;"> 
									<select name="cmbCicloi" id="cmbCicloi" style="font-size: 10px; height: 25px;margin-top: -5px; margin-left:-3px;">
      										<?php $ciclof=20; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>
									<select name="cmbGranjasemi" id="cmbGranjasemi" style="font-size: 10px; height: 25px;;margin-top: 1px; " >			<option value="4">Ahome</option>
    							</select>
    							Sección:
    								<?php if($usuario=="Antonio Valdez" || $usuario=="Supervisor" || $usuario=="Antonio Hernandez"  || $perfil=="Sup. Parametrista"){ ?>	 	
				 					<select name="secci" id="secci" style="font-size: 12px; height: 25px;margin-top: 1px;  " >								
    									<option value="0">Sel.</option>
    									<option value="41">20-30</option>		
    									<option value="42">21-25</option>
    									<option value="43">Doble Ciclo</option>
    								</select>
    							<?php } ?>
    							<?php
    							$sem=0;
								$this->load->model('aqpgranjac2_model'); 
								$sem=$this->aqpgranjac2_model->verFechat(); 
								?>
								Cierre:
             		<select name="idFeci" id="idFeci" style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
          				<?php	
           					$this->load->model('aqpgranjac2_model'); 
										$data['result']=$this->aqpgranjac2_model->verFecha(); $fec=new Libreria();
										foreach ($data['result'] as $row):    $row->fecb1 = $fec->fecha($row->fecb); ?> 
           						<option value="<?php echo $sem.':'.$row->fecb;?>" ><?php echo 'Sem '.$sem.': '.$row->fecb1;?></option>
           					<?php $sem-=1; endforeach;?>
   							</select>
             		</ul>						
    	    	    <img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	      <input type="hidden" name="tablasemi" value ="" class="htmlTable"/>          	            	    	           </form>   
                </div>  
                </div>
	        		</th>
            	</tr>
			</table>
		</div>
		<div id="est" class="tab_content"  >
			<div id='ver_informacion' class='hide' style="height: 90px" >
			<table border="0" style="margin-top: -5px">
				<tr style="background-color:lightyellow">
					<th colspan="3" >CONCENTRADO DE INFORMACIÓN BIOLÓGICA</th>
				</tr>
				<tr style="background-color:lightgray">
					<th>Granja</th>
					<th>Ciclo</th>
					<th>Estanque</th>
					<th rowspan="2">
						<div class="ajaxSorterDiv" id="tabla_inf" name="tabla_inf" style="width: 657px; margin-top: -5px" >                                		
            	    		<span class="ajaxTable" style="height: 47px; width: 655px " >
                		    	<table id="mytablaEst" name="mytablaEst" class="ajaxSorter" border="1">
                        			<thead title="Presione las columnas para ordenarlas como requiera"> 
                        				<th data-order = "hasg" >Has</th>
                            			<th data-order = "espg">Especie</th> 
                            			<th data-order = "orgg">Organismos</th>  
                            			<th data-order = "den" >Densidad</th>                            
                            			<th data-order = "prog" >Procedencia</th>
                            			<th data-order = "fecg1" >Fecha</th>
                            			<th data-order = "plg" >Estadío</th>
                            		</thead>
                        			<tbody style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
               			</div>
					</th>
				</tr>
				<tr >
					<th>
						<select name="cmbGranjae" id="cmbGranjae" style="font-size: 10px; height: 25px; margin-top: -5px; " >						<option value="4">Ahome</option>
    				</select>
					</th>
					<th>
						<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez"){ ?>	
    								<select name="cmbCicloEst" id="cmbCicloEst" style="font-size: 10px; height: 25px;margin-top: -5px; margin-left:4px;">
      										<?php $ciclof=15; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>
    							<?php } else{ ?>
    								<select name="cmbCicloEst" id="cmbCicloEst" style="font-size: 10px; height: 25px;margin-top: -5px; margin-left:4px;">
      										<?php $ciclof=20; $actual=date("y"); //$actual+=1;
											while($actual >= $ciclof){?>
												<option value="<?php echo '20'.$actual.'-2';?>" > <?php echo '20'.$actual.'-2';?> </option>
												<option value="<?php echo '20'.$actual.'-1';?>" > <?php echo '20'.$actual.'-1';?> </option>
            								<?php  $actual-=1; } ?>      									
    								  </select>	
    							<?php } ?>	 
					</th>
					<th>
						<select name='idEst' id="idEst" style="margin-top: -5px;  "  ></select>						
					</th>
				</tr>
			</table>
			<table border="0" style="margin-left: -5px; margin-top: -30px; ">
				<tr>
				 	<th >				 		
				 		<!--Alimento -->
            			<input type="hidden" id="a1"/><input type="hidden" id="a2"/><input type="hidden" id="a3"/><input type="hidden" id="a4"/><input type="hidden" id="a5"/>
						<input type="hidden" id="a6"/><input type="hidden" id="a7"/><input type="hidden" id="a8"/><input type="hidden" id="a9"/><input type="hidden" id="a10"/>
						<input type="hidden" id="a11"/><input type="hidden" id="a12"/><input type="hidden" id="a13"/><input type="hidden" id="a14"/>
            			<!--Biomasa -->
            			<input type="hidden" id="b1"/><input type="hidden" id="b2"/><input type="hidden" id="b3"/><input type="hidden" id="b4"/><input type="hidden" id="b5"/>
						<input type="hidden" id="b6"/><input type="hidden" id="b7"/><input type="hidden" id="b8"/><input type="hidden" id="b9"/><input type="hidden" id="b10"/>
						<input type="hidden" id="b11"/><input type="hidden" id="b12"/><input type="hidden" id="b13"/><input type="hidden" id="b14"/>
						<!--Talla -->
            			<input type="hidden" id="t1"/><input type="hidden" id="t2"/><input type="hidden" id="t3"/><input type="hidden" id="t4"/><input type="hidden" id="t5"/>
						<input type="hidden" id="t6"/><input type="hidden" id="t7"/><input type="hidden" id="t8"/><input type="hidden" id="t9"/><input type="hidden" id="t10"/>
						<input type="hidden" id="t11"/><input type="hidden" id="t12"/><input type="hidden" id="t13"/><input type="hidden" id="t14"/>
						<!--Salinidad -->
            			<input type="hidden" id="s1"/><input type="hidden" id="s2"/><input type="hidden" id="s3"/><input type="hidden" id="s4"/><input type="hidden" id="s5"/>
						<input type="hidden" id="s6"/><input type="hidden" id="s7"/><input type="hidden" id="s8"/><input type="hidden" id="s9"/><input type="hidden" id="s10"/>
						<input type="hidden" id="s11"/><input type="hidden" id="s12"/><input type="hidden" id="s13"/><input type="hidden" id="s14"/>
						<!--Temperatura Min -->
            			<input type="hidden" id="tm1"/><input type="hidden" id="tm2"/><input type="hidden" id="tm3"/><input type="hidden" id="tm4"/><input type="hidden" id="tm5"/>
						<input type="hidden" id="tm6"/><input type="hidden" id="tm7"/><input type="hidden" id="tm8"/><input type="hidden" id="tm9"/><input type="hidden" id="tm10"/>
						<input type="hidden" id="tm11"/><input type="hidden" id="tm12"/><input type="hidden" id="tm13"/><input type="hidden" id="tm14"/>
						<!--Temperatura Max -->
						<input type="hidden" id="tx1"/><input type="hidden" id="tx2"/><input type="hidden" id="tx3"/><input type="hidden" id="tx4"/><input type="hidden" id="tx5"/>
						<input type="hidden" id="tx6"/><input type="hidden" id="tx7"/><input type="hidden" id="tx8"/><input type="hidden" id="tx9"/><input type="hidden" id="tx10"/>
						<input type="hidden" id="tx11"/><input type="hidden" id="tx12"/><input type="hidden" id="tx13"/><input type="hidden" id="tx14"/>
						<!--Oxigeno Min -->
            			<input type="hidden" id="om1"/><input type="hidden" id="om2"/><input type="hidden" id="om3"/><input type="hidden" id="om4"/><input type="hidden" id="om5"/>
						<input type="hidden" id="om6"/><input type="hidden" id="om7"/><input type="hidden" id="om8"/><input type="hidden" id="om9"/><input type="hidden" id="om10"/>
						<input type="hidden" id="om11"/><input type="hidden" id="om12"/><input type="hidden" id="om13"/><input type="hidden" id="om14"/>
						<!--Oxigeno Max -->
						<input type="hidden" id="ox1"/><input type="hidden" id="ox2"/><input type="hidden" id="ox3"/><input type="hidden" id="ox4"/><input type="hidden" id="ox5"/>
						<input type="hidden" id="ox6"/><input type="hidden" id="ox7"/><input type="hidden" id="ox8"/><input type="hidden" id="ox9"/><input type="hidden" id="ox10"/>
						<input type="hidden" id="ox11"/><input type="hidden" id="ox12"/><input type="hidden" id="ox13"/><input type="hidden" id="ox14"/>
            			<!--Dias de Cultivo -->
            			<input type="hidden" id="d1"/><input type="hidden" id="d2"/><input type="hidden" id="d3"/><input type="hidden" id="d4"/><input type="hidden" id="d5"/>
						<input type="hidden" id="d6"/><input type="hidden" id="d7"/><input type="hidden" id="d8"/><input type="hidden" id="d9"/><input type="hidden" id="d10"/>
						<input type="hidden" id="d11"/><input type="hidden" id="d12"/><input type="hidden" id="d13"/><input type="hidden" id="d14"/>
						<!--Fecha -->
            			<input type="hidden" id="fe1"/><input type="hidden" id="f2"/><input type="hidden" id="f3"/><input type="hidden" id="f4"/><input type="hidden" id="f5"/>
						<input type="hidden" id="f6"/><input type="hidden" id="f7"/><input type="hidden" id="f8"/><input type="hidden" id="f9"/><input type="hidden" id="f10"/>
						<input type="hidden" id="f11"/><input type="hidden" id="f12"/><input type="hidden" id="f13"/><input type="hidden" id="f14"/>
            <div class="ajaxSorterDiv" id="tabla_estdat" name="tabla_estdat" style="width: 890px"  >                 			
						<span class="ajaxTable" style="height: 370px; margin-top: 1px; " >
            	<table id="mytablaEstDat" name="mytablaEstDat" class="ajaxSorter" border="1" style="width: 870px" >
            		<thead>     
            			<tr style="font-size: 9px">
            				<th data-order = "fecb2" rowspan="3" >Fecha <br /> Muestreo <br />Crecimiento</th>
                    <th rowspan="2" colspan="2" >Edad</th>
                    <th rowspan="2" colspan="3" >Peso</th>
                    <th rowspan="2" colspan="3" >Sobrevivencia</th>
                    <th rowspan="2" colspan="2" >Biomasa</th>
                    <th colspan="5" rowspan="2"  >Alimento Balanceado</th>
                    <th colspan="2" rowspan="2"  >F.C.A.</th>
                    <th colspan="6" >Parámetros Físico-Químicos</th>
                  </tr>                       
                  <tr style="font-size: 9px">
                  	<th colspan="2" >Oxígeno</th>
                    <th colspan="2" >Temperatura</th>
                    <th >Sal</th>
                    <th >Turb</th>
                  </tr>
                  <tr style="font-size: 9px">
                   <th data-order = "dias">Dias</th>
                   <th data-order = "sem">Sem</th>
                   <th data-order = "pesb">Actual</th>
                   <th data-order = "incp">Inc</th>
                   <th data-order = "prop">Prom</th>
                   <th data-order = "por">%</th>
                   <th data-order = "orgs">Orgs.</th>
                   <th data-order = "orgsm">org/m2</th>
                   <th data-order = "bio">Kg/Est</th>
                   <th data-order = "bioh">Kh/Ha</th>
                   <th data-order = "ahd">Kh/Ha/Día</th>
                   <th data-order = "f1">Factor</th>
                   <th data-order = "alis">Sem</th>	
                   <th data-order = "ach">Acu/Ha</th>
                   <th data-order = "alic">Acu</th>
                   <th data-order = "fcs">Sem</th>
                   <th data-order = "fca">Acu</th>
                   <th data-order = "omin">Min</th>
                   <th data-order = "omax">Max</th>
                   <th data-order = "tmin">Min</th>
                   <th data-order = "tmax">Max</th>
                   <th data-order = "sal">Prom</th>
                   <th data-order = "tur">Prom</th>
                  </tr>
                </thead>
                <tbody style="font-size: 9px">
                </tbody>                        	
              </table>
              </span> 
             	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px">
             		<ul class="order_list" style="margin-top: 1px; width: 690px;">
             			Gráficas: 
             			<button type="button" id="biomasax" style="width: 130px; cursor: pointer; background-color: lightblue" class="continuar used" >Alimento Vs Biomasa</button>
             			<button type="button" id="temperaturax" style="width: 180px; cursor: pointer; background-color: lightblue" >Temperatura Vs Crecimientos</button>
             			<button type="button" id="oxigenox" style="width: 155px; cursor: pointer; background-color: lightblue"  >Oxígeno Vs Crecimientos</button>
             			<button type="button" id="salinidadx" style="width: 160px; cursor: pointer; background-color: lightblue"  >Salinidad Vs Crecimientos</button>
             		</ul> 
							<form id="data_tablestdet" action="<?= base_url()?>index.php/aqpgranjac2/pdfrepestdet" method="POST" >					   <img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" id="export_data" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	       <input type="hidden" name="tablaestdet" id="html_estdet"/>
        	       <input type="hidden" name="tablaestdat" id="html_estdat"/>
        	       <input type="hidden" name="cicloed" id="cicloed" ><input type="hidden" name="ested" id="ested" >
							</form>   
            </div>  
            <script type="text/javascript">
							$(document).ready(function(){
								$('#export_data').click(function(){									
									$('#html_estdet').val($('#mytablaEstDat').html());
									$('#html_estdat').val($('#mytablaEst').html());
									$('#data_tablestdet').submit();
									$('#html_estdet').val('');									
									$('#html_estdat').val('');
								});
							});
						</script>     	                	
	        </div>
	      </th>
      </tr>
      </table>
			</div>
			<div id='ver_biomasa' class='hide' style="height: 90px" >
            	<div name="biomasa" id="biomasa" style="min-width: 900px; height: 430px; margin: 0 auto"></div>
            	<button type="button" id="biomasax1" style="cursor: pointer; background-color: red" class="continuar used" >Cerrar</button>
            </div>
            <div id='ver_temperatura' class='hide' style="height: 90px" >
            	<div name="temperatura" id="temperatura" style="min-width: 900px; height: 430px; margin: 0 auto"></div>
            	<button type="button" id="temperaturax1" style="cursor: pointer; background-color: red" class="continuar used" >Cerrar</button>
            </div>
            <div id='ver_oxigeno' class='hide' style="height: 90px" >
            	<div name="oxigeno" id="oxigeno" style="min-width: 900px; height: 430px; margin: 0 auto"></div>            	
            	<button type="button" id="oxigeno1" style="cursor: pointer; background-color: red" class="continuar used" >Cerrar</button>
            </div>
            <div id='ver_salinidad' class='hide' style="height: 90px" >
            	<div name="salinidad" id="salinidad" style="min-width: 900px; height: 430px; margin: 0 auto"></div>
            	<button type="button" id="salinidad1" style="cursor: pointer; background-color: red" class="continuar used" >Cerrar</button>
            </div>
        </div>
		<div id="pro" class="tab_content"  >
			<table border="0" style="margin-left: -5px; margin-top: -5px  ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_prod" name="tabla_prod" style="width: 890px"  >                 			
							<span class="ajaxTable" style="height: 435px; margin-top: -1px; " >
                    			<table id="mytablaProd" name="mytablaProd" class="ajaxSorter" border="1" style="width: 870px" >
                        			<thead>     
                        				<tr style="font-size: 9px">
                        					<th colspan="17" style="font-size: 12px">REPORTE GENERAL DE PRODCUCCIÓN 
                        					 <input id='granjasel' name="granjasel" style="border: none;background: none;" />
                        					
                        					</th>
                        				</tr>                       
                            			<tr style="font-size: 9px">
                            				<th data-order = "pisg" rowspan="2" >Est</th>
                            				<th data-order = "hasg" rowspan="2" >Has</th>
                            				<th data-order = "prog" rowspan="2" >Procedencia</th>
                            				<th data-order = "fecg1" rowspan="2" >Fecha<br />Siembra</th>
                            				<th colspan="2" >Organismos Sembrados</th>
                            				<th style="border-bottom: none" >Días de</th>
                            				<th colspan="2" >Biomasa</th>
                            				<th style="border-bottom: none">Peso</th>
                            				<th colspan="3" >Sobrevivencia</th>
                            				<th style="border-bottom: none" >Efi</th>
                            				<th colspan="3" >Alimento</th>
                            			</tr>
                            			<tr style="font-size: 9px">
                            				<th data-order = "orgg">Total</th>
                            				<th data-order = "den">Orgs/m2</th>
                            				<th style="border-top: none" data-order = "diasc" >Cultivo</th>
                            				<th data-order = "biogc">Est</th>
                            				<th data-order = "habio">Kh/Ha</th>
                            				<th data-order = "ppgc" style="border-top: none" >Prom</th>
                            				<th data-order = "orgsc">Orgs.</th>
                            				<th data-order = "orgsmc">org/m2</th>
                            				<th data-order = "sob">%</th>
                            				<th data-order = "efi" style="border-top: none">Est</th>
                            				<th data-order = "aliprod">Total</th>
                            				<th data-order = "alihas">Kh/Ha</th>
                            				<th data-order = "alifca">F.C.A.</th>
                            			</tr>
                            			
                            		</thead>
                        			<tbody style="font-size: 9px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px">
             			 		
								<form action="<?= base_url()?>index.php/aqpgranjac2/reporte" method="POST" >
									<ul class="order_list" style="width: 310px;margin-top: 1px">
									Granja
									
									
             						<select name="cmbGranjaprod" id="cmbGranjaprod" style="font-size: 10px; height: 25px;margin-top: 1px  " >								
    									<option value="4">Ahome</option>
    									
    								</select>
    							
    									 
									Ciclo de Producción: <input style="border: none; background: none;" size="6%"  readonly="true" type="text" name="cicloprod" id="cicloprod">	
									</ul> 							
    	    	            		
    	    	            		<!--<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />-->
        	            	    	<img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
        	            	    	<input type="hidden" name="tabla" value ="" class="htmlTable"/>
        	            	    	 
								</form> 
                			</div>  
                				                	
	                	</div>
	                	
            		</th>
            	</tr>
			</table>
		</div>
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#cmbCiclofq").change( function(){
	$("#filest").val($("#cmbCiclofq").val()+$("#seccp").val()+$("#cmbGranjafq").val());
	$("#idpisfq").trigger("update");
});	
$("#seccp").change( function(){
	$("#filest").val($("#cmbCiclofq").val()+$("#seccp").val()+$("#cmbGranjafq").val());
	$("#idpisfq").trigger("update");
});	
$("#pisg").change( function(){	
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/aqpgranjac2/buscarestsie", 
			data: "pil="+$("#pisg").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#hasg").val(obj.hasest);$("#secc").val(obj.secest);					
   			} 
	});	
  $("#orgg").focus();
	return true;
});
function radios(radio,dato){
	switch(radio){
		case 1: $("#salud1").val(dato); break;
	}
}

$("#borrars").click( function(){	
	numero=$("#idsob").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/borrar", 
						data: "id="+$("#idsob").val()+"&tabla=sobgra"+"&campo=idsob"+"&cic="+$("#cmbCiclos").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaSob").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update")
										$("#nuevos").click();	$("#idpiss").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Sobreviviencia para poder Eliminarla");
		return false;
	}
});
$("#borrarb").click( function(){	
	numero=$("#idbio").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/borrar", 
						data: "id="+$("#idbio").val()+"&tabla=biogra"+"&campo=idbio"+"&cic="+$("#cmbCiclob").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaBio").trigger("update");
										$("#mytablaEstSem").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update")
										$("#nuevob").click();																																				
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Biometria para poder Eliminarla");
		return false;
	}
});

$("#borrara").click( function(){	
	numero=$("#idali").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/borrar", 
						data: "id="+$("#idali").val()+"&tabla=aligra"+"&campo=idali",
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#mytablaAli").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update")
										//$("#nuevo2").click();																																				
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Alimentación para poder Eliminarla");
		return false;
	}
});
$('#alimentod').click(function(){$(this).removeClass('used');$('#ver_ali').hide();$('#ver_aligra').show();});
$('#alid').click(function(){$(this).removeClass('used');$('#ver_ali').show();$('#ver_aligra').hide();});
$('#biomasax').click(function(){$(this).removeClass('used');$('#ver_informacion').hide();$('#ver_biomasa').show();});
$('#biomasax1').click(function(){$(this).removeClass('used');$('#ver_biomasa').hide();$('#ver_informacion').show();});
$('#temperaturax').click(function(){$(this).removeClass('used');$('#ver_informacion').hide();$('#ver_temperatura').show();});
$('#temperaturax1').click(function(){$(this).removeClass('used');$('#ver_temperatura').hide();$('#ver_informacion').show();});
$('#oxigenox').click(function(){$(this).removeClass('used');$('#ver_informacion').hide();$('#ver_oxigeno').show();});
$('#oxigeno1').click(function(){$(this).removeClass('used');$('#ver_oxigeno').hide();$('#ver_informacion').show();});
$('#salinidadx').click(function(){$(this).removeClass('used');$('#ver_informacion').hide();$('#ver_salinidad').show();});
$('#salinidad1').click(function(){$(this).removeClass('used');$('#ver_salinidad').hide();$('#ver_informacion').show();});

$("#cmbCicloEst").change( function(){	
	$("#cicloed").val($("#cmbCicloEst").val());
	return true;
});
$("#cmbGranja").change( function(){	
	$("#granja").val($("#cmbGranja").val());
	return true;
});
$("#cmbGranjae").change( function(){	
	$("#mytablaEst").trigger("update");
	$("#mytablaEstDat").trigger("update");
	//return true;
});
$("#cmbGranjaprod").change( function(){	
	if($("#cmbGranjaprod").val()==2){
	$("#granjasel").val('Granja Gran kino');}
	else{
	$("#granjasel").val('Granja Jazmin');}
	return true;
});
$("#cmbCiclop").change( function(){	
	$("#cicloprod").val($("#cmbCiclop").val());
	return true;
});
$("#idEst").change( function(){	
	$("#ested").val($("#idEst").val());
	return true;
});
$("#nuevo").click( function(){	
	$("#idpis").val('');$("#fecg").val('');$("#pisg").val(0);$("#hasg").val('');$("#espg").val('Litopenaeus vannamei');
	$("#orgg").val('');$("#prog").val('Aquapacfic S.A. de C.V.');$("#plg").val('');$("#secc").val(0);$("#lote").val('');
	$("#cica").val('');$("#pisg").focus();
	("#fecgc").val('');("#biogc").val('');("#ppgc").val('');("#obsc").val('');
	return true;
});
$("#nuevob").click( function(){	
	$("#idbio").val('');$("#idpisb").val('');$("#pesb").val('');$("#obsb").val('');$("#incest").val('');
	$("#idpisb").focus();
	return true;
});
$("#nuevos").click( function(){	
	$("#idsob").val('');//$("#idpiss").val('');
	$("#sobp").val('');$("#obss").val('');$("#camm2").val('');
	$("#idsob").focus();
	return true;
});
$("#nuevoa").click( function(){	
	$("#idali").val('');$("#idpisali").val('');$("#canali").val('');$("#obsa").val('');
	$("#idpisali").focus();
	return true;
});
$("#nuevofq").click( function(){	
	$("#idpfq").val('');// $("#idpisfq").val(''); //$("#t1fq").val('');$("#t2fq").val('');
	$("#o1fq").val('');$("#o2fq").val(''); //$("#reca").val('');
	
	$("#salfq").val('');$("#turfq").val('');$("#phfq").val('');$("#salud1").val('1');
	$('input:radio[name=salud2]:nth(0)').attr('checked',true);$('input:radio[name=salud2]:nth(1)').attr('checked',false);$('input:radio[name=salud2]:nth(2)').attr('checked',false);$('input:radio[name=salud2]:nth(3)').attr('checked',false);
	$("#idpisfq").focus();
	return true;
});
$("#idpisb").change( function(){	
	$("#pesb").focus();
	return true;
});
$("#idpisb2").change( function(){	
	$("#pesb").focus();
	return true;
});
/*
$("#idpiss").change( function(){	
	$("#camm2").focus();
	return true;
});
*/
$("#idpiss").change( function(){	
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/aqpgranjac2/buscarest", 
			data: "pil="+$("#idpiss").val(),
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
					$("#hasest").val(obj.hasg);$("#orgest").val(obj.orgg);					
   			} 
	});	
  //$("#sobp").focus();
	$("#camm2").focus();
	return true;
});
$("#idpisali").change( function(){	
	$("#canali").focus();
	return true;
});
$("#idpisfq").change( function(){	
	$("#o2fq ").focus();
	return true;
});
$("#fecg").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
});	
$("#fecgc").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
});
$("#fecb").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
});	
$("#fecs").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
});	
$("#feca").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
});

$("#txtFI").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFF").datepicker( "option", "minDate", selectedDate );
		$("#mytablaBio").trigger("update");		
	}	
});
$("#txtFF").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFI").datepicker( "option", "maxDate", selectedDate );
		$("#mytablaBio").trigger("update");	
	}	
});	
$("#txtFIS").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFFS").datepicker( "option", "minDate", selectedDate );
		$("#mytablaSob").trigger("update");		
	}	
});
$("#txtFFS").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFIS").datepicker( "option", "maxDate", selectedDate );
		$("#mytablaSob").trigger("update");	
	}	
});
$("#txtFIAli").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFFAli").datepicker( "option", "minDate", selectedDate );
		$("#mytablaAli").trigger("update");		
	}	
});
$("#txtFFAli").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFIAli").datepicker( "option", "maxDate", selectedDate );
		$("#mytablaAli").trigger("update");	
	}	
});
$("#fecfq").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
});	
$("#txtFIfq").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFFfq").datepicker( "option", "minDate", selectedDate );
		$("#mytablaPar").trigger("update");		
	}	
});
$("#txtFFfq").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFIfq").datepicker( "option", "maxDate", selectedDate );
		$("#mytablaPar").trigger("update");	
	}	
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  
function cerrar(){
		$("#accordion").accordion( "activate",0 );
	
}

$("#aceptara").click( function(){		
	fec=$("#feca").val();can=$("#canali").val();
	numero=$("#idali").val();
	if( fec!=''){
	  if( can!=''){
	  	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/actualizarali", 
						data: "id="+$("#idali").val()+"&pis="+$("#idpisali").val()+"&fec="+$("#feca").val()+"&can="+$("#canali").val()+"&cic="+$("#cmbCicloa").val()+"&obs="+$("#obsa").val()+"&gra="+$("#cmbGranjaa").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevoa").click();
										$("#mytablaAli").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/agregarali", 
						data: "pis="+$("#idpisali").val()+"&fec="+$("#feca").val()+"&can="+$("#canali").val()+"&cic="+$("#cmbCicloa").val()+"&obs="+$("#obsa").val()+"&gra="+$("#cmbGranjaa").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevoa").click();
										$("#mytablaAli").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		
		}else{
			alert("Error: Registre Cantidad de Alimento");	
			$("#canali").focus();
			return false;
		}					
	}else{
		alert("Error: Seleccione Fecha");	
		$("#feca").focus();
		return false;
	}
});

$("#aceptarb").click( function(){	
	$("#aceptarb").attr("disabled","disabled");	
	fec=$("#fecb").val();pes=$("#pesb").val();org=$("#orgg").val();
	numero=$("#idbio").val();
	if( fec!=''){
	  if( pes!=''){
	  	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/actualizarbio", 
						data: "id="+$("#idbio").val()+"&pis="+$("#idpisb2").val()+"&fec="+$("#fecb").val()+"&pes="+$("#pesb").val()+"&cic="+$("#cmbCiclob").val()+"&obs="+$("#obsb").val()+"&gra="+$("#cmbGranjab").val()+"&inc="+$("#incest").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevob").click();
										$("#mytablaBio").trigger("update");
										$("#mytablaEstSem").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
										$("#idpisb2").focus();
										$("#aceptarb").removeAttr("disabled");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/agregarbio", 
						data: "pis="+$("#idpisb2").val()+"&fec="+$("#fecb").val()+"&pes="+$("#pesb").val()+"&cic="+$("#cmbCiclob").val()+"&obs="+$("#obsb").val()+"&gra="+$("#cmbGranjab").val()+"&inc="+$("#incest").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevob").click();
										$("#mytablaBio").trigger("update");
										$("#mytablaEstSem").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
										$("#idpisb2").focus();
										$("#aceptarb").removeAttr("disabled");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		
		}else{
			alert("Error: Registre Peso Promedio de Biometría");	
			$("#pesb").focus();$("#aceptarb").removeAttr("disabled");
			return false;
		}					
	}else{
		alert("Error: Seleccione Fecha");	
		$("#fecb").focus();$("#aceptarb").removeAttr("disabled");
		return false;
	}
});

$("#aceptars").click( function(){	
	$("#aceptars").attr("disabled","disabled");	
	fec=$("#fecs").val();sob=$("#sobp").val();org=$("#orgg").val();
	numero=$("#idsob").val();
	if( fec!=''){
	  //if( sob!=''){
	  	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/actualizarsob", 
						data: "id="+$("#idsob").val()+"&pis="+$("#idpiss").val()+"&fec="+$("#fecs").val()+"&sob="+$("#sobp").val()+"&cic="+$("#cmbCiclos").val()+"&obs="+$("#obss").val()+"&gra="+$("#cmbGranjass").val()+"&cam="+$("#camm2").val()+"&has="+$("#hasest").val()+"&org="+$("#orgest").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										//alert("Datos actualizados correctamente");
										$("#nuevos").click();
										$("#mytablaSob").trigger("update");
										$("#mytablaEstSem").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
										$("#idpiss").focus();$("#aceptars").removeAttr("disabled");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/agregarsob", 
						data: "pis="+$("#idpiss").val()+"&fec="+$("#fecs").val()+"&sob="+$("#sobp").val()+"&cic="+$("#cmbCiclos").val()+"&obs="+$("#obss").val()+"&gra="+$("#cmbGranjass").val()+"&cam="+$("#camm2").val()+"&has="+$("#hasest").val()+"&org="+$("#orgest").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										//alert("Datos registrados correctamente");
										$("#nuevos").click();
										$("#mytablaSob").trigger("update");
										$("#mytablaEstSem").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
										$("#idpiss").focus();$("#aceptars").removeAttr("disabled");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		
		/*}else{
			alert("Error: Registre Sobrevivencia");	
			$("#sobp").focus();
			return false;
		}	*/				
	}else{
		alert("Error: Seleccione Fecha");	
		$("#fecs").focus();$("#aceptars").removeAttr("disabled");
		return false;
	}
});
$("#borrarfq").click( function(){	
	numero=$("#idpfq").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/borrarfq", 
						//data: "id="+$("#idpfq").val()+"&cic="+$("#cmbCiclofq").val(),
						data: "id="+$("#idpfq").val()+"&tabla=pargra"+"&campo=idpfq"+"&cic="+$("#cmbCiclofq").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos Eliminados correctamente");
										$("#nuevofq").click();
										$("#mytablaPar").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
										//$("#nuevo2").click();																																				
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Estanque para poder Eliminar los parámetros");
		return false;
	}
});
$("#aceptarfq").click( function(){	
	$("#aceptarfq").attr("disabled","disabled");	
	fec=$("#fecfq").val();
	numero=$("#idpfq").val();
	if( fec!=''){
	  	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/actualizarpar", 
						data: "id="+$("#idpfq").val()+"&pis="+$("#idpisfq").val()+"&fec="+$("#fecfq").val()+"&t1="+$("#t1fq").val()+"&o1="+$("#o1fq").val()+"&t2="+$("#t2fq").val()+"&o2="+$("#o2fq").val()+"&sal="+$("#salfq").val()+"&tur="+$("#turfq").val()+"&ph="+$("#phfq").val()+"&cic="+$("#cmbCiclofq").val()+"&gra="+$("#cmbGranjafq").val()+"&salu="+$("#salud1").val()+"&reca="+$("#reca").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										$("#nuevofq").click();
										$("#mytablaPar").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
										$("#aceptarfq").removeAttr("disabled");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/agregarpar", 
						data: "pis="+$("#idpisfq").val()+"&fec="+$("#fecfq").val()+"&t1="+$("#t1fq").val()+"&o1="+$("#o1fq").val()+"&t2="+$("#t2fq").val()+"&o2="+$("#o2fq").val()+"&sal="+$("#salfq").val()+"&tur="+$("#turfq").val()+"&ph="+$("#phfq").val()+"&cic="+$("#cmbCiclofq").val()+"&gra="+$("#cmbGranjafq").val()+"&salu="+$("#salud1").val()+"&reca="+$("#reca").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										$("#nuevofq").click();
										$("#mytablaPar").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
										$("#aceptarfq").removeAttr("disabled");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		
	}else{
		alert("Error: Seleccione Fecha");	
		$("#fecfq").focus();$("#aceptarfq").removeAttr("disabled");
		return false;
	}
});

$("#aceptar").click( function(){		
	pis=$("#pisg").val();has=$("#hasg").val();org=$("#orgg").val();
	numero=$("#idpis").val();bio=0;pp=0;
	if( pis!=''){
	  if( has!=''){
	  	if( org!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/actualizarpis", 
						data: "id="+$("#idpis").val()+"&pis="+$("#pisg").val()+"&has="+$("#hasg").val()+"&esp="+$("#espg").val()+"&org="+$("#orgg").val()+"&pro="+$("#prog").val()+"&fec="+$("#fecg").val()+"&est="+$("#plg").val()+"&cic="+$("#cica").val()+"&fecc="+$("#fecgc").val()+"&bio="+$("#biogc").val()+"&pp="+$("#ppgc").val()+"&obs="+$("#obsc").val()+"&numgra="+$("#cmbGranjas").val()+"&secc="+$("#secc").val()+"&lote="+$("#lote").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaPis").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
										$("#accordion").accordion( "activate",0 );
										$("#nuevo").click();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/aqpgranjac2/agregarpis", 
						data: "pis="+$("#pisg").val()+"&has="+$("#hasg").val()+"&esp="+$("#espg").val()+"&org="+$("#orgg").val()+"&pro="+$("#prog").val()+"&fec="+$("#fecg").val()+"&est="+$("#plg").val()+"&cic="+$("#cica").val()+"&numgra="+$("#cmbGranjas").val()+"&secc="+$("#secc").val()+"&lote="+$("#lote").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos registrados correctamente");
										$("#mytablaPis").trigger("update");
										$("#mytablaEstDat").trigger("update");
										$("#mytablaProd").trigger("update");
										$("#accordion").accordion( "activate",0 );
										$("#nuevo").click();										
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
		}else{
			alert("Error: Organismos de Estanque");	
			$("#orgg").focus();
			return false;
		}
		}else{
			alert("Error: Registre las Héctareas de esta estanque");	
			$("#hasg").focus();
			return false;
		}					
	}else{
		alert("Error: Registre Número de Piscina");	
		$("#pisg").focus();
		return false;
	}
});

$(document).ready(function(){
	$('#ver_informacion').show();$('#ver_ali').show();$('#ver_aligra').hide();
	$('#ver_biomasa').hide();$('#ver_temperatura').hide();$('#ver_oxigeno').hide();$('#ver_salinidad').hide();
	$("#cicloed").val($("#cmbCicloEst").val());$("#ested").val($("#idEst").val());$("#cicloprod").val($("#cmbCiclop").val());
	$("#cmbCiclob").val($("#cmbCiclop").val());
	$("#granja").val($("#cmbGranja").val());
	$("#granjasel").val($("#cmbGranjaprod").val());
	$("#cmbCiclofq").change();$("#seccp").change();
	$("#salud1").val(1);
	if($("#cmbGranjaprod").val()==2){
	$("#granjasel").val('Granja Gran kino');}
	else{
	$("#granjasel").val('Granja Ahome');}
	
	var f = new Date();
	$("#txtFI").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());	
	$("#txtFIS").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#txtFIAli").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#txtFIfq").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	
	mes=f.getMonth() +1; dia=f.getDate();
   	if((f.getMonth() +1)<=9) mes="0" + (f.getMonth() +1);
   	if((f.getDate() )<=9) dia="0" + (f.getDate());
    $("#fecfq").val(f.getFullYear() + "-" + mes + "-" + dia);
    $("#fecs").val(f.getFullYear() + "-" + mes + "-" + dia);
	$("#fecb").val(f.getFullYear() + "-" + mes + "-" + dia);
	
	$("#tabla_bio").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjac2/tablabio',  
		//filters:['fecb','seccb','cmbCiclob'],
		filters:['fecb','cmbCiclob'],
        sort:false,
        onRowClick:function(){
           	$("#idbio").val($(this).data('idbio'));
           	$("#idpisb2").val($(this).data('pisg'));
           	$("#fecb").val($(this).data('fecb'));
           	$("#pesb").val($(this).data('pesb'));
           	$("#incest").val($(this).data('incest'));
           	$("#cmbCiclob").val($(this).data('cicb'));	
           	$("#obsb").val($(this).data('obsb'));
		},
		onSuccess:function(){
    		$('#tabla_bio tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    		$('#tabla_bio tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })	    	
    	},        
	});
	$("#tabla_sob").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjac2/tablasob',  
		//filters:['fecs','secco','cmbCiclos'],
		filters:['fecs','cmbCiclos'],
        sort:false,
        onRowClick:function(){
           	$("#idsob").val($(this).data('idsob'));
           	$("#idpiss").val($(this).data('idpiss'));
           	$("#fecs").val($(this).data('fecs'));
           	$("#sobp").val($(this).data('sobp'));
           	$("#cmbCiclos").val($(this).data('cics'));	
           	$("#obss").val($(this).data('obss'));
           	$("#camm2").val($(this).data('camm2'));
		},
		onSuccess:function(){
    		$('#tabla_sob tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })	    	
    	},        
	});
	$("#tabla_ali").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjac2/tablaali',  
		filters:['txtFIAli','txtFFAli'],
        sort:false,
        onRowClick:function(){
           	$("#idali").val($(this).data('idali'));
           	$("#idpisali").val($(this).data('idpisali'));
           	$("#feca").val($(this).data('fecali'));
           	$("#canali").val($(this).data('canali'));
           	$("#cmbCicloa").val($(this).data('cicali'));	
           	$("#obsa").val($(this).data('obsali'));
		}, 
		 onSuccess:function(){
    		$('#tabla_ali tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    		/*$('#tabla_ali tbody tr').click(function() {
    			var total1 = $(this).find("td:first-child").text();
 	 			var total = $(this).find("td:eq(2)").text();
 	 			var total2 = $(this).find("td:last-child").text();
  				alert(total1+total+total2);
			});	    	
			var total =document.getElementById("tabla_ali").rows[0].cells[0].innerHTML = "<p>Algo de <b>HTML</b></p>";
			alert(total);*/
    	},       
	});
	
	$("#tabla_parg").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjac2/tablaparg',  
		filters:['idpisfqg'],
        sort:false,
        onLoad: false
 	});
	
	
	$("#tabla_par").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjac2/tablapar',  
		//filters:['fecfq','seccp','cmbCiclofq'],
		filters:['fecfq','cmbCiclofq'],
        sort:false,
        onRowClick:function(){
           	$("#idpfq").val($(this).data('idpfq'));
           	$("#idpisfq").val($(this).data('idpisfq'));
           	$("#fecfq").val($(this).data('fecfq'));
           	$("#t1fq").val($(this).data('t1fq'));$("#o1fq").val($(this).data('o1fq'));
           	$("#t2fq").val($(this).data('t2fq'));$("#o2fq").val($(this).data('o2fq'));
           	$("#salfq").val($(this).data('salfq'));$("#turfq").val($(this).data('turfq'));
           	$("#phfq").val($(this).data('phfq'));$("#reca").val($(this).data('reca'));
           	$("#cmbCiclofq").val($(this).data('cicfq'));	
           	$("#obsb").val($(this).data('obsb'));
           	$("#salud1").val($(this).data('salud'));salu=$(this).data('salud');
			if(salu==1){ $('input:radio[name=salud2]:nth(0)').attr('checked',true); }
			else if(salu==0){ $('input:radio[name=salud2]:nth(1)').attr('checked',true); }
			else if(salu==-1){ $('input:radio[name=salud2]:nth(2)').attr('checked',true); }
			else if(salu==2){ $('input:radio[name=salud2]:nth(3)').attr('checked',true); }
			else { $('input:radio[name=salud2]:nth(0)').attr('checked',false);$('input:radio[name=salud2]:nth(1)').attr('checked',false);$('input:radio[name=salud2]:nth(2)').attr('checked',false);$('input:radio[name=salud2]:nth(3)').attr('checked',false); }			
       }, 
       onSuccess:function(){   						
			$('#tabla_par tbody tr td:nth-child(8)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() == 1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
            		}
	    			if($(this).html() == 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "orange");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "red");
        			}
        			if($(this).html() == 2 && $(this).html() != '' ) {
              				$(this).css("background-color", "purple"); $(this).css("color", "purple");
        			}
	    	})
	    	
		},	      
	});
	$("#tabla_pis").ajaxSorter({
	url:'<?php echo base_url()?>index.php/aqpgranjac2/tabla',  	
	filters:['cmbGranja','cmbCiclop'],		
    sort:false,
    onRowClick:function(){
    	if($(this).data('idpis')>'0'){
        	$("#accordion").accordion( "activate",1 );
        	$("#idpis").val($(this).data('idpis'));
        	$("#pisg").val($(this).data('pisg'));
       		$("#hasg").val($(this).data('hasg'));       	
					$("#espg").val($(this).data('espg'));
       		$("#orgg").val($(this).data('orgg'));       	
					$("#prog").val($(this).data('prog'));
					$("#fecg").val($(this).data('fecg'));
					$("#plg").val($(this).data('plg'));
					$("#cica").val($(this).data('cicg'));
					$("#fecgc").val($(this).data('fecgc'));
					$("#biogc").val($(this).data('biogc'));
					$("#ppgc").val($(this).data('ppgc'));
					$("#obsc").val($(this).data('obsc'));
					$("#secc").val($(this).data('secc'));
					$("#lote").val($(this).data('lote'));
					$("#cmbGranjas").val($(this).data('numgra'));
		}		
    }, 
    onSuccess:function(){
    		importe = 0;
    	   	$('#tabla_pistbody tr').map(function(){	    		
	    		/*if($(this).data('diferencia')>='60'){
	    			$(this).css('background','lightyellow');	    				    			
	    		}
	    		if($(this).data('si')=='-1'){
	    			$(this).css('background','red');	    				    			
	    		}*/	
	    		//if($(this).data('si')=='0'){	    			
	    		//	importe+=($(this).data('saldo1')!=' '?parseFloat($(this).data('saldo1')):0);	    				    			
	    		//}	
	    	});
	    	$('#tabla_pis tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_pis tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_pis tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
    },     
	});
	$("#tabla_inf").ajaxSorter({
	url:'<?php echo base_url()?>index.php/aqpgranjac2/tablaest',  	
	filters:['cmbCicloEst','idEst','cmbGranjae'],		
    sort:false,
    onSuccess:function(){
    		importe = 0;
    	   	$('#tabla_inf tr').map(function(){	    		
	    		$(this).css('text-align','center');
	    	});
    },   
	});
	$("#tabla_estsem").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjac2/tablaestsem',  
		filters:['cmbGranjasem','cmbCicloGra','idFec','secc'],
        sort:false,
      //  multipleFilter:true,
        onSuccess:function(){
    		importe = 0;
    	   	$('#tabla_estsem tr').map(function(){	    		
	    		$(this).css('text-align','right');
	    	});	
	    	$('#tabla_estsem tbody tr').map(function(){
		    	if($(this).data('pisg')=='Tot') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			
	    		}
		    });    	
    	},
    });
    $("#tabla_estsema").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjac2/tablaestsem',  
		filters:['cmbGranjasem','cmbCicloGra','idFec','secc'],
        sort:false,
      //  multipleFilter:true,
        onSuccess:function(){
    		importe = 0;
    		c1='rgb(169,209,141)';
    	   	/*$('#tabla_estsema tbody tr').map(function(){	    		
	    		$(this).css('text-align','right');
	    	});*/	
	    	$('#tabla_estsema tbody tr').map(function(){
		    	if($(this).data('pisg')=='Tot') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			
	    		}
		    }); 
		    $('#tabla_estsema tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');});
		    //$('#tabla_estsema tbody tr td:nth-child(2)').map(function(){$(this).css('text-align','center');});
		    //$('#tabla_estsema tbody tr td:nth-child(3)').map(function(){$(this).css('text-align','center');});
		    $('#tabla_estsema tbody tr td:nth-child(6)').map(function(){$(this).css('font-weight','bold');});
		    $('#tabla_estsema tbody tr td:nth-child(9)').map(function(){$(this).css('font-weight','bold');});
		    $('#tabla_estsema tbody tr td:nth-child(12)').map(function(){$(this).css('font-weight','bold'); $(this).css('background',c1); })
		    $('#tabla_estsema tbody tr td:nth-child(14)').map(function(){$(this).css('font-weight','bold'); $(this).css('background','rgb(132,151,176)'); })
		    $('#tabla_estsema tbody tr td:nth-child(16)').map(function(){ $(this).css('background','rgb(132,151,176)'); })
		    $('#tabla_estsema tbody tr td:nth-child(17)').map(function(){$(this).css('font-weight','bold'); $(this).css('background','yellow'); })   	
		    $('#tabla_estsema tbody tr td:nth-child(19)').map(function(){ $(this).css('background','yellow'); })
		    $('#tabla_estsema tbody tr td:nth-child(20)').map(function(){$(this).css('font-weight','bold'); $(this).css('background','rgb(0,176,80)'); })
		    $('#tabla_estsema tbody tr td:nth-child(21)').map(function(){$(this).css('font-weight','bold');});
		    $('#tabla_estsema tbody tr td:nth-child(22)').map(function(){$(this).css('font-weight','bold');});
    	},
    });
    $("#tabla_estsemi").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjac2/tablaestsem',  
		filters:['cmbGranjasemi','cmbCicloi','idFeci','secci'],
        sort:false,
      //  multipleFilter:true,
        onSuccess:function(){
    		importe = 0;
    	   	$('#tabla_estsemi tr').map(function(){	    		
	    		$(this).css('text-align','right');
	    	});	
	    	$('#tabla_estsemi tbody tr').map(function(){
		    	if($(this).data('pisg')=='Tot') {	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold');$(this).css('text-align','center');
	    			
	    		}
		    });  
		    $('#tabla_estsemi tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');	});
		    $('#tabla_estsemi tbody tr td:nth-child(5)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');	});
		    $('#tabla_estsemi tbody tr td:nth-child(8)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 0.1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() == 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "black");
        			}
        			
	    	});  	
	    	$('#tabla_estsemi tbody tr td:nth-child(9)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');	});
	    	$('#tabla_estsemi tbody tr td:nth-child(12)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 0.1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() == 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "black");
        			}
        			
	    	});
	    	$('#tabla_estsemi tbody tr td:nth-child(13)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');	});
	    	$('#tabla_estsemi tbody tr td:nth-child(16)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 0.1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() == 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "black");
        			}
        			
	    	});
	    	$('#tabla_estsemi tbody tr td:nth-child(17)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');	});
	    	$('#tabla_estsemi tbody tr td:nth-child(20)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 0.01 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() == 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "black");
        			}
        			
	    	});
	    	$('#tabla_estsemi tbody tr td:nth-child(21)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');	});
	    	$('#tabla_estsemi tbody tr td:nth-child(24)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 0.1 && $(this).html() != '' ) {
                		$(this).css("background-color", "#0F0"); $(this).css("color", "black");
            		}
	    			if($(this).html() == 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              				$(this).css("background-color", "red"); $(this).css("color", "black");
        			}
        			
	    	});
	    	$('#tabla_estsemi tbody tr td:nth-child(25)').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');	});
	    	$('#tabla_estsemi tbody tr td:nth-child(28)').map(function(){ 
				$(this).css('font-weight','bold');
	    			if($(this).html() >= 0.01 && $(this).html() != '' ) {
                		$(this).css("background-color", "red"); $(this).css("color", "black");
            		}
	    			if($(this).html() == 0 && $(this).html() != '' ) {
                		$(this).css("background-color", "orange"); $(this).css("color", "black");
            		}
	    			if($(this).html() < 0 && $(this).html() != '' ) {
              			$(this).css("background-color", "#0F0"); $(this).css("color", "black");
        			}
        			
	    	});
    	},
    });
	$("#tabla_estdat").ajaxSorter({
		url:'<?php echo base_url()?>index.php/aqpgranjac2/tablaestdat',  
		filters:['cmbCicloEst','idEst','cmbGranjae'],
        sort:false,
        onSuccess:function(){
    		obs = '';
    	   	$('#tabla_estdat tr').map(function(){	    		
	    		$(this).css('text-align','right');
	    		fc=$(this).data('fcfin');dc=$(this).data('dcfin');tp=$(this).data('tpfin');
	    		sob=$(this).data('sobfin');orgs=$(this).data('orgsfin');
	    		biot=$(this).data('biot');bioha=$(this).data('bioha');
	    		ali=$(this).data('alifin');fca=$(this).data('fcafin');
	    		obs=$(this).data('obsc1');tp1=$(this).data('ppgc1');bio1=$(this).data('biogc');fec1=$(this).data('fecgc');
	    		//obs='mortalidad';
	    	});	
	    	if(tp1>0 && bio1>0 && fec1!=0000-00-00){
	    	tr='<tr style=background-color:lightblue><td colspan=24>RESULTADOS FINALES</td></tr>';
	    	$('#tabla_estdat tbody').append(tr); 
	    	tr='<tr style=background-color:lightyellow><td rowspan=2 style=text-align:center>Fecha de Cosecha</td><td colspan=2 rowspan=2 style=text-align:center>Días de Cultivo</td><td colspan=2 rowspan=2 style=text-align:center>Talla Promedio</td><td colspan=3 style=text-align:center>Sobrevivencia</td><td colspan=4 style=text-align:center>Biomasa</td><td colspan=4 rowspan=2 style=text-align:center>Alimento Balanceado</td><td colspan=2 rowspan=2 style=text-align:center>F.C.A.</td><td colspan=6 rowspan=2>Observaciones</td></tr>';
	    	$('#tabla_estdat tbody').append(tr);
	    	tr='<tr style=background-color:lightyellow><td style=text-align:center>%</td><td colspan=2 style=text-align:center>Organismos</td><td colspan=2 style=text-align:center>Total</td><td style=text-align:center colspan=2>por Hectárea</td></tr>';
	    	$('#tabla_estdat tbody').append(tr);
	    	tr='<tr style=background-color:lightblue><td style=text-align:center>'+fc+'</td><td colspan=2 style=text-align:center>'+dc+'</td><td colspan=2 style=text-align:center>'+tp+'</td><td style=text-align:center>'+sob+'</td><td colspan=2 style=text-align:center>'+orgs+'</td><td colspan=2 style=text-align:center>'+biot+'</td><td colspan=2 style=text-align:center>'+bioha+'</td><td colspan=4 style=text-align:center>'+ali+'</td><td colspan=2 style=text-align:center>'+fca+'</td><td colspan=6 aling=justify>'+obs+'</td></tr>';
	    	$('#tabla_estdat tbody').append(tr);
	    	}
	    	$("#a1").val(0);$("#a10").val(0);$("#a11").val(0);$("#a12").val(0);$("#a13").val(0);$("#a14").val(0);
	    	$("#b10").val(0);$("#b11").val(0);$("#b12").val(0);$("#b13").val(0);$("#b14").val(0);
	    	$("#d10").val(0);$("#d11").val(0);$("#d12").val(0);$("#d13").val(0);$("#d14").val(0);
	    	$("#t10").val(0);$("#t11").val(0);$("#t12").val(0);$("#t13").val(0);$("#t14").val(0);	
	    	$("#tm10").val(0);$("#tm11").val(0);$("#tm12").val(0);$("#tm13").val(0);$("#tm14").val(0);
	    	$("#tx10").val(0);$("#tx11").val(0);$("#tx12").val(0);$("#tx13").val(0);$("#tx14").val(0);
	    	$("#om10").val(0);$("#om11").val(0);$("#om12").val(0);$("#om13").val(0);$("#om14").val(0);
	    	$("#ox10").val(0);$("#ox11").val(0);$("#ox12").val(0);$("#ox13").val(0);$("#ox14").val(0);
	    	$("#f10").val(0);$("#f11").val(0);$("#f12").val(0);$("#f13").val(0);$("#f14").val(0);
	    	$("#s10").val(0);$("#s11").val(0);$("#s12").val(0);$("#s13").val(0);$("#s14").val(0);
	    	//grafica
	    	$('#tabla_estdat tbody tr').map(function(){
	    	//biosmasa
	    	
	    	if($(this).data('b1')>0){$("#b1").val($(this).data('b1'));}
	    	if($(this).data('b2')>0){$("#b2").val($(this).data('b2'));}
	    	if($(this).data('b3')>0){$("#b3").val($(this).data('b3'));}
	    	if($(this).data('b4')>0){$("#b4").val($(this).data('b4'));}
	    	if($(this).data('b5')>0){$("#b5").val($(this).data('b5'));}
	    	if($(this).data('b6')>0){$("#b6").val($(this).data('b6'));}
	    	if($(this).data('b7')>0){$("#b7").val($(this).data('b7'));}
	    	if($(this).data('b8')>0){$("#b8").val($(this).data('b8'));}
	    	if($(this).data('b9')>0){$("#b9").val($(this).data('b9'));}
	    	if($(this).data('b10')>0){$("#b10").val($(this).data('b10'));}
	    	if($(this).data('b11')>0){$("#b11").val($(this).data('b11'));}
	    	if($(this).data('b12')>0){$("#b12").val($(this).data('b12'));}
	    	if($(this).data('b13')>0){$("#b13").val($(this).data('b13'));}
	    	if($(this).data('b14')>0){$("#b14").val($(this).data('b14'));}
	    	//talla
	    	if($(this).data('t1')>0){$("#t1").val($(this).data('t1'));}
	    	if($(this).data('t2')>0){$("#t2").val($(this).data('t2'));}
	    	if($(this).data('t3')>0){$("#t3").val($(this).data('t3'));}
	    	if($(this).data('t4')>0){$("#t4").val($(this).data('t4'));}
	    	if($(this).data('t5')>0){$("#t5").val($(this).data('t5'));}
	    	if($(this).data('t6')>0){$("#t6").val($(this).data('t6'));}
	    	if($(this).data('t7')>0){$("#t7").val($(this).data('t7'));}
	    	if($(this).data('t8')>0){$("#t8").val($(this).data('t8'));}
	    	if($(this).data('t9')>0){$("#t9").val($(this).data('t9'));}
	    	if($(this).data('t10')>0){$("#t10").val($(this).data('t10'));}
	    	if($(this).data('t11')>0){$("#t11").val($(this).data('t11'));}
	    	if($(this).data('t12')>0){$("#t12").val($(this).data('t12'));}
	    	if($(this).data('t13')>0){$("#t13").val($(this).data('t13'));}
	    	if($(this).data('t14')>0){$("#t14").val($(this).data('t14'));}
	    	//temperatura min
	    	if($(this).data('tm1')>0){$("#tm1").val($(this).data('tm1'));}
	    	if($(this).data('tm2')>0){$("#tm2").val($(this).data('tm2'));}
	    	if($(this).data('tm3')>0){$("#tm3").val($(this).data('tm3'));}
	    	if($(this).data('tm4')>0){$("#tm4").val($(this).data('tm4'));}
	    	if($(this).data('tm5')>0){$("#tm5").val($(this).data('tm5'));}
	    	if($(this).data('tm6')>0){$("#tm6").val($(this).data('tm6'));}
	    	if($(this).data('tm7')>0){$("#tm7").val($(this).data('tm7'));}
	    	if($(this).data('tm8')>0){$("#tm8").val($(this).data('tm8'));}
	    	if($(this).data('tm9')>0){$("#tm9").val($(this).data('tm9'));}
	    	if($(this).data('tm10')>0){$("#tm10").val($(this).data('tm10'));}
	    	if($(this).data('tm11')>0){$("#tm11").val($(this).data('tm11'));}
	    	if($(this).data('tm12')>0){$("#tm12").val($(this).data('tm12'));}
	    	if($(this).data('tm13')>0){$("#tm13").val($(this).data('tm13'));}
	    	if($(this).data('tm14')>0){$("#tm14").val($(this).data('tm14'));}
	    	//temperatura max
	    	if($(this).data('tx1')>0){$("#tx1").val($(this).data('tx1'));}
	    	if($(this).data('tx2')>0){$("#tx2").val($(this).data('tx2'));}
	    	if($(this).data('tx3')>0){$("#tx3").val($(this).data('tx3'));}
	    	if($(this).data('tx4')>0){$("#tx4").val($(this).data('tx4'));}
	    	if($(this).data('tx5')>0){$("#tx5").val($(this).data('tx5'));}
	    	if($(this).data('tx6')>0){$("#tx6").val($(this).data('tx6'));}
	    	if($(this).data('tx7')>0){$("#tx7").val($(this).data('tx7'));}
	    	if($(this).data('tx8')>0){$("#tx8").val($(this).data('tx8'));}
	    	if($(this).data('tx9')>0){$("#tx9").val($(this).data('tx9'));}
	    	if($(this).data('tx10')>0){$("#tx10").val($(this).data('tx10'));}
	    	if($(this).data('tx11')>0){$("#tx11").val($(this).data('tx11'));}
	    	if($(this).data('tx12')>0){$("#tx12").val($(this).data('tx12'));}
	    	if($(this).data('tx13')>0){$("#tx13").val($(this).data('tx13'));}
	    	if($(this).data('tx14')>0){$("#tx14").val($(this).data('tx14'));}
	    	//oxigeno min
	    	//$("#om1").val($(this).data('om1'));$("#om2").val($(this).data('om2'));$("#om3").val($(this).data('om3'));
	    	if($(this).data('om1')>0){$("#om1").val($(this).data('om1'));}
	    	if($(this).data('om2')>0){$("#om2").val($(this).data('om2'));}
	    	if($(this).data('om3')>0){$("#om3").val($(this).data('om3'));}
	    	if($(this).data('om4')>0){$("#om4").val($(this).data('om4'));}
	    	if($(this).data('om5')>0){$("#om5").val($(this).data('om5'));}
	    	if($(this).data('om6')>0){$("#om6").val($(this).data('om6'));}
	    	if($(this).data('om7')>0){$("#om7").val($(this).data('om7'));}
	    	if($(this).data('om8')>0){$("#om8").val($(this).data('om8'));}
	    	if($(this).data('om9')>0){$("#om9").val($(this).data('om9'));}
	    	if($(this).data('om10')>0){$("#om10").val($(this).data('om10'));}
	    	if($(this).data('om11')>0){$("#om11").val($(this).data('om11'));}
	    	if($(this).data('om12')>0){$("#om12").val($(this).data('om12'));}
	    	if($(this).data('om13')>0){$("#om13").val($(this).data('om13'));}
	    	if($(this).data('om14')>0){$("#om14").val($(this).data('om14'));}
	    	//oxigeno max
	    	//$("#ox1").val($(this).data('ox1'));$("#ox2").val($(this).data('ox2'));$("#ox3").val($(this).data('ox3'));
	    	if($(this).data('ox1')>0){$("#ox1").val($(this).data('ox1'));}
	    	if($(this).data('ox2')>0){$("#ox2").val($(this).data('ox2'));}
	    	if($(this).data('ox3')>0){$("#ox3").val($(this).data('ox3'));}
	    	if($(this).data('ox4')>0){$("#ox4").val($(this).data('ox4'));}
	    	if($(this).data('ox5')>0){$("#ox5").val($(this).data('ox5'));}
	    	if($(this).data('ox6')>0){$("#ox6").val($(this).data('ox6'));}
	    	if($(this).data('ox7')>0){$("#ox7").val($(this).data('ox7'));}
	    	if($(this).data('ox8')>0){$("#ox8").val($(this).data('ox8'));}
	    	if($(this).data('ox9')>0){$("#ox9").val($(this).data('ox9'));}
	    	if($(this).data('ox10')>0){$("#ox10").val($(this).data('ox10'));}
	    	if($(this).data('ox11')>0){$("#ox11").val($(this).data('ox11'));}
	    	if($(this).data('ox12')>0){$("#ox12").val($(this).data('ox12'));}
	    	if($(this).data('ox13')>0){$("#ox13").val($(this).data('ox13'));}
	    	if($(this).data('ox14')>0){$("#ox14").val($(this).data('ox14'));}
	    	//alimento
	    	if($(this).data('a1')>0){$("#a1").val($(this).data('a1'));}
	    	if($(this).data('a2')>0){$("#a2").val($(this).data('a2'));}
	    	if($(this).data('a3')>0){$("#a3").val($(this).data('a3'));}
	    	if($(this).data('a4')>0){$("#a4").val($(this).data('a4'));}
	    	if($(this).data('a5')>0){$("#a5").val($(this).data('a5'));}
	    	if($(this).data('a6')>0){$("#a6").val($(this).data('a6'));}
	    	if($(this).data('a7')>0){$("#a7").val($(this).data('a7'));}
	    	if($(this).data('a8')>0){$("#a8").val($(this).data('a8'));}
	    	if($(this).data('a9')>0){$("#a9").val($(this).data('a9'));}
	    	if($(this).data('a10')>0){$("#a10").val($(this).data('a10'));}
	    	if($(this).data('a11')>0){$("#a11").val($(this).data('a11'));}
	    	if($(this).data('a12')>0){$("#a12").val($(this).data('a12'));}
	    	if($(this).data('a13')>0){$("#a13").val($(this).data('a13'));}
	    	if($(this).data('a14')>0){$("#a14").val($(this).data('a14'));}
	    	//dias de cultivo y fecha de biometria
	    	if($(this).data('d1')>0){$("#d1").val($(this).data('d1'));$("#fe1").val($(this).data('fe1'));} 
			if($(this).data('d2')>0){$("#d2").val($(this).data('d2'));$("#f2").val($(this).data('f2'));}
			if($(this).data('d3')>0){$("#d3").val($(this).data('d3'));$("#f3").val($(this).data('f3'));}
			if($(this).data('d4')>0){$("#d4").val($(this).data('d4'));$("#f4").val($(this).data('f4'));}
			if($(this).data('d5')>0){$("#d5").val($(this).data('d5'));$("#f5").val($(this).data('f5'));}
			if($(this).data('d6')>0){$("#d6").val($(this).data('d6'));$("#f6").val($(this).data('f6'));}
			if($(this).data('d7')>0){$("#d7").val($(this).data('d7'));$("#f7").val($(this).data('f7'));}
			if($(this).data('d8')>0){$("#d8").val($(this).data('d8'));$("#f8").val($(this).data('f8'));}
			if($(this).data('d9')>0){$("#d9").val($(this).data('d9'));$("#f9").val($(this).data('f9'));}
			if($(this).data('d10')>0){$("#d10").val($(this).data('d10'));$("#f10").val($(this).data('f10'));}
			if($(this).data('d11')>0){$("#d11").val($(this).data('d11'));$("#f11").val($(this).data('f11'));}
			if($(this).data('d12')>0){$("#d12").val($(this).data('d12'));$("#f12").val($(this).data('f12'));}
			if($(this).data('d13')>0){$("#d13").val($(this).data('d13'));$("#f13").val($(this).data('f13'));}
			if($(this).data('d14')>0){$("#d14").val($(this).data('d14'));$("#f14").val($(this).data('f14'));}
			//salinidad
	    	if($(this).data('s1')>0){$("#s1").val($(this).data('s1'));}
	    	if($(this).data('s2')>0){$("#s2").val($(this).data('s2'));}
	    	if($(this).data('s3')>0){$("#s3").val($(this).data('s3'));}
	    	if($(this).data('s4')>0){$("#s4").val($(this).data('s4'));}
	    	if($(this).data('s5')>0){$("#s5").val($(this).data('s5'));}
	    	if($(this).data('s6')>0){$("#s6").val($(this).data('s6'));}
	    	if($(this).data('s7')>0){$("#s7").val($(this).data('s7'));}
	    	if($(this).data('s8')>0){$("#s8").val($(this).data('s8'));}
	    	if($(this).data('s9')>0){$("#s9").val($(this).data('s9'));}
	    	if($(this).data('s10')>0){$("#s10").val($(this).data('s10'));}
	    	if($(this).data('s11')>0){$("#s11").val($(this).data('s11'));}
			if($(this).data('s12')>0){$("#s12").val($(this).data('s12'));}
			if($(this).data('s13')>0){$("#s13").val($(this).data('s13'));}
			if($(this).data('s14')>0){$("#s14").val($(this).data('s14'));}
			});
	    	var chart;
	    	a1=$("#a1").val(); a1=parseFloat(a1);a2=$("#a2").val(); a2=parseFloat(a2);a3=$("#a3").val(); a3=parseFloat(a3);
	    	a4=$("#a4").val(); a4=parseFloat(a4);a5=$("#a5").val(); a5=parseFloat(a5);a6=$("#a6").val(); a6=parseFloat(a6);
	    	a7=$("#a7").val(); a7=parseFloat(a7);a8=$("#a8").val(); a8=parseFloat(a8);a9=$("#a9").val(); a9=parseFloat(a9);
	    	a10=$("#a10").val(); a10=parseFloat(a10);a11=$("#a11").val(); a11=parseFloat(a11);a12=$("#a12").val(); a12=parseFloat(a12);
	    	a13=$("#a13").val(); a13=parseFloat(a13);a14=$("#a14").val(); a14=parseFloat(a14);
	    	b1=$("#b1").val();b1=parseFloat(b1);b2=$("#b2").val();b2=parseFloat(b2);b3=$("#b3").val();b3=parseFloat(b3);
	    	b4=$("#b4").val();b4=parseFloat(b4);b5=$("#b5").val();b5=parseFloat(b5);b6=$("#b6").val();b6=parseFloat(b6);
	    	b7=$("#b7").val();b7=parseFloat(b7);b8=$("#b8").val();b8=parseFloat(b8);b9=$("#b9").val();b9=parseFloat(b9);
	    	b10=$("#b10").val();b10=parseFloat(b10);b11=$("#b11").val();b11=parseFloat(b11);b12=$("#b12").val();b12=parseFloat(b12);
	    	b13=$("#b13").val();b13=parseFloat(b13);b14=$("#b14").val();b14=parseFloat(b14);
	    	t1=$("#t1").val();t1=parseFloat(t1);t2=$("#t2").val();t2=parseFloat(t2);t3=$("#t3").val();t3=parseFloat(t3);
	    	t4=$("#t4").val();t4=parseFloat(t4);t5=$("#t5").val();t5=parseFloat(t5);t6=$("#t6").val();t6=parseFloat(t6);
	    	t7=$("#t7").val();t7=parseFloat(t7);t8=$("#t8").val();t8=parseFloat(t8);t9=$("#t9").val();t9=parseFloat(t9);
	    	t10=$("#t10").val();t10=parseFloat(t10);t11=$("#t11").val();t11=parseFloat(t11);t12=$("#t12").val();t12=parseFloat(t12);
	    	t13=$("#t13").val();t13=parseFloat(t13);t14=$("#t14").val();t14=parseFloat(t14);
	    	s1=$("#s1").val();s1=parseFloat(s1);s2=$("#s2").val();s2=parseFloat(s2);s3=$("#s3").val();s3=parseFloat(s3);
	    	s4=$("#s4").val();s4=parseFloat(s4);s5=$("#s5").val();s5=parseFloat(s5);s6=$("#s6").val();s6=parseFloat(s6);
	    	s7=$("#s7").val();s7=parseFloat(s7);s8=$("#s8").val();s8=parseFloat(s8);s9=$("#s9").val();s9=parseFloat(s9);
	    	s10=$("#s10").val();s10=parseFloat(s10);s11=$("#s11").val();s11=parseFloat(s11);s12=$("#s12").val();s12=parseFloat(s12);
	    	s13=$("#s13").val();s13=parseFloat(s13);s14=$("#s14").val();s14=parseFloat(s14);
	    	tm1=$("#tm1").val();tm1=parseFloat(tm1);tm2=$("#tm2").val();tm2=parseFloat(tm2);tm3=$("#tm3").val();tm3=parseFloat(tm3);
	    	tm4=$("#tm4").val();tm4=parseFloat(tm4);tm5=$("#tm5").val();tm5=parseFloat(tm5);tm6=$("#tm6").val();tm6=parseFloat(tm6);
	    	tm7=$("#tm7").val();tm7=parseFloat(tm7);tm8=$("#tm8").val();tm8=parseFloat(tm8);tm9=$("#tm9").val();tm9=parseFloat(tm9);
	    	tm10=$("#tm10").val();tm10=parseFloat(tm10);tm11=$("#tm11").val();tm11=parseFloat(tm11);tm12=$("#tm12").val();tm12=parseFloat(tm12);
	    	tm13=$("#tm13").val();tm13=parseFloat(tm13);tm14=$("#tm14").val();tm14=parseFloat(tm14);
	    	tx1=$("#tx1").val();tx1=parseFloat(tx1);tx2=$("#tx2").val();tx2=parseFloat(tx2);tx3=$("#tx3").val();tx3=parseFloat(tx3);
	    	tx4=$("#tx4").val();tx4=parseFloat(tx4);tx5=$("#tx5").val();tx5=parseFloat(tx5);tx6=$("#tx6").val();tx6=parseFloat(tx6);
	    	tx7=$("#tx7").val();tx7=parseFloat(tx7);tx8=$("#tx8").val();tx8=parseFloat(tx8);tx9=$("#tx9").val();tx9=parseFloat(tx9);
	    	tx10=$("#tx10").val();tx10=parseFloat(tx10);tx11=$("#tx11").val();tx11=parseFloat(tx11);tx12=$("#tx12").val();tx12=parseFloat(tx12);
	    	tx13=$("#tx13").val();tx13=parseFloat(tx13);tx14=$("#tx14").val();tx14=parseFloat(tx14);
	    	om1=$("#om1").val();if(om1>0)om1=parseFloat(om1); else om1=0;om2=$("#om2").val();if(om2>0)om2=parseFloat(om2); else om2=0;om3=$("#om3").val();if(om3>0)om3=parseFloat(om3); else om3=0;
	    	om4=$("#om4").val();om4=parseFloat(om4);om5=$("#om5").val();om5=parseFloat(om5);om6=$("#om6").val();om6=parseFloat(om6);
	    	om7=$("#om7").val();om7=parseFloat(om7);om8=$("#om8").val();om8=parseFloat(om8);om9=$("#om9").val();om9=parseFloat(om9);
	    	om10=$("#om10").val();om10=parseFloat(om10);om11=$("#om11").val();om11=parseFloat(om11);om12=$("#om12").val();om12=parseFloat(om12);
	    	om13=$("#om13").val();om13=parseFloat(om13);om14=$("#om14").val();om14=parseFloat(om14);
	    	ox1=$("#ox1").val();if(ox1>0)ox1=parseFloat(ox1); else ox1=0;ox2=$("#ox2").val();if(ox2>0)ox2=parseFloat(ox2); else ox2=0;ox3=$("#ox3").val();if(ox3>0)ox3=parseFloat(ox3); else ox3=0;
	    	ox4=$("#ox4").val();ox4=parseFloat(ox4);ox5=$("#ox5").val();ox5=parseFloat(ox5);ox6=$("#ox6").val();ox6=parseFloat(ox6);
	    	ox7=$("#ox7").val();ox7=parseFloat(ox7);ox8=$("#ox8").val();ox8=parseFloat(ox8);ox9=$("#ox9").val();ox9=parseFloat(ox9);
	    	ox10=$("#ox10").val();ox10=parseFloat(ox10);ox11=$("#ox11").val();ox11=parseFloat(ox11);ox12=$("#ox12").val();ox12=parseFloat(ox12);
	    	ox13=$("#ox13").val();ox13=parseFloat(ox13);ox14=$("#ox14").val();ox14=parseFloat(ox14);
	    	d1=$("#d1").val();d2=$("#d2").val();d3=$("#d3").val();d4=$("#d4").val();d5=$("#d5").val();d6=$("#d6").val();d7=$("#d7").val();
	    	d8=$("#d8").val();d9=$("#d9").val();d10=$("#d10").val();d11=$("#d11").val();d12=$("#d12").val();d13=$("#d13").val();d14=$("#d14").val();
	    	
	    	f1=$("#fe1").val();f2=$("#f2").val();f3=$("#f3").val();f4=$("#f4").val();f5=$("#f5").val();f6=$("#f6").val();f7=$("#f7").val();
	    	f8=$("#f8").val();f9=$("#f9").val();f10=$("#f10").val();f11=$("#f11").val();f12=$("#f12").val();f13=$("#f13").val();f14=$("#f14").val();
    		$(document).ready(function() {
        	chart = new Highcharts.Chart({
            chart: {
                renderTo: 'biomasa',
                zoomType: 'xy'
            },
            title: {
                text: 'Alimento Vs Biomasa'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: [ d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14],
                text: 'Días de Cultivo'
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    formatter: function() {
                        return this.value +'grs';
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: 'Talla',
                    style: {
                        color: '#89A54E'
                    }
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Biomasa',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' kg';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Alimento',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' kg';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                formatter: function() {
                    var unit = {
                        'Biomasa': 'kg',
                        'Talla': 'grs',
                        'Alimento': 'kg'
                    }[this.series.name];
    
                    return ''+
                        this.x +': '+ this.y +' '+ unit;
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 80,
                floating: true,
                backgroundColor: '#FFFFFF'
            },
            series: [{
                name: 'Biomasa',
                color: '#4572A7',
                type: 'column',
                yAxis: 1,
                data: [ b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14],
    
            }, {
                name: 'Alimento',
                type: 'spline',
                color: '#AA4643',
                yAxis: 2,
                data: [ a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14],
                marker: {
                    enabled: false
                },
                dashStyle: 'shortdot'
    
            }, {
                name: 'Talla',
                color: '#89A54E',
                type: 'spline',
                data: [	t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14],
            }]
        });
    });
   
   //grafica de temperatura 
     var chart1;
    $(document).ready(function() {
        chart1 = new Highcharts.Chart({
             chart: {
                renderTo: 'temperatura',
                type: 'line'
            },
            title: {
                text: 'Temperatura Vs Crecimiento'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [ f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14],
            },
            yAxis: {
                title: {
                    text: 'Temperatura (°C)'
                }
            },
            tooltip: {
                enabled: false,
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'°C';
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'T°C (max)',
                data: [tx1, tx2, tx3, tx4, tx5, tx6, tx7, tx8, tx9, tx10, tx11, tx12, tx13, tx14],
            }, {
                name: 'T°C (min)',
                data: [tm1, tm2, tm3, tm4, tm5, tm6, tm7, tm8, tm9, tm10, tm11, tm12, tm13, tm14],
            }, {
                name: 'Talla',
                data: [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14],
            }]
        });
    });
    
    //grafica de oxigeno 
     var chart2;
    $(document).ready(function() {
        chart2 = new Highcharts.Chart({
             chart: {
                renderTo: 'oxigeno',
                type: 'line'
            },
            title: {
                text: 'Oxígeno Vs Crecimiento'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [ f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14],
            },
            yAxis: {
                title: {
                    text: 'Oxígeno (O2)'
                }
            },
            tooltip: {
                enabled: false,
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'O2';
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'O2 (max)',
                data: [ox1, ox2, ox3, ox4, ox5, ox6, ox7, ox8, ox9, ox10, ox11, ox12, ox13, ox14],
            }, {
                name: 'O2 (min)',
                data: [om1, om2, om3, om4, om5, om6, om7, om8, om9, om10, om11, om12, om13, om14],
            }, {
                name: 'Talla',
                data: [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14],
            }, {
                name: 'Limite fundamental que demarca el punto de tolerancia en niveles de oxígeno (2 ml/lt) ). Por debajo el Litopenaeus vannamei puede estresarce y morir.',
                data: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
            }]
        });
    });
    //grafica de salinidad
     var chart3;
    $(document).ready(function() {
        chart4 = new Highcharts.Chart({
             chart: {
                renderTo: 'salinidad',
                type: 'line'
            },
            title: {
                text: 'Salinidad Vs Crecimiento'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [ d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14],
            },
            yAxis: {
                title: {
                    text: 'Salinidad 0/100'
                }
            },
            tooltip: {
                enabled: false,
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'ppt';
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'Sal (ppt)',
                data: [s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14],
            }, {
                name: 'Talla',
                data: [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14],
            }]
        });
    });
    	},
    });
	$("#tabla_prod").ajaxSorter({
	url:'<?php echo base_url()?>index.php/aqpgranjac2/tablaprod',  	
	filters:['cmbGranjaprod','cmbCiclop'],		
    sort:false,
    onSuccess:function(){
    		$('#tabla_prod tr').map(function(){	    		
	    		$(this).css('text-align','right');
	    	});	    	
	    	$('#tabla_prod tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','center'); })
	    	$('#tabla_prod tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','left'); })
	    	$('#tabla_prod tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })
	    	$('#tabla_prod tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','center'); })
    	},    	
	});
	$('#seccb').boxLoader({
            url:'<?php echo base_url()?>index.php/aqpgranjac2/combos/',
            equal:{id:'cicb',value:'#cmbCiclob'},
            select:{id:'secc',val:'val'},
            all:"Sel."
    });
	$('#idpisb2').boxLoader({
            url:'<?php echo base_url()?>index.php/aqpgranjac2/combob/',
            equal:{id:'cmbCiclob',value:'#cmbCiclob'},
            select:{id:'pisg',val:'val'},
            all:"Sel."
    });
   
    $('#idpiss').boxLoader({
            url:'<?php echo base_url()?>index.php/aqpgranjac2/comboo',
            //equal:{id:'secco',value:'#secco'},
            equal:{id:'cmbCiclos',value:'#cmbCiclos'},
            select:{id:'pisg',val:'val'},
            all:"Sel."
    });
    $('#idpisfq').boxLoader({
            url:'<?php echo base_url()?>index.php/aqpgranjac2/combopSecc',
            //equal:{id:'seccp',value:'#seccp'},
            equal:{id:'filest',value:'#filest'},
            select:{id:'pisg',val:'val'},      
            all:"Sel."
    });
    $('#idpisfqg').boxLoader({
            url:'<?php echo base_url()?>index.php/aqpgranjac2/combopg',
            equal:{id:'seccpg',value:'#seccpg'},
            select:{id:'idpis',val:'val'},
            all:"Sel."
    });
    $('#idpisali').boxLoader({
            url:'<?php echo base_url()?>index.php/aqpgranjac2/comboa',
            equal:{id:'secca',value:'#secca'},
            select:{id:'idpis',val:'val'},
            all:"Sel."
    });
    $('#idEst').boxLoader({
            url:'<?php echo base_url()?>index.php/aqpgranjac2/combobe',
            equal:{id:'cmbCicloEst',value:'#cmbCicloEst'},
            select:{id:'idpis',val:'val'},
            all:"Sel."
    });
});
</script>

