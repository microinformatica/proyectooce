<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 20px;
	/*background: none;*/		
}   
#imprimirB { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; color:#000000; }
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#conanu"><img src="<?php echo base_url();?>assets/images/menu/facturas2.png" width="25" height="25" border="0">Análisis General</a></li>			
		<!--<li style="font-size: 20px"><a href="#zona"><img src="<?php echo base_url();?>assets/images/grafica.png" width="30" height="20" border="0">Grafica</a></li>-->
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="conanu" class="tab_content" >
			<div id = "accordion" style="margin-top: -10px; margin-left: -10px; width: 958px; font-size: 12px;" >
					<h3 align="left" style="font-size: 12px" ><a href="#">Comparativo de adquisiciones actuales</a></h3>
					<div style="height: 360px;">				
						
						<div class="ajaxSorterDiv" id="tabla_cgral" name="tabla_cgral" style="margin-top: -21px;height: 410px;width: 920px ">                
                		<div style=" text-align:left; font-size: 12px;" class='filter'>
                		<div align="right">Ciclo	
                			<select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px;" >
                 				<option value="fc16"> 2016 </option>
                 				<?php if($usuario=="Jesus Benítez"){ ?>
                 				<option value="fc15"> 2015 </option>
                 				<option value="fc14"> 2014 </option>
                 				<?php }?>
            				</select>
                			Buscar Descripcion:
                    		<input id="buscar" name="buscar" maxlength="15" type="text" title="Ingrese el texto deseado para buscar"/>
                			Categoria:                    		
							<select name="cmbPM" id="cmbPM" style="font-size: 10px; height: 25px; width: 110px" >
          						<option value="0">Todas</option>
          						<option value="1">Larvicultura</option>
          						<option value="2">Maduración</option>
          						<option value="3">Microalgas</option>
          						<option value="4">Insumos Varios</option>
          					</select>
          					Moneda:                    		
							<select name="cmbMU" id="cmbMU" style="font-size: 10px; height: 25px; width: 80px" >
          						<option value="0">Todas</option>
          						<option value="1">Dólares</option>
          						<option value="2">Pesos</option>
          					</select>
          					</div>
                		</div>
                		<span class="ajaxTable" style="height: 345px;width: 900px; margin-top: 1px" >
                		<table id="mytablaCG" name="mytablaCG" class="ajaxSorter" border="1" >
                        	<thead>  
                        		<tr>
                        			<th rowspan="2" data-order = "des">Descripción</th> 
                        			<th colspan="3" style="text-align: center">Ciclo Anterior</th>
                        			<th colspan="7" style="text-align: center">Ciclo Actual</th>
                        			<!--<th colspan="2" style="text-align: center">Proyección Ciclo Actual</th>-->
                        		</tr>
                        	    <th data-order = "anterior">Cantidad</th>
                            	<th data-order = "ppant">Precio</th>
                            	<th data-order = "impant">Importe</th>  
                            	<th data-order = "cantidad">Cantidad</th>
                            	<th data-order = "ppact">Precio</th>
                            	<th data-order = "importe">Importe</th>                                                                                                          
                            	<th data-order = "pca">% <sub>(1)</sub></th>
                            	<th data-order = "inc">Inc. Uni</th>
                            	<th data-order = "incp">Inc. Pre</th>
                            	<th data-order = "incd">Inc. $</th>
                            	<!--<th data-order = "proyeccion">Cantidad</th>
                            	<th data-order = "pp">% <sub>(2)</sub></th>-->
                            </thead>
                        	<tbody style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="margin-top: -10px">        
	                    	<ul style="width: 430px" class="order_list"><sup>(fila roja) incremento en precio - (1) % adquisición actual vs ciclo anterior.</sup><!--<sup>(2) % adquisición actual vs proyección actual.</sup>--></ul>                                            
    	                	<form method="post" action="<?= base_url()?>index.php/faccomprasgral/pdfrep/" >
        	            		<input type="hidden" size="8%" name="catego" id="catego" />                      	             	                                                                                           
                	        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        		                        		                                                      
 							</form>   
                		</div>                                      
            			</div>  
            			            		
            	 	</div> 
        			<h3 align="left" style="font-size: 12px"><a href="#">Analizar Resultados</a></h3>
		       	 	<div style="height: 150px;">
		       	 		<table border="1" width="890px" style="margin-top: -10px;" >
		       	 			<tr >
								<th colspan="4" ><input style="border: none; font-size: 16px;" size="100%" name="pm" id="pm" /></th>
							</tr>
		       	 			<tr>
								<th style="border-top: none;border-bottom: none; border-right: none; width: 80px"></th>
								<th style="width: 115px; text-align: center; border-left: none;border-right: none">Anterior</th>
								<th style="width: 115px; text-align: center; border-left: none">Actual</th>
								<th>Detalle<input type="hidden" readonly="true" size="2%"  type="text" name="idpsc" id="idpsc"></th>
							</tr>
							<tr>
								<th  style="text-align: right; border-top: none;border-bottom: none">Cantidad</th>
								<th><input style="text-align: right; border: none" size="18%" name="ca" id="ca" /></th>
								<th><input style="text-align: right; border: none" size="18%" name="ct" id="ct" /></th>
								<th rowspan="11">
		       	 		
		       	 		<div class="ajaxSorterDiv" id="tabla_dms" name="tabla_dms" style="height: 320px;width: 580px" >
							<span class="ajaxTable" style= "margin-top: 1px; height: 290px; width: 579px" >
                    			<table id="mytablaDMS" name="mytablaDMS" class="ajaxSorter"   >
                        			<thead>                            
                            			<th data-order = "Razon">Proveedor</th>
                            			<th data-order = "fec1">Fecha</th>
                            			<th data-order = "fac" >Factura</th> 
                            			<th data-order = "can" >Cantidad</th>                            
                            			<!--<th data-order = "uni" style="width: 20px;"></th>-->
                            			<th data-order = "pre" >P.U.</th>
                            			<th data-order = "tot" >Total</th>
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablesm" action="<?= base_url()?>index.php/faccompras/pdfreps" method="POST">							
    	    	            		<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" id="export_datam" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" readonly="true" size="88%"  type="text" name="desm" id="desm" >
        	            	    	<input type="hidden" name="tabladms" id="html_dms"/>
								</form>    
                			</div>       
	                	</div>
		       			 </th>
							</tr>
							<tr>
								<th style="text-align: right; border-top: none;border-bottom: none">Precio Prom.</th>
								<th><input style="text-align: right; border: none" size="18%" name="pa" id="pa" /></th>
								<th><input style="text-align: right; border: none" size="18%" name="pt" id="pt" /></th>
							</tr>
							<tr>
								<th style="text-align: right; border-top: none;border-bottom: none">Importe</th>
								<th><input style="text-align: right; border: none" size="18%" name="ia" id="ia" /></th>
								<th><input style="text-align: right; border: none" size="18%" name="it" id="it" /></th>
							</tr>
							<tr>
								<th style="text-align: justify" colspan="3">RESULTADOS PRELIMINARES ACTUALES</th>
							</tr>
							<tr>
								<th style="text-align: right; border-top: none;border-bottom: none" colspan="2">% Adquisicion</th>
								<th><input style="text-align: right; border: none" size="18%" name="po" id="po" /></th>
							</tr>
							<tr>
								<th style="text-align: right; border-top: none;border-bottom: none" colspan="2">Incremento Unidad</th>
								<th><input style="text-align: right; border: none" size="18%" name="iu" id="iu" /></th>
							</tr>
							<tr>
								<th style="text-align: right; border-top: none;border-bottom: none" colspan="2">Incremento Precio</th>
								<th><input style="text-align: right; border: none" size="18%" name="ip" id="ip" /></th>
							</tr>
							<tr>
								<th style="text-align: right; border-top: none;border-bottom: none" colspan="2">Incremento Importe</th>
								<th><input style="text-align: right; border: none" size="18%" name="id" id="id" /></th>
							</tr>
							<tr>
								<th style="text-align: justify" colspan="3">RESULTADOS PRELIMINARES PROYECTADOS</th>
							</tr>
							<tr>
								<th style="text-align: justify; border-top: none;border-bottom: none" colspan="2">Si se alcanza o se superan las adquisiciones actuales con las del ciclo anterior, se generará un pago final de:</th>
								<th><input style="text-align: right; border: none" size="18%" name="itp" id="itp" /></th>
							</tr>
							<tr>
								<th style="text-align: justify; border-top: none;border-bottom: none" colspan="2">Obteniendo un incremento final de:</th>
								<th><input style="text-align: right; border: none" size="18%" name="itf" id="itf" /></th>
							</tr>		
            			</tr> 
            			<tr>
							<th colspan="4">Nota: denominación de moneda <input style="border: none" size="100%" name="mu" id="mu" /></th>
						</tr>          	
						</table> 
        			</div>
	 		</div>
					
					 
 	 	</div>
		<div id="zona" class="tab_content" >	
		</div>			
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#cmbPM").change( function(){		
	$("#catego").val($("#cmbPM").val());	
 return true;
});
$(document).ready(function(){
	$("#catego").val($("#cmbPM").val());	
	$("#tabla_cgral").ajaxSorter({
	url:'<?php echo base_url()?>index.php/faccomprasgral/tabla',  	
	filters:['cmbCiclo','cmbPM','cmbMU','buscar'],	
	active:['incp1', 0.09,'>='],
    sort:false,
     onRowClick:function(){
    	if($(this).data('nrc')>'0'){
        	$("#accordion").accordion( "activate",1 );
        	$("#idpsc").val($(this).data('nrc'));$("#pm").val($(this).data('des'));$("#desm").val($(this).data('des'));
        	$("#ca").val($(this).data('anterior'));$("#ct").val($(this).data('cantidad'));
        	$("#pa").val($(this).data('ppant'));$("#pt").val($(this).data('ppact'));
        	$("#ia").val($(this).data('impant'));$("#it").val($(this).data('importe'));
        	$("#po").val($(this).data('pca'));$("#iu").val($(this).data('inc'));
        	$("#ip").val($(this).data('incp'));$("#id").val($(this).data('incd'));
        	$("#itp").val($(this).data('itp'));$("#itf").val($(this).data('itf'));$("#mu").val($(this).data('dp'));
        	$("#tabla_dms").ajaxSorter({
				url:'<?php echo base_url()?>index.php/faccompras/tabladms',  
				filters:['cmbCiclo','idpsc'],
				sort:false,
				onSuccess:function(){
    					$('#tabla_dms tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
			   			$('#tabla_dms tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })	    	
			   			$('#tabla_dms tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
    			},  	
   			});
		}		
    }, 
    onSuccess:function(){
    		$('#tabla_cgral tbody tr').map(function(){
	    		if($(this).data('anterior')=='')
	    			$(this).css('background','lightblue'); //$(this).css('font-weight','bold');   		
	    	});
	    	$('#tabla_cgral tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(10)').map(function(){ $(this).css('text-align','right'); $(this).css('font-weight','bold');})
	    	$('#tabla_cgral tbody tr td:nth-child(11)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_cgral tbody tr td:nth-child(12)').map(function(){ $(this).css('text-align','right'); })
	},     
	});
});

	
</script>