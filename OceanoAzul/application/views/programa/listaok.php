<?php $this->load->view('header_cv');?>
<!--
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/base.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui/css/redmond/jquery-ui-1.8.4.custom.css" />
-->
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;
}   
table.ajaxSorter tbody tr td{
    font-family: arial;     
	padding-top:4px;
    padding-left: 5px;  
	color:black;
	height:15px;	
}

</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>	
		<li><a href="#entregas" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/programa.png" width="25" height="25" border="0"> Programación de Entregas </a></li>
		<li><a href="#salas" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/calendario.png" width="25" height="25" border="0"> Proyección Salas </a></li>
		<li><a href="#raceways" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/10.png" width="25" height="25" border="0"> Raceways </a></li>
		<li><a href="#mes" style="font-size: 24px"><img src="<?php echo base_url();?>assets/images/menu/mensual.png" width="20" height="20" border="0"> Mes </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" style="height: 472px;" >
		<div id="entregas" class="tab_content" style="margin-top: -5px" >
			<table border="2">
				<tr>
					<th rowspan="9">
						<div class="ajaxSorterDiv" id="tabla_prog" name="tabla_prog" style="width: 450px;"  >                 			
							<div class="ajaxpager" style=" font-size: 12px;  text-align: left"> 
								<form id="data_tablef" action="<?= base_url()?>index.php/programa/pdfrepdetdia" method="POST" >							
    	    	            		<ul class="order_list" style="width: 390px; ">
    	    	            			Ciclo:  
	                   						<select name="Ciclo" id="Ciclo" style="font-size: 10px; height: 25px;margin-top: 1px;" >
                    							<?php $ciclof=2014; $actual=date("Y"); //$actual+=1;
													while($actual >= $ciclof){?>
													<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
													<?php  $actual-=1; } ?>
          									</select>
    	    	            			Entrega:
    	    	            			<input size="10%" type="text" name="fecpd" id="fecpd" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
    	    	            			Origen:<select name="Origen" id="Origen" style="font-size: 11px;height: 25px;  margin-top: 1px;">
   											<option value="Todos" >Todos</option>
   											<option value="Aquapacific" >Aquapacific</option>
   											<option value="Ecuatoriana" >Ecuatoriana</option>
   										</select>
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: 5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tabladia" value ="" class="htmlTable"/> 
								</form>   
                			</div>
							<span class="ajaxTable" style="height: 400px; margin-top: -15px; background-color: white " >
                    			<table id="mytablaProg" name="mytablaProg" class="ajaxSorter" border="1" style="width: 440px" >
                        			<thead>                            
                        				<th data-order = "tipopl1" >Origen</th>
                            			<th data-order = "Razon" >Cliente</th>
                            			<th data-order = "canpd1" >Cantidad</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			       
             			</div>
					</th>
					<th style="background-color: gray; color: white">Cliente
						<input type="hidden" size="3%" name="idpd" id="idpd">
						<input type="hidden" size="3%" name="idcte" id="idcte">
					</th>
				</tr>
				<tr><th><input style="border: 0" readonly="true" size="60%"  type="text" name="cliente" id="cliente"></th></tr>
				<tr><th style="background-color: gray; color: white">Datos Cliente</th></tr>
				<tr><th style="border-bottom: none">Granja:<input style="border: 0" readonly="true" size="53%"  type="text" name="ubigra" id="ubigra"></th></tr>
				<tr>
					<th style="border-top: none" >Contacto:<input style="border: 0" size="35%" readonly="true"  type="text" name="con" id="con"><!--Tel:<input style="border: 1" size="15%" readonly="true"  type="text" name="tel" id="tel">--></th>
				</tr>
				<tr><th style="background-color: gray; color: white">Datos Entrega Postlarvas</th></tr>
				<tr>
					<th>Origen:
						<select name="cmbOrigen" id="cmbOrigen" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
   							<option value="Aquapacific" >Aquapacific</option>
   							<option value="Ecuatoriana" >Ecuatoriana</option>
   						</select>
						Cantidad:<input size="6%"  type="text" name="canpd" id="canpd" style="text-align: center">
						[+]  Porcentaje:<input size="4%"  type="text" name="porpd" id="porpd" value="5.00" style="text-align: center">
						<br />Obs:<input size="55%"  type="text" name="obspd" id="obspd" style="text-align: justify">
   					</th>
				</tr>
				<tr>
					<th style="text-align: center; background-color: gray">
						<input type="submit" id="pnuevo" name="pnuevo" value="Nuevo"  />
						<input type="submit" id="paceptar" name="paceptar" value="Guardar" />
						<input type="submit" id="pborrar" name="pborrar" value="Borrar" />
					</th>
				</tr>
				<tr>
					<th>
						<div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" >                 			
							<span class="ajaxTable" style="height: 240px;" >
                    			<table id="mytablaCli" name="mytablaCli" class="ajaxSorter" border="1">
                        			<thead>                            
                            			<th data-order = "Razon" >Cliente</th>
                            			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
									<input type="hidden" name="tablascli" id="html_scli"/>
							</div>       
	                	</div>
					</th>
				</tr>			
			</table>
		</div>	
		<div id="salas" class="tab_content" style="margin-top: -5px" >
			<table border="2">
				<tr>
					<th  colspan="7">
						<div class="ajaxSorterDiv" id="tabla_sala" name="tabla_sala" style="width: 665px;"  >                 			
							<div class="ajaxpager" style=" font-size: 12px;  text-align: left"> 
								<form id="data_tablef" action="<?= base_url()?>index.php/programa/pdfrepdetsal" method="POST" >							
    	    	            		<ul class="order_list" style="width: 390px; ">
    	    	            			Ciclo:  
	                   						<select name="CicloS" id="CicloS" style="font-size: 10px; height: 25px;margin-top: 1px;" >
                    							<?php $ciclof=2014; $actual=date("Y"); //$actual+=1;
													while($actual >= $ciclof){?>
													<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
													<?php  $actual-=1; } ?>
          									</select>
    	    	            			<!--Transf:
    	    	            			<input size="10%" type="text" name="fecSi" id="fecSi" class="fecha redondo mUp" readonly="true" style="text-align: center;" >-->
    	    	            			Origen:<select name="origens" id="origens" style="font-size: 11px;height: 25px;  margin-top: 1px;">
   											<option value="Todos" >Todos</option>
   											<option value="Aquapacific" >Aquapacific</option>
   											<option value="Ecuatoriana" >Ecuatoriana</option>
   										</select>
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: 5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablasala" value ="" class="htmlTable"/> 
								</form>   
                			</div>
							<span class="ajaxTable" style="height: 330px; margin-top: -15px; background-color: white " >
                    			<table id="mytablaSalas" name="mytablaSalas" class="ajaxSorter" border="1" style="width: 645px" >
                        			<thead>                            
                        				<th style="text-align: center" data-order = "sala" >Sala</th>
                        				<th style="text-align: center" data-order = "fecs1" >Siembra</th>
                            			<th style="text-align: center" data-order = "origens" >Origen</th>
                            			<th style="text-align: center" data-order = "corrida" >Corrida</th>
                            			<th style="text-align: center" data-order = "fect1" >Transferencia</th>
                            			<th style="text-align: center" data-order = "cans" >Producción Estimada</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			       
             			</div>
					</th>
					
				</tr>
				<tr>
					<th style="text-align: center;background-color: gray; color: white" >Sala<input type="hidden" size="3%" name="ids" id="ids"></th>
					<th style="text-align: center;background-color: gray; color: white">Origen</th>
					<th style="text-align: center;background-color: gray; color: white">Corrida No.</th>
   					<th style="text-align: center;background-color: gray; color: white">Siembra</th>
   					<th style="text-align: center;background-color: gray; color: white">Dias</th>
					<th style="text-align: center;background-color: gray; color: white">Cosecha</th>	  						
					<th style="text-align: center;background-color: gray; color: white">Producción Estimada</th>
				</tr>
				<tr>
					<th style="text-align: center;background-color: gray; color: white">
						<select name="sala" id="sala" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<option value="0" >Seleccione</option>
          					<?php	
          					//$this->load->model('edoctatec_model');
          					$data['result']=$this->programa_model->verSala();
							foreach ($data['result'] as $row):?>
           					<option value="<?php echo $row->Sala;?>" ><?php echo $row->Sala;?></option>
           					<?php endforeach;?>
   						</select>
					</th>
					<th style="text-align: center;background-color: gray; color: white">
						<select name="origenes" id="origenes" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
							<option value="Todos" >Seleccione</option>
   							<option value="Aquapacific" >Aquapacific</option>
   							<option value="Ecuatoriana" >Ecuatoriana</option>
   						</select>
					</th>
					<th style="text-align: center;background-color: gray; color: white"><input size="6%"  type="text" name="cor" id="cor" style="text-align: center"> 
						
						
						</th>
					<th style="text-align: center;background-color: gray; color: white"><input size="10%" type="text" name="fecs" id="fecs" class="fecha redondo mUp" readonly="true" style="text-align: center;" >	</th>
					<?php $mesact=date("m");
						if((date("m")>=3) && (date("m")<=9)){ $val=19;} else {$val=21;}
					 ?>
					
					<th style="text-align: center;background-color: gray; color: white"><input onKeyPress="return(dia(this,event));" size="2%"  type="text" name="dias" id="dias" style="text-align: center" value="<?php echo set_value('dias',$val); ?>"></th>
					<script>
						function dia (fld, e) {
							var dias=0;
							var key = '';
							var i = j = 0;
							var len = len2 = 0;
							var strCheck = '-0123456789';
							var aux = aux2 = '';
							var whichCode = (window.Event) ? e.which : e.keyCode;
							if (whichCode == 13) return true;  
							key = String.fromCharCode(whichCode);  // Get key value from key code
							if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
							len = fld.value.length;
							for(i = 0; i < len; i++)
							if ((fld.value.charAt(i) != '0')) break;
							aux = '';
							for(; i < len; i++)
							if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
							aux += key;
							len = aux.length;
							dias=(24*60*60*1000*aux);
							//dias=15;	
							var p = new Date( (Date.parse( $("#fecs").val() )) + dias);
							var curr_date = p.getDate();
							var curr_month = p.getMonth();
								curr_month++;
							var curr_year = p.getFullYear();
							var newdate = curr_year + "-"	+ curr_month + "-" + curr_date;
							$("#fect").datepicker( "setDate", newdate);	
							$("#fect").datepicker( "option", "minDate", newdate );
 							return true;
						}
					</script>
					<th style="text-align: center;background-color: gray; color: white"><input size="10%" type="text" name="fect" id="fect" class="fecha redondo mUp" readonly="true" style="text-align: center;" > </th>
					<th style="text-align: center;background-color: gray; color: white"><input size="6%"  type="text" name="cans" id="cans" style="text-align: center"></th>
				</tr>
				<tr>
					<th style="text-align: center; background-color: gray" colspan="7">
						<input type="submit" id="snuevo" name="snuevo" value="Nuevo"  />
						<input type="submit" id="saceptar" name="saceptar" value="Guardar" />
						<input type="submit" id="sborrar" name="sborrar" value="Borrar" />
						
					</th>
				</tr>
				
			</table>
		</div>
		<div id="raceways" class="tab_content" style="height: 452px" >
			
			<div align="left">
				<table class="tablaSorter" border="0px" style="margin-top: -10px; margin-left: -7px;">
					<?php $hoy=date("Y-m-d"); ?>
								<input type="hidden" style="border: none" size="12%" name="hoy" id="hoy" value="<?php echo set_value('hoy',$hoy); ?>">
								<input type="hidden" size="12%" name="ini1" id="ini1" value="1"><input type="hidden" size="12%" name="fin1" id="fin1" value="20">
								<input type="hidden" size="12%" name="ini2" id="ini2" value="21"><input type="hidden" size="12%" name="fin2" id="fin2" value="40">
								<input type="hidden" size="12%" name="ini3" id="ini3" value="41"><input type="hidden" size="12%" name="fin3" id="fin3" value="60">
								<input type="hidden" size="12%" name="ini4" id="ini4" value="61"><input type="hidden" size="12%" name="fin4" id="fin4" value="78">
								<input type="hidden" size="12%" name="ini5" id="ini5" value="87"><input type="hidden" size="12%" name="fin5" id="fin5" value="90">
								<input type="hidden" size="2%" name="tablasel" id="tablasel">
					<tbody>
						<tr >
							<th style="font-size: 10px; width: 370px"  >
								Existencia Postlarvas día:
								<input style="border: none; font-size: 10px" size="10%" name="hoy1" id="hoy1" value="<?php echo set_value('hoy1',date("d-m-Y",strtotime($hoy))); ?>">
								<div class="ajaxSorterDiv" id="tabla_ras7" name="tabla_ras7"  style="width: 210px" >                
            						<span class="ajaxTable" style="background-color: white; height: 145px ; width: 208px;" >
                   						<table id="mytablaR7" name="mytablaR7" class="ajaxSorter" >
                       						<thead >                            
                           						<th style="font-size: 8px" data-order = "PL1" >Generales en Raceways</th>	<th style="font-size: 8px" data-order = "totpl"  >Cantidad</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 10px">
                       						</tbody>                        	
                   						</table>
                   						<?php	
           									$this->load->model('programa_model');
											$data['result']=$this->programa_model->UltimaAct();
											foreach ($data['result'] as $row):
           										$hoyAct=$row->ultimaact;
           									endforeach;
           								?>
                   						Ultima Actualización: <input style="border: none; font-size: 10px; <? if($hoy!=$hoyAct){ ?> background-color: red; color: white;<? } ?> text-align: center" size="10%" name="hoyA" id="hoyA" value="<?php echo set_value('hoy1',date("d-m-Y",strtotime($hoyAct))); ?>">
                   						<?php
                   						//if($hoy!=$hoyAct){ echo ":(";} else{ echo ":)";}
                   						?>
                   					
            						</span>
            						<img style="cursor: pointer; margin-top: -10px" title="Enviar datos de tabla en archivo PDF" id="export_data" src="<?= base_url()?>assets/img/sorter/pdf.png" />
            						<form id="data_tables" action="<?= base_url()?>index.php/produccion/pdfrep" method="POST">
												<input type="hidden" name="tablar1" id="html_r1"/>
												<input type="hidden" name="tablar2" id="html_r2"/>
												<input type="hidden" name="tablar3" id="html_r3"/>
												<input type="hidden" name="tablar4" id="html_r4"/>
												<input type="hidden" name="tablar5" id="html_r5"/>
												<input type="hidden" name="tablar6" id="html_r6"/>
												<input type="hidden" name="tablar7" id="html_r7"/>
											</form>           
            					</div>
								<script type="text/javascript">
									$(document).ready(function(){
										$('#export_data').click(function(){									
											$('#html_r1').val($('#mytablaR1').html());
											$('#html_r2').val($('#mytablaR2').html());									
											$('#html_r3').val($('#mytablaR3').html());
											$('#html_r4').val($('#mytablaR4').html());
											$('#html_r5').val($('#mytablaR5').html());
											$('#html_r6').val($('#mytablaR6').html());
											$('#html_r7').val($('#mytablaR7').html());
											$('#data_tables').submit();
											$('#html_r1').val('');
											$('#html_r2').val('');
											$('#html_r3').val('');
											$('#html_r4').val('');
											$('#html_r5').val('');
											$('#html_r6').val('');
											$('#html_r7').val('');
										});
									});
								</script>
			      				
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras1" name="tabla_ras1" style="margin-top: -5px; width: 130px" >                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 130px" >
                   						<table id="mytablaR1" name="mytablaR1" class="ajaxSorter"  >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 25px" >Ext</th><th data-order = "Sala" style="width: 25px" >Sal</th> <th data-order = "PL1" style="width: 35px">Est</th>	<th data-order = "CanSie" style="width: 20px" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px;" >
                       						</tbody>                        	
                   						</table>
            						</span>          
            					</div>
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras2" name="tabla_ras2" style="margin-top: -5px; width: 130px">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 130px; " >
                   						<table id="mytablaR2" name="mytablaR2" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 25px" >Ext</th><th data-order = "Sala" style="width: 25px" >Sal</th> <th data-order = "PL1" style="width: 35px">Est</th>	<th data-order = "CanSie" style="width: 20px" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span> 
            					</div>
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras3" name="tabla_ras3" style="margin-top: -5px; width: 130px;">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 130px; " >
                   						<table id="mytablaR3" name="mytablaR3" class="ajaxSorter" >
                       						<thead style="font-size: 9px; ">                            
                           						<th data-order = "Nombre" style="width: 25px" >Ext</th><th data-order = "Sala" style="width: 25px" >Sal</th> <th data-order = "PL1" style="width: 35px">Est</th>	<th data-order = "CanSie" style="width: 20px" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>                                      
            					</div>
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras4" name="tabla_ras4" style="margin-top: -5px; width: 130px;">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 130px; " >
                   						<table id="mytablaR4" name="mytablaR4" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 25px" >Gen</th><th data-order = "Sala" style="width: 25px" >Sal</th> <th data-order = "PL1" style="width: 35px">Est</th>	<th data-order = "CanSie" style="width: 20px" >Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>                                      
            					</div>
							</th>
							<th rowspan="1" style="width: 110px;">
								<div class="ajaxSorterDiv" id="tabla_ras5" name="tabla_ras5" style="margin-top: -5px; width: 145px;">                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 135px; " >
                   						<table id="mytablaR5" name="mytablaR5" class="ajaxSorter" >
                       						<thead style="font-size: 9px;">                            
                           						<th data-order = "Nombre" style="width: 30px">Res</th><th data-order = "Sala" style="width: 25px" >Sal</th>	<th data-order = "PL1" style="width: 35px">Est</th>	<th data-order = "CanSie" style="width: 20px">Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span> 
            					</div>
            				</th>
							<th rowspan="1" style="width: 90px;">
								<div class="ajaxSorterDiv" id="tabla_ras6" name="tabla_ras6" style="margin-top: -5px; width: 80px" >                
            						<span class="ajaxTable" style="height: 461px; background-color: white; width: 75px"  >
                   						<table id="mytablaR6" name="mytablaR6" class="ajaxSorter"   >
                       						<thead style="font-size: 9px">                            
                           						<th data-order = "PL1" style="width: 30px">Est</th>	<th data-order = "totpl" style="width: 25px">Can</th>                                                	                            	
                       						</thead>
                       						<tbody style="font-size: 11px">
                       						</tbody>                        	
                   						</table>
            						</span>           
            					</div>
							</th>
							
							
							
						</tr>
					</tbody>
					
				</table>
			</div>
        </div>
		<div id="mes" class="tab_content" style="margin-top: -15px" >
					 
				<div class="ajaxSorterDiv" id="tabla_detmes" name="tabla_detmes" style="height: 470px; width: 950px; margin-left: -10px ">                
               		<div class="ajaxpager" style="margin-top: 0px" >        
	                	<form  id="data_tables1" method="post" action="<?= base_url()?>index.php/programa/pdfrepdetmes" >
	                   	<ul class="order_list" style="width: 500px" >
	                   		Ciclo:  
	                   	<select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px;margin-top: 1px; margin-left:4px;" >
                    	<?php $ciclof=2014; $actual=date("Y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
							<?php  $actual-=1; } ?>
          				</select> 
          				Mes:
	                   	<select name="cmbMesT" id="cmbMesT" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
   							<option value="12" >Diciembre</option>
   							<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
   							<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
   							<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
   							<option value="10" >Octubre</option><option value="11" >Noviembre</option>
   						</select>
   						Origen:<select name="OrigenMes" id="OrigenMes" style="font-size: 11px;height: 25px;  margin-top: 1px;">
   									<option value="Aquapacific" >Aquapacific</option>
   									<option value="Ecuatoriana" >Ecuatoriana</option>
   							   </select>
	                   		
	                   	</ul>  
	                   		<img style="cursor: pointer" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />
	                   		<input type="hidden"  name="tabla" class="htmlTable"/>
	                   		<!--<img style="cursor: pointer" style="margin-top: 6px" class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />-->
	                   		<!--<img style="cursor: pointer" id="exporterexcel" src="<?= base_url()?>assets/img/sorter/export.png" />-->                                                                  
                        	<!--<input type="hidden"  name="tablae" class="htmlTable"/>-->
                        	<!--<input type="hidden"  name="tabla" id="html_progmes"/>-->
                        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png"  />
                        </form >
                       	<script type="text/javascript">
							$(document).ready(function(){
									$('#exporterpdfi').click(function(){									
										$('#html_progmes').val($('#mytablaTM').html());
										$('#data_tablesi').submit();
											
										$('#html_progmes').val('');									
									});
							});
						</script> 		
 					</div>  
               		<span class="ajaxTable" style="height: 410px; background-color: white; width: 930px; margin-top: -15px" >
            	       	<table id="mytablaTM" class="ajaxSorter" border="1">
            	       		<thead >
            	       			<tr>
                        		<th style="font-size: 7px;width: 250px" data-order = "Razon">Dia</th>
                        		<th style="font-size: 7px;width: 10px" data-order = "d1">1</th><th style="font-size: 7px;width: 10px" data-order = "d2">2</th><th style="font-size: 7px;width: 10px" data-order = "d3">3</th>
                        		<th style="font-size: 7px;width: 10px" data-order = "d4">4</th><th style="font-size: 7px;width: 10px" data-order = "d5">5</th><th style="font-size: 7px;width: 10px" data-order = "d6">6</th>
                        		<th style="font-size: 7px;width: 10px" data-order = "d7">7</th><th style="font-size: 7px;width: 10px" data-order = "d8">8</th><th style="font-size: 7px;width: 10px" data-order = "d9">9</th><th style="font-size: 7px;width: 10px" data-order = "d10">10</th>
                        		<th style="font-size: 7px;width: 10px" data-order = "d11">11</th><th style="font-size: 7px;width: 10px" data-order = "d12">12</th><th style="font-size: 7px;width: 10px" data-order = "d13">13</th>
                        		<th style="font-size: 7px;width: 10px" data-order = "d14">14</th><th style="font-size: 7px;width: 10px" data-order = "d15">15</th><th style="font-size: 7px;width: 10px" data-order = "d16">16</th>
                        		<th style="font-size: 7px;width: 10px" data-order = "d17">17</th><th style="font-size: 7px;width: 10px" data-order = "d18">18</th><th style="font-size: 7px;width: 10px" data-order = "d19">19</th><th style="font-size: 7px;width: 10px" data-order = "d20">20</th>
                        		<th style="font-size: 7px;width: 10px" data-order = "d21">21</th><th style="font-size: 7px;width: 10px" data-order = "d22">22</th><th style="font-size: 7px;width: 10px" data-order = "d23">23</th>
                        		<th style="font-size: 7px;width: 10px" data-order = "d24">24</th><th style="font-size: 7px;width: 10px" data-order = "d25">25</th><th style="font-size: 7px;width: 10px" data-order = "d26">26</th>
                        		<th style="font-size: 7px;width: 10px" data-order = "d27">27</th><th style="font-size: 7px;width: 10px" data-order = "d28">28</th><th style="font-size: 7px;width: 10px" data-order = "d29">29</th><th style="font-size: 7px;width: 10px" data-order = "d30">30</th>
                        		<th style="font-size: 7px;width: 10px" data-order = "d31">31</th>
                        		<th data-order = "tdc" style="font-size: 8px;">Mes</th>
                        		</tr>
                        	</thead>
                        	<tbody style="font-size: 8px;">                        		
                        	</tbody>
                        	                        	
                    	</table>
                	</span>
                	
             	</div>  
           <div style="margin-top: -20px; font-size: 10px"><input type="hidden" size="3%" name="idcd" id="idcd">
            	Día de Corte:<input size="10%" type="text" name="feccd" id="feccd" class="fecha redondo mUp" readonly="true" style="text-align: center; background-color: green; color: white;" >
				Cantidad:<input size="6%"  type="text" name="cancd" id="cancd" style="text-align: center; background-color: green; color: white">
				<input style="font-size: 14px" type="submit" id="agrega" name="agrega" value="+" title="AGREGAR y ACTUALIZAR CORTE" />
				<input style="font-size: 14px" type="submit" id="quita" name="quita" value="-" title="QUITAR CORTE" />
			</div>
 		</div>
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#OrigenMes").change( function(){
	$("#feccd").change();
 return true;
})
$("#cmbMesT").change( function(){
	var f = new Date();
	$("#feccd").val(f.getFullYear() + "-" + ($("#cmbMesT").val()) + "-" + f.getDate());
	$("#feccd").change();
 return true;
})
$("#feccd").change( function(){
	//busca la cantidad de acuerdo al dia y el tipo de postlarva
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/programa/buscarcd", 
			data: "fec="+$("#feccd").val()+"&tip="+$("#OrigenMes").val()+"&cic="+$("#cmbCiclo").val(),
			success: resultado,	
	});	
});

function resultado (datos) {
  obj=null;
  var obj = jQuery.parseJSON( datos );
  if(obj!=null){
  	 //alert(msg);
  	if(obj.can != ''){
  		//alert(obj.can);
  		$("#cancd").val(obj.can);	
  		$("#idcd").val(obj.id);
  		return;
  	}else{
  		$("#cancd").val('');
  		$("#idcd").val('');
  		return;
  	}
  }
};
$("#quita").click( function(){	
	numero=$("#idcd").val();can=$("#cancd").val();
	if(numero!='' && can!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/programa/quitarcor", 
						data: "id="+$("#idcd").val()+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaTM").trigger("update");
										$("#cancd").val('');
   										$("#idcd").val('');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: intenta eliminar Corte Inexistente");
		return false;
	}
});
$("#agrega").click( function(){	
	can=$("#cancd").val();
	numero=$("#idcd").val();
	 if(can!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/programa/actualizarcor", 
						data: "id="+$("#idcd").val()+"&fec="+$("#feccd").val()+"&can="+$("#cancd").val()+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaTM").trigger("update");
										$("#cancd").val('');
   										$("#idcd").val('');
   										$("#feccd").change();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/programa/agregarcor", 
						data: "&fec="+$("#feccd").val()+"&can="+$("#cancd").val()+"&cic="+$("#cmbCiclo").val()+"&tip="+$("#OrigenMes").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaTM").trigger("update");
										$("#cancd").val('');
   										$("#idcd").val('');
   										$("#feccd").change();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		
		}
	}else{
		alert("Error: Necesita Registrar La Cantidad Actual de Corte");
		$("#cancd").focus();
		return false;
	}
	
});

$("#fecpd").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		//$("#feca").val( selectedDate );
		$("#mytablaProg").trigger("update");		
	}
});
$("#fecSi").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		//$("#feca").val( selectedDate );
		$("#mytablaSalas").trigger("update");		
	}
});
$("#feccd").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#feca").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

$("#fecs").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		var dias=0;
		dias=$("#dias").val();
		dias=(24*60*60*1000*dias);
		var p = new Date( (Date.parse( selectedDate )) + dias);
		
		var curr_date = p.getDate();
		var curr_month = p.getMonth();
		curr_month++;
		var curr_year = p.getFullYear();
		
		var newdate = curr_year + "-"	+ curr_month + "-" + curr_date;
		
		$("#fect").datepicker( "setDate", newdate);	
		$("#fect").datepicker( "option", "minDate", newdate );
	}	
});
$("#fect").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#fecs").datepicker( "option", "maxDate", selectedDate );
		//$("#mytablaSalas").trigger("update");	
	}	
});	
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

//PROGRAMACION DIA
$("#pnuevo").click( function(){
	$("#idpd").val('');$("#idcte").val('');$("#canpd").val('');$("#porpd").val('5.00');$("#obspd").val('');
	$("#cliente").val('');$("#ubigra").val('');$("#con").val('');$("#tel").val(''); //$("#cmbOrigen").val('');
 return true;
})
$("#Origen").change( function(){
	$("#OrigenMes").val($("#Origen").val());
	$("#cmbOrigen").val($("#Origen").val());
	$("#mytablaTM").trigger("update");
 return true;
})
$("#pborrar").click( function(){	
	numero=$("#idpd").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/programa/borrara", 
						data: "id="+$("#idpd").val()+"&cic="+$("#Ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Cliente Eliminado de Programación");
										$("#pnuevo").click();
										$("#mytablaProg").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Cliente para poder Eliminarlo");
		return false;
	}
});
$("#paceptar").click( function(){		
	cli=$("#cliente").val();
	numero=$("#idpd").val();
	if( cli!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/programa/actualizara", 
						data: "id="+$("#idpd").val()+"&fec="+$("#fecpd").val()+"&idcte="+$("#idcte").val()+"&can="+$("#canpd").val()+"&por="+$("#porpd").val()+"&obs="+$("#obspd").val()+"&tip="+$("#cmbOrigen").val()+"&cic="+$("#Ciclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#pnuevo").click();
										$("#mytablaProg").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/programa/agregara", 
						data: "fec="+$("#fecpd").val()+"&idcte="+$("#idcte").val()+"&can="+$("#canpd").val()+"&por="+$("#porpd").val()+"&obs="+$("#obspd").val()+"&tip="+$("#cmbOrigen").val()+"&cic="+$("#Ciclo").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Entrega registrada correctamente");
										$("#pnuevo").click();
										$("#mytablaProg").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}	
	}else{
		alert("Error: Seleccione Cliente de la Relación");	
		return false;
	}
});
//PROGRAMACION SALAS
$("#snuevo").click( function(){
	$("#mytablaSalas").trigger("update");
	$("#ids").val('');$("#sala").val('');$("#cans").val(''); $("#cor").val('');
	$("#fecs").val('');$("#fect").val('');//$("#origenes").val('');
 return true;
})
$("#origens").change( function(){
	$("#origenes").val($("#origens").val());
	//$("#cmbOrigen").val($("#Origen").val());
	$("#mytablaSalas").trigger("update");
	$("#snuevo").click();
 return true;
})
$("#origenes").change( function(){
	$("#origens").val($("#origenes").val());
	//$("#cmbOrigen").val($("#Origen").val());
	$("#mytablaSalas").trigger("update");
	
 return true;
})
$("#sborrar").click( function(){	
	numero=$("#ids").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/programa/borraras", 
						data: "id="+$("#ids").val()+"&cic="+$("#CicloS").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Sala Eliminada de Programación");
										$("#snuevo").click();
										$("#mytablaSalas").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Sala para poder Eliminarlo");
		return false;
	}
});
$("#saceptar").click( function(){		
	sala=$("#sala").val();ori=$("#origenes").val();cor=$("#cor").val();fecs=$("#fecs").val();fect=$("#fect").val();
	numero=$("#ids").val();
	if( sala>0){
	 if( ori!='Todos'){	
	 if( cor!=''){	
	 if( fecs!=''){
	  if( fect!=''){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/programa/actualizaras", 
						data: "id="+$("#ids").val()+"&fecs="+$("#fecs").val()+"&fect="+$("#fect").val()+"&can="+$("#cans").val()+"&cor="+$("#cor").val()+"&sala="+$("#sala").val()+"&ori="+$("#origenes").val()+"&cic="+$("#CicloS").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#snuevo").click();
										$("#mytablaSalas").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/programa/agregaras", 
						data: "fecs="+$("#fecs").val()+"&fect="+$("#fect").val()+"&can="+$("#cans").val()+"&cor="+$("#cor").val()+"&sala="+$("#sala").val()+"&ori="+$("#origenes").val()+"&cic="+$("#CicloS").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Sala registrada correctamente");
										$("#snuevo").click();
										$("#mytablaProg").trigger("update");
										$("#mytablaTM").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
	  }else{
		alert("Error: Seleccione Fecha de Transferencia");
		$("#fect").focus();	
		return false;
	  }		
	 }else{
		alert("Error: Seleccione Fecha de Siembra");
		$("#fecs").focus();	
		return false;
	 }	
	 }else{
		alert("Error: Registre Corrida");
		$("#cor").focus();	
		return false;
	 }	
	  }else{
		alert("Error: Seleccione Origen de Postlarva");
		$("#origenes").focus();	
		return false;
	 }			
	}else{
		alert("Error: Seleccione Sala");
		$("#sala").focus();	
		return false;
	}
});



        


$(document).ready(function(){ 
	var f = new Date();
	$("#fecpd").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());	
	$("#feccd").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#feccd").change();
	$("#cmbMesT").val((f.getMonth() +1));	
	
	$("#tabla_prog").ajaxSorter({
		url:'<?php echo base_url()?>index.php/programa/tablaprograma',  
		filters:['Ciclo','fecpd','Origen'],
		//multipleFilter:true, 
   		sort:false,	
   		onRowClick:function(){
   			if($(this).data('idpd')>0){
   				$("#cliente").val($(this).data('razon'));$("#idcte").val($(this).data('idcte'));$("#cmbOrigen").val($(this).data('tipopl'));
   				$("#idpd").val($(this).data('idpd'));$("#canpd").val($(this).data('canpd'));$("#porpd").val($(this).data('porpd'));
   				$("#ubigra").val($(this).data('ubigra'));$("#con").val($(this).data('con'));$("#tel").val($(this).data('tel'));$("#obspd").val($(this).data('obspd'));   				
   			}
   		},	
   		onSuccess:function(){   						
			$('#tabla_prog tbody tr td:nth-child(1)').map(function(){$(this).css('background','lightgray'); $(this).css('font-weight','bold');})
			$('#tabla_prog tbody tr td:nth-child(3)').map(function(){$(this).css('text-align','center'); $(this).css('font-weight','bold');})
				    		
			
	    	$('#tabla_prog tr').map(function(){	    		
	    		if($(this).data('razon')=='Total:'){
	    			$(this).css('background','lightgray');
	    		}
	    	});
	    	$('#tabla_prog tr').map(function(){	    		
	    		if($(this).data('tipopl1')=='Total Día:'){
	    			$(this).css('background','lightgreen');
	    		}
	    	});
	    },
   	});	
   	$("#tabla_cli").ajaxSorter({
		url:'<?php echo base_url()?>index.php/programa/tablacli',
		sort:false,
		multipleFilter:true,
		onRowClick:function(){
    	 		$("#idcte").val($(this).data('numero'));
				$("#cliente").val($(this).data('razon'));
				$("#ubigra").val($(this).data('ubigra'));$("#con").val($(this).data('con'));$("#tel").val($(this).data('tel'));
				$("#canpd").focus();
		},
	});
	$("#tabla_detmes").ajaxSorter({
		url:'<?php echo base_url()?>index.php/programa/tablaprogmes',
		filters:['cmbCiclo','cmbMesT','OrigenMes'],	
		sort:false,	
		onSuccess:function(){   						
			//ilumina el dia actual del año en curso
			if($("#cmbMesT").val()==(f.getMonth() +1)){
				$('#tabla_detmes tbody tr td:nth-child('+((f.getDate())+1)+')').map(function(){$(this).css('font-weight','bold');$(this).css('background','lightblue');})
			}
			
			//ilumina los clientes de la fecha actual
	    	$('#tabla_detmes tr').map(function(){	    		
	    		if($(this).data('d'+((f.getDate())))!='' && $(this).data('razon')!='' && $("#cmbMesT").val()==(f.getMonth() +1)){
	    			$(this).css('background','lightblue');
	    		}
	    		
	    	});
			$('#tabla_detmes tr').map(function(){	    		
	    		/*if($(this).data('razon')=='Programado'){
	    			$(this).css('font-weight','bold');
	    			$(this).css('background','Orange'); #fad42e
	    		}*/
	    		if($(this).data('razon')=='Programado'|| $(this).data('razon')=='Remisionado' || $(this).data('razon')=='Deficit o Superavit'){
	    			$(this).css('font-weight','bold');
	    			//$(this).css('background','#fad42e');$(this).css('opacity','.75');
	    			$(this).css('background','lightcoral');//$(this).css('opacity','.75');
	    		}
	    		if($(this).data('razon')=='Fecha de siembra'){
	    			$(this).css('background','lightblue');
	    			$(this).css('font-size','6px'); 
	    		}
	    		if($(this).data('razon')=='Sala' || $(this).data('razon')=='Corrida' || $(this).data('razon')=='Producción Estimada' || $(this).data('razon')=='Producción menos entrega'){
	    			$(this).css('background','lightgray');
	    			//if($(this).data('razon')=='Fecha de siembra'){ $(this).css('font-size','6px'); }
	    		}
	    		if($(this).data('razon')=='Cliente' || $(this).data('razon')=='Corte'){
	    			$(this).css('background','lightblue');
	    		}
	    	});	    		
			$('#tabla_detmes tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');$(this).css('font-size','8px');})
			$('#tabla_detmes tbody tr td:nth-child(33)').map(function(){$(this).css('text-align','right'); $(this).css('font-weight','bold');})
	    	/*$('#tabla_detmes tr').map(function(){
				if($(this).data('razon')=='Cliente'){
	    			$(this).css('position','fixed');
	    			$(this).css('display','block');
	    			$(this).css('margin-top','-40px');
	    		}
			});*/
	    },
	});
	
         
	$("#tabla_sala").ajaxSorter({
		url:'<?php echo base_url()?>index.php/programa/tablasalas',  
		filters:['CicloS','origens'],
		
		//multipleFilter:true, 
   		sort:false,	
   		onRowClick:function(){
   			if($(this).data('ids')>0){
   				$("#ids").val($(this).data('ids'));$("#sala").val($(this).data('sala'));
   				$("#origenes").val($(this).data('origens'));$("#cor").val($(this).data('corrida'));
   				$("#cans").val($(this).data('cans'));$("#fecs").val($(this).data('fecs'));$("#fect").val($(this).data('fect'));
   				$("#dias").val($(this).data('dia'));
   			}
   		},	
   		onSuccess:function(){
   			tot=0;
   			
   			$('#tabla_sala tr').map(function(){	$(this).css('text-align','center');	});
   			if($("#origenes").val()!='Todos' || $("#origens").val()!='Todos'){
   				$('#tabla_sala tbody tr').map(function(){
	    			if($(this).data('corrida')>0){tot+=1;}
    	   			if(tot>0){ $("#cor").val(tot+1); } 
	    		});  
	    	} else{
	    		$("#cor").val('');
	    	}						
			/*$('#tabla_prog tbody tr td:nth-child(1)').map(function(){$(this).css('background','lightgray'); $(this).css('font-weight','bold');})
			$('#tabla_prog tbody tr td:nth-child(3)').map(function(){$(this).css('text-align','center'); $(this).css('font-weight','bold');})
				    		
			
	    	
	    	$('#tabla_prog tr').map(function(){	    		
	    		if($(this).data('tipopl1')=='Total Día:'){
	    			$(this).css('background','lightgreen');
	    		}
	    	});*/
	    },
   	});
   	
   	
   	$("#tabla_ras1").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini1','fin1'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(1);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras1 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    }, 
	});
	$("#tabla_ras2").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini2','fin2'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(2);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras2 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    },
	});
	$("#tabla_ras3").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini3','fin3'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(3);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras3 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    },
	});
	$("#tabla_ras4").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini4','fin4'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(4);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras4 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    },	
	});
	$("#tabla_ras5").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablaras',
		filters:['hoy','ini5','fin5'],
		sort:false,	
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').show();
        	$("#id").val($(this).data('numpila'));
        	$("#pila").val($(this).data('nombre'));
       		$("#txtFecha").val($(this).data('fecsie'));
       		$("#pl").val($(this).data('pl'));
       		$("#can").val($(this).data('cansie'));
       		$("#cancela").val($(this).data('cancelacion'));
       		$("#tablasel").val(5);
       		ser=$(this).data('cancelacion');
			if(ser==-1 || ser==0 ){
				if(ser==-1){ $('input:radio[name=rblugar]:nth(0)').attr('checked',true); }
				if(ser==0){ $('input:radio[name=rblugar]:nth(1)').attr('checked',true); }
				
			}else{
				$('input:radio[name=rblugar]:nth(0)').attr('checked',false);$('input:radio[name=rblugar]:nth(1)').attr('checked',false);
				 
			}
       	},
       	onSuccess:function(){
    		$('#tabla_ras5 tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    },
	});
	$("#tabla_ras6").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablarasA',
		filters:['hoy'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').hide();
        },
		onSuccess:function(){    
    		$('#tabla_ras6 tbody tr').map(function(){
	    		if($(this).data('pl1')=='Tot:'){	    				    			
	    			$(this).css('background','lightblue');
	    		}
	    	});	
	    	$('#tabla_ras6 tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
    	}, 	
	});
	$("#tabla_ras7").ajaxSorter({
		url:'<?php echo base_url()?>index.php/produccion/tablarasAT',
		filters:['hoy'],
		sort:false,
		onRowClick:function(){
			$(this).removeClass('used');
			$('#ver_mas_pila').hide();
        },
		onSuccess:function(){    
    		$('#tabla_ras7 tbody tr').map(function(){
	    		if($(this).data('pl1')=='Total:'){	    				    			
	    			$(this).css('background','lightblue');
	    		}
	    	});	
	    	$('#tabla_ras7 tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })
    	}, 
	}); 
	 $('#mytablaTM1').fixheadertable({
             /**
      Set a title to your table (param : string, default : ' ')
     */
     caption        : '',
     /**
      Enable/disable the toggle display button (param : boolean, default : true)
      (works only with the caption option)
     */
     showhide       : true,
     /**
      Class name for add/modify theme (param : string, default : 'ui')
     */
     theme          : 'ui',
     /**
      Height of the table (param : integer, default : null)
     */
     height         : 360,
     /**
      Width of the table (param : integer, default : null)
     */
     width          : null,
     /**
      The minimum width of the table before horizontal scrolling (param : integer, default : null)
      (override the minWidthAuto option)
     */
     minWidth       : null,
     /**
      The minimum width calculated by the plugin (param : integer, default : null)
     */
     minWidthAuto   : false,
     /**
       Set the width of each column in pixel. (param : Array, default : [])
       The array must contain as much element as columns in your table, and
       each value must be an integer
     */
     colratio       : [],
     /**
       The CSS whiteSpace property applied to the table's cells (param : string, default : 'nowrap')
       values : 'nowrap' | 'normal'
     */
     whiteSpace     : 'nowrap',
     /**
       Add title bubbles when hovering cells (param : boolean, default : false)
     */
     addTitles      : false,
     /**
       Alternate row style (param : boolean, default : false)
     */
     zebra          : false,
     /**
       Set the CSS class applied alternatively (param : string, default : 'ui-state-active')
       (works with the zebra option)
     */
     zebraClass     : 'ui-state-active',
     /**
       Make your columns sortable (param : boolean, default : false)
       (all the columns will be sortable)
     */
     sortable       : false,
     /**
       Sort a column by default (param : integer, default : null)
     */
     sortedColId    : null,
     /**
       Set the sort callback of each column (param : Array, default : [])
       (if the array is empty, the default callback will be string)
       The array must contain as much element as columns in your table, and
       each value must be an string
       Available callbacks : 'string' | 'integer' | 'float' | 'date'
     */
     sortType       : [],
     /**
       The date format for the sort callback (param : string, default : 'd-m-y')
     */
     dateFormat     : 'd-m-y',
     /**
       Enable/Disable the pager (param : boolean, default : false)
     */
     pager          : false,
     /**
        The number of row to display (param : integer, default : 10)
        (works with the pager) allowed values : 10 | 25 | 50 | 100
     */
     rowsPerPage    : 10,
     /**
       Enable/Disable the column resizing (param : boolean, default : false)
     */
     resizeCol      : true,
     /**
       The minimum width in pixel of a resizable column (param : integer, default : 100)
     */
     minColWidth    : 100,
     /**
       Just wrap the table with a container (param : boolean, default : true)
     */
     wrapper        : true
        });  	
});  	
</script>
