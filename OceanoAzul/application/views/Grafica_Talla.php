<?php // content="text/plain; charset=utf-8"
require_once ('assets/jpgraph/src/jpgraph.php');
require_once ('assets/jpgraph/src/jpgraph_line.php');
/*$td=$_GET['datos'];	
$d1=$_GET['d1'];$d2=$_GET['d2'];$d3=$_GET['d3'];$d4=$_GET['d4'];$d5=$_GET['d5'];$d6=$_GET['d6'];$d7=$_GET['d7'];
$d8=$_GET['d8'];$d9=$_GET['d9'];$d10=$_GET['d10'];

// Some data
switch ($td){
	case 1:{$ydata = array($d1,$d1);break;}
	case 2:{$ydata = array($d1,$d2);break;}
	case 3:{$ydata = array($d1,$d2,$d3);break;}
	case 4:{$ydata = array($d1,$d2,$d3,$d4);break;}
	case 5:{$ydata = array($d1,$d2,$d3,$d4,$d5);break;}
	case 6:{$ydata = array($d1,$d2,$d3,$d4,$d5,$d6);break;}
	case 7:{$ydata = array($d1,$d2,$d3,$d4,$d5,$d6,$d7);break;}
	case 8:{$ydata = array($d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8);break;}
	case 9:{$ydata = array($d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9);break;}
	case 10:{$ydata = array($d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9,$d10);break;}
}*/
$ydata = array(11,3,8,12,5,1,9,13,5,10,7,8.5,);


// Create the graph. These two calls are always required
$graph = new Graph(845,250,"auto");
$graph->SetScale('textlin');
$graph->img->SetAntiAliasing();
$graph->xgrid->Show();


// Create the linear plot
$lineplot=new LinePlot($ydata);
$lineplot->SetColor('blue');
$lineplot->SetWeight(2);
//$lineplot->SetLegend('grs');

//Setup margin and titles
$graph->img->SetMargin(40,20,20,40);
$graph->title->Set('Crecimiento');
$graph->xaxis->title->Set('Semana');
$graph->yaxis->title->Set('Talla (grs)');
$graph->ygrid->SetFill(true,'#EFEFEF@0.5','#F9BB64@0.5');
$graph->SetShadow();

// Add the plot to the graph
$graph->Add($lineplot);

// Display the graph
$graph->Stroke();
?>
