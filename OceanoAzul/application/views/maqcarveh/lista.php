<?php $this->load->view('header_cv');?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 20px;		
}   
</style>
<div style="height:570px; ">
<div class="container" id="principal" style="background-color:#FFFFFF; height: 570px;">	
	<ul class="tabs">
		<strong>
		<li style="font-size: 20px"><a href="#general"><img src="<?php echo base_url();?>assets/images/menu/kpi.png" width="25" height="25" border="0"> KPI</a></li>
		<?php if($usuario=="Indelfonso Cota" ){ ?>
		<li style="font-size: 20px"><a href="#compara"><img src="<?php echo base_url();?>assets/images/menu/bomba.png" width="25" height="25" border="0"> Mantenimiento</a></li>
		<?php }else{ ?>
		<li style="font-size: 20px"><a href="#limpieza">Limpieza</a></li>
		<?php }?>
		 Ciclo:
			 <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px;margin-top: 1px; margin-left:4px;" >
               	<?php $ciclof=21; $actual=date("y"); //$actual+=1;
				while($actual >= $ciclof){?>
					<option value="<?php echo $actual;?>" > <?php echo '20'.$actual;?> </option>
            	<?php  $actual-=1; } ?>
          	</select>
          	
   			Día: <input size="10%" type="text" name="fecha" id="fecha" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >		
          	
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="general" class="tab_content" style="height: 560px;"  >	
			<table border="0" style="margin-left: -5px; margin-top: 1px; height: 560px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_eva" name="tabla_eva" style="width: 950px; margin-top: -20px;"  >                 			
							<span class="ajaxTable" style="height: 550px;" >
                    			<table id="mytablakpi" name="mytablakpi" class="ajaxSorter" border="1" >
                       				<thead >
                       					<th data-order = "desmcv">Des</th>
                       					<th data-order = "nom">Eva</th>
				                       		<?php	
				          						$cont=0;
												while($cont<31){ $cont+=1; if($cont<=9) $cero='0'; else $cero='';?>
				           							<th style="font-size: 9px" data-order ="<?php echo "d".$cont;?>" ><?php echo $cero.$cont;?></th>
				           					<?php } ?>
                       				</thead>
                       				<tbody style="font-size: 9px">
                       				</tbody>                        	
                   				</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tablesg" action="<?= base_url()?>index.php/maqcarveh/pdfmes" method="POST" >							
    	    	            		<ul class="order_list" style="width:380px; margin-top: 1px">
    	    	            			Mes:
						   					<select name="cmbMeskpi" id="cmbMeskpi" style="font-size: 10px;height: 23px;  margin-top: 1px; margin-left:4px;">   						
						   						<option value="1" >Enero</option><option value="2" >Febrero</option><option value="3" >Marzo</option>
						   						<option value="4" >Abril</option><option value="5" >Mayo</option><option value="6" >Junio</option>
						   						<option value="7" >Julio</option><option value="8" >Agosto</option><option value="9" >Septiembre</option>
						   						<option value="10" >Octubre</option><option value="11" >Noviembre</option>
						   						<option value="12" >Diciembre</option>
						   					</select>
						   			Categoria:

								          	<select name="categoria" id="categoria" style="font-size: 11px;height: 25px;  margin-top: 1px;">
								          		<?php if($usuario=="Indelfonso Cota" ){ ?>
								            	<option value="1">Maquinaria</option>
								            	<option value="2">Cárcamo</option>
								            	<option value="3">Vehículo</option>
									            <?php }else{ ?>
									            <option value="4">Limpieza</option>	
									            <?php }?>	
								          	</select>	
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablaeva" value ="" class="htmlTable"/> <input type="hidden" id="ciclorep" name="ciclorep" /> 
								</form>   
                			</div> 
                		</div>
	        		</th>
            		</tr>
     		</table>	
		</div>
		<?php if($usuario=="Indelfonso Cota" ){ ?>
		<div id="compara" class="tab_content" style="height: 560px;" >	
			<table border="1" style="margin-left: -5px; margin-top: 1px; height: 560px; width: 950px;">
				<tr>
				 	<td colspan="5">				 		
                 		<div class="ajaxSorterDiv" id="tabla_maq" name="tabla_maq" style="margin-top: 1px"  >                 		<span class="ajaxTable" style="height: 374px; width: 300px; " >
                    			<table id="mytablaMaq" name="mytablaMaq" class="ajaxSorter" border="1" style="width: 298px" >
                        			<thead>   
                        				<tr>
                            				<th data-order = "desmcv" >Maquinaria</th>
                            				<th data-order = "motmaq" >1<br /> Mot</th>
                            				<th data-order = "tramaq" >2<br /> Tra</th>
                            				<th data-order = "shimaq" >3<br /> Hid</th>
                            				<th data-order = "susmaq" >4<br /> Sus</th>
                            			</tr>		
                            		</thead>
                        			<tbody title="Seleccione para visualizar detalle" style="font-size: 12px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
	                	</div>
	               </td>
	                <td colspan="5">
	                	<div class="ajaxSorterDiv" id="tabla_car" name="tabla_car" style="margin-top: 1px"  >                 	<span class="ajaxTable" style="height: 374px; width: 300px; " >
                    			<table id="mytablaCar" name="mytablaCar" class="ajaxSorter" border="1" style="width: 292px" >
                        			<thead>   
                        				<tr>
                            				<th data-order = "desmcv" >Cárcamo</th>
                            				<th data-order = "motcar" >1<br /> Mot</th>
                            				<th data-order = "tracar" >2<br /> Tra</th>
                            				<th data-order = "bomcar" >3<br /> Bom</th>
                            			</tr>		
                            		</thead>
                        			<tbody title="Seleccione para visualizar detalle" style="font-size: 12px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
	                	</div>
	                </td>
	                <td colspan="5">
	                	<div class="ajaxSorterDiv" id="tabla_veh" name="tabla_veh" style="margin-top: 1px"  >                 	<span class="ajaxTable" style="height: 374px; width: 300px; " >
                    			<table id="mytablaVeh" name="mytablaVeh" class="ajaxSorter" border="1" style="width: 295px" >
                        			<thead>   
                        				<tr>
                            				<th data-order = "desmcv" >Vehículo</th>
                            				<th data-order = "motveh" >1<br /> Mot</th>
                            				<th data-order = "traveh" >2<br /> Tra</th>
                            				<th data-order = "shiveh" >3<br /> Hid</th>
                            				<th data-order = "susveh" >4<br /> Sus</th>
                            			</tr>		
                            		</thead>
                        			<tbody title="Seleccione para visualizar detalle" style="font-size: 12px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
	                	</div>
	                </td>		
	             </tr> 
	             <tr style="background-color: lightblue; font-weight: bold;">
	             	<th colspan="5">Maquinaria<input type="hidden" id="idmaq" name="idmaq" /></th>
	             	<th colspan="5">Cárcamo<input type="hidden" id="idcar" name="idcar" /></th>
	             	<th colspan="5">Vehículo<input type="hidden" id="idveh" name="idveh" /></th>
	             </tr> 
	             <tr>
	             	<td rowspan="4"><input type="hidden" id='desmcv' name="desmcv" style="font-size: 16px; width: 120px; border: none; margin-left: -5px;; text-align: center" />
	             		<select name="idmcv" id="idmcv" style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<option value="0" >Sel.</option>
          					<?php	
           						$cat='Maquinaria';
           						$this->load->model('maqcarveh_model'); 
								$data['result']=$this->maqcarveh_model->maqcarvehCat($cat); 
								foreach ($data['result'] as $row):  ?> 
           							<option value="<?php echo $row->idemcv;?>" ><?php echo $row->desmcv;?></option>
           						<?php endforeach;?>
   						</select>
	             	</td>
	                <td >1.- Motor <input size="2%" type="hidden" name="motmaq" id="motmaq" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rmotmaq" type="radio" value="1" onclick="radios(11,1)" /></td>
					<td style="background-color: orange"><input  name="rmotmaq" type="radio" value="0" onclick="radios(11,0)" /></td>									
					<td style="background-color: red"><input  name="rmotmaq" type="radio" value="-1" onclick="radios(11,-1)" /></td>
	                <td rowspan="4"><input type="hidden" id='descmv' name="descmv" style="font-size: 16px; width: 120px; border: none; margin-left: -5px; text-align: center" />
	                	<select name="idcmv" id="idcmv" style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<option value="0" >Sel.</option>
          					<?php	
           						$cat='Carcamo';
           						$this->load->model('maqcarveh_model'); 
								$data['result']=$this->maqcarveh_model->maqcarvehCat($cat); 
								foreach ($data['result'] as $row):  ?> 
           							<option value="<?php echo $row->idemcv;?>" ><?php echo $row->desmcv;?></option>
           						<?php endforeach;?>
   						</select>
	                </td>
	                <td >1.- Motor <input size="2%" type="hidden" name="motcar" id="motcar" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rmotcar" type="radio" value="1" onclick="radios(21,1)" /></td>
					<td style="background-color: orange"><input  name="rmotcar" type="radio" value="0" onclick="radios(21,0)" /></td>									
					<td style="background-color: red"><input  name="rmotcar" type="radio" value="-1" onclick="radios(21,-1)" /></td>
	               <td rowspan="4"><input type="hidden" id='desvmc' name="desvmc" style="font-size: 16px; width: 120px; border: none; margin-left: -5px;; text-align: center" />
	               	<select name="idvmc" id="idvmc" style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<option value="0" >Sel.</option>
          					<?php	
           						$cat='Vehiculo';
           						$this->load->model('maqcarveh_model'); 
								$data['result']=$this->maqcarveh_model->maqcarvehCat($cat); 
								foreach ($data['result'] as $row):  ?> 
           							<option value="<?php echo $row->idemcv;?>" ><?php echo $row->desmcv;?></option>
           						<?php endforeach;?>
   						</select>
	               </td>
	                <td >1.- Motor <input size="2%" type="hidden" name="motveh" id="motveh" /></td>
	                <td style="background-color: #0F0;"><input checked="true"  name="rmotveh" type="radio" value="1" onclick="radios(31,1)" /></td>
					<td style="background-color: orange"><input  name="rmotveh" type="radio" value="0" onclick="radios(31,0)" /></td>									
					<td style="background-color: red"><input  name="rmotveh" type="radio" value="-1" onclick="radios(31,-1)" /></td>
	             </tr>
	             <tr>
	             	<td >2.- Trasmisión <input type="hidden" id='tramaq' name="tramaq" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rtramaq" type="radio" value="1" onclick="radios(12,1)" /></td>
					<td style="background-color: orange"><input  name="rtramaq" type="radio" value="0" onclick="radios(12,0)" /></td>									
					<td style="background-color: red"><input  name="rtramaq" type="radio" value="-1" onclick="radios(12,-1)" /></td>
					<td >2.- Trasmisión <input type="hidden" id='tracar' name="tracar" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rtracar" type="radio" value="1" onclick="radios(22,1)" /></td>
					<td style="background-color: orange"><input  name="rtracar" type="radio" value="0" onclick="radios(22,0)" /></td>									
					<td style="background-color: red"><input  name="rtracar" type="radio" value="-1" onclick="radios(22,-1)" /></td>
					<td >2.- Trasmisión <input type="hidden" id='traveh' name="traveh" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rtraveh" type="radio" value="1" onclick="radios(32,1)" /></td>
					<td style="background-color: orange"><input  name="rtraveh" type="radio" value="0" onclick="radios(32,0)" /></td>									
					<td style="background-color: red"><input  name="rtraveh" type="radio" value="-1" onclick="radios(32,-1)" /></td>
	             </tr>
	             <tr>
	             	<td >3.- Sist. Hidraúlico <input type="hidden" id='shimaq' name="shimaq" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rshimaq" type="radio" value="1" onclick="radios(13,1)" /></td>
					<td style="background-color: orange"><input  name="rshimaq" type="radio" value="0" onclick="radios(13,0)" /></td>									
					<td style="background-color: red"><input  name="rshimaq" type="radio" value="-1" onclick="radios(13,-1)" /></td>
					<td >3.- Bomba <input type="hidden" id='bomcar' name="bomcar" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rbomcar" type="radio" value="1" onclick="radios(23,1)" /></td>
					<td style="background-color: orange"><input  name="rbomcar" type="radio" value="0" onclick="radios(23,0)" /></td>									
					<td style="background-color: red"><input  name="rbomcar" type="radio" value="-1" onclick="radios(23,-1)" /></td>
					<td >3.- Sist. Hidraúlico <input type="hidden" id='shiveh' name="shiveh" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rshiveh" type="radio" value="1" onclick="radios(33,1)" /></td>
					<td style="background-color: orange"><input  name="rshiveh" type="radio" value="0" onclick="radios(33,0)" /></td>									
					<td style="background-color: red"><input  name="rshiveh" type="radio" value="-1" onclick="radios(33,-1)" /></td>
	             </tr>
	             <tr>
	             	<td >4.- Suspensión <input type="hidden" id='susmaq' name="susmaq" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rsusmaq" type="radio" value="1" onclick="radios(14,1)" /></td>
					<td style="background-color: orange"><input  name="rsusmaq" type="radio" value="0" onclick="radios(14,0)" /></td>									
					<td style="background-color: red"><input  name="rsusmaq" type="radio" value="-1" onclick="radios(14,-1)" /></td>
					<td colspan="4"></td>
					<td >4.- Suspensión <input type="hidden" id='susveh' name="susveh" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rsusveh" type="radio" value="1" onclick="radios(34,1)" /></td>
					<td style="background-color: orange"><input  name="rsusveh" type="radio" value="0" onclick="radios(34,0)" /></td>									
					<td style="background-color: red"><input  name="rsusveh" type="radio" value="-1" onclick="radios(34,-1)" /></td>
			     </tr>
			     <?php if($usuario=="Indelfonso Cota"){ ?>
			     <tr>
			     	<td colspan="5" style="text-align: center">
	                		<input style="font-size: 12px" type="submit" id="aceptarmaq" name="aceptarmaq" value="Guardar" />
							&nbsp;&nbsp;<input style="font-size: 12px" type="submit" id="nuevomaq" name="nuevomaq" value="Nuevo" />
							&nbsp;&nbsp;<input style="font-size: 12px" type="submit" id="borrarmaq" name="borrarmaq" value="Borrar" />
				    </td>
	                <td colspan="5" style="text-align: center">
	                		<input style="font-size: 12px" type="submit" id="aceptarcar" name="aceptarcar" value="Guardar" />
	                		&nbsp;&nbsp;<input style="font-size: 12px" type="submit" id="nuevocar" name="nuevocar" value="Nuevo" />
							&nbsp;&nbsp;<input style="font-size: 12px" type="submit" id="borrarcar" name="borrarcar" value="Borrar" />
				    </td>
	                <td colspan="5" style="text-align: center">
	                		<input style="font-size: 12px" type="submit" id="aceptarveh" name="aceptarveh" value="Guardar" />
	                		&nbsp;&nbsp;<input style="font-size: 12px" type="submit" id="nuevoveh" name="nuevoveh" value="Nuevo" />
							&nbsp;&nbsp;<input style="font-size: 12px" type="submit" id="borrarveh" name="borrarveh" value="Borrar" />
				    </td>
			     </tr>
			     <?php } ?>	
	        </table>	
		</div>
	<?php }else{ ?>
		<div id="limpieza" class="tab_content" style="height: 560px;" >	
			<table border="1" style="margin-left: -5px; margin-top: 1px; height: 560px; width: 950px;">
				<tr>
				 	<td  colspan="5">				 		
                 		<div class="ajaxSorterDiv" id="tabla_lim" name="tabla_lim" style="margin-top: 1px"  >               	  <div class="ajaxpager" style=" font-size: 12px; margin-top: 1px"> 
								<form id="data_tablesg" action="<?= base_url()?>index.php/maqcarveh/pdfdia" method="POST" >		<ul class="order_list" style="width:380px; margin-top: 1px">
    	    	            			Categoria:
										<select name="categorial" id="categorial" style="font-size: 11px;height: 25px;  margin-top: 1px;">
								               <option value="4">Limpieza</option>	
									   	</select>	
    	    	            		</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablaevadia" value ="" class="htmlTable"/> 
        	            	    	<input type="hidden" id="dia" name="dia" />
        	            	    	<input type="hidden" id="cicdia" name="cicdia" /> 
								</form>   
                			</div> 			
                 			<span class="ajaxTable" style="height: 374px; width: 900px; " >
                    			<table id="mytablaLim" name="mytablaLim" class="ajaxSorter" border="1" style="width: 898px" >
                        			<thead>   
                        				<tr>
                            				<th data-order = "desmcv" >Departamento</th>
                            				<th data-order = "ordlim" >Orden</th>
                            				<th data-order = "limlim" >Limpieza</th>                            	
                            				<th data-order = "niclim" >Nivel de Cumplimiento (%)</th> 
                            				<th data-order = "obslim" > Observaciones</th>			
                            			</tr>		
                            		</thead>
                        			<tbody title="Seleccione para visualizar detalle" style="font-size: 12px;">
                        			</tbody>                        	
                    			</table>
                			</span> 
	                	</div>
	               </td>

	             </tr> 
	             <tr style="background-color: lightblue; font-weight: bold;">
	             	<th colspan="5">Limpieza<input type="hidden" id="idlim" name="idlim" /></th>
	             </tr> 
	             <tr>
	             	<td rowspan="3"><input type="hidden" id='deslim' name="deslim" style="font-size: 16px; width: 120px; border: none; margin-left: -5px;; text-align: center" />
	             		<select name="idol" id="idol" style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<option value="0" >Sel.</option>
          					<?php	
           						$cat='Limpieza';
           						$this->load->model('maqcarveh_model'); 
								$data['result']=$this->maqcarveh_model->maqcarvehCat($cat); 
								foreach ($data['result'] as $row):  ?> 
           							<option value="<?php echo $row->idemcv;?>" ><?php echo $row->desmcv;?></option>
           						<?php endforeach;?>
   						</select>
	             	</td>
	                <td >1.- Orden <input size="2%" type="hidden" name="ordlim" id="ordlim" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rordlim" type="radio" value="1" onclick="radios(41,1)" /></td>
					<td style="background-color: orange"><input  name="rordlim" type="radio" value="0" onclick="radios(41,0)" /></td>									
					<td style="background-color: red"><input  name="rordlim" type="radio" value="-1" onclick="radios(41,-1)" /></td>
	             </tr>
	             <tr>
	             	<td >2.- Limpieza <input type="hidden" id='limlim' name="limlim" /></td>
	                <td style="background-color: #0F0"><input checked="true"  name="rlimlim" type="radio" value="1" onclick="radios(42,1)" /></td>
					<td style="background-color: orange"><input  name="rlimlim" type="radio" value="0" onclick="radios(42,0)" /></td>									
					<td style="background-color: red"><input  name="rlimlim" type="radio" value="-1" onclick="radios(42,-1)" /></td>
	             </tr>
	             <tr>
	             	<td >3.- Observaciones </td>
	                <td colspan="3"><input type="text" id='obslim' name="obslim" style="width: 480px;" /></td>
	             </tr>
			     <?php if($usuario=="Edgar Rodriguez"){ ?>
			     <tr>
			     	<td colspan="5" style="text-align: center">
	                		<input style="font-size: 12px" type="submit" id="aceptarlim" name="aceptarlim" value="Guardar" />
							&nbsp;&nbsp;<input style="font-size: 12px" type="submit" id="nuevolim" name="nuevolim" value="Nuevo" />
							&nbsp;&nbsp;<input style="font-size: 12px" type="submit" id="borrarlim" name="borrarlim" value="Borrar" />
				    </td>
	                
			     </tr>
			     <?php } ?>	
	        </table>	
		</div>
	<?php } ?>	
	</div> 	
</div>
		
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
function radios(radio,dato){
	switch(radio){
		case 11: $("#motmaq").val(dato); break;
		case 12: $("#tramaq").val(dato); break;
		case 13: $("#shimaq").val(dato); break;
		case 14: $("#susmaq").val(dato); break;
		case 21: $("#motcar").val(dato); break;
		case 22: $("#tracar").val(dato); break;
		case 23: $("#bomcar").val(dato); break;
		case 31: $("#motveh").val(dato); break;
		case 32: $("#traveh").val(dato); break;
		case 33: $("#shiveh").val(dato); break;
		case 34: $("#susveh").val(dato); break;
		case 41: $("#ordlim").val(dato); break;
		case 42: $("#limlim").val(dato); break;
	}	
}
$("#nuevomaq").click( function(){	
	$("#idmaq").val('');$("#desmcv").val('');$("#motmaq").val('');$("#tramaq").val('');$("#shimaq").val('');$("#susmaq").val('');$("#idmcv").val('');
	$('input:radio[name=rmotmaq]:nth(0)').attr('checked',false);$('input:radio[name=rmotmaq]:nth(1)').attr('checked',false);$('input:radio[name=rmotmaq]:nth(2)').attr('checked',false);
	$('input:radio[name=rtramaq]:nth(0)').attr('checked',false);$('input:radio[name=rtramaq]:nth(1)').attr('checked',false);$('input:radio[name=rtramaq]:nth(2)').attr('checked',false);
	$('input:radio[name=rshimaq]:nth(0)').attr('checked',false);$('input:radio[name=rshimaq]:nth(1)').attr('checked',false);$('input:radio[name=rshimaq]:nth(2)').attr('checked',false);
	$('input:radio[name=rsusmaq]:nth(0)').attr('checked',false);$('input:radio[name=rsusmaq]:nth(1)').attr('checked',false);$('input:radio[name=rsusmaq]:nth(2)').attr('checked',false);
	return true;
});
$("#nuevocar").click( function(){	
	$("#idcar").val('');$("#descmc").val('');$("#motcar").val('');$("#tracar").val('');$("#bomcar").val('');$("#idcmv").val('');
	$('input:radio[name=rmotcar]:nth(0)').attr('checked',false);$('input:radio[name=rmotcar]:nth(1)').attr('checked',false);$('input:radio[name=rmotcar]:nth(2)').attr('checked',false);
	$('input:radio[name=rtracar]:nth(0)').attr('checked',false);$('input:radio[name=rtracar]:nth(1)').attr('checked',false);$('input:radio[name=rtracar]:nth(2)').attr('checked',false);
	$('input:radio[name=rbomcar]:nth(0)').attr('checked',false);$('input:radio[name=rbomcar]:nth(1)').attr('checked',false);$('input:radio[name=rbomcar]:nth(2)').attr('checked',false);
	return true;
});
$("#nuevoveh").click( function(){	
	$("#idveh").val('');$("#desvmc").val('');$("#motveh").val('');$("#traveh").val('');$("#shiveh").val('');$("#susveh").val('');$("#idvmc").val('');
	$('input:radio[name=rmotveh]:nth(0)').attr('checked',false);$('input:radio[name=rmotveh]:nth(1)').attr('checked',false);$('input:radio[name=rmotveh]:nth(2)').attr('checked',false);
	$('input:radio[name=rtraveh]:nth(0)').attr('checked',false);$('input:radio[name=rtraveh]:nth(1)').attr('checked',false);$('input:radio[name=rtraveh]:nth(2)').attr('checked',false);
	$('input:radio[name=rshiveh]:nth(0)').attr('checked',false);$('input:radio[name=rshiveh]:nth(1)').attr('checked',false);$('input:radio[name=rshiveh]:nth(2)').attr('checked',false);
	$('input:radio[name=rsusveh]:nth(0)').attr('checked',false);$('input:radio[name=rsusveh]:nth(1)').attr('checked',false);$('input:radio[name=rsusveh]:nth(2)').attr('checked',false);
	return true;
});
$("#nuevolim").click( function(){	
	$("#idlim").val('');$("#deslim").val('');$("#ordlim").val('');$("#limlim").val('');$("#idol").val('');
	$("#obslim").val('');
	$('input:radio[name=rordlim]:nth(0)').attr('checked',false);$('input:radio[name=rordlim]:nth(1)').attr('checked',false);$('input:radio[name=rordlim]:nth(2)').attr('checked',false);
	$('input:radio[name=rlimlim]:nth(0)').attr('checked',false);$('input:radio[name=rlimlim]:nth(1)').attr('checked',false);$('input:radio[name=rlimlim]:nth(2)').attr('checked',false);
	return true;
});

$("#borrarmaq").click( function(){	
	numero=$("#idmaq").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maqcarveh/borrar", 
						data: "id="+$("#idmaq").val()+"&tabla=man_maq_"+"&campo=idmaq"+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										$("#nuevomaq").click();
										$("#mytablaMaq").trigger("update");
										$("#mytablakpi").trigger("update");	
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Maquinaria para poder Eliminar sus datos");
		return false;
	}
});
$("#borrarcar").click( function(){	
	numero=$("#idcar").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maqcarveh/borrar", 
						data: "id="+$("#idcar").val()+"&tabla=man_car_"+"&campo=idcar"+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										$("#nuevocar").click();
										$("#mytablaCar").trigger("update");
										$("#mytablakpi").trigger("update");	
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Cárcamo para poder Eliminar sus datos");
		return false;
	}
});
$("#borrarveh").click( function(){	
	numero=$("#idveh").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maqcarveh/borrar", 
						data: "id="+$("#idveh").val()+"&tabla=man_veh_"+"&campo=idveh"+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										$("#nuevoveh").click();
										$("#mytablaVeh").trigger("update");
										$("#mytablakpi").trigger("update");	
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Vehículo para poder Eliminar sus datos");
		return false;
	}
});
$("#borrarlim").click( function(){	
	numero=$("#idlim").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maqcarveh/borrar", 
						data: "id="+$("#idlim").val()+"&tabla=man_lim_"+"&campo=idlim"+"&cic="+$("#cmbCiclo").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										$("#nuevolim").click();
										$("#mytablaLim").trigger("update");
										$("#mytablakpi").trigger("update");	
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar para poder Eliminar sus datos");
		return false;
	}
});

$("#aceptarmaq").click( function(){		
	fec=$("#fecha").val();numero=$("#idmaq").val();id=$("#idmcv").val();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/maqcarveh/buscardia", 
			data: "fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=1",
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
  					if(obj.dia!='0'){
						if( fec!=''){
						 if( id!='0'){
							if(numero!=''){
									$.ajax({
											type: "POST",//Envio
											url: "<?=base_url()?>index.php/maqcarveh/actualizarmcv", 
											data: "id="+$("#idmaq").val()+"&mot="+$("#motmaq").val()+"&tra="+$("#tramaq").val()+"&shi="+$("#shimaq").val()+"&sus="+$("#susmaq").val()+"&cic="+$("#cmbCiclo").val()+"&cat=1"+"&bom=0",
											success: 	
													function(msg){															
														if(msg!=0){														
															$("#nuevomaq").click();$("#mytablaMaq").trigger("update");$("#mytablakpi").trigger("update");																			
														}else{
															alert("Error con la base de datos o usted no ha actualizado nada");
														}					
													}		
									});
								}else{
									$.ajax({
											type: "POST",//Envio
											url: "<?=base_url()?>index.php/maqcarveh/agregarmcv", 
											data: "todos=2"+"&fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=1"+"&mcv="+$("#idmcv").val()+"&mot="+$("#motmaq").val()+"&tra="+$("#tramaq").val()+"&shi="+$("#shimaq").val()+"&sus="+$("#susmaq").val()+"&bom=0",
											success: 	
													function(msg){															
														if(msg!=0){														
															$("#nuevomaq").click();$("#mytablaMaq").trigger("update");$("#mytablakpi").trigger("update");																			
														}else{
															alert("Error con la base de datos o usted no ha actualizado nada");
														}					
													}		
									});
								}
							}else{
								alert("Error: Seleccione Maquinaria");$("#idmcv").focus();return false;
							}				
							}else{
								alert("Error: Seleccione Fecha de Captura");$("#fecha").focus();return false;
							}	
					}else{
						$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maqcarveh/agregarmcv", 
						data: "todos=1"+"&fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=1",
						success: 
								function(msg){															
									if(msg!=0){														
										$("#nuevomaq").click();$("#mytablaMaq").trigger("update");$("#mytablakpi").trigger("update");																					
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
						});
				  }					
   			} 
	});	
});

$("#aceptarcar").click( function(){		
	fec=$("#fecha").val();numero=$("#idcar").val();id=$("#idcmv").val();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/maqcarveh/buscardia", 
			data: "fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=2",
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
  					if(obj.dia!='0'){
						if( fec!=''){
						 if( id!='0'){
							if(numero!=''){
									$.ajax({
											type: "POST",//Envio
											url: "<?=base_url()?>index.php/maqcarveh/actualizarmcv", 
											data: "id="+$("#idcar").val()+"&mot="+$("#motcar").val()+"&tra="+$("#tracar").val()+"&shi=0"+"&sus=0"+"&cic="+$("#cmbCiclo").val()+"&cat=2"+"&bom="+$("#bomcar").val(),
											success: 	
													function(msg){															
														if(msg!=0){														
															$("#nuevocar").click();$("#mytablaCar").trigger("update");$("#mytablakpi").trigger("update");																			
														}else{
															alert("Error con la base de datos o usted no ha actualizado nada");
														}					
													}		
									});
								}else{
									$.ajax({
											type: "POST",//Envio
											url: "<?=base_url()?>index.php/maqcarveh/agregarmcv", 
											data: "todos=2"+"&fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=2"+"&mcv="+$("#idcmv").val()+"&mot="+$("#motcar").val()+"&tra="+$("#tracar").val()+"&shi=0"+"&sus=0"+"&bom="+$("#bomcar").val(),
											success: 	
													function(msg){															
														if(msg!=0){														
															$("#nuevocar").click();$("#mytablaCar").trigger("update");$("#mytablakpi").trigger("update");																			
														}else{
															alert("Error con la base de datos o usted no ha actualizado nada");
														}					
													}		
									});
								}
							}else{
								alert("Error: Seleccione Cárcamo");$("#idcmv").focus();return false;
							}				
							}else{
								alert("Error: Seleccione Fecha de Captura");$("#fecha").focus();return false;
							}	
					}else{
						$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maqcarveh/agregarmcv", 
						data: "todos=1"+"&fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=2",
						success: 
								function(msg){															
									if(msg!=0){														
										$("#nuevocar").click();$("#mytablaCar").trigger("update");$("#mytablakpi").trigger("update");																					
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
						});
				  }					
   			} 
	});	
});

$("#aceptarveh").click( function(){		
	fec=$("#fecha").val();numero=$("#idveh").val();id=$("#idvmc").val();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/maqcarveh/buscardia", 
			data: "fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=3",
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
  					if(obj.dia!='0'){
						if( fec!=''){
						 if( id!='0'){
							if(numero!=''){
									$.ajax({
											type: "POST",//Envio
											url: "<?=base_url()?>index.php/maqcarveh/actualizarmcv", 
											data: "id="+$("#idveh").val()+"&mot="+$("#motveh").val()+"&tra="+$("#traveh").val()+"&shi="+$("#shiveh").val()+"&sus="+$("#susveh").val()+"&cic="+$("#cmbCiclo").val()+"&cat=3"+"&bom=0",
											success: 	
													function(msg){															
														if(msg!=0){														
															$("#nuevoveh").click();$("#mytablaVeh").trigger("update");$("#mytablakpi").trigger("update");																			
														}else{
															alert("Error con la base de datos o usted no ha actualizado nada");
														}					
													}		
									});
								}else{
									$.ajax({
											type: "POST",//Envio
											url: "<?=base_url()?>index.php/maqcarveh/agregarmcv", 
											data: "todos=2"+"&fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=3"+"&mcv="+$("#idvmc").val()+"&mot="+$("#motveh").val()+"&tra="+$("#traveh").val()+"&shi="+$("#shiveh").val()+"&sus="+$("#susveh").val()+"&bom=0",
											success: 	
													function(msg){															
														if(msg!=0){														
															$("#nuevoveh").click();$("#mytablaVeh").trigger("update");$("#mytablakpi").trigger("update");																			
														}else{
															alert("Error con la base de datos o usted no ha actualizado nada");
														}					
													}		
									});
								}
							}else{
								alert("Error: Seleccione Vehículo");$("#idvmc").focus();return false;
							}				
							}else{
								alert("Error: Seleccione Fecha de Captura");$("#fecha").focus();return false;
							}	
					}else{
						$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maqcarveh/agregarmcv", 
						data: "todos=1"+"&fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=3",
						success: 
								function(msg){															
									if(msg!=0){														
										$("#nuevoveh").click();$("#mytablaVeh").trigger("update");$("#mytablakpi").trigger("update");																					
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
						});
				  }					
   			} 
	});	
});

$("#aceptarlim").click( function(){		
	fec=$("#fecha").val();numero=$("#idlim").val();id=$("#idol").val();
	$.ajax({
			type: "POST",//Envio
			url: "<?=base_url()?>index.php/maqcarveh/buscardia", 
			data: "fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=4",
			success: function(datos){
					obj=null;
  					var obj = jQuery.parseJSON( datos );
  					if(obj.dia>'0'){
						if( fec!=''){
						 if( id!='0'){
							if(numero!=''){
									$.ajax({
											type: "POST",//Envio
											url: "<?=base_url()?>index.php/maqcarveh/actualizarmcv", 
											data: "id="+$("#idlim").val()+"&mot="+$("#ordlim").val()+"&tra="+$("#limlim").val()+"&shi="+$("#obslim").val()+"&sus=0"+"&cic="+$("#cmbCiclo").val()+"&cat=4"+"&bom=0",
											success: 	
													function(msg){															
														if(msg!=0){														
															$("#nuevolim").click();$("#mytablaLim").trigger("update");$("#mytablakpi").trigger("update");																			
														}else{
															alert("Error con la base de datos o usted no ha actualizado nada");
														}					
													}		
									});
								}else{
									$.ajax({
											type: "POST",//Envio
											url: "<?=base_url()?>index.php/maqcarveh/agregarmcv", 
											data: "todos=2"+"&fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=4"+"&mcv="+$("#idlim").val()+"&mot="+$("#ordlim").val()+"&tra="+$("#limlim").val()+"&shi="+$("#obslim").val()+"&sus=0"+"&bom=0",
											success: 	
													function(msg){															
														if(msg!=0){														
															$("#nuevolim").click();$("#mytablaLim").trigger("update");$("#mytablakpi").trigger("update");																			
														}else{
															alert("Error con la base de datos o usted no ha actualizado nada");
														}					
													}		
									});
								}
							}else{
								alert("Error: Seleccione Departamento");$("#idlim").focus();return false;
							}				
							}else{
								alert("Error: Seleccione Fecha de Captura");$("#fecha").focus();return false;
							}	
					}else{
						$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/maqcarveh/agregarmcv", 
						data: "todos=1"+"&fec="+$("#fecha").val()+"&cic="+$("#cmbCiclo").val()+"&cat=4",
						success: 
								function(msg){															
									if(msg!=0){														
										$("#nuevolim").click();$("#mytablaLim").trigger("update");$("#mytablakpi").trigger("update");																					
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
						});
				  }					
   			} 
	});	
});

$("#cmbCiclo").change( function(){	
	$("#cic").val('20'+$("#cmbCiclo").val());
	$("#ciclorep").val($("#cmbCiclo").val());
	return true;
});
$("#fecha").change( function(){	
	$("#dia").val($("#fecha").val());
	return true;
});
$("#fecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",	
});	

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  
$(document).ready(function(){
	var f = new Date();
	$("#cmbMeskpi").val((f.getMonth() +1));$("#ciclorep").val($("#cmbCiclo").val());
	$("#fecha").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());		
	$("#fecha").change();
	$("#cic").val('20'+$("#cmbCiclo").val());$("#cicdia").val('20'+$("#cmbCiclo").val());
	$("#nuevomaq").click();$("#nuevocar").click();$("#nuevoveh").click();
	$("#tabla_eva").ajaxSorter({
		url:'<?php echo base_url()?>index.php/maqcarveh/tablaeva',  
		filters:['cmbCiclo','cmbMeskpi','categoria'],
		//active:['color',1],
        sort:false,
        onSuccess:function(){
       		$('#tabla_eva tbody tr td:nth-child(1)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
       		$('#tabla_eva tbody tr td:nth-child(2)').map(function(){ $(this).css('font-weight','bold');$(this).css('text-align','left');});
			col=3;
       		while(col<=33){
        		$('#tabla_eva tbody tr td:nth-child('+col+')').map(function(){
        			if($(this).html() > 0 && $(this).html() != '' ) {
              			$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
        			}
        			if($(this).html() == 0 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "orange");
        			}
        			if($(this).html() < 0 && $(this).html() != '' ) {
             				$(this).css("background-color", "red");$(this).css("color", "red");
        			}
        		});
        	col+=1;
	    	} 
		},   
	});
	$("#tabla_maq").ajaxSorter({
		url:'<?php echo base_url()?>index.php/maqcarveh/tablamaq',  
		filters:['cmbCiclo','fecha'],
		//active:['color',1],
        sort:false,
        onRowClick:function(){
    		if($(this).data('idmaq')>'0'){
        		$("#idmaq").val($(this).data('idmaq'));$("#idmcv").val($(this).data('idmcv'));
        		$("#desmcv").val($(this).data('desmcv'));
        		$("#motmaq").val($(this).data('motmaq'));motmaq=$(this).data('motmaq');
				if(motmaq==1){ $('input:radio[name=rmotmaq]:nth(0)').attr('checked',true); }
				else if(motmaq==0){ $('input:radio[name=rmotmaq]:nth(1)').attr('checked',true); }
				else if(motmaq==-1){ $('input:radio[name=rmotmaq]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rmotmaq]:nth(0)').attr('checked',false);$('input:radio[name=rmotmaq]:nth(1)').attr('checked',false);$('input:radio[name=rmotmaq]:nth(2)').attr('checked',false); }
				$("#tramaq").val($(this).data('tramaq'));tramaq=$(this).data('tramaq');
				if(tramaq==1){ $('input:radio[name=rtramaq]:nth(0)').attr('checked',true); }
				else if(tramaq==0){ $('input:radio[name=rtramaq]:nth(1)').attr('checked',true); }
				else if(tramaq==-1){ $('input:radio[name=rtramaq]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rtramaq]:nth(0)').attr('checked',false);$('input:radio[name=rtramaq]:nth(1)').attr('checked',false);$('input:radio[name=rtramaq]:nth(2)').attr('checked',false); }
				$("#shimaq").val($(this).data('shimaq'));shimaq=$(this).data('shimaq');
				if(shimaq==1){ $('input:radio[name=rshimaq]:nth(0)').attr('checked',true); }
				else if(shimaq==0){ $('input:radio[name=rshimaq]:nth(1)').attr('checked',true); }
				else if(shimaq==-1){ $('input:radio[name=rshimaq]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rshimaq]:nth(0)').attr('checked',false);$('input:radio[name=rshimaq]:nth(1)').attr('checked',false);$('input:radio[name=rshimaq]:nth(2)').attr('checked',false); }
				$("#susmaq").val($(this).data('susmaq'));susmaq=$(this).data('susmaq');
				if(susmaq==1){ $('input:radio[name=rsusmaq]:nth(0)').attr('checked',true); }
				else if(susmaq==0){ $('input:radio[name=rsusmaq]:nth(1)').attr('checked',true); }
				else if(susmaq==-1){ $('input:radio[name=rsusmaq]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rsusmaq]:nth(0)').attr('checked',false);$('input:radio[name=rsusmaq]:nth(1)').attr('checked',false);$('input:radio[name=rsusmaq]:nth(2)').attr('checked',false); }
       		}		
    	}, 
        onSuccess:function(){
        	$('#tabla_maq tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');});
        	col=2;
       		while(col<=5){
        		$('#tabla_maq tbody tr td:nth-child('+col+')').map(function(){
        			if($(this).html() > 0 && $(this).html() != '' ) {
              			$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
        			}
        			if($(this).html() == 0 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "orange");
        			}
        			if($(this).html() < 0 && $(this).html() != '' ) {
             				$(this).css("background-color", "red");$(this).css("color", "red");
        			}
        		});
        	col+=1;
	    	} 
		}	 
	});
	
	$("#tabla_car").ajaxSorter({
		url:'<?php echo base_url()?>index.php/maqcarveh/tablacar',  
		filters:['cmbCiclo','fecha'],
		//active:['color',1],
        sort:false,
        onRowClick:function(){
    		if($(this).data('idcar')>'0'){
        		$("#idcar").val($(this).data('idcar'));$("#idcmv").val($(this).data('idcmv'));
        		$("#descmv").val($(this).data('desmcv'));
        		$("#motcar").val($(this).data('motcar'));motcar=$(this).data('motcar');
				if(motcar==1){ $('input:radio[name=rmotcar]:nth(0)').attr('checked',true); }
				else if(motcar==0){ $('input:radio[name=rmotcar]:nth(1)').attr('checked',true); }
				else if(motcar==-1){ $('input:radio[name=rmotcar]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rmotcar]:nth(0)').attr('checked',false);$('input:radio[name=rmotcar]:nth(1)').attr('checked',false);$('input:radio[name=rmotcar]:nth(2)').attr('checked',false); }
				$("#tracar").val($(this).data('tracar'));tracar=$(this).data('tracar');
				if(tracar==1){ $('input:radio[name=rtracar]:nth(0)').attr('checked',true); }
				else if(tracar==0){ $('input:radio[name=rtracar]:nth(1)').attr('checked',true); }
				else if(tracar==-1){ $('input:radio[name=rtracar]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rtracar]:nth(0)').attr('checked',false);$('input:radio[name=rtracar]:nth(1)').attr('checked',false);$('input:radio[name=rtracar]:nth(2)').attr('checked',false); }
				$("#bomcar").val($(this).data('bomcar'));bomcar=$(this).data('bomcar');
				if(bomcar==1){ $('input:radio[name=rbomcar]:nth(0)').attr('checked',true); }
				else if(bomcar==0){ $('input:radio[name=rbomcar]:nth(1)').attr('checked',true); }
				else if(bomcar==-1){ $('input:radio[name=rbomcar]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rbomcar]:nth(0)').attr('checked',false);$('input:radio[name=rbomcar]:nth(1)').attr('checked',false);$('input:radio[name=rbomcar]:nth(2)').attr('checked',false); }				
       		}		
    	}, 
        onSuccess:function(){
        	$('#tabla_car tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');$(this).css('text-align','center');});
        	col=2;
       		while(col<=4){
        		$('#tabla_car tbody tr td:nth-child('+col+')').map(function(){
        			if($(this).html() > 0 && $(this).html() != '' ) {
              			$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
        			}
        			if($(this).html() == 0 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "orange");
        			}
        			if($(this).html() < 0 && $(this).html() != '' ) {
             				$(this).css("background-color", "red");$(this).css("color", "red");
        			}
        		});
        	col+=1;
	    	} 
		}	 
	});
	
	$("#tabla_veh").ajaxSorter({
		url:'<?php echo base_url()?>index.php/maqcarveh/tablaveh',  
		filters:['cmbCiclo','fecha'],
		//active:['color',1],
        sort:false,
        onRowClick:function(){
    		if($(this).data('idveh')>'0'){
        		$("#idveh").val($(this).data('idveh'));$("#idvmc").val($(this).data('idvmc'));
        		$("#desvmc").val($(this).data('desmcv'));
        		$("#motveh").val($(this).data('motveh'));motveh=$(this).data('motveh');
				if(motveh==1){ $('input:radio[name=rmotveh]:nth(0)').attr('checked',true); }
				else if(motveh==0){ $('input:radio[name=rmotveh]:nth(1)').attr('checked',true); }
				else if(motveh==-1){ $('input:radio[name=rmotveh]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rmotveh]:nth(0)').attr('checked',false);$('input:radio[name=rmotveh]:nth(1)').attr('checked',false);$('input:radio[name=rmotveh]:nth(2)').attr('checked',false); }
				$("#traveh").val($(this).data('traveh'));traveh=$(this).data('traveh');
				if(traveh==1){ $('input:radio[name=rtraveh]:nth(0)').attr('checked',true); }
				else if(traveh==0){ $('input:radio[name=rtraveh]:nth(1)').attr('checked',true); }
				else if(traveh==-1){ $('input:radio[name=rtraveh]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rtraveh]:nth(0)').attr('checked',false);$('input:radio[name=rtraveh]:nth(1)').attr('checked',false);$('input:radio[name=rtraveh]:nth(2)').attr('checked',false); }
				$("#shiveh").val($(this).data('shiveh'));shiveh=$(this).data('shiveh');
				if(shiveh==1){ $('input:radio[name=rshiveh]:nth(0)').attr('checked',true); }
				else if(shiveh==0){ $('input:radio[name=rshiveh]:nth(1)').attr('checked',true); }
				else if(shiveh==-1){ $('input:radio[name=rshiveh]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rshiveh]:nth(0)').attr('checked',false);$('input:radio[name=rshiveh]:nth(1)').attr('checked',false);$('input:radio[name=rshiveh]:nth(2)').attr('checked',false); }
				$("#susveh").val($(this).data('susveh'));susveh=$(this).data('susveh');
				if(susveh==1){ $('input:radio[name=rsusveh]:nth(0)').attr('checked',true); }
				else if(susveh==0){ $('input:radio[name=rsusveh]:nth(1)').attr('checked',true); }
				else if(susveh==-1){ $('input:radio[name=rsusveh]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rsusveh]:nth(0)').attr('checked',false);$('input:radio[name=rsusveh]:nth(1)').attr('checked',false);$('input:radio[name=rsusveh]:nth(2)').attr('checked',false); }
       		}		
    	}, 
        onSuccess:function(){
        	$('#tabla_veh tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');});
        	col=2;
       		while(col<=5){
        		$('#tabla_veh tbody tr td:nth-child('+col+')').map(function(){
        			if($(this).html() > 0 && $(this).html() != '' ) {
              			$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
        			}
        			if($(this).html() == 0 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "orange");
        			}
        			if($(this).html() < 0 && $(this).html() != '' ) {
             				$(this).css("background-color", "red");$(this).css("color", "red");
        			}
        		});
        	col+=1;
	    	} 
		}	 
	});

	$("#tabla_lim").ajaxSorter({
		url:'<?php echo base_url()?>index.php/maqcarveh/tablalim',  
		filters:['cmbCiclo','fecha'],
		//active:['color',1],
        sort:false,
        onRowClick:function(){
    		if($(this).data('idlim')>'0'){
        		$("#idlim").val($(this).data('idlim'));$("#idol").val($(this).data('idol'));
        		$("#deslim").val($(this).data('desmcv'));$("#obslim").val($(this).data('obslim'));
        		$("#ordlim").val($(this).data('ordlim'));ordlim=$(this).data('ordlim');
				if(ordlim==1){ $('input:radio[name=rordlim]:nth(0)').attr('checked',true); }
				else if(ordlim==0){ $('input:radio[name=rordlim]:nth(1)').attr('checked',true); }
				else if(ordlim==-1){ $('input:radio[name=rordlim]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rordlim]:nth(0)').attr('checked',false);$('input:radio[name=rordlim]:nth(1)').attr('checked',false);$('input:radio[name=rordlim]:nth(2)').attr('checked',false); }
				$("#limlim").val($(this).data('limlim'));limlim=$(this).data('limlim');
				if(limlim==1){ $('input:radio[name=rlimlim]:nth(0)').attr('checked',true); }
				else if(limlim==0){ $('input:radio[name=rlimlim]:nth(1)').attr('checked',true); }
				else if(limlim==-1){ $('input:radio[name=rlimlim]:nth(2)').attr('checked',true); }
				else { $('input:radio[name=rlimlim]:nth(0)').attr('checked',false);$('input:radio[name=rlimlim]:nth(1)').attr('checked',false);$('input:radio[name=rlimlim]:nth(2)').attr('checked',false); }				
       		}		
    	}, 
        onSuccess:function(){
        	$('#tabla_lim tbody tr td:nth-child(1)').map(function(){$(this).css('font-weight','bold');});
        	col=2;
       		while(col<=3){
        		$('#tabla_lim tbody tr td:nth-child('+col+')').map(function(){
        			if($(this).html() > 0 && $(this).html() != '' ) {
              			$(this).css("background-color", "#0F0"); $(this).css("color", "#0F0");
        			}
        			if($(this).html() == 0 &&$(this).html() != '' ) {
              				$(this).css("background-color", "orange"); $(this).css("color", "orange");
        			}
        			if($(this).html() < 0 && $(this).html() != '' ) {
             				$(this).css("background-color", "red");$(this).css("color", "red");
        			}
        		});
        	col+=1;
	    	}
	    	
	    	$('#tabla_lim tbody tr td:nth-child(4)').map(function(){$(this).css('font-weight','bold');$(this).css('text-align','center');
	    		if($(this).html() >= 90.00 && $(this).html() != '' ) {
              			//$(this).css("background-color", "#0F0"); 
              			//$(this).css("color", "#0F0");
        			}
        			if($(this).html() >= 80.00 && $(this).html() < 90.00 && $(this).html() != '' ) {
              				//$(this).css("background-color", "orange");
              				 $(this).css("color", "orange");
        			}
        			if($(this).html() < 80.00 && $(this).html() != '' ) {
             				//$(this).css("background-color", "red");
             				$(this).css("color", "red");
        			}
	    	});	 
		}	 
	});
});
</script>