<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/abonos.png" width="25" height="25" border="0"> Estado de Cuenta </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion">
			<h3 align="left"><a href="">Saldo Actual</a></h3>
        	<div  style="height:548px; text-align: right; font-size: 12px;">                                                                      
            <div class="ajaxSorterDiv" id="tabla_ect" name="tabla_ect" align="left" style=" height: 498px; margin-top: -10px; " >             	
            	<div style="margin-top: 0px;">
            	Técnico: <select name="enc" id="enc" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
            				<option value="0">Seleccione Encargado</option>
          					<?php	
          					//$this->load->model('edoctatec_model');
          					$data['result']=$this->edoctatec_model->verTecnico();
							foreach ($data['result'] as $row):?>
           					<option value="<?php echo $row->NumBio;?>" ><?php echo $row->NomBio;?></option>
           					<?php endforeach;?>
   						</select>
   						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Estatus Actual:<select id="cmbEstatus" style="font-size: 10px;">
						<option value="10">Todos</option>
						<option value="1">Cargos</option>
						<option value="2">Abonos</option>						
						<option value="3">Improvistos</option>						
					</select>
					Cierre de Cuentas:
					<select name="corte" id="corte" style="font-size: 10px;margin-left:10px; margin-top: 1px;">
      								<option value="0"> Todos </option>
      								<option value="1"> Corte 1 </option>
      								<option value="2"> Corte 2 </option>      									
      								<option value="3"> Corte 3 </option>
    				</select>
    				Mes:
    				<select name="mes" id="mes" style="font-size: 10px;margin-left:10px; margin-top: 1px;">
      								<option value="0"> Todos </option>
      								<option value="01"> Enero </option>
      								<option value="02"> Febrero </option>      									
      								<option value="03"> Marzo </option>
      								<option value="04"> Abril </option>
      								<option value="05"> Mayo </option>
      								<option value="06"> Junio </option>
      								<option value="07"> Julio </option>
      								<option value="08"> Agosto </option>
      								<option value="09"> Septiembre </option>
      								<option value="10"> Octubre </option>
      								<option value="11"> Noviembre </option>
      								<option value="12"> Diciembre </option>
    				</select>
				</div>	       
                <span class="ajaxTable" style=" height: 430px; width: 880px ">
                    <table id="mytablaECT" name="mytablaECT" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px"> 
                        	<th data-order = "FechaS1" style="width: 55px">Fecha</th>
                            <th data-order = "RemisionR" style="width: 30px">Remisión</th>                                                        
                            <th data-order = "Razon" style="width: 250px">Razón Social / Concepto</th>                            
                            <th data-order = "Cargo1" style="width: 50px">Cargo</th>
                            <th data-order = "Abono1" style="width: 50px;">Abono</th>
                            <th data-order = "saldofila" style="width: 50px" >Saldo</th>                                                       
                        </thead>
                        <?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Zuleima Benitez"){ ?>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        
                        <?php } else {?>
                        <tbody title="Seleccione para analizar" style="font-size: 10px">	
                        <?php }?>
                        </tbody>
                    </table>
                    
                </span> 
             	<div class="ajaxpager" style="margin-top: -10px">        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/edoctatec/pdfrep" >
                    	<input type="hidden" id="tecnico" name="tecnico" />
                        <img style="cursor: pointer" style="margin-top: 6px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                                                                          
 					</form>   
 					
                </div>  
                                                 
            </div>             
        </div>        
        <?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino" || $usuario=="Zuleima Benitez"){ ?>
        <h3 align="left"><a href="" >Agregar, Actualizar o Eliminar Abonos</a></h3>
        <?php } else {?>	
        <h3 align="left"><a href="" >Analizar Abono Seleccionado</a></h3>
        <?php }?>
       	<div style="margin-top: -8px">
			<table border="2px">
				<thead style="background-color: #DBE5F1">
					<th colspan="4" height="15px" style="font-size: 13px">Datos del Abono <select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="enc1" id="enc1" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          			<option value="0">Seleccione Encargado</option>
          						<?php	
          							//$this->load->model('edoctatec_model');
									$data['result']=$this->edoctatec_model->verTecnico();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumBio;?>" ><?php echo $row->NomBio;?></option>
           							<?php endforeach;?>
   						</select>
					<input size="8%" type="hidden" name="id" id="id"/>		
					<input size="8%" type="hidden" name="ur" id="ur" value="<?php echo set_value('ur',$urr); ?>">											
					</th>
					<th colspan="2">
						<center>
							<?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino" or $usuario=="Zuleima Benitez"){ ?>
								Aplicar a:
								<select name="rblugar" id="rblugar" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
      								<option value="1"> Corte 1 </option>
      								<option value="2"> Corte 2 </option>      									
      								<option value="3"> Corte 3 </option>
    							</select>
								<input style="font-size: 12px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
								<input style="font-size: 12px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
								<input style="font-size: 12px" type="submit" id="borrar" name="borrar" value="Borrar" />
							<?php }?>										
						</center>
					</th>	
				</thead>	
				<tbody style="background-color: #F7F7F7">							
				<tr>
					<th><label style="margin: 2px; font-size: 12px" for="txtFecha">Fecha</label></th>
					<th  colspan="2" ><label style="margin: 2px; font-size: 12px;" for="des" >Concepto</label></th>
					<th><label style="margin: 2px; font-size: 12px" for="cho">Chofer</label></th>
					<th><label style="margin: 2px; font-size: 12px" for="uni">Unidad</label></th>																										
					<th><label style="margin: 2px; font-size: 12px;" for="rem">Remision</label></th>										
				</tr>
    			<tr style="font-size: 12px; background-color: white;">										
					<th><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="13%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>								
					<th colspan="2" ><input  name="gto" type="text" id="gto" size="35" /></th>
					<th>
						<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="cho" id="cho" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
           							//$this->load->model('edoctatec_model');
									$data['result']=$this->edoctatec_model->verChofer();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumCho;?>" ><?php echo $row->NomCho;?></option>
           							<?php endforeach;?>
   						</select>
					</th>																		
					<th>
						<?php //if($des!="Improvistos" and $des!="Complemento Gasto"){ ?>
						<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="uni" id="uni" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          						<?php	
           							//$this->load->model('edoctatec_model');
									$data['result']=$this->edoctatec_model->verUnidad();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumUni;?>" ><?php echo $row->NomUni;?></option>
           							<?php endforeach;?>
   						</select>
						<?php // } ?>
					</th>
					<th>
						<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="rem" id="rem"  style="font-size: 12px;height: 25px;  margin-top: 1px; margin-left:4px;">
           						<?php	
           							//$this->load->model('edoctatec_model');
									$data['result']=$this->edoctatec_model->verRemision();
									?>
									<option value="0" ><?php echo "SN";?></option>
									<?php
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->NumRegR;?>" ><?php if($row->NumRegR==0) { echo "SN";} else { echo $row->RemisionR;}?></option>
           							<?php endforeach;?>
    					</select>
					</th>
				</tr>
				<thead style="background-color: #DBE5F1">
					<th colspan="6">Conceptos</th>
				</thead>
				<tr>
					<th><label style="margin: 2px; font-size: 12px" for="ali">Ali:</label>
						<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="ali" id="ali" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;">
					</th>
					<th><label style="margin: 2px; font-size: 12px" for="com">Com:</label>
						<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="com" id="com" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;">
					</th>
					<th><label style="margin: 2px; font-size: 12px;" for="cas" >Cas:</label>
						<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="cas" id="cas" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;">
					</th>										
					<th><label style="margin: 2px; font-size: 12px" for="hos">Hos:</label>
						<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="hos" id="hos" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;">
					</th>																										
					<th><label style="margin: 2px; font-size: 12px;" for="fit">Fit-Otr:</label>
						<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="fit" id="fit" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;">
					</th>										
					<th><label style="margin: 2px; font-size: 12px;" for="sub">Subtot:</label>
						<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="10%" readonly="true" type="text" name="sub" id="sub" style="text-align: right;">
					</th>
				</tr>
    			</tbody>    							
    			<thead style="background-color: #DBE5F1">
    				<th colspan="6">Otros Conceptos - <label style="margin: 2px; font-size: 12px" for="rep">Reposición :
						<input size="2%" type="hidden"  name="repos" id="repos" readonly="true" >
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick="radios(1,1)"  name="repo" value="1" /> Otros							
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick="radios(1,2)"  name="repo" value="2" /> Improvistos
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick="radios(1,3)"  name="repo" value="3" /> Gasto
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="16%" type="text" name="rep" id="rep" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;">
						</label>
						<label style="margin: 2px; font-size: 12px;" for="cas" >TOTAL
						$ <input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="16%" type="text" name="tot" id="tot" value="" style="text-align: right;">
						</label>
					</th>
				<tfoot style="background-color: #DBE5F1">
				 		<th colspan="6" style="font-size: 12px;"><textarea <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> readonly="true" <?php } ?> name="obs" id="obs" rows="2" cols="90" style="resize: none"></textarea></th>
				<br />
				 </tfoot>
    		</table>  
    		 <div class="ajaxSorterDiv" id="tabla_facs" name="tabla_facs" > 
                 <span class="ajaxTable" style="margin-top: 1px" >
                    <table id="mytablafacs" name="mytablafacs" class="ajaxSorter" border="1" style="width: 860px" >
                			<thead >  
                				<th data-order = "fecf1" >Fecha</th>
                				<th data-order = "facf" >Factura</th>
                				<th data-order = "Razon" >Proveedor</th>
                				<th data-order = "alif" >Alimento</th>	
                				<th data-order = "conf" >Combustible</th>
                				<th data-order = "casf" >Casetas</th>
                				<th data-order = "hosf" >Hospedaje</th>
                				<th data-order = "otrf" >Fit-Otros</th>
                				<th data-order = "totf" >Total</th>
                           	</thead>
                        	<tbody  style="font-size: 11px">                                                 	  
                        	</tbody>
                    </table>
                </span> 
             	 <div class="ajaxpager" style="margin-top: -5px">        
                    <ul class="order_list" style="width: 860px; height: 55px; margin-top: 1px">
                    	<table border="1">
                    		<tr>
                    			<th>Fecha</th>
                    			<th>Factura</th>
                    			<th>Proveedor - Deducible de Impuestos:
                    				<select name="dedf" id="dedf" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
      										<option style="color: red" value="1" > Si </option>
      										<option style="color: blue" value="0"  > No </option>      									
    								  </select>	
                    			</th>
                    			<th>Tipo</th>
                    			<th>Importe $</th>
                    			<th></th>
                    		</tr>
                    		<tr>
                    			<th>
                    				<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="fecf" id="fecf" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
                    			</th>
                    			<th><input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="facf" id="facf" class="fecha redondo mUp" style="text-align: center;" >
                    			</th>
                    			<th>
                    				<select <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> name="prof" id="prof" style="font-size: 10px;height: 23px;  margin-top: 1px;">
                    					<option value="0"></option>
          						<?php	
           							$data['result']=$this->edoctatec_model->verProveedor();
									foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->Numero;?>" ><?php echo $row->Razon;?></option>
           							<?php endforeach;?>
   									</select>                    				
                    			</th>
                    			<th>
                    				 <select name="tipf" id="tipf" style="font-size: 10px;margin-left:10px; height: 23px; margin-top: 1px;">
	      								<option value="1">Alimentos</option>
	      								<option value="2">Combustible</option>
	      								<option value="3">Casetas</option>
	      								<option value="4">Hospedaje</option>
	      								<option value="5">Fitosanitaria</option>
    	  								<option value="5">Otros</option>      									
      								</select>                    				
                    			</th>
                    			<th>
									<input <?php if($usuario!="Jesus Benítez" && $usuario!="Anita Espino" && $usuario!="Zuleima Benitez"){ ?> disabled="true" <?php } ?> size="10%" type="text" name="impf" id="impf" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right;">
								</th>
								<th>
									<input style="font-size: 14px" type="submit" id="agrega" name="agrega" value="+" title="AGREGAR y ACTUALIZAR DETALLE" />
									<input style="font-size: 14px" type="submit" id="quita" name="quita" value="-" title="QUITAR DETALLE" />
									<input type="hidden" name="idfac" id="idfac"/>
									<input type="hidden" name="gasf" id="gasf"/>
								</th>
                    		</tr>
                    	</table>
                    	
                    </ul>     
                    <form method="post" action="<?= base_url()?>index.php/edoctatec/pdfrepfacs" >                    	             	                                                
                        <img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <input type="hidden"  name="tablafacs" value ="" class="htmlTable"/>    
                        <input type="hidden" style="width: 120px; border: none" name="gto1" id="gto1"/>
                        <input type="hidden" style="width: 120px; border: none" name="fec1" id="fec1"/>
                		<input type="hidden" style="width: 480px" name="tec1" id="tec1"/>                                                  
                	</form>   
 				</div>
 			</div>    
 		     
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#quita").click( function(){	
	numero=$("#idfac").val();;
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edoctatec/quitarDet", 
						data: "id="+$("#idfac").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablafacs").trigger("update");
										$("#fecf").val('');$("#facf").val('');$("#prof").val('');$("#tipf").val('');$("#impf").val('');
										$("#idfac").val('');$("#dedf").val('1');
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Factura para poder Quitarlo");
		return false;
	}
});
$("#agrega").click( function(){	
	numero=$("#idfac").val();ids=$("#id").val(); //idf=$("#ur").val();
	fec=$("#fecf").val();fac=$("#facf").val();prov=$("#prof").val();imp=$("#impf").val();
	if(ids==''){  $("#gasf").val($("#ur").val());$("#id").val($("#ur").val());}
	if(fec!=''){
		if(fac!=''){
			if(prov>0){
				if(imp!=0){
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edoctatec/actualizafac", 
						data: "id="+$("#idfac").val()+"&fecf="+$("#fecf").val()+"&facf="+$("#facf").val()+"&prof="+$("#prof").val()+"&tipf="+$("#tipf").val()+"&impf="+$("#impf").val()+"&dedf="+$("#dedf").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										//$("#mytablaECT").trigger("update");
										$("#mytablafacs").trigger("update");
										$("#fecf").val('');$("#facf").val('');$("#prof").val('');$("#tipf").val('');$("#impf").val('');$("#dedf").val('1');
										$("#idfac").val('');
										//$("#fecf").focus();
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edoctatec/agregafac", 
						data: "&fecf="+$("#fecf").val()+"&facf="+$("#facf").val()+"&prof="+$("#prof").val()+"&tipf="+$("#tipf").val()+"&impf="+$("#impf").val()+"&gasf="+$("#gasf").val()+"&dedf="+$("#dedf").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										//$("#mytablaECT").trigger("update");
										$("#mytablafacs").trigger("update");
										$("#fecf").val('');$("#facf").val('');$("#prof").val('');$("#tipf").val('');$("#impf").val('');$("#dedf").val('1');
										$("#idfac").val('');
										//$("#fecf").focus();
										$("#tabla_facs").ajaxSorter({
											url:'<?php echo base_url()?>index.php/edoctatec/tabladetafac', 
											filters:['id'],
											active:['dedf',0],
											sort:false,	
											onRowClick:function(){
						$("#fecf").val($(this).data('fecf'));
						$("#facf").val($(this).data('facf'));
						$("#prof").val($(this).data('prof'));
						$("#tipf").val($(this).data('tipf'));
						$("#impf").val($(this).data('impf1'));
						$("#dedf").val($(this).data('dedf'));
						$("#idfac").val($(this).data('nfac'));
					}, 
										onSuccess:function(){	
										$('#tabla_facs tbody tr').map(function(){
	    									if($(this).data('razon')=='Total Neto'){	    				    			
	    										$(this).css('background','lightblue');
	    										$(this).css('font-weight','bold');
	    										$("#ali").val($(this).data('alif'));
												$("#com").val($(this).data('conf'));
												$("#cas").val($(this).data('casf'));
												$("#hos").val($(this).data('hosf'));
												$("#fit").val($(this).data('otrf'));
												$("#sub").val($(this).data('totf'));	
												$("#tot").val($(this).data('totf'));
											}
										});	
											}, 	
										});	
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});		
		}
		}else{
		alert("Error: Registre Importe");	
		$("#impf").focus();
		return false;
		}
		}else{
		alert("Error: Seleccione Proveedor");	
		$("#prof").focus();
		return false;
		}
		}else{
		alert("Error: Registre Factura");	
		$("#facf").focus();
		return false;
		}
	}else{
		alert("Error: Seleccione Fecha");	
		$("#fecf").focus();
		return false;
	}		
});
$("#enc").change( function(){		
	$("#ecn1").val($("#enc").val());	
	$("#tecnico").val($("#enc").val());
 return true;
});	
function radios(radio,dato){
	if(radio==1) { $("#repos").val(dato); }	
}
$("#provf").change( function(){	
	$("#tipf").focus();
	
});
	
$("#nuevo").click( function(){	
	$("#id").val('');
	//$("#enc1").val('');
    $("#txtFecha").val('');
	$("#gto").val('');
	$("#cho").val('');
	$("#uni").val('');
	$("#rem").val('0');	
	$("#ali").val('');
	$("#com").val('');
	$("#cas").val('');
	$("#hos").val('');
	$("#fit").val('');
	$("#sub").val('');			
	$("#rep").val('');
	$("#obs").val('');
	$("#tot").val('');
	$("#gto").val('');
	$("#nomdess").val('');
	$("#rblugar").val('');
	$('input:radio[name=repo]:nth(0)').attr('checked',false);$('input:radio[name=repo]:nth(1)').attr('checked',false);
	$('input:radio[name=repo]:nth(2)').attr('checked',false);
	$("#mytablafacs").trigger("update");
	$("#fecf").val('');$("#facf").val('');$("#prof").val('');$("#tipf").val('');$("#impf").val('');
	$("#idfac").val('');
	$("#tabla_facs").ajaxSorter({
		url:'<?php echo base_url()?>index.php/edoctatec/tabladetafac', 
		filters:['id'],
		sort:false,	
	});
 return true;
})

$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edoctatec/borrar", 
						data: "id="+$("#id").val(), //+"&usd="+$("#usd").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Abono Eliminado correctamente");
										$("#nuevo").click();
										$("#mytablaECT").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Abono para poder Eliminarlo");
		$("#accordion").accordion( "activate",0 );
		return false;
	}
});


function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#aceptar").click( function(){
	ids=$("#id").val(); idf=$("#ur").val();
	if(ids==idf){ $("#id").val('');} 		
	enc=$("#enc1").val();
	fec=$("#txtFecha").val();
	numero=$("#id").val();
	rem=$("#rem").val();
	if( enc>'0'){
	if( fec!=''){
	  if( rem>'0'){
		if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edoctatec/actualizar", 
						data: "&id="+$("#id").val()+"&fec="+$("#txtFecha").val()+"&rem="+$("#rem").val()+"&enc="+$("#enc1").val()+"&des="+$("#gto").val()+"&cho="+$("#cho").val()+"&uni="+$("#uni").val()+"&ali="+$("#ali").val()+"&com="+$("#com").val()+"&cas="+$("#cas").val()+"&hos="+$("#hos").val()+"&fit="+$("#fit").val()+"&rep="+$("#rep").val()+"&obs="+$("#obs").val()+"&repos="+$("#repos").val()+"&corte="+$("#rblugar").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Abono actualizado correctamente");
										$("#nuevo").click();
										$("#mytablaECT").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/edoctatec/agregar", 
						data: "&fec="+$("#txtFecha").val()+"&rem="+$("#rem").val()+"&enc="+$("#enc1").val()+"&des="+$("#gto").val()+"&cho="+$("#cho").val()+"&uni="+$("#uni").val()+"&ali="+$("#ali").val()+"&com="+$("#com").val()+"&cas="+$("#cas").val()+"&hos="+$("#hos").val()+"&fit="+$("#fit").val()+"&rep="+$("#rep").val()+"&obs="+$("#obs").val()+"&repos="+$("#repos").val()+"&corte="+$("#rblugar").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Abono registrado correctamente");
										$("#nuevo").click();
										$("#mytablaECT").trigger("update");
										$("#ur").val(parseFloat($("#ur").val())+1);	
										$("#accordion").accordion( "activate",0 );												
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Seleccione remision para aplicar Abono");	
			$("#rem").focus();
			return false;
			}
		}else{
			alert("Error: Fecha no valido");	
			$("#txtFecha").focus();
				return false;
		}
		}else{
			alert("Error: Seleccione Encargado ");	
			$("#enc1").focus();
				return false;
		}
});

$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		var value= $(this).val();
		$('#fec1').val(value)
	}
	
});
$("#fecf").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#fec1").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  


function noNumbers(e){
		var keynum;
		var keychar;
		var numcheck;
		if(window.event){
			jeynum = e.keyCode;
		}
		else if(e.which){
			heynum = eArray.which;
		}
		keychar = String.formCharccode(keynum);
		numcheck = /\d/;
		return !numcheck.test(keychar);
	}
		
$(document).ready(function(){
	$('#gto').keyup(function(){
		var value= $(this).val();
		$('#gto1').val(value);
	});
	$('#txtFecha').keyup(function(){
		var value= $(this).val();
		$('#fec1').val(value);
	});
	$("#nuevo").click(); 
	$("#ecn1").val($("#enc").val());
	$("#tecnico").val($("#enc").val());	$("#tec1").val($(this).data('nombio'));
	$("#tabla_ect").ajaxSorter({
		url:'<?php echo base_url()?>index.php/edoctatec/tabla',  		
		filters:['enc','cmbEstatus','corte','mes'],
		active:['CarAbo',2],
    	//multipleFilter:true, 
    	sort:false,            
    	//onLoad:false,  
    	onRowClick:function(){    						
    		if($(this).data('carabo')!=2 && $(this).data('fechas1')==''){
    			$("#enc").val($(this).data('remisionr'));
    			$("#enc1").val($(this).data('remisionr'));
    			$("#mytablaECT").trigger("update");
    		}		
			if($(this).data('carabo')==2){
				$("#accordion").accordion( "activate",1 );
				$("#gto1").val($(this).data('nomdess'));
       			$("#id").val($(this).data('numsol'));$("#gasf").val($(this).data('numsol'));
       			$("#txtFecha").val($(this).data('fechas'));
       			$("#fec1").val($(this).data('fechas'));
       			$("#enc").val($(this).data('numbios'));
       			$("#enc1").val($(this).data('numbios'));
       			$("#tec1").val($(this).data('nombio'));
       			$("#cho").val($(this).data('numchos'));
       			$("#uni").val($(this).data('numunis'));
       			$("#gto").val($(this).data('nomdess'));
       			gtos=$(this).data('nomdess');
				if(gtos=='Otros' || gtos=='Improvistos'){
					if(gtos=='Otros'){ $('input:radio[name=repo]:nth(0)').attr('checked',true); $("#repos").val(1);}
					if(gtos=='Improvistos'){ $('input:radio[name=repo]:nth(1)').attr('checked',true); $("#repos").val(2);}
					//if(gtos=='Improvistos-Gasto'){ $('input:radio[name=repo]:nth(2)').attr('checked',true); $("#repos").val(3); }
				}else{
					$('input:radio[name=repo]:nth(2)').attr('checked',true); $("#repos").val(3);
				//	$('input:radio[name=repo]:nth(0)').attr('checked',false);$('input:radio[name=repo]:nth(1)').attr('checked',false);
				//	$('input:radio[name=repo]:nth(2)').attr('checked',false); 
				}
       			$("#rem").val($(this).data('numrems'));	
				$("#ali").val($(this).data('ali'));
				$("#com").val($(this).data('com'));
				$("#cas").val($(this).data('cas'));
				$("#hos").val($(this).data('hos'));
				$("#fit").val($(this).data('fit'));
				$("#sub").val($(this).data('sub'));			
				$("#rep").val($(this).data('rep'));
				$("#obs").val($(this).data('obser'));
				$("#tot").val($(this).data('tot'));
				$("#rblugar").val($(this).data('corte'));	
				//aqui se piden los detalles de las facturas
				$("#tabla_facs").ajaxSorter({
					url:'<?php echo base_url()?>index.php/edoctatec/tabladetafac', 
					filters:['id'],
					active:['dedf',0],
					sort:false,	
					onRowClick:function(){
						$("#fecf").val($(this).data('fecf'));
						$("#facf").val($(this).data('facf'));
						$("#prof").val($(this).data('prof'));
						$("#tipf").val($(this).data('tipf'));
						$("#impf").val($(this).data('impf1'));
						$("#dedf").val($(this).data('dedf'));
						$("#idfac").val($(this).data('nfac'));
					}, 
					onSuccess:function(){
    					$('#tabla_facs tbody tr').map(function(){
	    					if($(this).data('razon')=='Total Neto'){	    				    			
	    						$(this).css('background','lightblue');
	    						$(this).css('font-weight','bold');
	    						$("#ali").val($(this).data('alif'));
								$("#com").val($(this).data('conf'));
								$("#cas").val($(this).data('casf'));
								$("#hos").val($(this).data('hosf'));
								$("#fit").val($(this).data('otrf'));
								$("#sub").val($(this).data('totf'));	
								$("#tot").val($(this).data('totf'));
							}
	    				});
	    				$('#tabla_facs tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');  })
	    				$('#tabla_facs tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');  })
	    				$('#tabla_facs tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right');  })
	    				$('#tabla_facs tbody tr td:nth-child(7)').map(function(){ $(this).css('text-align','right');  })
	    				$('#tabla_facs tbody tr td:nth-child(8)').map(function(){ $(this).css('text-align','right');  })
	    				$('#tabla_facs tbody tr td:nth-child(9)').map(function(){ $(this).css('text-align','right');  })
	   				}, 	
				});			
			}			
    	}, 
    	onSuccess:function(){  	   	
	    	$('#tabla_ect tbody tr').map(function(){
	    		if($(this).data('fechas1')=='Total'){	    				    			
	    			$(this).css('background','lightblue');
	    			$(this).css('font-weight','bold'); 
	    		}
	    	});
	    	$('#tabla_ect tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','center');  })
	    	$('#tabla_ect tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center');  })
	    	$('#tabla_ect tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');  })
	    	$('#tabla_ect tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');  })
	    	$('#tabla_ect tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right');  })   
    	},  
	});
	
});  	
</script>