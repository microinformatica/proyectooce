<?php $this->load->view('header_cv');?>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   
</style>
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/abonos.png" width="25" height="25" border="0"> Estado de Cuenta </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion">
			<h3 align="left"><a href="">Saldo Actual</a></h3>
        	<div  style="height:350px;">                                                                      
            <div class="ajaxSorterDiv" id="tabla_ect" name="tabla_ect" align="left" style=" height: 396px; margin-top: -10px; " >             	
            	<div style="margin-top: 0px;">
            	Técnico: <select name="enc" id="enc" style="font-size: 11px;height: 25px;  margin-top: 1px; margin-left:4px;">
          					<?php	
          					//$this->load->model('edoctatec_model');
          					$data['result']=$this->edoctatec_model->verTecnico();
							foreach ($data['result'] as $row):?>
           					<option value="<?php echo $row->NumBio;?>" ><?php echo $row->NomBio;?></option>
           					<?php endforeach;?>
   						</select>
   						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Seleccion por estatus Actual: <select id="cmbEstatus" style="font-size: 10px;">
						<option value="10">Todos</option>
						<option value="1">Cargos</option>
						<option value="2">Abonos</option>						
						<option value="3">Improvistos</option>						
					</select>
				</div>	       
                <span class="ajaxTable" style=" height: 310px; width: 880px ">
                    <table id="mytablaECT" name="mytablaECT" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera" style="font-size: 10px"> 
                        	<th data-order = "FechaS1" style="width: 55px">Fecha</th>
                            <th data-order = "RemisionR" style="width: 30px">Remisión</th>                                                        
                            <th data-order = "Razon" style="width: 250px">Razón Social / Concepto</th>                            
                            <th data-order = "Cargo1" style="width: 50px">Cargo</th>
                            <th data-order = "Abono1" style="width: 50px;">Abono</th>
                            <th data-order = "saldofila" style="width: 50px" >Saldo</th>                                                       
                        </thead>
                        <?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino"){ ?>
                        <tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        
                        <?php } else {?>
                        <tbody title="Seleccione para analizar" style="font-size: 10px">	
                        <?php }?>
                        </tbody>
                    </table>
                    
                </span> 
             	<div class="ajaxpager" style="margin-top: -15px">        
                    <ul class="order_list"></ul>                          
                    <form method="post" action="<?= base_url()?>index.php/edoctatec/pdfrep" >
                    	<input type="hidden" id="tecnico" name="tecnico" />
                        <img  style="margin-top: 6px" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/> 
                                                                          
 					</form>   
 					
                </div>  
                                                 
            </div>             
        </div>        
        <?php if($usuario=="Jesus Benítez" || $usuario=="Anita Espino"){ ?>
        <h3 align="left"><a href="" >Agregar, Actualizar o Eliminar Abonos</a></h3>
        <?php }?>	
       	<div style="height: 150px;">
			
			       	 
        </div> 	
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#enc").change( function(){		
	$("#ecn1").val($("#enc").val());	
	$("#tecnico").val($("#enc").val());
 return true;
});	
$(document).ready(function(){
	 
	$("#ecn1").val($("#enc").val());
	$("#tecnico").val($("#enc").val());	
	$("#tabla_ect").ajaxSorter({
		url:'<?php echo base_url()?>index.php/edoctatec/tabla',  		
		filters:['enc','cmbEstatus'],
		active:['CarAbo',2],
    	//multipleFilter:true, 
    	sort:false,              
    	onRowClick:function(){    								
			if($(this).data('carabo')==2){
				$("#accordion").accordion( "activate",1 );
       			
			}			
    	}, 
    	onSuccess:function(){
    		//tc = 0;ta = 0;    	   	
	    	$('#tabla_ect tbody tr').map(function(){
	    		if($(this).data('fechas1')=='Total'){	    				    			
	    			$(this).css('background','lightblue');
	    		}
	    	});
		},  
	});
	
	
		
});  	
</script>