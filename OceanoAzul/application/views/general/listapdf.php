<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 80px 50px 20px; }
	#header { position: fixed; left: 0px; top: -80px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: 0px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: orange; }	
</style>
<title>Reporte General de Granja</title>
</head>
<body>
	<div id="header"> 
		<table style="width: 700px;">
			<tr>
				<td width="200px">
					<p style="font-size: 8px;"  >
						<img src="<?php echo base_url();?>assets/images/oceanoazul.jpg" width="200" height="60" border="0">
						SIA - <?= $usuario?> - <?= $perfil?> <br />
					</p>
				</td>	
				<td align="center" style="font-size: 14px; color: navy"><strong>Acuicola Oceano Azul S.A. de C.V. <br /> Generales del Ciclo - Granja: <?= $granja?></strong>
				</td>						
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: navy;">
				<td width="160px">www.acuicolaoceanoazul.mx</td>	
				<td width="490">
					Av. Reforma No.2007-B Int. 803, Col. Flamingos, Mazatlán, Sin. México, RFC:AOA-180206-SI2, CP:82149 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
	</div>
 <?php 
	function weekday($fechas){
	    $fechas=str_replace("/","-",$fechas);
	    list($dia,$mes,$anio)=explode("-",$fechas);
	    return (((mktime ( 0, 0, 0, $mes, $dia, $anio) - mktime ( 0, 0, 0, 7, 17, 2006))/(60*60*24))+700000) % 7;
	}
	$f=explode("-",$dia); 
    $nummes=(int)$f[1]; 
    $mes1="0-Enero-Febrero-Marzo-Abril-Mayo-Junio-Julio-Agosto-Septiembre-Octubre-Noviembre-Diciembre"; 
    $mes1=explode("-",$mes1); 
    $desfecha="$f[2]/$nummes/$f[0]"; 
    $diaN=weekday($desfecha);
	if ( $diaN == 0 ) { $dia="Lunes";}
	if ( $diaN == 1 ) { $dia="Martes";}
	if ( $diaN == 2 ) { $dia="Miércoles";}
	if ( $diaN == 3 ) { $dia="Jueves";}
	if ( $diaN == 4 ) { $dia="Viernes";}
	if ( $diaN == 5 ) { $dia="Sábado";}
	if ( $diaN == 6 ) { $dia="Domingo";}
?>
<div style="margin-left:-15px; margin-top: 20px; text-align: justify; width: 750px">
	Reporte General de Producción de Camarón, al día <?= $dia.", ".$f[2]." de ".$mes1[$nummes]. " de ".$f[0]."."?>
</div>
<div style="margin-left:-15px; margin-top: 15px; text-align: justify; width: 750px">
	<table border="1" style="text-align: center; font-size: 12px" width="750px" <?= $tablab?></table><br>
    <table border="1" style="text-align: center; font-size: 12px" width="750px"><?= $tablav?></table><br>
    <table border="1" style="text-align: center; font-size: 12px" width="750px"><?= $tablam?></table><br>
    <table border="1" style="text-align: center; font-size: 12px" width="750px"><?= $tablag?></table><br>
	<table border="1" style="text-align: center; font-size: 12px" width="750px" >	
     	<tr><td>Ingresos</td><td>Gastos</td><td>Saldo</td></tr>
     	<tr><td style="color: blue; font-size: 18px;"><?= $ing?></td><td style="color: red; font-size: 18px;"><?= $gto?></td>
     		<td style="font-size: 18px;"><?= $sal?></td>
     	</tr>
	</table>
</div>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/exporting.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/export-data.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts-5.0.12/code/modules/accessibility.js"></script>
<script type="text/javascript">
Highcharts.chart('graficas', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Stacked bar chart'
    },
    xAxis: {
        categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total fruit consumption'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: [{
        name: 'John',
        data: [5, 3, 4, 7, 2]
    }, {
        name: 'Jane',
        data: [2, 2, 3, 2, 1]
    }, {
        name: 'Joe',
        data: [3, 4, 4, 2, 5]
    }]
});
</script>
<figure class="highcharts-figure" style="margin-top: -10px;">
    <div id="graficas" style="height: 250px;"></div>
</figure> 	

<div style="margin-left:-15px; margin-top: 20px; text-align: justify; width: 750px">
	Agradeciendo de antemano su atencion, quedo a sus órdenes para cualquier aclaración al respecto.<br />
	Atentamente
	<br />
	<br /><?= $usuario?>
	<br /><?= $perfil?>
</div>

<script type="text/php">
	if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        $foot = $pdf->open_object();
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
        $size = 7;
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        $pdf->close_object();
        $pdf->add_object($foot, "all");
	}
</script>
</body>
</html>