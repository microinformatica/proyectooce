<?php $this->load->view('header_cv'); ?>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.tablesorter.js" ></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.tablesorter.pager.js"></script>
<script src="<?=base_url();?>assets/js/jquery.loader.js"></script>
<link href="<?=base_url();?>assets/css/jquery.loader.css" rel="stylesheet" />
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/banco.png" width="25" height="25" border="0"> Depósitos - Busqueda</li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal">
			<div id="clientes" class="tab_content" style="overflow-x:hidden;" >
				<div style="height:420px; margin-top: -15px">
				<form method="post"   >	<!--action="<?= base_url()?>index.php/depositos/buscar"-->	
		<table id="myTablaE" class="tablesorter" border="2px" width="44%">
			<thead>
				<tr><th>DEPÓSITOS EFECTUADOS DEL DÍA
								<input type="text" name="dpDesde" id="dpDesde" class="fecha redondo mUp" readonly="true" value="<?= $dpDesde?>" style="padding-left:3px;border: 1px #008B8B solid;width:60px;height:14px;"/>
								AL DÍA <input type="text" name="dpHasta" id="dpHasta"  class="fecha redondo mUp" readonly="true" value="<?= $dpHasta?>" style="padding-left:3px;border: 1px #008B8B solid;width:60px;height:14px;"/>
								Razon Social: <input type="text" name="busqueda" id="busqueda" size="50%" value="<?= $busqueda?>" />
								<!--<button id="btn_buscar">Buscar</button>-->
								<button style="margin-bottom:5px; id="btnBusca" onClick="javascript:generar()">Generar</button>
								</th>
							</tr>					
			</thead>			
		</table>
	</form>	
	<?php
		if($result>0){ ?>		
		<table id="myTabla" class="tablesorter" border="2px" width="6500px" style="margin-top: -15px">
			<thead>				
				<tr>
					<th width="10px">Fecha</th>
					<th width="400px">Razon Social</th>
					<th width="60px">Camarón</th>
					<th width="60px">U.S.D.</th>
					<th width="60px">M.N.</th>					
					<th width="60px">U.S.D.</th>					
				</tr>
			</thead>			
			<tbody>
				<?php						
				$feci=""; $usac=0; $usa=0; $camaron=0; $mex=0; $dolar=0; foreach ($result as $row):
				$cantidadR=$row->ImporteD;$cantidadRC=$cantidadR;
				$mn=$row->Pesos;$camaron=$row->Cam;
				if($camaron==0){
					if($row->Pesos==0){ $dolar=$dolar+$cantidadR; } else { $mex=$mex+$row->Pesos; }
					$usa=$usa+$cantidadR; 
				} else {
					$usac=$usac+$cantidadRC; 
				}
						?>
				<tr>
					<?php
					if($feci!=$row->Fecha){ $feci=$row->Fecha;?>
					<td width="10px"><strong><?php echo $row->Fecha; ?></strong></td>
					<?php }else{ ?>
					<td width="10px" style="color: grey" align="right"><?php echo $row->Fecha; ?></td>	
					<?php } ?>	
					<?php if($usuario=="Lety Lizárraga" or $usuario=="Jesus Benítez"){ ?>
					<td width="400px"><a href="<?=base_url()?>index.php/depositos/actualizar/<?=$row->ND?>"><?php echo $row->Razon;?></a></td>
					<?php } else {?> 
					<td width="400px" "><?php echo $row->Razon ?><?php ?></td> 
					<?php }?>				
					<td width="60px" align="right" style="color:#0033FF">
         			 <?php if($camaron==-1){ echo "$ ".number_format($cantidadRC, 2, '.', ','); }?>
        			</td>
        			<td width="60px" align="right" style="color:#009900" >
          			<?php if(($mn==0) and ($camaron==0)){ echo "$ ".number_format($cantidadR, 2, '.', ','); }?>
        			</td>
        			<td width="60px" align="right" style="color:#FF0000">
          				<?php if(($mn>0) and ($camaron==0)){ echo "$ ".number_format($mn, 2, '.', ','); }?>
        			</td>
        			<td width="60px" align="right" ><?php if($camaron==0){ echo "$ ".number_format($cantidadR, 2, '.', ','); }?></td>
				</tr>
				<?php endforeach;?>						
				</tbody>
				<tfoot>
				<tr>
        			<th colspan="2" width="210px" style="color:#000000">Totales:</th>
        			<th width="50px"><div align="right"  style="color:#0033FF"><?php echo "$ ".number_format($usac, 2, '.', ',');?></div></th>
        			<th width="50px"><div align="right" style="color:#009900"><?php echo "$ ".number_format($dolar, 2, '.', ',');?></div></th>
        			<th width="50px"><div align="right" style="color:#FF0000"><?php echo "$ ".number_format($mex, 2, '.', ',');?></div></th>
        			<th width="50px"><div align="right" style="color:#000000"><?php echo "$ ".number_format($usa, 2, '.', ',');?></div></th>
   				</tr>
   				<tr>
        			<th align="right" colspan="5" width="360px" style="color:#000000">Ingresos Totales con Pago de Camaron:</th>
        			<th width="50px"><div align="right" style="color:#000000"><?php echo "$ ".number_format(($usa+$usac), 2, '.', ',');?></div></th>
   				</tr>
				</tfoot>
		</table>			
	<?php	}else{ ?>
		<table id="myTable" class="tablesorter" width="40%" border="5px" >
			<thead>
				<tr>
					<th width="40%" colspan="3" style="color: red">ERROR: LO SENTIMOS PERO NO HAY RESULTADOS PARA SU BUSQUEDA...</th>					
				</tr>
			</thead>			
		</table>			
	<?php }?>	
	
			</div>
		</div>
	</div>	
</div>
<div  id="pager" class="pager"  >
	<form>
		<img style="margin-top: 0px" src="<?php echo base_url()?>assets/addons/pager/icons/first.png" class="first"/>
		<img src="<?php echo base_url()?>assets/addons/pager/icons/prev.png" class="prev"/>
		<input type="text" class="pagedisplay" size="4px" readonly="true"/>
		<img src="<?php echo base_url()?>assets/addons/pager/icons/next.png" class="next"/>
		<img src="<?php echo base_url()?>assets/addons/pager/icons/last.png" class="last"/>
		<select style="margin-top: 1px" class="pagesize">
			<option selected="selected" value="10">10</option>
			<option value="15">15</option>													
		</select>		
		<a style="color: white" href="<?=base_url()?>index.php/depositos/agregar">AGREGAR DEPÓSITO</a>
	</form>			
</div>		
</div>
<?php $this->load->view('footer_cv'); ?>
<script>
//Formato para datapicker
$("#dpDesde").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#dpHasta").datepicker( "option", "minDate", selectedDate );
	}
});
$("#dpHasta").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#dpDesde").datepicker( "option", "maxDate", selectedDate );
	}
});	
	
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  	

function generar(){	
	var desde=$('#dpDesde').val();
	var hasta=$('#dpHasta').val();
	var ok=true;
	//console.debug('desde->'+desde+".....hasta"+hasta);
		
	if(desde==""){
		alert('Seleccione fecha Inicial para la busqueda.');
		$("#dpDesde").focus();
		ok=false;
	}else{
	if(hasta==""){
		alert('Seleccione fecha de Final para la busqueda.');
		$("#dpHasta").focus();
		ok=false;
	}
	}
	if(ok){
		$.loader({
			className:"black",
			content:''
		});
		$.ajax({	
		  	type: "POST",
		    url: "<?= base_url()?>index.php/depositos/buscar", 
		    data: "desde="+desde+"&hasta="+hasta,		    
			statusCode:{500:function(){alert('error 500');}},
		    
		});
		$.loader('close');
	}	
} 

$(document).ready(function(){ 			
		$("#myTabla").tablesorter ({headers:{}, widthFixed: true, widgets: ['zebra']}).tablesorterPager ({container: $("#pager")});
  
});

/*

$("#dpDesde").keydown(function(e){
    if (e.keyCode == 46 || e.keyCode == 8) {
        //Delete and backspace clear text 
        $(this).val(''); //Clear text
        $(this).datepicker("hide"); //Hide the datepicker calendar if displayed
        $(this).blur(); //aka "unfocus"
    }

    //Prevent user from manually entering in a date - have to use the datepicker box
    e.preventDefault();
});
$("#dpHasta").keydown(function(e){
    if (e.keyCode == 46 || e.keyCode == 8) {
        //Delete and backspace clear text 
        $(this).val(''); //Clear text
        $(this).datepicker("hide"); //Hide the datepicker calendar if displayed
        $(this).blur(); //aka "unfocus"
    }

    //Prevent user from manually entering in a date - have to use the datepicker box
    e.preventDefault();
});*/
</script>
