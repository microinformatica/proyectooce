<?php $this->load->view('header_cv'); ?>
<script src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>
<style>        
.tab_content {
	padding: 10px; font-size: 1.2em; height: 452px;
}
ul.tabs li {
	border-top: none; border-left: none; border-right: none; border-bottom: 1px ; font-size: 25px; background: none;		
}
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/banco.png" width="25" height="25" border="0"> Depósitos - Agregar</li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal">
			<div id="clientes" class="tab_content" style="overflow-x:hidden;" >
				<div style="height:420px; margin-top: -15px">
					<form method="post" action="<?= base_url()?>index.php/depositos/agregar" id="form">			
			<table border="2px" border="5px" class="tablesorter">
				<thead><th colspan="4" height="25px" style="font-size: 25px"><center>Ingresar Datos</center></th></thead>
				<tr>
					<th rowspan="2"><label style="margin: 2px; font-size: 25px" for="nombre">Razón Social:</label></th>
					<th colspan="3" style="font-size: 20px; "><input size="78%" type="text" name="nombre" id="nombre">
					</th>
				</tr>
				<tr align="right">
					<th colspan="3" align="right"><label style="margin: 2px; font-size: 20px" for="txtZona">Zona:</label><input style="font-size: 20px" type="text" name="txtZona" id="txtZona"></th>
					
				</tr>
				<tr>
					<th><label style="margin: 2px; font-size: 25px" for="txtFecha">Fecha:</label></th>
					<th style="font-size: 20px; background-color: white"><input type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="padding-left:3px;border: 1px #008B8B solid;"/></th>
					<th><label style="margin: 2px; font-size: 25px" for="txtTC">T.C.:</label></th>
					<th style="font-size: 20px; background-color: white">$<input type="text" name="txtTC" id="txtTC"></th>			
				</tr>
    			<tr>
					<th><label style="margin: 2px; font-size: 25px" for="txtPesos">M.N.:</label></th>
					<th style="font-size: 20px; background-color: white">$<input type="text" name="txtPesos" id="txtPesos"></th>
					<th><label style="margin: 2px; font-size: 25px" for="edo">U.S.D.:</label></th>
					<th style="font-size: 20px; background-color: white">$<input type="text" name="txtDolar" id="txtDolar"></th>
					
				</tr>
    			<tr>
					<th><label style="margin: 2px; font-size: 25px" for="txtCta">Cuenta:</label></th>
					<th style="font-size: 20px; background-color: white"><input type="text" name="txtCta" id="txtCta"></th>
					<th><label style="margin: 2px; font-size: 25px" for="lstciclo">Aplicar Depósito a Ciclo:</label></th>
					<th style="font-size: 20px; background-color: white">
						<select name="lstciclo" id="lstciclo" style="font-size: 20px">
							<option value="2013" <?php if(isset($ciclo) && $ciclo==2013) echo "Selected='selected'";?> > 2013 </option>
            				<option value="2012" <?php if(isset($ciclo) && $ciclo==2012) echo "Selected='selected'";?> > 2012 </option>
							<option value="2011" <?php if(isset($ciclo) && $ciclo==2011) echo "Selected='selected'";?> > 2011 </option>
							<option value="2010" <?php if(isset($ciclo) && $ciclo==2010) echo "Selected='selected'";?> > 2010 </option>
            				<option value="2009" <?php if(isset($ciclo) && $ciclo==2009) echo "Selected='selected'";?> > 2009 </option>
            				<option value="2008" <?php if(isset($ciclo) && $ciclo==2008) echo "Selected='selected'";?> > 2008 </option>
            				<option value="2007" <?php if(isset($ciclo) && $ciclo==2007) echo "Selected='selected'";?> > 2007 </option>
          				</select>
					</th>
				</tr>
    			<tr style="font-size: 25px">
        			<th colspan="4">Observaciones</th>        							
      			</tr>
      			<tr style="font-size: 18px">
        			<th colspan="4" bgcolor="white"><div align="center"><textarea style="font-size: 25px" id ="txtobs" name="txtobs" cols="78" wrap="virtual"></textarea></div></th>        							
      			</tr>
				<tfoot>
					<th colspan="3"></th>
					<th ><center><button name="boton" value="1" style="font-size: 25px">Guardar</button></center></th>
				</tfoot>
    		</table>    		
    			
		</form>
			</div>
		</div>
	</div>	
</div>
<a style="color: white;" href="<?=base_url()?>index.php/depositos">LISTA DE DEPÓSITOS</a>		
</div>
<?php $this->load->view('footer_cv'); ?>
<script>

$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

// Este evento verifica que el valor sea numero
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}

$("#form").submit(function(){
	nombre=$("#nombre").val();
	cp=$("#cp").val();
	if(nombre!=''){
		if( is_int(cp)){
			return true;
		}else{
			alert("Error: Codigo Postal no valido");	
				return false;
		}
	}else{
		alert("Error: Nombre de Cliente no valido");
			return false;
	}
	return false;	
});
</script>