<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ajaxsorter</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ajaxSorterStyle/style.css">        
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui-1.8.23.custom.min.js"></script>                
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.ajaxsorter.js"></script>        
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.boxloader.js"></script>            
        <style>
            .Wiz{
                padding: 0px;
                width: 750px;
                margin: 0px auto;
                background-color: lightgray;
            }
            .steps{
                margin: 0px;
                width: 100%;                 
            }  
            .steps div{
                padding: 0px;
            }
            .steps div h4{
                background-color: #DBE5F1;
                color:black;
                text-align: right;
                padding: 5px;
                margin: 0px;                
            }
            .steps div p{                
                text-align: justify;
                font-size: 12pt;
                padding: 5px;
                margin: 0px;
            }            
            .step-son{
                margin-bottom: 10px;
                overflow: hidden;
            }
            .step-son input[type='checkbox'], .step-son label{
                margin-bottom: 10px;
                margin-left: 10px;
                float: left;
            }
            #info{
                text-align: justify;
                padding: 10px;                
            }
            #info p, h3, h4, ol li{
                padding: 10px; 
                margin: 10px 30px
            }
            h3{
                color:dodgerblue;
            }  
            .redd td{
                color: red;
            }
        </style>
    </head>
    <body style="background-color:lightgray; padding: 0px; margin: 0px">
        <div style="width: 960px; margin: 0px auto; background-color:white;"> 
            <a href="#ajaxsorter">Ajaxsorter</a>
            <a href="#boxloader">Boxloader</a>
            <!--Tabla-->
            <h1 id="ajaxsorter" style="color:#003399; text-align:center; margin-top:0px; padding-top: 0px">Tablas dinámicas con la herramienta jquery.ajaxsorter</h1>
            <div class="ajaxSorterDiv" id="tabla_a" style="">                
                <div style=" text-align:right" class='filter' >
                    Donde:
                    <select class='filter_type' title="Seleccione el campo para su filtrado"></select> 
                    es
                    <select class='filter_equ' title="Seleccione un tipo de comparacion">
                        <option value="">Igual</option>
                        <option value="like">Parecido</option>
                        <option value=">">Mayor</option>
                        <option value="<">Menor</option>
                        <option value="!=">Diferente</option>
                    </select>                               
                    a
                    <input class="filter_text" maxlength="15" type="text" title="Ingrese el texto deseado para buscar"/>                             
                    <input type="checkbox" class="keep_filter" title="Mantener filtros previos"/>                
                </div>
                <span class="ajaxTable">
                    <table id="mytabla" class="ajaxSorter">
                        <thead title="Presione las columnas para ordenarlas como requiera">
                            <th data-order = "id_caseta">ID</th>
                            <th data-order = "nombre">Nombre caseta</th>
                            <th data-order = "latitud">Latitud</th>
                            <th data-order = "longitud">Longitud</th> 
                            <th data-order = "elementos">Elementos</th>                    
                            <th data-order = "unidades">Unidades</th>                                                                               
                        </thead>
                        <tbody>                            
                        </tbody>
                    </table>
                </span>
                <div class="ajaxpager">        
                    <ul class="order_list"></ul>
                    <form method="post" action="<?= base_url()?>index.php/reporter">                          
                        <img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                        <input type="text" class="pagedisplay" size="5"/> /
                        <input type="text" class="pagedisplayMax" size="5" readonly="1" disabled="disabled"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                        <select class="pagesize">
                                <option selected="selected" value="10">10</option>
                                <option value="20">20</option>                            
                                <option value='0'>Gral</option>                                          
                        </select>                              
                        <img class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
                        <img class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />                                                                
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/>
                    </form>                    
                </div>     
            </div>    
            <div id="info" style="padding: 10px;">
                <h3>¿Que es?</h3>
                <p>
                   La herramienta se compone de una librería en javascript llamada jquery.ajaxsorter y de una librería para codeigniter llamada Ajaxsorter, además cuenta con su propia hoja de estilos que predefine una apariencia, la cual puede ser modificada a criterio. 
                </p>
                <p>
                   Esta librería permite paginar automáticamente el contenido de las tablas, además permite un filtrado que permite filtrar por medio de las columnas que visualizamos. 
                </p>
                 <h3>¿C&oacute;mo trabaja?</h3>
                <p>
                    Ajaxsorter es una librería que como su nombre lo indica, trabaja mediante peticiones Ajax, con el objetivo de mantener una paginación dinámica con registros controlados, Ajaxsorter combina la el MVC de codeingniter para trabajar, ya que esta depende de lo que se realice en el controlador y el método. 
                </p>
                <p>
                    La librería fue adaptada para trabajar con el framework codeigniter, el cual en su caso, la única adaptación realizada fue el trabajo con filtros extras, los cuales se envían mediante el método get, ya que codeigniter utiliza una manera distinta a la tradicional de recibir parámetros por el método get.
                </p>
                <h3>Dependencias</h3>
                <p>
                    Ajaxsorter depende directamente de JQuery, asi como de una carpeta con sus estilos llamada AjaxsorterStyle donde se encuentran sus iconos. Esta librería también depende de consultas elaboradas por el programador ya que necesita de cierta estructura para funcionar, pero a diferencia de otras herramientas esta estructura beneficia al programador aumentando su productividad reduciendo el tiempo en el que elabora un CRUD (créate, read, update and delete) o algunas otras tareas básicas relacionadas con la programación de módulos de catálogos.
                </p>
                <h3>Datos adicionales</h3>
                <p>
                    Para el uso de esta librería se necesita lo siguiente: 
                    <ul>
                        <li>El uso de los atributos data de JQuery con los cuales se trabaja, donde inicialmente se utiliza uno en cada elemento th dentro de nuestra tabla, los cuales son declarados como data-order=”Campo o alias del campo” y en el caso del uso de elementos internos como checkbox, es necesario además del campo relacionado indicar con data-type=”checkbox" para que se creen automáticamente elementos checkbox que servirán como selección múltiple de registros en la tabla.</li>
                        <li>Utilizar la estructura HTML proporcionada, con la intención de reducir los errores y permitir que la librería trabaje mediante clases y otro tipo de selectores, ya que esto es lo que permite su trabajo.</li>
                        <li>Para reducción de codificación, utilizar la librería Ajaxsorter en codeigniter para la recepción de los parámetros automáticos.</li>
                        <li>Mantener una estructura en el controlador y enviar un objeto de tipo JSON con los datos previos de las consultas, lo cual se explicara mas adelante.</li>
                    </ul>
                </p>
                <h3>Ejemplos</h3>
                <h4>Controlador</h4>
                <p>
                    Se carga la librería de Ajaxsorter:
                </p>
                <p><b>$this->load->library('ajaxsorter');</b></p>
                <p>Se codificara una acción como la siguiente:</p>
                <p> <b>public function tabla($extra=0){<br>        
                    //Se obtienen los paremetros basicos enviados por Ajaxsorter<br>
                    $filter = $this->ajaxsorter->filter($this->input);<br>
                    //Filtro extra recibido por GET<br>
                    $filter['num'] = $extra; <br>
                    //Obtencion de los registros en base a los parametros enviados de filtro<br>
                    $data['rows'] = $this->base_model->getUsuarios($filter);<br>
                    //Obtencion del numero de registros sin la limitacion del paginado, para calcular las paginas<br>
                    $data['num_rows'] = $this->base_model->getNumRows($filter);<br>
                    //Se envia mediante un echo los datos al Ajaxsorter<br>
                    echo '('.json_encode($data).')';          <br>      
                    }</b>
                </p>
                <p>Donde se aprecia que se esta recibiendo un parámetro llamado extra, por el método get, mediante la librería previamente cargada se obtienen los parámetros comunes que Ajaxsorter envía, se recibe en un propiedad del objeto $filter llamada num el valor de la variable enviada por el método get. </p>
                <p>Se establece un objeto con la variable $data['rows'] en este caso donde se le indica la consulta de los registros, llamando la función GetUsuarios del modelo base_model y pasándole como parámetro el filtro recibido por la clase antes mencionada, además se le envía como segunda propiedad del objeto el numero de registros total a partir de las condicionantes proporcionadas, el cual se establece con $data['num_rows'] ya que es necesario contar con la cantidad de registros que pertenecen a la consulta pero sin sus limites por pagina, es decir, que si se tienen 15 registros y la paginación tiene un limite de 10 registros, es necesario contar con el dato para paginar correctamente y en este caso indicar que son 2 paginas y no una.</p>
                <p>Los parámetros que se reciban por get, pueden variar de acuerdo a las necesidades del programador, estos pueden ser adaptados de una manera simple.</p>
                <h4>Modelo</h4>
                <p>Como antes se menciona en el modelo se necesita  2 cosas, la consulta de los registros y la consulta del número de registros, como lo muestra el siguiente ejemplo:</p>
                <p> <b>
                     Consulta de registros:<br><br>

                    function getUsuarios($filter){<br>        
                     //Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta<br>
                            if($filter['order']!='')<br>
                                $this->db->order_by($filter['order']);<br>
                    //Se verifica si existen  condiciones por medio del filtrado, de ser así se considera en la consulta<br>
                            if($filter['where']!='')<br>
                                $this->db->where($filter['where']);   <br>
                    //Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta<br>
                            if($filter['num']!=0)<br>
                                $this->db->where('unidades',$filter['num']);<br>
                    //Se realiza la consulta con una limitación, en caso de que sea valida<br>
                            If($filter['limit']!=0)<br>
                             $result = $this->db->get('casetas',$filter['limit'],$filter['offset']);          <br>
                           else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como            <br>
                             $result = $this->db->get('casetas');     <br>
                    //Se inicializa un arreglo para el caso de que la consulta retorne algo vacío     <br>
                            $data = array();        <br>
                    //Se forma el arreglo que sera retornado<br>
                            foreach($result->result() as $row):<br>
                                $data[] = $row;<br>
                            endforeach;        <br>
                            return $data;<br>
                        }<br><br>
                    Consulta de número de registros<br><br>
                    function getNumRows($filter){         <br>
                            if($filter['where']!='')<br>
                                $this->db->where($filter['where']);  //Se toman en cuenta los filtros solicitados <br>
                            if($filter['num']!=0)<br>
                                $this->db->where('unidades',$filter['num']);<br>
                            $result = $this->db->get('casetas');//En este caso no es necesario limitar los registros                      <br>
                            return $result->num_rows();//Se regresan la cantidad de registros encontrados e<br>
                        }<br>   
                    </b>
                </p>
                <h4>Vista</h4>
                <p>Se puede utilizar de distintas maneras</p>
                <ol>
                    <li>
                        Básico (Solo tabla). En esta opción únicamente se realiza la consulta directa sin limitaciones ni filtros, la cual no realizara filtros ni paginara el contenido, para lo cual se necesita lo siguiente:
                        <p>Establecer el la siguiente estructura HTML:<br>
                            <b>
                            &ltdiv class="ajaxSorterDiv" id="tabla_k"&gt<br> 
                             &ltspan class="ajaxTable"&gt<br>
                            &lttable id="myTable_y" class="ajaxSorter"&gt<br>
                               &ltthead title="Presione las columnas para ordenarlas como requiera"&gt<br>
                                    &ltth data-order = "id_caseta"&gtID&lt/th&gt<br>
                                    &ltth data-order = "nombre"&gtNombre caseta&lt/th&gt<br>
                                    &ltth data-order = "latitud"&gtLatitud&lt/th&gt<br>
                                    &ltth data-order = "longitud"&gtLongitud&lt/th&gt <br>
                                    &ltth data-order = "elementos"&gtElementos&lt/th&gt                    <br>
                                &lt/thead&gt<br>
                                &lttbody&gt                         <br>
                                &lt/tbody&gt<br>
                                &lt/table&gt              <br>  
                                &lt/span&gt<br>
                            &lt/div&gt                           
                            </b>
                        </p>
                        <p>Este HTML mostrara lo siguiente, donde únicamente se permitirá la ordenación de la tabla por columna.</p>
                        <p>
                            <div class="ajaxSorterDiv" id="tabla_k" style="">                                            
                            <span class="ajaxTable">
                                <table id="" class="ajaxSorter">
                                    <thead title="Presione las columnas para ordenarlas como requiera">
                                        <th data-order = "id_caseta">ID</th>
                                        <th data-order = "nombre">Nombre caseta</th>
                                        <th data-order = "latitud">Latitud</th>
                                        <th data-order = "longitud">Longitud</th> 
                                        <th data-order = "elementos">Elementos</th>                    
                                        <th data-order = "unidades">Unidades</th>                                                   
                                    </thead>
                                    <tbody>                            
                                    </tbody>
                                </table>
                            </span>                              
                            </div>                
                        </p>
                        <p>Note en el código HTML generado que Ajaxsorter genera variables como data-campo, las cuales podrán ser utilizadas para cargar datos,  sin embargo este puede mostrar una cantidad de variables diferente a las columnas mostradas, ya que de acuerdo a la consulta realizada, Ajaxsorter ubica cada campo recibido en la consulta en el html del tr, en cambio solo muestra aquellas que le indicamos previamente con los elementos th de nuestra tabla.</p>
                        <p>En el javascript se inicializa de esta manera:<br>
                            $("#tabla_k").ajaxSorter({url:'&lt?php echo base_url()?&gtindex.php/welcome/tabla'});  
                        </p>
                    </li>
                    <li>Básico (Tabla y paginado)<br>
                    <p>Establecer el la siguiente estructura HTML:<br>
                            <b>
                            &ltdiv class="ajaxSorterDiv" id="tabla_x"&gt<br>
                            &ltspan class="ajaxTable"&gt<br>
                            &lttable id="myTable_y" class="ajaxSorter"&gt<br>
                               &ltthead title="Presione las columnas para ordenarlas como requiera"&gt<br>
                                    &ltth data-order = "id_caseta"&gtID&lt/th&gt<br>
                                    &ltth data-order = "nombre"&gtNombre caseta&lt/th&gt<br>
                                    &ltth data-order = "latitud"&gtLatitud&lt/th&gt<br>
                                    &ltth data-order = "longitud"&gtLongitud&lt/th&gt <br>
                                    &ltth data-order = "elementos"&gtElementos&lt/th&gt                    <br>
                                &lt/thead&gt<br>
                                &lttbody&gt                         <br>
                                &lt/tbody&gt<br>
                                &lt/table&gt              <br>  
                                &lt/span&gt
                                &ltdiv class="ajaxpager"&gt <br>        
                                    &ltul class="order_list"&gt &lt/ul&gt<br>  
                                    &ltform&gt         <br>                   
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/first.png" class="first"/&gt <br> 
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/prev.png" class="prev"/&gt <br> 
                                        &ltinput type="text" class="pagedisplay" size="5"/&gt  /<br> 
                                        &ltinput type="text" class="pagedisplayMax" size="5" readonly="1" disabled="disabled"/&gt <br> 
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/next.png" class="next"/&gt <br> 
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/last.png" class="last"/&gt <br> 
                                        &ltselect class="pagesize"&gt <br> 
                                                 &ltoption selected="selected" value="3"&gt 3&lt/option&gt <br> 
                                                &ltoption value="5"&gt 5&lt/option&gt   <br> 
                                                &ltoption value="10"&gt 10&lt/option&gt                               <br> 
                                        &lt/select&gt                               <br> 
                                    &lt/form&gt  <br> 
                                &lt/div&gt       <br> 

                            &lt/div&gt                           
                            </b>
                        </p>
                        <p>El paginado es agregado con el HTML resaltado, donde es posible la declaración de las opciones de numero de registros por consulta, en este caso se tienen las opciones 3, 5 y 10.</p>
                        <p>Para la paginación  es necesario utilizar los links proporcionados o simplemente con la introducción de la hoja deseada para paginar, en el cuadro de texto que detectara si la petición es valida y de ser así, paginara el contenido.</p>
                        <p>En el javascript se inicializa de esta manera:<br>
                            $("#tabla_x").ajaxSorter({url:'&lt?php echo base_url()?&gtindex.php/welcome/tabla'});  
                        </p>
                        <p>
                           <div class="ajaxSorterDiv" id="tabla_x" style="">                                            
                            <span class="ajaxTable">
                                <table id="" class="ajaxSorter">
                                    <thead title="Presione las columnas para ordenarlas como requiera">
                                        <th data-order = "id_caseta">ID</th>
                                        <th data-order = "nombre">Nombre caseta</th>
                                        <th data-order = "latitud">Latitud</th>
                                        <th data-order = "longitud">Longitud</th> 
                                        <th data-order = "elementos">Elementos</th>                    
                                        <th data-order = "unidades">Unidades</th>                                                   
                                    </thead>
                                    <tbody>                            
                                    </tbody>
                                </table>
                            </span>
                               <div class="ajaxpager">       
                                    <ul class="order_list"></ul>
                                    <form>                          
                                        <img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                                        <img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                                        <input type="text" class="pagedisplay" size="5"/> /
                                        <input type="text" class="pagedisplayMax" size="5" readonly="1" disabled="disabled"/>
                                        <img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                                        <img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                                        <select class="pagesize">
                                                 <option selected="selected" value="3">3</option>
                                                <option value="5">5</option>  
                                                <option value="10">10</option>                              
                                        </select>                              
                                    </form>
                                </div>      
                            </div>    
                        </p>
                    </li>
                    <li>Básico (Tabla, paginado y filtro)<br>
                    <p>Establecer el la siguiente estructura HTML:<br>
                            <b>                                                           
                            &ltdiv class="ajaxSorterDiv" id="tabla_y"&gt<br>                     
                            &ltdiv style=" text-align:right" class='filter'&gt<br>
                             Buscar:<br>
                                 &ltinput class="filter_text" maxlength="15" type="text" title="Ingrese el texto deseado para buscar"/&gt<br> 
                                Filtrar por columna<br>
                                 &ltselect class='filter_type' title="Seleccione el campo para su filtrado"> &lt/select&gt<br>                               
                                 &ltinput type="checkbox" class="keep_filter" title="Mantener filtros previos"/&gt<br>                
                             &lt/div&gt<br>   
                            &lttable id="myTable_y" class="ajaxSorter"&gt<br>
                               &ltthead title="Presione las columnas para ordenarlas como requiera"&gt<br>
                                    &ltth data-order = "id_caseta"&gtID&lt/th&gt<br>
                                    &ltth data-order = "nombre"&gtNombre caseta&lt/th&gt<br>
                                    &ltth data-order = "latitud"&gtLatitud&lt/th&gt<br>
                                    &ltth data-order = "longitud"&gtLongitud&lt/th&gt <br>
                                    &ltth data-order = "elementos"&gtElementos&lt/th&gt                    <br>
                                &lt/thead&gt<br>
                                &lttbody&gt                         <br>
                                &lt/tbody&gt<br>
                                &lt/table&gt              <br>                                    
                                &ltdiv class="ajaxpager"&gt <br>        
                                    &ltul class="order_list"&gt &lt/ul&gt<br>  
                                    &ltform&gt         <br>                   
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/first.png" class="first"/&gt <br> 
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/prev.png" class="prev"/&gt <br> 
                                        &ltinput type="text" class="pagedisplay" size="5"/&gt  /<br> 
                                        &ltinput type="text" class="pagedisplayMax" size="5" readonly="1" disabled="disabled"/&gt <br> 
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/next.png" class="next"/&gt <br> 
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/last.png" class="last"/&gt <br> 
                                        &ltselect class="pagesize"&gt <br> 
                                                 &ltoption selected="selected" value="3"&gt 3&lt/option&gt <br> 
                                                &ltoption value="5"&gt 5&lt/option&gt   <br> 
                                                &ltoption value="10"&gt 10&lt/option&gt                               <br> 
                                        &lt/select&gt                               <br> 
                                    &lt/form&gt <br> 
                                &lt/div&gt       <br> 
                                &lt/div&gt       <br> 
                                                      
                            </b>
                        </p>
                        <p>El paginado es agregado con el HTML resaltado, donde es posible la declaración de las opciones de numero de registros por consulta, en este caso se tienen las opciones 3, 5 y 10.</p>
                        <p>Para la paginación  es necesario utilizar los links proporcionados o simplemente con la introducción de la hoja deseada para paginar, en el cuadro de texto que detectara si la petición es valida y de ser así, paginara el contenido.</p>
                        <p>En el javascript se inicializa de esta manera:<br>
                            $("#tabla_y").ajaxSorter({url:'&lt?php echo base_url()?&gtindex.php/welcome/tabla'});  
                        </p>
                        <p>
                            <div class="ajaxSorterDiv" id="tabla_y" style="">
                            <div style=" text-align:right" class='filter' >
                                Buscar:
                                <input class="filter_text" maxlength="15" type="text" title="Ingrese el texto deseado para buscar"/> 
                                Filtrar por columna
                                <select class='filter_type' title="Seleccione el campo para su filtrado"></select>                               
                                <input type="checkbox" class="keep_filter" title="Mantener filtros previos"/>                
                            </div>    
                            <span class="ajaxTable">
                            <table id="" class="ajaxSorter">
                                <thead title="Presione las columnas para ordenarlas como requiera">
                                    <th data-order = "id_caseta">ID</th>
                                    <th data-order = "nombre">Nombre caseta</th>
                                    <th data-order = "latitud">Latitud</th>
                                    <th data-order = "longitud">Longitud</th> 
                                    <th data-order = "elementos">Elementos</th>                    
                                </thead>
                                <tbody id="myTable_body">                            
                                </tbody>
                            </table>              
                                </span>
                            <div class="ajaxpager">        
                                <ul class="order_list"></ul>
                                <form>                          
                                    <img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                                    <img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                                    <input type="text" class="pagedisplay" size="5"/> /
                                    <input type="text" class="pagedisplayMax" size="5" readonly="1" disabled="disabled"/>
                                    <img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                                    <img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                                    <select class="pagesize">
                                            <option selected="selected" value="3">3</option>
                                            <option value="5">5</option>  
                                            <option value="10">10</option>                            
                                    </select>                              
                                </form>
                            </div>      
                        </div>
                        </p>
                        <p>Al agregar el filtrado, este cargara automáticamente las opciones de filtrado en el combo y el contenido de la tabla podrá ser filtrado a partir de lo ingresado en el buscador con respecto ala columna seleccionada, y si se presiona el checkbox para mantener los filtros previos, Ajaxsorter continuara filtrando y paginando en base a lo ingresado. Mostrando una tabla de filtros debajo de la tabla como la siguiente:</p>
                        <p>Donde como se visualiza es posible eliminar los filtros individualmente presionando doble click para eliminarlos o de manera general presionando en la parte superior “Filtros”. Ajaxsorter detectara el o los filtros eliminados y paginara automáticamente la tabla eliminando los filtros.</p>                        
                    </li>
                    <li>Personalizado <br>(Ajaxsorter completo + elementos adicionales ajenos a él)
                    En esta opción se pueden añadir elementos como input o select, con los cuales Ajaxsorter filtrara el contenido con filtros adicionales en la consulta, esto se añadió por la diversidad de consultas personalizadas que puedan utilizarse y su adaptación es muy simple.
                    La estructura puede ser cualquiera de las anteriormente mencionadas, ya que se adapta de acuerdo a lo que se elija. En este caso se muestra el siguiente ejemplo.
                    <p>
                        Unidades
                        <select id="cmbUnidad">
                            <option value="0">Todos</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="20">20</option>
                        </select>
                        <div class="ajaxSorterDiv" id="tabla_c" style="">                       
                             <span class="ajaxTable">
                            <table id="myTable_a" class="ajaxSorter">
                                <thead title="Presione las columnas para ordenarlas como requiera">
                                    <th data-order = "id_caseta">ID</th>
                                    <th data-order = "nombre">Nombre caseta</th>
                                    <th data-order = "latitud">Latitud</th>
                                    <th data-order = "longitud">Longitud</th> 
                                    <th data-order = "elementos">Elementos</th>
                                    <th data-order = "unidades">Unidades</th> 
                                </thead>
                                <tbody id="myTable_body">                            
                                </tbody>
                            </table> 
                             </span>
                            <div class="ajaxpager">        
                                <ul class="order_list"></ul>
                                <form>                          
                                    <img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                                    <img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                                    <input type="text" class="pagedisplay" size="5"/> /
                                    <input type="text" class="pagedisplayMax" size="5" readonly="1" disabled="disabled"/>
                                    <img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                                    <img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                                    <select class="pagesize">
                                             <option selected="selected" value="3">3</option>
                                            <option value="5">5</option>  
                                            <option value="10">10</option>                              
                                    </select>                              
                                </form>
                            </div>      
                        </div>  
                        Donde el elemento superior esta declarado de la siguiente manera:
                    </p>
                    <p>
                         &ltselect id="cmbUnidad"&gt<br>
                            &ltoption value="0"&gtTodos&lt/option&gt<br>
                            &ltoption value="3"&gt3&lt/option&gt<br>
                            &ltoption value="4"&gt4&lt/option&gt<br>
                            &ltoption value="20"&gt20&gt/option&gt<br>
                        &lt/select&gt<br>
                    </p>
                    <p>
                        Para la integración en javascript se necesita lo siguiente:<br>
                        $("#tabla_a").ajaxSorter({
            url:'&lt?php echo base_url()?&gtindex.php/welcome/tabla',
            filters:['cmbUnidad']            
        });

                    </p>
                    <p>Y debido a que pueden necesitarse multiples filtros adicionales el cambio sera el siguiente</p>
                    <p>$("#tabla_a").ajaxSorter({
            url:'&lt?php echo base_url()?&gtindex.php/welcome/tabla',
            filters:['cmbUnidad','combo2','combo3']            
        });</p>
                    <p>De esta manera los parametros se enviaran a traves del metodo GET en el orden establecido en el arreglo</p>
                    </li>
                     <li>Uso de checkbox <br>
                    <p>                       
                        <div class="ajaxSorterDiv" id="tabla_d" style="">                       
                             <span class="ajaxTable">
                            <table id="myTable_a" class="ajaxSorter">
                                <thead title="Presione las columnas para ordenarlas como requiera">
                                    <th data-order = "id_caseta">ID</th>
                                    <th data-order = "nombre">Nombre caseta</th>
                                    <th data-order = "latitud">Latitud</th>
                                    <th data-order = "longitud">Longitud</th> 
                                    <th data-order = "elementos">Elementos</th>
                                    <th data-order = "unidades">Unidades</th> 
                                    <th data-order = "unidades" data-type="checkbox" style="text-align: center;"><input type="checkbox"></th> 
                                </thead>
                                <tbody id="myTable_body">                            
                                </tbody>
                            </table> 
                             </span>
                            <div class="ajaxpager">        
                                <ul class="order_list"></ul>
                                <form>                          
                                    <img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                                    <img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                                    <input type="text" class="pagedisplay" size="5"/> /
                                    <input type="text" class="pagedisplayMax" size="5" readonly="1" disabled="disabled"/>
                                    <img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                                    <img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                                    <select class="pagesize">
                                             <option selected="selected" value="3">3</option>
                                            <option value="5">5</option>  
                                            <option value="10">10</option>                              
                                    </select>                              
                                </form>
                            </div>      
                        </div>  
                        Donde se añade lo siguiente en el th ademas del campo relacionado, se añade data-type, para indicar que este campo sera checkbox
                    </p>
                    <p>
                        &ltth data-order = "unidades" data-type="checkbox" style="text-align: center;"&gt&ltinput type="checkbox"&gt&lt/th&gt
                    </p>
                    <p>
                        Para la integración en javascript se necesita lo siguiente:<br>
                        $("#tabla_a").ajaxSorter({<br>
            url:'&lt?php echo base_url()?&gtindex.php/welcome/tabla',<br>
            filters:['cmbUnidad']            <br>
        });

                    </p>
                                        
                    </li>
                    <li>Filtros particulares, Exportacion de tabla a Excel, Opcion de imprimir directa(Nuevo)
                        <p>
                            <b>                                 
                                &ltinput class="filter_text" maxlength="15" type="text" title="Ingrese el texto deseado para buscar"/&gt                             <br>
                    &ltinput type="checkbox" class="keep_filter" title="Mantener filtros previos"/&gt<br>
                            &ltdiv class="ajaxSorterDiv" id="tabla_y"&gt<br>                     
                            &ltdiv style=" text-align:right" class='filter'&gt<br>
                             Donde:
                    &ltselect class='filter_type' title="Seleccione el campo para su filtrado"&gt&lt/select&gt <br>
                            es
                            &ltselect class='filter_equ' title="Seleccione un tipo de comparacion"&gt<br>
                                &ltoption value=""&gtIgual&lt/option&gt<br>
                                &ltoption value="like"&gtParecido&lt/option&gt<br>
                                &ltoption value=">"&gtMayor&lt/option&gt<br>
                                &ltoption value="<"&gtMenor&lt/option&gt<br>
                                &ltoption value="!="&gtDiferente&lt/option&gt<br>
                            &lt/select&gt                               <br>
                            a
                             &lt/div&gt<br>   
                            &lttable id="myTable_y" class="ajaxSorter"&gt<br>
                               &ltthead title="Presione las columnas para ordenarlas como requiera"&gt<br>
                                    &ltth data-order = "id_caseta"&gtID&lt/th&gt<br>
                                    &ltth data-order = "nombre"&gtNombre caseta&lt/th&gt<br>
                                    &ltth data-order = "latitud"&gtLatitud&lt/th&gt<br>
                                    &ltth data-order = "longitud"&gtLongitud&lt/th&gt <br>
                                    &ltth data-order = "elementos"&gtElementos&lt/th&gt                    <br>
                                &lt/thead&gt<br>
                                &lttbody&gt                         <br>
                                &lt/tbody&gt<br>
                                &lt/table&gt              <br>                                    
                                &ltdiv class="ajaxpager"&gt <br>        
                                    &ltul class="order_list"&gt &lt/ul&gt<br>  
                                    &ltform&gt         <br>                   
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/first.png" class="first"/&gt <br> 
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/prev.png" class="prev"/&gt <br> 
                                        &ltinput type="text" class="pagedisplay" size="5"/&gt  /<br> 
                                        &ltinput type="text" class="pagedisplayMax" size="5" readonly="1" disabled="disabled"/&gt <br> 
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/next.png" class="next"/&gt <br> 
                                        &ltimg src="&lt?php echo base_url();?&gt assets/img/sorter/last.png" class="last"/&gt <br> 
                                        &ltselect class="pagesize"&gt <br> 
                                                 &ltoption selected="selected" value="3"&gt 3&lt/option&gt <br> 
                                                &ltoption value="5"&gt 5&lt/option&gt   <br> 
                                                &ltoption value="10"&gt 10&lt/option&gt                               <br> 
                                        &lt/select&gt                               <br> 
                                        &ltimg class="printer" src="&lt?= base_url()?&gtassets/img/sorter/print.png"  data-url="&lt?= base_url()?&gt"   style="float: right"&gt
                                        &ltimg class="exporter" src="&lt?= base_url()?&gtassets/img/sorter/export.png" style="float: right; margin-right:5px;"&gt                        
                                        &ltinput type="hidden" name="tabla" value ="" class="htmlTable"/&gt
                                    &lt/form&gt <br> 
                                &lt/div&gt       <br> 
                                &lt/div&gt       <br> 
                                                      
                            </b>
                        </p>
                        <p>
                             <div class="ajaxSorterDiv" id="tabla_b" style="">                
                                <div style=" text-align:right" class='filter' >
                                    Donde:
                                    <select class='filter_type' title="Seleccione el campo para su filtrado"></select> 
                                    es
                                    <select class='filter_equ' title="Seleccione un tipo de comparacion">
                                        <option value="">Igual</option>
                                        <option value="like">Parecido</option>
                                        <option value=">">Mayor</option>
                                        <option value="<">Menor</option>
                                        <option value="!=">Diferente</option>
                                    </select>                               
                                    a
                                    <input class="filter_text" maxlength="15" type="text" title="Ingrese el texto deseado para buscar"/>                             
                                    <input type="checkbox" class="keep_filter" title="Mantener filtros previos"/>                
                                </div>
                                <span class="ajaxTable">
                                    <table id="" class="ajaxSorter">
                                        <thead title="Presione las columnas para ordenarlas como requiera">
                                            <th data-order = "id_caseta">ID</th>
                                            <th data-order = "nombre">Nombre caseta</th>
                                            <th data-order = "latitud">Latitud</th>
                                            <th data-order = "longitud">Longitud</th> 
                                            <th data-order = "elementos">Elementos</th>                    
                                            <th data-order = "unidades">Unidades</th>                                                                                               
                                        </thead>
                                        <tbody>                            
                                        </tbody>
                                    </table>
                                </span>
                                <div class="ajaxpager">        
                                    <ul class="order_list"></ul>
                                    <form method="post" action="<?= base_url()?>index.php/reporter">                          
                                        <img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                                        <img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                                        <input type="text" class="pagedisplay" size="5"/> /
                                        <input type="text" class="pagedisplayMax" size="5" readonly="1" disabled="disabled"/>
                                        <img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                                        <img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                                        <select class="pagesize">
                                                <option selected="selected" value="10">10</option>
                                                <option value="20">20</option>                            
                                                <option value='0'>Gral</option>                                          
                                        </select>                              
                                        <img class="exporter" src="<?= base_url()?>assets/img/sorter/export.png" />
                        <img class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />                                                                                       
                                        <input type="hidden" name="tabla" value ="" class="htmlTable"/>
                                    </form>                    
                                </div>     
                            </div>    
                        </p>
                    </li>
                </ol>
                <h4>Opciones adicionales</h4>
                <p>En el caso de que se busque actualizar la tabla por algún proceso ajeno a la funcionalidad de Ajaxsorter, el cual podría ser una agregación, modificación o eliminación de un registro, al finalizar con dicha tarea se ejecutara un $("selector").trigger("update");, para que se actualice el contenido.</p>
                <p>Cuando se inicialice un Ajaxsorter y se busque que no realice la consulta si no hasta que se le indique con un elemento ajeno (un select o input relacionado) o  después de un proceso como se menciono anteriormente. Se añadirá lo siguiente al inicializar:</p>
                <p>
                    $("#tabla_a").ajaxSorter({<br>
            url:'&lt?php echo base_url()?&gtindex.php/welcome/tabla',<br>
            filters:['cmbUnidad']  ,<br>
	onLoad:false          <br>
        });<br>
                
                </p>
                <p>Para resaltar los elementos que tenga un campo igual a un valor (tabla superior), esto significa que la tabla resaltara automaticamente los renglones que tengan 3 unidades.</p>
                <p>
                      $("#tabla_a").ajaxSorter({<br>
                        url:'&lt?php echo base_url()?&gtindex.php/welcome/tabla',<br>
                        filters:['cmbUnidad']  ,<br>
                    active:['unidades',3]          <br>
                    });<br> 
                </p>
                <p>Para el funcionamiento correcto verifique la existencia de los siguientes archivos
                    <ul>
                        <li>Ajaxsorter.php - Ubicado en la carpeta applications/libraries</li>
                        <li>jquery.ajaxsorter.js - Ubicado en la carpeta assets/js</li>
                        <li>Carpeta ajaxSorterStyle - Ubicado en la carpeta assets/css</li>
                    </ul>
                </p>
                <h4>Notas:</h4>
                <ul>
                    <li>Para el trabajo con las filas de la tabla por el metodo click, añada onRowClick:function(){ //SU CODIGO} como en el siguiente ejemplo con el que funciona la tabla superior<br>
                         $("#tabla_a").ajaxSorter({<br>
                            url:'&lt?php echo base_url()?&gtindex.php/welcome/tabla',<br>
                            active:['unidades',3],       <br>
                            onRowClick:function(){<br>
                                alert($(this).data('nombre'));<br>
                            }<br>
                        });
						<p>Esto enviara una alerta mostrando el campo nombre de la fila seleccionada</p>
                    </li>
                </ul> 
                 <h1 id="boxloader" style="color:#003399; text-align:center; margin-top:0px; padding-top: 0px">Combos dinamicos con jquery.boxloader</h1>
                 <h3>¿Que es?</h3>
                 <p>Boxloader es una herramienta que como Ajaxsorter combina Jquery, codeigniter(php) y html b&aacute;sico para la creaci&oacute;n de combobox con valores din&aacute;micos. La idea general se presento en base a la carga consecutiva de otros combos, es decir, que un combo hijo se cargara a partir de un padre.</p>
                 <h3>¿Como trabaja?</h3>
                 <p>Boxloader es una librer&iacute;a desarrollada con jquery, la cual realiza peticiones ajax hacia una acci&oacute;n de un controlador que regresa un objeto de codificaci&oacute;n JSON el cual es decifrado por la libreria y realiza un acomodo de esos datos en la estructura html que se le añadir&aacute; al combo.</p>
                 <h3>Dependencias</h3>
                 <p>Como se menciono anteriormente, boxloader depende de jquery, ya que esta librer&iacute;a provee las funcionalidades que necesita para trabajar.</p>
                 <h3>Ejemplos</h3>
                 <h4>Controlador</h4>
                 <p>Debido a que es una libreria simple, esta no necesita una configuraci&oactue;n adicional como Ajaxsorter y en el controlador se establecere de una manera similar a la siguiente:</p>
                 <p>
                     Una funci&oacute;n comun donde se recibe por post (en caso de ser necesario) una varriable que se indica mediante el codigo javascript en la vista.<br>
                     public function combo(){<br>        
                        $filter['id_unidad']=$this->input->post('id_unidad');//EN SU CASO           <br>
                        $data = $this->base_model->getElements($filter);//SE MANDA A LLAMAR LA FUNCION DEL MODELO QUE REGRESARA LA CONSULTA.        <br>
                        echo '('.json_encode($data).')';//SE ENVIA UN ECHO CON LOS DATOS CODIFICADOS CON JSON_ENCODE              <br>
                    }  <br>
                 </p>
                 <h4>Modelo</h4>
                 <p>En el modelo no se tiene que realizar algo espec&iacute;fico, ya que es una simple consulta. En este caso se le da un filtro llamado 'id_unidad'.</p>
                 <p>
                      function getElements($where){<br>        
                        $this->db->select("id, nombre as val"); //SE SELECCIONAN LOS DATOS Y SI ES DESEADO SE LES DA UN ALIAS       <br>
                        if($where['id_unidad']!=0)//ESTA CONDICION SE UTILIZARA CUANDO SEA NECESARIA, ES DECIR, DEPENDIENDO DE COMO SE INICIALIZE EN LA VISTA EL COMBO.<br>
                            $this->db->where($where);  <br>          
                        $result=$this->db->get('departamentos');//SE CONSULTA.<br>
                        $data = array();//SE CREA UN ARREGLO VACIO        <br>
                        foreach($result->result() as $row):<br>
                            $data[] = $row;//SE LLENA EL ARREGLO CON EL RESULTADO DE LA CONSULTA<br>
                        endforeach;       <br>
                        return $data;//REGRESAMOS EL ARREGLO<br>
    }                   }<br>
                 </p>
                 <h4>Vista</h4>
                 <p>Pueden ser diversas formas;</p>
                 <ol>
                     <li>Solo URL. En esta opcion solo basta indicar la url donde se obtendran los datos.
                         <p>HTML</p>
                         <p> &ltselect id="cmbDepartamentos1"&gt&lt/select&gt</p>                        
                         <p>Javascript:</p>
                         <p>
                             $('#cmbDepartamentos1').boxLoader({<br>
                                url:'&lt?php echo base_url()?&gtindex.php/welcome/combo'  <br>         
                             });<br>
                         </p>
                          <p>Resultado</p>
                         <p>
                             <select id="cmbDepartamentos1"></select>
                         </p>
                     </li>
                     <li>Con la opcion TODOS(determinada por el usuario, valor 0). En esta opci&oacute;n se permite indicar un primer elemento, el cual puede ser la instrucci&oacute;n que indique al usuario que hacer, un claro ejemplo seria: "Seleccione una opcion", al declarar esto el valor directo correspondera a un cero.
                         <p>HTML</p>
                         <p>&ltselect id="cmbDepartamentos2"&gt&lt/select&gt</p>                        
                         <p>Javascript:</p>
                         <p>
                              $('#cmbDepartamentos2').boxLoader({<br>
                                url:'&lt?php echo base_url()?&gtindex.php/welcome/combo',<br>
                                all:"Seleccione una opcion"    <br>       
                             });<br>
                         </p>
                          <p>Resultado</p>
                         <p>
                             <select id="cmbDepartamentos2"></select>
                         </p>
                     </li>
                     <li>Con la seleccion de los campos value y texto que se mostraran (default id, val). 
                         Cuando se realiza una consulta, los nombres de los campos de la base de datos, tienden a cambiar de acuerdo a las necesidades, es por ello que se da la opcion de establecer estos valores
                         en este caso, boxloader utliza id para indicar en el atributo value cual sera el valor de dicha opcion, y utiliza val para lo que se mostrara.
                         Sin embargo esto no implica que tenga que ser asi ya que se puede indicar como se llama el campo para value y el campo para mostrar manualmente como se muestra en el siguiente ejemplo.
                         <p>HTML</p>
                         <p>&ltselect id="cmbDepartamentos3"&gt&lt/select&gt</p>                                
                         <p>Javascript:</p>
                         <p>
                             $('#cmbDepartamentos3').boxLoader({<br>
                                url:'&lt?php echo base_url()?&gtindex.php/welcome/combo',<br>
                                all:"Seleccione una opcion", <br>
                                select:{id:'id',val:'val'} //AQUI SE INDICAN LOS CAMPOS A MOSTRAR, YA QUE EL EJEMPLO LOS USA DE ESTA MANERA NO SE REALIZAN CAMBIOS (VEASE EN EL MODELO) <br>          
                             });<br>
                             SI EL CAMPO LLAVE, TUVIERA EL NOMBRE DE 'CLAVE' Y EL CAMPO A MOSTRAR TUVIESE EL NOMBRE DE 'DESCRIPCION', EL CAMBIO FUESE DIRECTAMENTE A select:{id:'CLAVE',val:'DESCRIPCION'}
                         </p>
                         <p>Resultado</p>
                         <p>
                             <select id="cmbDepartamentos3"></select>
                         </p>
                     </li>
                     <li>Con una igualdad de un elemento input o select (change). Cambio de hijos en base a un padre.
                          <p>HTML</p>
                            <p>Se planteo un ejemplo fijo para que sea posible vizualizar las diversas opciones.<br>
                               &ltselect id="cmbUnidad2"&gt<br>
                                &ltoption value="0"&gt TODOS&lt/option&gt<br>
                                &ltoption value="1"&gt 1&lt/option&gt<br>
                                &ltoption value="2"&gt 2&lt/option&gt<br>
                                &ltoption value="3"&gt 3&lt/option&gt<br>
                                &ltoption value="4"&gt 4&lt/option&gt<br>
                                &ltoption value="5"&gt 5&lt/option&gt<br>
                            &lt/select&gt<br>
                            &ltselect id="cmbDepartamentos"&gt&lt/select&gt <br>
                         </p>                         
                         <p>Javascript:</p>
                         <p>
                             $('#cmbDepartamentos').boxLoader({<br>
                                url:'&lt?php echo base_url()?&gtindex.php/welcome/combo',<br>
                                equal:{id:'id_unidad',value:'#cmbUnidad2'},//AQUI SE DEFINE EL PADRE, COMO SE PUEDE APRECIAR, PRIMERAMENTE SE LE INDICA
                                EL ID CON EL CUAL SE RELACIONARA EL HIJO Y DESPUES CON VALUE SE LE INDICA EL ID DEL PADRE.<br>
                                select:{id:'id',val:'val'},<br>
                                all:"Seleccione una opcion" <br>                     
                             });  <br>
                         </p>
                         <p>Resultado</p>
                         <p>
                             <select id="cmbUnidad2">
                                <option value="0">Todos</option>
                                <option value="1">Unidad 1</option>
                                <option value="2">Unidad 2</option>
                                <option value="3">Unidad 3</option>
                                <option value="4">Unidad 4</option>
                                <option value="5">Unidad 5</option>
                            </select>
                            <select id="cmbDepartamentos"></select> 
                         </p>
                     </li>
                 </ol>  
                 <p>
                 <h3>Opciones adicionales:</h3>
                 <p>Asi como Ajaxsorter, Boxloader cuenta con la opcion, onLoad, la cual permite que el elemento no se carge inicialmente, si no hasta que se le indique con un $('selector').trigger('update'); o hasta que un padre cambie.</p>
                 </p>
                 <p>Agregar dicha opcion al codigo es algo muy simple, como se puede apreciar en el siguiente ejemplo de javascript.</p>
                 <p>
                      $('#cmbDepartamentos').boxLoader({<br>
                                url:'&lt?php echo base_url()?&gtindex.php/welcome/combo',<br>
                                equal:{id:'id_unidad',value:'#cmbUnidad2'},//AQUI SE DEFINE EL PADRE, COMO SE PUEDE APRECIAR, PRIMERAMENTE SE LE INDICA
                                EL ID CON EL CUAL SE RELACIONARA EL HIJO Y DESPUES CON VALUE SE LE INDICA EL ID DEL PADRE.<br>
                                select:{id:'id',val:'val'},<br>
                                all:"Seleccione una opcion", <br>
                                onLoad:false<br>
                             });  <br>
                 </p>
                <div style="text-align: center;">
                    <h3>Descargar ejemplo Completo</h3>
                    <p>Ultima modificación (incluyeFiltros particulares, Exportacion de tabla a Excel, 
                        Opcion de imprimir directa), boxloader y base de datos de prueba. <a href="<?= base_url()?>../Softguare/AjaxSorter.rar">Descargar</a></p>
                </div>
            </div>           
        </div>
    </body>
</html>
<script>
    $(document).ready(function(){          
        //AJAXSORTER
        $("#tabla_a").ajaxSorter({
            url:'<?php echo base_url()?>index.php/welcome/tabla',
            active:['unidades',3],       
            onRowClick:function(){
                alert($(this).data('nombre'));
            }
        });    
        $("#tabla_b").ajaxSorter({
            url:'<?php echo base_url()?>index.php/welcome/tabla'       
        });
        $("#tabla_d").ajaxSorter({
            url:'<?php echo base_url()?>index.php/welcome/tabla'       
        });
        $("#tabla_k").ajaxSorter({
            url:'<?php echo base_url()?>index.php/welcome/tabla'       
        });
        $("#tabla_x").ajaxSorter({
            url:'<?php echo base_url()?>index.php/welcome/tabla'       
        });
        $("#tabla_y").ajaxSorter({
            url:'<?php echo base_url()?>index.php/welcome/tabla'       
        });
         $("#tabla_c").ajaxSorter({
            url:'<?php echo base_url()?>index.php/welcome/tabla',
            filters:['cmbUnidad']            
        }); 

        //BOX LOADER
        $('#cmbDepartamentos1').boxLoader({
            url:'<?php echo base_url()?>index.php/welcome/combo'           
         });
          $('#cmbDepartamentos2').boxLoader({
            url:'<?php echo base_url()?>index.php/welcome/combo',
            all:"Seleccione una opcion"           
         });
         $('#cmbDepartamentos3').boxLoader({
            url:'<?php echo base_url()?>index.php/welcome/combo',
            all:"Seleccione una opcion", 
            select:{id:'id',val:'val'}           
         }); 
        $('#cmbDepartamentos').boxLoader({
            url:'<?php echo base_url()?>index.php/welcome/combo',
            equal:{id:'id_unidad',value:'#cmbUnidad2'},
            select:{id:'id',val:'val'},
            all:"Seleccione una opcion"                      
         });   
    });   
</script>