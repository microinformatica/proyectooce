<?php $this->load->view('header_cv'); ?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   

</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/banco.png" width="25" height="25" border="0"> Depósitos </li>		
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion" style="font-size: 12px">
			<h3 align="left" style="font-size: 12px" ><a href="#"> Registrados </a></h3>
        	<div  style="height:345px; text-align: right; font-size: 12px;"> 
        		<div class="ajaxSorterDiv" id="tabla_dep" name="tabla_dep" style="margin-top: 1px; height: 387px; " >
        		<!--<div class="ajaxSorterDiv" id="tabla_rem" name="tabla_rem" style="margin-top: -20px; height: 407px;"  > --> 
        		<div style=" text-align:center; font-size: 12px;" class='filter' >
                	Del Día <input size="12%" type="text" name="dpDesde" id="dpDesde" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >               	
					Al 	<input size="12%" type="text" name="dpHasta" id="dpHasta"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >  
                	Buscar donde:
                    <select class='filter_type' title="Seleccione el campo para su filtrado" style="font-size: 10px; height: 23px" ></select> 
                    es
                    <select class='filter_equ' title="Seleccione un tipo de comparacion" style="font-size: 10px; height: 23px">
                        <option value="">Igual</option>
                        <option value="like">Parecido</option>
                        <option value=">">Mayor</option>
                        <option value="<">Menor</option>
                        <option value="!=">Diferente</option>
                    </select>                               
                    a
                    <input id="buscar" name="buscar" class="filter_text" maxlength="15" type="text" title="Ingrese el texto deseado para buscar"/>                             
                    <input type="checkbox" class="keep_filter" title="Mantener filtros previos"/>
                    <!--<button id="filtrar" onclick=""> Buscar</button> 
                    <script>
                    	$('#filtrar').click(function(){
                    		$('#tabla_dep').trigger('update')
                    	});
                    </script>-->            
                </div>                                                          
            	                
                	<span class="ajaxTable" style="height: 290px; width: 882px; margin-top: 1px">
                    	<table id="mytablaD" name="mytablaD" class="ajaxSorter" width="880px">
                        	<thead title="" >                            
                            	<th data-order = "Fecha1" style="width: 70px;">Fecha</th>
                           		<th data-order = "Razon" style="width: 470px;">Razon Social</th>                                                        							                                                                                  
                            	<th data-order = "Pesos2" style="width: 70px;">M.N.</th>
                            	<th data-order = "ImporteD2" style="width: 70px;">U.S.D.</th>                           
                        	</thead>
                        	<tbody title="Seleccione para realizar cambios" style="font-size: 11px">  	</tbody>
                    	</table>
                	</span> 
             		<div class="ajaxpager" style="margin-top: -5px">        
                    	<ul class="order_list"></ul>     
                    	<form method="post" action="<?= base_url()?>index.php/depositos/pdfrep" >                    	             	                                                
                        <img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                        <input type="text" class="pagedisplay" size="3" /> /
                        <input type="text" class="pagedisplayMax" size="3" readonly="1" disabled="disabled"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                        <img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                        <select class="pagesize" style="font-size: 10px; height: 23px">
                                <option selected="selected" value="10">10</option>
                                <option value="20">20</option>                            
                                <option value="0">Gral</option>                                          
                        </select>                                                
                        <img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 					</form>    
 					</div>                                      
            </div>    
        </div>
       <?php if($usuario=="Zuleima Benitez" or $usuario=="Lety Lizárraga"){ ?>
        <h3 align="left" style="font-size: 12px"><a href="#">Agregar, Actualizar o Eliminar</a></h3>
        <?php } else {?>
        <h3 align="left" style="font-size: 12px"><a href="#">Analizar</a></h3>	
        <?php } ?>
       	<div style="height:150px; ">  
        	<table style="width: 730px;" border="2px" >
							<thead style="background-color: #DBE5F1" >
								<th colspan="4" height="15px" style="font-size: 20px">Datos de Depósito
								- Venta de Producto
								<select name="bormaq" id="bormaq" style="font-size: 10px; height: 23px">
                                	<option value="0">Sel.</option>
                                	<option value="1">Bordo</option>                            
                                	<option value="2">Procesado</option>                                          
                        		</select>  
								<input size="8%" type="hidden" name="id" id="id"/>								
								<input size="8%" type="hidden" name="numcli" id="numcli"/>
								<th colspan="2">
									<center>
									<?php if($usuario=="Zuleima Benitez" or $usuario=="Lety Lizárraga" or $usuario=="Efrain Lizárraga"){ ?>
									<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />									
									<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
									<input style="font-size: 14px" type="submit" id="borrar" name="borrar" value="Borrar" />
									<?php }?>	
									</center>	
								</th>
							</thead>
							<tbody style="background-color: #F7F7F7">
								<tr>
									<th><label style="margin: 2px; font-size: 14px" for="nombre">Razón Social</label></th>
									<th colspan="5" style="font-size: 14px; background-color: white"><input style="border: 1" readonly="true" size="88%"  type="text" name="nombre" id="nombre" ></th>
								</tr>
								<tr>
									<th><label style="margin: 2px; font-size: 14px;" for="rem">Zona</label></th>
									<th><label style="margin: 2px; font-size: 14px" for="txtFecha">Fecha</label></th>
									<th><label style="margin: 2px; font-size: 14px" for="mn">Pesos $</label></th>
									<th><label style="margin: 2px; font-size: 14px;" for="usd" >Dólares $</label></th>	
									<th><label style="margin: 2px; font-size: 14px" for="tc">Tipo de Cambio</label></th>
									<th><label style="margin: 2px; font-size: 14px" for="cta">Cuenta</label></th>																	
									
																			
								</tr>
    							<tr style="font-size: 14px; background-color: white;">
									<th rowspan="4" style="font-size: 12px;" ><input readonly="true" size="10%" type="text" name="zon" id="zon" style="text-align: center; border: none; color: red;" ></th>	
									<th><input size="14%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="text-align: center; margin-left: 10px" ></th>								
									<th><input size="14%" type="text" name="mn" id="mn" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right; margin-left: 10px"></th>
									<th><input size="14%" type="text" name="usd" id="usd" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" style="text-align: right; margin-left: 10px"></th>									
									<th><input size="8%" type="text" name="tc" id="tc" style="text-align: right; margin-left: 10px"></th>
									<th><input size="10%" type="text" name="cta" id="cta" value="" style="text-align: center; margin-left: 10px"></th>
									
								</tr>
    							
								<tr>									
									<th><label style="margin: 2px; font-size: 14px" for="pro">Aplicar a Ciclo</label></th>									
									<th colspan="4"><label style="margin: 2px; font-size: 14px" for="obs">Observaciones</label></th>
								</tr>
    							<tr style="background-color: white">
									<th style="font-size: 13px;padding-bottom: 1px;">									  																	
									    <select name="lstCiclo" id="lstCiclo" style="font-size: 10px; margin-top: 1px; margin-left: 12px; height: 23px;" >
											<?php $ciclof=2017; $actual=date("Y"); 
											$mes=date("m");//$actual-=1;
											//if($mes>=11) $actual+=1;
											$actual=2022;
											while($actual >= $ciclof){?>
												<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            									<?php $actual-=1; } ?>
          								</select>   								  
									</th>
									<th colspan="4" style="font-size: 11px;"><input size="73%" type="text" name="obs" id="obs" value="" style="text-align: left;"></th>
								</tr>  
							</tbody> 
							<tfoot style="background-color: #DBE5F1">
								<tr>
									<th colspan="6"><center><input style="font-size: 14px" type="submit" id="x" name="x" value="Cerrar" onclick="cerrar()" /></center></th>
										
								</tr>
								<?php if($usuario=="Zuleima Benitez" or $usuario=="Lety Lizárraga" or $usuario=="Efrain Lizárraga"){ ?>
								<tr height="140px">
							<th colspan="6" >
    							<div class="ajaxSorterDiv" id="tabla_cli" name="tabla_cli" style=" margin-top: 1px;">                
               			          <span class="ajaxTable" style=" height: 130px; background-color: white">
                    				<table id="mytabla" name="mytabla" class="ajaxSorter">
                    					<thead title="Presione las columnas para ordenarlas como requiera">    
                    						<th data-order = "referencia" >Referencia</th>                        
                    						<th data-order = "Razon" >Razon Social</th>                                                                                    
                    					</thead>
                    					<tbody title="Seleccione para asignar cliente a Depósito">                                                 	  
                    					</tbody>
                    				</table>
                				 </span> 
            	 				</div>            											
    			           </th>
						</tr>
						<?php }?>
							</tfoot> 						
    				</table>
        </div>	
	 </div> 	
 	</div> 	
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
function cerrar(){
	$("#accordion").accordion( "activate",0 );
}
$("#nuevo").click( function(){	
	$("#id").val('');
	$("#txtFecha").val('');
    //$("#rem").val($("#rem1").val());    
	$("#nombre").val('');
	$("#zon").val('');
	$("#mn").val('');
	$("#usd").val('');
	$("#tc").val('');
	$("#cta").val('');
	$("#obs").val('');	
	$("#lstCiclo").val('');
	$("#numcli").val('');	
	$("#bormaq").val('0');
	$("#nombre").focus();
 return true;
});

function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#borrar").click( function(){	
	numero=$("#id").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/depositos/borrar", 
						data: "id="+$("#id").val()+"&usd="+$("#usd").val()+"&nrc="+$("#numcli").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Depósito Eliminado correctamente");
										$("#nuevo").click();
										$("#mytablaD").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Depósito para poder Eliminarlo");
		$("#accordion").accordion( "activate",0 );
		return false;
	}
	
	 // return false;
});


$("#aceptar").click( function(){	
	nombre=$("#nombre").val();
	fec=$("#txtFecha").val();
	mn=$("#mn").val();
	usd=$("#usd").val();
	numero=$("#id").val();
	bm=$("#bormaq").val();
	if(mn==""){ mn=0; }
	if(usd==""){ usd=0; }
	if(bm!=0){
	if(nombre!=''){
		if( fec!=''){
		  if(mn!=0 || usd!=0){	
			if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/depositos/actualizar", 
						data: "id="+$("#id").val()+"&fec="+$("#txtFecha").val()+"&usd="+$("#usd").val()+"&tc="+$("#tc").val()+"&cta="+$("#cta").val()+"&obs="+$("#obs").val()+"&mn="+$("#mn").val()+"&nrc="+$("#numcli").val()+"&ciclo="+$("#lstCiclo").val()+"&bormaq="+$("#bormaq").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#nuevo").click();
										$("#mytablaD").trigger("update");
										$("#accordion").accordion( "activate",0 );																											
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/depositos/agregar", 
						data: "&fec="+$("#txtFecha").val()+"&usd="+$("#usd").val()+"&tc="+$("#tc").val()+"&cta="+$("#cta").val()+"&obs="+$("#obs").val()+"&mn="+$("#mn").val()+"&nrc="+$("#numcli").val()+"&ciclo="+$("#lstCiclo").val()+"&bormaq="+$("#bormaq").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Depósito registrado correctamente");
										$("#nuevo").click();
										$("#mytablaD").trigger("update");
										$("#accordion").accordion( "activate",0 );												
									}else{
										alert("Error con la base de datos o usted no ha ingresado nada");
									}					
								}		
				});
			}
			}else{
		alert("Error: Necesita registrar cantidad en Pesos o Dólares");
		$("#mn").focus();
		return false;
		}			
		}else{
			alert("Error: Fecha no válida");	
			$("#txtFecha").focus();
				return false;
		}
	}else{
		alert("Error: Nombre de Cliente no válido");
		$("#nombre").focus();
			return false;
	}
	}else{
		alert("Error: Seleccione la entrada del depósito por Venta de Producto");
		$("#bormaq").focus();
			return false;
	}
	 // return false;
});

$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#dpDesde").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#dpHasta").datepicker( "option", "minDate", selectedDate );
		$('#tabla_dep').trigger('update')
	}
});
$("#dpHasta").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#dpDesde").datepicker( "option", "maxDate", selectedDate );
		$('#tabla_dep').trigger('update')
	}
});		
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

$(document).ready(function(){     
	/*$.loader({
			className:"black",
			content:''
	});   */
    $("#nuevo").click(); 
    $("#tabla_dep").ajaxSorter({
	url:'<?php echo base_url()?>index.php/depositos/tabla',  
	filters:['dpDesde','dpHasta'],
    //active:array(['Tipo',3],['Tipo',2]),
    //active:['Recibida','√'],
    //active:['Tipo',2],   
    //             
    onRowClick:function(){
    	if($(this).data('nd')>0){
        	$("#accordion").accordion( "activate",1 );
       		$("#id").val($(this).data('nd'));
       		$("#txtFecha").val($(this).data('fecha'));
       		$("#zon").val($(this).data('zona'));
			$("#nombre").val($(this).data('razon'));
			$("#mn").val($(this).data('pesos'));
			$("#usd").val($(this).data('imported'));
			$("#tc").val($(this).data('tc'));
			$("#cta").val($(this).data('cuenta'));
			$("#lstCiclo").val($(this).data('aplicar'));
			$("#obs").val($(this).data('obs'));		
			$("#numcli").val($(this).data('nrc'));
			$("#bormaq").val($(this).data('bormaq'));
		}		
    },
    onSuccess:function(){
    		mn = 0;
    		importe = 0;
    		//if($(this).data('pesos')!=undefined){
	    		$('#tabla_dep tbody tr').map(function(){
	    			mn+=($(this).data('pesos1')!=' '?parseFloat($(this).data('pesos1')):0);
	    			importe+=($(this).data('imported1')!=' '?parseFloat($(this).data('imported1')):0);
	    			//if($(this).data('status')==1)
	    				//$(this).css('background','red');	    		
	    		});
    		//}
    		tr='<tr><td colspan=2>Totales</td><td align=right>$ '+(parseFloat(mn)).toFixed(2)+'</td><td align=right>$ '+(parseFloat(importe)).toFixed(2)+'</td></tr>';
	    	$('#tabla_dep tbody').append(tr);
	    	$('#tabla_dep tbody tr td:nth-child(1)').map(function(){ $(this).css('text-align','center'); })
	    	$('#tabla_dep tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    	$('#tabla_dep tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right'); })
    	}, 
    sort:false,
	});

        $("#tabla_cli").ajaxSorter({
			url:'<?php echo base_url()?>index.php/depositos/tablaclientes', 
			multipleFilter:true, 		
			onRowClick:function(){
    		    $("#numcli").val($(this).data('numero'));
				$("#nombre").val($(this).data('razon'));
				$("#zon").val($(this).data('zona'));
		    }	
		});
	//$.loader('close');
});
</script>