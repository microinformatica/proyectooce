<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts/js/modules/exporting.js"></script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	font-size: 25px;		
}   

</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#todos"><img src="<?php echo base_url();?>assets/images/menu/circulocalidad.png" width="25" height="25" border="0"> Circulos de Calidad</a> </li>
		
					
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="todos" class="tab_content"  >	
			<table border="0" style="margin-top: -5px">
				<tr>
					<th>
						<div class="ajaxSorterDiv" id="tabla_est" name="tabla_est" style="height: 455px;width: 640px; ">                
                		<div style=" text-align:left; font-size: 12px;" class='filter'  >
                			<div align="left">Responsabilidades de                			
    							<?php if($usuario=="Enrique Sánchez"){ $usu='Produccion';}
    							if($usuario=="Daniel Lizárraga"){ $usu='Logistica';} 
    							if($usuario=="Leonardo Sainz"){ $usu='Genetica';}	
    							if($usuario=="Joaquin Domínguez"){ $usu='Mantenimiento-I';}	
    							if($usuario=="Miguel Mendoza"){ $usu='Mantenimiento-II';} ?>
    							<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez" || $usuario=="Perla Peraza"){ $usu='Todos';?>
                		       	- Depto:                    		
								<select id="cmbDepto" style="font-size: 10px; height: 25px; width: 120px " >								
    								<option value="Todos" >Todos</option><option value="-" >-</option> 
									<option value="Administracion" >Administración</option><option value="Administracion" >Almacen</option>
									<option value="Produccion">Artemia</option><option value="Administracion">Cocina</option>
    								<option value="Calidad">Control de Calidad</option> <option value="Logistica" >Cosecha</option>	
    								<option value="Direccion">Director</option><option value="Genetica">Genética</option> 
    								<option value="Produccion">Larvicultura</option>
    								<option value="Produccion">Lar. Mod. 1</option><option value="Produccion"  >Lar. Mod. 2</option>
    								<option value="Produccion">Lar. Mod. 3</option><option value="Produccion"  >Lar. Mod. 4</option>
    								<option value="Produccion">Lar. Mod. 5</option><option value="Produccion"  >Lar. Ext. 1</option>
    								<option value="Produccion">Lar. Ext. 2</option>
    								<option value="Produccion">Maduración</option>
    								<option value="Produccion">Maduración I</option><option value="Produccion">Maduración II</option>
    								<option value="Mantenimiento-I" >Mantenimiento I</option><option value="Mantenimiento-II" >Mantenimiento II</option>
    								<option value="Produccion">Microalgas</option>
    								<option value="Produccion">Microalgas I</option> <option value="Produccion">Microalgas II</option>
    								<option value="Genetica">Reservas</option><option value="Logistica">Transporte</option>
    								<option value="Produccion">Producción</option><option value="Administracion">Vigilancia</option>
    							</select> 
    							<?php } ?>
							<select name="cmbCancelacion" id="cmbCancelacion" style="font-size: 10px; height: 25px; width:120px;" >
          						<option value="Culminado">Ocultar Culminados</option>
          						<option value="Todos">Todos</option>
          					</select>   
          					<select name="diaj" id="diaj" style="font-size: 10px;height: 25px;  margin-top: 1px; margin-left:4px;width: 90px ">
          					<option value="0" >Día</option>
          					<?php	
          					//$this->load->model('edoctatec_model');
          					$data['result']=$this->circal_model->verDiaj();
							foreach ($data['result'] as $row):?>
           					<option value="<?php echo $row->fecd;?>" ><?php echo $row->fecd;?></option>
           					<?php endforeach;?>
   							</select>   
   							<select name="logros" id="logros" style="font-size: 10px; height: 25px; width:70px;" >
          						<option value="0">Logros</option>
          						<option value="Todos">Mostrar</option>
          					</select>    				
							</div>
                		</div>
                		<span class="ajaxTable" style="height: 390px; width: 630px; margin-top: 1px" >
                    	<table id="mytablaCir" name="mytablaCir" class="ajaxSorter" border="1" >
                        	<thead title="Presione las columnas para ordenarlas como requiera">                            
                            <tr>	
                            	<th rowspan="2" style="text-align: center" data-order = "fecdd" >Día</th>                                                        							                                                                                                              
                            	<th rowspan="2" style="text-align: center" data-order = "nfic" >N.P.</th>
                            	<th rowspan="2" style="text-align: center" data-order = "pro" >Problema</th> 
                            	<th rowspan="2" style="text-align: center" data-order = "sol" >Solución</th>
                            	<th colspan="3" style="text-align: center" >Ejecución</th>  
                          	</tr>
                        	<tr>
                        		<?php if($usuario=="Joel Lizárraga" || $usuario=="Jesus Benítez" || $usuario=="Perla Peraza"){ $usu='Todos';?>
                        		<th data-order = "eje" >Departamento</th>                            
                        		<?php }?>
                        		<th data-order = "fecff" >Fecha</th>
                            	<th data-order = "estatus" >Estatus</th>
                            	<?php if($usuario!="Joel Lizárraga" && $usuario!="Jesus Benítez" && $usuario!="Perla Peraza"){ ?>
                        		<th data-order = "obs" >Observaciones</th>                            
                        		<?php }?>
                        	</tr>
                        	 </thead>
                        	<tbody title="Seleccione para realizar cambios" style="font-size: 10px;">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="margin-top: -10px">        
	                    	<ul class="order_list" style="width: 150px; color: red">PROCESOS <input size="2%"  type="text" name="solp" id="solp" style="text-align: left;  border: 0; background: none"></ul>                                            
    	                	<form method="post" action="<?= base_url()?>index.php/circal/pdfrep/" >
        	            		<input size="10%" type="hidden" name="depa" id="depa" value="<?php echo set_value('depa',$usu); ?>">                    	             	                                                                                           
                	        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        		                        		                                                      
 							</form>   
                		</div>                                      
            			</div>             	
					</th>
					<th>
						<table border="0" class="ajaxSorter">
							<thead>
							<tr>
								<th >Registro</th>	
								<th style="text-align: right" >Proceso</th>							
							</tr>
							</thead>
							<tbody>
							<tr>
								<th>Día: <input size="10%" type="text" name="fecd" id="fecd" class="fecha redondo mUp" readonly="true" style="text-align: center;"  <?php if($usuario!="Jesus Benítez" && $usuario!="Perla Peraza"){ ?> disabled="true" <?php } ?>></th>
								<th  style="text-align: right" ><textarea  disabled="true"  name="nfic" id="nfic"  cols="4" style="resize: none; text-align: left" rows="1"></textarea></th>
							</tr>
							
							<tr>
								<th colspan="2" >1.- Problema</th>
							</tr>
							<tr>
								<th colspan="2"><textarea name="pro" id="pro"  cols="50" style="resize: none; text-align: justify" rows="5" <?php if($usuario!="Jesus Benítez" && $usuario!="Perla Peraza"){ ?> disabled="true" <?php } ?> ></textarea></th>
							</tr>
							<tr>
								<th colspan="2" >2.- Solución</th>
							</tr>
							<tr>
								<th colspan="2"><textarea  name="sol" id="sol"  cols="50" style="resize: none; text-align: justify" rows="5" <?php if($usuario!="Jesus Benítez" && $usuario!="Perla Peraza"){ ?> disabled="true" <?php } ?> ></textarea></th>
							</tr>
							<tr>
								<th colspan="2">3.- Ejecución</th>
							</tr>
							<tr>
								<th style="text-align: right" >Departamento<br /> <br />Prioridad
									</th>
								
								<th style="text-align: center">
									<select name="eje" id="eje" style="margin-top: 1px;font-size: 10px;" <?php if($usuario!="Jesus Benítez" && $usuario!="Perla Peraza"){ ?> disabled="true" <?php } ?>>
										<option value="-" >-</option> 
										<option value="Administracion" >Administración</option><option value="Administracion" >Almacen</option>
										<option value="Produccion">Artemia</option><option value="Administracion">Cocina</option>
    									<option value="Calidad">Control de Calidad</option> <option value="Logistica">Cosecha</option>	
    									<option value="Direccion">Director</option><option value="Genetica">Genética</option> 
    									<option value="Produccion">Larvicultura</option>
    									<option value="Produccion">Lar. Mod. 1</option><option value="Produccion">Lar. Mod. 2</option>
    									<option value="Produccion">Lar. Mod. 3</option><option value="Produccion">Lar. Mod. 4</option>
    									<option value="Produccion">Lar. Mod. 5</option><option value="Produccion">Lar. Ext. 1</option>
    									<option value="Produccion">Lar. Ext. 2</option>
    									<option value="Produccion">Maduración</option>
    									<option value="Produccion">Maduración I</option><option value="Produccion">Maduración II</option>
    									<option value="Mantenimiento-I" >Mantenimiento I</option><option value="Mantenimiento-II" >Mantenimiento II</option>
    									<option value="Produccion">Microalgas</option>
    									<option value="Produccion">Microalgas I</option> <option value="Produccion">Microalgas II</option>
    									<option value="Genetica">Reservas</option><option value="Logistica">Transporte</option>
    									<option value="Produccion">Producción</option><option value="Administracion">Vigilancia</option>
    						   		</select>
									<br />
									<select name="priori" id="priori" style=" background-color: white;  font-size: 10px;height: 25px; margin-top: 1px;" <?php if($usuario!="Jesus Benítez" && $usuario!="Perla Peraza"){ ?> disabled="true" <?php } ?>>
      										<option style="color: blue" value=0  > Normal </option>
      										<option style="color: blue" value=3  > Media </option>
      										<option style="color: blue" value=5  > Necesario </option>
      										<option style="color: blue" value=10 > Urgente </option>
      								</select> 
								</th>
							</tr>
							
							<tr>
								<th >4.- Evaluación - Estatus</th>
								<th style="text-align: center">
									<select name="estatus" id="estatus" style=" background-color: white;  font-size: 10px;height: 25px; margin-top: 1px;" <?php if($usuario=="Perla Peraza"){ ?> disabled="true" <?php } ?>>
      										<option style="color: blue" value="Proceso" > En Proceso </option>
      										<option style="color: blue" value="Culminado"  > Culminado </option>      									
      										<option style="color: blue" value="Incompleto"  > Incompleto </option>
      										<option style="color: blue" value="Suspendido"  > Suspendido </option>
    								  </select>    								
    							</th>
							</tr>
							<tr>
								<th >Observaciones</th>
								<th>Día <input size="10%" type="text" name="fecf" id="fecf" class="fecha redondo mUp" readonly="true" style="text-align: center;" <?php if($usuario=="Perla Peraza"){ ?> disabled="true" <?php } ?> ></th>
							</tr>
							<tr>
								<th colspan="2"><textarea  name="obs" id="obs"  cols="50" style="resize: none; text-align: justify" rows="5" <?php if($usuario=="Perla Peraza"){ ?> disabled="true" <?php } ?>></textarea></th>
							</tr>
							</tbody>
							<tfoot>
							<tr>
								<th style="text-align: right" colspan="2">
										<?php if($usuario=="Jesus Benítez" || $usuario=="Perla Peraza"){ ?>
											<input style="font-size: 14px" type="submit" id="nuevo" name="nuevo" value="Nuevo" />
										<?php } ?>
										<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar Datos" />											
								</th>
							</tr>
							</tfoot>
							
						</table>						
					</th>
				</tr>
			</table>
					 
        			
	 								
		</div>
		
					
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#nuevo").click( function(){
	$("#nfic").val(''); //$("#fecd").val('');
	$("#fecf").val('');$("#pro").val('');$("#sol").val('');$("#eje").val('');$("#estatus").val('');
	$("#obs").val('');$("#priori").val('');
	var f = new Date();
	$("#fecd").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
 return true;
})

$("#aceptar").click( function(){	
	dia=$("#fecd").val();pro=$("#pro").val();sol=$("#sol").val();
	nfic=$("#nfic").val();
	 if(dia!=''){
	 	if(pro!=''){
	 		if(sol!=''){
		if(nfic!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/circal/actualizar", 
						data: "id="+$("#nfic").val()+"&fecd="+$("#fecd").val()+"&pro="+$("#pro").val()+"&sol="+$("#sol").val()+"&eje="+$("#eje").val()+"&estatus="+$("#estatus").val()+"&fecf="+$("#fecf").val()+"&obs="+$("#obs").val()+"&pri="+$("#priori").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaCir").trigger("update");	
										$("#nfic").val(''); //$("#fecd").val('');
										$("#fecf").val('');$("#pro").val('');$("#sol").val('');$("#eje").val('');$("#estatus").val('');
										$("#obs").val('');$("#priori").val('');
										//javascript:location.reload();						
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
			
			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/circal/agregar", 
						data: "&fecd="+$("#fecd").val()+"&pro="+$("#pro").val()+"&sol="+$("#sol").val()+"&eje="+$("#eje").val()+"&estatus="+$("#estatus").val()+"&fecf="+$("#fecf").val()+"&obs="+$("#obs").val()+"&pri="+$("#priori").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										$("#mytablaCir").trigger("update");
										var f = new Date();
										$("#fecd").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
										//$("#fecd").val('');
										$("#nuevo").click();	
										//javascript:location.reload();							
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		
		}
	}else{
		alert("Error: Necesita Registrar La Solución del Problema");
		$("#sol").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Registrar La descripción del Problema");
		$("#pro").focus();
		return false;
	}
	}else{
		alert("Error: Necesita Registrar La Fecha");
		$("#fecd").focus();
		return false;
	}
	
});
$("#fecf").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2003:+0",  
});	
$("#fecd").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2003:+0",  // se cambia a 1 si hay entregas en diciembre y en enero se pone 0
	onSelect: function( selectedDate ){
		$("#fecf").datepicker( "option", "minDate", selectedDate );
		//$("diaj").val($("#fecd").datepicker());
		//$("#mytablaSalas").trigger("update");	
	}	
});		
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});  

$(document).ready(function(){
	$("#zonasel").val($("#cmbZona").val());	
	var f = new Date();
	$("#fecd").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	$("#tabla_est").ajaxSorter({
	url:'<?php echo base_url()?>index.php/circal/tabla',  	
	//filters:['cmbDepto','cmbCancelacion'],		
	filters:['cmbDepto','depa','cmbCancelacion','diaj','logros'],
    active:['focodia',8,'>='],
    sort:false,
    onRowClick:function(){
    	if($(this).data('nfic')>'0'){
        	$("#nfic").val($(this).data('nfic'));       	
			$("#fecd").val($(this).data('fecd'));$("#fecf").val($(this).data('fecf'));$("#pro").val($(this).data('pro'));
			$("#sol").val($(this).data('sol'));$("#eje").val($(this).data('eje'));$("#estatus").val($(this).data('estatus'));
			$("#obs").val($(this).data('obs'));$("#priori").val($(this).data('priori'));
			//$("#obs").focus();
		}		
    }, 
    onSuccess:function(){
    		$('#tabla_est tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','center'); })
    		$('#tabla_est tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','justify'); })
	    	$('#tabla_est tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','justify'); })
	    	$('#tabla_est tbody tr').map(function(){
    	   		$("#solp").val($(this).data('totp'));	
    	   });
   },     
});
});


</script>