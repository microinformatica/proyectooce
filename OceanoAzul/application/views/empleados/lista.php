<?php $this->load->view('header_cv');?>

<script>
	
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 20px;
	/*background: none;*/		
}   
#exportc_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; } 
#exports_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#exportr_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#exportv_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#exportb_data { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
#exportb_datap { background:url('<?= base_url()?>assets/img/sorter/pdf.png') no-repeat; height: 28px; }
</style>
<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
			
		<li><a href="#todos"><img src="<?php echo base_url();?>assets/images/menu/empleados.png" width="25" height="25" border="0"> Empleados</a> </li>			
		<?php if($usuario=="Jesus Benítez" or $usuario=="Trinidad Leon" or $usuario=="Zuleima Benitez" or $usuario=="Narda Benitez"){ ?>
		<li style="font-size: 20px"><a href="#prestamos"><img src="<?php echo base_url();?>assets/images/menu/prestamos.png" width="25" height="25" border="0"> Préstamos</a></li>
		<!--<li style="font-size: 20px"><a href="#general"><img src="<?php echo base_url();?>assets/images/menu/general.png" width="25" height="25" border="0"> Saldos</a></li>-->
		<li style="font-size: 20px"><a href="#saldos"><img src="<?php echo base_url();?>assets/images/menu/abonos.png" width="25" height="25" border="0"> Estado de Cuenta</a></li>
		<?php } else {?>
		<li style="font-size: 20px"><a href="#saldos"><img src="<?php echo base_url();?>assets/images/menu/general.png" width="25" height="25" border="0"> Préstamos</a></li>
		<?php } ?>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="todos" class="tab_content" >
			<div id = "accordion" style="margin-top: -10px; margin-left: -10px; width: 958px;" >
					<h3 align="left" style="font-size: 12px" ><a href="#"> Registrados </a></h3>
        			<div  style="height:345px; text-align: right; font-size: 12px;margin-top: -10px;"> 
        				Empleados:  
    						<select name="cmbActivos" id="cmbActivos" style="font-size: 10px; height: 25px; margin-top: -15px;  " >        						
          						<option value="2">Todos</option>
          						<option value="0">Activos</option>
          						<option value="-1">Bajas</option>          						
          					</select>
        				Departamento	
          					<select name="deptos" id="deptos" style="font-size: 10px; height: 25px; margin-top: -15px;  ">
								<option value="Todos" >Todos</option>
								<option value="-" >-</option> 
								<option value="Administracion" >Administración</option>
								<option value="Almacen" >Almacen</option>
								<option value="Cocina">Cocina</option>
    							<option value="Compras" >Compras</option>	
    							<option value="Mantenimiento" >Mantenimiento</option>
    							<option value="Maternidades" >Maternidades</option>
    							<option value="Produccion">Producción</option>
    							<option value="Vigilancia">Vigilancia</option>
    						</select>
        				Filtrar aquellos que les falte la captura de: 
        					<select name="cmbSindatos" id="cmbSindatos" style="font-size: 10px; height: 25px; margin-top: -15px;  " >        						
          						<option value="0">Elija Opcion</option>
          						<option value="1">Apellido Paterno</option><option value="2">Apellido Materno</option><option value="3">CURP</option>
          						<option value="4">RFC</option><option value="5">IMSS</option><option value="6">E-Mail</option><option value="7">Domicilio</option>
          					</select>  
          				<div class="ajaxSorterDiv" id="tabla_emp" name="tabla_emp" style="margin-top: 1px; height: 367px; " >  
                			<span class="ajaxTable" style="height: 301px;width: 882px; " >
                    			<table id="mytabla" name="mytabla" class="ajaxSorter" >
                        			<thead title="Presione las columnas para ordenarlas como requiera">                            
                            			<th data-order = "ide" >No.</th> 	<th data-order = "nombre" style="width: 180px" >Nombre</th>                                                        
                            			<th data-order = "ap" >Apellido Paterno</th>  <th data-order = "am" >Apellido Materno</th>
                            			<th data-order = "puesto" >Puesto</th>
                            			<th data-order = "ing" >Ingresó</th>                     
                        		</thead>
                        		<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        		</tbody>                        	
                    			</table>
                			</span> 
             				<div class="ajaxpager" style="margin-top: -5px">        
	                    		<ul class="order_list">Total: <input size="3%"  type="text" name="num" id="num" style="text-align: left;  border: 0; background: none"></ul>                                            
    	                		<form method="post" action="<?= base_url()?>index.php/empleados/pdfrep/" >
        	            			<input type="hidden"  name="depa" id="depa" readonly="true" >
        	            			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                    	    		<input type="hidden" name="tabla" value ="" class="htmlTable"/>                        		                        		                                                      
 								</form>   
                			</div>                                      
            			</div>                                                               
            	
       				 </div>				
					<h3 align="left" style="font-size: 12px"><a href="#">Agregar o Actualizar </a></h3>
		       	 	<div style="height: 400px;margin-top: -8px;">
		       	 		<table border="2px" width="950px" style="margin-left: -30px; margin-top: -15px">
							<tbody style="background-color: #F7F7F7">
								<tr>
									<th rowspan="2" style="background-color: #DBE5F1; font-size: 12px" >Grales</th>
									<th style="margin: 2px; font-size: 10px">No.</th>
									<th style="margin: 2px; font-size: 10px">Nombre</th>
									<th style="margin: 2px; font-size: 10px">Apellido Paterno</th>
									<th style="margin: 2px; font-size: 10px">Apellido Materno</th>
									<th style="margin: 2px; font-size: 10px">Sexo</th>
									<th style="margin: 2px; font-size: 10px">Edo civil</th>
									<th style="font-size: 10px; text-align: center; ">Estatus<input size="2%" type="hidden" name="verifica" id="verifica" readonly="true" >	</th>
								</tr>
								<tr>
									<th  style="font-size: 14px; background-color: white;" ><input size="4%"  type="text" name="ide" id="ide" style="text-align: center"  ></th>
									<th style="font-size: 14px; background-color: white"><input size="25%"  type="text" name="nombre" id="nombre" ></th>
									<th style="font-size: 14px; background-color: white"><input size="20%" type="text" name="ap" id="ap" value=""></th>
									<th style="font-size: 14px; background-color: white"><input size="20%" type="text" name="am" id="am" value=""></th>
									<th style="font-size: 12px; background-color: white">
									<!--<input size="25%" type="text" name="ciu" id="ciu" value="">-->
										<select name="sexo" id="sexo" style="margin-top: 1px;font-size: 11px;">
											<option value="1">Hombre</option>
    										<option value="2">Mujer</option>
  										</select>
									</th>
									<th style="font-size: 12px; background-color: white">
									<!--<input size="25%" type="text" name="ciu" id="ciu" value="">-->
										<select name="civil" id="civil" style="margin-top: 1px;font-size: 11px;">
											<option value="1">Soltero</option>
    										<option value="2">Casado</option>
  										</select>
									</th>
									<th style="font-size: 12px; background-color: white; text-align: center">
										<input name="rbVerifica" type="radio" value="0" onclick="radios(7,0)"/><span>Activo</span>
										&nbsp;&nbsp;<input name="rbVerifica" type="radio" value="-1" onclick="radios(7,-1)"/><span style="color: red">Baja</span>
									</th>
								</tr>
							</tbody>
						</table>	
						<table border="2px" width="950px" style="margin-top: -18px; margin-left: -30px">
							<tbody style="background-color: #F7F7F7">
								<tr>
									<th style="margin: 2px; font-size: 10px">Calle y Número</th>
									<th style="margin: 2px; font-size: 10px">Colonia</th>
									<th style="margin: 2px; font-size: 10px">Ciudad</th>
									<th style="margin: 2px; font-size: 10px">Estado</th>
									<th style="margin: 2px;font-size: 10px;">C.P.</th>
								</tr>
								<tr>
									<th  style="font-size: 14px; background-color: white;" ><input size="34%"  type="text" name="canu" id="canu" ></th>
									<th style="font-size: 14px; background-color: white"><input size="22%"  type="text" name="col" id="col" ></th>
									<th style="font-size: 12px; background-color: white">
									<input size="25%" type="text" name="ciu" id="ciu" value="">
									<!--
										<select name="ciu" id="ciu" style="margin-top: 1px;font-size: 11px;">
											<option value="-" >-</option>	
    										<option value="Agua Verde, El Ros.">Agua Verde, El Ros.</option>
    										<option value="C. Ojo de Agua No.2, El Ros.">C. Ojo de Agua No.2, El Ros.</option>
    										<option value="La Pedregoza, El Ros.">La Pedregoza, El Ros.</option>
    										<option value="Los Ojitos, El Ros.">Los Ojitos, El Ros.</option>
    										<option value="El Rosario">El Rosario</option>
    										<option value="Chametla, El Ros.">Chametla, El Ros.</option>
    										<option value="Apoderado, El Ros.">Apoderado, El Ros.</option>
    										<option value="El Huajote, Concordia">El Huajote, Concordia</option>
    										<option value="Mazatlán">Mazatlán</option>
    										<option value="Villa Unión, Mazatlán">Villa Unión, Mazatlán</option>
    										<option value="Siqueros, Mazatlán">Siqueros, Mazatlán</option>
    										<option value="Altata, Navolato">Altata, Navolato</option>
    										<option value="Potrero de Los Laureles, El Ros.">Potrero de Los Laureles, El Ros.</option>
    										<option value="Angostura">Angostura</option>
  										</select>
>										-->
									</th>
									<th style="font-size: 12px; background-color: white">
										<select name="edo" id="edo" style="margin-top: 1px;font-size: 11px;">
											<option value="Sinaloa" >Sinaloa</option>	
    										<option value="Aguascalientes">Aguascalientes</option>
    										<option value="Baja California">Baja California</option> <option value="Baja C. Sur" >Baja C. Sur</option>	
    										<option value="Campeche">Campeche</option> <option value="Chiapas"  >Chiapas</option>	
    										<option value="Chihuahua">Chihuahua</option> <option value="Coahulia" >Coahuila</option>
    										<option value="Colima" >Colima</option> <option value="Distrito Federal" >Distrito Federal</option>	
    										<option value="Durango" >Durango</option> <option value="Guanajuato" >Guanajuato</option>	
    										<option value="Guerrero" >Guerrero</option> <option value="Hidalgo" >Hidalgo</option>	
    										<option value="Jalisco" >Jalisco</option> <option value="Edo de Mexico" >Edo de M&eacute;xico</option>
    										<option value="Michoacan" >Michoac&aacute;n</option> <option value="Morelos" >Morelos</option>		
    										<option value="Nayarit" >Nayarit</option> <option value="Nuevo Leon" >Nuevo Le&oacute;n</option>			
    										<option value="Oaxaca" >Oaxaca</option> <option value="Puebla" >Puebla</option>
    										<option value="Queretaro" >Queretaro</option> <option value="Quintana Roo" >Quintana Roo</option>
    										<option value="San Luis Potosi" >San Luis Potos&iacute;</option> 
    										<option value="Sonora" >Sonora</option> <option value="Tamaulipas" >Tamaulipas</option>
    										<option value="Tabasco" >Tabasco</option> <option value="Tlaxcala" >Tlaxcala</option>
    										<option value="Veracruz" >Veracruz</option> <option value="Yucatan" >Yucat&aacute;n</option>
    										<option value="Zacatecas" >Zacatecas</option>
  										</select>
									</th>
									<th style="font-size: 14px; background-color: white; text-align: center"><input style="width: 45px" type="text" name="cp" id="cp" value="0"></th>
								</tr>
    						</tbody>				
    				</table>
    				<table border="2px" width="950px" style="margin-top: -18px; margin-left: -30px">
						<tbody style="background-color: #F7F7F7">
							<tr>
								<th style="margin: 2px; font-size: 10px">Fec. Nac.</th>
								<th style="margin: 2px; font-size: 10px">C.U.R.P.</th>
								<th style="margin: 2px; font-size: 10px">R.F.C.</th>
								<th style="margin: 2px; font-size: 10px">I.M.S.S.</th>
								<th style="margin: 2px;font-size: 10px;">E-mail</th>
							</tr>
							<tr>
								<th  style="font-size: 14px; background-color: white;" ><input style="text-align: center; width: 90px" type="text" name="fnac" id="fnac" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>
								<th style="font-size: 14px; background-color: white"><input style="text-align: center; width: 170px" size="25%"  type="text" name="curp" id="curp" ></th>
								<th style="font-size: 14px; background-color: white"><input style="text-align: center; width: 140px" size="17%" type="text" name="rfc" id="rfc" value=""></th>
								<th style="font-size: 14px; background-color: white"><input style="text-align: center; width: 100px" type="text" name="imss" id="imss" ></th>
								<th style="font-size: 14px; background-color: white; text-align: center"><input size="43%" type="text" name="mail" id="mail" value=""></th>
							</tr>
    					</tbody>				
    				</table>
    				<table border="2px" width="950px" style="margin-top: -18px; margin-left: -30px">
						
						<tbody style="background-color: #F7F7F7">
							<tr>
								<th rowspan="2" style="background-color: #DBE5F1; font-size: 12px" >Administrativo</th>
								<th style="margin: 2px; font-size: 10px">Departamento</th>
								<th style="margin: 2px; font-size: 10px">Puesto</th>
								<th style="margin: 2px; font-size: 10px">Fecha Ingreso</th>
								<th style="margin: 2px; font-size: 10px">Cta. Nómina</th>
								<th style="margin: 2px; font-size: 10px">Cta. Viáticos</th>
								<th style="margin: 2px; font-size: 10px" rowspan="2">
									<form id="plastico" action="<?= base_url()?>index.php/empleados/pdfrepbacp" method="POST" >
									<button id="exportb_datap" type="button" style="width:75px; cursor: pointer; background-color: lightyellow; text-align: right"  >Plástico</button>  
									<input type="hidden" style="font-size: 10px" size="40%" type="text" name="empleadop" id="empleadop">    	    	  					
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="firmarp" id="firmarp">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="puestorp" id="puestorp">
    	    	  					<input type="hidden" style="font-size: 10px" size="50%" type="text" name="imssp" id="imssp">
    	    	  					<input type="hidden" style="font-size: 10px" size="50%" type="text" name="deptop" id="deptop">
    	    	  					<input type="hidden" style="font-size: 10px" size="50%" type="text" name="puesp" id="puesp">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="ingp" id="ingp">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="nomp" id="nomp">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="viap" id="viap">
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="fecexpp" id="fecexpp">
    	    	  				</form>
    	    	  				<script type="text/javascript">
									$(document).ready(function(){
										$('#exportb_datap').click(function(){	
											$('#firmarp').val($('#firma').val());
											$('#puestorp').val($('#puesto').val());
											$('#empleadop').val($('#nomc').val());								
											$('#imssp').val($('#imss').val());
											$('#deptop').val($('#depto').val());
											$('#puesp').val($('#pues').val());
											$('#ingp').val($('#txtFecha').val());
											$('#nomp').val($('#ctadep').val());
											$('#viap').val($('#ctavia').val());
											$('#fecexpp').val($('#Fexp').val());
											$('#plastico').submit();
										});
									});
								</script>	
								</th>
								<th style="margin: 2px; font-size: 10px">Sueldo Semanal</th>
							</tr>
							<tr>
								<th style="font-size: 14px; background-color: white">
									<select name="depto" id="depto" style="margin-top: 1px;font-size: 13px;">
										<option value="Todos" >Todos</option>
										<option value="-" >-</option> 
										<option value="Administracion" >Administración</option>
										<option value="Almacen" >Almacen</option>
										<option value="Cocina">Cocina</option>
    									<option value="Compras" >Compras</option>	
    									<option value="Mantenimiento" >Mantenimiento</option>
    									<option value="Maternidades" >Maternidades</option>
    									<option value="Produccion">Producción</option>
    									<option value="Vigilancia">Vigilancia</option>
    						   		</select>
								</th>
								<th  style="font-size: 14px; background-color: white;" >
									<select name="pues" id="pues" style="margin-top: 1px;font-size: 13px;">
										<option value="-" >-</option>	
										<option value="Almacenista">Almacenista</option>
										<option value="Auxiliar">Auxiliar</option>
										<option value="AuxSoldador">Aux Soldador</option>
										<option value="AuxMantenimiento">Aux Mantenimiento</option>
										<option value="AuxMecanico">Aux Mecánico</option>
										<option value="Bombero">Bombero</option>
										<option value="Chofer" >Chofer</option>
										<option value="Encargado">Encargado</option>
										<option value="Gerente" >Gerente</option>
										<option value="Mecanico" >Mecánico</option>
										<option value="Operario">Operario</option>
										<option value="Soldador">Soldador</option>
										<option value="Supervisor" >Supervisor</option>
										<option value="Tecnico">Técnico</option>
										<option value="SupAlimento">Sup Alimento</option>
										<option value="SupCalidadAgua">Sup Calidad de Agua</option>
										<option value="TecAlimento">Téc Alimento</option>
										<option value="TecCalidadAgua">Téc Calidad de Agua</option>
    									<option value="Vigilante">Vigilante</option>    						
  									</select>
								</th>
								<th style="font-size: 14px; background-color: white"><input type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="text-align: center; width: 90px" ></th>
								<th style="font-size: 14px; background-color: white;" ><input style="text-align: right; width: 90px"  onKeyPress="return(currencyFormat(this,'','',event));" type="text" name="ctadep" id="ctadep" ></th>
								<th style="font-size: 14px; background-color: white;" ><input style="text-align: right; width: 90px"  onKeyPress="return(currencyFormat(this,'','',event));" type="text" name="ctavia" id="ctavia" ></th>
								<th style="font-size: 14px; background-color: white;" >$ <input style="text-align: right; width: 90px" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" type="text" name="suel" id="suel" ></th>
							</tr>
    					</tbody>
						
    				</table>
    				<table border="2px" width="950px" style="margin-top: -18px; margin-left: -30px">
						<tbody style="background-color: #F7F7F7">
							<tr>
								<th rowspan="2" style="background-color: #DBE5F1; font-size: 12px" >Estudios</th>
								<th style="margin: 2px; font-size: 10px">Escolaridad</th>
								<th style="margin: 2px; font-size: 10px">Carrera</th>
								<th style="margin: 2px; font-size: 10px">Terminada</th>
								<th style="margin: 2px; font-size: 10px">Documento</th>
								<th style="margin: 2px; font-size: 10px">Cédula</th>
								<th style="margin: 2px; font-size: 10px">Observaciones</th>
								<th style="margin: 2px; font-size: 10px" rowspan="2">
									<center>
								<?php if($usuario=="Jesus Benítez" or $usuario=="Trinidad Leon" or $usuario=="Zuleima Benitez" or $usuario=="Narda Benitez"){ ?>
								<input style="font-size: 14px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  /> 
								<input style="font-size: 14px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
								<input size="4%" type="hidden"  name="id" id="id" style="text-align: center"  >
								<?php }?>
									</center>
								</th>				
							</tr>
							<tr>
								<th style="font-size: 14px; background-color: white">
									<select name="esc" id="esc" style="margin-top: 1px;font-size: 13px;">
										<option value="0" >-</option> 
										<option value="1" >Primaria</option><option value="2" >Secundaria</option><option value="3" >Preparatoria</option>
										<option value="4" >Licenciatura</option><option value="5" >Ingeniería</option><option value="6" >Maestría</option>
										<option value="7" >Doctorado</option>
    						   		</select>
								</th>
								<th  style="font-size: 14px; background-color: white;" >
									<input style="width: 200px" type="text" name="car" id="car" >
								</th>
								<th style="font-size: 14px; background-color: white;">
									<select name="ter" id="ter" style="margin-top: 1px;font-size: 13px;">
										<option value="0" >-</option>	
										<option value="1">Si</option>
										<option value="2">No</option>    						
  									</select>
								</th>
								<th style="font-size: 14px; background-color: white">
									<select name="tit" id="tit" style="margin-top: 1px;font-size: 13px;">
										<option value="0" >-</option>	
										<option value="1">Certificado</option>
										<option value="2">Título</option>    						
  									</select>
								</th>
								<th style="font-size: 14px; background-color: white;">
									<select name="ced" id="ced" style="margin-top: 1px;font-size: 13px;">
										<option value="0" >-</option>	
										<option value="1">Si</option>
										<option value="2">No</option>    						
  									</select>
								</th>
								<th style="font-size: 14px; background-color: white;">
									<input style="width: 200px" type="text" name="obs" id="obs" >
								</th>
								
								
							</tr>
    					</tbody>
						
    				</table>
    				<table width="950px" border="2px" style="margin-top: -18px; margin-left: -30px">
						<thead style="background-color: #DBE5F1" >
							<th height="18px" style="font-size: 12px;">DOCUMENTOS ACREDITABLES - Dia de Elaboración: <input size="10%" type="text" name="Fexp" id="Fexp" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" ></th>
							<th style="margin: 2px; font-size: 12px; text-align: center  " >
								<form id="data_tablesa" action="<?= base_url()?>index.php/empleados/pdfrepcon" method="POST" >							
    	    	  					 1.- <button id="exports_data" type="button" style="width: 80px;cursor: pointer; background-color: lightyellow; text-align: right" >Contrato</button>   
    	    	  					<input type="hidden" type="text" name="nomb" id="nomb" >
    	    	  					<input type="hidden" type="text" name="domi" id="domi" >
    	    	  					<input type="hidden" type="text" name="imss1" id="imss1" >
    	    	  					<input type="hidden" type="text" name="rfc1" id="rfc1" ><input type="hidden" type="text" name="curp1" id="curp1" >
    	    	  					<input type="hidden" type="text" name="fec1" id="fec1" >
    	    	  					<input type="hidden" type="text" name="sue1" id="sue1" >
    	    	  					<input type="hidden" type="text" name="pue1" id="pue1" >
    	    	  					<input type="hidden" type="text" name="dep1" id="dep1" >
    	    	  					<input type="hidden" type="text" name="fnac1" id="fnac1" ><input type="hidden" type="text" name="sexo1" id="sexo1" >
    	    	  					<input type="hidden" type="text" name="civil1" id="civil1" >
    	    	  					<!--<img style="cursor: pointer;" title="Enviar datos a Contrato en archivo PDF" id="exports_data1" src="<?= base_url()?>assets/img/sorter/pdf.png" />-->
    	    	  				</form>
        						<script type="text/javascript">
									$(document).ready(function(){
										$('#exports_data').click(function(){									
											$('#data_tablesa').submit();
										});
									});
								</script> 
							</th>
							<th style="margin: 2px; font-size: 12px;text-align: center ">
								<form id="data_tablesc" action="<?= base_url()?>index.php/empleados/pdfrepcre" method="POST" >
									2.- <button id="exportc_data" type="button" style="width: 90px; cursor: pointer; background-color: lightyellow; text-align: right"  >Credencial</button> 
									<input name="rbFoto" type="radio" value="0" /><span style="font-size: 10px">Sin Foto</span>
									&nbsp;&nbsp;<input name="rbFoto" type="radio" value="-1" /><span style="font-size: 10px">Con Foto</span>						
    	    	  					<input type="hidden" type="text" name="nemp" id="nemp" >
    	    	  					<input type="hidden" type="text" name="nomc" id="nomc" >
    	    	  					<input type="hidden" type="text" name="domc" id="domc" >
    	    	  					<input type="hidden" type="text" name="imss2" id="imss2" >
    	    	  					<input type="hidden" type="text" name="rfc2" id="rfc2" >
    	    	  					<input type="hidden" type="text" name="pue2" id="pue2" >
    	    	  					<input type="hidden" type="text" name="dep2" id="dep2" >
    	    	  					<input type="hidden" type="text" name="curp2" id="curp2" >
    	    	  					<!--<img style="cursor: pointer;" title="Enviar datos a Contrato en archivo PDF" id="exportc_data" src="<?= base_url()?>assets/img/sorter/pdf.png" />-->
    	    	  				</form>
        						<script type="text/javascript">
									$(document).ready(function(){
										$('#exportc_data').click(function(){	
											$('#data_tablesc').submit();
										});
									});
								</script>
							</th>
							<th rowspan="4">
								<div>
								<img id="numero"  style=" margin-top: -5px"  width="100" height="100" border="0" class="rounded">
								</div>
								<div >
									<img id="firmas" style="margin-top: -30px;" width="100" height="30" border="0">
								</div>
							</th>
							</tr>
							<tr>
							<th colspan="2" style="margin: 2px; font-size: 12px; " >
								<form id="recomendacion" action="<?= base_url()?>index.php/empleados/pdfreprec" method="POST" >
									3.- <button id="exportr_data" type="button" style="width: 130px; cursor: pointer; background-color: lightyellow; text-align: right"  >Recomendación</button>  
									Periodo de: <input style="font-size: 10px" size="20%" type="text" name="per1" id="per1"> a 
									<input style="font-size: 10px" size="20%" type="text" name="per2" id="per2">
									<input type="hidden" style="font-size: 10px" size="40%" type="text" name="empleado" id="empleado">    	    	  					
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="depar" id="depar">
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="puest" id="puest">
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="fecexp" id="fecexp">
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="firmar" id="firmar">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="puestor" id="puestor">
    	    	  				</form>
        						<script type="text/javascript">
									$(document).ready(function(){
										$('#exportr_data').click(function(){	
											$('#firmar').val($('#firma').val());
											$('#puestor').val($('#puesto').val());
											$('#empleado').val($('#nomc').val());								
											$('#depar').val($('#depto').val());
											$('#puest').val($('#pues').val());
											$('#fecexp').val($('#Fexp').val());
											$('#recomendacion').submit();
										});
									});
								</script>
							</th>
							<th rowspan="3" style="margin: 2px; font-size: 12px; ">
								Elaboró documento: <br />
								Firma: <!--<input style="font-size: 10px" size="40%" type="text" name="firma" id="firma"  value="">--> 
								<select name="firma" id="firma" style="margin-top: 1px;font-size: 10px;">
										<option value="Trinidad Leon Castro" >Trinidad Leon Castro</option>
										<option value="Jose Antonio Valdez Valdez" >Jose Antonio Valdez Valdez</option>
										<option value="Ezequiel Rodriguez Galindo">Ezequiel Rodriguez Galindo</option>									
								</select><br />
    	    	  				Puesto: <!--<input style="font-size: 10px" size="30%" type="text" name="puesto" id="puesto" value="Gerente Administrativo">-->
    	    	  				<select name="puesto" id="puesto" style="margin-top: 1px;font-size: 10px;">
										<option value="Administración" >Administración</option>
										<option value="Gerente Producción">Gerente Producción</option>
										<option value="Gerente Administrativo" >Gerente Administrativo</option>
								</select>
							</th>	
							</tr>
							<tr>
							<th colspan="2" style="margin: 2px; font-size: 12px; ">
								<form id="vacaciones" action="<?= base_url()?>index.php/empleados/pdfrepvac" method="POST" >
									4.- <button id="exportv_data" type="button" style="width: 130px; cursor: pointer; background-color: lightyellow; text-align: right"  >Vacaciones</button>  
									Periodo de: <input style="font-size: 10px" size="4%" type="text" name="per1v" id="per1v"> dias 
									<input type="hidden" style="font-size: 10px" size="40%" type="text" name="empleadov" id="empleadov">    	    	  					
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="deparv" id="deparv">
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="puestv" id="puestv">
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="fecexpv" id="fecexpv">
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="fecingv" id="fecingv">
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="firmarv" id="firmarv">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="puestorv" id="puestorv">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="imssv" id="imssv">
    	    	  				</form>
        						<script type="text/javascript">
									$(document).ready(function(){
										$('#exportv_data').click(function(){	
											$('#firmarv').val($('#firma').val());
											$('#puestorv').val($('#puesto').val());
											$('#empleadov').val($('#nomc').val());								
											$('#deparv').val($('#depto').val());
											$('#puestv').val($('#pues').val());
											$('#fecexpv').val($('#Fexp').val());
											$('#imssv').val($('#imss').val());
											$('#fecingv').val($('#txtFecha').val());
											$('#vacaciones').submit();
										});
									});
								</script>
							</th>	
							</tr>
							<tr>
							<th colspan="2" style="margin: 2px; font-size: 12px; ">
								
								<form id="banco" action="<?= base_url()?>index.php/empleados/pdfrepbac" method="POST" >
									5.- <button id="exportb_data" type="button" style="width: 65px; cursor: pointer; background-color: lightyellow; text-align: right"  >Banco</button>  
									<input style="font-size: 10px" size="32%" type="text" name="bene" id="bene" placeholder="Beneficiario"> 
									<input style="font-size: 10px" size="10%" type="text" name="paren" id="paren" placeholder="Parentesco">
									ID:
									<select name="identificacion" id="identificacion" style="margin-top: 1px;font-size: 10px;width: 65px;">
										<option value="Credencial del IFE" >IFE</option><!--<option value="Credencial de Trabajo" >Trabajo</option>-->
										<option value="Pasaporte">Pasaporte</option><option value="Licencia de Manejo">Manejo</option>
										<option value="Visa Americana">Visa</option>    									
    						   		</select>
									<input type="hidden" style="font-size: 10px" size="40%" type="text" name="empleadob" id="empleadob">    	    	  					
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="firmarb" id="firmarb">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="puestorb" id="puestorb">
    	    	  					<input type="hidden" style="font-size: 10px" size="50%" type="text" name="canub" id="canub">
    	    	  					<input type="hidden" style="font-size: 10px" size="50%" type="text" name="colb" id="colb">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="ciub" id="ciub">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="edob" id="edob">
    	    	  					<input type="hidden" style="font-size: 10px" size="30%" type="text" name="rfcb" id="rfcb">
    	    	  					<input type="hidden" style="font-size: 10px" size="40%" type="text" name="fecexpb" id="fecexpb">
    	    	  				</form>
        						<script type="text/javascript">
									$(document).ready(function(){
										$('#exportb_data').click(function(){	
											$('#firmarb').val($('#firma').val());
											$('#puestorb').val($('#puesto').val());
											$('#empleadob').val($('#nomc').val());								
											$('#canub').val($('#canu').val());
											$('#colb').val($('#col').val());
											$('#ciub').val($('#ciu').val());
											$('#edob').val($('#edo').val());
											$('#rfcb').val($('#rfc').val());
											$('#fecexpb').val($('#Fexp').val());
											$('#banco').submit();
										});
									});
								</script>
							</th>	
							</tr>
						</thead>
					</table>  
					
        		</div>
	 		</div>	
	 		
	 	</div>
	 	<?php if($usuario=="Jesus Benítez" or $usuario=="Trinidad Leon" or $usuario=="Zuleima Benitez" or $usuario=="Narda Benitez"){ ?>
		<div id="prestamos" class="tab_content"  >	
			<div style="width: 860px">
			<table class="ajaxSorter" border="2" style="width: 860px" >
				<thead>
						<th>Registrar</th><th>Fecha</th><th>No. Emp</th><th>Cantidad</th><th colspan="2">Observaciones</th>
						
				</thead>
				<tbody>
						<th style="text-align: center">
							<input name="rbSel" type="radio" value="1" onclick="radios(6,1)" /> Cargo
							<input name="rbSel" type="radio" value="2" onclick="radios(6,2)"/> Abono
            				<input type="hidden" readonly="true" size="2%"  type="text" name="idpca" id="idpca">
            				<input type="hidden" readonly="true" size="2%"  type="text" name="idpe" id="idpe">
						</th>
						<th style="text-align: center"><input size="10%" type="text" name="pfec" id="pfec" class="fecha redondo mUp" readonly="true" style="text-align: center;" ></th>
						<th style="text-align: center"><input size="5%"  type="text" name="pide" id="pide"></th>
						<th style="text-align: center">$ <input style="text-align: right" size="10%" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" type="text" name="pimp" id="pimp" ></th>
						<th ><input size="35%"  type="text" name="pobs" id="pobs"></th>
						<th style="text-align: center">
							<input type="submit" id="pnuevo" name="pnuevo" value="Nuevo"  />
							<input type="submit" id="paceptar" name="paceptar" value="Guardar" />
							<input type="submit" id="pborrar" name="pborrar" value="Borrar" />
						</th>
				</tbody>
			</table>
			</div>
			<table border="0" style="margin-left: -5px; margin-top: 1px; ">
				<tr>
				 	<th >				 		
                 		<div class="ajaxSorterDiv" id="tabla_pre" name="tabla_pre" style="width: 860px"  >                 			
							<span class="ajaxTable" style="height: 385px; margin-top: 1px; " >
                    			<table id="mytablaPres" name="mytablaPres" class="ajaxSorter" border="1" style="width: 843px" >
                        			<thead>                            
                            			<th data-order = "cap" >Estatus</th>
                            			<th data-order = "pfecf" >Fecha</th>
                            			<th data-order = "pide" >No</th>
                            			<th data-order = "nom"  >Nombre</th>
                            			<th data-order = "pimpp"   >Cantidad</th>
                            			<th data-order = "pobs" >Observaciones</th>
                            			<?php 
                            				//for ($j = 0; $j < 5; $j++): 
                            				?>	
    									<!--<th data-order ="<?php echo "d".$j;?>"  style="width: 65px;"><?php echo $j;?></th>-->		
						        		<?php //endfor ;?>                         
                            			                            			
                        			</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<form id="data_tables" action="<?= base_url()?>index.php/empleados/pdfrepdia" method="POST" >							
    	    	            		<ul class="order_list" style="width: 350px; margin-top: 1px">
    	    	            			Fecha de Movimientos:<!-- <input size="13%" type="text" name="pfecd" id="pfecd" class="fecha redondo mUp" readonly="true" style="text-align: center;" >-->
    	    	            			<input size="10%" type="text" name="txtFI" id="txtFI" class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
                	&nbsp; al <input size="10%" type="text" name="txtFF" id="txtFF"  class="fecha redondo mUp" readonly="true" style="margin-left:12px;text-align: center;" >
    	    	            			</ul>
    	    	            		<img style="cursor: pointer; margin-top: -5px" title="Enviar datos de tabla en archivo PDF" id="export_data" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tabladps" id="html_dps"/>
								</form>   
                			</div>       
	                	<script type="text/javascript">
							$(document).ready(function(){
								$('#export_data').click(function(){									
									$('#html_dps').val($('#mytablaPres').html());
									$('#data_tables').submit();
									$('#html_dps').val('');
								});
							});
						</script>
	                	</div>
	                	
            		</th>
            	</tr>
			</table>	
		</div>
		<?php } ?>
			
		<div id="saldos" class="tab_content"  >	
			<table  border="0" width="930px" >
				<tr>
					<th >Saldos Actuales de Empleados</th>
					<th >Estado de Cuenta: 
						<input size="40%" type="text" name="nom" id="nom" readonly="true" style="border: none"  >
						<input type="hidden" size="4%" name="idpd" id="idpd" readonly="true" style="border: none" >
					</th>
				</tr>
				<tr>
					<th>
						<div class="ajaxSorterDiv" id="tabla_sem" name="tabla_sem" style="width: 500px; margin-top: -10px"  >                 			
							<span class="ajaxTable" style="height: 400px;  " >
                    			<table id="mytablaSemp" name="mytablaSemp" class="ajaxSorter" style="width: 482px" >
                        			<thead>                            
                            			<th data-order = "ide" >No</th>
                            			<th data-order = "nombre"  >Nombre</th>
                            			<th data-order = "car"  >Cargos</th>
                            			<th data-order = "abo"  >Abonos</th>
                            			<th data-order = "sal"  >Saldo</th>                          			
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
                			<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<ul class="order_list" >
									<select name="cmbCancelacion" id="cmbCancelacion" style="font-size: 10px; height: 25px; width:120px; margin-top: -10px" >
          								<option value="-1">Excluir Cancelados</option>
          								<option value="0">Todos</option>
          							</select>
								</ul>
								<form id="data_tablesGe" action="<?= base_url()?>index.php/empleados/pdfrepgen" method="POST">							
    	    	            		<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" id="export_data3" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tablagen" id="html_gene"/>
								</form>   
                			</div>       
	                	<script type="text/javascript">
							$(document).ready(function(){
								$('#export_data3').click(function(){									
									$('#html_gene').val($('#mytablaSemp').html());
									$('#data_tablesGe').submit();
									$('#html_gene').val('');
								});
							});
						</script>             
	                	</div>
					</th>
					<th>
						<div class="ajaxSorterDiv" id="tabla_sde" name="tabla_sde" style="width: 420px; margin-top: -10px"  >                 			
							<span class="ajaxTable" style="height:400px;" >
                    			<table id="mytablaSdet" name="mytablaSdet" class="ajaxSorter"  style="width: 402px" >
                        			<thead>                            
                            			<th data-order = "fecd" >Fecha</th>
                            			<th data-order = "pobs"  >Concepto</th>
                            			<th data-order = "car"  >Cargo</th>
                            			<th data-order = "abo"  >Abono</th>
                            			<th data-order = "sal"  >Saldo</th>
                            		</thead>
                        			<tbody title="Seleccione para realizar cambios" style="font-size: 11px">
                        			</tbody>                        	
                    			</table>
                			</span> 
             			 	<div class="ajaxpager" style=" font-size: 12px; margin-top: -10px"> 
								<?php if($usuario=="Jesus Benítez" or $usuario=="Trinidad Leon" or $usuario=="Zuleima Benitez" or $usuario=="Narda Benitez"){ ?>	
								<ul class="order_list" style="width: 320px" >Cancelar Cargos y Abonos: &nbsp;&nbsp;<input name="rbVerificaCA" type="radio" value="0" onclick="radios(5,0)"/><span> No</span>
									&nbsp;&nbsp;<input name="rbVerificaCA" type="radio" value="-1" onclick="radios(5,-1)"/><span style="color: red"> Si</span>
									<input type="hidden" size="2%"  name="pcancel" id="pcancel"  >
									<input type="submit" id="caceptar" name="caceptar" value="Guardar" />
								</ul>
								<?php } ?>	
								<form id="data_tablesdet" action="<?= base_url()?>index.php/empleados/pdfrepdet" method="POST">							
    	    	            		<input size="50%" type="hidden" name="nomt" id="nomt" readonly="true" style="border: none" >
    	    	            		<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" id="export_data1" src="<?= base_url()?>assets/img/sorter/pdf.png" />
        	            	    	<input type="hidden" name="tabladpsd" id="html_dpsd"/>
								</form>
								 
                			</div>       
	                	<script type="text/javascript">
							$(document).ready(function(){
								$('#export_data1').click(function(){									
									$('#html_dpsd').val($('#mytablaSdet').html());
									$('#data_tablesdet').submit();
									$('#html_dpsd').val('');
								});
							});
						</script>
	                	</div>
					</th>
				</tr>						
			</table>
		</div>		
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#Fexp").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"1950:+0",
});
$("#fnac").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"1950:+0",
});
$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2004:+0",
});
$("#txtFI").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFF").datepicker( "option", "minDate", selectedDate );
		$("#mytablaPres").trigger("update");		
	}	
});
$("#txtFF").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2013:+0",
	onSelect: function( selectedDate ){
		$("#txtFI").datepicker( "option", "maxDate", selectedDate );
		$("#mytablaPres").trigger("update");	
	}	
});	
$("#pfec").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
$("#pfecd").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});

$("#pfecd").change( function(){	
	$("#pfec").val($("#pfecd").val());
	return true;
});
$("#deptos").change( function(){	
	$("#depa").val($("#deptos").val());
	return true;
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});
function radios(radio,dato){
	if(radio==5) { $("#pcancel").val(dato);}
	if(radio==6) { $("#idpca").val(dato);}
	if(radio==7) { $("#verifica").val(dato);}
}

$("#nuevo").click( function(){	
	$("#id").val('');$("#ide").val('');$("#nombre").val('');$("#ap").val('');$("#am").val('');$("#ide").focus();
	$("#verifica").val('');	$("#canu").val('');	$("#col").val('');$("#ciu").val('');$("#edo").val('');$("#cp").val('82872');
	$("#tel").val('');	$("#curp").val('');	$("#rfc").val('');	$("#imss").val('');	$("#mail").val('');	$("#depto").val('');
	$("#pues").val('');	$("#txtFecha").val('');	$("#suel").val('');$("#fnac").val('');$("#ctadep").val('');
 return true;
})
$("#pnuevo").click( function(){	
	$("#idpe").val('');
	$("#pide").val('');	$("#pimp").val('');	$("#pobs").val('');$("#pide").focus();
 return true;
})
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#pborrar").click( function(){	
	id=$("#idpe").val();
	if(id!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/empleados/pborrar", 
				data: "id="+$("#idpe").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								alert("Datos Eliminados correctamente");
								$("#mytablaPres").trigger("update");
								$("#mytablaSG").trigger("update");
								$("#mytablaSemp").trigger("update");
								$("#mytablaSdet").trigger("update");
								$("#pnuevo").click();					
							}else{
								alert("Error con la base de datos o usted no ha eliminado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder eliminar datos");				
		return false;
	}	 
});
$("#caceptar").click( function(){	
	id=$("#idpd").val();
	if(id!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/empleados/cancelar", 
				data: "id="+$("#idpd").val()+"&pcancel="+$("#pcancel").val(),
				success: 	
						function(msg){															
							if(msg!=0){														
								alert("Cargos y Abonos de Empleado se han actualizados");
								$("#mytablaPres").trigger("update");
								$("#mytablaSG").trigger("update");
								$("#mytablaSemp").trigger("update");
								$("#mytablaSdet").trigger("update");
								$("#pnuevo").click();					
							}else{
								alert("Error con la base de datos o usted no ha actualizado nada");
							}					
						}	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder actualizar datos de empleado");				
		return false;
	}	 
});
$("#paceptar").click( function(){	
	imp=$("#pimp").val();
	id=$("#idpe").val();
	numero=$("#pide").val();
	if(numero!=''){
	  if(imp!=''){
	  	if(id!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/empleados/pactualizar", 
						data: "pidp="+$("#idpe").val()+"&pfec="+$("#pfec").val()+"&pimp="+$("#pimp").val()+"&pide="+$("#pide").val()+"&pca="+$("#idpca").val()+"&pobs="+$("#pobs").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytablaPres").trigger("update");
										$("#mytablaSG").trigger("update");
										$("#mytablaSemp").trigger("update");
										$("#mytablaSdet").trigger("update");
										$("#pnuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/empleados/pagregar", 
						data: "pfec="+$("#pfec").val()+"&pimp="+$("#pimp").val()+"&pide="+$("#pide").val()+"&pca="+$("#idpca").val()+"&pobs="+$("#pobs").val(),
						success: 
								function(msg){															
									if(msg!=0){														
										alert("Datos Registrados correctamente");
										$("#mytablaPres").trigger("update");
										$("#mytablaSG").trigger("update");
										$("#mytablaSemp").trigger("update");
										$("#mytablaSdet").trigger("update");
										$("#pnuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
			}
						
		}else{
			alert("Error: Registre Cantidad");	
			$("#pimp").focus();
				return false;
		}
	}else{
		alert("Error: Registre Numero de Empleado");
		$("#pide").focus();
			return false;
	}

});
$("#contrato").click( function(){	
	id=$("#id").val();
	if(id!=''){
		$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/empleados/pdfrepcon/", 
				async:false,
				data: "nom="+$("#nombre").val(),	
			});
	}else{
		alert("Error: Necesita realizar seleccion para poder generar contrato");				
		return false;
	}	 
});
$("#aceptar").click( function(){	
	nombre=$("#nombre").val();
	id=$("#id").val();
	numero=$("#ide").val();
	if(numero!=''){
	  if(nombre!=''){
	  	if(id!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/empleados/actualizar", 
						data: "id="+$("#id").val()+"&nombre="+$("#nombre").val()+"&ap="+$("#ap").val()+"&am="+$("#am").val()+"&veri="+$("#verifica").val()+"&canu="+$("#canu").val()+"&col="+$("#col").val()+"&ciu="+$("#ciu").val()+"&edo="+$("#edo").val()+"&cp="+$("#cp").val()+"&tel="+$("#tel").val()+"&curp="+$("#curp").val()+"&rfc="+$("#rfc").val()+"&imss="+$("#imss").val()+"&mail="+$("#mail").val()+"&depto="+$("#depto").val()+"&pues="+$("#pues").val()+"&fec="+$("#txtFecha").val()+"&suel="+$("#suel").val()+"&fnac="+$("#fnac").val()+"&ctad="+$("#ctadep").val()+"&ctav="+$("#ctavia").val()+"&sex="+$("#sexo").val()+"&civ="+$("#civil").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytabla").trigger("update");
										$("#accordion").accordion( "activate",0 );	
										$("#nuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/empleados/agregar", 
						data: "ide="+$("#ide").val()+"&nombre="+$("#nombre").val()+"&ap="+$("#ap").val()+"&am="+$("#am").val()+"&canu="+$("#canu").val()+"&col="+$("#col").val()+"&ciu="+$("#ciu").val()+"&edo="+$("#edo").val()+"&cp="+$("#cp").val()+"&tel="+$("#tel").val()+"&curp="+$("#curp").val()+"&rfc="+$("#rfc").val()+"&imss="+$("#imss").val()+"&mail="+$("#mail").val()+"&depto="+$("#depto").val()+"&pues="+$("#pues").val()+"&fec="+$("#txtFecha").val()+"&suel="+$("#suel").val()+"&fnac="+$("#fnac").val()+"&ctad="+$("#ctadep").val()+"&ctav="+$("#ctavia").val()+"&sex="+$("#sexo").val()+"&civ="+$("#civil").val(),
						success: 
								function(msg){	
									var obj = jQuery.parseJSON( msg );
									//alert (obj.msg);													
									if(obj.msg=='0'){														
										alert("Empleado registrado correctamente");
										$("#mytabla").trigger("update");
										$("#accordion").accordion( "activate",0 );	
										$("#nuevo").click();	
									}else if(obj.msg=='1'){
											alert("Numero de Empleado ya Registrado");
											$("#ide").val('');
											$("#id").val('');
										}else{
											alert("Error con la base de datos o usted no ha ingresado nada");
									}
									return false;					
								}		
				});
			}
						
		}else{
			alert("Error: Registre Nombre");	
			$("#nombre").focus();
				return false;
		}
	}else{
		alert("Error: Registre Numero de Empleado");
		$("#ide").focus();
			return false;
	}

});
$(document).ready(function(){
	var f = new Date();
	$("#txtFI").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());		
	$("#Fexp").val(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
	var totp=0;
	$("#depa").val($("#deptos").val());
	$("#tabla_emp").ajaxSorter({
		url:'<?php echo base_url()?>index.php/empleados/tabla',  
		filters:['cmbActivos','deptos','cmbSindatos'],
        active:['activo',-1],  
        multipleFilter:true,
        sort:false,
        onRowClick:function(){
            $("#accordion").accordion( "activate",1 );
           	$("#id").val($(this).data('id'));	$("#ide").val($(this).data('ide'));$("#ide").focus();$("#sexo").val($(this).data('sexo'));$("#sexo1").val($(this).data('sexo'));$("#civil").val($(this).data('civil'));$("#civil1").val($(this).data('civil'));
			$("#nombre").val($(this).data('nombre'));$("#ap").val($(this).data('ap'));$("#am").val($(this).data('am'));
			$("#verifica").val($(this).data('activo'));
			$("#canu").val($(this).data('canu'));$("#col").val($(this).data('col'));$("#ciu").val($(this).data('ciu'));	$("#edo").val($(this).data('edo'));			
			$("#cp").val($(this).data('cp'));
			$("#tel").val($(this).data('tel'));$("#curp").val($(this).data('curp'));$("#rfc").val($(this).data('rfc'));$("#imss").val($(this).data('imss'));
			$("#mail").val($(this).data('mail'));
			$("#depto").val($(this).data('depto'));$("#pues").val($(this).data('puesto'));$("#txtFecha").val($(this).data('fecing'));
			$("#suel").val($(this).data('sueldo'));$("#fnac").val($(this).data('fnac'));$("#ctadep").val($(this).data('ctadep'));$("#ctavia").val($(this).data('ctavia'));
			verifica=$(this).data('activo');
			$("#nomb").val($(this).data('nombre')+' '+$(this).data('ap')+' '+$(this).data('am'));
			$("#domi").val($(this).data('canu')+' '+$(this).data('col')+' '+$(this).data('ciu')+' '+$(this).data('edo'));
			$("#rfc1").val($(this).data('rfc'));$("#imss1").val($(this).data('imss'));$("#curp1").val($(this).data('curp'));
			$("#fec1").val($(this).data('fecing'));$("#sue1").val($(this).data('sueldo'));$("#pue1").val($(this).data('puesto'));
			$("#dep1").val($(this).data('depto'));$("#fnac1").val($(this).data('fnac'));
			$("#nomc").val($(this).data('nombre')+' '+$(this).data('ap')+' '+$(this).data('am'));
			$("#domc").val($(this).data('canu')+' '+$(this).data('col')+' '+$(this).data('ciu')+' '+$(this).data('edo'));
			$("#rfc2").val($(this).data('rfc'));$("#imss2").val($(this).data('imss'));
			$("#dep2").val($(this).data('depto'));$("#pue2").val($(this).data('puesto'));$("#curp2").val($(this).data('curp'));
			num="<?= base_url()?>"+'assets/credencial/'+$("#ide").val()+'.jpg';
			fim="<?= base_url()?>"+'assets/credencial/'+$("#ide").val()+'f.jpg';
	 		$("#numero").attr("src",num);
	 		$("#firmas").attr("src",fim);
			$('input:radio[name=rbFoto]:nth(0)').attr('checked',true);
			$("#nemp").val($(this).data('ide'));
			//$("#nemp").val($(this).data('ide')+'_fir.jpg');
			if(verifica==0 || verifica==-1){
				if(verifica==0){ $('input:radio[name=rbVerifica]:nth(0)').attr('checked',true); }
				if(verifica==-1){ $('input:radio[name=rbVerifica]:nth(1)').attr('checked',true); }								
			}else{
				$('input:radio[name=rbVerifica]:nth(0)').attr('checked',false);$('input:radio[name=rbVerifica]:nth(1)').attr('checked',false);				
			}		
        },
       onSuccess:function(){
       		$('#tabla_emp tbody tr').map(function(){	    		
	    		$("#num").val($(this).data('totp'));    	
	    	});
    	}
    });
	$("#tabla_pre").ajaxSorter({
		url:'<?php echo base_url()?>index.php/empleados/tablapre',  
		//filters:['pfecd'],
		filters:['txtFI','txtFF'],
        active:['pcancel',-1],  
        //multipleFilter:true,
        sort:false,
        onRowClick:function(){
           	$("#idpe").val($(this).data('idp'));	
           	$("#pide").val($(this).data('pide'));
			$("#pfec").val($(this).data('pfec'));
			$("#pimp").val($(this).data('pimp'));
			$("#idpca").val($(this).data('pca'));
			idpca=$(this).data('pca');
			if(idpca==1 || idpca==2){
				if(idpca==1){ $('input:radio[name=rbSel]:nth(0)').attr('checked',true); }
				if(idpca==2){ $('input:radio[name=rbSel]:nth(1)').attr('checked',true); }								
			}else{
				$('input:radio[name=rbSel]:nth(0)').attr('checked',false);$('input:radio[name=rbSel]:nth(1)').attr('checked',false);				
			}		
			$("#pobs").val($(this).data('pobs'));
       },
        onSuccess:function(){
        	$('#tabla_pre tbody tr').map(function(){	    		
	    		if($(this).data('cap')=='Total:'){
	    			$(this).css('background','lightgreen');	    				    				    			    			
	    		}    	
	    	});
    		$('#tabla_pre tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','center');  })
    		$('#tabla_pre tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');  })
    	}
            
	});
	$("#tabla_sem").ajaxSorter({
		url:'<?php echo base_url()?>index.php/empleados/tablaE',  
		filters:['cmbCancelacion'],
		//multipleFilter:true,
		//active:['sal1',0,'<='],
		sort:false,
        onRowClick:function(){
        	if($(this).data('ide')>0){
            $("#idpd").val($(this).data('ide'));
			$("#nom").val($(this).data('nom'));$("#nomt").val($(this).data('nom'));
			$("#pcancel").val($(this).data('pcancel'));
			pcancel=$(this).data('pcancel');
			if(pcancel==0 || pcancel==-1){
				if(pcancel==0){ $('input:radio[name=rbVerificaCA]:nth(0)').attr('checked',true); }
				if(pcancel==-1){ $('input:radio[name=rbVerificaCA]:nth(1)').attr('checked',true); }								
			}else{
				$('input:radio[name=rbVerificaCA]:nth(0)').attr('checked',false);$('input:radio[name=rbVerificaCA]:nth(1)').attr('checked',false);				
			}	
			$("#tabla_sde").ajaxSorter({
				url:'<?php echo base_url()?>index.php/empleados/tablaSD', 
				filters:['idpd','cmbCancelacion'], 
				active:['pcancel',-1],
				sort:false,
				onSuccess:function(){
        			$('#tabla_sde tbody tr').map(function(){	    		
	    				if($(this).data('fecd')=='TOTALES'){
	    					$(this).css('background','lightyellow');	    				    				    			    			
	    				}    	
	    			});
    				$('#tabla_sde tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right');  })
    				$('#tabla_sde tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');  })
    				$('#tabla_sde tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');  })
    			}
			});
			}
        }, 
        onSuccess:function(){
        	$('#tabla_sem tbody tr').map(function(){	    		
	    		if($(this).data('ide')=='TOTALES'){
	    			$(this).css('background','lightyellow');	    				    				    			    			
	    	}   
	    	if($(this).data('sal1')<='0'){
	    			$(this).css('background','red');	    				    				    			    				    			
	    	}    	
	    	});
    		$('#tabla_sem tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right');  })
    		$('#tabla_sem tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','right');  })
    		$('#tabla_sem tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right');  })    		
    	}   
	});	
});
</script>