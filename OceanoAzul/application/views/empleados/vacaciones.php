    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 20px 70px 20px; }
	#header { position: fixed; left: 0px; top: -10px; right: 0px; height: 150px; text-align: center;  } 
	#footer { position: fixed; margin-left:-50px; left: -20px; bottom: -120px; right: 0px; width:830px; height: 120px; background-color: navy; }
</style>  
<title>Recomendacion</title>
</head>
<body>
	<div id="header"> 
		<table style="width: 770px;">
			<tr>
				<td width="200px">
					<p style="font-size: 8px;"  >
						<img src="<?php echo base_url();?>assets/images/oceanoazul.jpg" width="200" height="60" border="0">
						 <br />
					</p>
				</td>	
							
			</tr>
		</table>
	</div> 
	<div id="footer" > 
		<table style="width: 770px; margin-top: 1px; margin-left: 50px ">
			<tr style="font-size: 10px; color: white;">
				<td width="160px">www.acuicolaoceanoazul.mx</td>	
				<td width="490">
					Av. Reforma No.2007-B Int. 803, Col. Flamingos, Mazatlán, Sin. México, RFC:AOA-180206-SI2, CP:82149 - Tel:(669)985-64-46 
				</td>		
			</tr>
		</table> 
	</div>
	<?php 
		$f=explode("-",$fecha); 
      	$nummes=(int)$f[1]; 
      	$mes1="0-Enero-Febrero-Marzo-Abril-Mayo-Junio-Julio-Agosto-Septiembre-Octubre-Noviembre-Diciembre"; 
      	$mes1=explode("-",$mes1); 
      	//$desfecha="$f[2]-$mes1[$nummes]-$f[0]";
		$fi=explode("-",$fechaing);
		$nummesi=(int)$fi[1]; 
      	$mesi="0-Enero-Febrero-Marzo-Abril-Mayo-Junio-Julio-Agosto-Septiembre-Octubre-Noviembre-Diciembre"; 
      	$mesi=explode("-",$mesi); 
	?>	
<div style="margin-left: -25px">
	<table style="font-family: Verdana; font-size: 13">
		<tr>
			<th colspan="2" style="text-align: right"><br /><br /><br /><br />Mazatlán, Sinaloa  <?= $f[2]." de ".$mes1[$nummes]. " de ".$f[0]?>.<br /><br /><br /> </th>	
		</tr>
		<tr>
			<th colspan="2" style="text-align: right">Asunto: <u>Carta de Vacaciones.</u> <br /><br /><br /><br /></th>
		</tr>
		<tr>
			<th colspan="2" style="text-align: left">A quien Corresponda.-<br /><br /></th>
		</tr>
		<tr>
			<th></th>
			<th style="width: 480px">
				<p style="text-align: justify">  	
				Por medio de la presente hago de su conocimiento que el C. <u><?= $empleado?></u>. Con número de afiliación de IMSS <?= $imss?>,
				 labora en esta empresa desde el día <?= $fi[2]." de ".$mesi[$nummesi]. " de ".$fi[0]?>, que su responsabilidad es <u><?= $pues?></u> en el Departamento de <u><?= $depa?></u>
				  y así mismo notificamos también que inicia su período vacacional de <?= $dias?> días a partir de la elaboración de este documento. <br /> <br />  
				Sin otro particular por el momento y para mayor información quedo a sus órdenes muy, 
 
				</p>
				<br /><br />
			</th>	
		</tr>
		<tr>
			<th></th>
			<th style="width: 480px">
				<p style="text-align: center">  	
				Atentamente <br /><br /><br /><br /><br />
				___________________________________________<br />
				<?= $firma?><br />                                  	
				<?= $puesto?>
				</p>
				<br />
			</th>	
		</tr>	
	</table>
</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>