<?php $this->load->view('header_cv'); ?>
<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;
	font-size: 25px;
	background: none;		
}   

</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><img src="<?php echo base_url();?>assets/images/menu/empleados.png" width="25" height="25" border="0"> Empleados </li>			
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="accordion" style="font-size: 12px">
			<h3 align="left" style="font-size: 12px" ><a href="#"> Registrados </a></h3>
        	<div  style="height:345px; text-align: right; font-size: 12px;"> 
        		Filtrar aquellos que les falte la captura de: 
        		<select name="cmbSindatos" id="cmbSindatos" style="font-size: 10px; height: 25px;  margin-top: -15px;" >
          				<option value="0">Elija Opcion</option>
          				<option value="1">Apellido Paterno</option><option value="2">Apellido Materno</option><option value="3">CURP</option>
          				<option value="4">RFC</option><option value="5">IMSS</option><option value="6">E-Mail</option>
          		</select>                                                                   
            	<div class="ajaxSorterDiv" id="tabla_emp" name="tabla_emp" style="margin-top: 1px; height: 367px; " >                
                	<span class="ajaxTable" style="height: 301px; width: 882px">
                    	<table id="mytabla" name="mytabla" class="ajaxSorter" width="880px">
                        	<thead title="" >                            
                            	<th data-order = "ide" >No.</th> 	<th data-order = "nombre" >Nombre</th>                                                        
                            	<th data-order = "ap" >Apellido Paterno</th>  <th data-order = "am" >Apellido Materno</th>
                        	</thead>
                        	<tbody title="Seleccione para realizar cambios" style="font-size: 11px">  	</tbody>
                    	</table>
                	</span> 
             		<div class="ajaxpager" style="margin-top: -5px">        
                    	<ul class="order_list"></ul>     
                    	<form method="post" action="<?= base_url()?>index.php/empleados/pdfrep" >                    	             	                                                
                        	<img src="<?php echo base_url();?>assets/img/sorter/first.png" class="first"/>
                        	<img src="<?php echo base_url();?>assets/img/sorter/prev.png" class="prev"/>
                        	<input type="text" class="pagedisplay" size="3" /> /
                        	<input type="text" class="pagedisplayMax" size="3" readonly="1" disabled="disabled"/>
                        	<img src="<?php echo base_url();?>assets/img/sorter/next.png" class="next"/>
                        	<img src="<?php echo base_url();?>assets/img/sorter/last.png" class="last"/>
                        	<select class="pagesize" style="font-size: 10px; height: 23px">
                                <option selected="selected" value="10">10</option>
                                <option value="20">20</option>                            
                                <option value='0'>Gral</option>                                          
                        	</select>                                                
                        	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        	<!--<button id="imprimirB" name="imprimirB" style="height: 28px; width: 25px;"></button>-->
                        	<!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        	<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 						</form>   
 					</div>                                      
            </div>    
        </div>
        <h3 align="left" style="font-size: 12px" ><a href="#"> Agregar O Actualizar </a></h3>
       	<div style="height:150px; ">  
        	<table border="2px" width="910px">
				<thead style="background-color: #DBE5F1" ><th colspan="5" height="18px" style="font-size: 16px">Generales
					<input type="hidden" size="4%"  type="text" name="id" id="id" >	</th>
				</thead>
				<tbody style="background-color: #F7F7F7">
					<tr>
						<th style="margin: 2px; font-size: 14px">No.</th>
						<th style="margin: 2px; font-size: 14px">Nombre</th>
						<th style="margin: 2px; font-size: 14px">Apellido Paterno</th>
						<th style="margin: 2px; font-size: 14px">Apellido Materno</th>
						<th style="font-size: 14px; text-align: center; ">
							<input size="2%" type="hidden" name="verifica" id="verifica" readonly="true" >
							Estatus 
						</th>
					</tr>
					<tr>
						<th  style="font-size: 14px; background-color: white;" ><input size="4%"  type="text" name="ide" id="ide" style="text-align: center"  ></th>
						<th style="font-size: 14px; background-color: white"><input size="50%"  type="text" name="nombre" id="nombre" ></th>
						<th style="font-size: 14px; background-color: white"><input size="30%" type="text" name="ap" id="ap" value=""></th>
						<th style="font-size: 14px; background-color: white"><input size="30%" type="text" name="am" id="am" value=""></th>
						<th style="font-size: 14px; background-color: white; text-align: center">
							<input name="rbVerifica" type="radio" value="0" onclick="radios(7,0)"/><span>Activo</span>
							&nbsp;&nbsp;<input name="rbVerifica" type="radio" value="-1" onclick="radios(7,-1)"/><span style="color: red">Baja</span>
						</th>
					</tr>
				</tbody>
				</table>	
				<table border="2px" width="910px" style="margin-top: -10px">
				<thead style="background-color: #DBE5F1" ><th colspan="5" height="18px" style="font-size: 16px">Domicilio</th>
				</thead>
				<tbody style="background-color: #F7F7F7">
					<tr>
						<th style="margin: 2px; font-size: 14px">Calle y Número</th>
						<th style="margin: 2px; font-size: 14px">Colonia</th>
						<th style="margin: 2px; font-size: 14px">Ciudad</th>
						<th style="margin: 2px; font-size: 14px">Estado</th>
						<th style="margin: 2px;font-size: 14px;">C.P.</th>
					</tr>
					<tr>
						<th  style="font-size: 14px; background-color: white;" ><input size="45%"  type="text" name="canu" id="canu" ></th>
						<th style="font-size: 14px; background-color: white"><input size="30%"  type="text" name="col" id="col" ></th>
						<th style="font-size: 14px; background-color: white"><input size="25%" type="text" name="ciu" id="ciu" value=""></th>
						<th style="font-size: 14px; background-color: white">
						<select name="edo" id="edo" style="margin-top: 1px;font-size: 13px;">
							<option value="Sinaloa" >Sinaloa</option>	
    						<option value="Aguascalientes">Aguascalientes</option>
    						<option value="Baja California">Baja California</option> <option value="Baja California Sur" >Baja California Sur</option>	
    						<option value="Campeche">Campeche</option> <option value="Chiapas"  >Chiapas</option>	
    						<option value="Chihuahua">Chihuahua</option> <option value="Coahulia" >Coahuila</option>
    						<option value="Colima" >Colima</option> <option value="Distrito Federal" >Distrito Federal</option>	
    						<option value="Durango" >Durango</option> <option value="Guanajuato" >Guanajuato</option>	
    						<option value="Guerrero" >Guerrero</option> <option value="Hidalgo" >Hidalgo</option>	
    						<option value="Jalisco" >Jalisco</option> <option value="Estado de Mexico" >Estado de M&eacute;xico</option>
    						<option value="Michoacan" >Michoac&aacute;n</option> <option value="Morelos" >Morelos</option>		
    						<option value="Nayarit" >Nayarit</option> <option value="Nuevo Leon" >Nuevo Le&oacute;n</option>			
    						<option value="Oaxaca" >Oaxaca</option> <option value="Puebla" >Puebla</option>
    						<option value="Queretaro" >Queretaro</option> <option value="Quintana Roo" >Quintana Roo</option>
    						<option value="San Luis Potosi" >San Luis Potos&iacute;</option> <!--<option value="Sinaloa" >Sinaloa</option>-->
    						<option value="Sonora" >Sonora</option> <option value="Tamaulipas" >Tamaulipas</option>
    						<option value="Tabasco" >Tabasco</option> <option value="Tlaxcala" >Tlaxcala</option>
    						<option value="Veracruz" >Veracruz</option> <option value="Yucatan" >Yucat&aacute;n</option>
    						<option value="Zacatecas" >Zacatecas</option>
  						</select>
						</th>
						<th style="font-size: 14px; background-color: white; text-align: center">
							<input size="6%" type="text" name="cp" id="cp" value="">
						</th>
					</tr>
    			</tbody>				
    		</table>
    		<table border="2px" width="910px" style="margin-top: -10px">
				<thead style="background-color: #DBE5F1" ><th colspan="5" height="18px" style="font-size: 16px">Adicionales</th>
				</thead>
				<tbody style="background-color: #F7F7F7">
					<tr>
						<th style="margin: 2px; font-size: 14px">Teléfono</th>
						<th style="margin: 2px; font-size: 14px">C.U.R.P.</th>
						<th style="margin: 2px; font-size: 14px">R.F.C.</th>
						<th style="margin: 2px; font-size: 14px">I.M.S.S.</th>
						<th style="margin: 2px;font-size: 14px;">E-mail</th>
					</tr>
					<tr>
						<th  style="font-size: 14px; background-color: white;" ><input size="17%"  type="text" name="tel" id="tel" ></th>
						<th style="font-size: 14px; background-color: white"><input size="29%"  type="text" name="curp" id="curp" ></th>
						<th style="font-size: 14px; background-color: white"><input size="20%" type="text" name="rfc" id="rfc" value=""></th>
						<th style="font-size: 14px; background-color: white"><input size="15%"  type="text" name="imss" id="imss" ></th>
						<th style="font-size: 14px; background-color: white; text-align: center">
							<input size="48%" type="text" name="mail" id="mail" value="">
						</th>
					</tr>
    			</tbody>				
    		</table>
    		<table border="2px" width="910px" style="margin-top: -10px">
				<thead style="background-color: #DBE5F1" ><th colspan="4" height="18px" style="font-size: 16px">Administrativo</th>
				</thead>
				<tbody style="background-color: #F7F7F7">
					<tr>
						<th style="margin: 2px; font-size: 14px">Departamento</th>
						<th style="margin: 2px; font-size: 14px">Puesto</th>
						<th style="margin: 2px; font-size: 14px">Fecha Ingreso</th>
						<th style="margin: 2px; font-size: 14px">Sueldo Semanal</th>						
					</tr>
					<tr>
						<th style="font-size: 14px; background-color: white">
						<select name="depto" id="depto" style="margin-top: 1px;font-size: 13px;">
							<option value="-" >-</option> 
							<option value="Administración" >Administración</option><option value="Almacen" >Almacen</option>
							<option value="Artemia">Artemia</option><option value="Cocina">Cocina</option>
    						<option value="Control de Calidad">Control de Calidad</option> <option value="Cosceha" >Cosecha</option>	
    						<option value="Genética">Genética</option> <option value="Larvicultura"  >Larvicultura</option>	
    						<option value="Maduración">Maduración</option> <option value="Mantenimiento" >Mantenimiento</option>
    						<option value="Microalgas">Microalgas</option><option value="Transporte">Transporte</option>
    						<option value="Producción">Producción</option><option value="Vigilancia">Vigilancia</option>
    						   						
  						</select>
						</th>
						<th  style="font-size: 14px; background-color: white;" >
							<select name="pues" id="pues" style="margin-top: 1px;font-size: 13px;">
							<option value="-" >-</option>	
							<option value="Auxiliar">Auxiliar</option><option value="Ayudante">Ayudante</option>
							<option value="Biólogo" >Biólogo</option><option value="Encargado" >Encargado</option>
							<option value="Gerente" >Gerente</option><option value="Técnico">Técnico</option>
    						<option value="Vigilante">Vigilante</option>    						
  						</select>
						</th>
						<th style="font-size: 14px; background-color: white">
							<input size="13%" type="text" name="txtFecha" id="txtFecha" class="fecha redondo mUp" readonly="true" style="text-align: center;" >
						</th>
						<th style="font-size: 14px; background-color: white;" >$ <input style="text-align: right" size="13%" onKeyPress="return(currencyFormat(this,',','.',event,'P'));" type="text" name="suel" id="suel" ></th>
					</tr>
    			</tbody>
				<tfoot style="background-color:#DBE5F1">
					<th colspan="4"><center>
						<?php if($usuario=="Jesus Benítez" or $usuario=="Anita Espino"){ ?>
							<input style="font-size: 18px;" type="submit" id="nuevo" name="nuevo" value="Nuevo"  />
						<?php }?>
						<input style="font-size: 18px" type="submit" id="aceptar" name="aceptar" value="Guardar" />
						</center></th>
				</tfoot>
    		</table>
        </div>	
	 </div> 	
 	</div> 	
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#txtFecha").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
});
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});
function radios(radio,dato){
	if(radio==7) { $("#verifica").val(dato);}
}
$("#tabla_emp").ajaxSorter({
		url:'<?php echo base_url()?>index.php/empleados/tabla',  
		filters:['cmbSindatos'],
        active:['activo',-1],  
        multipleFilter:true,
        //sort:false,
        onRowClick:function(){
            $("#accordion").accordion( "activate",1 );
           	$("#id").val($(this).data('id'));	$("#ide").val($(this).data('ide'));$("#ide").focus();
			$("#nombre").val($(this).data('nombre'));$("#ap").val($(this).data('ap'));$("#am").val($(this).data('am'));
			$("#verifica").val($(this).data('activo'));
			$("#canu").val($(this).data('canu'));$("#col").val($(this).data('col'));$("#ciu").val($(this).data('ciu'));	$("#edo").val($(this).data('edo'));			
			$("#cp").val($(this).data('cp'));
			$("#tel").val($(this).data('tel'));$("#curp").val($(this).data('curp'));$("#rfc").val($(this).data('rfc'));$("#imss").val($(this).data('imss'));
			$("#mail").val($(this).data('mail'));
			$("#depto").val($(this).data('depto'));$("#pues").val($(this).data('puesto'));$("#txtFecha").val($(this).data('fecing'));
			$("#suel").val($(this).data('sueldo'));
			verifica=$(this).data('activo');
			if(verifica==0 || verifica==-1){
				if(verifica==0){ $('input:radio[name=rbVerifica]:nth(0)').attr('checked',true); }
				if(verifica==-1){ $('input:radio[name=rbVerifica]:nth(1)').attr('checked',true); }								
			}else{
				$('input:radio[name=rbVerifica]:nth(0)').attr('checked',false);$('input:radio[name=rbVerifica]:nth(1)').attr('checked',false);				
			}		
        }   
});
$("#nuevo").click( function(){	
	$("#id").val('');$("#ide").val('');$("#nombre").val('');$("#ap").val('');$("#am").val('');$("#ide").focus();
	$("#verifica").val('');	$("#canu").val('');	$("#col").val('');$("#ciu").val('');$("#edo").val('');$("#cp").val('');
	$("#tel").val('');	$("#curp").val('');	$("#rfc").val('');	$("#imss").val('');	$("#mail").val('');	$("#depto").val('');
	$("#pues").val('');	$("#txtFecha").val('');	$("#suel").val('');
 return true;
}
)
function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 	
      return false;
  } 
}
$("#aceptar").click( function(){	
	nombre=$("#nombre").val();
	id=$("#id").val();
	numero=$("#ide").val();
	if(numero!=''){
	  if(nombre!=''){
	  	if(id!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/empleados/actualizar", 
						data: "id="+$("#id").val()+"&nombre="+$("#nombre").val()+"&ap="+$("#ap").val()+"&am="+$("#am").val()+"&veri="+$("#verifica").val()+"&canu="+$("#canu").val()+"&col="+$("#col").val()+"&ciu="+$("#ciu").val()+"&edo="+$("#edo").val()+"&cp="+$("#cp").val()+"&tel="+$("#tel").val()+"&curp="+$("#curp").val()+"&rfc="+$("#rfc").val()+"&imss="+$("#imss").val()+"&mail="+$("#mail").val()+"&depto="+$("#depto").val()+"&pues="+$("#pues").val()+"&fec="+$("#txtFecha").val()+"&suel="+$("#suel").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Datos actualizados correctamente");
										$("#mytabla").trigger("update");
										$("#accordion").accordion( "activate",0 );	
										$("#nuevo").click();																										
									}else{
										alert("Error con la base de datos o usted no ha cambiado nada");
									}					
								}		
				});
			}else{
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/empleados/agregar", 
						data: "ide="+$("#ide").val()+"&nombre="+$("#nombre").val()+"&ap="+$("#ap").val()+"&am="+$("#am").val()+"&canu="+$("#canu").val()+"&col="+$("#col").val()+"&ciu="+$("#ciu").val()+"&edo="+$("#edo").val()+"&cp="+$("#cp").val()+"&tel="+$("#tel").val()+"&curp="+$("#curp").val()+"&rfc="+$("#rfc").val()+"&imss="+$("#imss").val()+"&mail="+$("#mail").val()+"&depto="+$("#depto").val()+"&pues="+$("#pues").val()+"&fec="+$("#txtFecha").val()+"&suel="+$("#suel").val(),
						success: 
								function(msg){	
									var obj = jQuery.parseJSON( msg );
									//alert (obj.msg);													
									if(obj.msg=='0'){														
										alert("Empleado registrado correctamente");
										$("#mytabla").trigger("update");
										$("#accordion").accordion( "activate",0 );	
										$("#nuevo").click();	
									}else if(obj.msg=='1'){
											alert("Numero de Empleado ya Registrado");
											$("#ide").val('');
											$("#id").val('');
										}else{
											alert("Error con la base de datos o usted no ha ingresado nada");
									}
									return false;					
								}		
				});
			}
						
		}else{
			alert("Error: Registre Nombre");	
			$("#nombre").focus();
				return false;
		}
	}else{
		alert("Error: Registre Numero de Empleado");
		$("#ide").focus();
			return false;
	}

});

</script>