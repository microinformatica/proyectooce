    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	@page { margin: 20px 70px 20px; }
</style>  
<title>Contrato Laboral</title>
</head>
<body>
	<?php 
		$f=explode("-",$fec); 
      	$nummes=(int)$f[1]; 
      	$mes1="0-Enero-Febrero-Marzo-Abril-Mayo-Junio-Julio-Agosto-Septiembre-Octubre-Noviembre-Diciembre"; 
      	$mes1=explode("-",$mes1); 
      	$desfecha="$f[2]-$mes1[$nummes]-$f[0]"; 
		$act=date("Y");
		$e=explode("-",$fnac);
		$edad=$act-$e[0];	
	?>	
<div style="margin-left: -25px">
	<p style="text-align: justify">
		<strong>CONTRATO INDIVIDUAL DE TRABAJO POR TIEMPO DETERMINADO </strong>QUE CELEBRAN POR UNA PARTE,<strong> ACUICOLA OCEANO AZUL, S.A. DE C.V.</strong>, REPRESENTADA EN ESTE ACTO POR EL SEÑOR <strong>JOEL LIZARRAGA VALDEZ</strong>, A QUIEN EN LO SUCESIVO SE DESIGNARÁ COMO <strong><strong>“LA EMPRESA”</strong></strong>, Y POR LA OTRA,<strong> <?= $nom?></strong>, A QUIEN EN LO SUCESIVO SE DESIGNAR Á COMO <strong>“EL TRABAJADOR”</strong>,  AL TENOR DE LAS SIGUIENTES: 
	</p>
	<p style="text-align: center">
		D E C L A R A C I O N E S	 
	</p>
	<p style="text-align: justify">
		A) DECLARA  <strong><strong>“LA EMPRESA”</strong></strong> <br /><br />
		I.- Ser una sociedad mercantil, legalmente constituida de conformidad con las leyes aplicables en la república mexicana, según consta en la escritura pública número 565, de fecha 06 seis de Febrero del 2018 dos mil dieciocho, otorgada ante la fé del licenciado Rodrigo Llausás Azcona, Notario público número 200 en el Estado de Sinaloa, e inscrita en el Registro Público de la Propiedad y de Comercio de esta ciudad, bajo el folio mercantil Electrónico No. 2018012831, fechado en 16 dieciséis de Febrero 2019 dos mil diecinueve.<br /><br />
		II.- Que el objeto de la entidad sustituye, entre otras actividades, el diseño, construcción, establecimiento y operación de instalaciones para el cultivo, desarrollo, procesamiento y transportación de organismos acuáticos.<br /><br />
		III.- Que con motivo de la excesiva demanda temporal de trabajo, tiene un recargo en sus labores ordinarias, imposible de desahogar con su personal de planta, por lo que requiere de los servicios <strong>PERSONALES DEPENDIENTES</strong> de <strong>“EL EMPLEADO”</strong>, a efecto de que éste realice eventualmente  las funciones <?= $pues ?> en el área de <?= $depa ?>, durante un período de tiempo,  que vencerá precisamente el día __________________________________ al concluir la situación especial por la que atraviesa <strong><strong>“LA EMPRESA”</strong></strong>. <br /><br />
		IV.- Que tiene su domicilio fiscal en Av. Reforma 2007-B Int 803, colonia Flamingos de esta ciudad, y  su  Registro Federal de Contribuyentes es <strong>AOA-180206-SI2</strong>.<br /><br /> 
	</p>
	<p style="text-align: justify">
		B) BAJO PROTESTA DE DECIR VERDAD, DECLARA <strong>“EL TRABAJADOR”</strong> <br /><br />
		I.- Por sus generales, ser de nacionalidad mexicana, sexo <?= $sexo ?>,  estado civil <?= $civil ?>, de <?= $edad ?> años de edad, con registro federal de contribuyentes <?= $rfc?> y clave única de registro de población <?= $curp?>, inscrito en el Instituto Mexicano del Seguro Social con el número <?= $imss?>  y con domicilio <?= $dom?>.<br /><br />
		II.- Que cuenta con los conocimientos y aptitudes necesarias para prestar los servicios personales subordinados  de <?= $pues ?> del área de <?= $depa ?>, objeto de este convenio.<br /><br />
		III.- Estar en estado de salud excelente para desempeñar las actividades asignadas.<br />
	</p>
	<p style="text-align: center">
		C L Á U S U L A S	 
	</p>
	<p style="text-align: justify">
		 PRIMERA.- <strong>“EL TRABAJADOR”</strong> conviene en prestar sus servicios personales por tiempo determinado, y a partir del <?= $f[2]." de ".$mes1[$nummes]. " de ".$f[0]?> en el Área de <?= $depa ?>, de las instalaciones de “ LA EMPRESA”, ejecutando, con el esmero e intensidad apropiados, forma y condiciones que la patronal indique, todas las labores inherentes al puesto de <?= $pues ?>, más las que se relacionen directa o indirectamente con él, acorde a este contrato, la Ley Federal del Trabajo, buena fe, equidad, usos y costumbres de <strong><strong>“LA EMPRESA”</strong></strong>.
	</p>
	<p style="text-align: justify">
		Precisamente, y por lo que toca a este último rubro, deberá: <br />
		<?php
		switch ($tip){
				case '11': {?>
							a)	Realizar reportes de información contable.<br />
							b)	Recopilación y archivo de facturas correspondientes a gastos o costos en granja.<br />
							c)	Realizar reportes de asistencia e inasistencia de los empleados de la granja.<br />
							d)	Revisar consumos de diesel en cárcamos de bombeo.<br />
							e)	Recabar reportes de consumos de alimento para operación en granja.<br />
							f)	Apoyo en almacén.<br />
							g)	Uso de vehículos de la granja.<br />
							<?php break;}
				case '16': {?>
							a)	Trasladar al personal para sus actividades de trabajo en sus respectivos horarios.<br />
							b)	Apoyo en actividades de granja.<br />
							<?php break;}
				case '18': {?>
							!!! sin actividades registradas - Gerente Administración !!! <br />
							<?php break;}
				case '27': {?>
							a)	Encargado de realizar requisiciones de mercancías. <br />
							b)	Recibir insumos y registrar en el sistema de almacenamiento. <br />
							c)	Dar salida a la mercancía. <br />
							d)	Operación de montacargas para descargar alimento del transporte. <br />
							e)	Uso de vehículos de la granja. <br />
							<?php break;}
				case '31': {?>
							a)	Preparar desayuno y comida, así mismo dejar alimento para los empleados de la jornada mixta;<br /> 
							b)	Mantener en buen estado de limpieza el área de cocina de <strong><strong>“LA EMPRESA”</strong></strong>;<br />
							c)	Asistir y participar a las juntas departamentales a las que sea convocado, con motivo de la naturaleza de sus servicios,  con el objeto de aplicar y evaluar los sistemas operativos y de administración de <strong><strong>“LA EMPRESA”</strong></strong>;<br /> 
							d)	Asistir con toda puntualidad a sus labores, en los horarios pactados, y aprovechar integralmente el tiempo disponible para el desarrollo de programas de capacitación;<br />
							e)	Observar conducta intachable dentro de las instalaciones de <strong><strong>“LA EMPRESA”</strong></strong>; y,<br />
							f)	Mantener debidamente informado a su Superior, o Jefe  del Departamento al que pertenezca, sobre los avances en la prestación de sus servicios y cualquier incidente o problema surgido en la actualización de los mismos.<br />
							<?php break;}
				case '37': {?>
							a)	Preparar desayuno y comida, así mismo dejar alimento para los empleados de la jornada mixta;<br /> 
							b)	Mantener en buen estado de limpieza el área de cocina de <strong><strong>“LA EMPRESA”</strong></strong>;<br />
							c)	Asistir y participar a las juntas departamentales a las que sea convocado, con motivo de la naturaleza de sus servicios,  con el objeto de aplicar y evaluar los sistemas operativos y de administración de <strong><strong>“LA EMPRESA”</strong></strong>;<br /> 
							d)	Asistir con toda puntualidad a sus labores, en los horarios pactados, y aprovechar integralmente el tiempo disponible para el desarrollo de programas de capacitación;<br />
							e)	Observar conducta intachable dentro de las instalaciones de <strong><strong>“LA EMPRESA”</strong></strong>; y,<br />
							f)	Mantener debidamente informado a su Superior, o Jefe  del Departamento al que pertenezca, sobre los avances en la prestación de sus servicios y cualquier incidente o problema surgido en la actualización de los mismos.<br />
							<?php break;}			
				case '47': {?>
							a)	Prospección, búsqueda y negociación con proveedores.<br />
							b)	Analizar periódicamente los precios de los equipos, refacciones y materiales.<br />
							c)	Realizar todo tipo de compras en general que la granja requiere para su funcionamiento.<br />
							d)	Transporte de equipos, refacciones y materiales que requiera la granja para su óptimo funcionamiento.<br />
							e)	Uso de vehículos de la granja para cumplir con las actividades encomendadas.<br />
							f)	Hacer uso correcto de los recursos, buscando calidad, cantidad y precios.<br />
							<?php break;}
				case '52': {?>
							a)	Apoyo en todas aquellas actividades que conlleve a herrería y en ocasiones fabricación de estructuras.<br />
							b)	Apoyar en actividades del auxiliar mecánico.<br />
							<?php break;}
				case '53': {?>
							!!! sin actividades registradas - Aux Mantenimiento !!! <br />
							<?php break;}
				case '54': {?>
							a)	Apoyo en limpieza, reparación y mantenimiento de vehículos, maquinaria y equipos. <br />
							b)	Uso de maquinaria pesada. <br />
							<?php break;}
				case '57': {?>
							!!! sin actividades registradas - Encargado Mantenimiento !!! <br />
							<?php break;}
				case '510': {?>
							a)	Limpieza, reparación y mantenimiento y reparacion de todo vehiculos, maquinaria y equipos.<br /> 
							b)	Realizar actividades de auxiliar mecanico.<br />
							<?php break;}
				case '511': {?>
							a)	Supervisar el optimo funcionamiento de motores de bombeo, maquinaria y vehiculos.<br />
							b)	Detectar corregir y dirigir la reparación de fallas de motores de bombeo, maquinaria y vehiculos.<br />
							<?php break;}
				case '518': {?>
							a)	Limpieza, reparación y mantenimiento y reparacion de todo vehiculos, maquinaria y equipos.<br /> 
							b)	Realizar actividades de auxiliar mecanico.<br />
							<?php break;}
				case '65': {?>
							a)	Encender y apagar equipos de bombeo de agua.<br />
							b)	Apoyar en la reparación y mantenimiento de motores, transmisiones, bombas e infraestructura del cárcamo de bombeo.<br />
							c)	Mantener en óptimas condiciones de operación los motores, transmisiones, bombas e infraestructura del cárcamo de bombeo.<br />
							d)	Mantener limpio y ordenado las instalaciones de los cárcamo y sus alrededores.<br />
							e)	Uso de maquinaria pesada para apoyar en el mantenimiento de los bordos.<br />
							<?php break;}
				case '68': {?>
							!!! sin actividades registradas - Gerente Produccion !!! <br />
							<?php break;}
				case '69': {?>
							a)	Pesar, cargar y aplicar el alimento balanceado a cada uno de los estanques. <br />
							b)	Realizar actividades de alimentación balanceado con blowers y alimentadores automáticos. <br />
							c)	Elaboración, instalación y mantenimiento de tablas para regular nivel de agua en estanques, así como entradas y salidas. <br />
							d)	Elaboración, instalación y mantenimiento de bastidores. <br />
							e)	Instalación y limpieza de bolsos. <br />
							f)	Elaboración e instalación de estadales. <br />
							g)	Realizar batimetrías. <br />
							h)	Desasolve, limpieza y pintura de compuertas de entrada y salida. <br />
							i)	Elaboración y mantenimiento de muelles. <br />
							j)	Elaboración y mantenimiento de charolas de alimentación. <br />
							k)	Aplicación de cal y productos químicos. <br />
							l)	Recolección de basura. <br />
							m)	Apoyo en actividades de cosecha. <br />
							n)	Mantener limpio y ordenado las instalaciones de la granja. <br />
							<?php break;}
				case '613': {?>
							a)	Organizar y supervisar actividades de técnicos y operarios. <br />
							b)	Asegurarse de pesar el alimento balanceado los kilos correspondientes para cada estanque.<br />
							c)	Supervisar la correcta aplicación y distribución de alimento balanceado en los estanques.<br />
							d)	Asegurarse que la aplicación de muestra de alimento balanceado a la charola se esté aplicando en tiempo y forma establecidos.<br />
							e)	Asegurarse que los charoleros realicen una correcta lectura de charola.<br />
							f)	Realizar biometrías.<br />
							g)	Mantener limpio y ordenado el almacén de alimento balanceado.<br />
							h)	Establecer las necesidades de requerimiento de agua, y acordar las horas/bombas necesarias.<br />
							i)	Asegurarse estén suministrando las horas/bomba acordadas.<br />
							j)	Cumplir con el recambio establecido para cada uno de los estanques.<br />
							k)	Asegurar que se estén lavando los bolsos de manera eficiente.<br />
							l)	Asegurarse que los bolsos de filtrado se encuentren en buenas condiciones, y en caso de tener algún desperfecto, corregirlo a la brevedad posible.<br />
							m)	Asegurarse que el bastidor de entrada y salida se cepillen de forma correcta y en todo momento se encuentre en óptimas condiciones, y en caso de tener algún desperfecto, corregirlo a la brevedad posible.<br />
							n)	Mantener limpio y óptimas condiciones los vehículos y equipos para llevar a cabo las actividades propias de la granja.<br />
							<?php break;}
				case '614': {?>
							a)	Organizar y supervisar actividades de técnicos y operarios. <br />
							b)	Asegurarse de pesar el alimento balanceado los kilos correspondientes para cada estanque.<br />
							c)	Supervisar la correcta aplicación y distribución de alimento balanceado en los estanques.<br />
							d)	Asegurarse que la aplicación de muestra de alimento balanceado a la charola se esté aplicando en tiempo y forma establecidos.<br />
							e)	Asegurarse que los charoleros realicen una correcta lectura de charola.<br />
							f)	Realizar biometrías.<br />
							g)	Mantener limpio y ordenado el almacén de alimento balanceado.<br />
							h)	Establecer las necesidades de requerimiento de agua, y acordar las horas/bombas necesarias.<br />
							i)	Asegurarse estén suministrando las horas/bomba acordadas.<br />
							j)	Cumplir con el recambio establecido para cada uno de los estanques.<br />
							k)	Asegurar que se estén lavando los bolsos de manera eficiente.<br />
							l)	Asegurarse que los bolsos de filtrado se encuentren en buenas condiciones, y en caso de tener algún desperfecto, corregirlo a la brevedad posible.<br />
							m)	Asegurarse que el bastidor de entrada y salida se cepillen de forma correcta y en todo momento se encuentre en óptimas condiciones, y en caso de tener algún desperfecto, corregirlo a la brevedad posible.<br />
							n)	Mantener limpio y óptimas condiciones los vehículos y equipos para llevar a cabo las actividades propias de la granja.<br />
							<?php break;}
				case '615': {?>
							a)	Lectura de charolas de alimentación.<br />
							b)	Realizar recambios de agua.<br />
							c)	Lectura de parámetros fisicoquímicos.<br />
							d)	Instalación de equipos y herramientas de cosecha.<br />
							e)	Realizar muestreos sanitarios y de población de los organismos en estanquería.<br />
							f)	Realizar arrastres de triangulo y telescopio en estanquería.<br />
							g)	Disposición en actividades de operarios.<br />
							<?php break;}
				case '616': {?>
							a)	Lectura de charolas de alimentación.<br />
							b)	Realizar recambios de agua.<br />
							c)	Lectura de parámetros fisicoquímicos.<br />
							d)	Instalación de equipos y herramientas de cosecha.<br />
							e)	Realizar muestreos sanitarios y de población de los organismos en estanquería.<br />
							f)	Realizar arrastres de triangulo y telescopio en estanquería.<br />
							g)	Disposición en actividades de operarios.<br />
							<?php break;}
				case '77': {?>
							a)	Realizar la supervisión de vigilancia de la granja.<br />
							b)	Uso de vehículos de la granja.<br />
							<?php break;}
				case '717': {?>
							a)	Realizar labores de vigilancia al interior y periferia de la granja.<br />
							b)	Revisión de mochilas y vehículos de todo el personal.<br />
							c)	Desinfección de vehículos que ingresen a la granja.<br />
							d)	Registro del personal y visitas que accesan a la granja.<br />
							e)	Uso de vehículos de la granja.<br />
							<?php break;}
		} ?>
		Dada la naturaleza de las labores encomendadas a <strong>“EL TRABAJADOR”</strong>, la enunciación de las obligaciones anteriores resulta no limitativa, sino meramente enunciativa.<br />
	</p>
	<p style="text-align: justify">
		SEGUNDA.- <strong>“EL TRABAJADOR”</strong> conviene en prestar los servicios en tratamiento en el domicilio precisado en la <strong>DECLARACIÓN IV</strong> de <strong>“LA EMPRESA”</strong>, o en cualesquier otro lugar, en que por la naturaleza de sus labores deba concurrir, acorde a las instrucciones escritas giradas por el segundo.
   		Para el supuesto de que <strong>“EL TRABAJADOR”</strong> cambiara de lugar de trabajo, en los términos del párrafo precedente, <strong><strong>“LA EMPRESA”</strong></strong> estará obligada a notificárselo con toda oportunidad y satisfacer los viáticos y gastos de traslado correspondientes; esto último, sólo cuando se trate de movilizaciones fuera de la localidad.<br /><br />
        TERCERA.- <strong><strong>“LA EMPRESA”</strong></strong> tendrá amplias facultades para cambiar, transitoria o permanentemente, las actividades propias de <strong>“EL TRABAJADOR”</strong>, o incluso, para moverlo de adscripción, siempre y cuando lo anterior, no sea en perjuicio de las percepciones salariales del aludido.<br /><br />
		CUARTA.- <strong><strong>“LA EMPRESA”</strong></strong> queda facultada también, para cambiar los sistemas de trabajo y la organización de las labores de <strong>“EL TRABAJADOR”</strong>, y en consecuencia, este último estará obligado a obedecer cualquier orden relacionada directa o indirectamente con los nuevos sistemas u organización impuestas.<br /><br />
		QUINTA.- <strong><strong>“LA EMPRESA”</strong></strong> se obliga a pagar a <strong>“EL TRABAJADOR”</strong>, en sus oficinas, y como salario semanal, la cantidad de $<?= $suel ?>, después de impuestos, debiendo cubrírsele los días sábados de cada semana.<br /><br />
		En el salario indicado en esta CLÁUSULA, 	queda comprendido el pago de días séptimos y los días festivos laborados se le pagarán conforme a la Ley al <strong>“EL TRABAJADOR”</strong>.<br /><br />
		SEXTA.- <strong>“EL TRABAJADOR”</strong> disfruta de <strong>BONOS DE DESPENSA EN EFECTIVO</strong> en los términos de la reglamentación establecida por <strong><strong>“LA EMPRESA”</strong></strong>, por lo que autoriza a ésta para deducir de su sueldo el importe de la aportación que resulte.<br /><br />
		SEPTIMA.- La jornada laboral en ningún caso excederá los máximos establecidos por la Ley Federal de Trabajo para las jornadas diurna, nocturna y mixta; pero <strong><strong>“LA EMPRESA”</strong></strong> queda desde ahora facultada para repartir las horas de trabajo de acuerdo a sus necesidades, considerando  en todo caso 1 una hora, por tratarse de jornada continua,  para que <strong>“EL TRABAJADOR”</strong> pueda ingerir con toda libertad sus alimentos, fuera de las instalaciones de la primera, o de 1/2 media hora, cuando lo haga en el seno de la misma. <br />
		Las jornadas, horarios y descansos pactados, quedarían como siguen:<br /> 
		<strong>JORNADA DIURNA (48 horas)</strong> <br />
		Abarca de 8.00 a 16.00 horas, con un descanso para ingerir alimentos, dentro de las instalaciones de <strong><strong>“LA EMPRESA”</strong></strong>.<br />
		<strong>JORNADA NOCTURNA (42 horas)</strong><br /> 
		Abarca de las 00.00 a las 7.00 horas, con un descanso para ingerir alimentos dentro de las instalaciones de <strong><strong>“LA EMPRESA”</strong></strong>.<br />
   		<strong>JORNADA MIXTA (45 horas)</strong><br />
		Abarca de las 16.30 horas a las 24.00, con un descanso para ingerir alimentos dentro de las instalaciones de <strong><strong>“LA EMPRESA”</strong></strong>.<br /> 
        <strong>“EL TRABAJADOR"</strong> autoriza a  <strong><strong>“LA EMPRESA”</strong></strong> para que  modifique su jornada de trabajo ya que laborará una semana de cada turno  porque la actividad productiva de la empresa así lo requiere.<br /> <br /> 
		OCTAVA.- <strong>“EL TRABAJADOR”</strong> estará autorizado para laborar horas extras, salvo que mediantee orden expresa de la patronal, justificativa de las circunstancias específicas por las que se requiera el aumento de la jornada de trabajo.
		<strong>“EL TRABAJADOR”</strong> autoriza a <strong><strong>“LA EMPRESA”</strong></strong> para que modifique su horario, en caso de que la prestación de trabajo extraordinario sea necesaria.<br /><br />
		NOVENA.- <strong>“EL TRABAJADOR”</strong> estará obligado a registrar hora de arribo y salida de la fuente de trabajo, y firmar el control de asistencia que determine <strong><strong>“LA EMPRESA”</strong></strong>. 
		La omisión de los deberes enunciados precedentemente, indicarán falta injustificada de <strong>“EL TRABAJADOR”</strong> a sus labores, para todos los efectos legales conducentes.<br /> <br />
		DÉCIMA.- En relación con la <strong>CLÁUSULA</strong> precedente, ambas partes acuerdan que la ausencia por enfermedad de <strong>“EL TRABAJADOR”</strong>, sólo se justificará con la incapacidad otorgada por el <strong>INSTITUTO MEXICANO DEL SEGURO SOCIAL</strong>, nunca por recetas de esta institución o comprobantes médicos particulares.<br /> <br />
		DÉCIMA PRIMERA.- <strong>“EL TRABAJADOR”</strong> disfrutará como descanso semanal el día domingo; sin embargo, autoriza expresamente a <strong><strong>“LA EMPRESA”</strong></strong>, para que, de ser necesario, le fije como tal prestación un día distinto al domingo, pero en tal coyuntura, la patronal pagará un 25% del salario pactado como prima dominical, lo anterior en los términos del artículo 70 de la Ley Federal del Trabajo.<br /> <br /> 
		DÉCIMA SEGUNDA.- <strong>“EL TRABAJADOR”</strong>  disfrutará de un período vacacional, por cada año natural de servicios, en estricto apego a lo establecido por el artículo 76 de la Ley Federal del Trabajo.<br /> <br />
        DÉCIMA TERCERA.- <strong><strong>“LA EMPRESA”</strong></strong> cubrirá a <strong>“EL TRABAJADOR”</strong>, la prima legal de vacaciones (25%), calculada con base al número efectivo de días, que por año le correspondan de vacaciones, conforme a la Ley Federal de Trabajo, según antigüedad al servicio del primero.<br /> <br />
		DÉCIMA CUARTA.- <strong>“EL TRABAJADOR”</strong> se obliga a someterse a los exámenes médicos que <strong><strong>“LA EMPRESA”</strong></strong> ordene, tanto al iniciar el servicio, como en cualquier otra ocasión que la patronal juzgue conveniente, así como involucrarse en el <strong>PROGRAMA DE PREVENCIÓN DE ADICCIONES</strong> establecido por el Centro de Integración Juvenil (CIJ), en la pretensión de generar una cultura de salud y seguridad en los empleados, por lo que figurará entre las obligaciones de <strong>“EL TRABAJADOR”</strong>, el acceder a la realización de pruebas clínicas (exámenes antidoping)para la detección de consumo de drogas; en el claro entendido, de que negarse a colaborar en la actualización de las mismas, será considerada causal de rescisión de la presente relación de trabajo, sin responsabilidad para la patronal, con basamento en las fracciones II y X del artículo 134 de la Ley Federal del Trabajo, y bajo el supuesto de que tal negativa implica tanto la inobservancia de las medidas preventivas indicadas para la seguridad personal de trabajadores, proveedores y clientes de <strong><strong>“LA EMPRESA”</strong></strong>, como el no sometimiento a los reconocimientos médicos previstos por las normas y políticas de la patronal.<br /> <br />
		DÉCIMA QUINTA.- <strong>“EL TRABAJADOR”</strong> se compromete a observar, en todos sus extremos, los reglamentos internos, políticas y misión de <strong><strong>“LA EMPRESA”</strong></strong>.<br /> <br />
	    DÉCIMA SEXTA.- <strong><strong>“LA EMPRESA”</strong></strong> se obliga a afiliar a <strong>“EL TRABAJADOR”</strong>, en el <strong>INSTITUTO MEXICANO DEL SEGURO SOCIAL</strong> y  en el <strong>INSTITUTO NACIONAL PARA LA VIVIENDA</strong>,  pagando por éste las cuotas correspondientes a dichos conceptos y aportaciones a la <strong>AFORE</strong>,  y ofrecerle, al mismo tiempo, la capacitación y adiestramiento establecidos y ya aprobados por las autoridades del trabajo.<br /> <br />
		DÉCIMA SEPTIMA.- <strong><strong>“LA EMPRESA”</strong></strong> se obliga para con <strong>“EL TRABAJADOR”</strong>, a  cubrirle con toda oportunidad las prestaciones laborales a que tenga derecho, acorde con las normas de trabajo aplicables y beneficios establecidos por la propia empresa.<br /> <br />
		DÉCIMA OCTAVA.- Lo no mencionado o pactado expresamente por las partes en este instrumento, será objeto regulación o interpretación al tenor de lo establecido por la Ley Federal del Trabajo.<br /> <br /> 
		En Mazatlán, Sinaloa, a <?= $f[2]." de ".$mes1[$nummes]. " de ".$f[0]?>, y a tono con lo dispuesto por los artículos 24 y 25 de la Ley Federal del Trabajo, los contratantes, conformes en todos sus extremos con el presente instrumento, lo firman por duplicado en cada una de sus hojas y al calce de la última, quedando un tanto en poder del trabajador y otro del patrón.<br /> <br /> 
		
		<table style="font-family: Verdana; font-size: 10">
			<tr>
				<th>LA EMPRESA<br /><br /><br /><br /><br /></th>
				<th style="color: white;">-----------</th>
				<th>EL TRABAJADOR<br /><br /><br /><br /><br /></th>
			</tr>
			<tr>
				<th>
				___________________________________________<br />
				JOEL LIZARRAGA VALDEZ                                  	
				</th>
				<th style="color: white;">-----------</th>
				<th>
				___________________________________________<br />
				<?= $nom?>	
				</th>
			</tr>
			<tr>
				<th><br /><br />TESTIGO	<br /><br /><br /><br /><br /></th>
				<th style="color: white;">-----------</th>
				<th><br /><br />TESTIGO<br /><br /><br /><br /><br /></th>
			</tr>
			<tr>
				<th>
				___________________________________________<br />
				<?php if($depa=='PRODUCCIÓN'){ ?>  JOSE ANTONIO VALDEZ VALDEZ    <?php }else{ ?>   EZEQUIEL RODRIGUEZ GALINDO   <?php } ?>                      	
				</th>
				<th style="color: white;">-----------</th>
				<th>
				___________________________________________<br />
				TRINIDAD LEON CASTRO
				</th>
			</tr>
		</table>
		
	</p>
</div>
<script type="text/php">
	
if(isset($pdf)){
	    $font = Font_Metrics::get_font("verdana");
        $size = 10;
        $color = array(1,1,1);
        $text_height = Font_Metrics::get_font_height($font, $size);
        
        
        $foot = $pdf->open_object();
        
        $w = $pdf->get_width();
        $h = $pdf->get_height()-10;
        $y = $h - 3 * $text_height - 24;
                
        
        $size = 7;
                
        $text = "Pag. {PAGE_NUM}/{PAGE_COUNT} ";
        $width = Font_Metrics::get_text_width($text, $font, $size);
        $pdf->page_text(18, $y+55, $text, $font, $size, $color);
        
        $pdf->close_object();
        $pdf->add_object($foot, "all");
}
</script>
</body>
</html>