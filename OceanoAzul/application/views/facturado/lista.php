<?php $this->load->view('header_cv');?>
<script src="<?php echo base_url();?>assets/Highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/Highcharts/js/modules/exporting.js"></script>

<script>
	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>
<style>        
/*
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}*/
/*ul.tabs li {*/
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	/*font-size: 25px;*/
	/*background: none;*/		
/*}*/   

</style>

<div style="height:500px">
<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li style="font-size: 20px"><a href="#entregas"><img src="<?php echo base_url();?>assets/images/menu/entregado.png" width="25" height="25" border="0"> Facturado </a></li>			
		<li style="font-size: 20px"><a href="#grafica"><img src="<?php echo base_url();?>assets/images/grafica.png" width="25" height="25" border="0"> Gráfica </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="entregas" class="tab_content" >
			<div id = "accordion" style="margin-top: -10px; margin-left: -10px; width: 958px;" >
					<h3 align="left" style="font-size: 12px" ><a href="#">  Entregas Actuales </a></h3>
					<div  style="height:348px; text-align: right; font-size: 12px;">				
						<div class="ajaxSorterDiv" id="tabla_fac" name="tabla_fac" style="height: 408px;margin-top: -21px; ">                
                		<div style=" text-align:left; font-size: 12px;" class='filter'  >
                		<div align="left">	Ciclo:                 
                    <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 25px; width: 120px;" >
                    	<option value="0">Seleccione Ciclo</option>
                    	<?php $ciclof=2008;  //$actual=date("Y"); //$actual+=1;
                    		$actual=date("Y"); 
							if ($actual<2016) $actual+=1;  //cuando se entregan en diciembre se suma uno, 2015 se cambia en diciembre a 2016
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            				<?php  $actual-=1; } ?>
          			</select>
          			
                	Seleccione Zona:
                	<select name="cmbZonas" id="cmbZonas" style="font-size: 10px; height: 25px; width: 150px; ">
                		<!--<option value='Todos'>Todos</option>-->
                	</select>
                    <!--<select id="cmbZona" style="font-size: 10px; height: 25px; " >	
                    			<option value='Todos'>Todos</option>							
    							<?php foreach ($result as $row):  ?>	
    							<option value="<?php echo $row->Zona; ?>"><?php echo $row->Zona;?></option>		
						        <?php endforeach ;?>
							</select> -->
							</div>
                		</div>
                		<span class="ajaxTable" style="height: 318px; background-color: white; width: 870px" >
                    	<table id="mytablaR" name="mytablaR" class="ajaxSorter" >
                        	<thead title="Presione las columnas para ordenarlas como requiera">                            
                            	<th data-order = "Zona" style="width: 80px" >Zona</th>
                            	<th data-order = "Razon">Razón Social</th>                                                        
                            	<th data-order = "CantidadRR" style="width: 80px;">Cantidad</th>                                                	
                            	<th data-order = "est" style="width: 80px;">Estanques</br>Sembrados</th>
                        	</thead>
                        	<tbody title="Seleccione para analizar cliente" style="font-size: 11px">
                        	</tbody>                        	
                    	</table>
                		</span> 
             			<div class="ajaxpager" style="">        
	                    	<ul class="order_list"></ul>     
                   			 <form method="post" action="<?= base_url()?>index.php/facturado/pdfrep" >  
                    			<input readonly="true" type="hidden"  name="zonasel" id="zonasel" >
                    			<input type="hidden" size="2%" name="ciclosel2" id="ciclosel2"/>                    			                       
                        		<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />                                                                      
                        		<!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        		<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                             
 							</form>   
                		</div>                                      
            			</div>             	
            		
            	 	</div> 
        			<h3 align="left" style="font-size: 12px"><a href="#">Analizar Cliente</a></h3>
		       	 	<div style="height: 150px;">
		       	 	<div id='ver_mas_det' class='hide' style="height: 238px; margin-top: 4px" align="" >	
		       			 <div class="ajaxSorterDiv" id="tabla_ent" name="tabla_ent" style="height: 400px;margin-top: -16px;">                
                			<div style="text-align:left" class='filter' >                	 
                				<input type="hidden"   size="8%" name="numcli" id="numcli"/>									                	
								<th colspan="6" style="font-size: 14px;"><input style="border:0; background-color: lightgray" readonly="true" size="150%"  type="text" name="nombre" id="nombre" ></th>		                 	                
                			</div>
                			<span class="ajaxTable" style="height: 330px; background-color: white; width: 870px">
                    			<table id="mytablaEnt" name="mytablaEnt" class="ajaxSorter"  >
                        			<thead title="Presione las columnas para ordenarlas como requiera">                            
                            			<th data-order = "FechaR1" style="width: 50px;">Fecha</th>
                            			<th data-order = "RemisionR" style="width: 50px;">Remisión</th>                                                        
                            			<th data-order = "CantidadRR" style="width: 50px;">Cantidad</th>                                                        
                            			<th data-order = "PrecioR" style="width: 50px;">Precio</th>
                            			<th data-order = "Importe" style="width: 50px;">Importe</th>
                            			<th data-order = "Folio" style="width: 50px;">Folio</th>
                        			</thead>
                        			<tbody title="Seleccione para analizar" style="font-size: 11px">
                        			</tbody>
                    			</table>
                			</span> 
             				<div class="ajaxpager" style="margin-top: -15px">        
                    			<ul class="order_list"><input style="font-size: 14px" type="submit" id="granja" name="granja" value="Distribucion en Granja" /></ul>     
                    			<form method="post" action="<?= base_url()?>index.php/facturado/pdfrepdetalle" >    
                    				<input readonly="true" type="hidden" name="cliente" id="cliente" >                	             	                                                
                        			<input type="hidden" size="2%" name="ciclosel1" id="ciclosel1"/>                                             
                        			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        			<!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        			<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 								</form>    					
                			</div>                                      
            			</div>      
        			</div>
        			<div id='ver_mas_granja' class='hide' style="height: 150px; " >	
        				 <div class="ajaxSorterDiv" id="tabla_granja" name="tabla_granja" style="height: 400px; width: 920px; margin-left: -20px;margin-top: -25px;">                
                			<div style="text-align:left" class='filter'  >                	 
                				<input style="border:0; background-color: lightgray" readonly="true" size="100%"  type="text" name="nombre2" id="nombre2" >
                				<select  name="cmbCli" id="cmbCli" style="font-size: 10px; height: 25px; width: 10px; visibility: hidden  " >
          			           		<?php	
          							$data['result']=$this->facturado_model->clientes(); ?>
									<?php foreach ($data['result'] as $row):?>
           								<option value="<?php echo $row->Numero;?>" ><?php echo $row->Razon;?></option>
           							<?php endforeach;?>
                				</select>
          			           	Seccion:
                				<select name="cmbSeccion" id="cmbSeccion" style="font-size: 10px; height: 25px; width: 150px; ">
                				</select>		                 	                
                			</div>
                			<span class="ajaxTable" style="background-color: white; height: 335px; margin-top: 1px; " >
                    			<table id="mytablaGranja" name="mytablaGranja" class="ajaxSorter" border="1"  style="width: 1600px;"  >
                        			<thead >                            
                            			<tr>
                            				<th rowspan="2" colspan="7" style="text-align: center;">Datos de Siembra</th>
                            				<th colspan="17" style="text-align: center;">Datos de Cosecha</th>
                            				<th rowspan="2" colspan="5" style="text-align: center" >Total Final</th>
                            			</tr>
                            			<tr height="12px">
                            				<th colspan="5" style="text-align: center;font-size: 10px">Cultivo</th>
                            				<th colspan="2" style="text-align: center;font-size: 10px">Alimento<br>Acum. (kg)</th>
                            				<th style="text-align: center;font-size: 10px">Inicial</th>
                            				<th colspan="2" style="text-align: center;font-size: 10px">F-10 grs</th>
                            				<th colspan="3" style="text-align: center;font-size: 10px">Precosechas</th>
                            				<th colspan="3" style="text-align: center;font-size: 10px">Cosechas</th>
                            				<th style="text-align: center;font-size: 10px">Organismos</th>
                            				
                            			</tr>
                            			<tr>
                            				<th data-order = "seccion1" style="text-align: center;font-size: 10px">Secc.</th>
                            				<th data-order = "ciclo1" style="text-align: center;font-size: 10px">Ciclo<br>No.</th>
                            				<th data-order = "lab1" style="text-align: center;font-size: 10px">Origen<br>Larva</th>
                            				<th data-order = "est1" style="font-size: 10px">Est.<br>No.</th>                                                        
                            				<th data-order = "sup1" style="font-size: 10px">Sup.<br>(ha)</th>
                            				<th data-order = "pls1" style="font-size: 10px">Postlarvas</th>
                            				<th data-order = "fs1" style="font-size: 10px">Fecha</th>
                            				<th data-order = "fc1" style="font-size: 10px">Fecha</th>
                            				<th data-order = "dc1" style="font-size: 10px">Días</th>
                            				<th data-order = "sc1" style="font-size: 10px">Sem.</th>
                            				<th data-order = "pp1" style="font-size: 10px">Peso<br>Prom.<br>Final</th>
                            				<th data-order = "isp1" style="font-size: 10px">ISP</th>
                            				<th data-order = "aha1" style="font-size: 10px">ha.</th>
                            				<th data-order = "at1" style="font-size: 10px">Total</th>
                            				<th data-order = "cami1" style="font-size: 10px">Cam/m2</th>
                            				<th data-order = "camf1" style="font-size: 10px">cam/m2</th>
                            				<th data-order = "sob1" style="font-size: 10px">% Sob</th>
                            				<th data-order = "biop1" style="font-size: 10px">Biom</th>
                            				<th data-order = "pha1" style="font-size: 10px">Ha</th>
                            				<th data-order = "camp1" style="font-size: 10px">cam/m2</th>
                            				<th data-order = "bioc1" style="font-size: 10px">Biom</th>
                            				<th data-order = "cha1" style="font-size: 10px">Ha</th>
                            				<th data-order = "camc1" style="font-size: 10px">cam/m2</th>
                            				<th data-order = "otc1" style="font-size: 10px">Totales<br>Cosechados</th>
                            				<th data-order = "sobfin1" style="font-size: 10px" >% Sob</th>
                            				<th data-order = "camfin1" style="font-size: 10px" >cam/m2</th>
                            				<th data-order = "ren1" style="font-size: 10px">Rend.<br>kg/ha</th>
                            				<th data-order = "biot1" style="font-size: 10px">Biom.<br>Cos.</th>
                            				<th data-order = "fca1" style="font-size: 10px">FCA<br>Ciclo</th>
                            			</tr>
                        			</thead>
                        			<tbody title="Seleccione para analizar" style="font-size: 11px">
                        			</tbody>
                    			</table>
                			</span> 
             				<div class="ajaxpager" style="margin-top: -15px">        
                    			<ul class="order_list">
                    				<input style="font-size: 14px" type="submit" id="nuevog" name="nuevog" value="Nuevo" />
                    				<input style="font-size: 14px" type="submit" id="x1" name="x1" value="Cerrar" onclick="cerrar(1)" />
                    			</ul>     
                    			<form method="post" action="<?= base_url()?>index.php/facturado/pdfrepdetalle" >    
                    				<input readonly="true" type="hidden" name="cliente" id="cliente" >                	             	                                                
                        			<input type="hidden" size="2%" name="ciclosel1" id="ciclosel1"/>                                             
                        			<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        			<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 								</form>    					
                			</div>  
                			                                    
            			</div>  
            			<div id='ver_mas_nuevo' class='hide' style="margin-top: -30px ">
                				<table style="width: 680px;" border="2px">
									<thead style="background-color: #DBE5F1">
										<th colspan="7" height="15px" style="font-size: 14px">Registro de Datos de Siembra</th>																	
									</thead>
									<tbody style="background-color: #F7F7F7">
									<tr>
										<th style="font-size: 12px; text-align: center">Seccion</th>
										<th style="font-size: 12px; text-align: center">Fecha</th>
										<th style="font-size: 12px; text-align: center">Origen<br>Larva</th>	
										<th style="font-size: 12px; text-align: center">Ciclo<br>No.</th>
										<th style="font-size: 12px; text-align: center">Estanque<br>No.</th>
										<th style="font-size: 12px; text-align: center">Superficie<br>(ha)</th>
										<th style="font-size: 12px; text-align: center">Postlarvas</th>
									</tr>
    								<tr style="font-size: 12px; background-color: white;">
										<th style="font-size: 12px; text-align: center"><input size="20%" type="text" name="sec" id="sec"  value="1" ></th>
										<th style="font-size: 12px; text-align: center"><input size="13%" type="text" name="txtFs" id="txtFs" class="fecha redondo mUp" style="text-align: center;"  readonly="true" ></th>																			
										<th style="font-size: 12px; text-align: center"><input size="20%" type="text" name="ori" id="ori" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="cic" id="cic" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="est" id="est" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="sup" id="sup" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="13%" type="text" name="pls" id="pls" style="text-align: right"></th>
									</tr>
									</tbody>
    								<tfoot style="background-color: #DBE5F1">
    									<th colspan="7" style="text-align: right">
											<input style="font-size: 14px" type="submit" id="aceptarg" name="aceptarg" value="Guardar" />											
											<input style="font-size: 14px" type="submit" id="x2" name="x2" value="Cerrar" onclick="cerrar(2)" />
										</th>	
									</tfoot>
								</table>
                		</div>   
                		<div id='ver_mas_actualizar' class='hide' style="margin-top: -30px ">
                				<table style="width: 640px;" border="2px" >
									<thead style="background-color: #DBE5F1">
										<th colspan="7" height="15px" style="font-size: 14px">Registro de Datos de Siembra
											<input size="8%" type="hidden" name="idg" id="idg"/>																								
										</th>																	
									</thead>
									<tbody style="background-color: #F7F7F7">
									<tr>
										<th style="font-size: 12px; text-align: center">Seccion</th>
										<th style="font-size: 12px; text-align: center">Fecha</th>
										<th style="font-size: 12px; text-align: center">Origen<br>Larva</th>	
										<th style="font-size: 12px; text-align: center">Ciclo<br>No.</th>
										<th style="font-size: 12px; text-align: center">Estanque<br>No.</th>
										<th style="font-size: 12px; text-align: center">Superficie<br>(ha)</th>
										<th style="font-size: 12px; text-align: center">Postlarvas</th>
									</tr>
    								<tr style="font-size: 12px; background-color: white;">
										<th style="font-size: 12px; text-align: center"><input size="20%" type="text" name="sec1" id="sec1"  value="1" ></th>
										<th style="font-size: 12px; text-align: center"><input size="13%" type="text" name="txtFs1" id="txtFs1" class="fecha redondo mUp" style="text-align: center;"  readonly="true" ></th>																			
										<th style="font-size: 12px; text-align: center"><input size="20%" type="text" name="ori1" id="ori1" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="cic1" id="cic1" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="est1" id="est1" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="sup1" id="sup1" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="13%" type="text" name="pls1" id="pls1" style="text-align: right"></th>
									</tr>
									</tbody>
								</table>
								<table style="width: 640px; margin-top: -17px" border="2px">
									<thead style="background-color: #DBE5F1">
									
     									<th colspan="9" height="15px" style="font-size:14px"><div align="center">Registro de Datos de Cosecha</div>	 </th>
  									
  									</thead>
  									<tbody style="background-color: #F7F7F7">
  									<tr >
     									<th rowspan="2" style="font-size: 12px; text-align: center">Fecha</th>
  	 									<th rowspan="2" style="font-size: 12px; text-align: center">Peso Prom.</th> 
	 									<th rowspan="2" style="font-size: 12px; text-align: center">Alim. Acum. <br /> Total</th>     
	 									<th rowspan="1" colspan="2" style="font-size: 12px; text-align: center">F-10 grs.</th>     
	 									<th rowspan="1" colspan="2" style="font-size: 12px; text-align: center">Precosechas</th>     
	 									<th rowspan="1" colspan="2" style="font-size: 12px; text-align: center">Cosecha</th> 
   									</tr>
   									<tr >
  										<th style="font-size: 12px; text-align: center">cam/m2</th>     
										<th style="font-size: 12px; text-align: center"> % sob</th> 
										<th style="font-size: 12px; text-align: center"> biom.</th>
										<th style="font-size: 12px; text-align: center">cam/m2</th>
										<th style="font-size: 12px; text-align: center"> biom.</th>
										<th style="font-size: 12px; text-align: center">cam/m2</th>        
   									</tr>
   									<tr style="font-size: 12px; background-color: white;">
										<th style="font-size: 12px; text-align: center"><input size="13%" type="text" name="txtFc" id="txtFc" class="fecha redondo mUp" style="text-align: center;"  readonly="true" ></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="pp" id="pp"  style="text-align: center"></th>																			
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="at" id="at" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="camf" id="camf" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="sob" id="sob" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="biop" id="biop" style="text-align: center"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="camp" id="camp" style="text-align: right"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="bioc" id="bioc" style="text-align: right"></th>
										<th style="font-size: 12px; text-align: center"><input size="8%" type="text" name="camc" id="camc" style="text-align: right"></th>
									</tr>
									</tbody>
    								<tfoot style="background-color: #DBE5F1">
    									<th colspan="9" style="text-align: right">
											<input style="font-size: 14px" type="submit" id="aceptarga" name="aceptarga" value="Guardar" />
											<input style="font-size: 14px" type="submit" id="borrarg" name="borrarg" value="Borrar" />
											<input style="font-size: 14px" type="submit" id="x2" name="x2" value="Cerrar" onclick="cerrar(3)" />
										</th>	
									</tfoot>
								</table>
                		</div>
        			</div>
        			 
	 			</div>	
	 	</div>
	 	</div>
		<div id="grafica" class="tab_content" >
			 <table style="margin-top: -10px; margin-left: -5px"  > 
                <th style="width: 700px">	
               		<div style="text-align:left; font-size: 16px;" >           
						CICLO [<input style="border:0; font-size: 16px; text-align: center; color: blue" size="2%" name="ciclosel" id="ciclosel"/>] RESULTADOS GENERALES POR ZONA
                	</div>
					<div id="containere" style="min-width: 690px; height: 437px; margin: 0 auto" ></div>
					<input type="hidden" id="a1"/><input type="hidden" id="a2"/><input type="hidden" id="a3"/><input type="hidden" id="a4"/><input type="hidden" id="a5"/>
					<input type="hidden" id="a6"/><input type="hidden" id="a7"/><input type="hidden" id="a8"/><input type="hidden" id="a9"/><input type="hidden" id="a10"/>
					<input type="hidden" id="a11"/><input type="hidden" id="a12"/><input type="hidden" id="a13"/><input type="hidden" id="a14"/><input type="hidden" id="a15"/>
					<input type="hidden" id="a16"/>
					<input type="hidden" id="z1"/><input type="hidden" id="z2"/><input type="hidden" id="z3"/><input type="hidden" id="z4"/><input type="hidden" id="z5"/>
					<input type="hidden" id="z6"/><input type="hidden" id="z7"/><input type="hidden" id="z8"/><input type="hidden" id="z9"/><input type="hidden" id="z10"/>
					<input type="hidden" id="z11"/><input type="hidden" id="z12"/><input type="hidden" id="z13"/><input type="hidden" id="z14"/><input type="hidden" id="z15"/>
					<input type="hidden" id="z16"/>
                </th>
                <th >				
					<div class="ajaxSorterDiv" id="tabla_zon" name="tabla_zon" style="width: 250px;"    >                
                	 	<span class="ajaxTable" style="width: 240px;height: 430px; background-color: white;">
                    		<table id="mytablaZon" name="mytablaZon" class="ajaxSorter" >
                        		<thead title="Presione las columnas para ordenarlas como requiera">                            
                            		<th data-order = "Zona" >Zona</th>
                            		<th data-order = "CantidadRR" style="width:50px; text-align: center" >Cantidad</th>                                                                                                                
                            		<th data-order = "por" style="width:40px; text-align: center" >%</th>
                        		</thead>
                        		<tbody style="font-size: 11px">
                        		</tbody>
                    		</table>
                		</span> 
             			<div class="ajaxpager" style="margin-top: -15px">        
                    		<form method="post" action="<?= base_url()?>index.php/facturado/pdfrepdetalle" >  
                    	    	<img style="cursor: pointer" title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        		<input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 							</form>    					
                		</div>  
            		</div>    	
                </th> 
            </table>                                   
        </div>	
 	</div> 	
</div>
</div>
<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">
$("#txtFs").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#ori").focus();
	}
});
$("#txtFs1").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFc").datepicker( "option", "minDate", selectedDate );
		$("#ori1").focus();
	}			
});
$("#txtFc").datepicker({
	dateFormat:"yy-mm-dd",changeMonth:true,changeYear:true,yearRange:"2006:+0",
	onSelect: function( selectedDate ){
		$("#txtFs1").datepicker( "option", "maxDate", selectedDate );
		$("#pp").focus();
	}	
});	
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});

$("#granja").click( function(){
	$(this).removeClass('used');
	$('#ver_mas_det').hide();	
	$('#ver_mas_nuevo').hide();
	$('#ver_mas_granja').show();
 return true;
})
$("#nuevog").click( function(){
	$(this).removeClass('used');
	$('#ver_mas_actualizar').hide();	
	$('#ver_mas_nuevo').show();
	//$('#ver_mas_granja').show();
	$("#idg").val('');
	$("#sec").focus();
 return true;
})

function cerrar(sel){
	$(this).removeClass('used');
	if(sel==1){$('#ver_mas_det').show();	$('#ver_mas_granja').hide();$('#ver_mas_nuevo').hide();$('#ver_mas_actualizar').hide();}
	if(sel==2){$('#ver_mas_nuevo').hide();}
	if(sel==3){$('#ver_mas_actualizar').hide();}
}

/*$("#x").click( function($sel){
	$(this).removeClass('used');
	if($sel==1){$('#ver_mas_det').show();	$('#ver_mas_granja').hide();}
	if($sel==2){$('#ver_mas_nuevo').hide();}
 return true;
})*/
$("#cmbZonas").change( function(){		
	$("#zonasel").val($("#cmbZonas").val());	
 return true;
});
$("#cmbCiclo").change( function(){		
	$("#ciclosel").val($("#cmbCiclo").val());	
	$("#ciclosel1").val($("#cmbCiclo").val());
	$("#ciclosel2").val($("#cmbCiclo").val());
	$("#cmbZonas").val(0);	
 return true;
});

$("#borrarg").click( function(){	
	numero=$("#idg").val();
	if(numero!=''){
				$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/facturado/borrarg", 
						data: "id="+$("#idg").val(),
						success: 	
								function(msg){															
									if(msg!=0){														
										alert("Estanque Eliminado");
										//$("#nuevog").click();
										$(this).removeClass('used');
										$('#ver_mas_actualizar').hide();
										$("#mytablaGranja").trigger("update");
									}else{
										alert("Error con la base de datos o usted no ha actualizado nada");
									}					
								}		
				});
		}else{
		alert("Error: Necesita seleccionar Estanque para poder Eliminarlo");
		return false;
	}
});
$("#aceptarg").click( function(){		
	est=$("#est").val();
	numero=$("#idg").val();
	if( est!=''){
	  	$.ajax({
				type: "POST",//Envio
				url: "<?=base_url()?>index.php/facturado/agregarg", 
				data: "sec="+$("#sec").val()+"&fs="+$("#txtFs").val()+"&ori="+$("#ori").val()+"&cic="+$("#cic").val()+"&est="+$("#est").val()+"&sup="+$("#sup").val()+"&pls="+$("#pls").val()+"&cli="+$("#numcli").val(),
				success: 
						function(msg){															
							if(msg!=0){														
								alert("Registro de Siembra correcto");
								//$("#nuevog").click();
								$(this).removeClass('used');
								$('#ver_mas_nuevo').hide();
								$("#mytablaGranja").trigger("update");
							}else{
								alert("Error con la base de datos o usted no ha ingresado nada");
							}					
						}		
		});
		
	}else{
		alert("Error: Falta Registrar Estanque");	
		$("#est").focus();
		return false;
	}
});
$("#aceptarga").click( function(){		
	est=$("#est1").val(); fs=$("#txtFs1").val();fc=$("#txtFc").val();sup=$("#sup1").val();pls=$("#pls1").val();
	numero=$("#idg").val();
	if(fc==''){
		if( est!=''){
	 			$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/facturado/actualizarg", 
						data: "id="+$("#idg").val()+"&sec="+$("#sec1").val()+"&fs="+$("#txtFs1").val()+"&ori="+$("#ori1").val()+"&cic="+$("#cic1").val()+"&est="+$("#est1").val()+"&sup="+$("#sup1").val()+"&pls="+$("#pls1").val()+"&cli="+$("#numcli").val()+"&fc="+$("#txtFc").val()+"&pp="+$("#pp").val()+"&at="+$("#at").val()+"&camf="+$("#camf").val()+"&sob="+$("#sob").val()+"&biop="+$("#biop").val()+"&camp="+$("#camp").val()+"&bioc="+$("#bioc").val()+"&camc="+$("#camc").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										if(msg==1){													
											alert("Datos actualizados correctamente");
											//$("#nuevog").click();
											$(this).removeClass('used');
											$('#ver_mas_actualizar').hide();
											$("#mytablaGranja").trigger("update");
										}else{
											// es tres
											alert("Error: No puede registrar fecha de cosecha sin antes registrar fecha de siembra");	
											$("#txtFs1").focus();
											$("#txtFc").val('');
										}
									}else{
										//alert("Error con la base de datos o usted no ha actualizado nada1");
										$(this).removeClass('used');
										$('#ver_mas_actualizar').hide();
									}					
								}		
				});
			
		}else{
			alert("Error: Falta Registrar Estanque");	
			$("#est1").focus();
			return false;
		}
	}else{
		if(fs!=''){
			if(sup!=''){
				//if(pls!=0){
					$.ajax({
						type: "POST",//Envio
						url: "<?=base_url()?>index.php/facturado/actualizarg", 
						data: "id="+$("#idg").val()+"&sec="+$("#sec1").val()+"&fs="+$("#txtFs1").val()+"&ori="+$("#ori1").val()+"&cic="+$("#cic1").val()+"&est="+$("#est1").val()+"&sup="+$("#sup1").val()+"&pls="+$("#pls1").val()+"&cli="+$("#numcli").val()+"&fc="+$("#txtFc").val()+"&pp="+$("#pp").val()+"&at="+$("#at").val()+"&camf="+$("#camf").val()+"&sob="+$("#sob").val()+"&biop="+$("#biop").val()+"&camp="+$("#camp").val()+"&bioc="+$("#bioc").val()+"&camc="+$("#camc").val(),
						success: 	
								function(msg){															
									if(msg!=0){	
										if(msg==1){													
											alert("Datos actualizados correctamente");
											//$("#nuevog").click();
											$(this).removeClass('used');
											$('#ver_mas_actualizar').hide();
											$("#mytablaGranja").trigger("update");
										}else{
											// es tres
											alert("Error: No puede registrar fecha de cosecha sin antes registrar fecha de siembra2");	
											$("#txtFs1").focus();
											$("#txtFc").val('');
										}
									}else{
										//alert("Error con la base de datos o usted no ha actualizado nada2");
										$(this).removeClass('used');
										$('#ver_mas_actualizar').hide();
									}					
								}		
				});
					
				/*}else{
					alert("Error: Registre Cantidad de Postlarvas");
					$("#pls1").focus();	
					return false;
				}*/
			}else{
				alert("Error: Registre Superficie de Estanque");
				$("#sup1").focus();	
				return false;
			}
		}else{
			alert("Error: Registre Fecha de Siembra");
			$("#txtFs1").focus();	
			return false;
		}
		
	}
	
});

$(document).ready(function(){ 
	$("#cmbZonas").boxLoader({
            url:'<?php echo base_url()?>index.php/facturado/combo',
            equal:{id:'actual',value:'#cmbCiclo'},
            select:{id:'zona',val:'val'},
            all:"Todos",
            //onLoad:false            
    });	
    
    $(this).removeClass('used');
	$('#ver_mas_granja').hide();
	$('#ver_mas_nuevo').hide();
	$('#ver_mas_actualizar').hide();
	$("#ciclosel").val($("#cmbCiclo").val());
	$("#ciclosel1").val($("#cmbCiclo").val());	     
	$("#ciclosel2").val($("#cmbCiclo").val());
	$("#tabla_fac").ajaxSorter({
	url:'<?php echo base_url()?>index.php/facturado/tabla',  	
	//filters:['cmbZona','cmbCiclo'],	
	filters:['cmbCiclo','cmbZonas'],
	onLoad:false,
	onRowClick:function(){
		if($(this).data('numero')>'0'){
    		$("#accordion").accordion( "activate",1 );
    		$(this).removeClass('used');
			$('#ver_mas_det').show(); 
			$('#ver_mas_granja').hide();
			$("#nombre").val($(this).data('razon'));
			$("#nombre2").val($(this).data('razon'));
       		$("#cliente").val($(this).data('razon'));
       		$("#zona").val($(this).data('zona'));
			$("#numcli").val($(this).data('numero'));
			$("#cmbCli").val($(this).data('numero'));
			$("#cmbSeccion").boxLoader({
        	    url:'<?php echo base_url()?>index.php/facturado/comboSecc',
	            equal:{id:'actual',value:'#cmbCli'},
    	        select:{id:'seccion',val:'val'},
        	    all:"Todos",
	            //onLoad:false            
		    });
			$("#ciclosel").val($("#cmbCiclo").val());
			$("#ciclosel1").val($("#cmbCiclo").val());					
			$("#ciclosel2").val($("#cmbCiclo").val());
			$("#tabla_ent").ajaxSorter({
				url:'<?php echo base_url()?>index.php/facturado/tablaentregado/'+$("#numcli").val(),
				//data:"numcli="+$("#numcli").val(),//+"&ciclo="+$("#ciclosel").val(),//+$("#ciclosel").val(), 
				filters:['cmbCiclo'],
				//onLoad:false,
				onSuccess:function(){
					//total = 0;totalimp = 0;
    		   		$('#tabla_ent tbody tr').map(function(){
	    				if($(this).data('estatus')=='1'){
	    					$(this).css('background','blue');	    				    				    			    			
	    				}
	    				if($(this).data('fechar1')=='Total:'){
	    					$(this).css('background','lightblue');	    				    				    			    			
	    				}
	    				$(this).css('text-align','center');	
	    				
	    				//total+=($(this).data('tot')>=0?parseFloat($(this).data('tot')):0);
	    				//totalimp+=($(this).data('totimp')>=0?parseFloat($(this).data('totimp')):0);
	    			});
	    			$('#tabla_ent tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
	    			$('#tabla_ent tbody tr td:nth-child(5)').map(function(){ $(this).css('text-align','right'); })
	    			//tr='<tr><td colspan=2>Total</td><td>'+(parseFloat(total)).toFixed(3)+'</td><td></td><td>$ '+(parseFloat(totalimp)).toFixed(2)+'</td><td></td></tr>';
    				//$('#tabla_ent tbody').append(tr);
    			},  
			});
			
			$("#tabla_granja").ajaxSorter({
				url:'<?php echo base_url()?>index.php/facturado/tablagranja',
				filters:['cmbCli','cmbSeccion'],
				sort:false,  
				onRowClick:function(){
					$(this).removeClass('used');
					if($(this).data('est1')!=''){
						$('#ver_mas_actualizar').show(); 
						$('#ver_mas_nuevo').hide();
						$("#sec1").focus();
						$("#idg").val($(this).data('reg'));
						$("#sec1").val($(this).data('seccion'));
						$("#ori1").val($(this).data('lab'));
						$("#cic1").val($(this).data('ciclo'));
						$("#sup1").val($(this).data('sup1'));
						$("#est1").val($(this).data('est'));
						$("#txtFs1").val($(this).data('fs'));
						$("#pls1").val($(this).data('pls1'));
						$("#txtFc").val($(this).data('fc'));
						$("#pp").val($(this).data('pp1'));
						$("#at").val($(this).data('at1'));
						$("#camf").val($(this).data('camf1'));
						$("#sob").val($(this).data('sob1'));
						$("#biop").val($(this).data('biop1'));
						$("#camp").val($(this).data('camp1'));
						$("#bioc").val($(this).data('bioc1'));
						$("#camc").val($(this).data('camc1'));
					}else{
						$('#ver_mas_actualizar').hide(); 
					}
				},	
				onSuccess:function(){
					$('#tabla_granja tbody tr').map(function(){
	    				if(($(this).data('lab1')=='Promedio')||($(this).data('lab1')=='Total')||($(this).data('pinta')=='1')){
	    					$(this).css('background','lightblue');	    				    				    			    			
	    				}
	    				/*if(($(this).data('pinta')=='1')){
	    					$(this).css('background','lightyellow');	    				    				    			    			
	    				}*/
	    			});
	    			$('#tabla_granja tbody tr td:nth-child(6)').map(function(){ $(this).css('text-align','right'); })
	    		}, 
			});
		}
    },
    onSuccess:function(){    
    	total = 0;    	
	    	$('#tabla_fac tbody tr').map(function(){	    		
	    		if($(this).data('zona')=='Total:'){
	    					$(this).css('background','lightblue');	    				    				    			    			
	    		}    	
	    	}); 
	    	$('#tabla_fac tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })   	
	    	$('#tabla_fac tbody tr td:nth-child(4)').map(function(){ $(this).css('text-align','center'); })
    	//tr='<tr><td colspan=2>Total</td><td>'+(parseFloat(total)).toFixed(3)+'</td><td></td></tr>';
    	//$('#tabla_fac tbody').append(tr);
    	
    },   
	});
	 $("#tabla_zon").ajaxSorter({
		url:'<?php echo base_url()?>index.php/facturado/tablazon',
		filters:['cmbCiclo'],
		sort:false,
		onSuccess:function(){   
			cont=1; 
			$("#a1").val('');$("#a2").val('');$("#a3").val(''); a4=$("#a4").val('');$("#a5").val('');$("#a6").val('');$("#a7").val('');$("#a8").val('');
    		$("#a9").val('');$("#a10").val('');$("#a11").val(''); $("#a12").val('');$("#a13").val('');$("#a14").val(''); 
    		$("#a15").val('');$("#a16").val('');
    		$("#z1").val('');$("#z2").val('');$("#z3").val('');$("#z4").val('');$("#z5").val('');$("#z6").val('');$("#z7").val('');$("#z8").val('');
    		$("#z9").val('');$("#z10").val('');$("#z11").val('');$("#z12").val('');$("#z13").val('');$("#z14").val('');$("#z15").val('');$("#z16").val('');
    		$('#tabla_zon tbody tr').map(function(){
			if($(this).data('zona')!="Total: "){	
				if(cont==1){ $("#a1").val($(this).data('cantidadrrg'));$("#z1").val($(this).data('zona'));}
				if(cont==2){ $("#a2").val($(this).data('cantidadrrg'));$("#z2").val($(this).data('zona'));}
				if(cont==3){ $("#a3").val($(this).data('cantidadrrg'));$("#z3").val($(this).data('zona'));}
				if(cont==4){ $("#a4").val($(this).data('cantidadrrg'));$("#z4").val($(this).data('zona'));}
				if(cont==5){ $("#a5").val($(this).data('cantidadrrg'));$("#z5").val($(this).data('zona'));}
				if(cont==6){ $("#a6").val($(this).data('cantidadrrg'));$("#z6").val($(this).data('zona'));}
				if(cont==7){ $("#a7").val($(this).data('cantidadrrg'));$("#z7").val($(this).data('zona'));}
				if(cont==8){ $("#a8").val($(this).data('cantidadrrg'));$("#z8").val($(this).data('zona'));}
				if(cont==9){ $("#a9").val($(this).data('cantidadrrg'));$("#z9").val($(this).data('zona'));}
				if(cont==10){ $("#a10").val($(this).data('cantidadrrg'));$("#z10").val($(this).data('zona'));}
				if(cont==11){ $("#a11").val($(this).data('cantidadrrg'));$("#z11").val($(this).data('zona'));}
				if(cont==12){ $("#a12").val($(this).data('cantidadrrg'));$("#z12").val($(this).data('zona'));}
				if(cont==13){ $("#a13").val($(this).data('cantidadrrg'));$("#z13").val($(this).data('zona'));}
				if(cont==14){ $("#a14").val($(this).data('cantidadrrg'));$("#z14").val($(this).data('zona'));}
				if(cont==15){ $("#a15").val($(this).data('cantidadrrg'));$("#z15").val($(this).data('zona'));}
				if(cont==16){ $("#a16").val($(this).data('cantidadrrg'));$("#z16").val($(this).data('zona'));}
				cont+=1; 
			}	
			var chart;
  			a1=$("#a1").val(); a1=parseFloat(a1);a2=$("#a2").val(); a2=parseFloat(a2);a3=$("#a3").val(); a3=parseFloat(a3);a4=$("#a4").val(); a4=parseFloat(a4); 
    		a5=$("#a5").val(); a5=parseFloat(a5);a6=$("#a6").val(); a6=parseFloat(a6);a7=$("#a7").val(); a7=parseFloat(a7);a8=$("#a8").val(); a8=parseFloat(a8);
    		a9=$("#a9").val(); a9=parseFloat(a9);a10=$("#a10").val(); a10=parseFloat(a10);
    		a11=$("#a11").val(); a11=parseFloat(a11);a12=$("#a12").val(); a12=parseFloat(a12);a13=$("#a13").val(); a13=parseFloat(a13);a14=$("#a14").val(); a14=parseFloat(a14); 
    		a15=$("#a15").val(); a15=parseFloat(a15);a16=$("#a16").val(); a16=parseFloat(a16);
    		z1=$("#z1").val();z2=$("#z2").val();z3=$("#z3").val();z4=$("#z4").val();z5=$("#z5").val();z6=$("#z6").val();z7=$("#z7").val();z8=$("#z8").val();z9=$("#z9").val();
    		z10=$("#z10").val();z11=$("#z11").val();z12=$("#z12").val();z13=$("#z13").val();z14=$("#z14").val();z15=$("#z15").val();z16=$("#z16").val();
    		$(document).ready(function() {
        		chart = new Highcharts.Chart({
            		chart: { renderTo: 'containere', type: 'bar' },
            		title: { text: '' },
            		subtitle: { text: '' },
            		xAxis: { categories: [ z1, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, z14, z15, z16 ] },
            		yAxis: { min: 0, title: { text: 'Postlarvas (millones)' }  },
            		legend: { layout: 'horizontal', backgroundColor: '#FFFFFF', align: 'left', verticalAlign: 'top', x: 570, y: 250, floating: true, shadow: true },
            		tooltip: {
                		formatter: function() {
                    		return ''+
                        		this.x +': '+ this.y +' mlls';
                		}
            		},
            		plotOptions: { column: { pointPadding: 0.2, borderWidth: 0 } },
            		series: [{
                		name: 'Zonas',
                		data: [ a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16]
            		}
            		]
        		});
    		});
    		
	    });
	    $('#tabla_zon tbody tr td:nth-child(2)').map(function(){ $(this).css('text-align','right'); })   	
	   	$('#tabla_zon tbody tr td:nth-child(3)').map(function(){ $(this).css('text-align','right'); })
    }, 
   });
});
</script>
