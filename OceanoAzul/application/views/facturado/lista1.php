<?php $this->load->view('header_cv'); 

?>



<script>

	$(function() {
		$("#accordion").accordion({
			animated:'bouncelside'
		});
	});
</script>

<style>        
.tab_content {
	padding: 10px;
	font-size: 1.2em;
	height: 452px;
}
ul.tabs li {
	/*border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 1px ;*/
	font-size: 25px;
	/*background: none;*/			
}   
</style>

<div class="container" id="principal" style="background-color:#FFFFFF" >	
	<ul class="tabs">
		<strong>
		<li><a href="#entregas"><img src="<?php echo base_url();?>assets/images/menu/entregado.png" width="25" height="25" border="0"> Facturado </a></li>			
		<li style="font-size: 20px"><a href="#grafica"><img src="<?php echo base_url();?>assets/images/grafica.png" width="25" height="25" border="0"> Gráfica </a></li>
		</strong>
	</ul>
	<div class="tab_container" id="tabPrincipal" >
		<div id="entregas" class="tab_content" >
		  <div id="accordion" style="margin-top: -10px; margin-left: -10px; width: 958px;">
			<h3 align="left" style="font-size: 12px" ><a href="#"> Entregas Actuales </a></h3>
       		<div >                                                                      		
            	<div class="ajaxSorterDiv" id="tabla_fac" name="tabla_fac" style="margin-top: -21px;" >                
               		<div align="left">
               		 Ciclo:
                    <select name="cmbCiclo" id="cmbCiclo" style="font-size: 10px; height: 24px;" >
						<?php $ciclof=2008; $actual=date("Y"); //$actual+=1;
							while($actual >= $ciclof){?>
								<option value="<?php echo $actual;?>" > <?php echo $actual;?> </option>
            				<?php  $actual-=1; } ?>
          			</select>
                	Seleccione Zona:
                    <select name="cmbZona" id="cmbZona" style="font-size: 10px; height: 24px;">
						<option value="0">Todos</option>			<option value="1">Angostura</option>
						<option value="2">Campeche</option>			<option value="3">Colima</option>
						<option value="4">El Dorado</option>		<option value="5">Elota</option>			
						<option value="6">Guasave</option>			<option value="7">Guerrero</option>
						<option value="8">Hermosillo</option>		<option value="9">Mochis</option>
						<option value="10">Navojoa</option>			<option value="11">Navolato</option>
						<option value="12">Nayarit</option>			<option value="13">Obregon</option>	
						<option value="14">Sur Sinaloa</option>		<option value="15">Tamaulipas</option>
						<option value="16">Yucatán</option>
					</select> 
					</div>         	               
                
                <span class="ajaxTable" style="height: 315px" >
                    <table id="mytablaR" name="mytablaR" class="ajaxSorter">
                        <thead title="Presione las columnas para ordenarlas como requiera">                            
                            <th data-order = "Zona" >Zona</th>
                            <th data-order = "Razon">Razón Social</th>                                                        
                            <th data-order = "CantidadRR" style="width: 70px;">Cantidad</th>                                                        
                        </thead>
                        <tbody title="Seleccione para analizar sus entrregas a detalle" style="font-size: 11px">                        	
                        </tbody>
                    </table>
                </span> 
             	<div class="ajaxpager">        
                    <ul class="order_list"></ul>     
                    <form method="post" action="<?= base_url()?>index.php/facturado/pdfrep" >  
                    	<input readonly="true" type="hidden"  name="zonasel" id="zonasel" ></th>                  	             	                                                
                        <img title="Enviar datos de tabla en archivo PDF" class="exporter" src="<?= base_url()?>assets/img/sorter/pdf.png" />                                                                      
                        <!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                             
 					</form>    					
                </div>        
            </div>    
        </div>        
        <h3 align="left" style="font-size: 12px"><a href="#">Analizar Cliente</a></h3>
        <div style="height:150px; ">  
        	 <div class="ajaxSorterDiv" id="tabla_ent" name="tabla_ent" >                
                <div style="text-align:left" class='filter' >                	 
                	<input type="hidden"  size="8%" name="numcli" id="numcli"/>									                	
					<th colspan="6" style="font-size: 14px;"><input style="border:0; background-color: lightgray" readonly="true" size="150%"  type="text" name="nombre" id="nombre" ></th>		                 	                
                </div>
                <span class="ajaxTable" style="height: 290px; background-color: white">
                    <table id="mytablaEnt" name="mytablaEnt" class="ajaxSorter" >
                        <thead title="Presione las columnas para ordenarlas como requiera">                            
                            <th data-order = "FechaR" style="width: 50px;">Fecha</th>
                            <th data-order = "RemisionR" style="width: 50px;">Remisión</th>                                                        
                            <th data-order = "CantidadRR" style="width: 50px;">Cantidad</th>                                                        
                            <th data-order = "PrecioR" style="width: 50px;">Precio</th>
                            <th data-order = "Importe" style="width: 50px;">Importe</th>
                            <th data-order = "Folio" style="width: 50px;">Folio</th>
                        </thead>
                        <tbody title="Seleccione para analizar cliente" style="font-size: 11px">
                        </tbody>
                    </table>
                </span> 
             	<div class="ajaxpager" style="">        
                    <ul class="order_list"></ul>     
                    <form method="post" action="<?= base_url()?>index.php/facturado/pdfrepdetalle" >    
                    	<input readonly="true" type="hidden" name="cliente" id="cliente" ></th>                	             	                                                
                                                                      
                        <img title="Enviar datos de tabla en archivo PDF" class="exporterent" src="<?= base_url()?>assets/img/sorter/pdf.png" />
                        <!--<img title="Enviar datos de tabla a Impresora" class="printer" src="<?= base_url()?>assets/img/sorter/print.png" data-url="<?= base_url()?>" />-->  
                        <input type="hidden" name="tabla" value ="" class="htmlTable"/>                                                      
 					</form>    					
                </div>                                      
            </div>                       
        </div>	
	 </div> 
	 </div>	
	 <div id="grafica" class="tab_content"  >	
			<?php $c=0;$cont=0;$tot=0; $cantidad=0; //$selciclo=2013;
			$this->load->model('facturado_model');
			$data['result']=$this->facturado_model->gralzona();
			?>
			<table class="tablesorter" style=" font-size:9px; margin-top: -5px " border="2" cellpadding="1" cellspacing="1">
	  			<thead>
	  			  <tr style=" font-size:14px; color:#000000;">	
	  				<th style="text-align: center" colspan="<?php echo $cont; ?>">RESULTADOS GENERALES POR ZONA EN CICLO <input style="background: none; border: none;" size="8%" name="ciclosel" id="ciclosel"/>	</th>
	  			</tr>			
	  		<tr>		
			<?php foreach ($data['result'] as $row): 	$zona=$row->Zona; $zonas[$c]=$row->Zona;$datos[$c]=$row->CantidadRR;$c+=1;$tot+=$row->CantidadRR; ?>	
		    	<th  width="72" style="color:#000000;"><?php echo $zona; ?></th>
			<?php endforeach; ?>
	  		</tr>
	  		</thead>
	  		<tbody>
	  		<tr>
	  		<?php while($cont<=$c-1){ ?>	
		    	<td width="72" aling "right" style=" font-size:9px;color:#000000;"><?php echo number_format($datos[$cont],3); $cont+=1;?></td>
			<?php } ?>
	  		</tr>
	  		</tbody>
	  		 <tr>				
				<?php $c=0;$cont1=1; while($cont1<=$cont){ ?>
	    		<td style="font-size:10px;color:#000000;" ><div align="center"><?php echo number_format((($datos[$c]/$tot)*100),2, '.', ',')."%";?></div></td>
				<?php $cont1+=1;$c+=1; } ?>		
	  		</tr>
	  		<tfoot>
	  			<tr style=" font-size:14px; color:#000000;">	
	  				<th style="text-align: center" colspan="<?php echo $cont; ?>">CANTIDAD TOTAL ENTREGADA -- <?php echo number_format($tot, 3, '.', ','); ?></th>
	  			</tr>
	  		</tfoot>
			</table>
		</div>			
 	</div> 	
</div>

<?php $this->load->view('footer_cv'); ?>

<script type="text/javascript">

$("#tabla_fac").ajaxSorter({
	url:'<?php echo base_url()?>index.php/facturado/tabla',  	
	filters:['cmbZona','cmbCiclo'],	
	onRowClick:function(){
		if($(this).data('numero')>'0'){
    		$("#accordion").accordion( "activate",1 );
       		$("#nombre").val($(this).data('razon'));
       		$("#cliente").val($(this).data('razon'));
       		$("#zona").val($(this).data('zona'));
			$("#numcli").val($(this).data('numero'));
			$("#ciclosel").val($("#cmbCiclo").val());					
			$("#tabla_ent").ajaxSorter({
				url:'<?php echo base_url()?>index.php/facturado/tablaentregado/'+$("#numcli").val(),
				//data:"numcli="+$("#numcli").val(),//+"&ciclo="+$("#ciclosel").val(),//+$("#ciclosel").val(), 
				onSuccess:function(){
    		   	$('#tabla_ent tbody tr').map(function(){
	    		if($(this).data('estatus')=='1'){
	    			$(this).css('background','blue');	    				    				    			    			
	    		}
	    		});
    		},  
			});
		}
    },
    onSuccess:function(){    
    	total = 0;    	
	    	$('#tabla_fac tbody tr').map(function(){	    		
	    		total+=($(this).data('tot')!=' '?parseFloat($(this).data('tot')):0);
	    			    	
	    	});    	
    	tr='<tr><td colspan=2>Totales</td><td align=right>'+(parseFloat(total)).toFixed(3)+'</td></tr>';
    	$('#tabla_fac tbody').append(tr);
    	
    },    
});

$("#cmbZona").change( function(){		
	$("#zonasel").val($("#cmbZona").val());	
 return true;
});
$("#cmbCiclo").change( function(){		
	$("#ciclosel").val($("#cmbCiclo").val());	
	$("#tabla_fac").ajaxSorter({
	url:'<?php echo base_url()?>index.php/facturado/tabla',  	
	filters:['cmbCiclo'],	
});
 return true;
});

</script>
