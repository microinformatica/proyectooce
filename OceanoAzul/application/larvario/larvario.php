<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Larvario extends CI_Controller {
        public function __construct() {
        parent::__construct();	
		$this->load->database();
        $this->load->model('larvario_model');         
        $this->load->library(array('ajaxsorter','session','libreria'));		
        $this->load->helper(array('url','form','html','pdf'));
        $this->load->model('usuarios_model');
		$id_usuario=$this->session->userdata('id_usuario');
	    $this->usuario=$this->session->userdata('nombre');
		$this->perfil=$this->session->userdata('perfil');
		if($id_usuario==false)redirect('login');  
	   }
        
        function index() {
            $this->load->model('larvario_model');
			//$data['result']=$this->larvario_model->Entregado();
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;	
			$rem['result'] = $this->larvario_model->ultimacorrida();
			$uc=0;$uc1=0;
			foreach ($rem['result'] as $row): 
				$uc=$row->ultimo+1;
				$uc1=$row->ultimo+1;											
			endforeach;
			$data['uc']=$uc;$data['uc1']=$uc1;
			$this->load->view('larvario/lista',$data);
        }
		function pdfrep() {
            $this->load->model('larvario_model');
			
			$data['usuario']=$this->usuario;
			$data['perfil']=$this->perfil;
			/*$data['zona']=$this->input->post('zonasel');
			$data['ciclo']=$this->input->post('ciclosel2');*/
			$this->load->view('larvario/lista',$data);
			$data['tablac'] = $this->input->post('tablacos');
			$data['cosal'] = $this->input->post('cosal');
			$data['grafica'] = $this->input->post('imgcs');
			$html = $this->load->view('larvario/listapdf', $data, true);  
			pdf ($html,'C'.$data['grafica'], true);		
        	set_paper('letter');
        }
		function tablasallar(){        
        	$filter = $this->ajaxsorter->filter($this->input);  
			$data['rows'] = $this->larvario_model->getsalLar($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function tablapilsie($co=''){        
        	$filter = $this->ajaxsorter->filter($this->input);
			if($co>0) $filter['where']['nco =']=$co;  
			$data['rows'] = $this->larvario_model->getsalcos($filter);
        	echo '('.json_encode($data).')'; 
    	}
		function actualizars($id=0){
			$this->load->model('larvario_model');
			$id=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$sal=$this->input->post('sal');
			if($id!=''){
				$return=$this->larvario_model->actualizars($id,$fec,$sal); 			
				redirect('larvario');
			}
		}
		function agregars(){
			$fec=$this->input->post('fec');
			$sal=$this->input->post('sal');
			$uc=$this->input->post('uc');
			if($sal!=''){	
				$this->larvario_model->agregars($fec,$sal,$uc);			
				redirect('larvario');
			}
		} 
		function actualizarp($id=0){
			$this->load->model('larvario_model');
			$id=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$can=$this->input->post('can');
			$mad=$this->input->post('mad');
			$pil=$this->input->post('pil');
			$nco=$this->input->post('nco');
			if($id!=''){
				$return=$this->larvario_model->actualizarp($id,$fec,$can,$mad,$pil,$nco); 			
				redirect('larvario');
			}
		}
		function actualizarc($id=0){
			$this->load->model('larvario_model');
			$id=$this->input->post('id'); 
			$fec=$this->input->post('fec');
			$p1=$this->input->post('p1');
			$o1=$this->input->post('o1');
			$p2=$this->input->post('p2');
			$o2=$this->input->post('o2');
			$p3=$this->input->post('p3');
			$o3=$this->input->post('o3');
			if($p3=='') $p3=0;
			if($o3=='') $o3=0;
			$pesa=$this->input->post('pesa');
			$repa=$this->input->post('repa');
			$cos=$this->input->post('cos');
			$te1=$this->input->post('te1');
			$ce1=$this->input->post('ce1');
			$te2=$this->input->post('te2');
			$ce2=$this->input->post('ce2');
			if($ce1=='') $ce1=0;
			if($ce2=='') $ce2=0;
			if($id!=''){
				$return=$this->larvario_model->actualizarc($id,$fec,$p1,$o1,$p2,$o2,$p3,$o3,$pesa,$repa,$cos,$te1,$ce1,$te2,$ce2); 			
				redirect('larvario');
			}
		}
		function agregarp(){
			$fec=$this->input->post('fec');
			$can=$this->input->post('can');
			$mad=$this->input->post('mad');
			$pil=$this->input->post('pil');
			$nco=$this->input->post('nco');
			if($fec!=''){	
				$this->larvario_model->agregarp($fec,$can,$mad,$pil,$nco);			
				redirect('larvario');
			}
		} 
    }
    
?>