<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Ventas_model extends CI_Model {
        
		
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function getVentas($filter){
			
			/*select year(FechaR) as ao,month(FechaR) as mes,sum(CantidadR-(CantidadR*DescuentoR)) as canti from r16 where Tipo=1 and Estatus=0 group by mes 
union all select year(FechaR) as ao,month(FechaR) as mes,sum(CantidadR-(CantidadR*DescuentoR)) as canti from r15 where Tipo=1 and Estatus=0 group by mes  
union all select year(FechaR) as ao,month(FechaR) as mes,sum(CantidadR-(CantidadR*DescuentoR)) as canti from r14 where Tipo=1 and Estatus=0 group by mes  
union all select year(FechaR) as ao,month(FechaR) as mes,sum(CantidadR-(CantidadR*DescuentoR)) as canti from r13 where Tipo=1 and Estatus=0 group by mes  
union all select year(FechaR) as ao,month(FechaR) as mes,sum(CantidadR-(CantidadR*DescuentoR)) as canti from r12 where Tipo=1 and Estatus=0 group by mes  
union all select year(FechaR) as ao,month(FechaR) as mes,sum(CantidadR-(CantidadR*DescuentoR)) as canti from r11 where Tipo=1 and Estatus=0 group by mes  
union all select year(FechaR) as ao,month(FechaR) as mes,sum(CantidadR-(CantidadR*DescuentoR)) as canti from r10 where Tipo=1 and Estatus=0 group by mes  
union all select year(FechaR) as ao,month(FechaR) as mes,sum(CantidadR-(CantidadR*DescuentoR)) as canti from r09 where Tipo=1 and Estatus=0 group by mes  
union all select year(FechaR) as ao,month(FechaR) as mes,sum(CantidadR-(CantidadR*DescuentoR)) as canti from r08 where Tipo=1 and Estatus=0 group by mes  
order by ao desc,mes*/
			
			// se determina el ciclo con el que iniciara
			$tbl=16;
			$this->db->select('MAX(NumRegR) as ultimo');
			$result = $this->db->get('r'.$tbl);
			foreach ($result->result() as $row):
		 	if(($row->ultimo+1)>1) {$ciclo="r".$tbl; $ciclo1=$ciclo; }	
		 	else{
				 $tbl=date("Y"); $tbl=2016; $tbl=explode("0",$tbl,2); $tbl=(int)$tbl[1];
			 	if($tbl<10){ $ciclo="r0".$tbl;}		 
		 	 	else { $ciclo="r".$tbl; }
			 	$ciclo1=$ciclo;
		 	}
			endforeach;
		 	$veces=($tbl-8)+1;
			//$ciclo="r13";
			//$veces=6; 
			$contador=1;			
			$data = array();
	 		while($contador<=$veces){
		 		$mes=1; 
				//$sqlConsulta="SELECT sum(CantidadR-(CantidadR*DescuentoR)) as canti From ".$ciclo." WHERE month(FechaR) = 12 AND Estatus=0 and Tipo=1 AND NumRegR>0 GROUP BY Tipo,month(FechaR)";
				//aqui imprime diciembre
				$this->db->select('sum(CantidadR-(CantidadR*DescuentoR)) as canti');
				//$this->db->where('month(FechaR) =',12);
				//$this->db->where('Estatus =',0);
				if($filter['where']!=''){$this->db->where($filter['where']);}
				$this->db->where('Tipo =',1);
				$this->db->where('NumRegR >',0);
				$this->db->group_by(array("Tipo", "month(FechaR)"));
				$result = $this->db->get($ciclo);	
				$cantidadre=$result->num_rows();
 				$cantidadt=0;
				//if($cantidadre>=1){
				foreach ($result->result() as $row):	
						
				endforeach;
				if($tbl<10){ $row->cic = "200".$tbl; } else {$row->cic = "20".$tbl; }
				//$data[] = $row;
				$this->db->select('sum(CantidadR-(CantidadR*DescuentoR)) as canti');
				$this->db->where('month(FechaR) =',12);
				$this->db->where('Estatus =',0);
				$this->db->where('Tipo =',1);
				$this->db->where('NumRegR >',0);
				$this->db->group_by(array("Tipo", "month(FechaR)"));
				$result = $this->db->get($ciclo);	
				$cantidadre=$result->num_rows();
 				$cantidadt=0;
				if($cantidadre>=1){
				foreach ($result->result() as $row1):	
					$row->dic = number_format($row1->canti, 3, '.', ',');$row->dic1 = $row1->canti;
					$cantidadt=$row1->canti;
					//$data[] = $row;	
				endforeach;	
				} else {
					$row->dic = ""; $row->dic1 = 0;
				}
				 //aqui se obtiene de enero a septiembre
				while($mes<=11){
					//$sqlConsulta="SELECT sum(CantidadR-(CantidadR*DescuentoR)) as canti From ".$ciclo." WHERE month(FechaR)  = ".$mes." AND Estatus=0 and Tipo=1 AND NumRegR>0  GROUP BY Tipo,month(FechaR)";
					$this->db->select('sum(CantidadR-(CantidadR*DescuentoR)) as canti');
					$this->db->where('month(FechaR) =', $mes);
					//$this->db->where('Estatus =',0);
					if($filter['where']!=''){$this->db->where($filter['where']);}
					$this->db->where('Tipo =',1);
					$this->db->where('NumRegR >',0);
					$this->db->group_by(array("Tipo", "month(FechaR)"));
					$result = $this->db->get($ciclo);
					$cantidadre=$result->num_rows();
					if($cantidadre>=1){
					foreach ($result->result() as $row2):	
						if($mes==1){$row->ene = number_format($row2->canti, 3, '.', ',');$row->ene1 = $row2->canti;}
						if($mes==2){$row->feb = number_format($row2->canti, 3, '.', ',');$row->feb1 = $row2->canti;}
						if($mes==3){$row->mar = number_format($row2->canti, 3, '.', ',');$row->mar1 = $row2->canti;}
						if($mes==4){$row->abr = number_format($row2->canti, 3, '.', ',');$row->abr1 = $row2->canti;}
						if($mes==5){$row->may = number_format($row2->canti, 3, '.', ',');$row->may1 = $row2->canti;}
						if($mes==6){$row->jun = number_format($row2->canti, 3, '.', ',');$row->jun1 = $row2->canti;}
						if($mes==7){$row->jul = number_format($row2->canti, 3, '.', ',');$row->jul1 = $row2->canti;}
						if($mes==8){$row->ago = number_format($row2->canti, 3, '.', ',');$row->ago1 = $row2->canti;}
						if($mes==9){$row->sep = number_format($row2->canti, 3, '.', ',');$row->sep1 = $row2->canti;}
						if($mes==10){$row->oct = number_format($row2->canti, 3, '.', ',');$row->oct1 = $row2->canti;}
						if($mes==11){$row->nov = number_format($row2->canti, 3, '.', ',');$row->nov1 = $row2->canti;}
						$cantidadt+=$row2->canti;
						//$data[] = $row;	
					endforeach;
					}else{
						if($mes==1){$row->ene = "";$row->ene1 = 0;}
						if($mes==2){$row->feb = "";$row->feb1 = 0;}
						if($mes==3){$row->mar = "";$row->mar1 = 0;}
						if($mes==4){$row->abr = "";$row->abr1 = 0;}
						if($mes==5){$row->may = "";$row->may1 = 0;}
						if($mes==6){$row->jun = "";$row->jun1 = 0;}
						if($mes==7){$row->jul = "";$row->jul1 = 0;}
						if($mes==8){$row->ago = "";$row->ago1 = 0;}
						if($mes==9){$row->sep = "";$row->sep1 = 0;}
						if($mes==10){$row->oct = "";$row->oct1 = 0;}
						if($mes==11){$row->nov = "";$row->nov1 = 0;}
					}	
					$mes=$mes+1;
				}
				//aqui imprime el total por ciclo
				$row->tot = number_format($cantidadt, 3, '.', ',');
				$data[] = $row;	
				//aqui incrementa el contador y toma el siguiente ciclo
				$contador=$contador+1;$tbl-=1;
				if($tbl<10){ $ciclo="r0".$tbl;} else { $ciclo="r".$tbl; }
			}
			return $data;
		}
		function getNumRowsV($filter){
			$tbl=16;
			$this->db->select('MAX(NumRegR) as ultimo');
			$result = $this->db->get('r'.$tbl);
			foreach ($result->result() as $row):
		 	if(($row->ultimo+1)>1) {$ciclo="r".$tbl; $ciclo1=$ciclo; }	
		 	else{
				 $tbl=date("Y"); $tbl=2016; $tbl=explode("0",$tbl,2); $tbl=(int)$tbl[1];
			 	if($tbl<10){ $ciclo="r0".$tbl;}		 
		 	 	else { $ciclo="r".$tbl; }
			 	$ciclo1=$ciclo;
		 	}
			endforeach;
		 	$veces=($tbl-8)+1;			
			return $veces;
		}
		function getVentasC($filter){
			
			// se determina el ciclo con el que iniciara
			//$tbl=13;
			$this->db->select('MAX(NumRegR) as ultimo');
			//If($filter['num']!=0){
				if($filter['num']==2008){$result = $this->db->get('r08'); $tbl1="08"; $tbl=8;}
				if($filter['num']==2009){$result = $this->db->get('r09'); $tbl1="09"; $tbl=9;}
				if($filter['num']==2010){$result = $this->db->get('r10'); $tbl1="10"; $tbl=10;}
				if($filter['num']==2011){$result = $this->db->get('r11'); $tbl1="11"; $tbl=11;}
				if($filter['num']==2012){$result = $this->db->get('r12'); $tbl1="12"; $tbl=12;}
				if($filter['num']==2013){$result = $this->db->get('r13'); $tbl1="13"; $tbl=13;}
				if($filter['num']==2014){$result = $this->db->get('r14'); $tbl1="14"; $tbl=14;}
				if($filter['num']==2015){$result = $this->db->get('r15'); $tbl1="15"; $tbl=15;}
				if($filter['num']==2016){$result = $this->db->get('r16'); $tbl1="16"; $tbl=16;}
			//}
			//$result = $this->db->get('r'.$tbl);
			foreach ($result->result() as $row):
		 	if(($row->ultimo+1)>1) {$ciclo="r".$tbl1; $ciclo1=$ciclo; }	
		 	else{
				 $tbl=date("Y"); $tbl=2016; $tbl=explode("0",$tbl1,2); $tbl=(int)$tbl[1];
			 	if($tbl<10){ $ciclo="r0".$tbl1;}		 
		 	 	else { $ciclo="r".$tbl; }
			 	$ciclo1=$ciclo;
		 	}
			endforeach;
		 	//$veces=($tbl-8)+1;
			//$ciclo="r13";
			$veces=2; 
			$contador=1;			
			$data = array(); $c=0;$datos[$c]=0;
	 		while($contador<=$veces){
		 		$mes=1; 
				//$sqlConsulta="SELECT sum(CantidadR-(CantidadR*DescuentoR)) as canti From ".$ciclo." WHERE month(FechaR) = 12 AND Estatus=0 and Tipo=1 AND NumRegR>0 GROUP BY Tipo,month(FechaR)";
				//aqui imprime diciembre
				$this->db->select('sum(CantidadR-(CantidadR*DescuentoR)) as canti');
				//$this->db->where('month(FechaR) =',12);
				if($filter['where']!=''){$this->db->where($filter['where']);}
				//$this->db->where('Estatus =',0);
				$this->db->where('Tipo =',1);
				$this->db->where('NumRegR >',0);
				$this->db->group_by(array("Tipo", "month(FechaR)"));
				$result = $this->db->get($ciclo);	
				$cantidadre=$result->num_rows();
 				$cantidadt=0;
				//if($cantidadre>=1){
				foreach ($result->result() as $row):	
						
				endforeach;
				if($tbl<10){ $row->cic = "200".$tbl; } else {$row->cic = "20".$tbl; }
				if($filter['num']==2008 && $veces==2) { $row->cic = "2008"; }
				//$data[] = $row;
				
				$this->db->select('sum(CantidadR-(CantidadR*DescuentoR)) as canti');
				$this->db->where('month(FechaR) =',12);
				//$this->db->where('Estatus =',0);
				if($filter['where']!=''){$this->db->where($filter['where']);}
				$this->db->where('Tipo =',1);
				$this->db->where('NumRegR >',0);
				$this->db->group_by(array("Tipo", "month(FechaR)"));
				$result = $this->db->get($ciclo);	
				$cantidadre=$result->num_rows();
 				$cantidadt=0;
				if($cantidadre>=1){
				foreach ($result->result() as $row1):	
					$row->dic = number_format($row1->canti, 3, '.', ',');
					$cantidadt=$row1->canti;
					$datos [$c]=$row1->canti;$c=$c+1;
					//$data[] = $row;	
				endforeach;	
				} else {
					$row->dic = "";$datos [$c]=0;$c=$c+1;
				}
				 //aqui se obtiene de enero a noviembre
				while($mes<=11){
					//$sqlConsulta="SELECT sum(CantidadR-(CantidadR*DescuentoR)) as canti From ".$ciclo." WHERE month(FechaR)  = ".$mes." AND Estatus=0 and Tipo=1 AND NumRegR>0  GROUP BY Tipo,month(FechaR)";
					$this->db->select('sum(CantidadR-(CantidadR*DescuentoR)) as canti');
					$this->db->where('month(FechaR) =', $mes);
					//$this->db->where('Estatus =',0);
					if($filter['where']!=''){$this->db->where($filter['where']);}
					$this->db->where('Tipo =',1);
					$this->db->where('NumRegR >',0);
					$this->db->group_by(array("Tipo", "month(FechaR)"));
					$result = $this->db->get($ciclo);
					$cantidadre=$result->num_rows();
					if($cantidadre>=1){
					foreach ($result->result() as $row2):	
						if($mes==1){$row->ene = number_format($row2->canti, 3, '.', ',');}
						if($mes==2){$row->feb = number_format($row2->canti, 3, '.', ',');}
						if($mes==3){$row->mar = number_format($row2->canti, 3, '.', ',');}
						if($mes==4){$row->abr = number_format($row2->canti, 3, '.', ',');}
						if($mes==5){$row->may = number_format($row2->canti, 3, '.', ',');}
						if($mes==6){$row->jun = number_format($row2->canti, 3, '.', ',');}
						if($mes==7){$row->jul = number_format($row2->canti, 3, '.', ',');}
						if($mes==8){$row->ago = number_format($row2->canti, 3, '.', ',');}
						if($mes==9){$row->sep = number_format($row2->canti, 3, '.', ',');}
						if($mes==10){$row->oct = number_format($row2->canti, 3, '.', ',');}
						if($mes==11){$row->nov = number_format($row2->canti, 3, '.', ',');}
						$cantidadt+=$row2->canti;
						$datos [$c]=$row2->canti;$c=$c+1;
						//$data[] = $row;	
					endforeach;
					}else{
						if($mes==1){$row->ene = "";}
						if($mes==2){$row->feb = "";}
						if($mes==3){$row->mar = "";}
						if($mes==4){$row->abr = "";}
						if($mes==5){$row->may = "";}
						if($mes==6){$row->jun = "";}
						if($mes==7){$row->jul = "";}
						if($mes==8){$row->ago = "";}
						if($mes==9){$row->sep = "";}
						if($mes==10){$row->oct = "";}
						if($mes==11){$row->nov = "";}
						$datos [$c]=0;$c=$c+1;
					}	
					$mes=$mes+1;
				}
				//aqui incrementa el contador y toma el siguiente ciclo
				$contador=$contador+1;$tbl-=1;
				if($tbl<10){ $ciclo="r0".$tbl;} else { $ciclo="r".$tbl; }
				if($tbl<=8){ $ciclo="r08";}
				//aqui imprime el total por ciclo
				$row->tot = number_format($cantidadt, 3, '.', ',');
				$data[] = $row;	
				$datos [$c]=$cantidadt;$c=$c+1;
			}
			//se hace esta consulta solo para poder agregar la diferencia de postlarvas
			$this->db->select('MAX(NumRegR) as ultimo');$resultz = $this->db->get($ciclo);
			foreach ($resultz->result() as $rowz):
		 		$rowz->cic = "Pl's";
				$rowz->dic =number_format($datos[0]-$datos[13], 3, '.', ','); if($rowz->dic==0.000) {$rowz->dic="";}
				$rowz->ene =number_format($datos[1]-$datos[14], 3, '.', ','); if($rowz->ene==0.000) {$rowz->ene="";}
				$rowz->feb =number_format($datos[2]-$datos[15], 3, '.', ','); if($rowz->feb==0.000) {$rowz->feb="";}
				$rowz->mar =number_format($datos[3]-$datos[16], 3, '.', ','); if($rowz->mar==0.000) {$rowz->mar="";}
				$rowz->abr =number_format($datos[4]-$datos[17], 3, '.', ','); if($rowz->abr==0.000) {$rowz->abr="";}
				$rowz->may =number_format($datos[5]-$datos[18], 3, '.', ','); if($rowz->may==0.000) {$rowz->may="";}
				$rowz->jun =number_format($datos[6]-$datos[19], 3, '.', ','); if($rowz->jun==0.000) {$rowz->jun="";}
				$rowz->jul =number_format($datos[7]-$datos[20], 3, '.', ','); if($rowz->jul==0.000) {$rowz->jul="";}
				$rowz->ago =number_format($datos[8]-$datos[21], 3, '.', ','); if($rowz->ago==0.000) {$rowz->ago="";}
				$rowz->sep =number_format($datos[9]-$datos[22], 3, '.', ','); if($rowz->sep==0.000) {$rowz->sep="";}
				$rowz->oct =number_format($datos[10]-$datos[23], 3, '.', ','); if($rowz->oct==0.000) {$rowz->oct="";}
				$rowz->nov =number_format($datos[11]-$datos[24], 3, '.', ','); if($rowz->nov==0.000) {$rowz->nov="";}
				$rowz->tot =number_format($datos[12]-$datos[25], 3, '.', ','); if($rowz->tot==0.000) {$rowz->tot="";}
				$data[] = $rowz;
				
			endforeach;
			//se hace esta consulta solo para poder agregar el porcentaje
			$this->db->select('MAX(NumRegR) as ultimo');$resultp = $this->db->get($ciclo);
			foreach ($resultp->result() as $rowp):
				$rowp->cic = "%";
				if($datos[13]>0){ $rowp->dic=number_format((($datos[0]-$datos[13])/$datos[13]*100), 2, '.', ',')."%";}else{$rowp->dic="";}
				if($datos[14]>0){ $rowp->ene=number_format((($datos[1]-$datos[14])/$datos[14]*100), 2, '.', ',')."%";}else{$rowp->ene="";}
				if($datos[15]>0){ $rowp->feb=number_format((($datos[2]-$datos[15])/$datos[15]*100), 2, '.', ',')."%";}else{$rowp->feb="";}
				if($datos[16]>0){ $rowp->mar=number_format((($datos[3]-$datos[16])/$datos[16]*100), 2, '.', ',')."%";}else{$rowp->mar="";}
				if($datos[17]>0){ $rowp->abr=number_format((($datos[4]-$datos[17])/$datos[17]*100), 2, '.', ',')."%";}else{$rowp->abr="";}
				if($datos[18]>0){ $rowp->may=number_format((($datos[5]-$datos[18])/$datos[18]*100), 2, '.', ',')."%";}else{$rowp->may="";}
				if($datos[19]>0){ $rowp->jun=number_format((($datos[6]-$datos[19])/$datos[19]*100), 2, '.', ',')."%";}else{$rowp->jun="";}
				if($datos[20]>0){ $rowp->jul=number_format((($datos[7]-$datos[20])/$datos[20]*100), 2, '.', ',')."%";}else{$rowp->jul="";}
				if($datos[21]>0){ $rowp->ago=number_format((($datos[8]-$datos[21])/$datos[21]*100), 2, '.', ',')."%";}else{$rowp->ago="";}
				if($datos[22]>0){ $rowp->sep=number_format((($datos[9]-$datos[22])/$datos[22]*100), 2, '.', ',')."%";}else{$rowp->sep="";}
				if($datos[23]>0){ $rowp->oct=number_format((($datos[10]-$datos[23])/$datos[23]*100), 2, '.', ',')."%";}else{$rowp->oct="";}
				if($datos[24]>0){ $rowp->nov=number_format((($datos[11]-$datos[24])/$datos[24]*100), 2, '.', ',')."%";}else{$rowp->nov="";}
				if($datos[25]>0){ $rowp->tot=number_format((($datos[12]-$datos[25])/$datos[25]*100), 2, '.', ',')."%";}else{$rowp->tot="";}
				$data[] = $rowp;
				endforeach;
			return $data;
		}
		function getNumRowsVC($filter){
			$veces=2;			
			return $veces;
		}
		function getZonasvta($filter){
			$mes=0;$ac=date("Y")+1; $data = array(); $tota=0;$totb=0;$totc=0;$totd=0;$tote=0;$totf=0;$totg=0;$toth=0;
  			while($mes<=19){
  				if($mes==0){ $nommes="-"; }
	 			if($mes==1){ $nommes="Angostura"; } if($mes==2){ $nommes="Campeche"; } if($mes==3){ $nommes="Chiapas"; } if($mes==4){ $nommes="Colima"; }
	 			if($mes==5){ $nommes="El Dorado"; } if($mes==6){ $nommes="Elota"; } if($mes==7){ $nommes="Guasave"; } if($mes==8){ $nommes="Guerrero"; }
	 			if($mes==9){ $nommes="Hermosillo"; } if($mes==10){ $nommes="Mochis"; } if($mes==11){ $nommes="Navolato"; } if($mes==12){ $nommes="Navojoa"; } 
	 			if($mes==13){ $nommes="Nayarit";} 
				if($mes==14){ $nommes="Obregon"; } if($mes==15){ $nommes="Sur Sinaloa"; } if($mes==16){ $nommes="Tamaulipas"; } 
				if($mes==17){ $nommes="Tabasco"; }
				if($mes==18){ $nommes="Veracruz";}
				if($mes==19){ $nommes="Yucatan";}
				$this->db->select('max(RemisionR)');
				$resultZ = $this->db->get('r16');	
				foreach ($resultZ->result() as $rowT):
						$rowT->zon=$nommes;$uno=0;$dos=0;
						//$contador=1; 
						$ao=date("Y"); 
						//while($contador<=7){
		  					
		  				
							
						$query=$this->db->query("SELECT sum(CantidadRR) as canti From r16 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->a='';
						foreach ($query->result() as $rowa):
							$var= number_format($rowa->canti, 3, '.', ','); $uno=$rowa->canti; 
							if($rowa->canti>0 && $ao='2016'){$rowT->a= $var;$tota+=$rowa->canti;}else{$rowT->a='';}
						endforeach;	
						$ao=$ao-1;
						
						$query=$this->db->query("SELECT sum(CantidadRR) as canti From r15 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->b='';
						foreach ($query->result() as $rowb):
							$var= number_format($rowb->canti, 3, '.', ','); $dos=$rowb->canti; 
							if($rowb->canti>0 && $ao='2015'){$rowT->b= $var;$totb+=$rowb->canti;}else{$rowT->b='';}
						endforeach;	
						$ao=$ao-1;
						
						//if($uno>0 && $dos=0) $rowT->por="100.00%"; 
						if($dos>0) $rowT->por=number_format((($uno-$dos)/$dos*100), 2, '.', ',')."%";
						elseif($uno==0 && $dos==0)  $rowT->por=''; else $rowT->por='100.00%';
						
						$query=$this->db->query("SELECT sum(CantidadRR) as canti From r14 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->c='';
						foreach ($query->result() as $rowc):
							$var= number_format($rowc->canti, 3, '.', ','); //$dos=$rowb->canti; 
							if($rowc->canti>0 && $ao='2014'){$rowT->c= $var;$totc+=$rowc->canti;}else{$rowT->c='';}
						endforeach;	
						$ao=$ao-1;
					
						$query=$this->db->query("SELECT sum(CantidadRR) as canti From r13 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->d='';
						foreach ($query->result() as $rowd):
							$var= number_format($rowd->canti, 3, '.', ',');
							if($rowd->canti>0 && $ao='2013'){$rowT->d= $var;$totd+=$rowd->canti;}else{$rowT->d='';}
						endforeach;	
						$ao=$ao-1;	
							
						$query=$this->db->query("SELECT sum(CantidadRR) as canti From r12 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->e='';
						foreach ($query->result() as $rowe):
							$var= number_format($rowe->canti, 3, '.', ',');
							if($rowe->canti>0 && $ao='2012'){$rowT->e= $var;$tote+=$rowe->canti;}else{$rowT->e='';}
						endforeach;	
						$ao=$ao-1;
							
						$query=$this->db->query("SELECT sum(CantidadRR) as canti From r11 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->f='';
						foreach ($query->result() as $rowf):
							$var= number_format($rowf->canti, 3, '.', ',');
							if($rowf->canti>0 && $ao='2011'){$rowT->f= $var;$totf+=$rowe->canti;}else{$rowT->f='';}
						endforeach;	
						$ao=$ao-1;	
							
						$query=$this->db->query("SELECT sum(CantidadRR) as canti From r10 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->g='';
						foreach ($query->result() as $rowg):
							$var= number_format($rowg->canti, 3, '.', ',');
							if($rowg->canti>0 && $ao='2010'){$rowT->g= $var;$totg+=$rowg->canti;}else{$rowT->g='';}
						endforeach;	
						$ao=$ao-1;	
							
						$query=$this->db->query("SELECT sum(CantidadRR) as canti From r09 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->h='';
						foreach ($query->result() as $rowh):
							$var= number_format($rowh->canti, 3, '.', ',');
							if($rowh->canti>0 && $ao='2009'){$rowT->h= $var;$toth+=$rowh->canti;}else{$rowT->h='';}
						endforeach;	
						$ao=$ao-1;						
						/*
						$query=$this->db->query("SELECT sum(CantidadRR) as canti From r08 inner join clientes on Numero=NumCliR WHERE Estatus=0 AND Zona='$nommes'");
						$rowT->i='';
						foreach ($query->result() as $rowi):
							$var= number_format($rowi->canti, 3, '.', ',');
							if($rowi->canti>0 && $ao='2008'){$rowT->i= $var;$toti+=$rowi->canti;}else{$rowT->i='';}
						endforeach;	
						$ao=$ao-1;*/
						
						$data[] = $rowT;	
				endforeach;
				$mes=$mes+=1;
			}
			$this->db->select('max(RemisionR)');
			$resultZ = $this->db->get('r16');	
			foreach ($resultZ->result() as $rowT):
			$rowT->zon='Total:';$rowT->por=number_format((($tota-$totb)/$totb*100), 2, '.', ',')."%";;
			$rowT->a=number_format($tota, 3, '.', ',');
			$rowT->b=number_format($totb, 3, '.', ',');
			$rowT->c=number_format($totc, 3, '.', ',');
			$rowT->d=number_format($totd, 3, '.', ',');
			$rowT->e=number_format($tote, 3, '.', ',');
			$rowT->f=number_format($totf, 3, '.', ',');
			$rowT->g=number_format($totg, 3, '.', ',');
			$rowT->h=number_format($toth, 3, '.', ',');
			//$rowT->i=number_format($toti, 3, '.', ',');
			$data[] = $rowT;
			endforeach;
			
			return $data;
		}			
    }    
?>