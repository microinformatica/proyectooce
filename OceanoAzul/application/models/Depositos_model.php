<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Depositos_model extends CI_Model {
        public $nrc="NRC";
		public $id="Numero";
		public $fecha="Fecha";
        public $razon="Razon";		
		public $imported="ImporteD";
		public $pesos="Pesos";
		public $cuenta="Cuenta";
		public $tc="TC";
		public $obs="Obs";
		public $nd="ND";
		public $cam="Cam";
		public $des="Des";
		public $car="Car";
		public $aplicar="Aplicar";
		public $bormaq="bormaq";
		public $tabla="depositos";
        
        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		function getDepositos($filter){
				
			$this->db->select('ND,Fecha,ImporteD,Pesos,TC,Cuenta,Obs,NRC,Cam,Des,Car,Aplicar,Numero,Razon,Zona,bormaq');
			$this->db->join('clientes', 'clientes.Numero=depositos.NRC');			
			$this->db->where('Des',0);
			$this->db->where('Car',0);
			$this->db->order_by($this->fecha,'desc');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			//if($filter['num']!=0){
				/*if($filter['num']>=1 && $filter['num']<=3){
					$this->db->where($this->estatus,$filter['num']);}
				else{
					$this->db->where('r12.FechaR >=' , 'txtFI');
					$this->db->where('r12.FechaR <=' , 'txtFF');
				}*/
			//}
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabla);
				#echo $this->db->last_query();
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();
			//Se forma el arreglo que sera retornado
			$feci=""; $usac=0; $usa=0; $camaron=0; $mex=0; $dolar=0;
			foreach($result->result() as $row):
				$cantidadR=$row->ImporteD;$cantidadRC=$cantidadR;
				$row->ImporteD1=$row->ImporteD;
				$row->Pesos1=$row->Pesos;
				$mn=$row->Pesos;$camaron=$row->Cam;
				$row->Fecha1 = $fec->fecha($row->Fecha);
				//$row->Fecha1 = date("d-m-Y",strtotime($row->Fecha));
				/*if($camaron==0){
					if($row->Pesos==0){ $dolar=$dolar+$cantidadR; } else { $mex=$mex+$row->Pesos; }
						$usa=$usa+$cantidadR; 
				} else {
						$usac=$usac+$cantidadRC; 
				}
				if(($mn>0) and ($camaron==0)){
					 //$row->Pesos = number_format($mn, 2, '.', ','); 
					 $row->Pesos = $mn; } else { $row->Pesos=0; }*/
				if($row->Pesos>0){$row->Pesos2 =  "$ ".number_format($mn, 2, '.', ',');$row->Pesos = number_format($mn, 2, '.', ',');}else{$row->Pesos2 ="";$row->Pesos ="";}					 
				if($row->ImporteD>0){$row->ImporteD2 = "$ ".number_format($cantidadR, 2, '.', ',');$row->ImporteD = number_format($cantidadR, 2, '.', ',');}else{$row->ImporteD2 ="";$row->ImporteD ="";} 				
				//$row->TC = number_format($row->TC, 2, '.', ',');
				$data[] = $row;				
			endforeach;
			//pone el total
			/*
			$this->db->select('sum(Pesos) as mn,sum(ImporteD) as usd');
			$this->db->join('clientes', 'clientes.Numero=depositos.NRC');			
			$this->db->where('Des',0);
			$this->db->where('Car',0);
			if($filter['where']!=''){$this->db->where($filter['where']); }
			//If($filter['limit']!=0)
				$result1 = $this->db->get($this->tabla,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
			//	$result1 = $this->db->get($this->tabla);			
			foreach($result1->result() as $row):
				$usd=$row->usd;
				$mn=$row->mn;
				$row->Fecha="Total: ";				
				$row->Razon="";
				if($row->mn>0){$row->Pesos2 =  "$ ".number_format($mn, 2, '.', ',');}else{$row->Pesos2 ="";}					 
				if($row->usd>0){$row->ImporteD2 = "$ ".number_format($usd, 2, '.', ',');}else{$row->ImporteD2 ="";} 								
				$data[] = $row;				
			endforeach;
			*/
			return $data;
		}
		function getNumRows($filter){
			$this->db->select('ND,Fecha,ImporteD,Pesos,TC,Cuenta,Obs,NRC,Cam,Des,Car,Aplicar,Numero,Razon');
			$this->db->join('clientes', 'clientes.Numero=depositos.NRC');
			$this->db->where('Des',0);
			$this->db->where('Car',0);					
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			$result = $this->db->get($this->tabla);//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
			
		public function agregar($fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$bormaq){
			$usd = str_replace(",", "", $usd);$mn = str_replace(",", "", $mn);
			//Obtengo El Saldo Actual				
			$this->db->select('Saldo');
			$this->db->where('Numero',$numcli);
			$result = $this->db->get('clientes');
			foreach($result->result() as $row):
				$saldoact=$row->Saldo;	
			endforeach;	
			//actualizo saldo de cliente
			$saldoactual=0;
			$saldoactual=$saldoact-$usd;										
			$datasc=array('Saldo'=>$saldoactual,'FecUD'=>$fec,'ImpUD'=>$usd);
			$this->db->where('Numero',$numcli);
			$this->db->update('clientes',$datasc);
						
			$data=array($this->fecha=>$fec,$this->imported=>$usd,$this->tc=>$tc,$this->cuenta=>$cta,$this->obs=>$obs,$this->pesos=>$mn,$this->nrc=>$numcli,$this->aplicar=>$ciclo,$this->bormaq=>$bormaq);			
			$this->db->insert($this->tabla,$data);
			return $this->db->insert_id();
		}	
		public function actualizar($id,$fec,$usd,$tc,$cta,$obs,$mn,$numcli,$ciclo,$bormaq){
			$usd = str_replace(",", "", $usd);				
			//obtengo la cantidaremisionada y el precio de la remision que se va a modificar
			$this->db->select('(ImporteD) as sumar');
			$this->db->where('ND',$id);
			$result = $this->db->get($this->tabla);
			foreach($result->result() as $row):
				$agregale=$row->sumar;	
			endforeach;		
			//if($agregale!=$usd){
				//Obtengo El Saldo Actual	
				$this->db->select('Saldo');
				$this->db->where('Numero',$numcli);
				$result = $this->db->get('clientes');
				foreach($result->result() as $row):
					$saldoact=$row->Saldo;	
				endforeach;
				//al siguiente resultado le voy a sumar lo nuevo
				$saldoprevio=0;
				$saldoprevio=($saldoact+$agregale);
				$saldoactual=0;
				$saldoactual=$saldoprevio-$usd;
				//actualizo saldo de cliente								
				$datasc=array('Saldo'=>$saldoactual,'FecUD'=>$fec,'ImpUD'=>$usd);
				$this->db->where('Numero',$numcli);
				$this->db->update('clientes',$datasc);
			//}
			$mn = str_replace(",", "", $mn);
			
			//$tc = str_replace(",", "", $tc);
			$data=array($this->fecha=>$fec,$this->imported=>$usd,$this->tc=>$tc,$this->cuenta=>$cta,$this->obs=>$obs,$this->pesos=>$mn,$this->nrc=>$numcli,$this->aplicar=>$ciclo,$this->bormaq=>$bormaq);
			$this->db->where($this->nd,$id);
			$this->db->update($this->tabla,$data);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
	public function borrar($id,$usd,$numcli){
			$usd = str_replace(",", "", $usd);				
			//obtengo la cantidaremisionada y el precio de la remision que se va a modificar
			$this->db->select('(ImporteD) as quitar');
			$this->db->where('ND',$id);
			$result = $this->db->get($this->tabla);
			foreach($result->result() as $row):
				$borrale=$row->quitar;	
			endforeach;		
			if($borrale==$usd){
				//Obtengo El Saldo Actual	
				$this->db->select('Saldo');
				$this->db->where('Numero',$numcli);
				$result = $this->db->get('clientes');
				foreach($result->result() as $row):
					$saldoact=$row->Saldo;	
				endforeach;
				//actualizo saldo de cliente
				$saldoactual=0;
				$saldoactual=$saldoact-$usd;										
				$datasc=array('Saldo'=>$saldoactual);
				$this->db->where('Numero',$numcli);
				$this->db->update('clientes',$datasc);
				
				$this->db->where($this->nd,$id);
				$this->db->delete($this->tabla);
				//$this->db->delete($this->tabla,$data);
				if($this->db->affected_rows()>0)
					return 1;
				else {
					return 0;
				}
			}			
		}
	public function getClientes($filter){
			$this->db->select('Numero,Razon,Zona,referencia');
			$this->db->join('refbco', 'idcli = Numero','left');
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!='')
				$this->db->where($filter['where']);
			//Se verifica si el filtro extra enviado es valido, de ser así se considera en la consulta
			if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);
			//Se realiza la consulta con una limitación, en caso de que sea valida
			If($filter['limit']!=0)
				$result = $this->db->get('clientes',$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get('clientes');
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array();
			//Se forma el arreglo que sera retornado
			foreach($result->result() as $row):
				if($row->referencia=='') $row->referencia='';
				//$cp=$row->CP;
				$data[] = $row;	
				/*$data['zona'] = $row->Zona;
				$data['razon'] = $row->Razon;
				$data['dom'] = $row->Dom;
				$data['loc'] = $row->Loc;
				$data['edo'] = $row->Edo;
				$data['rfc'] = $row->RFC;
				$data['cp'] = $row->CP;*/
			endforeach;
			return $data;
		}
		public function getNumClientes($filter){
			$this->db->join('refbco', 'idcli = Numero','left');
			if($filter['where']!='')
				$this->db->where($filter['where']); //Se toman en cuenta los filtros solicitados
			if($filter['num']!=0)
				$this->db->where($this->id,$filter['num']);
			$result = $this->db->get('clientes');//En este caso no es necesario limitar los registros
			return $result->num_rows();//Se regresan la cantidad de registros encontrados e
		}
    }
    
?>