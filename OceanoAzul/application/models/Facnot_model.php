<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Facnot_model extends CI_Model {
		
        public $idctefn="idctefn";public $rfcfn="rfcfn";public $rasfn="rasfn";
		public $tablafn="clientesfn";
		
		public $idfn="idfn";public $idcfn="idcfn";public $numfn="numfn";public $fecfn="fecfn";public $impfn="impfn";
		public $tippd="tippd";public $tippc="tippc";public $tipfn="tipfn";public $estfn="estfn";public $obsfn="obsfn";
		public $efefn="efefn";public $aplfac="aplfac";public $facsd="facsd";public $cicfn="cicfn";
		public $tablafacnot="facnot";
		
		public $fecha="Fecha";public $razon="Razon";public $factura="factura";public $nd="ND";public $estatuss="estatuss";
		public $ngra="ngra";
		public $tabdep="depositos";

        function __construct() {
            parent::__construct(); //llamar al constructor de CI_Model
            $this->load->database(); //carga librerias para manejar db
        }
		
		public function getDatosfn($filter){
			$data = array(); 
			$this->db->order_by($this->rasfn,'ASC');
			if($filter['where']!='')
				$this->db->where($filter['where']);
			If($filter['limit']!=0)
				$result = $this->db->get($this->tablafn,$filter['limit'],$filter['offset']);
			else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablafn);
			foreach ($result->result() as $row):
				$data[] = $row;
			endforeach;	
			return $data;	 
		}
		public function actualizarD($id,$fac,$est,$gra){
			$data=array($this->factura=>$fac,$this->estatuss=>$est,$this->ngra=>$gra); 
			$this->db->where($this->nd,$id);
			$this->db->update($this->tabdep,$data);
			if($this->db->affected_rows()>0){
			return 1;
			}else {
				return 0;
			}
		}
		
		public function quitarabo($id){
			$this->db->where($this->idfn,$id);
			$this->db->delete($this->tablafacnot);
			if($this->db->affected_rows()>0)
				return 1;
			else {
				return 0;
			}
		}
		function getElementsb($filter){
			//$cic=18;        
        	$this->db->select("aplfac,aplfac as val"); 
        	if($filter['where']!='') $this->db->where($filter['where']);
        	$this->db->group_by($this->aplfac);      
        	$result = $this->db->get($this->tablafacnot);
        	$data = array();        
        	foreach($result->result() as $row):
            	$data[] = $row;
        	endforeach;        
        	return $data;
    	}
		
		public function getSaldos($filter){
			//select * from facnot where idcfn=118 and estfn=1 order by fecfn,tipfn
			if($filter['where']!='') $this->db->where($filter['where']);
			//$this->db->order_by($this->fecfn,'DESC');
			//$this->db->order_by($this->numfn,'DESC');
			$this->db->where($this->estfn,1);
			$this->db->where($this->facsd,1);
			//
			$this->db->order_by($this->aplfac);
			$this->db->order_by($this->tipfn);
			$result = $this->db->get($this->tablafacnot);
			$data = array(); $fec=new Libreria();
			if($result->num_rows()>0){
				//$fecha1='';
				foreach ($result->result() as $row):
				$aplfac=$row->aplfac;$car=0;
				$row->fecfn1 = $fec->fecha($row->fecfn);
				/*	if($fecha1!=$row->fecfn){
					$fecha1=$row->fecfn; 
					$row->fecfn1=$row->fecfn;
				} else{ 
		  			$row->fecfn1="";
				}*/
				if($row->tipfn==7) $row->numfn='C'.str_pad($row->numfn, 3, "0", STR_PAD_LEFT);
				if($row->tipfn>=4 and $row->tipfn!=7){
					//$row->numfng=$row->numfn;
					if($row->tipfn==1) $row->numfng='F'.$row->numfn; elseif($row->tipfn==4) $row->numfng='NC'.$row->numfn; elseif($row->tipfn==7) $row->numfng='FC'.$row->numfn; elseif($row->tipfn==8) $row->numfng='PC'.$row->numfn; elseif($row->tipfn==9) $row->numfng='NC-C'.$row->numfn; else $row->numfng='P'.$row->numfn;
					$row->numfn='';
				}else{
					if($row->tipfn==3){
						$row->numfng='Abono';$row->numfn='';
					}else{
						$row->numfng='';
					}
				}
				if($row->tipfn==1 || $row->tipfn==7){
					$row->Cargo1="$ ".number_format($row->impfn, 2, '.', ','); 
					$car=$row->impfn;
					//suma todos los abonos de la factura
					$abono=0;
					$this->db->select('sum(impfn) as abo');	
					$this->db->where($this->aplfac,$aplfac);
					$this->db->where($this->estfn,1);
					if($row->tipfn==1){
						$this->db->where($this->tipfn.' >=',4);
						$this->db->where($this->tipfn.' <=',5);

					}
					if($row->tipfn==7){
						//$this->db->where($this->tipfn.' =',8);
						$this->db->where($this->tipfn.' >=',8);
						$this->db->where($this->tipfn.' <=',9);
						$this->db->where('Year(fecfn) >=',2022);
						$this->db->where($this->tipfn.' !=',7);
						//$this->db->where($this->tipfn.' =',9);
					}
					//$this->db->where($this->tipfn.' !=',6);
					$resultab = $this->db->get($this->tablafacnot);
					foreach ($resultab->result() as $rowab):				
						$abono=$rowab->abo;
					endforeach;	
					 if($abono>0) $row->Abono1="$ ".number_format($abono, 2, '.', ','); else $row->Abono1='';
					 //saca el saldo por la factura
					 if(($car-$abono)==0) $row->saldofila="$ -"; else $row->saldofila="$ ".number_format($car-$abono, 2, '.', ',');
				} else{
					 $row->Cargo1="";
				}
				if($row->tipfn==9 || $row->tipfn==8 || $row->tipfn==5 || $row->tipfn==4 || $row->tipfn==3){
					 $row->Abono1="$ ".number_format($row->impfn, 2, '.', ','); $row->Cargo1=''; $row->saldofila='';
					 $row->saldofila="";
				} else{
					 //$row->Abono1="2";
				}
				//$row->saldofila="$ ".number_format($saldofila, 2, '.', ',');
				$data[] = $row;
				endforeach;	
			}
			return $data;	 
		}
		
		function verClientes(){
			//SELECT idctefn,rasfn from facnot inner join clientesfn on idctefn=idcfn where facsd=1 group by idctefn order by rasfn
			$this->db->join($this->tablafacnot,$this->idcfn.'='.$this->idctefn, 'inner');
			$this->db->where($this->facsd,1);
			$this->db->group_by('idctefn');
			$this->db->order_by('rasfn');
			$query=$this->db->get($this->tablafn);
			return $query->result();			
		}
		
		public function actualizarfacnot($id,$idc,$num,$fec,$imp,$tippd,$tipc,$tipfn,$est,$obs,$efe,$apl,$fsd,$cic,$ban){
			$imp = str_replace(",", "", $imp);
			if($efe==1) $apl=$num; //else $fsd=1;
			if($tipc!=3 and $ban==2) $cic=0;
			$data=array($this->idcfn=>$idc,$this->numfn=>$num,$this->fecfn=>$fec,$this->impfn=>$imp,$this->tippd=>$tippd,$this->tippc=>$tipc,$this->tipfn=>$tipfn,$this->estfn=>$est,$this->obsfn=>$obs,$this->efefn=>$efe,$this->aplfac=>$apl,$this->facsd=>$fsd,$this->cicfn=>$cic); 
			$this->db->where($this->idfn,$id);
			$this->db->update($this->tablafacnot,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		public function agregarfacnot($idc,$num,$fec,$imp,$tippd,$tipc,$tipfn,$est,$obs,$efe,$apl,$fsd,$cic,$ban){
			$imp = str_replace(",", "", $imp);
			if($efe==1) $apl=$num; //else $fsd=1;
			if($tipc!=3 and $ban==2) $cic=0;
			$data=array($this->idcfn=>$idc,$this->numfn=>$num,$this->fecfn=>$fec,$this->impfn=>$imp,$this->tippd=>$tippd,$this->tippc=>$tipc,$this->tipfn=>$tipfn,$this->estfn=>$est,$this->obsfn=>$obs,$this->efefn=>$efe,$this->aplfac=>$apl,$this->facsd=>$fsd,$this->cicfn=>$cic);	
			$this->db->insert($this->tablafacnot,$data);
			return $this->db->insert_id();
		}
		public function actualizarfn($id,$rfc,$ras){
			$imp = str_replace(",", "", $imp);
			$data=array($this->rfcfn=>$rfc,$this->rasfn=>$ras); 
			$this->db->where($this->idctefn,$id);
			$this->db->update($this->tablafn,$data);
			if($this->db->affected_rows()>0)
			return 1;
			else {
				return 0;
			}
		}
		
		
		public function agregarfn($rfc,$ras){
			$data=array($this->rfcfn=>$rfc,$this->rasfn=>$ras);		
			$this->db->insert($this->tablafn,$data);
			return $this->db->insert_id();
		}
		
		public function getDatosfacnot($filter){
			$this->db->join($this->tablafn,$this->idctefn .'='.$this->idcfn, 'inner');
			$this->db->order_by($this->fecfn,'DESC');
			$this->db->order_by($this->numfn,'DESC');
			if($filter['where']!='')
				$this->db->where($filter['where']);
			$this->db->where($this->tipfn.' !=',3);
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tablafacnot,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tablafacnot);
			$data = array();
			if($result->num_rows()>0){
			$fec=new Libreria();$tp=0;$td=0;
			foreach ($result->result() as $row):
				$row->fecfn1 = $fec->fecha($row->fecfn);
				if($row->tippc==1) $row->obsfn1='Postlarva '.$row->obsfn; elseif($row->tippc==2) $row->obsfn1='Camarón '.$row->obsfn; else $row->obsfn1='Camarón Maquilado'.$row->obsfn;
				if($row->tipfn==1) $row->numfn1='F'.$row->numfn; 
				elseif($row->tipfn==7) $row->numfn1='C'.str_pad($row->numfn, 3, "0", STR_PAD_LEFT);
				elseif($row->tipfn==4) $row->numfn1='NC'.$row->numfn; 
				elseif($row->tipfn==5) $row->numfn1='P'.$row->numfn.' F'.$row->aplfac;
				elseif($row->tipfn==8) $row->numfn1='PC'.$row->numfn.' FC'.$row->aplfac;
				elseif($row->tipfn==9) $row->numfn1='NC-C'.$row->numfn;
				if($row->efefn==1) $row->aplfac='';
				if($row->tippd=='MN') {$row->impp='$ '.number_format($row->impfn, 2, '.', ',');$row->impd=''; if($row->estfn==1)$tp+=$row->impfn;}
				else{$row->impd='$ '.number_format($row->impfn, 2, '.', ',');$row->impp='';if($row->estfn==1) $td+=$row->impfn;}
				$row->impfn=number_format($row->impfn, 2, '.', ',');
				$data[] = $row;
			endforeach;	
			$this->db->select('max(idctefn)');	
			$result = $this->db->get($this->tablafn);
			foreach ($result->result() as $row):				
				$row->fecfn1 = "Total:"; $row->numfn1="";$row->rasfn="";
				if($tp>0) $row->impp='$ '.number_format($tp, 2, '.', ','); else $row->impp=''; 		 					
				if($td>0) $row->impd='$ '.number_format($td, 2, '.', ','); else $row->impd='';
				$row->obsfn1="";				
				$data[] = $row;	
			endforeach;	
			}
			return $data;	 
		}
		function historyrfc($rfc){
			$this->db->select($this->rfcfn." ,".$this->rasfn);
			$this->db->from($this->tablafn);
			$this->db->where('rfcfn =', $rfc);
			$query=$this->db->get();
			return $query->row();
		}	
		
		function historyfn1($fn){
			$this->db->select('max(numfn) as numfn');
			$this->db->from($this->tablafacnot);
			$this->db->where('tipfn =', $fn);
			$query=$this->db->get();
			return $query->result();
		}
		function historyfn($fn){
			//select max(numfn) as ultimo from facnot where tipfn=1									
			$this->db->select('max(numfn) as ultimo');
			$this->db->where('tipfn =', $fn);
			$query = $this->db->get($this->tablafacnot);
			return $query->row();
		}
		function ultimafn(){									
			$this->db->select('MAX(numfn) as ultimo,MAX(idfn) as ultimoR');
			$query = $this->db->get($this->tablafacnot);
			return $query->result();
		}
		function getDep($filter){
			$this->db->select('ND,Fecha,ImporteD,Pesos,TC,Cuenta,Obs,NRC,Cam,Des,Car,Aplicar,evo,factura,Numero,Razon,Zona,estatuss,ngra');
			$this->db->join('clientes', 'clientes.Numero=depositos.NRC');			
			$this->db->where('Des',0);
			$this->db->where('Car',0);
			$this->db->order_by($this->fecha);
			$this->db->order_by($this->razon);
			//Se verifica si alguna ordenación es necesaria, de ser así se considera en la consulta
			if($filter['order']!='')
				$this->db->order_by($filter['order']);	
			//Se verifica si existen condiciones por medio del filtrado, de ser así se considera en la consulta
			if($filter['where']!=''){
				$this->db->where($filter['where']); }
			
			//Se realiza la consulta con una limitación, en caso de que sea valida
			//If($filter['limit']!=0)
			//	$result = $this->db->get($this->tabdep,$filter['limit'],$filter['offset']);
			//else //Si no es valida se realiza una consulta general, esto se realiza con propósitos comunes como
				$result = $this->db->get($this->tabdep);
				#echo $this->db->last_query();
			//Se inicializa un arreglo para el caso de que la consulta retorne algo vacío
			$data = array(); $fec=new Libreria();
			//Se forma el arreglo que sera retornado
			$feci=""; $usac=0; $usa=0; $camaron=0; $mex=0; $dolar=0; $feca=''; $tota=0;
			if($result->num_rows()>0){
			foreach($result->result() as $row):
				$cantidadR=$row->ImporteD;$cantidadRC=$cantidadR;
				$row->ImporteD1=$row->ImporteD;
				$row->Pesos1=$row->Pesos;
				$mn=$row->Pesos;$camaron=$row->Cam;
				if($row->factura=='0') $row->factura="";
				if($feca!=$row->Fecha){
						if($tota>0){ $tota=0;	
							$this->db->select('max(idctefn)');	
							$resulta = $this->db->get($this->tablafn);
							foreach ($resulta->result() as $rowa):				
								$rowa->Fecha1 = "Total:"; $rowa->Razon="";$rowa->factura="";$rowa->estatuss="";
								if($mex>0) $rowa->Pesos2='$ '.number_format($mex, 2, '.', ','); else $rowa->Pesos2=''; 		 					
								if($dolar>0) $rowa->ImporteD2='$ '.number_format($dolar, 2, '.', ','); else $rowa->ImporteD2='';
								$data[] = $rowa;	
								$mex=0; $dolar=0;
							endforeach;		
						}
						$feca=$row->Fecha; 
						$row->Fecha1 = $fec->fecha($row->Fecha);
				}else{ $row->Fecha1="";} 
				
				if($row->Pesos>0){$row->Pesos2 =  number_format($mn, 2, '.', ',');$row->ImporteD2 =""; $mex+=$mn; $row->impo='$ '.$row->Pesos2.' MN';}else{					 
				$row->ImporteD2 = number_format($cantidadR, 2, '.', ',');$row->Pesos2 = ''; $dolar+=$cantidadR;	
				$row->impo='$ '.$row->ImporteD2.' USD';	
				}	
				if($row->estatuss==0){ $row->estatuss='';}	
				$tota+=1;	
				$data[] = $row;				
			endforeach;
			$this->db->select('max(idctefn)');	
			$resulta = $this->db->get($this->tablafn);
			foreach ($resulta->result() as $rowa):				
				$rowa->Fecha1 = "Total:"; $rowa->Razon="";$rowa->factura="";$rowa->estatuss="";
				if($mex>0) $rowa->Pesos2='$ '.number_format($mex, 2, '.', ','); else $rowa->Pesos2=''; 		 					
				if($dolar>0) $rowa->ImporteD2='$ '.number_format($dolar, 2, '.', ','); else $rowa->ImporteD2='';
				$data[] = $rowa;	
			endforeach;	
			}
			return $data;
		}
    }
    
?>