$(document).ready(function(){
						   
 $(".accordion h3:first").addClass("active");//Pone el primer componente activo
 $(".accordion p:not(:first)").hide();// Oculta todos los componentes menos el primero

 $(".accordion h3").click(function(){//En la funcion click
 $(this).next("p").slideToggle("slow").siblings("p:visible").slideUp("slow");//si el componente esta cerrado lo abre en caso contrario lo cierra
 $(this).toggleClass("active");//Activa el componente
 $(this).siblings("h3").removeClass("active");//Y en caso que este se cierre quita la propiedad activo.
 });

});